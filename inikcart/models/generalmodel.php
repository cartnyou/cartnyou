<?php
class Generalmodel extends CI_Model{
	
    public function __construct(){
        parent::__construct();
    }
	
    public function send_sms($phone,$msg,$countryCode=91) {
        $msg = urlencode($msg);
        $mobile = urlencode($countryCode.$phone);
        $ch = curl_init('http://api.msg91.com/api/sendhttp.php?country=IN&sender=Cartnu&message='.$msg.'&mobiles='.$mobile.'&authkey=181687ABzhTX6X0l5b6a973f&campaign=OTP&unicode=0&flash=0&route=4');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        curl_close($ch);
    }
	
	
/*'smtp_user' => 'auto-order-confirmation@gojojo.in',
'smtp_pass' => 'Gojojo123#',*/
    public function send_email($to,$subject,$body,$from) {
        $config = Array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => '465',
            'smtp_user' => 'no-reply@cartnyou.com',
            'smtp_pass' => 'Noreply123#',
            'mailtype'  => 'html',
            'newline'   => "\r\n"
        );
        $this->email->initialize($config);
        $this->email->from('no-reply@cartnyou.com',$from);
        $this->email->to($to);
        //$this->email->bcc('brijesh@inikworld.com.in');
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function url_shortener($longUrl) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("longUrl"=> $longUrl)));    
        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAgJ979xWBdQMnrh18WO19LDDCIRgUhM_4');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        $decodedResponse = json_decode($response, true);
        if(isset($decodedResponse['id'])){
            return $decodedResponse['id'];
        }
        return false;
    }

    public function hit_delhivery($method,$endpoint,$data) {

        //test
        //$token      = '96d998b6bf83952fc868fc4e424ba0d2ab5b1fd8';
        //$base_url   = 'https://test.delhivery.com/api/';
        //production
        $token    = 'dcb408b598ecb305a76d148321537767af2173d5';
        $base_url = 'https://track.delhivery.com/api/';

        /*if($endpoint == 'invoice/charges'){
            $token    = 'dcb408b598ecb305a76d148321537767af2173d5';
            //$base_url = 'https://track.delhivery.com/kinko/api/';
            $base_url = 'https://track.delhivery.com/api/kinko/v1/';
        }*/

        if($method == 'get'){

            if($endpoint == 'kinko/v1/invoice/charges'){
                $url = $base_url.$endpoint.'/.json?token='.$token.'&';
            } else {
                $url = $base_url.$endpoint.'/json/?token='.$token.'&';
            }

            if(!empty($data)){
                $data_str = array();
                foreach ($data as $key => $value) {
                    $data_str[] = $key.'='.$value;
                }
                $url .= implode('&', $data_str);
            }
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                    'Authorization: Token '.$token,
                                                    'Content-Type: application/json'
                                                ));
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);
        
        } else if($method == 'post'){

            $url = $base_url.$endpoint;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                                    'Authorization: Token '.$token,
                                                    'Content-Type: application/json'
                                                ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'format=json&data='.json_encode($data));
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);
        }
        return $response;
    }   

    public function shorten_string($string, $wordsreturned, $withDots=true)
    {
      $retval = $string;
      $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
      $string = str_replace("\n", " ", $string);
      $array = explode(" ", $string);
      if (count($array)<=$wordsreturned)
      {
        $retval = $string;
      }
      else
      {
        array_splice($array, $wordsreturned);
        if($withDots){
            $retval = implode(" ", $array)." ...";
        } else {
            $retval = implode(" ", $array);
        }
      }
      return $retval;
    } 

}