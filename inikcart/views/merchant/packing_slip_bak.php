<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
<div style="font-size:12px; font-family:arial, verdana, sans-serif;">
    
    <table style="border:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:50%; vertical-align:top;border-right:1px solid #000;">
                <?php
                if($data->cl_logo != ''){
                    echo '<img src="'.$data->cl_logo.'">';
                } else {
                    echo '<img src="http://gojojo.in/inikcart/themes/gojojo/assets/img/logo-gojojo.png" style="max-width:100%">';
                }
                ?>
            </td>
            <td style="width:50%; vertical-align:top;float: right;">
                <?php
                //if($data->delhivery_logo != ''){
                //    echo '<img src="'.$data->delhivery_logo.'">';
                //} else {
                    echo '<img src="https://freshservice.com/files/2414/8301/5365/delhivery-2x.png" style="max-width:100%">';
                //}
                ?>
            </td>
        </tr>
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;">
                <img src="<?php echo $data->barcode;?>">
            </td>
            <td style="width:40%; vertical-align:top;border-top:1px solid #000;">
                <table>
                    <tr>
                        <td><?php echo $data->wbn;?></td>
                    </tr>
                    <tr>
                        <td><h4><?php echo $data->sort_code;?></h4></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table style="border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:70%; vertical-align:top;border-right:1px solid #000;">
                Shipping Address:
                <h3 style="margin-top:5px;margin-bottom:5px;"><?php echo $data->name;?></h3>
                <h4 style="margin-top:5px;margin-bottom:5px;">Phone: <?php echo $data->contact;?></h4>
                <?php echo $data->address;?><br>
                <?php echo $data->destination_city;?><br>
                <?php echo $data->pin;?>
            </td>
            <td style="width:30%; vertical-align:top;">
                <h2><?php echo $data->pt;?></h2>
            </td>
        </tr>
    </table>

    <table style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:60%; vertical-align:top;border-right:1px solid #000;">
                Seller: <?php echo $data->snm;?><br>
                Address: <?php echo $data->sadd;?>
            </td>
            <td style="width:40%; vertical-align:top;">
                <!-- Tin: <?php echo $data->tin;?><br> -->
                IGST: <?php echo $data->cst;?><br>
                Invoice No.: <?php echo $data->si;?><br>
                Invoice Date: <?php echo str_replace('T',' ', substr($data->sid, 0, 19));?>
            </td>
        </tr>
    </table>

    <table style="border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                Product
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                Price
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                Total
            </td>
        </tr>
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                <?php echo $data->prd;?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($data->rs);?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($data->rs);?>
            </td>
        </tr>
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                <strong>Total</strong>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                <strong>Rs <?php echo format_currency($data->rs);?></strong>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                <strong>Rs <?php echo format_currency($data->rs);?></strong>
            </td>
        </tr>
    </table>

    <table style="border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                <img src="<?php echo $data->oid_barcode;?>">
            </td>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                Return Address:
            </td>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                <?php echo $data->snm;?><br>
                Phone: <?php echo $data->rph[0];?><br>
                <?php echo $data->radd;?><br>
                <?php echo $data->rcty;?><br>
                <?php echo $data->rst;?><br>
                <?php echo $data->rpin;?>
            </td>
        </tr>
        
    </table>

</div>
</body>
</html>