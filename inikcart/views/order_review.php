<!DOCTYPE html>
<html>
  <head>
    <style media="screen">
    .rate{
      width: 60px;
      height: 16px;
      display: inline-block;
      background: url(http://35.167.215.182/cartnyouEmailer/img/st.png) 0 0 no-repeat;
      background-position-x: 0px;
      background-position-y: 0px;
    }
    .rate-0 {
      background-position: -60px 0;
    }
      .rate-1{
        background-position: -47px 0;
      }
      .rate-2{
        background-position: -35px 0;
      }
      .rate-3{
        background-position: -23px 0;
      }
      .rate-4{
        background-position: -11px 0;
      }
      .rate-5{
        background-position: 0 0;
      }

    </style>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">
  <table style="background: #f9f9f9;padding: 0 10px;" width="600" cellspacing="0" cellpadding="0" border="0">
      <tbody>
      <tr style="background: #f9f9f9;">
          <td width="150" style="padding: 30px 10px 20px 30px;text-align: left;border-bottom: 2px solid #ddd;">
              <a target="_blank" href="<?php echo site_url('secure/orders');?>" style="color:#0071bb;text-decoration: none; border-right: 1px solid #fff;padding-right: 5px;">Your Orders</a>
          </td>
          <td width="300" style="padding: 30px 0px 8px 10px;width: 130px;border-bottom: 2px solid #ddd;"><a href="<?php echo site_url();?>"><img style="width: 150px;max-width: 200px;margin: 0 auto;" border="0" src="<?php echo theme_img('cartnyouEmailer/img/logo.png');?>" alt="" width="150" /></a></td>
          <td width="150" style="padding: 30px 30px 20px 0px;text-align: right;border-bottom: 2px solid #ddd;">
              <a target="_blank" href="<?php echo site_url('secure/my-account');?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Your Account</a>

          </td>
      </tr>





      </tbody>
  </table>

  <table width="600" style="padding: 0 10px;background: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td width="600" style="width:600px">
              <p style="text-align: left; border-bottom:2px solid #adadad; padding-bottom:15px;">
                Hi  <?php echo '<b>'.$ship_firstname.' '.$ship_lastname.'</b>';?>, can you please take out a minute to give us your valuable feedback!
              </p>
          </td>
      </tr>
      <tr>
          <td width="600" align="left" style="width:600px">
            <span style="color:#1970ca;text-align:left;font-weight: bold;">You Purchased:</span> <br>
            <?php
                $subtotal = 0;
                $product = unserialize($contents);
                //foreach($contents as $cartkey=>$product):
                ?>
              <p style="text-align: left; border-bottom:2px solid #adadad; padding-bottom:15px;vertical-align:top;">
                <!-- <img src="http://35.167.215.182/cartnyouEmailer/img/logo.png" style="vertical-align: top;margin-right: 13px;margin-bottom: 10px;" width="100" alt=""> -->

                <?php
                    $photo = '';
                      if(!empty($product['images'][0]))
                      {
                        $primary = $product['images'][0];
                        foreach(json_decode($product['images']) as $photo)
                          {
                            if(isset($photo->primary))
                            {
                              $primary = $photo;
                            }
                        }

                        $photo = '<img style="vertical-align: middle;margin-right: 13px;margin-bottom: 10px;" width="100" class="img img-responsive" src="'.base_url('uploads/images/small/'.$primary->filename).'" alt="'.$product['seo_title'].'"/>';
                      }
                      echo $photo;
                 ?>
                <?php echo ucwords($product['name']);?> <br>
                <span style="color:#737373;">Delivered on: <?php echo $delivered_at;?></span>
              </p>
              	<?php //endforeach;?>
          </td>
      </tr>
      <tr>
          <td width="600" align="left" style="width:600px">
              <p style="text-align: left;padding-bottom:0;margin-top:10px; font-weight:bold;font-size:16px;">
                Please Select a rating for the Product Experience on these questions.

              </p>
              <p style="margin-top:0;margin-bottom:5px;">Is the product reasonably priced based on the quality of the product?</p>
              <p style="margin-top:0;margin-bottom:5px;">  Are you happy with the value of the product based on the price and quality ?</p>
              <p style="margin-top:0;margin-bottom:5px;">Are you happy with the quality of the product?</p>
              <p style="margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;"> Price
                  <span style=" margin-left:60px;" class="rate-<?php echo round($product_rating->rating_price_avg);?> rate"></span>
                </span> <br>
              </p>
              <p style="margin-top:0;margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;"> Value
                  <span style=" margin-left:57px;" class="rate-<?php echo round($product_rating->rating_value_avg);?> rate"></span>
                </span> <br>
              </p>
              <p style="margin-top:0;margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;"> Quality
                  <span style=" margin-left:48px;" class="rate-<?php echo round($product_rating->rating_quality_avg);?> rate"></span>
                </span> <br> <br>
              </p>

              <p style="text-align: left;margin-bottom:3px; font-weight:bold;font-size:16px;">
                Does the product meet your expectations?
              </p>
              <p style="margin-top:0;margin-bottom:5px; font-weight:bold;font-size:16px;margin-bottom:20px;">
                <a style="color:#1970ca;" href="<?php echo site_url().$product['slug'];?>?review=5">Click Here to Rate & Review the Product</a>
              </p>
          </td>
      </tr>
      </tbody>
  </table>

    <table width="600" style="background-color: #fff;" cellspacing="0" cellpadding="0" border="0">
      <tbody>

      </tbody>
    </table>

  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;padding: 0 10px;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td align="left" width="600" style="">
              <h3 style="font-size: 18px;margin-bottom: 5px;">Thank You!</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">Team CartNYou</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">T: +91-8283004545</h3>

          </td>
      </tr>
      <tr>
          <td align="left" width="600">
              <p style="color: #666;">
                  Check out our <a href="<?php echo site_url('return-policy');?>">Easy Return Policy </a> | <a href="<?php echo site_url('terms-and-conditions');?>">Terms & Conditions</a>
              </p>
              <p style="color: #666;">
                  If you have any queries about your order please <a href="<?php echo site_url('contact-us');?>">Contact Us</a>
              </p>
          </td>
      </tr>
      <tr>
          <td width="600" style="text-align: center;padding: 20px 0;">
              <img src="http://35.167.215.182/cartnyouEmailer/img/logo.png" width="150" alt="">
          </td>

      </tr>
      <tr>
          <td>
              <ul style="padding-left: 0;text-align: center;">
                  <li style="list-style: none;display: inline-block;padding:0 10px 5px 10px;">
                      <a href="https://www.facebook.com/CartnYou-277729052962135/"><img src="<?php echo theme_assets('cartnyouEmailer/img/facebook.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">
                      <a href="https://twitter.com/CartnYou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/twitter.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">

                      <a href="https://www.instagram.com/cartnyou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/instagram.png');?>" width="32" alt="alt"></a>
                  </li>

              </ul>
          </td>
      </tr>

      </tbody>
  </table>
  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td style="border-bottom: 5px solid #445268;"></td>
      </tr>
      </tbody>
  </table>

  </body>
</html>
