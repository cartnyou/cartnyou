<div class="row">
	<div class="span12">
		<strong>Name: </strong><?php echo $row->name;?>
	</div>
	<div class="span12">
		<strong>Email: </strong><?php echo $row->email;?>
	</div>
	<div class="span12">
		<strong>Phone: </strong><?php echo $row->phone;?>
	</div>
	<div class="span12">
		<strong>Query: </strong><?php echo $row->query;?>
	</div>
	<div class="span12">
		<strong>Raised At: </strong><?php echo $row->created_at;?>
	</div>
	<div class="span12">
		<strong>Last Updated At: </strong><?php echo $row->updated_at;?>
	</div>
	<div class="span12">
		<strong>Status: </strong>
		<?php echo ($row->enabled == '1') ? 'Closed' : 'Open';?>
		<?php if($row->enabled == '1'){ ?>
			<a class="btn btn-info" href="<?php echo site_url('admin/queries/query_status/'.$row->q_id);?>">Open</a>
		<?php } else { ?>
			<a class="btn btn-danger" href="<?php echo site_url('admin/queries/query_status/'.$row->q_id);?>">Close</a>
		<?php } ?>
	</div>
</div>

<form action="<?php echo site_url();?>admin/queries/detail/<?php echo $row->q_id;?>" class="form-inline" method="post" accept-charset="utf-8">
	<fieldset>
	    <div class="form-actions">
	    	<div class="row">
		        <div class="span12">
		            <h3>Add Note</h3>
		            <textarea name="notes" class="span12"></textarea>
		        </div>
		    </div>
	        <input class="btn btn-primary" value="Update Query" type="submit">
	    </div>
	</fieldset>
</form>

<?php if(!empty($replies)): ?>
<div class="row">
	<div class="span12">
		<h3>Follow Ups</h3>
	</div>

	<?php
	foreach ($replies as $reply) {
		echo '<div class="span12"><hr><b><em>'.$reply->created_at.'</em></b><br>'.$reply->qr_note.'<br></div>';
	}
	?>
</div>
<?php endif;?>