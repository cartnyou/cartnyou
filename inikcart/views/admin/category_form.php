<?php echo form_open_multipart($this->config->item('admin_folder').'/categories/form/'.$id); ?>

<div class="tabbable">

	<ul class="nav nav-tabs">
		<li class="active"><a href="#description_tab" data-toggle="tab"><?php echo lang('description');?></a></li>
		<li><a href="#attributes_tab" data-toggle="tab"><?php echo lang('attributes');?></a></li>
		<li><a href="#seo_tab" data-toggle="tab"><?php echo lang('seo');?></a></li>
		<li><a href="#special_products" data-toggle="tab">Special Products</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="description_tab">
			
			<fieldset>
				<label for="name"><?php echo lang('name');?></label>
				<?php
				$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'span12');
				echo form_input($data);
				?>
				
				<label for="description"><?php echo lang('description');?></label>
				<?php
				$data	= array('name'=>'description', 'class'=>'redactor', 'value'=>set_value('description', $description));
				echo form_textarea($data);
				?>

				<label for="tax_rate">Tax Rate </label>
				<?php
				$data	= array('name'=>'tax_rate', 'value'=>set_value('tax_rate', $tax_rate));
				echo form_input($data);
				?>

				<label for="commission">Gojojo Commission </label>
				<?php
				$data	= array('name'=>'commission', 'value'=>set_value('commission', $commission));
				echo form_input($data);
				?>

				<label for="enabled"><?php echo lang('enabled');?> </label>
        		<?php echo form_dropdown('enabled', array('0' => lang('disabled'), '1' => lang('enabled')), set_value('enabled',$enabled)); ?>

        		<label for="featured">Featured (Show on Homepage)</label>
        		<?php echo form_dropdown('featured', array('0' => lang('disabled'), '1' => lang('enabled')), set_value('enabled',$featured)); ?>

			</fieldset>
		</div>

		<div class="tab-pane" id="attributes_tab">
			
			<fieldset>
				<label for="slug"><?php echo lang('slug');?> </label>
				<?php
				$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug));
				echo form_input($data);
				?>
				
				<label for="sequence"><?php echo lang('sequence');?> </label>
				<?php
				$data	= array('name'=>'sequence', 'value'=>set_value('sequence', $sequence));
				echo form_input($data);
				?>
				
				<label for="parent_id"><?php echo lang('parent');?> </label>
				<?php
				$data	= array(0 => lang('top_level_category'));
				foreach($categories as $parent)
				{
					if($parent->id != $id)
					{
						$data[$parent->id] = $parent->name;
					}
				}
				echo form_dropdown('parent_id', $data, $parent_id);
				?>

				<label for="slider_banner">Slider Banner </label>
				<?php
				$data	= array(0 => '-Select Slider-');
				foreach($banners as $banner)
				{
					$data[$banner->banner_collection_id] = $banner->name;
				}
				echo form_dropdown('slider_banner', $data, $slider_banner);
				?>

				<label for="side_banner">Side Banner </label>
				<?php
				$data	= array(0 => '-Select Side Banner Slider-');
				foreach($banners as $banner)
				{
					$data[$banner->banner_collection_id] = $banner->name;
				}
				echo form_dropdown('side_banner', $data, $side_banner);
				?>
				
				<label for="excerpt"><?php echo lang('excerpt');?> </label>
				<?php
				$data	= array('name'=>'excerpt', 'value'=>set_value('excerpt', $excerpt), 'class'=>'span12', 'rows'=>3);
				echo form_textarea($data);
				?>

				<label for="icon">Icon </label>
				<div class="input-append">
					<?php echo form_upload(array('name'=>'icon'));?><span class="add-on"><?php echo lang('max_file_size');?> <?php echo  $this->config->item('size_limit')/1024; ?>kb</span>
				</div>
					
				<?php if($id && $icon != ''):?>
				
				<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$icon);?>" alt="current"/><br/><?php echo lang('current_file');?></div>
				
				<?php endif;?>

				<label for="image"><?php echo lang('image');?> </label>
				<div class="input-append">
					<?php echo form_upload(array('name'=>'image'));?><span class="add-on"><?php echo lang('max_file_size');?> <?php echo  $this->config->item('size_limit')/1024; ?>kb</span>
				</div>
					
				<?php if($id && $image != ''):?>
				
				<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image);?>" alt="current"/><br/><?php echo lang('current_file');?></div>
				
				<?php endif;?>
				
			</fieldset>
			
		</div>
		
		<div class="tab-pane" id="seo_tab">
			<fieldset>
				<label for="seo_title"><?php echo lang('seo_title');?> </label>
				<?php
				$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'span12');
				echo form_input($data);
				?>
				
				<label><?php echo lang('meta');?></label> 
				<?php
				$data	= array('rows'=>3, 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'span12');
				echo form_textarea($data);
				?>
				<p class="help-block"><?php echo lang('meta_data_description');?></p>
			</fieldset>
		</div>

		<div class="tab-pane" id="special_products">
			<div class="row">
				<div class="span8">
					<label><strong>Select a Product</strong></label>
				</div>
			</div>
			<div class="row">
				<div class="span4" style="text-align:center">
					<div class="row">
						<div class="span4">
							<input class="span4" type="text" id="product_search" />
							<script type="text/javascript">
							$('#product_search').keyup(function(){
								$('#product_list').html('');
								run_product_query();
							});
					
							function run_product_query()
							{
								$.post("<?php echo site_url($this->config->item('admin_folder').'/products/product_autocomplete/');?>", { name: $('#product_search').val(), limit:10},
									function(data) {
								
										$('#product_list').html('');
								
										$.each(data, function(index, value){
								
											if($('#related_product_'+index).length == 0)
											{
												$('#product_list').append('<option id="product_item_'+index+'" value="'+index+'">'+value+'</option>');
											}
										});
								
								}, 'json');
							}
							</script>
						</div>
					</div>
					<div class="row">
						<div class="span4">
							<select class="span4" id="product_list" size="5" style="margin:0px;"></select>
						</div>
					</div>
					<div class="row">
						<div class="span2" style="margin-top:8px;">
							<a href="#" onclick="add_related_product();return false;" class="btn" title="Add Special Product">Add Special Product</a>
						</div>
					</div>
				</div>
				<div class="span6">
					<table class="table table-striped" style="margin-top:10px;">
						<tbody id="product_items_container">
						<?php
						foreach($special_products as $rel)
						{
							echo related_items($rel->id, $rel->name);
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>

</div>

<div class="form-actions">
	<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
</div>
</form>

<script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>
<script type="text/javascript">
function add_related_product()
{
	//if the related product is not already a related product, add it
	if($('#related_product_'+$('#product_list').val()).length == 0 && $('#product_list').val() != null)
	{
		<?php $new_item	 = str_replace(array("\n", "\t", "\r"),'',related_items("'+$('#product_list').val()+'", "'+$('#product_item_'+$('#product_list').val()).html()+'"));?>
		var related_product = '<?php echo $new_item;?>';
		$('#product_items_container').append(related_product);
		run_product_query();
	}
	else
	{
		if($('#product_list').val() == null)
		{
			alert('Please select a product to add first.');
		}
		else
		{
			alert('This product is already special.');
		}
	}
}

function remove_related_product(id)
{
	if(confirm('Are you sure you want to remove this special product?'))
	{
		$('#related_product_'+id).remove();
		run_product_query();
	}
}
</script>
<?php
function related_items($id, $name) {
	return '
			<tr id="related_product_'.$id.'">
				<td>
					<input type="hidden" name="special_products[]" value="'.$id.'"/>
					'.$name.'</td>
				<td>
					<a class="btn btn-danger pull-right btn-mini" href="#" onclick="remove_related_product('.$id.'); return false;"><i class="icon-trash icon-white"></i> Remove</a>
				</td>
			</tr>
		';
}