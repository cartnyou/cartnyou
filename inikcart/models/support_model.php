<?php
class Support_model extends CI_Model{
	
    public function __construct(){
        parent::__construct();
    }

    public function add_query($save) {
        $this->db->insert('queries',$save);
        $id = $this->db->insert_id();

        $data  = array('ticket_number'=> date('U').$id);
        $this->db->where('q_id', $id);
        $this->db->update('queries', $data);
                        
        return $order_number = $data['ticket_number'];
    }

    function get_queries($user_type, $user_id=false ,$count=false, $rows=false, $page=false, $term=false){
        if($user_type == 'user' || $user_type == 'merchant'){
            $this->db->where('queries.user_type',$user_type);
            $this->db->where('queries.user_id',$user_id);
        }

        if($count){
            return $this->db->count_all_results('queries');
        } else {
            $this->db->select('queries.*');

            if($term){

                $this->db->where('(ticket_number LIKE "%'.$term.'%"');
                $this->db->or_where('email LIKE "%'. $term.'%"');
                $this->db->or_where('phone LIKE "%'. $term.'%")');
            }

            $this->db->order_by('queries.enabled','asc');
            $this->db->order_by('queries.created_at','desc');

            if($rows){
                $this->db->limit($rows);
            }
            if($page){
                $this->db->offset($page);
            }
            return $this->db->get('queries')->result();
        }
    }

    function get_query($q_id){
        $this->db->where('queries.q_id',$q_id);
        return $this->db->get('queries')->row();
    }

    function get_query_replies($q_id){
        $this->db->where('query_replies.q_id',$q_id);
        $this->db->order_by('created_at','desc');
        return $this->db->get('query_replies')->result();
    }

    function query_status($id){
        $this->db->query('UPDATE queries SET enabled = IF(enabled=1, 0, 1) WHERE q_id='.$id);
        return $this->db->affected_rows();
    }

    public function add_note($save) {
        $this->db->insert('query_replies',$save);
        return $this->db->insert_id();
    }

}