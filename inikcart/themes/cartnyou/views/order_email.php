<!DOCTYPE html>
<html>
  <head>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">

    <table style="background: #f9f9f9;padding: 0 10px;" width="600" cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr style="background: #f9f9f9;">
            <td width="150" style="padding: 30px 10px 20px 30px;text-align: left;border-bottom: 2px solid #ddd;">
                <a target="_blank" href="<?php echo site_url('secure/orders');?>" style="color:#0071bb;text-decoration: none; border-right: 1px solid #fff;padding-right: 5px;">Your Orders</a>
            </td>
            <td width="300" style="padding: 30px 0px 8px 10px;width: 130px;border-bottom: 2px solid #ddd;"><a href="<?php echo site_url();?>"><img style="width: 150px;max-width: 200px;margin: 0 auto;" border="0" src="<?php echo theme_img('cartnyouEmailer/img/logo.png');?>" alt="" width="150" /></a></td>
            <td width="150" style="padding: 30px 30px 20px 0px;text-align: right;border-bottom: 2px solid #ddd;">
                <a target="_blank" href="<?php echo site_url('secure/my-account');?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Your Account</a>

            </td>
        </tr>
        </tbody>
    </table>



    <table width="600" style="padding: 0 10px;background: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="600" style="width:600px">
                <p style="padding: 0 40px;">
                    Dear <?php
                    $ship = $customer['ship_address'];
                    echo '<b>'.$ship['firstname'].' '.$ship['lastname'].'</b>';?>
                </p>
                <!--<h3 style="text-align: center;color: #FDA007;font-size: 27px;"> Here’s What You Ordered!</h3>-->
                <p style="padding: 0 40px;">
                    Thanks for Ordering with CartnYou.com

                </p>
                <p style="padding: 0 40px;">
                    Your order ID: <?php echo $order_id ?>
                </p>
                <p width="500" align="left" style="font-family: Arial;font-size: 16px;;padding: 0 40px;">
                   <span style="color:#FDA007">Payment Mode </span> : <?php echo $payment['description']; ?>
                </p>
            <p width="500" align="left" style="font-family: Arial;font-size: 16px;color:#FDA007;padding: 0 40px;">
                Shipping Address
            </p>
            <p width="500" align="left" style="font-family: Arial;color:#102327;padding: 0 40px;">
                <?php
                $ship = $customer['ship_address'];
                echo '<b>'.$ship['firstname'].' '.$ship['lastname'].'</b><br>';
                echo $ship['email'].'<br>';
                echo $ship['phone'].'<br>';
                echo $ship['address1'].'<br>';
                if(!empty($ship['address2'])) echo $ship['address2'].'<br>';
                echo $ship['city'].', '.$ship['zone'].' '.$ship['zip'];
                ?>
            </p>

            </td>
        </tr>
        </tbody>
    </table>

    <table width="600" style="background-color: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="50">
            
          </td>
          <td width="500" style="background-color: #f9f9f9;padding: 15px;">
            <table>
              <tbody>

                <tr>
                  <td width="500" style="border-bottom: 1px solid #828080;color: #102327;padding: 15px 0px;">
                    <table width="500">
                      <tr>
                        <td style="font-family: Arial;text-align: left;">Order #: <?php echo $order_id ?></td>
                        <td align="right" style="text-align: right;font-family: Arial;">Order date: <?php date('Y-m-d H:i:s');?></td>
                      </tr>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="border-bottom: 1px solid #828080;color: #102327;padding: 15px 0px;">
                    <table width="500">
                    	<thead>
	                      <tr>
	                        <td style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px;text-align: left;" width="180">ITEM NAME</td>
	                        <td style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px;text-align: left;" width="80">DESCRIPTION</td>
	                        <td style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px;text-align: left;"  width="80">PRICE</td>
	                        <td style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px;text-align: left;"  width="80">QTY</td>
	                        <td style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px;text-align: left;" width="80">TOTAL</td>
	                      </tr>
                      	</thead>

                      	<tfoot>
                        <?php if($this->go_cart->group_discount() > 0)  : ?>
                            <tr>
                                <td align="left" colspan="4"><?php echo lang('group_discount');?></td>
                                <td align="left"><?php echo format_currency_rs(0-$this->go_cart->group_discount()); ?></td>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <td align="left" colspan="4" style="border-top: 1px solid #828080;"><?php echo lang('subtotal');?></td>
                            <td align="left" style="border-top: 1px solid #828080;"><?php echo format_currency_rs($this->go_cart->subtotal()); ?></td>
                        </tr>
                        <tr>
							<td align="left" colspan="4"><?php echo lang('shipping');?></td>
							<td align="left"><?php echo format_currency_rs($shipping['price']) ?></td>
						<tr>

                        <?php if($this->go_cart->coupon_discount() > 0)  : ?>
                            <tr>
                                <td align="left" colspan="4"><?php echo lang('coupon_discount');?></td>
                                <td align="left"><?php echo format_currency_rs(0-$this->go_cart->coupon_discount()); ?></td>
                            </tr>

                            <?php if($go_cart['order_tax'] != 0) : // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?>
                                <tr>
                                    <td align="left" colspan="4"><?php echo lang('discounted_subtotal');?></td>
                                    <td align="left"><?php echo format_currency_rs($this->go_cart->discounted_subtotal(), 2, '.', ','); ?> </td>
                                </tr>
                            <?php
                            endif;
						endif;
						?>
                        
                        <?php if($this->go_cart->order_tax() != 0) : ?> 
				         	<tr>
								<td align="left" colspan="4"><?php echo lang('taxes');?></td>

								<td align="left"><?php echo format_currency_rs($this->go_cart->order_tax()); ?></td>
							</tr>
			          	<?php endif;   ?>

			           	<?php if($this->go_cart->gift_card_discount() != 0) : ?> 
				         	<tr>
								<td align="left" colspan="4"><?php echo lang('gift_card');?></td>

								<td align="left"><?php echo format_currency_rs($this->go_cart->gift_card_discount()); ?></td>
							</tr>
			          	<?php endif;   ?>
			            <tr> 
							<td align="left" colspan="4">
								<strong><?php echo lang('grand_total');?></strong>
							</td>
							<td align="left">
								<strong><?php echo format_currency_rs($this->go_cart->total()); ?></strong>
							</td>
						</tr>
                        </tfoot>

                      	<tbody>
                      	<?php
						$subtotal = 0;
						foreach($this->go_cart->contents() as $cartkey=>$product):
						?>
                        	<tr>
                                <td style="padding:7px 0;vertical-align: top;" align="left" width="180"><?php echo ucwords($product['name']);?></td>
                                <td style="padding:7px 0;vertical-align: top;" align="left" width="80"><?php echo $product['excerpt'];
                                    if(isset($product['options'])) {
                                        foreach ($product['options'] as $name=>$value)
                                        {
                                            if(is_array($value))
                                            {
                                                echo '<div>'.$name.':<br/>';
                                                foreach($value as $item)
                                                    echo '- '.$item.'<br/>';
                                                echo '</div>';
                                            }
                                            else
                                            {
                                                echo '<div>'.$name.': '.$value.'</div>';
                                            }
                                        }
                                    }
                                    echo "<b>Seller:</b> <br>".$product['store_name'];
                                    ?>
                                </td>
                                <td style="padding:7px 0;vertical-align: top;" align="left" width="80"><?php echo format_currency_rs($product['price']);?></td>
                                <td style="padding:7px 0;vertical-align: top;" align="left" width="80"><?php echo $product['quantity'];?></td>
                                <td style="padding:7px 0;vertical-align: top;" align="left" width="80"><?php echo format_currency_rs($product['price']*$product['quantity']); ?></td>
                            </tr>
                      	<?php endforeach;?>
                      	</tbody>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="text-align:center;font-family: Arial;font-size: 20px;color:#FDA007;padding:15px 0px 5px;">
                      Payable Amount – <?php echo format_currency_rs($this->go_cart->total()); ?> <br>
                      <span style="font-size: 13px;font-weight: normal;color: #9c9c9c;">(Will be shipped in 2-3 working days)</span>
                  </td>
                </tr>
              <tr>
                  <td width="500" align="left" style="font-family: Arial;font-size: 13px;;padding:15px 0px 5px;">
                      Note: After the dispatch, the delivery of your item(s) will approximately be delivered between 1 to 4 days depending on your Address Location. For any information, kindly mail to support@cartnyou.com / Contact  - 011-64606020

                  </td>
              </tr>




              </tbody>
            </table>
          
          </td>
          <td width="50">
            
          </td>
        </tr>
      </tbody>
    </table>

    <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;padding: 0 10px;">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="600" align="left" style="">
                <h3 style="font-size: 18px;margin-bottom: 5px;">Thank You!</h3>
                <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">Team CartNYou</h3>
                <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">T: +91-8283004545</h3>

            </td>
        </tr>
        <tr>
            <td align="left" width="600">
                <p style="color: #666;">
                    Check out our <a href="<?php echo site_url('return-policy');?>">Easy Return Policy </a> | <a href="<?php echo site_url('terms-and-conditions');?>">Terms & Conditions</a>
                </p>
                <p style="color: #666;">
                    If you have any queries about your order please  <a href="<?php echo site_url('contact-us');?>">Contact Us</a>
                </p>
            </td>
        </tr>
        <tr>
            <td width="600" style="text-align: center;padding: 20px 0;">
                <img src="<?php echo theme_img('cartnyouEmailer/img/logo.png');?>" width="150" alt="">
            </td>

        </tr>
        <tr>
            <td>
                <ul style="padding-left: 0;text-align: center;">
                    <li style="list-style: none;display: inline-block;padding:0 10px 5px 10px;">
                        <a href="https://www.facebook.com/CartnYou-277729052962135/"><img src="<?php echo theme_img('cartnyouEmailer/img/facebook.png');?>" width="32" alt=""></a>
                    </li>
                    <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">
                        <a href="https://twitter.com/CartnYou/"><img src="<?php echo theme_img('cartnyouEmailer/img/twitter.png');?>" width="32" alt=""></a>
                    </li>
                    <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">

                        <a href="https://www.instagram.com/cartnyou/"><img src="<?php echo theme_img('cartnyouEmailer/img/instagram.png');?>" width="32" alt="alt"></a>
                    </li>

                </ul>
            </td>
        </tr>

        </tbody>
    </table>
    <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="border-bottom: 5px solid #445268;"></td>
        </tr>
        </tbody>
    </table>



  </body>
</html>