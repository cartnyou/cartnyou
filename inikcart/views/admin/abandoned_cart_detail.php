<?php
$cart = json_decode($row->cart_contents, true);
?>

<div class="row">
	<div class="span12">
		<?php $contents=json_decode($row->cart_contents);?>
		<strong>Customer Name: </strong><?php echo ($row->id!=null)?ucwords($row->firstname.' '.$row->lastname):(($contents->customer)?$contents->customer->firstname:"");?><br>
		<strong>Email: </strong><?php echo ($row->id!=null)?$row->email:(($contents->customer)?$contents->customer->email:"");?><br>
		<strong>Phone: </strong><?php echo ($row->id!=null)?$row->phone:(($contents->customer)?$contents->customer->phone:"");?><br>
		<strong>Last Updated At: </strong><?php echo $row->updated_at;?><br>
	</div>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
    	<?php
        $subtotal           = 0;
        $coupon_discount    = 0;
        $shipping           = 0;
        $total              = 0;
        ?>
        <?php foreach($cart['items'] as $orderkey=>$product):?>
        	<?php
            $subtotal           += $product['price']*$product['quantity'];
            //$coupon_discount  += $product['total_coupon_discount'];
            if(!isset($product['coupon_discount'])){
                $product['coupon_discount'] = 0;
            }
            $coupon_discount    += $product['coupon_discount']*$product['quantity'];
            $shipping           += $product['shipping'];
            $total              += $product['total'] - ($product['coupon_discount']*$product['quantity']);
            ?>
        <tr>
            <td>
                <?php echo $product['name'];?>
                <?php echo (trim($product['sku']) != '')?'<br/><small>sku: '.$product['sku'].'</small>':'';?>
                
            </td>
            <td>
                <?php //echo $product['excerpt'];?>
                <?php
                
                // Print options
                if(isset($product['options']))
                {
                    foreach($product['options'] as $name=>$value)
                    {
                        $name = explode('-', $name);
                        $name = trim($name[0]);
                        if(is_array($value))
                        {
                            echo '<div>'.$name.':<br/>';
                            foreach($value as $item)
                            {
                                echo '- '.$item.'<br/>';
                            }   
                            echo "</div>";
                        }
                        else
                        {
                            echo '<div>'.$name.': '.$value.'</div>';
                        }
                    }
                }
                ?>
                <p>Seller: <a target="_blank" href="<?php echo site_url().'seller/info/'.$product['merchant_slug'];?>" class="linkSt"><?php echo $product['store_name'];?></a></p>
                <?php
                if(isset($product['gc_status'])) echo $product['gc_status'];
                ?>
            </td>
            <td><?php echo 'Rs '.$product['price'];?></td>
            <td><?php echo $product['quantity'];?></td>
            <td><?php echo 'Rs '.$product['price']*$product['quantity'];?></td>
        </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
        <tr>
            <td><strong>Subtotal</strong></td>
            <td colspan="3"></td>
            <td><?php echo 'Rs '.$subtotal; ?></td>
        </tr>
        <?php if($cart['coupon_discount'] > 0):?>
        <tr>
            <td><strong>Coupon Discount</strong></td>
            <td colspan="3"></td>
            <td><?php echo 'Rs '.(0-$coupon_discount); ?></td>
        </tr>
        <?php endif;?>
        
        <?php 
        $charges = @$cart['custom_charges'];
        if(!empty($charges))
        {
            foreach($charges as $name=>$price) : ?>
                
        <tr>
            <td><strong><?php echo $name?></strong></td>
            <td colspan="3"></td>
            <td><?php echo 'Rs '.$price; ?></td>
        </tr>   
                
        <?php endforeach;
        }
        ?>
        <tr>
            <td><strong>Shipping</strong></td>
            <td colspan="3"></td>
            <td><?php echo 'Rs '.$shipping; ?></td>
        </tr>
        
        <?php if($cart['gift_card_discount'] > 0):?>
        <tr>
            <td><strong>Giftcard Discount></strong></td>
            <td colspan="3"></td>
            <td><?php echo 'Rs '.(0-$cart['gift_card_discount']); ?></td>
        </tr>
        <?php endif;?>
        <tr>
            <td><h3>Total</h3></td>
            <td colspan="3"></td>
            <td><strong><?php echo 'Rs '.$cart['cart_total']; ?></strong></td>
        </tr>
    </tfoot>
</table>