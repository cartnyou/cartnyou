<?php

$lang['charge_on_delivery']	= 'Pay on Delivery (Cash)';
$lang['cod_disable']		= 'Pay On Delivery is Not Available on this Product';
$lang['processing_error']	= 'There was an error processing your payment';