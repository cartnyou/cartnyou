<?php echo theme_js('jquery.matchHeight-min.js', true);?>

<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Addresses</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

        	<div class="column col-xs-12 col-sm-12">
        		<?php if ($this->session->flashdata('message')):?>
		            <div class="alert alert-info">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('message');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if ($this->session->flashdata('error')):?>
		            <div class="alert alert-danger">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('error');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if(validation_errors()):?>
		        	<div class="alert alert-danger">
		        		<a class="close" data-dismiss="alert">×</a>
		        		<?php echo validation_errors();?>
		        	</div>
		        <?php endif;?>
        	</div>

            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li><span></span><a href="<?php echo site_url('secure/my-account');?>">Profile</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/orders');?>">Orders</a></li>
                                    <li class="active"><span></span><a href="<?php echo site_url('secure/addresses');?>">My Address</a></li>
                                    <li><span></span><a href="<?php echo site_url('cart/wishlist');?>">My Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- Banner silebar -->

                <!-- ./Banner silebar -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->

                <script>
		        	$(document).ready(function(){
		        		$('.delete_address').on('click', function(){
		        			if($('.delete_address').length > 1)
		        			{
		        				if(confirm('<?php echo lang('delete_address_confirmation');?>'))
		        				{
		        					$.post("<?php echo site_url('secure/delete_address');?>", { id: $(this).attr('rel') },
		        						function(data){
		        							$('#address_'+data).remove();
		        							$('.match-item').matchHeight();
		        						});
		        				}
		        			}
		        			else
		        			{
		        				alertify.alert('<?php echo lang('error_must_have_address');?>');
		        			}	
		        		});

		        		$('.edit_address').on('click', function(){
		        			$.post('<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
		        				function(data){
		        					$('#address-form-container').html(data);
		        					$('#address-form-modal').modal('show');
		        					//$('.basic').fancySelect();
		        				}
		        				);
		        		});
		        	});


		        	function set_default(address_id)
		        	{
		        		$.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:'bill'});
		        		$.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:'ship'});
		        		location.reload();
		        	}
		        </script>

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 mtb-5">
                        <div class="addAddr match-item">
                            <button class="btn btn-default btn-block addrBtn edit_address match-item" rel="0">
                                <i class="fa fa-plus lht"></i><br>Add Address
                            </button>
                        </div>
                    </div>

                    <?php
                    if(count($addresses) > 0):
                    	$c = 1;

                    	foreach($addresses as $a):?>
		                    <div class="col-md-4 col-sm-6 col-xs-12 mtb-5" id="address_<?php echo $a['id'];?>">
		                        <div class="addrCard match-item">
		                            <p class="text-right">
		                            	<span>
		                            		<?php if($customer['default_billing_address'] !== $a['id']){ ?>
		                            			<a href="javascript:;" onclick="set_default(<?php echo $a['id'] ?>)" class="btn btn-green btn-default-addres">Set Default</a>
		                            		<?php } ?>
			                            	<i class="fa fa-edit fag f19 colorGreen edit_address" rel="<?php echo $a['id'];?>"></i> 
			                            	<i class="fa fa-trash fag f19 delete_address" rel="<?php echo $a['id'];?>"></i>
		                            	</span>
		                            </p>
		                            <?php
									$b	= $a['field_data'];
									echo $b['firstname'].' '.$b['lastname'];
									echo '<p>'.$b['phone'].'</p>';
									echo '<p>'.nl2br(format_address($b,false,true)).'</p>';
									?>

		                        </div>
		                    </div>
		                <?php
		                endforeach;
		            endif;
		            ?>



                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<div class="modal fade" id="address-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content" id="address-form-container">

		</div>
	</div>
</div>

<script>
    $(function(){
    	$('.match-item').matchHeight();
        $('.vertical-menu-content').css('display','none');
    });
</script>