<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><a href="<?php echo site_url('secure/orders');?>">Order History</a></span>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Order #<?php echo $order->order_number;?></span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">

                <?php if ($this->session->flashdata('message')):?>
                    <div class="alert alert-info">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php endif;?>
                
                <?php if ($this->session->flashdata('error')):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif;?>
                
                <?php if(validation_errors()):?>
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert">×</a>
                        <?php echo validation_errors();?>
                    </div>
                <?php endif;?>

                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">Order #<?php echo $order->order_number;?></span>
                </h2>
                <!-- Content page -->
                <div class="row mt-12">
                    <div class="col-md-4 col-sm-6">
                        <table class="table">
                            <tr>
                                <td><strong>Order Date</strong></td>
                                <td class="text-left">
                                    <?php
                                    $d = format_date($order->ordered_on); 
                                    $d = explode(' ', $d);
                                    echo $d[1].' '.$d[0].', '.$d[3];
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Amount</strong></td>
                                <td class="text-left"><?php echo format_currency($order->total);?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6 col-md-offset-1 col-sm-6">
                        <h3><?php echo ucwords($order->ship_firstname.' '.$order->ship_lastname);?></h3>
                        <?php echo $order->ship_email;?><br/>
                        <?php echo $order->ship_phone;?>
                        <p>
                        <?php echo $order->ship_address1;?><br>
                        <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
                        <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
                        <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
                        <?php echo $order->ship_country;?><br/>
                        </p>

                    </div>
                </div>
                <div class="orderBox mt-12">
                    <div class="itemList">

                        <?php
                        if(!empty($order->contents)){
                            foreach ($order->contents as $item) {
                            ?>
                                <div class="itms mtb-6">
                                    <div class="row mt-12 pdlr-20">
                                        <div class="col-md-2">
                                            <div class="cart-product text-center">
                                                <a href="<?php echo site_url().$item['slug'];?>" target="_blank">
                                                    <?php
                                                    $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                                                    $item['images'] = array_values((array)json_decode($item['images']));
                                            
                                                    if(!empty($item['images'][0]))
                                                    {
                                                        $primary    = $item['images'][0];
                                                        foreach($item['images'] as $photo)
                                                        {
                                                            if(isset($photo->primary))
                                                            {
                                                                $primary    = $photo;
                                                            }
                                                        }

                                                        $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" />';
                                                    }
                                                    echo $photo;
                                                    ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="<?php echo site_url().$item['slug'];?>" target="_blank"><?php echo format_product_name($item['name']);?></a><br>
                                            <?php
                                            if(isset($item['options']))
                                            {
                                                foreach($item['options'] as $name=>$value)
                                                {
                                                    $name = explode('-', $name);
                                                    $name = trim($name[0]);
                                                    if(is_array($value))
                                                    {
                                                        echo '<p>'.$name.':<br/>';
                                                        foreach($value as $item)
                                                        {
                                                            echo '- '.$item.'<br/>';
                                                        }   
                                                        echo "</p>";
                                                    }
                                                    else
                                                    {
                                                        echo '<p>'.$name.': '.$value.'</p>';
                                                    }
                                                }
                                            }
                                            ?>
                                            <p>Seller: <a target="_blank" href="<?php echo site_url().'seller/info/'.$item['merchant_slug'];?>" class="linkSt"><?php echo $item['store_name'];?></a></p>
                                       </div>

                                       <div class="col-md-5">
                                           <div class="row mt2b3">
                                               <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                                                   <div id="wid-id-0">
                                                       <!-- widget div-->
                                                       <div role="content">
                                                           <!-- widget content -->
                                                           <div class="widget-body">
                                                                <?php if(!empty($item['latest_status'])){ ?>
                                                                    <?php
                                                                    $item_status = $item['latest_status']->ots_status;

                                                                    $active_number 		= 1;
                                                                    $status_1 			= 'Order Placed';
                                                                    //$status_4 			= 'Delivered';
                                                                    $status_5 			= 'Delivered';
                                                                    $active_extra_class	= '';

                                                                    /*if($item_status == 'Order Placed'){
                                                                        $active_number = 1;
                                                                        $status_1 = 'Order Placed';
                                                                    } else if($item_status == 'Pending'){
                                                                        $active_number = 1;
                                                                        $status_1 = 'Pending';
                                                                    } else if($item_status == 'Processing'){
                                                                        $active_number = 2;
                                                                    } else if($item_status == 'Pickup generated'){
                                                                        $active_number = 2;
                                                                    } else if($item_status == 'Shipped'){
                                                                        $active_number = 3;
                                                                    } else if($item_status == 'On Hold'){
                                                                        $active_number = 3;
                                                                        $status_4 = 'On Hold';
                                                                        $active_extra_class = 'orange';
                                                                    } else if($item_status == 'Cancelled'){
                                                                        $active_number = 4;
                                                                        $status_4 = 'Cancelled';
                                                                        $active_extra_class = 'red';
                                                                    } else if($item_status == 'Delivered'){
                                                                        $active_number = 4;
                                                                        $status_4 = 'Delivered';
                                                                        $active_extra_class = 'green';
                                                                    } else if($item_status == 'Return Requested'){
                                                                        $active_number = 4;
                                                                        $status_4 = 'Return Requested';
                                                                        $active_extra_class = 'orange';
                                                                    } else if($item_status == 'Reverse Pickup generated'){
                                                                        $active_number = 4;
                                                                        $status_4 = 'Reverse Pickup generated';
                                                                        $active_extra_class = 'orange';
                                                                    } else if($item_status == 'Returned'){
                                                                        $active_number = 4;
                                                                        $status_4 = 'Returned';
                                                                        $active_extra_class = 'orange';
                                                                    }*/
																	
																	//shahwaz
																	if($item_status == 'Order Placed'){
                                                                        $active_number = 1;
                                                                        $status_1 = 'Order Placed';
                                                                    } else if($item_status == 'Preparing to Ship'){
                                                                        $active_number = 1;
                                                                        $status_1 = 'Preparing to Ship';
                                                                    }  else if($item_status == 'Dispatched'){
                                                                        $active_number = 2;
                                                                    } else if($item_status == 'In Transit'){
                                                                        $active_number = 3;
                                                                    } else if($item_status == 'Out for Delivery'){
                                                                        $active_number = 4;
                                                                    } else if($item_status == 'Cancelled'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Cancelled';
                                                                        $active_extra_class = 'red';
                                                                    } else if($item_status == 'Customer Request Cancelled'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Cancelled';
                                                                        $active_extra_class = 'red';
                                                                    } else if($item_status == 'Cancelled by Seller'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Cancelled';
                                                                        $active_extra_class = 'red';
                                                                    } else if($item_status == 'RTO'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Undelivered';
                                                                        $active_extra_class = 'orange';
                                                                    } else if($item_status == 'Delivered'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Delivered';
                                                                        $active_extra_class = 'green';
                                                                    }else if($item_status == 'Return Requested'){
                                                                        $active_number = 5;
                                                                        $status_5 = 'Return Requested';
                                                                        $active_extra_class = 'orange';
                                                                    } 
																	//ends here
                                                                    ?>
																	
																	

                                                                    <!--<div class="row">
                                                                        <div id="bootstrap-wizard-1" class="col-sm-12">
                                                                            <div class="form-bootstrapWizard">
                                                                                <ul class="bootstrapWizard form-wizard">
                                                                                    <li class="<?php //if($active_number >= 1){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">1</span> <span class="title"><?php //echo $status_1;?></span>
                                                                                    </li>
                                                                                    <li class="<?php //if($active_number >= 2){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">2</span> <span class="title">Processing</span>
                                                                                    </li>
                                                                                    <li class="<?php //if($active_number >= 3){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">3</span> <span class="title">Shipped</span>
                                                                                       <?php
                                                                                       /*if($active_number == 3){
                                                                                            if($item['waybill_method'] == 'Delhivery'){
                                                                                                echo '<span class="track-description"><a href="javascript:;" class="cursor ot-track-updates-open" data-ot-id="'.$item['order_item_id'].'">Track</a></span><br>';
                                                                                                //echo '<span class="track-description"><a class="cursor" target="_blank" href="http://www.delhivery.com/track/package/'.$item['waybill'].'">Alternative Tracking</a></span>';
                                                                                            } else {
                                                                                                echo '<span class="track-description">'.$item['waybill'].'<br>('.$item['waybill_method'].')</span>';
                                                                                            }
                                                                                       }*/
                                                                                       ?>
                                                                                    </li>
                                                                                    <li class="<?php //if($active_number >= 4){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">4</span> <span class="title"><?php //echo $status_4;?></span>
                                                                                    </li>
                                                                                </ul>
                                                                                <div class="clearfix"></div>
                                                                            </div>

                                                                        </div>
                                                                    </div>-->
																	
																	<!-- shahwaz -->
																	<div class="row">
                                                                        <div id="bootstrap-wizard-1" class="col-sm-12">
                                                                            <div class="form-bootstrapWizard">
                                                                                <ul class="bootstrapWizard form-wizard">
                                                                                    <li class="<?php if($active_number >= 1){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">1</span> <span class="title"><?php echo $status_1;?></span>
                                                                                    </li>
                                                                                    <li class="<?php if($active_number >= 2){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">2</span> <span class="title">Dispatched</span>
																					   <?php
                                                                                        if($active_number == 2){
                                                                                            if($item['waybill_method'] == 'Delhivery'){
                                                                                                echo '<span class="track-description"><a href="javascript:;" class="cursor ot-track-updates-open" data-ot-id="'.$item['order_item_id'].'">Track</a></span><br>';
                                                                                                //echo '<span class="track-description"><a class="cursor" target="_blank" href="http://www.delhivery.com/track/package/'.$item['waybill'].'">Alternative Tracking</a></span>';
                                                                                            } else {
                                                                                                echo '<span class="track-description">'.$item['waybill'].'<br>('.$item['waybill_method'].')</span>';
                                                                                            }
                                                                                        }
                                                                                       ?>
                                                                                    </li>
                                                                                    <li class="<?php if($active_number >= 3){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">3</span> <span class="title">In Transit</span>
                                                                                       <?php
                                                                                        if($active_number == 3){
                                                                                            if($item['waybill_method'] == 'Delhivery'){
                                                                                                echo '<span class="track-description"><a href="javascript:;" class="cursor ot-track-updates-open" data-ot-id="'.$item['order_item_id'].'">Track</a></span><br>';
                                                                                                //echo '<span class="track-description"><a class="cursor" target="_blank" href="http://www.delhivery.com/track/package/'.$item['waybill'].'">Alternative Tracking</a></span>';
                                                                                            } else {
                                                                                                echo '<span class="track-description">'.$item['waybill'].'<br>('.$item['waybill_method'].')</span>';
                                                                                            }
                                                                                        }
                                                                                       ?>
                                                                                    </li>
																					<li class="<?php if($active_number >= 4){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">4</span> <span class="title">Out for Delivery</span>
																					   <?php
                                                                                        if($active_number == 4){
                                                                                            if($item['waybill_method'] == 'Delhivery'){
                                                                                                echo '<span class="track-description"><a href="javascript:;" class="cursor ot-track-updates-open" data-ot-id="'.$item['order_item_id'].'">Track</a></span><br>';
                                                                                                //echo '<span class="track-description"><a class="cursor" target="_blank" href="http://www.delhivery.com/track/package/'.$item['waybill'].'">Alternative Tracking</a></span>';
                                                                                            } else {
                                                                                                echo '<span class="track-description">'.$item['waybill'].'<br>('.$item['waybill_method'].')</span>';
                                                                                            }
                                                                                        }
                                                                                       ?>
                                                                                    </li>
                                                                                    <li class="<?php if($active_number >= 5){ echo 'active '.$active_extra_class;}?>">
                                                                                       <span class="step">5</span> <span class="title"><?php echo $status_5;?></span>
                                                                                    </li>
                                                                                </ul>
                                                                                <div class="clearfix"></div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
																	<!-- ends here -->
                                                                <?php } ?>

                                                           </div>
                                                           <!-- end widget content -->

                                                       </div>
                                                       <!-- end widget div -->

                                                   </div>
                                                   <!-- end widget -->

                                               </article>
                                           </div>
                                       </div>
                                       <div class="col-md-2 text-center">
                                            <p><?php echo format_currency($item['price']);?></p>
                                            <?php
                                            if($item['quantity'] > 1){
                                                echo '<p><b>Quantity:</b>'.$item['quantity'].'</p>';
                                            }
                                            ?>
                                            </p>
                                            <div class="text-center mt5">
                                                <?php
                                                if($item_status == 'Delivered'){
                                                	echo '<a href="'.site_url().'secure/invoice/'.$order->id.'/'.$item['order_item_id'].'" target="_blank" class="btn btn-red">Print Invoice</a><br><br>';
                                                    echo '<a href="javascript:;" class="btn-layout-2 return-modal-open" data-ot-id="'.$item['order_item_id'].'" class="btn-layout-2" >Return/Refund</a>';
                                                //} elseif($item_status != 'Cancelled') {
                                                } elseif($active_number < 2) {
                                                    echo '<a href="javascript:;" class="btn btn-red mt5 order-item-cancel" data-ot-id="'.$item['order_item_id'].'">Cancel</a>';
                                                }
                                                ?>
                                            </div>
                                            <div class="text-center mt5">
	                                            <a href="javascript:;" class="ot-status-updates-open" data-ot-id="<?php echo $item['order_item_id'];?>"><u>Order Status >></u></a>
	                                        </div>
                                       </div>
                                   </div>
                               </div>

                       <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="row mt-12 pdlr-20">
                        <hr>
                        <div class="col-xs-12">
                            <p class="text-right">Subtotal: <?php echo format_currency($order->subtotal);?></p>
                        </div>

                        <?php if($order->coupon_discount > 0){ ?>
                        <div class="col-xs-12">
                            <p class="text-right">Discount: -<?php echo format_currency($order->coupon_discount);?></p>
                        </div>
                        <?php } ?>

                        <?php if($order->subtotal > 0 || $order->shipping > 0){ ?>
                        <div class="col-xs-12">
                            <p class="text-right">Shipping: <?php echo format_currency($order->shipping);?></p>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="row mt-12 pdlr-20 orderFooter">
                        <div class="col-xs-12">
                            <p class="text-right"><strong>Order Total</strong>: <?php echo format_currency($order->total);?></p>
                        </div>
                    </div>
                </div>

                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<div class="modal fade" id="refundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabe22">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel22">Return/Refund</h4>
            </div>
            <div class="modal-body">
                <form class="" id="refund-form">
                    <input type="hidden" name="order_item_id" id="return_ot_id">
                    <div class="form-group">
                        <label>Why are you returning this? <span class="red">*</span></label>
                        <select class="form-control" name="return_reason">
                            <option value="">--Select--</option>
                            <option value="Bought By mistake">Bought By mistake</option>
                            <option value="Performance or Quality not adequate">Performance or Quality not adequate</option>
                            <option value="Product damaged but shipping box OK">Product damaged but shipping box OK</option>
                            <option value="Missing parts or accessories">Missing parts or accessories</option>
                            <option value="Both product and shipping box damaged">Both product and shipping box damaged</option>
                            <option value="Wrong item was sent">Wrong item was sent</option>
                            <option value="Item defective or does not work">Item defective or does not work</option>
                            <option value="Received extra item I did not buy">Received extra item I did not buy</option>
                            <option value="No longer needed">No longer needed</option>
                            <option value="Didn't approve purchase">Didn't approve purchase</option>
                            <option value="Inaccurate website description">Inaccurate website description</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Image (Upload Product image that you returning) <span class="red">*</span></label>
                        <input type="file" name="return_image">
                    </div>
                    <div class="form-group">
                        <label>Video(optional - Unboxing Video)</label>
                        <input type="file" name="return_video">
                    </div>
                    <div class="form-group">
                        <label>Comments(optional)</label>
                        <textarea cols="30" rows="3" class="form-control" name="return_description"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <div class="progress" id="return_progress" style="display: none;">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                0%
                            </div>
                        </div>
                        <button class="btn btn-red" type="submit" id="return_submit">Submit</button>
                    </div>

                    <!--<div class="form-group">
                        <p class="or orCss"><span>OR</span></p>
                    </div>-->

                </form>

            </div>

        </div>
    </div>
</div>

<div id="status-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Status Updates</h4>
      </div>
      <div class="modal-body">
       <div id="previous_statuses">
        </div>
      </div>
    </div>
  </div>
</div>

<div id="live-status-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tracking Data</h4>
      </div>
      <div class="modal-body">
       <div id="live_previous_statuses">
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(function(){
    $('.vertical-menu-content').css('display','none');

    $('.ot-track-updates-open').on("click", function(e){
        e.preventDefault();
        var order_item_id = $(this).attr('data-ot-id');
        $('#live_previous_statuses').html('<center><img src="<?php echo theme_img('Preloader_4.gif');?>"><br>Fetching Data...</center>');

        if(order_item_id){
            $.ajax({
                url : '<?php echo site_url();?>'+'secure/getOrderItemLiveStatuses/'+order_item_id,
                method: 'GET'
            }).done(function(getdata){
                getdata = JSON.parse(getdata);

                var append_per = '';
                if(getdata.status){

                    append_per += '<b>'+getdata.data.status.status+' - '+getdata.data.status.timestamp+'<br><em>'+getdata.data.status.instructions+'</em></b>';

                    if(getdata.data.hasOwnProperty('scans')){
                        $.each(getdata.data.scans, function(row,i){
                            append_per += '<hr><b>'+i.timestamp+': </b><em>'+i.location+'</em><br>'+i.instructions;
                        });
                    }
                    $('#live_previous_statuses').html(append_per);

                } else {
                    $('#live_previous_statuses').html('Data not available yet. Please try after sometime.');
                }
            });
            $('#live-status-modal').modal('show');
        }
    });

    $('.ot-status-updates-open').on("click", function(e){
        e.preventDefault();
        var order_item_id = $(this).attr('data-ot-id');
        $('#previous_statuses').html('');

        if(order_item_id){
            $.ajax({
                url : '<?php echo site_url();?>'+'secure/get_order_item_statuses/'+order_item_id,
                method: 'GET'
            }).done(function(getdata){
                getdata = JSON.parse(getdata);
                var append_per = '';
                $.each(getdata, function(row,i){
					var replace={ "RTO": "Undelivered", "Customer Request Cancelled": "Cancelled", "Cancelled by Seller": "Cancelled" };
					var d = new Date(i.created_at);
                    append_per = '<b>'+(replace.hasOwnProperty(i.ots_status)?replace[i.ots_status]:i.ots_status)+': </b><em>'+d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+','+d.toLocaleTimeString()+'</em><br>'+i.notes;
                    if(i.data){
                        di = JSON.parse(i.data);
                        append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Image: <a target="_blank" href="<?php echo site_url();?>uploads/return/'+di.image+'"><u>'+di.image+'</u></a>';
                        if(di.video){
                            append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Video: <a target="_blank" href="<?php echo site_url();?>uploads/return/'+di.video+'"><u>'+di.video+'</u></a>';
                        }
                        if(di.comments){
                            append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Comments: '+di.comments;
                        }
                    }
                    $('#previous_statuses').append(append_per+'<hr>');
                });
            });
            $('#status-modal').modal('show');
        }
    });

    $('.order-item-cancel').on("click", function(e){
        e.preventDefault();
        var order_item_id = $(this).attr('data-ot-id');
        $(this).html('Working...');

        $.ajax({
            data : 'order_item_id='+order_item_id,
            url : '<?php echo site_url()."secure/cancel_order";?>',
            method: 'POST'
        }).done(function(getdata){
            getdata = JSON.parse(getdata);
            if (getdata.result) {
                location.reload();
            } else {
                $(this).html('Cancel');
                alertify.alert(getdata.error);
            }
        });
    });

    $('.return-modal-open').on("click", function(e){
        e.preventDefault();
        var order_item_id = $(this).attr('data-ot-id');

        $('#return_ot_id').val(order_item_id);
        $('#refundModal').modal('show');
    });

    $('#refund-form').on('submit', function(ev){
        ev.preventDefault();
        var formData = new FormData($('#refund-form')[0]);
        $.ajax({
            url: '<?php echo site_url();?>secure/return_order/<?php echo $order->id;?>',
            type: 'POST',
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                $('#return_submit').hide();
                $('#return_progress').show();
                if(myXhr.upload){
                    //myXhr.upload.addEventListener('progress',progressHandlerFunction, false);
                    var pvalue = 0;
                    myXhr.upload.addEventListener('progress', function(e) {
                        var done = e.position || e.loaded
                        var total = e.totalSize || e.total;
                        pvalue = Math.round(done/total*100);
                        $('.progress-bar').css('width', pvalue+'%').attr('aria-valuenow', pvalue).html(pvalue+'%'); 
                    });
                    myXhr.upload.addEventListener('load', function(e) {
                        //alertify();
                        setTimeout(function() {
                            var resp = JSON.parse(myXhr.responseText);
                            if(resp.result){
                                //alertify.alert(resp.data);
                                location.reload();
                            } else {
                                alertify.alert(resp.data);
                            }
                            $('#return_submit').show();
                            $('#return_progress').hide();
                        }, 1000);
                    });
                }
                return myXhr;
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        });
    });
});
</script>