<?php

class Orders extends Merchant_Controller {	

	function __construct()
	{		
		parent::__construct();

		$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->helper(array('formatting'));
		$this->lang->load('order');
	}
	
	function index($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export')
		{
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;

			$data['orders']	= $this->Order_model->get_orders($term);		

			foreach($data['orders'] as &$o)
			{
				$o->items	= $this->Order_model->get_items($o->id);
			}

			force_download_content('orders.xml', $this->load->view($this->config->item('merchant_folder').'/orders_xml', $data, true));
			
			//kill the script from here
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= lang('orders');
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_orders($term, $sort_by, $sort_order, $rows, $page, $merchant_id);
		$data['total']	= $this->Order_model->get_orders_count($term, $merchant_id);
		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('merchant_folder').'/orders/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
				
		$this->view($this->config->item('merchant_folder').'/orders', $data);
	}

	function pending($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export')
		{
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;

			$data['orders']	= $this->Order_model->get_pending_orders($term);		

			foreach($data['orders'] as &$o)
			{
				$o->items	= $this->Order_model->get_items($o->id);
			}

			force_download_content('orders.xml', $this->load->view($this->config->item('merchant_folder').'/orders_xml', $data, true));
			
			//kill the script from here
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= 'Pending Orders';
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_pending_orders($term, $sort_by, $sort_order, $rows, $page, $merchant_id);
		$data['total']	= $this->Order_model->get_pending_orders(false,'','DESC',0,0,$merchant_id,true);
		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('merchant_folder').'/orders/pending/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
				
		$this->view($this->config->item('merchant_folder').'/orders', $data);
	}
	
	function export()
	{
		$this->load->model('customer_model');
		$this->load->helper('download_helper');
		$post	= $this->input->post(null, false);
		$term	= (object)$post;
		
		$data['orders']	= $this->Order_model->get_orders($term);		

		foreach($data['orders'] as &$o)
		{
			$o->items	= $this->Order_model->get_items($o->id);
		}

		force_download_content('orders.xml', $this->load->view($this->config->item('merchant_folder').'/orders_xml', $data, true));
		
	}
	
	function order($id)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];

		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		$this->load->model('Gift_card_model');
			
		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
	
		$message	= $this->session->flashdata('message');
		
	
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$orderInfo = $this->Order_model->get_order($id,false,$merchant_id);
		$data['page_title']	= lang('view_order').' # '. $orderInfo->order_number;
		if($orderInfo){
			$data['order']		= $orderInfo;
		} else {
			redirect(site_url('merchant/orders'));
		}

		/*****************************
		* Order Notification details *
		******************************/
		// get the list of canned messages (order)
		$this->load->model('Messages_model');
    	$msg_templates = $this->Messages_model->get_list('order');
		
		// replace template variables
    	foreach($msg_templates as $msg)
    	{
 			// fix html
 			$msg['content'] = str_replace("\n", '', html_entity_decode($msg['content']));
 			
 			// {order_number}
 			$msg['subject'] = str_replace('{order_number}', $data['order']->order_number, $msg['subject']);
			$msg['content'] = str_replace('{order_number}', $data['order']->order_number, $msg['content']);
    		
    		// {url}
			$msg['subject'] = str_replace('{url}', $this->config->item('base_url'), $msg['subject']);
			$msg['content'] = str_replace('{url}', $this->config->item('base_url'), $msg['content']);
			
			// {site_name}
			$msg['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['subject']);
			$msg['content'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['content']);
			
			$data['msg_templates'][]	= $msg;
    	}

		// we need to see if any items are gift cards, so we can generate an activation link
		foreach($data['order']->contents as $orderkey=>$product)
		{
			if(isset($product['is_gc']) && (bool)$product['is_gc'])
			{
				if($this->Gift_card_model->is_active($product['sku']))
				{
					$data['order']->contents[$orderkey]['gc_status'] = '[ '.lang('giftcard_is_active').' ]';
				} else {
					$data['order']->contents[$orderkey]['gc_status'] = ' [ <a href="'. base_url() . $this->config->item('merchant_folder').'/giftcards/activate/'. $product['code'].'">'.lang('activate').'</a> ]';
				}
			}
		}
		
		$this->view($this->config->item('merchant_folder').'/order', $data);
		
	}

	function status_update($id)
	{
		$data = array();
		$this->auth->is_merchant_logged_in();
		$merchant_id = $this->session->userdata('merchant')['id'];

		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		$this->load->model('Gift_card_model');
			
		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
	
		$message	= $this->session->flashdata('message');
		
		$sendMail = false;
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['order_item_id']	= $this->input->post('item_id');
			$save['ots_status']		= $this->input->post('status');
			$save['notes']			= $this->input->post('notes');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->status_update($save);
			
			if($save['ots_status'] == 'Shipped' || $save['ots_status'] == 'Cancelled' || $save['ots_status'] == 'Delivered'){
	            $sendMail = true;
	        }
		}

		$this->load->model('Messages_model');

		//get the order information, this way if something was posted before the new one gets queried here
		$orderInfo = $this->Order_model->get_order($id,false,$merchant_id);
		$data['page_title']	= lang('view_order').' # '. $orderInfo->order_number;
		if($orderInfo){
			$data['order']		= $orderInfo;
		} else {
			redirect(site_url('merchant/orders'));
		}

		if($sendMail){
			$this->load->model('Generalmodel');

			$products_list = array();
			foreach($orderInfo->contents as $product):
				$products_list[] = ucwords(strtolower($this->Generalmodel->shorten_string($product['name'], 4, false)));
			endforeach;

			$trackingString = '';
			if($orderInfo->contents[0]['waybill_method'] == 'Delhivery'){
				$tracking_url = 'http://www.delhivery.com/track/package/'.$orderInfo->contents[0]['waybill'];
				$trackingShortUrl = $this->Generalmodel->url_shortener($tracking_url);
				if(!$trackingShortUrl){
					$trackingShortUrl = $tracking_url;
				}
				$trackingString = ' Track at '.$trackingShortUrl;
			}
			

			$order_page_url = site_url().'secure/order/'.$orderInfo->order_number;
			$orderPageShortUrl = $this->Generalmodel->url_shortener($order_page_url);
			if(!$orderPageShortUrl){
				$orderPageShortUrl = $order_page_url;
			}

			if($save['ots_status'] == 'Shipped'){
	            $row = $this->Messages_model->get_message(3);
	            $row['content'] = $this->load->view('order_shipped_email', $data['order'], true);
	            $sms_content 	= 'Your order number '.$orderInfo->order_number.' placed with Gojojo has been shipped via '.$orderInfo->contents[0]['waybill_method'].' againt tracking ID '.$orderInfo->contents[0]['waybill'].'.'.$trackingString.' For more info contact us at https://goo.gl/Q7L3jv';
	        } else if($save['ots_status'] == 'Cancelled'){
	            $row = $this->Messages_model->get_message(9);
	            $row['content'] = $this->load->view('order_cancelled_email', $data['order'], true);
	            $sms_content 	= 'Your package '.implode(', ', $products_list).' ref to order '.$orderInfo->order_number.' is cancelled if you have already paid, refund will be initiated shortly. Details: '.$orderPageShortUrl;
	        } else if($save['ots_status'] == 'Delivered'){
	            $row = $this->Messages_model->get_message(10);
	            $row['content'] = $this->load->view('order_delivered_email', $data['order'], true);
	            $sms_content 	= 'Your package '.implode(', ', $products_list).' ref to order '.$orderInfo->order_number.' was successfully delivered. More info at '.$orderPageShortUrl;
	        	
	        	// review email
	            /*$rowReview = $this->Messages_model->get_message(11);
	            $rowReview['content'] = $this->load->view('order_review', $data['order'], true);

	            $rowReview['subject'] = str_replace('{customer_name}', $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname, $rowReview['subject']);
				$rowReview['subject'] = str_replace('{url}', $this->config->item('base_url'), $rowReview['subject']);
				$rowReview['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $rowReview['subject']);
				$rowReview['subject'] = str_replace('{order_number}', $orderInfo->order_number, $rowReview['subject']);
				
				$this->load->library('email');
				
				$config = Array(
		        	'protocol'  => 'smtp',
		        	'smtp_host' => 'ssl://smtp.zoho.com',
		        	'smtp_port' => '465',
		        	'smtp_user' => 'auto-order-confirmation@gojojo.in',
		        	'smtp_pass' => 'Gojojo123#',
		        	'mailtype'  => 'html',
				    'newline'   => "\r\n"
		        );
				$this->email->initialize($config);
				$this->email->from('auto-order-confirmation@gojojo.in', $this->config->item('company_name'));
				$this->email->to($orderInfo->ship_email);
				$this->email->subject($rowReview['subject']);
				$this->email->message($rowReview['content']);
				$this->email->send();*/

				// ./ review email
	        }

			$row['subject'] = str_replace('{customer_name}', $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname, $row['subject']);
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
			$row['subject'] = str_replace('{order_number}', $orderInfo->order_number, $row['subject']);
			
			$this->load->library('email');
			
			$config = Array(
	        	'protocol'  => 'smtp',
	        	'smtp_host' => 'ssl://smtp.zoho.com',
	        	'smtp_port' => '465',
	        	'smtp_user' => 'brijesh@inikworld.com',
	        	'smtp_pass' => 'Brijesh123#',
	        	'mailtype'  => 'html',
			    'newline'   => "\r\n"
	        );
			$this->email->initialize($config);

			$this->email->from('brijesh@inikworld.com', $this->config->item('company_name'));
			
			$this->email->to($orderInfo->ship_email);
			
			//email the admin
			$this->email->bcc('auto-order-confirmation@gojojo.in');
			
			$this->email->subject($row['subject']);
			$this->email->message($row['content']);
			
			$this->email->send();

			//send sms to user
			$this->Generalmodel->send_sms($orderInfo->ship_phone, $sms_content);
		}

		// get the list of canned messages (order)
		
		$msg_templates = $this->Messages_model->get_list('order');
		
		// replace template variables
    	foreach($msg_templates as $msg)
    	{
 			// fix html
 			$msg['content'] = str_replace("\n", '', html_entity_decode($msg['content']));
 			
 			// {order_number}
 			$msg['subject'] = str_replace('{order_number}', $data['order']->order_number, $msg['subject']);
			$msg['content'] = str_replace('{order_number}', $data['order']->order_number, $msg['content']);
    		
    		// {url}
			$msg['subject'] = str_replace('{url}', $this->config->item('base_url'), $msg['subject']);
			$msg['content'] = str_replace('{url}', $this->config->item('base_url'), $msg['content']);
			
			// {site_name}
			$msg['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['subject']);
			$msg['content'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['content']);
			
			$data['msg_templates'][]	= $msg;
    	}

		// we need to see if any items are gift cards, so we can generate an activation link
		foreach($data['order']->contents as $orderkey=>$product)
		{
			if(isset($product['is_gc']) && (bool)$product['is_gc'])
			{
				if($this->Gift_card_model->is_active($product['sku']))
				{
					$data['order']->contents[$orderkey]['gc_status'] = '[ '.lang('giftcard_is_active').' ]';
				} else {
					$data['order']->contents[$orderkey]['gc_status'] = ' [ <a href="'. base_url() . $this->config->item('merchant_folder').'/giftcards/activate/'. $product['code'].'">'.lang('activate').'</a> ]';
				}
			}
		}
		
		
		$this->view($this->config->item('merchant_folder').'/order', $data);
		
	}

	function get_order_item_statuses($order_item_id, $latest=false){
		$this->auth->is_merchant_logged_in();
		die(json_encode($this->Order_model->get_order_item_statuses($order_item_id, $latest)));
	}

	function packing_slip($order_id){
		$this->load->helper('date');

    	$merchant_id 	= $this->session->userdata('merchant')['id'];
		$data['order'] 	= $this->Order_model->get_package_ready_order_item($order_id,false,$merchant_id);
		
		$this->load->view($this->config->item('merchant_folder').'/packing_slip.php', $data);
	}
	
	/*
	function packing_slip($waybill){
		$this->load->model('Generalmodel');

		$data 			= array();
		$data['wbns'] 	= $waybill;
		$response 		= $this->Generalmodel->hit_delhivery('get','p/packing_slip',$data);
		$return 		= json_decode($response);
		
		if($return->packages_found > 0){
			$send['data'] = $return->packages[0];
			$this->load->view($this->config->item('merchant_folder').'/packing_slip.php', $send);
		} else {
			$this->session->set_flashdata('error', 'Invalid Waybill Number.');
			redirect(site_url('merchant/orders'));
		}
	}
	*/

	function packing_slip_order($order_id)
	{
		$this->load->helper('date');
		$data['order']		= $this->Order_model->get_order($order_id);
		
		$this->load->view($this->config->item('merchant_folder').'/packing_slip_order.php', $data);
	}
	
	function edit_status()
    {
    	$this->auth->is_merchant_logged_in();
    	$order['id']		= $this->input->post('id');
    	$order['status']	= $this->input->post('status');
    	
    	$this->Order_model->save_order($order);
    	
    	echo url_title($order['status']);
    }
    
    function send_notification($order_id='')
    {
			// send the message
   		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);

		$this->email->from($this->config->item('email'), $this->config->item('company_name'));
		$this->email->to($this->input->post('recipient'));
		
		$this->email->subject($this->input->post('subject'));
		$this->email->message(html_entity_decode($this->input->post('content')));
		
		$this->email->send();
		
		$this->session->set_flashdata('message', lang('sent_notification_message'));
		redirect($this->config->item('merchant_folder').'/orders/order/'.$order_id);
	}
	
	function bulk_action(){
		$merchant_id 	= $this->session->userdata('merchant')['id'];
		$orders			= $this->input->post('order');

    	if($orders){
			if($this->input->post('submit') == 'delete'){
				foreach($orders as $order){
		   			$this->Order_model->delete($order);
		   		}
				$this->session->set_flashdata('message', lang('message_orders_deleted'));
				redirect($this->config->item('merchant_folder').'/orders');	
			}

			// generate html manifest
			if($this->input->post('submit') == 'manifest'){
				$this->load->model('Generalmodel');
				
				$data = array();
				$i 	  = 0;
				foreach($orders as $order){
					$waybillInfo 		= $this->Order_model->get_waybill_number_with_method($order,$merchant_id);

					if(!empty($waybillInfo)){

						if($waybillInfo['waybill_method'] == 'Delhivery'){

				   			$response 		= $this->Generalmodel->hit_delhivery('get','p/packing_slip',array('wbns'=>$waybillInfo['waybill']));
							$return 		= json_decode($response);
							
							if($return->packages_found > 0){
								$package = $return->packages[0];

								$data['orders'][$i]['waybill'] 			= $waybillInfo['waybill'];
								$data['orders'][$i]['order_number'] 	= $package->oid;
								$data['orders'][$i]['invoice_number']	= $package->si;
								$data['orders'][$i]['seller_name']		= $package->snm;
								$data['orders'][$i]['payment_mode'] 	= $package->pt.'<br>('.$package->rs.')';
								$data['orders'][$i]['attention'] 		= $package->name.'<br>('.$package->contact.')';
								$data['orders'][$i]['city'] 			= $package->address.'<br>'.$package->destination_city.'<br>'.$package->pin;
								$data['orders'][$i]['contents'] 		= $package->prd;
								$data['orders'][$i]['weight'] 			= $package->weight;
								$data['orders'][$i]['barcode'] 			= $package->barcode;

								$i++;
							}
						}
					}
		   		}

				$this->load->view($this->config->item('merchant_folder').'/manifest.php', $data);
			}

			// generate excel manifest
			if($this->input->post('submit') == 'manifest_excel'){
				$this->load->model('Generalmodel');
				$this->load->library('excel');
				$this->excel->setActiveSheetIndex(0);
				$this->excel->getActiveSheet()->setTitle('Delivery Manifest');

				//set excel headers
				$this->excel->getActiveSheet()->setCellValue('A1', 'Waybill');
				$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('B1', 'order no');
				$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('C1', 'Consignee Name');
				$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('D1', 'city');
				$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('E1', 'state');
				$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('F1', 'country');
				$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('G1', 'Address');
				$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('H1', 'Pincode');
				$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('I1', 'phone');
				$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('J1', 'mobile');
				$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('K1', 'Weight (gms)');
				$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('L1', 'payment mode');
				$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('M1', 'package amount');
				$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('N1', 'cod amount (To Be collected )');
				$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('O1', 'product to be shipped');
				$this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('P1', 'Shipping Client');
				$this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('Q1', 'seller name');
				$this->excel->getActiveSheet()->getStyle('Q1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('R1', 'seller address');
				$this->excel->getActiveSheet()->getStyle('R1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('S1', 'seller cst no');
				$this->excel->getActiveSheet()->getStyle('S1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('T1', 'seller tin');
				$this->excel->getActiveSheet()->getStyle('T1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('U1', 'invoiceno');
				$this->excel->getActiveSheet()->getStyle('U1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('V1', 'invoicedate');
				$this->excel->getActiveSheet()->getStyle('V1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('W1', 'LENGTH');
				$this->excel->getActiveSheet()->getStyle('W1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('X1', 'Breadth');
				$this->excel->getActiveSheet()->getStyle('X1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('Y1', 'Height');
				$this->excel->getActiveSheet()->getStyle('Y1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('Z1', 'return address');
				$this->excel->getActiveSheet()->getStyle('Z1')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('AA1', 'return pin');
				$this->excel->getActiveSheet()->getStyle('AA1')->getFont()->setBold(true);

				$rowNum = 2;
				foreach($orders as $order){
					$waybill 		= $this->Order_model->get_waybill_number($order,$merchant_id);

					if($waybill != ''){
			   			$response 		= $this->Generalmodel->hit_delhivery('get','p/packing_slip',array('wbns'=>$waybill));
						$return 		= json_decode($response);
						
						if($return->packages_found > 0){
							$package = $return->packages[0];

				            $this->excel->getActiveSheet()->setCellValue('A'.$rowNum, $waybill);
				            $this->excel->getActiveSheet()->setCellValue('B'.$rowNum, $package->oid);
				            $this->excel->getActiveSheet()->setCellValue('C'.$rowNum, $package->name);
				            $this->excel->getActiveSheet()->setCellValue('D'.$rowNum, $package->destination_city);
				            $this->excel->getActiveSheet()->setCellValue('E'.$rowNum, ''); //
				            $this->excel->getActiveSheet()->setCellValue('F'.$rowNum, 'India'); //
				            $this->excel->getActiveSheet()->setCellValue('G'.$rowNum, $package->address);
				            $this->excel->getActiveSheet()->setCellValue('H'.$rowNum, $package->pin);
				            $this->excel->getActiveSheet()->setCellValue('I'.$rowNum, '');
				            $this->excel->getActiveSheet()->setCellValue('J'.$rowNum, $package->contact);
				            $this->excel->getActiveSheet()->setCellValue('K'.$rowNum, $package->weight);
				            $this->excel->getActiveSheet()->setCellValue('L'.$rowNum, $package->pt);
				            $this->excel->getActiveSheet()->setCellValue('M'.$rowNum, $package->rs);
				            $this->excel->getActiveSheet()->setCellValue('N'.$rowNum, $package->cod);
				            $this->excel->getActiveSheet()->setCellValue('O'.$rowNum, $package->prd);
				            $this->excel->getActiveSheet()->setCellValue('P'.$rowNum, $package->cl);
				            $this->excel->getActiveSheet()->setCellValue('Q'.$rowNum, $package->snm);
				            $this->excel->getActiveSheet()->setCellValue('R'.$rowNum, $package->sadd);
				            $this->excel->getActiveSheet()->setCellValue('S'.$rowNum, $package->cst);
				            $this->excel->getActiveSheet()->setCellValue('T'.$rowNum, $package->tin);
				            $this->excel->getActiveSheet()->setCellValue('U'.$rowNum, $package->si);
				            $this->excel->getActiveSheet()->setCellValue('V'.$rowNum, str_replace('T',' ', substr($package->sid, 0, 19)) );
				            $this->excel->getActiveSheet()->setCellValue('W'.$rowNum, '');
				            $this->excel->getActiveSheet()->setCellValue('X'.$rowNum, '');
				            $this->excel->getActiveSheet()->setCellValue('Y'.$rowNum, '');
				            $this->excel->getActiveSheet()->setCellValue('Z'.$rowNum, $package->rph[0].', '.$package->radd.', '.$package->rcty.', '.$package->rst);
				            $this->excel->getActiveSheet()->setCellValue('AA'.$rowNum, $package->rpin);
				            $rowNum++;
						}
					}
		   		}
		   		$filename = 'Delivery Manifest - '.date('Y-m-d H:i:s').'.xls';
	            header('Content-Type: application/vnd.ms-excel');
	            header('Content-Disposition: attachment;filename="'.$filename.'"');
	            header('Cache-Control: max-age=0');

	            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	            $objWriter->save('php://output');
		   		//exit;

		   		redirect($this->config->item('merchant_folder').'/orders');	
			}

		} else {
			$this->session->set_flashdata('error', 'You did not selected any order.');
			redirect($this->config->item('merchant_folder').'/orders');	
		}
    }

    function generate_pickup(){
    	$order_id 		= $this->input->post('order_id');
    	$method 		= $this->input->post('method');
    	$company 		= $this->input->post('company');
    	$awb 			= $this->input->post('awb');

    	if(!$method){
    		$this->session->set_flashdata('error', 'You must select a Courier Service.');
    		redirect(site_url('merchant/orders/order/'.$order_id));
    	}
    	if($method == 'Other'){
    		if(!$company || !$awb){
				$this->session->set_flashdata('error', 'You must enter Courier Company Name and AWB Number.');
				redirect(site_url('merchant/orders/order/'.$order_id));
			}
    	}

    	$this->load->model('Generalmodel');

    	$merchant_id 	= $this->session->userdata('merchant')['id'];
		$orderInfo 		= $this->Order_model->get_package_ready_order_item($order_id,false,$merchant_id);
		//$itemInfo 		= unserialize($orderInfo->contents);

		//print_r($orderInfo);
		//print_r(unserialize($orderInfo->contents)['weight']);
		//exit;
		
		$data = array();
		
		$i 					= 0;
		$weight 			= 0;
		$product_desc 		= '';
		$product_total 		= 0.00;
		$product_quantity 	= 0;
		$cod_amount 		= 0.00;
		$waybill 			= false;
		$waybill_method 	= $method;

		foreach($orderInfo->contents as $orderkey=>$product):
			$weight = $weight + (floatval($product['weight']) * intval($product['quantity']));
			$product_desc .= '('.($orderkey+1).') '.ucwords( str_replace('&','',preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $product['name'])) ).'<br>';
			$product_quantity  = $product_quantity + intval($product['quantity']);

			if(($orderInfo->payment_info == 'Charge on Delivery') || ($orderInfo->payment_info == 'Cash on Delivery') || ($orderInfo->payment_info == 'Pay on Delivery (Cash)')){
		    	$payment_mode 	= 'COD';
		    	$cod_amount 	= $cod_amount + floatval($product['total']);
		    } else if($orderInfo->payment_info == 'prepaid' || strtolower($orderInfo->payment_info) == 'payumoney'){
		    	$payment_mode 	= 'Pre-paid';
		    	$cod_amount 	= 0;
		    }
		    $product_total 	= $product_total + floatval($product['total']);

		endforeach;

		$amount_to_deduct = 0;
		$invoice_number = date('Y').'/'.$orderInfo->order_number.'/'.$orderInfo->merchant_id;
		$ship_to_add = $orderInfo->ship_address1;
	    if(trim($orderInfo->ship_address2) != ''){
	    	$ship_to_add .= ', '.trim($orderInfo->ship_address2);
	    }

	    if($method == 'Delhivery'){
			//$data['dispatch_date'] 						= null;
			//$data['dispatch_id'] 						= null;

			$data['pickup_location']['city'] 			= $orderInfo->merchant_city;
			$data['pickup_location']['name'] 			= 'GOJOJODEALS EXPRESS';
			$data['pickup_location']['pin'] 			= $orderInfo->merchant_zip;
			$data['pickup_location']['country']			= 'India';
			$data['pickup_location']['phone'] 			= $orderInfo->merchant_phone;
			$data['pickup_location']['add'] 			= $orderInfo->merchant_address;
			$data['pickup_location']['state'] 			= $orderInfo->merchant_state;

			$data['shipments'][$i]['pin'] 				= $orderInfo->ship_zip;
		    $data['shipments'][$i]['seller_tin'] 		= $orderInfo->merchant_tin;
		    $data['shipments'][$i]['seller_inv'] 		= $invoice_number;
		    $data['shipments'][$i]['city'] 				= $orderInfo->ship_city;
		    $data['shipments'][$i]['volumetric'] 		= 0.0;
		    $data['shipments'][$i]['weight'] 			= $weight." gm";
		    $data['shipments'][$i]['return_country'] 	= "India";
		    $data['shipments'][$i]['return_state'] 		= $orderInfo->merchant_state;
		    $data['shipments'][$i]['products_desc'] 	= trim($product_desc);
		    $data['shipments'][$i]['add'] 				= $ship_to_add;
		    $data['shipments'][$i]['state'] 			= $orderInfo->ship_zone;
		    $data['shipments'][$i]['billable_weight'] 	= $weight;
		    $data['shipments'][$i]['supplier'] 			= "";
		    $data['shipments'][$i]['dimensions'] 		= "";
		    $data['shipments'][$i]['waybill'] 			= "";
		    $data['shipments'][$i]['phone'] 			= $orderInfo->ship_phone;
		    $data['shipments'][$i]['payment_mode'] 		= $payment_mode;
		    $data['shipments'][$i]['order_date'] 		= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['name'] 				= $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname;
		    $data['shipments'][$i]['return_add'] 		= $orderInfo->merchant_address;
		    $data['shipments'][$i]['cod_amount'] 		= $cod_amount;
		    $data['shipments'][$i]['seller_cst'] 		= $orderInfo->merchant_cst;
		    $data['shipments'][$i]['total_amount'] 		= $product_total;
		    $data['shipments'][$i]['seller_name'] 		= $orderInfo->store_name;
		    $data['shipments'][$i]['return_city'] 		= $orderInfo->merchant_city;
		    $data['shipments'][$i]['country'] 			= $orderInfo->ship_country;
		    $data['shipments'][$i]['seller_add'] 		= null;
		    $data['shipments'][$i]['return_pin'] 		= $orderInfo->merchant_zip;
		    $data['shipments'][$i]['return_phone'] 		= $orderInfo->merchant_phone;
		    $data['shipments'][$i]['seller_inv_date'] 	= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['return_name'] 		= $orderInfo->merchant_firstname.' '.$orderInfo->merchant_lastname;
		    $data['shipments'][$i]['order'] 			= $orderInfo->order_number.'/'.$orderInfo->merchant_id;
		    $data['shipments'][$i]['return_reason'] 	= '{"return_reason":""}';
		    $data['shipments'][$i]['quantity'] 			= $product_quantity;

		    //print_r(json_encode($data));exit;

			// get shipping charge of delhivery
			$dataCharge 			= array();
			$dataCharge['pt'] 		= trim(strtolower($payment_mode));
			$dataCharge['gm'] 		= $weight;
			$dataCharge['o_pin'] 	= $orderInfo->merchant_zip;
			$dataCharge['d_pin'] 	= $orderInfo->ship_zip;
			$dataCharge['cl'] 		= 'GOJOJODEALS%20EXPRESS';
			$dataCharge['zn'] 		= 'A';
			$dataCharge['ss'] 		= 'Delivered';
			$dataCharge['md'] 		= 'E';

			$dataCharge['cod'] 		= $cod_amount;
		
			$responseCharge = $this->Generalmodel->hit_delhivery('get','kinko/v1/invoice/charges',$dataCharge);
			$responseCharge = json_decode($responseCharge, true);

			if(isset($responseCharge[0]['total_amount'])){
				$amount_to_deduct = $responseCharge[0]['total_amount'];
				$amount_to_deduct = $amount_to_deduct + (($amount_to_deduct * floatval(config_item('gojojo_delhivery_commission')))/100);

				$this->load->model('Shipping_model');
				$remaining_amount = $this->Shipping_model->get_amount($merchant_id);

				if($remaining_amount >= $amount_to_deduct){

					$response = $this->Generalmodel->hit_delhivery('post','cmu/create.json',$data);
					$result = json_decode($response);

					if($result->success){
						if(!empty($result->packages)){
							foreach ($result->packages as $package) {
								$waybill = $package->waybill;
							}
						}


					} else {
						$errors = array();
						if(!empty($result->packages)){
							foreach ($result->packages as $package) {
								$errors[] = $package->remarks[0];
							}
						}
						$this->session->set_flashdata('error', $result->rmk.'<br>'.implode('<br>', $errors));
					}

				} else {
					$this->session->set_flashdata('error', 'You don\'t have enough credit in your account to complete this pickup.<br>Required Amount: '.format_currency($amount_to_deduct).'<br>Remaining Amount: '.format_currency($remaining_amount));
				}

			} else {
				//print_r($responseCharge);exit;
				$this->session->set_flashdata('error', 'Something went wrong.'.'<br>'.json_encode($responseCharge));
			}

		} else if($method == 'Other'){
			$waybill = $awb;
			$waybill_method = $company;
		}


		if($waybill){
			$time = time();

			// update waybill and invoice number to db
			$save 								= array();
			$save['order_id'] 					= $order_id;
			$save['merchant_invoice_number'] 	= $invoice_number;
			$save['waybill'] 					= $waybill;
			$save['waybill_method'] 			= $waybill_method;
			foreach($orderInfo->contents as $orderkey=>$product):
				$save['id'] 					= $product['order_item_id'];
				$this->Order_model->update_shipping_info($save);

				// push status update
				$saveStatus						= array();
				$saveStatus['order_item_id']	= $product['order_item_id'];
				$saveStatus['ots_status']		= 'Pickup generated';
				//$saveStatus['notes']			= 'Pickup generated on '.date('Y-m-d h:i:s',$time).'. Track on Delhivery with AWB '.$waybill;
				$saveStatus['notes']			= 'Pickup generated on '.date('Y-m-d h:i:s',$time).'.';
				$this->Order_model->status_update($saveStatus);

			endforeach;

			if($method == 'Delhivery'){
				// deduct recharge amount
				$saveTransaction 				= array();
				$saveTransaction['merchant_id']	= $merchant_id;
				$saveTransaction['txn_id']		= $waybill;
				$saveTransaction['order_id'] 	= $order_id;
				$saveTransaction['weight'] 		= $weight;
				$saveTransaction['amount'] 		= (-1) * $amount_to_deduct;
				$saveTransaction['created_at'] 	= date('Y-m-d h:i:s',$time);
				$saveTransaction['updated_at'] 	= date('Y-m-d h:i:s',$time);

				$this->Shipping_model->add_transaction($saveTransaction);
			}
			
			$this->session->set_flashdata('message', 'Pickup generated with waybill number '.$waybill);
		}

		redirect(site_url('merchant/orders/order/'.$order_id));
		//die(json_encode(array('result'=>true,'data'=>$response)));
    }

    /*function generate_pickup($order_id){
    	$merchant_id 	= $this->session->userdata('merchant')['id'];
		$orderInfo 		= $this->Order_model->get_package_ready_order_item($order_id,false,$merchant_id);
		//$itemInfo 		= unserialize($orderInfo->contents);

		//print_r($orderInfo);
		//print_r(unserialize($orderInfo->contents)['weight']);
		//exit;
		
		$data = array();
		$data['dispatch_date'] 						= null;
		$data['dispatch_id'] 						= null;

		$data['pickup_location']['city'] 			= $orderInfo->merchant_city;
		$data['pickup_location']['name'] 			= 'GOJOJODEALS EXPRESS';
		$data['pickup_location']['pin'] 			= $orderInfo->merchant_zip;
		$data['pickup_location']['country']			= 'India';
		$data['pickup_location']['phone'] 			= $orderInfo->merchant_phone;
		$data['pickup_location']['add'] 			= $orderInfo->merchant_address;

		$i = 0;
		foreach($orderInfo->contents as $orderkey=>$product):
			$data['shipments'][$i]['pin'] 				= $orderInfo->ship_zip;
		    $data['shipments'][$i]['seller_tin'] 		= $orderInfo->merchant_tin;
		    $data['shipments'][$i]['seller_inv'] 		= "02197637";
		    $data['shipments'][$i]['city'] 				= $orderInfo->ship_city;
		    $data['shipments'][$i]['volumetric'] 		= 0.0;
		    $data['shipments'][$i]['weight'] 			= $product['weight']." gm";
		    $data['shipments'][$i]['return_country'] 	= "India";
		    $data['shipments'][$i]['return_state'] 		= $orderInfo->merchant_state;
		    $data['shipments'][$i]['products_desc'] 	= $product['name'];

		    $ship_to_add = $orderInfo->ship_address1;
		    if(trim($orderInfo->ship_address2) != ''){
		    	$ship_to_add .= ', '.trim($orderInfo->ship_address2);
		    }
		    $data['shipments'][$i]['add'] 				= $ship_to_add;
		    $data['shipments'][$i]['state'] 			= $orderInfo->ship_zone;
		    $data['shipments'][$i]['billable_weight'] 	= $product['weight'];
		    $data['shipments'][$i]['supplier'] 			= "";
		    $data['shipments'][$i]['dimensions'] 		= "";
		    $data['shipments'][$i]['waybill'] 			= "";
		    $data['shipments'][$i]['phone'] 			= $orderInfo->ship_phone;

		    if(($orderInfo->payment_info == 'Charge on Delivery') || ($orderInfo->payment_info == 'Cash on Delivery')){
		    	$payment_mode 	= 'COD';
		    	$cod_amount 	= $product['total'];
		    } else if($orderInfo->payment_info == 'prepaid'){
		    	$payment_mode 	= 'Prepaid';
		    	$cod_amount 	= 0;
		    }
		    $data['shipments'][$i]['payment_mode'] 		= $payment_mode;
		    $data['shipments'][$i]['order_date'] 		= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['name'] 				= $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname;
		    $data['shipments'][$i]['return_add'] 		= $orderInfo->merchant_address;
		    $data['shipments'][$i]['cod_amount'] 		= $cod_amount;
		    $data['shipments'][$i]['seller_cst'] 		= $orderInfo->merchant_cst;
		    $data['shipments'][$i]['total_amount'] 		= $product['total'];
		    $data['shipments'][$i]['seller_name'] 		= $orderInfo->store_name;
		    $data['shipments'][$i]['return_city'] 		= $orderInfo->merchant_city;
		    $data['shipments'][$i]['country'] 			= $orderInfo->ship_country;
		    $data['shipments'][$i]['seller_add'] 		= null;
		    $data['shipments'][$i]['return_pin'] 		= $orderInfo->merchant_zip;
		    $data['shipments'][$i]['return_phone'] 		= $orderInfo->merchant_phone;
		    $data['shipments'][$i]['seller_inv_date'] 	= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['return_name'] 		= $orderInfo->merchant_firstname.' '.$orderInfo->merchant_lastname;
		    $data['shipments'][$i]['order'] 			= $orderInfo->order_number;
		    $data['shipments'][$i]['return_reason'] 	= '{"return_reason":""}';
		    $data['shipments'][$i]['quantity'] 			= $product['quantity'];

		    $i++;
		endforeach;

	    print_r(json_encode($data));exit;

		$this->load->model('Generalmodel');
		$response = $this->Generalmodel->hit_delhivery('post','cmu/create.json',$data);
		$result = json_decode($response);

		if($result->success){
			$waybills = array();
			if(!empty($result->packages)){
				foreach ($result->packages as $package) {
					$waybills[] = $package->waybill;
				}
			}
			$this->session->set_flashdata('message', 'Pickup generated with waybill numbers '.implode(', ', $waybills));
		} else {
			$errors = array();
			if(!empty($result->packages)){
				foreach ($result->packages as $package) {
					$errors[] = $package->remarks[0];
				}
			}
			$this->session->set_flashdata('error', $result->rmk.'<br>'.implode('<br>', $errors));
		}
		redirect(site_url('merchant/orders/order/'.$order_id));
		//die(json_encode(array('result'=>true,'data'=>$response)));
    }*/

    function invoice($order_id){
    	$this->load->helper('date');
    	$merchant_id 		= $this->session->userdata('merchant')['id'];
		$data['order'] 		= $this->Order_model->get_package_ready_order_item($order_id,false,$merchant_id);

		$this->load->view($this->config->item('merchant_folder').'/invoice.php', $data);
    }

    function get_order_ship_address($order_id){
    	$merchant_id = $this->session->userdata('merchant')['id'];
    	$orderInfo = $this->Order_model->get_order($order_id,false,$merchant_id);
    	if($orderInfo){
	    	die(json_encode(array('success'=>true,'data'=>$orderInfo)));
	    } else {
	    	die(json_encode(array('success'=>false)));
	    }
    }

    function address_update($order_id){
    	if($order_id){
	    	$this->load->library('form_validation');	
	    	$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|max_length[32]');
			$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|max_length[32]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required|regex_match[/^[0-9]{10}$/]');
			$this->form_validation->set_rules('address1', 'Address 1', 'trim|required|max_length[128]');
			$this->form_validation->set_rules('address2', 'Address 2', 'trim|max_length[128]');
			$this->form_validation->set_rules('landmark', 'Landmark', 'trim|max_length[128]');
			$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[32]');
			$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');
			$this->form_validation->set_rules('zip', 'Pincode', 'trim|required|numeric|min_length[6]|max_length[6]');

			if ($this->form_validation->run() == FALSE)
			{
				if(validation_errors() != '')
				{
					die(json_encode(array('success'=>false, 'data'=>validation_errors() )));
				}
			}
			else
			{
				$zone					= $this->Location_model->get_zone($this->input->post('zone_id'));
				$a = array();
				$a['id']	= $order_id;
				$a['ship_firstname']	= $this->input->post('firstname');
				$a['ship_lastname']		= $this->input->post('lastname');
				$a['ship_email']		= $this->input->post('email');
				$a['ship_phone']		= $this->input->post('phone');
				$a['ship_address1']		= $this->input->post('address1');
				$a['ship_address2']		= $this->input->post('address2');
				$a['ship_landmark']		= $this->input->post('landmark');
				$a['ship_city']			= $this->input->post('city');
				$a['ship_zip']			= $this->input->post('zip');
				$a['ship_zone_id']		= $this->input->post('zone_id');
				$a['ship_zone']			= $zone->name;
				$a['bill_firstname']	= $this->input->post('firstname');
				$a['bill_lastname']		= $this->input->post('lastname');
				$a['bill_email']		= $this->input->post('email');
				$a['bill_phone']		= $this->input->post('phone');
				$a['bill_address1']		= $this->input->post('address1');
				$a['bill_address2']		= $this->input->post('address2');
				$a['bill_landmark']		= $this->input->post('landmark');
				$a['bill_city']			= $this->input->post('city');
				$a['bill_zip']			= $this->input->post('zip');
				$a['bill_zone_id']		= $this->input->post('zone_id');
				$a['bill_zone']			= $zone->name;

				$this->Order_model->save_order($a);
				$this->session->set_flashdata('message', 'Address Updated Successfully.');
				die(json_encode(array('success'=>true)));
			}
		} else {
			die(json_encode(array('success'=>false, 'data'=>'Order ID is required.')));
		}
    }

    function cancelled($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export')
		{
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;

			$data['orders']	= $this->Order_model->get_cancelled_orders($term);		

			foreach($data['orders'] as &$o)
			{
				$o->items	= $this->Order_model->get_items($o->id);
			}

			force_download_content('orders.xml', $this->load->view($this->config->item('merchant_folder').'/orders_xml', $data, true));
			
			//kill the script from here
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= 'Cancelled Orders';
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_cancelled_orders($term, $sort_by, $sort_order, $rows, $page, $merchant_id);
		$data['total']	= $this->Order_model->get_cancelled_orders(false,'','DESC',0,0,$merchant_id,true);
		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('merchant_folder').'/orders/cancelled/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
				
		$this->view($this->config->item('merchant_folder').'/orders', $data);
	}

	function returns($sort_by='order_number',$sort_order='desc', $code=0, $page=0, $rows=15)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		
		//if they submitted an export form do the export
		if($this->input->post('submit') == 'export')
		{
			$this->load->model('customer_model');
			$this->load->helper('download_helper');
			$post	= $this->input->post(null, false);
			$term	= (object)$post;

			$data['orders']	= $this->Order_model->get_returns_orders($term);		

			foreach($data['orders'] as &$o)
			{
				$o->items	= $this->Order_model->get_items($o->id);
			}

			force_download_content('orders.xml', $this->load->view($this->config->item('merchant_folder').'/orders_xml', $data, true));
			
			//kill the script from here
			die;
		}
		
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= 'Returns';
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_returns_orders($term, $sort_by, $sort_order, $rows, $page, $merchant_id);
		$data['total']	= $this->Order_model->get_returns_orders(false,'','DESC',0,0,$merchant_id,true);
		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('merchant_folder').'/orders/returns/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sort_order']	= $sort_order;
				
		$this->view($this->config->item('merchant_folder').'/orders', $data);
	}

	function reverse_order_pickup($order_id, $order_item_id){
    	$this->load->model('Generalmodel');

    	$merchant_id 	= $this->session->userdata('merchant')['id'];
		$orderInfo 		= $this->Order_model->get_package_ready_order_item($order_id,false,$merchant_id);
		//$itemInfo 		= unserialize($orderInfo->contents);

		//print_r($orderInfo);
		//print_r(unserialize($orderInfo->contents)['weight']);
		//exit;
		
		$data = array();
		
		$i 					= 0;
		$weight 			= 0;
		$product_desc 		= '';
		$product_total 		= 0.00;
		$product_quantity 	= 0;
		$cod_amount 		= 0.00;
		$waybill 			= false;
		$rp_generated		= false;
		
		foreach($orderInfo->contents as $orderkey=>$product):
			$weight = $weight + (floatval($product['weight']) * intval($product['quantity']));
			$product_desc .= '('.($orderkey+1).') '.ucwords( str_replace('&','',preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $product['name'])) ).'<br>';
			$product_quantity  = $product_quantity + intval($product['quantity']);

			$payment_mode 	= 'Pickup';
		    $cod_amount 	= 0;
		    $product_total 	= $product_total + floatval($product['total']);

		    $method 		= $product['waybill_method']; 
		    $waybill 		= $product['waybill']; 
		endforeach;

		$waybill_method 	= $method;

		$amount_to_deduct = 0;
		$invoice_number = date('Y').'/'.$orderInfo->order_number.'/'.$orderInfo->merchant_id;
		$ship_to_add = $orderInfo->ship_address1;
	    if(trim($orderInfo->ship_address2) != ''){
	    	$ship_to_add .= ', '.trim($orderInfo->ship_address2);
	    }

	    if($method == 'Delhivery'){
			$data['dispatch_date'] 						= null;
			$data['dispatch_id'] 						= null;

			$data['pickup_location']['city'] 			= $orderInfo->merchant_city;
			$data['pickup_location']['name'] 			= 'GOJOJODEALS EXPRESS';
			$data['pickup_location']['pin'] 			= $orderInfo->merchant_zip;
			$data['pickup_location']['country']			= 'India';
			$data['pickup_location']['phone'] 			= $orderInfo->merchant_phone;
			$data['pickup_location']['add'] 			= $orderInfo->merchant_address;

			$data['shipments'][$i]['pin'] 				= $orderInfo->ship_zip;
		    $data['shipments'][$i]['seller_tin'] 		= $orderInfo->merchant_tin;
		    $data['shipments'][$i]['seller_inv'] 		= $invoice_number;
		    $data['shipments'][$i]['city'] 				= $orderInfo->ship_city;
		    $data['shipments'][$i]['volumetric'] 		= 0.0;
		    $data['shipments'][$i]['weight'] 			= $weight." gm";
		    $data['shipments'][$i]['return_country'] 	= "India";
		    $data['shipments'][$i]['return_state'] 		= $orderInfo->merchant_state;
		    $data['shipments'][$i]['products_desc'] 	= $product_desc;
		    $data['shipments'][$i]['add'] 				= $ship_to_add;
		    $data['shipments'][$i]['state'] 			= $orderInfo->ship_zone;
		    $data['shipments'][$i]['billable_weight'] 	= $weight;
		    $data['shipments'][$i]['supplier'] 			= "";
		    $data['shipments'][$i]['dimensions'] 		= "";
		    $data['shipments'][$i]['waybill'] 			= $waybill;
		    $data['shipments'][$i]['phone'] 			= $orderInfo->ship_phone;
		    $data['shipments'][$i]['payment_mode'] 		= $payment_mode;
		    $data['shipments'][$i]['package_type'] 		= 'pickup';
		    $data['shipments'][$i]['order_date'] 		= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['name'] 				= $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname;
		    $data['shipments'][$i]['return_add'] 		= $orderInfo->merchant_address;
		    $data['shipments'][$i]['cod_amount'] 		= $cod_amount;
		    $data['shipments'][$i]['seller_cst'] 		= $orderInfo->merchant_cst;
		    $data['shipments'][$i]['total_amount'] 		= $product_total;
		    $data['shipments'][$i]['seller_name'] 		= $orderInfo->store_name;
		    $data['shipments'][$i]['return_city'] 		= $orderInfo->merchant_city;
		    $data['shipments'][$i]['country'] 			= $orderInfo->ship_country;
		    $data['shipments'][$i]['seller_add'] 		= null;
		    $data['shipments'][$i]['return_pin'] 		= $orderInfo->merchant_zip;
		    $data['shipments'][$i]['return_phone'] 		= $orderInfo->merchant_phone;
		    $data['shipments'][$i]['seller_inv_date'] 	= date(DATE_ISO8601, strtotime($orderInfo->ordered_on));
		    $data['shipments'][$i]['return_name'] 		= $orderInfo->merchant_firstname.' '.$orderInfo->merchant_lastname;
		    $data['shipments'][$i]['order'] 			= $orderInfo->order_number.'/'.$orderInfo->merchant_id;
		    $data['shipments'][$i]['return_reason'] 	= '{"return_reason":""}';
		    $data['shipments'][$i]['quantity'] 			= $product_quantity;

		    //print_r(json_encode($data));exit;

			// get shipping charge of delhivery
			$dataCharge 			= array();
			$dataCharge['pt'] 		= trim(strtolower($payment_mode));
			$dataCharge['gm'] 		= $weight;
			$dataCharge['o_pin'] 	= $orderInfo->merchant_zip;
			$dataCharge['d_pin'] 	= $orderInfo->ship_zip;
			$dataCharge['cl'] 		= 'GOJOJODEALS%20EXPRESS';
			$dataCharge['zn'] 		= 'A';
			$dataCharge['ss'] 		= 'Delivered';
			$dataCharge['md'] 		= 'E';

			$dataCharge['cod'] 		= $cod_amount;
		
			$responseCharge = $this->Generalmodel->hit_delhivery('get','kinko/v1/invoice/charges',$dataCharge);
			$responseCharge = json_decode($responseCharge, true);
			if(isset($responseCharge[0]['total_amount'])){
				$amount_to_deduct = $responseCharge[0]['total_amount'];
				$amount_to_deduct = $amount_to_deduct + (($amount_to_deduct * floatval(config_item('gojojo_delhivery_commission')))/100);

				$this->load->model('Shipping_model');
				$remaining_amount = $this->Shipping_model->get_amount($merchant_id);

				if($remaining_amount >= $amount_to_deduct){

					$response = $this->Generalmodel->hit_delhivery('post','cmu/create.json',$data);
					$result = json_decode($response);

					if($result->success){
						if(!empty($result->packages)){
							foreach ($result->packages as $package) {
								$rp_generated = $package->refnum;
							}
						}

					} else {
						$errors = array();
						if(!empty($result->packages)){
							foreach ($result->packages as $package) {
								$errors[] = $package->remarks[0];
							}
						}
						$this->session->set_flashdata('error', $result->rmk.'<br>'.implode('<br>', $errors));
					}

				} else {
					$this->session->set_flashdata('error', 'You don\'t have enough credit in your account to complete this pickup.<br>Required Amount: '.format_currency($amount_to_deduct).'<br>Remaining Amount: '.format_currency($remaining_amount));
				}

			} else {
				//print_r($responseCharge);exit;
				$this->session->set_flashdata('error', 'Something went wrong.'.'<br>'.json_encode($responseCharge));
			}

		} else if($method == 'Other'){
			$rp_generated = TRUE;
		}


		if($rp_generated){
			$time = time();

			foreach($orderInfo->contents as $orderkey=>$product):
				// push status update
				$saveStatus						= array();
				$saveStatus['order_item_id']	= $product['order_item_id'];
				$saveStatus['ots_status']		= 'Reverse Pickup generated';
				//$saveStatus['notes']			= 'Pickup generated on '.date('Y-m-d h:i:s',$time).'. Track on Delhivery with AWB '.$waybill;
				$saveStatus['notes']			= 'Reverse Pickup generated on '.date('Y-m-d h:i:s',$time).'.';
				$this->Order_model->status_update($saveStatus);

			endforeach;

			if($method == 'Delhivery'){
				// deduct recharge amount
				$saveTransaction 				= array();
				$saveTransaction['merchant_id']	= $merchant_id;
				$saveTransaction['txn_id']		= $waybill;
				$saveTransaction['order_id'] 	= $order_id;
				$saveTransaction['weight'] 		= $weight;
				$saveTransaction['amount'] 		= (-1) * $amount_to_deduct;
				$saveTransaction['created_at'] 	= date('Y-m-d h:i:s',$time);
				$saveTransaction['updated_at'] 	= date('Y-m-d h:i:s',$time);

				$this->Shipping_model->add_transaction($saveTransaction);

				$this->session->set_flashdata('message', 'Reverse Pickup generated with reference number '.$rp_generated);
			} else {
				$this->session->set_flashdata('message', 'Reverse Pickup status updated');
			}
			
		}

		redirect(site_url('merchant/orders/order/'.$order_id));
		//die(json_encode(array('result'=>true,'data'=>$response)));
    }

    function product_received($order_id, $item_id){
		//get last order status -> check if not delivered
		$last_status = $this->Order_model->get_order_item_statuses($item_id, true);

		if($last_status->ots_status == 'Reverse Pickup generated'){
			$save			= array();
			$save['order_item_id']	= $item_id;
			$save['ots_status']		= 'Returned';
			$save['notes']			= '';
			
			if($this->Order_model->status_update($save)){

				$this->load->model('Generalmodel');

				$orderInfo = $this->Order_model->get_order_by_item_package($item_id);
				$pm_id = 0;
				$pm_quantity = 0;
				$products_list = array();
				foreach($orderInfo->contents as $product):
					$products_list[] = ucwords(strtolower($this->Generalmodel->shorten_string($product['name'], 4, false)));
					if($product['order_item_id'] == $item_id){
						$pm_id = $product['pm_id'];
						$pm_quantity = $product['quantity'];
					}
				endforeach;

				//increase stock
				if($pm_id > 0 && $pm_quantity > 0){
					$this->load->model('Product_model');
					$this->Product_model->increase_product_stock($pm_id, $pm_quantity);
				}

				$sms_content 	= 'Your package '.implode(', ', $products_list).' ref to order '.$orderInfo->order_number.' is returned. Refunds will be initiated shortly.';
	        
				//send sms to user
				$this->Generalmodel->send_sms($orderInfo->ship_phone, $sms_content);

				$this->session->set_flashdata('message', 'Request has been completed.');
			} else {
				$this->session->set_flashdata('error', 'This product cant be returned at this stage.');
			}
		} else {
			$this->session->set_flashdata('error', 'This product cant be returned at this stage.');
		}
		redirect(site_url('merchant/orders/order/'.$order_id));
	}


}