<link rel="stylesheet" href="<?php echo base_url('assets/global/plugins/morris/morris.css');?>">
<script src="<?php echo base_url('assets/global/plugins/morris/raphael-min.js');?>"></script>
<script src="<?php echo base_url('assets/global/plugins/morris/morris.min.js');?>"></script>

<?php if(!$payment_module_installed):?>
	
	<div class="alert">
		<a class="close" data-dismiss="alert">×</a>
		<strong><?php echo lang('common_note') ?>:</strong> <?php echo lang('no_payment_module_installed'); ?>
	</div>

<?php endif;?>

<?php if(!$shipping_module_installed):?>
	<div class="alert">
		<a class="close" data-dismiss="alert">×</a>
		<strong><?php echo lang('common_note') ?>:</strong> <?php echo lang('no_shipping_module_installed'); ?>
	</div>

<?php endif;?>
<div class="row">
	<div class="span3" >
		<h2>Pending Orders</h2><span>(Last 48 hours)</span>
		<h1 style="text-align:center;"><?php echo $pending_last;?></h1>
	</div>
	<div class="span3" >
		<h2>Pending Orders</h2><span>(Before 48 hours)</span>
		<h1 style="text-align:center;"><?php echo $pending_before;?></h1>
	</div>
	<div class="span3" >
		<h2>Orders</h2><span>(No of orders wise)</span>
		<div id="donutOrders" style="height: 250px;"></div>
	</div>
	<div class="span3" >
		<h2>Orders</h2><span>(Percentage wise)</span>
		<div id="donutPercent" style="height: 250px;"></div>
	</div>
</div>
<div class="row">
	<div class="span12" >
		<h2>Sales
			<span>
				<?php echo form_open($this->config->item('admin_folder').'/dashboard/index', 'class="form-inline"');?>
					<fieldset>
						<input id="from_date" name="from_date" value="<?php echo $date_from;?>"  class="span2 datepicker" type="text" placeholder="From Date"/>
						<input id="to_date"  name="to_date" value="<?php echo $date_to;?>" class="span2 datepicker" type="text"  placeholder="To Date"/>
						<select id="" name="filter_status" class="span2">
							<option value="">--select--</option>
							<?php foreach($this->config->item('order_statuses') as $key=>$value){?>
								<option value="<?php echo $value;?>" <?php echo ($value==$filter_status?'selected':''); ?> ><?php echo $value;?></option>
							<?php } ?>
						</select>
						
						<button class="btn" name="submit" value="filter"><?php echo lang('search')?></button>
					</fieldset>
				</form>
			</span>
		</h2>
		<span><?php echo '('.$filter_status.')';?></span>
		<h1 style="text-align:center;"><?php echo $sales;?></h1>
	</div>
</div>
<h2><?php echo lang('recent_orders') ?></h2>
<table class="table table-striped">
    <thead>
		<tr>
			<th class="gc_cell_left"><?php echo lang('order_number') ?></th>
			<th><?php echo lang('product_reference') ?></th>
			<th><?php echo lang('bill_to') ?></th>
			<th><?php echo lang('ship_to') ?></th>
			<th><?php echo lang('ordered_on') ?></th>
			<th><?php echo lang('status') ?></th>
			<th class="gc_cell_right"><?php echo lang('notes') ?></th>
	    </tr>
	</thead>

    <tbody>
    <?php foreach($orders as $order): ?>
	<tr>
		<td  class="gc_cell_left"><a href="<?php echo site_url($this->config->item('admin_folder').'/orders/order/'.$order->id); ?>"><?php echo $order->order_number; ?></a></td>
		<td><?php echo $order->order_number.'-'.$order->item_id.'-'.$order->product_id.'-'.$order->pm_id; ?></td>
		<td><?php echo $order->bill_firstname.' '.$order->bill_lastname; ?></td>
		<td><?php echo $order->ship_firstname.' '.$order->ship_lastname; ?></td>
		<td><?php echo format_date($order->ordered_on); ?></td>
		<td style="width:150px;">
			<?php echo $order->ots_status ?> 
				
		</td>
		<td class="gc_cell_right"><div class="MainTableNotes"><?php echo $order->product_notes; ?></div></td>
	</tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="row">
	<div class="span12" style="text-align:center;">
		<a class="btn btn-large" href="<?php echo site_url(config_item('admin_folder').'/orders');?>"><?php echo lang('view_all_orders');?></a>
	</div>
</div>


<h2><?php echo lang('recent_customers') ?></h2>
<table class="table table-striped">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left"><?php echo lang('firstname') ?></th>
			<th><?php echo lang('lastname') ?></th>
			<th><?php echo lang('email') ?></th>
			<th class="gc_cell_right"><?php echo lang('active') ?></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($customers as $customer):?>
		<tr>
			<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
			<td class="gc_cell_left"><?php echo  $customer->firstname; ?></td>
			<td><?php echo  $customer->lastname; ?></td>
			<td><a href="mailto:<?php echo  $customer->email;?>"><?php echo  $customer->email; ?></a></td>
			<td>
				<?php if($customer->active == 1)
				{
					echo lang('yes');
				}
				else
				{
					echo lang('no');
				}
				?>
			</td>
		
		</tr>
<?php endforeach; ?>
	</tbody>
</table>


<div class="row">
	<div class="span12" style="text-align:center;">
		<a class="btn btn-large" href="<?php echo site_url(config_item('admin_folder').'/customers');?>"><?php echo lang('view_all_customers');?></a>
	</div>
</div>
<script>
$('.datepicker').datepicker({dateFormat:'dd-mm-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});

new Morris.Donut({
  // ID of the element in which to draw the chart.
  element: 'donutOrders',
  data: jQuery.parseJSON('<?php echo json_encode($orders_status)?>')
});

new Morris.Donut({
  // ID of the element in which to draw the chart.
  element: 'donutPercent',
  data: jQuery.parseJSON('<?php echo json_encode($orders_percent)?>')
});
</script>