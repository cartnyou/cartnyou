<div class="row">
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="advertiseCard">
        <div class="cardHeader card1">
            <h3 class="text-center">Basic</h3>
            <p class="text-center"><i class="fa fa-inr"></i>100</p>
        </div>
        <div class="cardContent">

            <ul>
                <li>
                    1 GB RAM
                </li>
                <li>
                   2 GB HD
                </li>
                <li>
                    2 GHz SPEED
                </li>
                <li>
                    UNLIMITED UPLOAD
                </li>
                <li>
                    <div class="form-group text-center mb-0">
                        <button class="btn btn-primary mtb-5">Select</button>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="advertiseCard">
            <div class="cardHeader card2">
                <h3 class="text-center">Basic</h3>
                <p class="text-center"><i class="fa fa-inr"></i>100</p>
            </div>
            <div class="cardContent">

                <ul>
                    <li>
                        1 GB RAM
                    </li>
                    <li>
                        2 GB HD
                    </li>
                    <li>
                        2 GHz SPEED
                    </li>
                    <li>
                        UNLIMITED UPLOAD
                    </li>
                    <li>
                        <div class="form-group text-center mb-0">
                            <button class="btn btn-primary mtb-5">Select</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="advertiseCard">
            <div class="cardHeader card3">
                <h3 class="text-center">Basic</h3>
                <p class="text-center"><i class="fa fa-inr"></i>100</p>
            </div>
            <div class="cardContent">

                <ul>
                    <li>
                        1 GB RAM
                    </li>
                    <li>
                        2 GB HD
                    </li>
                    <li>
                        2 GHz SPEED
                    </li>
                    <li>
                        UNLIMITED UPLOAD
                    </li>
                    <li>
                        <div class="form-group text-center mb-0">
                            <button class="btn btn-primary mtb-5">Select</button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>