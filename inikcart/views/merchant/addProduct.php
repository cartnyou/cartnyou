<div class="row">
	<div class="col-md-12 border-bottom-gray">
		<div class="row">
			<div class="col-sm-6">
				<h3>Sell an Existing Product</h3>
				
				<?php echo form_open($this->config->item('merchant_folder').'/products/search', array("class"=>"form-inline","id"=>"search-form"));?>
					<input type="text" class="col-md-2 form-control sm-70" name="name" placeholder="Search by product name" />
					<button class="btn btn-success table-group-action-submit" name="submit" value="search"><i class="fa fa-search"></i> Find</button>
				</form>
			
				<div class="light-text">
					Choose from list of products.<br>
					Just add a few details and start selling.
				</div>
			</div>
			<div class="col-sm-1 or-section">
				<span>or</span>
			</div>
			<div class="col-sm-5">
				<h3>Sell a New product</h3>
				<a class="btn btn-sm btn-success" href="<?php echo site_url($this->config->item('merchant_folder').'/products/form');?>"><i class="fa fa-plus-circle"></i> <?php echo lang('add_new_product');?></a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 hide" id="search-result-div">
		<h3>Search Results</h3>
		<div id="search-result">
		</div>

		<div id="pagination" class="pagination"><ul class="pagination pagination-sm"></ul></div>

	</div>
</div>

<script>
$(document).ready(function(){

    $(document).on('click','#pagination li',function(e){
 		e.preventDefault();
 		//$("#search-result").html('loading...');
 		
 		$(this).addClass('active');
        var pageNum = $(this).attr('data-id');

        var ele = $('#search-form');
		var data = ele.serializeArray();
        data.push({name: 'limit', value: 10});
		data.push({name: 'page', value: pageNum});
		get_search_results(data,pageNum);
    });

	$(document).on("submit", '#search-form', function(e){
		e.preventDefault();
		var ele = $('#search-form');
		var data = ele.serializeArray();
		data.push({name: 'limit', value: 10});
		data.push({name: 'page', value: 1});
		get_search_results(data,1);
  	});

  	function get_search_results(data, pageNum){
		$('#search-result').html('');
		$('#search-result-div').removeClass('hide');
		$('#pagination li').removeClass('active');

		$.ajax({
			data : data,
			url : "<?php echo site_url();?>"+"merchant/products/search",
			method: 'POST'
		}).done(function(getdata){
			getdata = JSON.parse(getdata);
			var html = '';
			if(getdata.products.length != 0){
				$.each(getdata.products, function(key, row) {
					html += '<div class="col-md-12 border-bottom-gray"><div class="col-md-2">';
					if(row.image != ''){
						html += '<img src="<?php echo base_url('uploads/images/thumbnails');?>/'+row.image+'">';
					}
					html += '</div><div class="col-md-8"><h4 class="theme-text">'+row.name+'</h4></div><div class="col-md-2"><a class="btn btn-sm btn-success" href="<?php echo site_url($this->config->item('merchant_folder').'/products/variant');?>/'+row.id+'">Start Selling</a></div></div>';
			 	});

			 	$('#pagination > ul').html(getdata.paginationHtml);
			 	$('#pagination').show();
			 	$('#page-'+pageNum).addClass('active');
			} else {
				html = 'No product found. Please try another name.';
			}
			$('#search-result').html(html);
		});
  	}
});
</script>