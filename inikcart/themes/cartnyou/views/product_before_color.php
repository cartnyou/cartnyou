<?php echo theme_js('jquery.matchHeight-min.js', true);?>
<?php
//$other_sellers = $product;
//array_splice($other_sellers, 0, 1);
$product = $product[0];
?>

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <?php if(!empty($category)){ ?>
	            <span class="navigation-pipe">&nbsp;</span>
	            <a class="home" href="<?php echo site_url($category->slug);?>" title="<?php echo ucwords($category->name);?>"><?php echo ucwords($category->name);?></a>
            <?php } ?>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><?php echo ucwords($product->name);?></span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <div class="center_column col-xs-12 col-sm-12" id="center_column">

                <?php if ($this->session->flashdata('message')):?>
                    <div class="alert alert-info">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php endif;?>
                
                <?php if ($this->session->flashdata('error')):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif;?>
                
                <?php if (!empty($error)):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $error;?>
                    </div>
                <?php endif;?>

                <!-- Product -->
                <div id="product">
                        
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-md-4 col-sm-5">
                            <!-- product-imge-->
                            <div class="product-image">
                                <div class="product-full">
                                    <?php if(!empty($product->images)): ?>
                                        <img id="product-zoom" class="width-100" src='<?php echo base_url('uploads/images/medium/'.$product->images[0]->filename);?>' data-zoom-image="<?php echo base_url('uploads/images/full/'.$product->images[0]->filename);?>"/>
                                    <?php else:
                                        echo theme_img('no_picture.png', lang('no_image_available'));
                                        endif; ?>
                                </div>
                                <div class="product-img-thumb" id="gallery_01">
                                    <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="2" data-loop="true">
                                        <?php if(!empty($product->images)): ?>
                                            <?php foreach($product->images as $image):?>
                                                <li>
                                                    <a href="javascript:void(0);" data-image="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" data-zoom-image="<?php echo base_url('uploads/images/full/'.$image->filename);?>">
                                                        <img id="product-zoom" src="<?php echo base_url('uploads/images/small/'.$image->filename);?>" />
                                                    </a>
                                                </li>
                                            <?php endforeach;?>
                                        <?php endif; ?>

                                        <?php
                                        if(count($options) > 0):
                                            foreach($options as $option):
                                                if($option->type == 'radiolist'):
                                                    foreach ($option->values as $values):
                                                        if($values->image != ''): ?>
                                                            <li>
                                                                <a href="javascript:void(0);" data-image="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" data-zoom-image="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>">
                                                                    <img id="product-zoom" src="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" />
                                                                </a>
                                                            </li>
                                                        <?php
                                                        endif;
                                                    endforeach;
                                                endif;
                                            endforeach;
                                        endif;
                                        ?>

                                    </ul>
                                </div>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-md-5 col-sm-7">
                            <h1 class="product-name"><?php echo ucwords($product->name);?></h1>
                            
                            <div class="info-orther">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <?php echo $product->brand_name;?>
                                    </div>
                                    <?php if(!empty($product->sku)):?>
                                        <div class="col-md-6 col-sm-6">
                                           <p class="pull-right">Product Code: #<?php echo $product->sku; ?></p>
                                        </div>
                                    <?php endif;?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>

                                        <?php else:?>
                                            <button class="add_bag colorRed" disabled="disabled">OUT OF STOCK</button>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>

                            <div class="product-comments">
                                <?php if($avg_rating->rating > 0){ ?>
                                    <div class="product-star">
                                        <div class="clearfix ratebox" data-id="<?php echo $product->id;?>" data-rating="<?php echo $avg_rating->rating;?>"></div>
                                        <span class="bsr"><a href="javascript:void(0);">Based on <?php echo $avg_rating->count;?> rating<?php echo ($avg_rating->count > 1) ? 's' : '';?></a></span>
                                    </div>
                                <?php } ?>

                                <div class="comments-advices pull-right">
                                    <span class="colorRed fw-b"> <?php echo $sold_times+rand(300,700);?> Sold Till Now</span>

                                </div>
                            </div>


                            <div class="product-price-group">
                                <?php if($product->saleprice > 0):?>
                                    <span class="price"><?php echo format_currency($product->saleprice); ?></span>
                                    <span class="old-price"></i><?php echo format_currency($product->price); ?></span>
                                <?php else: ?>
                                    <span class="price"><?php echo format_currency($product->price); ?></span>
                                <?php endif;?>
                                <?php
                                if($product->saleprice > 0){
                                    $discount_percent = 0;
                                    $discount = 0;
                                    if($product->price > 0){
                                        $discount = $product->price - $product->saleprice;
                                        $discount_percent = ($discount * 100) / $product->price;
                                    }
                                    if($discount_percent > 0){
                                        echo '<span class="discount">'.ceil($discount_percent).'% off</span>';
                                    }
                                }
                                ?>
                            </div>
                            <?php
                            if($product->saleprice > 0){
                                if($discount > 0){
                                    echo '<div class="product-price-group">';
                                        echo '<span>You Save:</span>';
                                        echo '<span class="price pdl-10">'.format_currency($discount).'</span>';
                                    echo '</div>';
                                }
                            }
                            ?>

                            <form id="form-pincode">
	                            <div class="pincodeAvail row">
	                                <div class="col-md-12 col-xs-12">
	                                    <div class="form-group col-md-6 col-sm-6 col-xs-12 mb0 p0">
	                                        <div class="input-group pin">
	                                        	<span class="input-group-addon checkBtn"><i class="fa fa-map-marker"></i></span>
	                                            <input type="text" class="form-control" placeholder="Check Pincode" id="input-pincode" name="zip">
	                                            <span class="input-group-addon checkBtn"><button id="btn-pincode">Check</button></span>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-md-12 col-xs-12 mb15" id="result-pincode"></div>
	                            </div>
                            </form>

                            <?php echo form_open('cart/add_to_cart', 'class="form-horizontal"');?>
		                        <input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
		                        <input type="hidden" name="id" value="<?php echo $product->id?>"/>
		                        <input type="hidden" name="pm_id" value="<?php echo $product->pm_id?>"/>

                                <div class="form-option">
                                    <div class="row pdlr-15">
                                        <div class="offerBox col-md-12 ptCursor" data-toggle="collapse" data-target="#allOffers">
                                           <div class="row">
                                               <div class="col-md-3 col-sm-3 col-xs-5">
                                                   <p class="offers colorGreen fw-b text-center">
                                                       3 OFFERS
                                                   </p>
                                               </div>
                                               <div class="col-md-9 col-sm-9 col-xs-7 text-right " >
                                                   <p>
                                                       <a href="javascript:;" data-toggle="collapse" data-target="#allOffers" class="linkSt ml-10">  View offers <i class="fa fa-caret-down"></i></a>
                                                   </p>
                                               </div>
                                           </div>
                                            <div class="col-md-12 collapse posr" id="allOffers">
                                                <p class="offers"><i class="fa fa-tag linkSt"></i> 10% Instant Discount* on Prepaid Order
                                                    <a href="javascript:;" class="linkSt getTc">T&C</a>
                                                </p>
                                                <div class="tcContainer">
                                                    <p class="text-right col-md-12"><i class="fa fa-close tcclose"></i></p>
                                                    <ul>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                        <li>10% Instant Discount* on IndusInd Bank Credit Card</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                    
                                <div class="form-option">
                                    <?php if(count($options) > 0): ?>
                                        <div class="row">
                                            <?php foreach($options as $option):
                                                $required   = '';
                                                if($option->required)
                                                {
                                                    $required = '<span class="required">*</span>';
                                                }
                                                ?>
                                                <?php
                                                $extra_class = "";
                                                if($option->html_class != ''):
                                                    if(($option->html_class == 'power-type') || ($option->html_class == 'color') || ($option->html_class == 'size')):
                                                        $extra_class = "hidden";
                                                    endif;
                                                endif;
                                                ?>

                                                <div class="col-md-12 col-sm-12">
                                                    <div class="attributes">
                                                        <div class="attribute-label col-sm-3 col-xs-4">
                                                            <p>
                                                                <?php echo $option->name.$required;?>
                                                                <?php if($option->hint !== ""){ ?>
                                                                    &nbsp;[<button class="product-option-hint" data-toggle="modal" data-target="#hintModal" data-heading="<?php echo $option->name;?> Details" data-details="<?php echo $option->hint;?>">Details</button>]
                                                                <?php } ?> :
                                                            </p>

                                                        </div>
                                                        <div class="attribute-list col-sm-9 col-xs-8">
                                                            <?php
                                                            if($option->type == 'checklist')
                                                            {
                                                                $value  = array();
                                                                if($posted_options && isset($posted_options[$option->id]))
                                                                {
                                                                    $value  = $posted_options[$option->id];
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if(isset($option->values[0]))
                                                                {
                                                                    $value  = $option->values[0]->value;
                                                                    if($posted_options && isset($posted_options[$option->id]))
                                                                    {
                                                                        $value  = $posted_options[$option->id];
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    $value = false;
                                                                }
                                                            }
                                                            if($option->type == 'textfield'):?>
                                                                <div class="controls">

                                                                            <input type="text" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>" class="col-md-12 form-control <?php if($option->html_class != '') echo $option->html_class;?>"/>
                                                                            <span class=""><?php echo($option->values[0]->price != 0)?' (+'.format_currency($option->values[0]->price).') ':'';?></span>
                                                                </div>
                                                            <?php elseif($option->type == 'textarea'):?>
                                                                <div class="controls">
                                                                    <textarea class="col-md-12 <?php if($option->html_class != '') echo $option->html_class;?>" name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
                                                                </div>
                                                            <?php elseif($option->type == 'droplist'):?>
                                                                <div class="controls">
                                                                    <select name="option[<?php echo $option->id;?>]" class="basic form-control <?php if($option->html_class != '') echo $option->html_class;?>">
                                                                        <option value=""><?php echo lang('choose_option');?></option>

                                                                    <?php foreach ($option->values as $values):
                                                                        $selected   = '';
                                                                        if($value == $values->id)
                                                                        {
                                                                            $selected   = ' selected="selected"';
                                                                        }?>

                                                                        <option<?php echo $selected;?> value="<?php echo $values->id;?>">
                                                                            <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                                        </option>

                                                                    <?php endforeach;?>
                                                                    </select>
                                                                </div>
                                                            <?php elseif($option->type == 'radiolist'):?>
                                                                <div class="controls">
                                                                    <ul class="listInline">
                                                                    <?php foreach ($option->values as $values):

                                                                        $checked = '';
                                                                        if($value == $values->id)
                                                                        {
                                                                            $checked = ' checked="checked"';
                                                                        }?>
                                                                        <li>
                                                                        <?php if($values->image != ''){ ?>
                                                                            <label class="radio radio-option-img">
                                                                        <?php } else { ?>
                                                                            <label class="radio">
                                                                        <?php } ?>
                                                                            <?php if($values->image != ''){ ?>
                                                                                <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>" class="hidden option-img-radio <?php if($option->html_class != '') echo $option->html_class;?>" data-smallImage="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" data-largeImage="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" />
                                                                                <img src="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" class="option-img" />
                                                                            <?php } else { ?>
                                                                                <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>" class="<?php if($option->html_class != '') echo $option->html_class;?>" />
                                                                            <?php } ?>

                                                                            <?php echo($values->price != 0)?'(+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                                        </label>
                                                                            </li>
                                                                    <?php endforeach;?>
                                                                    </ul>
                                                                </div>
                                                            <?php elseif($option->type == 'checklist'):?>
                                                                <div class="controls">
                                                                    <ul class="listInline">
                                                                    <?php foreach ($option->values as $values):

                                                                        $checked = '';
                                                                        if(in_array($values->id, $value))
                                                                        {
                                                                            $checked = ' checked="checked"';
                                                                        }?>

                                                                        <li>
                                                                        <label class="checkbox">
                                                                            <input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>" class="<?php if($option->html_class != '') echo $option->html_class;?>" />
                                                                            <?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
                                                                        </label>
                                                                        </li>

                                                                        
                                                                    <?php endforeach; ?>
                                                                    </ul>
                                                                </div>
                                                            <?php endif;?>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endforeach;?>
                                        </div>
                                    <?php endif;?>

                                    <div class="row">
                                        <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>
                                            <?php if(!$product->fixed_quantity) : ?>
                                                <div class="col-md-12">
                                                    <div class="attributes">
                                                        <div class="attribute-label col-md-3 col-xs-4">Qty:</div>
                                                        <div class="attribute-list product-qty">
			                                                <div class="qty">
			                                                    <input id="option-product-qty" type="text" value="1" name="quantity">
			                                                </div>
			                                                <div class="btn-plus">
			                                                    <a href="#" class="btn-plus-up">
			                                                        <i class="fa fa-caret-up"></i>
			                                                    </a>
			                                                    <a href="#" class="btn-plus-down">
			                                                        <i class="fa fa-caret-down"></i>
			                                                    </a>
			                                                </div>
			                                            </div>
                                                        <!-- <div class="attribute-list col-md-9 col-xs-8">
                                                            <select class="basic" name="quantity">
                                                                <option value="">QTY</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif;?>
                                        <div class="col-md-6 col-sm-6">
                                            <a href="javascript:;"  class="linkSt toSpecs">See More Specification</a>
                                        </div>

                                    </div>
                                </div>
                                <div class="">
                                    <div>
                                        <?php
                                        if($product->shipping_charges > 0){
                                            echo 'Shipping: '.format_currency($product->shipping_charges);
                                        } else {
                                            echo 'Free Shipping';
                                        }
                                        ?>
                                    </div>
                                    <div>
                                    	<?php
                                        if($product->payment_mode == 1){
                                        	echo 'Prepaid Only';
                                        } else if($product->payment_mode == 0){
                                        	echo 'Cash on Delivery Available';
                                        }
                                        ?>
                                    </div>
                                    <div class="">Sold By <a href="#" class="linkSt"><?php echo $product->store_name;?></a> <?php if($seller_avg_rating->count > 0){ ?> (<?php echo number_format($seller_avg_rating->rating_seller,1);?> out of 5 | <?php echo number_format($seller_avg_rating->count,0).' rating';echo ($seller_avg_rating->count > 1)?'s':'';?>)<?php } ?>
                                        <?php if($product->item_location != ''){ ?>
                                            <br> Item Location: <?php echo $product->item_location;?>
                                        <?php } ?>
                                        <br> Powered By
                                        <a href="javascript:;"><img src="<?php echo theme_img('/powered.png');?>" width="95" alt=""></a>
                                    </div>
                                </div>
                                <div class="form-action">
                                    <div class="button-group">
                                        <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>
                                            <button class="add_bag btn-add-cart" type="submit" value="submit"><i class="fa fa-cart-plus"></i> <?php echo lang('form_add_to_cart');?></button>
                                            <button class="add_bag btn-buy-now" type="submit" value="buy_now" name="buy_now_redirect"><i class="fa fa-shopping-cart"></i> Buy Now</button>
                                        <?php else:?>
                                            <button class="add_bag" disabled="disabled">OUT OF STOCK</button>
                                        <?php endif;?>
                                    </div>
                                    <a href="javascript:;" class="colorGreen add-to-wishlist" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?>>Add to Wishlist</a>

                                </div>

                            </form>


                            <div class="form-share">
                                <!--<div class="sendtofriend-print">
                                    <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                    <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                </div>
                                <div class="network-share">
                                </div>-->
                                <div class="socialShare">
                                    <a href="https://www.facebook.com/sharer.php?u=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm facebook-btn"><i class="fa fa-facebook"></i> Share</a>
                                    <a href="https://plus.google.com/share?url=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm googlebtn"><i class="fa fa-google-plus"></i> Share</a>
                                	<a href="https://twitter.com/share?text=<?php echo $product->name;?>&url=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm twitter-btn"><i class="fa fa-twitter"></i> Tweet</a>
                                	<a href="whatsapp://send?text=<?php echo $product->name.' '.site_url($product->slug);?>" class="btn btn-primary btn-sm whatsapp-btn" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i> Share</a>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-xs-12 col-md-3 col-sm-12">

                            <div class="allMerchant">
                                <h3 class="ots">Sold By</h3>

                                <div class="merchant mb-5">
                                    <p class="col-md-12 col-sm-12 col-xs-12">

                                        <a href="<?php echo site_url('seller/info/'.$product->merchant_slug);?>">
                                            <span class="pPrice pull-left">
                                                <?php echo $product->store_name;?></span>
                                            </span>
                                        </a>
                                        <span class="pull-right">
                                            <a class="btn-add-cart-2 mtb-6" href="<?php echo site_url('seller/info/'.$product->merchant_slug);?>"> View Seller</a>
                                        </span>

                                    </p>
                                    <p class="col-md-12 col-sm-12 col-xs-12">
                                         <span>
                                             <?php if($seller_avg_rating->count > 0){ ?> (<?php echo number_format($seller_avg_rating->rating_seller,1);?> out of 5 | <?php echo number_format($seller_avg_rating->count,0).' rating';echo ($seller_avg_rating->count > 1)?'s':'';?>)<?php } ?>
                                        </span>
                                    </p>
                                    <?php if($product->free_shipping){ ?>
                                        <p class="col-md-12 col-sm-12 col-xs-12">Free Delivery</p>
                                    <?php } ?>

                                </div>

                            </div>

                            <?php if(!empty($other_sellers)){ ?>
                                <div class="allMerchant mt-12">
                                    <h3 class="ots">Other Sellers</h3>

                                    <?php foreach ($other_sellers as $seller) { ?>
                                        <div class="row merchant mlr0 mb-5">
                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <span class="pPrice pull-left">
                                                <?php if($seller->saleprice > 0):?>
                                                    <?php echo format_currency($seller->saleprice);?>
                                                <?php else: ?>
                                                    <?php echo format_currency($seller->price);?>
                                                <?php endif;?></span>
                                                <span class="pull-right">
                                                    <a class="btn-add-cart-2 mtb-6" href="<?php echo site_url($seller->slug);?>"> View</a>
                                                </span>

                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Sold By:</strong> <a href="<?php echo site_url('seller/info/'.$seller->merchant_slug);?>" class="linkSt"><?php echo $seller->store_name;?></a></div>
                                            <?php if($seller->free_shipping){ ?>
                                                <div class="col-md-12 col-sm-12 col-xs-12">Free Delivery</div>
                                            <?php } ?>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
	                                            <?php
	                                            if($seller->payment_mode == 1){
	                                            	echo 'Prepaid Only';
	                                            } else if($seller->payment_mode == 0){
	                                            	echo 'Cash on Delivery Available';
	                                            }
	                                            ?>
                                            </div>
                                            
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        
                        </div>
                    </div>

                    <!-- tab product -->
                    <div class="product-tab" id="specs">
                        <ul class="nav-tab">
                            <li class="active">
                                <a aria-expanded="false" data-toggle="tab" href="#product-detail">Description</a>
                            </li>
                            <li>
                                <a aria-expanded="true" data-toggle="tab" href="#specification">Specification</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#reviews">Reviews</a>
                            </li>

                        </ul>
                        <div class="tab-container">
                            <div id="product-detail" class="tab-panel active">
                                <?php echo $product->description; ?>
                            </div>
                            <div id="specification" class="tab-panel">
                                <!-- <?php echo $product->excerpt;?>  -->
                                <?php echo $product->specification;?>  
                                <!-- <table class="table table-bordered">
                                    <tr>
                                        <td width="200">Compositions</td>
                                        <td>Cotton</td>
                                    </tr>
                                    <tr>
                                        <td>Styles</td>
                                        <td>Girly</td>
                                    </tr>
                                    <tr>
                                        <td>Properties</td>
                                        <td>Colorful Dress</td>
                                    </tr>
                                </table> -->
                            </div>
                            <div id="reviews" class="tab-panel">
                                <div class="product-comments-block-tab">
                                    <?php
                                    if(!empty($reviews)){
                                        foreach ($reviews as $review) {
                                    ?>
                                    <div class="comment row">
                                        <div class="col-sm-3 author">
                                            <div class="grade">
                                                <span><strong><?php echo ucwords($review->firstname.' '.$review->lastname);?></strong></span>
                                                <span class="reviewRating">
                                                    <div class="pull-right clearfix ratebox" data-id="<?php echo $review->pr_id;?>" data-rating="<?php echo $review->rating;?>"></div>
                                                </span>
                                            </div>
                                            <div class="info-author">
                                                <em><?php echo $review->created_at;?></em>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 commnet-dettail">
                                            <?php echo $review->review;?>
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    }
                                    ?>
                                    
                                    <div class="br"></div>
                                    <!-- <a class="btn-comment" href="#">Write your review !</a> -->
                                    <h3>WRITE A REVIEW</h3>

                                    <?php if(!$this->Customer_model->is_logged_in(false, false)):?>
										You need to login to review this product.<br>
										<a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-red mt-12">Login Now</a>
									<?php else: ?>
										<div class="clearfix">
	                                        <?php echo form_open('cart/add_review', 'class="form-horizontal"');?>
	                                            <input type="hidden" name="product_id" value="<?php echo $product->id?>" />
	                                            <input type="hidden" name="pm_id" value="<?php echo $product->pm_id?>"/>

	                                            <textarea id="review-textarea" name="review" rows="4" class="width-100 customTextarea"></textarea>
	                                            
                                                <input id="rating-input-price" name="rating_price" class="hidden form-control">
		                                        <div>Price: <span class="ratebox-input mt5" data-id="rating-input-price" data-rating="0"></span></div>
                                                <div class="br"></div>
		                                        <input id="rating-input-value" name="rating_value" class="hidden form-control">
		                                        <div>Value: <span class="ratebox-input mt5" data-id="rating-input-value" data-rating="0"></span></div>
		                                        <div class="br"></div>
                                                <input id="rating-input-quality" name="rating_quality" class="hidden form-control">
                                                <div>Quality: <span class="ratebox-input mt5" data-id="rating-input-quality" data-rating="0"></span></div>
                                                <div class="br"></div>

	                                            <input type="submit" class="btn btn-red mt-12" value="Submit Review">
	                                        </form>

	                                    </div>
									<?php endif; ?>

                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- ./tab product -->
                    
                    <?php if(!empty($product->related_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">Related Products</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                                
                                <?php foreach($product->related_products as $relate):?>
                                <li>
                                    <div class="product-container match-item">
                                        <div class="left-block">

                                            <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                <div class="group-price">
                                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                </div>
                                            <?php } ?>
                                            
                                            <a href="<?php echo site_url($relate->slug); ?>">
                                                <?php
                                                $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                                
                                                $relate->images = array_values((array)json_decode($relate->images));
                                        
                                                if(!empty($relate->images[0]))
                                                {
                                                    $primary    = $relate->images[0];
                                                    foreach($relate->images as $photo)
                                                    {
                                                        if(isset($photo->primary))
                                                        {
                                                            $primary    = $photo;
                                                        }
                                                    }

                                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                }
                                                echo $photo;
                                                ?>
                                            </a>

                                            <div class="quick-view">
                                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->id;?>" ></a>
                                                <a title="Add to compare" class="compare add-to-compare" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->id;?>" ></a>
                                                <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>
                                            <?php if($relate->rating > 0){ ?>
                                                <div class="product-star">
                                                    <div class="clearfix ratebox" data-id="<?php echo $relate->id;?>" data-rating="<?php echo $relate->rating;?>"></div>
                                                </div>
                                            <?php } ?>
                                            <div class="content_price">
                                                <?php if($relate->saleprice > 0):?>
                                                    <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                    <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php else: ?>
                                                    <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php endif; ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>

                    <?php if(!empty($category->special_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">You may also like</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                                
                                <?php foreach($category->special_products as $relate):?>
                                <li>
                                    <div class="product-container match-item">
                                        <div class="left-block">

                                            <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                <div class="group-price">
                                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                </div>
                                            <?php } ?>
                                            
                                            <a href="<?php echo site_url($relate->slug); ?>">
                                                <?php
                                                $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                                
                                                $relate->images = array_values((array)json_decode($relate->images));
                                        
                                                if(!empty($relate->images[0]))
                                                {
                                                    $primary    = $relate->images[0];
                                                    foreach($relate->images as $photo)
                                                    {
                                                        if(isset($photo->primary))
                                                        {
                                                            $primary    = $photo;
                                                        }
                                                    }

                                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                }
                                                echo $photo;
                                                ?>
                                            </a>

                                            <div class="quick-view">
                                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->id;?>" ></a>
                                                <a title="Add to compare" class="compare add-to-compare" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->id;?>" ></a>
                                                <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>
                                            <?php if($relate->rating > 0){ ?>
                                                <div class="product-star">
                                                    <div class="clearfix ratebox" data-id="<?php echo $relate->id;?>" data-rating="<?php echo $relate->rating;?>"></div>
                                                </div>
                                            <?php } ?>
                                            <div class="content_price">
                                                <?php if($relate->saleprice > 0):?>
                                                    <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                    <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php else: ?>
                                                    <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php endif; ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<div class="modal fade" id="hintModal" tabindex="-1" role="dialog" aria-labelledby="hintModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="hintModalLabel">Details</h4>
      </div>
      <div class="modal-body">
        <div id="hintDetails"></div>
      </div>
    </div>
  </div>
</div>

<?php echo theme_css('raterater.css', true);?>
<?php echo theme_js('raterater.jquery.js', true);?>

<script>
function ratingChanged(id, rating){
    $('#'+id).val(rating);
}

$(function(){ 
    $('.vertical-menu-content').css('display','none');
    
    /*$(".option-img").elevateZoom({
        zoomWindowPosition: 2,
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 500,
        lensFadeIn: 500,
        lensFadeOut: 500,
        zoomWindowWidth:200,
        zoomWindowHeight:200
    });*/

    $('#hintModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var hintHeading = button.data('heading');
        var hintDetails = button.data('details');
        var modal = $(this);
        modal.find('.modal-title').text(hintHeading);
        modal.find('.modal-body #hintDetails').html(hintDetails);
    });

    $('.ratebox').raterater( { 
        submitFunction: 'rateAlert', 
        allowChange: false,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
        isStatic: true,
    });

    $('.ratebox-input').raterater( { 
        submitFunction: 'ratingChanged', 
        allowChange: true,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
    });

    $('.ratebox-input-seller').raterater( { 
        submitFunction: 'ratingChanged', 
        allowChange: true,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
    });

    $(document).on("submit","#form-pincode", function(e){
        e.preventDefault();
        $('#result-pincode').html('');
        var val = $('#input-pincode').val().trim();

        if(val){
        	if(val.length == 6){
        		$.ajax({
		            data : {
		            	zip: val
		            },
		            url : '<?php echo site_url();?>cart/check_service_availability',
		            method: 'POST'
		        }).done(function(getdata){
		        	getdata = JSON.parse(getdata);
		            if (getdata.success) {
		                $('#result-pincode').html(getdata.data);
		            } else {
		                $('#result-pincode').html(getdata.data);
		            }
		        });
        	}
        }
    });

    setTimeout(function(){
        $('.match-item').matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });
    }, 500);

    $('.option-img-radio').on('change',function() {
        if($(this).val() != '') {
            var ez =   $('#product-zoom').data('elevateZoom');
            var smallImage = $(this).attr('data-smallImage');
            var largeImage = $(this).attr('data-largeImage');
            ez.swaptheimage(smallImage, largeImage); 
        } 
    });

});
</script>