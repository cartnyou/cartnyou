<?php

class Queries extends Admin_Controller { 
    
    function __construct(){
        parent::__construct();
        
        $this->auth->check_access('Admin', true);
        $this->load->model('Support_model');
    }
    
    function index($page=0){
        $data['page_title'] = 'Queries';
        $rows = 15;
        $term = $this->input->get('term');
        $data['term'] = $term;
        $data['data'] = $this->Support_model->get_queries('admin',false,false, $rows, $page, $term);
        $data['total'] = $this->Support_model->get_queries('admin',false,true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/queries/index/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        
        $this->view($this->config->item('admin_folder').'/queries', $data);
    }

    function detail($q_id=false){
        if(!$q_id){
            redirect(site_url('admin'));
        }

        $this->load->helper(array('form', 'date'));
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('notes', 'Notes', 'required');
        
        $message    = $this->session->flashdata('message');
        
        if ($this->form_validation->run() == TRUE){
            $save               = array();
            $save['q_id']       = $q_id;
            $save['qr_note']    = $this->input->post('notes');
            
            $data['message']    = 'Note Added.';
            
            $this->Support_model->add_note($save);
        }

        $data['row']        = $this->Support_model->get_query($q_id);
        $data['replies']    = $this->Support_model->get_query_replies($q_id);

        $data['page_title'] = 'Query: '.$data['row']->ticket_number;
        $this->view($this->config->item('admin_folder').'/query_detail', $data);
    }

    function query_status($id){
        $affected_rows   = $this->Support_model->query_status($id);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/queries');
    }

    
}