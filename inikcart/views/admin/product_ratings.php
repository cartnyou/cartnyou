<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Product</th>
			<th>Customer</th>
			<th>Rating(5)</th>
			<th>Review</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo ucwords($row->name);?></td>
					<td>
						<?php
						if($row->customer_id != 0){
                            echo ucwords($row->customer_firstname.' '.$row->customer_lastname);
                        } else {
                            echo ucwords($row->pr_name).'<br>'.$row->pr_email.'<br>'.$row->pr_phone;
                        }
                        ?>
					</td>
					<td>
						<b>Price:</b> <?php echo $row->rating_price;?><br>
						<b>Value:</b> <?php echo $row->rating_value;?><br>
						<b>Quality:</b> <?php echo $row->rating_quality;?>
					</td>
					<td><?php echo $row->review;?></td>
					<td><?php echo ($row->pr_enabled == '1') ? lang('enabled') : lang('disabled'); ?></td>
					<td>
						<div class="btn-group" style="float:right">
							<?php if($row->pr_enabled == '1'){ ?>
								<a class="btn" href="<?php echo  site_url(ADMIN_FOLDER.'/products/rating_change/'.$row->id);?>"><i class="icon-remove"></i> Disable</a>
							<?php } else { ?>
								<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/products/rating_change/'.$row->id);?>"><i class="icon-ok"></i> Enable</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="4">No rating found.</td></tr>';
		}
		?>
	</tbody>
</table>