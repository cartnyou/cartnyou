<div class="flexslider top_slider" id="carousel_<?php echo $id;?>">
	<ul class="slides">

		<?php
		$count = 1;
		foreach($banners as $banner):?>
			<li class="slide<?php echo $count;?>" style="background-image:url(<?php echo base_url('uploads/'.$banner->image);?>)">
				
				<!-- CONTAINER -->
				<div class="container">
					<div class="flex_caption1">
						<?php if($banner->name): ?>
							<div class="carousel-caption">
								<p class="title1 captionDelay2 FromTop"><?php echo $banner->name ?></p>
							</div>
						<?php endif; ?>
					</div>

					<?php
					if($banner->link)
					{
						$target=false;
						if($banner->new_window)
						{
							$target=' target="_blank"';
						}
						echo '<a class="flex_caption2" href="'.$banner->link.'"'.$target.'><div class="middle"><span>shop</span>now</div></a>';
					}
					?>
					<!-- <div class="flex_caption3 slide_banner_wrapper">
						<a class="slide_banner slide1_banner1 captionDelay4 FromBottom" href="javascript:void(0);" ><img src="images/slider/slide1_baner1.jpg" alt="" /></a>
						<a class="slide_banner slide1_banner2 captionDelay5 FromBottom" href="javascript:void(0);" ><img src="images/slider/slide1_baner2.jpg" alt="" /></a>
						<a class="slide_banner slide1_banner3 captionDelay6 FromBottom" href="javascript:void(0);" ><img src="images/slider/slide1_baner3.jpg" alt="" /></a>
					</div> -->
				</div><!-- //CONTAINER -->
			</li>
		
		<?php
			$count++;
		endforeach;
		?>
	</ul>
</div>