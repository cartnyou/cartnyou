<!DOCTYPE html>
<html>
  <head>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">
    <table width="600" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="400">
            <a href="<?php echo site_url();?>" target="_blank"><img style="width: 100px;display:block;" border="0" src="<?php echo theme_img('logo-gojojo.png');?>" alt="Gojojo" width="100" /></a>
          </td>
          <td width="200" align="right">
            <a href="<?php echo site_url();?>" target="_blank"><img style="width: 100px;display:block;" border="0" src="<?php echo theme_img('store_email.png');?>" width="100" /></a>
          </td>
        </tr>
        <tr></tr>
      </tbody>
    </table>

    <table width="600" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="600">
            <img style="width: 600px;display:block;" border="0" src="<?php echo theme_img('order_delivered.jpg');?>" alt="" width="600" />
          </td>
        </tr>
      </tbody>
    </table>

    <table width="600" style="background-color: #50c0c1; text-align: center;" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="600" style="font-size: 28px;padding-bottom: 15px;font-family: Arial;">
            <a target="_blank" href="<?php echo site_url().'secure/order/'.$order_number;?>" style="color:#000;"><u>Order #<?php echo $order_number ?></u></a>
          </td>
        </tr>
      </tbody>
    </table>

    <table width="600" style="background-color: #50c0c1;" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="50">
            
          </td>
          <td width="500" style="background-color: #ffffff;padding: 15px;">
            <table>
              <tbody>
                <tr>
                  <td width="500" style="border-bottom: 1px solid #d3d3d3;color: #102327;padding: 15px 0px;">
                    <table width="500">
                      <tr>
                        <td style="font-family: Arial;">Order #: <?php echo $order_number ?></td>
                        <td align="right" style="text-align: right;font-family: Arial;">Order date: <?php echo $ordered_on;?></td>
                      </tr>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="border-bottom: 1px solid #d3d3d3;color: #102327;padding: 15px 0px;">
                    <table width="500">
                    	<thead>
	                      <tr>
	                        <td style="font-family: Arial;color:#d3d3d3;font-size:12px;padding-bottom:15px">ITEM</td>
	                        <td style="font-family: Arial;color:#d3d3d3;font-size:12px;padding-bottom:15px">DESCRIPTION</td>
	                        <td style="font-family: Arial;color:#d3d3d3;font-size:12px;padding-bottom:15px">PRICE</td>
	                        <td style="font-family: Arial;color:#d3d3d3;font-size:12px;padding-bottom:15px">QTY</td>
	                        <td style="font-family: Arial;color:#d3d3d3;font-size:12px;padding-bottom:15px">TOTAL</td>
	                      </tr>
                      	</thead>

                      	<tfoot>
                        <tr>
                            <td colspan="4" style="border-top: 1px solid #d3d3d3;"><?php echo lang('subtotal');?></td>
                            <td style="border-top: 1px solid #d3d3d3;""><?php echo format_currency_rs($subtotal); ?></td>
                        </tr>
                        <tr>
            							<td colspan="4"><?php echo lang('shipping');?></td>
            							<td><?php echo format_currency_rs($shipping) ?></td>
            						<tr>

                        <?php if($coupon_discount > 0)  : ?>
                            <tr>
                                <td colspan="4"><?php echo lang('coupon_discount');?></td>
                                <td><?php echo format_currency_rs(0-$coupon_discount); ?></td>
                            </tr>
                            <?php
            						endif;
            						?>
                        

			           	<?php if($gift_card_discount != 0) : ?> 
				         	<tr>
								<td colspan="4"><?php echo lang('gift_card');?></td>

								<td><?php echo format_currency_rs($gift_card_discount); ?></td>
							</tr>
			          	<?php endif;   ?>
			            <tr> 
							<td colspan="4">
								<strong><?php echo lang('grand_total');?></strong>
							</td>
							<td>
								<strong><?php echo format_currency_rs($total); ?></strong>
							</td>
						</tr>
                        </tfoot>

                      	<tbody>
                      	<?php
						$subtotal = 0;
						foreach($contents as $cartkey=>$product):
						?>
                        	<tr>
                                <td style="padding:7px 0;"><?php echo ucwords($product['name']);?></td>
                                <td style="padding:7px 0;"><?php echo $product['excerpt'];
                                    if(isset($product['options'])) {
                                        foreach ($product['options'] as $name=>$value)
                                        {
                                            if(is_array($value))
                                            {
                                                echo '<div>'.$name.':<br/>';
                                                foreach($value as $item)
                                                    echo '- '.$item.'<br/>';
                                                echo '</div>';
                                            }
                                            else
                                            {
                                                echo '<div>'.$name.': '.$value.'</div>';
                                            }
                                        }
                                    }
                                    echo "<b>Seller:</b> ".$product['store_name'];
                                    ?>
                                </td>
                                <td style="padding:7px 0;"><?php echo format_currency_rs($product['price']);?></td>
                                <td style="padding:7px 0;"><?php echo $product['quantity'];?></td>
                                <td style="padding:7px 0;"><?php echo format_currency_rs($product['price']*$product['quantity']); ?></td>
                            </tr>
                      	<?php endforeach;?>
                      	</tbody>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="font-family: Arial;font-size: 20px;color:#50c0c1;padding:15px 0px 5px;">
                    Sent to:
                  </td>
                </tr>
                <tr>
                  <td width="500" style="font-family: Arial;color:#102327;">
                  	<?php
				  	echo '<b>'.$ship_firstname.' '.$ship_lastname.'</b><br>';
				  	echo $ship_email.'<br>';
				  	echo $ship_phone.'<br>';
				  	echo $ship_address1.'<br>';
				  	if(!empty($ship_address2)) echo $ship_address2.'<br>';
				  	echo $ship_city.', '.$ship_zone.' '.$ship_zip;
				  	?>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="font-family: Arial;font-size: 20px;color:#50c0c1;padding:15px 0px 5px;">
                    Payment Information
                  </td>
                </tr>
                <tr>
                  <td width="500" style="font-family: Arial;color:#102327;">
                  	<?php echo $payment_info; ?>
                  </td>
                </tr>

              </tbody>
            </table>
          
          </td>
          <td width="50">
            
          </td>
        </tr>
      </tbody>
    </table>

    <table width="600" style="background-color: #50c0c1;border-top: 3px solid #39a5a8;"" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="600" style="color:#ffffff;text-align: center;padding: 15px;font-family: Arial;">
          If you have any questions about your order,<br>
          please <a href="<?php echo site_url('contact-us');?>" target="_blank">contact us</a>
          </td>
        </tr>
        <tr></tr>
      </tbody>
    </table>

  </body>
</html>