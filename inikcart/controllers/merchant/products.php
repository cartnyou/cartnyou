<?php

class Products extends Merchant_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->auth->check_access_merchant('Merchant', true);
		
		$this->load->model(array('Product_model'));
		$this->load->helper('form');
		$this->lang->load('product');
	}

	function index($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=15)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		$data['active_nav'] = 'products';
		$data['active_sub_menu'] = 'products_list';
		$data['page_title']	= lang('products');
		
		$data['code']		= $code;
		$term				= false;
		$category_id		= false;
		
		//get the category list for the drop menu
		$data['categories']	= $this->Category_model->get_categories_tiered();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		if($post)
		{
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
		}
		elseif ($code)
		{
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search termalert
		$data['term']		= $term;
		$data['order_by']	= $order_by;
		$data['sort_order']	= $sort_order;
		
		$data['products']	= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page, 'merchant_id'=>$merchant_id));

		//total number of products
		$data['total']		= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'merchant_id'=>$merchant_id), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('merchant_folder').'/products/index/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		
		$this->view($this->config->item('merchant_folder').'/products', $data);
	}
	
	//basic category search
	function product_autocomplete($same_merchant=false)
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$merchant_id = false;
			if($same_merchant){
				$merchant_id = $this->session->userdata('merchant')['id'];
			}
			$results	= $this->Product_model->product_autocomplete($name, $limit, $merchant_id);
			
			$return		= array();
			
			foreach($results as $r)
			{
				if($same_merchant){
					$return[$r->id.'_'.$r->pm_id]	= $r->name;
				} else {
					$return[$r->id]	= $r->name;
				}
			}
			echo json_encode($return);
		}
		
	}
	
	function bulk_save()
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('merchant_folder').'/products');
		}
				
		foreach($products as $id=>$product)
		{
			$save['id']					= $id;
			$savePM['merchant_id']		= $merchant_id;
			$savePM['sku']				= $product['sku'];
			$save['name']				= $product['name'];
			$savePM['price']			= $product['price'];
			$savePM['saleprice']		= $product['saleprice'];
			$savePM['quantity']			= $product['quantity'];
			$savePM['enabled']			= $product['enabled'];
			$this->Product_model->save($save, false, false, $savePM);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('merchant_folder').'/products');
	}
	
	function form($id = false, $duplicate = false)
	{
		$this->product_id	= $id;
		$merchant_id = $this->session->userdata('merchant')['id'];

		$this->load->library('form_validation');
		$this->load->model(array('Option_model', 'Category_model', 'Digital_Product_model'));
		$this->lang->load('digital_product');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data['categories']		= $this->Category_model->get_merchant_categories_tiered($merchant_id);
		$data['file_list']		= $this->Digital_Product_model->get_list();

		$data['active_nav'] = 'products';
		$data['active_sub_menu'] = 'product_add';	
		$data['page_title']		= lang('product_form');

		//default values are empty if the product is new
		$data['id']					= '';
		$data['sku']				= '';
		$data['hns']				= '';
		$data['name']				= '';
		$data['slug']				= '';
		$data['description']		= '';
		$data['specification']		= '';
		$data['excerpt']			= '';
		$data['price']				= '';
		$data['saleprice']			= '';
		$data['weight']				= '';
		$data['track_stock'] 		= '';
		$data['seo_title']			= '';
		$data['meta']				= '';
		$data['shippable']			= '';
		$data['live_on']			= date('Y-m-d H:i:s',time());
		$data['payment_mode']		= 0;
		$data['shipping_charges']	= 0.00;
		$data['taxable']			= '';
		$data['tax_rate']			= 0.00;
		$data['fixed_quantity']		= '';
		$data['quantity']			= '';
		$data['enabled']			= '';
		$data['brand']				= '';
		$data['brand_name']			= '';
		$data['related_products']	= array();
		$data['product_categories']	= array();
		$data['images']				= array();
		$data['product_files']		= array();

		//create the photos array for later use
		$data['photos']		= array();

		if ($id)
		{	
			// get the existing file associations and create a format we can read from the form to set the checkboxes
			$pr_files 		= $this->Digital_Product_model->get_associations_by_product($id);
			foreach($pr_files as $f)
			{
				$data['product_files'][]  = $f->file_id;
			}
			
			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($id);
			$product					= $this->Product_model->get_product($id, true, $merchant_id);
			
			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('merchant_folder').'/products');
			}
			
			//helps us with the slug generation
			$this->product_name	= $this->input->post('slug', $product->slug);
			
			//set values to db values
			$data['id']					= $id;
			$data['sku']				= $product->sku;
			$data['hns']				= $product->hns;
			$data['name']				= $product->name;
			$data['seo_title']			= $product->seo_title;
			$data['meta']				= $product->meta;
			$data['slug']				= $product->slug;
			$data['description']		= $product->description;
			$data['specification']		= $product->specification;
			$data['excerpt']			= $product->excerpt;
			$data['price']				= $product->price;
			$data['saleprice']			= $product->saleprice;
			$data['weight']				= $product->weight;
			$data['track_stock'] 		= $product->track_stock;
			$data['shippable']			= $product->shippable;
			$data['live_on']			= $product->live_on;
			$data['payment_mode']		= $product->payment_mode;
			$data['shipping_charges']	= $product->shipping_charges;
			$data['quantity']			= $product->quantity;
			$data['taxable']			= $product->taxable;
			$data['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
			$data['fixed_quantity']		= $product->fixed_quantity;
			$data['enabled']			= $product->enabled;
			$data['brand']				= $product->brand;
			$data['brand_name']			= $product->brand_name;
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit'))
			{
				
				$data['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$data['product_categories'][] = $product_category->id;
				}
				
				$data['related_products']	= $product->related_products;
				$data['images']				= (array)json_decode($product->images);
			}
		}
		
		//if $data['related_products'] is not an array, make it one.
		if(!is_array($data['related_products']))
		{
			$data['related_products']	= array();
		}
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= array();
		}

		
		//no error checking on these
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');

		$this->form_validation->set_rules('sku', 'lang:sku', 'trim');
		$this->form_validation->set_rules('hns', 'HNS', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta_data', 'trim');
		$this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim');
		$this->form_validation->set_rules('description', 'lang:description', 'trim|required');
		$this->form_validation->set_rules('specification', 'Specification', 'trim|required');
		$this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
		$this->form_validation->set_rules('price', 'lang:price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('saleprice', 'lang:saleprice', 'trim|numeric|floatval');
		$this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric|floatval');
		$this->form_validation->set_rules('track_stock', 'lang:track_stock', 'trim|numeric');
		$this->form_validation->set_rules('quantity', 'lang:quantity', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'lang:shippable', 'trim|numeric');
		$this->form_validation->set_rules('live_on','Live On', 'trim');
		$this->form_validation->set_rules('payment_mode', 'Payment Mode', 'trim|numeric');
		$this->form_validation->set_rules('shipping_charges', 'Shipping Charges', 'trim|numeric|floatval|required');
		$this->form_validation->set_rules('taxable', 'lang:taxable', 'trim|numeric');
		//$this->form_validation->set_rules('tax_rate', 'Tax Rate', 'trim|numeric|floatval|required');
		$this->form_validation->set_rules('fixed_quantity', 'lang:fixed_quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|numeric|required');
		$this->form_validation->set_rules('categories[]','Category', 'required');

		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		if($duplicate)
		{
			$data['id']	= false;
		}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$data['product_options']	= $this->input->post('option');
			$data['related_products']	= $this->input->post('related_products');
			$data['product_categories']	= $this->input->post('categories');
			$data['images']				= $this->input->post('images');
			$data['product_files']		= $this->input->post('downloads');
			
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['mShip'] = $this->Product_model->is_merchant_associated_to_ship($merchant_id);
			$this->view($this->config->item('merchant_folder').'/product_form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('name').'-'.$this->session->userdata('merchant')['firstname'];
			}

			//add merchant id to slug
			$mEx = explode('mi'.$merchant_id, $slug);
			if (!end($mEx) == '') {
				$slug = $slug.'-mi'.$merchant_id;
			}
			
			$slug	= url_title(convert_accented_characters(substr($slug,0,80)), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');

			if($id)
			{
				$slug		= $this->Routes_model->validate_slug($slug, $product->route_id);
				$route_id	= $product->route_id;
			}
			else
			{
				$slug	= $this->Routes_model->validate_slug($slug);
				
				$route['slug']	= $slug;	
				$route_id	= $this->Routes_model->save($route);
			}

			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}

			$first_tax = 0;
			$first_commission = 0;
			if(!empty($categories)){
				foreach($categories as $c){
					$first_category_id 	= $c;
					$categoryInfo 		= $this->Category_model->get_category($c);
					$first_tax 			= $categoryInfo->tax_rate;
					$first_commission 	= $categoryInfo->commission;
					break;
				}
			}

			$save['id']					= $id;
			//$savePM['product_id']		= $id;
			$savePM['merchant_id']		= $merchant_id;
			$savePM['sku']				= $this->input->post('sku');
			$save['hns']				= $this->input->post('hns');
			$save['name']				= $this->input->post('name');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');
			$save['description']		= $this->input->post('description');
			$save['specification']		= $this->input->post('specification');
			$save['excerpt']			= $this->input->post('excerpt');
			$savePM['price']			= $this->input->post('price');
			$savePM['saleprice']		= $this->input->post('saleprice');
			$save['weight']				= $this->input->post('weight');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$savePM['quantity']			= $this->input->post('quantity');
			$save['shippable']			= $this->input->post('shippable');
			$savePM['live_on']			= $this->input->post('live_on');
			$savePM['payment_mode']		= $this->input->post('payment_mode');
			$savePM['shipping_charges']	= $this->input->post('shipping_charges');
			$save['taxable']			= $this->input->post('taxable');
			$save['tax_rate']			= $first_tax;
			$save['commission']			= $first_commission;
			$savePM['enabled']			= $this->input->post('enabled');
			$save['brand']				= $this->input->post('brand');
			$post_images				= $this->input->post('images');
			
			$savePM['slug']				= $slug;
			$savePM['route_id']			= $route_id;
			
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					foreach($post_images as $key => &$pi)
					{
						if($primary == $key)
						{
							$pi['primary']	= true;
							continue;
						}
					}	
				}
				
			}
			
			$save['images']				= json_encode($post_images);
			
			
			if($this->input->post('related_products'))
			{
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					if(strtolower($option['name']) == 'color' || strtolower($option['name']) == 'colors' || strtolower($option['name']) == 'colour' || strtolower($option['name']) == 'colours'){
						$option['name'] = 'Colour';
					}
					$options[]	= $option;
				}

			}
			
			// save product 
			$product_id	= $this->Product_model->save($save, $options, $categories, $savePM);
			
			// add file associations
			// clear existsing
			$this->Digital_Product_model->disassociate(false, $product_id);
			// save new
			$downloads = $this->input->post('downloads');
			if(is_array($downloads))
			{
				foreach($downloads as $d)
				{
					$this->Digital_Product_model->associate($d, $product_id);
				}
			}			

			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'cart/product/'.$product_id.'/'.$merchant_id;
			
			$this->Routes_model->save($route);
			
			$this->session->set_flashdata('message', lang('message_saved_product'));

			//go back to the product list
			redirect($this->config->item('merchant_folder').'/products');
		}
	}
	
	function product_image_form()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('merchant_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_form_multiple()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('merchant_folder').'/iframe/product_image_uploader_multiple', $data);
	}
	
	function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 600;
			$config['height'] = 500;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			//card
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/card/'.$upload_data['file_name'];
			$config['maintain_ratio'] = FALSE;
			$config['width'] = 268;
			$config['height'] = 327;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['file_name']	= $upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
		$this->load->view($this->config->item('merchant_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_upload_multiple()
	{
		$data['file_name'] 	= array();
		$data['error']		= array();	

		if(isset($_FILES['upl_files'])){
			$this->load->library('upload');

			$number_of_files_uploaded = count($_FILES['upl_files']['name']);

		    for ($i = 0; $i < $number_of_files_uploaded; $i++) :
		    	$_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
		    	$_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
		    	$_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
		      	$_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
		      	$_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];

		      	$config = array(
			        'allowed_types' => 'jpg|jpeg|png|gif|JPG|JPEG',
			        'max_size'      => $this->config->item('size_limit'),
			        'upload_path'	=> 'uploads/images/full',
			        'encrypt_name'	=> true,
			        'remove_spaces'	=> true
		      	);
		      	
		      	$this->upload->initialize($config);

		      	if ( ! $this->upload->do_upload()) :
		        	$data['error'][$i]	= $this->upload->display_errors();

		      	else :
		        	$upload_data = $this->upload->data();

		        	$this->load->library('image_lib');

					//this is the larger image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 600;
					$config['height'] = 500;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();

					//small image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 235;
					$config['height'] = 235;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$this->image_lib->clear();

					//cropped thumbnail
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 150;
					$config['height'] = 150;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					//card
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/card/'.$upload_data['file_name'];
					$config['maintain_ratio'] = FALSE;
					$config['width'] = 268;
					$config['height'] = 327;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					$data['file_name'][$i]	= $upload_data['file_name'];

		      	endif;
		    endfor;
		}

	    $this->load->view($this->config->item('merchant_folder').'/iframe/product_image_uploader_multiple', $data);		
	}
	
	function delete($id = false)
	{
		if ($id)
		{	
			$product	= $this->Product_model->get_product($id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('merchant_folder').'/products');
			}
			else
			{

				// remove the slug
				$this->load->model('Routes_model');
				$this->Routes_model->delete($product->route_id);

				//if the product is legit, delete them
				$this->Product_model->delete_product($id);

				$this->session->set_flashdata('message', lang('message_deleted_product'));
				redirect($this->config->item('merchant_folder').'/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('merchant_folder').'/products');
		}
	}

	function addProduct(){
		$data['active_nav'] = 'products';
		$data['active_sub_menu'] = 'product_add';	
		$data['page_title']		= "Add Product";
		$this->view($this->config->item('merchant_folder').'/addProduct', $data);
	}

	function search()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');

		$page = 1;
		if(!empty($this->input->post('page'))) {
			$page = $this->input->post('page');
		}
		$offset = ($page-1)*$limit;
		if($offset < 0) $offset = 0;

		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$total_records	= $this->Product_model->product_autocomplete($name, $limit, false, false, true);
			$results	= $this->Product_model->product_autocomplete($name, $limit, false, $offset);
			
			$products 	= array();
			
			foreach($results as $r)
			{
				$image = '';
				if(isset($r->images)){
					if($r->images != 'false'){
						$images = json_decode($r->images, true);
						if($images){
							foreach($images as $imageSingle):
		                    	if(isset($imageSingle['primary'])){
		                    		$image = $imageSingle['filename'];
		                    	}
		                    endforeach;
		                }
					}
				}
				$products[]	= array(
							'id' => $r->id,
							'name' => $r->name,
							'image' => $image
						);
			}

			$paginationHtml = '';

			$total_pages = ceil($total_records / $limit); 
			if(!empty($total_pages)):
				if($total_pages > 1){
					for($i=1; $i<=$total_pages; $i++):  
						$paginationHtml .= '<li data-id="'.$i.'" id="page-'.$i.'"><a href="javascript:;">'.$i.'</a></li>';
					endfor;
				}
			endif;

			echo json_encode(array('products'=>$products,'paginationHtml'=>$paginationHtml));
		}
		
	}

	function variant($id = false, $duplicate = true,$pmId=false)
	{
		$this->product_id	= $id;
		$merchant_id = $this->session->userdata('merchant')['id'];

		$this->load->library('form_validation');
		$this->load->model(array('Option_model', 'Category_model', 'Digital_Product_model'));
		$this->lang->load('digital_product');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data['categories']		= $this->Category_model->get_categories_tiered();
		$data['file_list']		= $this->Digital_Product_model->get_list();

		$data['active_nav'] = 'products';
		$data['active_sub_menu'] = 'product_add';	
		$data['page_title']		= lang('product_form');

		$data['commission'] = $this->Product_model->get_product_commission_by_category($id);

		//default values are empty if the product is new
		$data['id']					= '';
		$data['pmId']				= $pmId;
		$data['sku']				= '';
		$data['hns']				= '';
		$data['name']				= '';
		$data['slug']				= '';
		$data['description']		= '';
		$data['specification']		= '';
		$data['excerpt']			= '';
		$data['price']				= '';
		$data['saleprice']			= '';
		$data['weight']				= '';
		$data['track_stock'] 		= '';
		$data['seo_title']			= '';
		$data['meta']				= '';
		$data['shippable']			= '';
		$data['live_on']			= date('Y-m-d H:i:s',time());
		$data['payment_mode']		= 0;
		$data['taxable']			= '';
		$data['tax_rate']			= 0.0;
		$data['fixed_quantity']		= '';
		$data['quantity']			= '';
		$data['enabled']			= '';
		$data['related_products']	= array();
		$data['product_categories']	= array();
		$data['images']				= array();
		$data['product_files']		= array();

		//create the photos array for later use
		$data['photos']		= array();

		if ($id)
		{	
			// get the existing file associations and create a format we can read from the form to set the checkboxes
			$pr_files 		= $this->Digital_Product_model->get_associations_by_product($id);
			foreach($pr_files as $f)
			{
				$data['product_files'][]  = $f->file_id;
			}
			
			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($id);

			if($pmId){
				$product					= $this->Product_model->get_product($id, true,'',false,'',$pmId);
			} else {
				$product					= $this->Product_model->get_product($id, false);
			}

			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('merchant_folder').'/products');
			}
			
			//helps us with the slug generation
			//$this->product_name	= $this->input->post('slug', $product->slug);
			
			//set values to db values
			if($pmId){
				$data['id']					= $id;
				$data['sku']				= $product->sku;
				$data['hns']				= $product->hns;
				$data['name']				= $product->name;
				$data['seo_title']			= $product->seo_title;
				$data['meta']				= $product->meta;
				$data['slug']				= $product->slug;
				$data['description']		= $product->description;
				$data['specification']		= $product->specification;
				$data['excerpt']			= $product->excerpt;
				$data['price']				= $product->price;
				$data['saleprice']			= $product->saleprice;
				$data['weight']				= $product->weight;
				$data['track_stock'] 		= $product->track_stock;
				$data['shippable']			= $product->shippable;
				$data['live_on']			= $product->live_on;
				$data['payment_mode']		= $product->payment_mode;
				$data['shipping_charges']	= $product->shipping_charges;
				$data['quantity']			= $product->quantity;
				$data['taxable']			= $product->taxable;
				$data['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
				$data['fixed_quantity']		= $product->fixed_quantity;
				$data['enabled']			= $product->enabled;
			} else {
				$data['id']					= $id;
				$data['sku']				= '';
				$data['hns']				= $product->hns;
				$data['name']				= $product->name;
				$data['seo_title']			= $product->seo_title;
				$data['meta']				= $product->meta;
				//$data['slug']				= $product->slug;
				$data['description']		= $product->description;
				$data['specification']		= $product->specification;
				$data['excerpt']			= $product->excerpt;
				$data['price']				= '';
				$data['saleprice']			= '';
				$data['weight']				= $product->weight;
				$data['track_stock'] 		= $product->track_stock;
				$data['shippable']			= $product->shippable;
				$data['live_on']			= date('Y-m-d H:i:s',time());
				$data['payment_mode']		= 0;
				$data['shipping_charges']	= 0.00;
				$data['quantity']			= '';
				$data['taxable']			= $product->taxable;
				$data['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
				$data['fixed_quantity']		= $product->fixed_quantity;
				$data['enabled']			= $product->enabled;
			}
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit'))
			{
				
				$data['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$data['product_categories'][] = $product_category->id;
				}
				
				$data['related_products']	= $product->related_products;
				$data['images']				= (array)json_decode($product->images);
			}
		}
		
		//if $data['related_products'] is not an array, make it one.
		if(!is_array($data['related_products']))
		{
			$data['related_products']	= array();
		}
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= array();
		}

		
		//no error checking on these
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');

		$this->form_validation->set_rules('sku', 'lang:sku', 'trim');
		$this->form_validation->set_rules('hns', 'HNS', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta_data', 'trim');
		$this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim');
		$this->form_validation->set_rules('description', 'lang:description', 'trim|required');
		$this->form_validation->set_rules('specification', 'Specification', 'trim');
		$this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
		$this->form_validation->set_rules('price', 'lang:price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('saleprice', 'lang:saleprice', 'trim|numeric|floatval');
		$this->form_validation->set_rules('weight', 'lang:weight', 'trim|numeric|floatval');
		$this->form_validation->set_rules('track_stock', 'lang:track_stock', 'trim|numeric');
		$this->form_validation->set_rules('quantity', 'lang:quantity', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'lang:shippable', 'trim|numeric');
		$this->form_validation->set_rules('live_on', 'Live On', 'trim');
		$this->form_validation->set_rules('payment_mode', 'Payment Mode', 'trim|numeric');
		$this->form_validation->set_rules('shipping_charges', 'Shipping Charges', 'trim|numeric|floatval|required');
		$this->form_validation->set_rules('taxable', 'lang:taxable', 'trim|numeric');
		$this->form_validation->set_rules('fixed_quantity', 'lang:fixed_quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');

		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		//if($duplicate){
			//$data['id']	= false;
		//}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$data['product_options']	= $this->input->post('option');
			$data['related_products']	= $this->input->post('related_products');
			$data['product_categories']	= $this->input->post('categories');
			$data['images']				= $this->input->post('images');
			$data['product_files']		= $this->input->post('downloads');
			
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['mShip'] = $this->Product_model->is_merchant_associated_to_ship($merchant_id);
			$this->view($this->config->item('merchant_folder').'/variant_form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//first check the slug field
			$slug = $this->input->post('slug');
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $this->input->post('name').'-'.$this->session->userdata('merchant')['firstname'];
			}
			
			//add merchant id to slug
			$mEx = explode('mi'.$merchant_id, $slug);
			if (!end($mEx) == '') {
				$slug = $slug.'-mi'.$merchant_id;
			}

			$slug	= url_title(convert_accented_characters(substr($slug,0,80)), 'dash', TRUE);
			
			//validate the slug
			$this->load->model('Routes_model');

			if($id)
			{
				if($pmId){
					//edit
					$slug		= $this->Routes_model->validate_slug($slug, $product->route_id);
					$route_id	= $product->route_id;
//echo $route_id.'<br>';echo $slug;exit;
					//save the route
					$route['id']	= $route_id;
					$route['slug']	= $slug;
					$route['route']	= 'cart/product/'.$id.'/'.$merchant_id;
					
					$this->Routes_model->save($route);
				} else {
					//new
					$slug		= $this->Routes_model->validate_slug($slug);
					//$route_id	= $product->route_id;

					//save the route
					$route['slug']	= $slug;
					$route['route']	= 'cart/product/'.$id.'/'.$merchant_id;
					
					$route_id = $this->Routes_model->save($route);
				}
				
			}
			else
			{
				$slug	= $this->Routes_model->validate_slug($slug);
				
				$route['slug']	= $slug;	
				$route_id	= $this->Routes_model->save($route);
			}

			$save['id']					= $id;
			//$savePM['product_id']		= $id;
			$savePM['merchant_id']		= $merchant_id;
			$savePM['sku']				= $this->input->post('sku');
			$save['hns']				= $this->input->post('hns');
			$save['name']				= $this->input->post('name');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');
			$save['description']		= $this->input->post('description');
			$save['specification']		= $this->input->post('specification');
			$save['excerpt']			= $this->input->post('excerpt');
			$savePM['price']			= $this->input->post('price');
			$savePM['saleprice']		= $this->input->post('saleprice');
			$save['weight']				= $this->input->post('weight');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$savePM['quantity']			= $this->input->post('quantity');
			$save['shippable']			= $this->input->post('shippable');
			$savePM['live_on']			= $this->input->post('live_on');
			$savePM['payment_mode']		= $this->input->post('payment_mode');
			$savePM['shipping_charges']	= $this->input->post('shipping_charges');
			$save['taxable']			= $this->input->post('taxable');
			//$save['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
			$savePM['enabled']			= $this->input->post('enabled');
			$post_images				= $this->input->post('images');
			
			$savePM['slug']				= $slug;
			$savePM['route_id']			= $route_id;
			
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					foreach($post_images as $key => &$pi)
					{
						if($primary == $key)
						{
							$pi['primary']	= true;
							continue;
						}
					}	
				}
				
			}
			
			$save['images']				= json_encode($post_images);
			
			
			if($this->input->post('related_products'))
			{
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}
			
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	
			
			// save product 
			$product_id	= $this->Product_model->save($save, $options, $categories, $savePM);
			
			// add file associations
			// clear existsing
			$this->Digital_Product_model->disassociate(false, $product_id);
			// save new
			$downloads = $this->input->post('downloads');
			if(is_array($downloads))
			{
				foreach($downloads as $d)
				{
					$this->Digital_Product_model->associate($d, $product_id);
				}
			}
			
			
			$this->session->set_flashdata('message', lang('message_saved_product'));

			//go back to the product list
			redirect($this->config->item('merchant_folder').'/products');
		}
	}

	function delete_variant($variant_id = false)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		if ($variant_id)
		{	
			$product	= $this->Product_model->get_product_by_variant($variant_id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('merchant_folder').'/products');
			}
			else
			{

				// remove the slug
				//$this->load->model('Routes_model');
				//$this->Routes_model->delete($product->route_id);

				//if the variant is legit, delete them
				$this->Product_model->delete_variant($variant_id);

				$this->session->set_flashdata('message', lang('message_deleted_product'));
				redirect($this->config->item('merchant_folder').'/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('merchant_folder').'/products');
		}
	}

	function disable_variant($variant_id = false)
	{
		$merchant_id = $this->session->userdata('merchant')['id'];
		if ($variant_id)
		{	
			$product	= $this->Product_model->get_product_by_variant($variant_id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('merchant_folder').'/products');
			}
			else
			{

				// remove the slug
				//$this->load->model('Routes_model');
				//$this->Routes_model->delete($product->route_id);

				//if the variant is legit, delete them
				$this->Product_model->disable_variant($variant_id);

				$this->session->set_flashdata('message', 'Product Disabled Successfully.');
				redirect($this->config->item('merchant_folder').'/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('merchant_folder').'/products');
		}
	}

	function addBrand(){
		$data['active_nav'] = 'products';
		$data['active_sub_menu'] = 'product_brand';	
		$data['page_title']		= "Add Brand";
		$this->view($this->config->item('merchant_folder').'/addBrand', $data);
	}

}
