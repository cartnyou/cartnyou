<?php 
class Seller extends Front_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Seller_model');
	}

	function index(){
		redirect(site_url());
	}

	function info($slug=''){
		if($slug==''){
			redirect(site_url());
		}

		$data['seller'] = $this->Seller_model->get_seller_by_slug($slug);
		$id = $data['seller']->id;
		$data['reviews'] = $this->Seller_model->get_seller_reviews($id);
		$data['avg_rating'] = $this->Seller_model->get_seller_avg_rating($id);
		$data['split_rating'] = $this->Seller_model->get_seller_split_rating($id);
		$data['seo_title'] = $data['seller']->store_name;
		$this->view('seller', $data);
	}

	function add_review(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('rating_item', 'Item as Described Rating', 'numeric|required');
		$this->form_validation->set_rules('rating_communication', 'Communication Rating', 'numeric|required');
		$this->form_validation->set_rules('rating_time', 'Shipping Time Rating', 'numeric|required');
		$this->form_validation->set_rules('rating_charges', 'Shipping Charges Rating', 'numeric|required');
		$this->form_validation->set_rules('review', 'Review', 'trim|required');
		
		$merchant_id = $this->input->post('id');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {
			$customer	= $this->go_cart->customer();

			if(isset($customer['id'])){
				$save['merchant_id'] 			= $merchant_id;
				$save['rating_item']  			= $this->input->post('rating_item');
				$save['rating_communication']  	= $this->input->post('rating_communication');
				$save['rating_time']  			= $this->input->post('rating_time');
				$save['rating_charges']  		= $this->input->post('rating_charges');
				$save['review']  				= $this->input->post('review');
				$save['customer_id']  			= $customer['id'];
				$save['created_at'] 			= date('Y-m-d H:i:s',time());
				
				$this->Seller_model->add_review($save);
				$this->session->set_flashdata('message', 'Your review has been submitted.');
			} else {
				$this->session->set_flashdata('error', 'You must login to submit a review.');
			}	
		}
		redirect(site_url('seller/info/'.$this->Seller_model->get_seller_slug($merchant_id)));
	}

	function shop($slug=''){
		if($slug==''){
			redirect(site_url());
		}
		
		$data['seller'] = $this->Seller_model->get_seller_by_slug($slug);
		$id = $data['seller']->id;
				
		if (!$data['seller'] || $data['seller']->enabled==0)
		{
			show_404();
		}
				
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['seller']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$data['seo_title']	= $data['seller']->store_name;
		$data['page_title']	= $data['seller']->store_name;
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'products.name', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$brands = array();
		$filters['brand_checked'] = array();
		$filters['category'] = 0;

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['brand'])){
			$filters['brand_checked'] = explode(',',urldecode($_GET['brand']));
		}
		if(isset($_GET['category'])){
			$filters['category'] = $_GET['category'];
		}
		
		$brands = $this->Product_model->get_merchant_products_brands($data['seller']->id,true);

		$config['per_page']		= 24;
		
		$data['products']	= $this->Product_model->get_merchant_products($data['seller']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		
		$product_count = $this->Product_model->get_merchant_products($data['seller']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);
		$data['total_products']	= $product_count;

		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);

		//$brands_used = array();
		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}

			/*if(in_array($p->brand_id, $brands_used)){
				$brands[$p->brand_id]['count'] = $brands[$p->brand_id]['count'] + 1;
			} else {
				$brands[$p->brand_id] = array('name'=>$p->brand_name, 'count'=>1);
			}
			$brands_used[] = $p->brand_id;*/
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$filters['brands'] = $brands;

		$data['filters'] = $filters;

		$this->view('shop', $data);
	}

}