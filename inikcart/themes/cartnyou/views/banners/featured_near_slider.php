<?php
if(!empty($banners)){
    foreach($banners as $banner):
        if($banner->link){
            $target=false;
            if($banner->new_window){
                $target=' target="_blank"';
            }
            echo '<a href="'.$banner->link.'"'.$target.'>';
        }

        $banner_name = '';
        if($banner->name):
            $banner_name = $banner->name;
        endif;
            echo '<img class="" alt="'.$banner_name.'" src="'.base_url('uploads/'.$banner->image).'" title="'.$banner_name.'" />';
        if($banner->link){
            echo '</a>';
        }
    endforeach;
}
?>