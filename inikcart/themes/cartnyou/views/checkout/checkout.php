<div class="columns-container pdt-129-header">

    <div class="container" id="columns">

        <h2 class="page-heading no-line">
            <span class="page-heading-title2"> Checkout</span>
        </h2>

        <div class="page-content page-order">
            <div class="row">

                <div class="col-md-12">
                    <div class="alert alert-info cpal"  id="cart-notifiation-coupon">

                    </div>

                    <?php if ($this->session->flashdata('message')):?>
                        <div class="alert alert-info">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    <?php endif;?>

                    <?php if ($this->session->flashdata('error')):?>
                        <div class="alert alert-danger">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php endif;?>

                    <?php if (!empty($error)):?>
                        <div class="alert alert-danger">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $error;?>
                        </div>
                    <?php endif;?>

                    <?php if (validation_errors()):?>
                        <div class="alert alert-danger">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php echo validation_errors();?>
                        </div>
                    <?php endif;?>
                </div>

                <div class="col-md-8">
                    <h3 class="checkout-h f-500">Billing Address
                        <?php if(!$this->Customer_model->is_logged_in(false,false)){ ?>
                            <span class="chkspan">Already have account?
                                <a data-toggle="modal" data-target="#loginModal" href="javascript:;" class="colorBlue f-600">Login</a>
                            </span>
                        <?php } else { ; ?>
                            <a href="javascript:;" rel="0" class="colorBlue f-600 edit_address pull-right">Add Address</a>
                        <?php } ;?>

                    </h3>

                    <?php if($this->Customer_model->is_logged_in(false,false)): ?>
                        <div class="address" id="checkout-address-list">

                            <?php
                            $c = 1;
                            if(!empty($customer_addresses)):
                                foreach($customer_addresses as $a):?>
                                    <?php
                                    $b  = $a['field_data'];
                                    ?>
                                    <label class=" address-label w-100 <?php if(isset($selected_address['id'])){ echo ($selected_address['id']==$a['id'])?'selected':'';}?>" id="addr-<?php echo $a['id']?>">
                                        <div class="col-md-8 col-sm-8 col-xs-12 p0">
                                            <strong class="colorBlack"><?php echo $b['firstname'].' '.$b['lastname'];?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="mrl-20 colorBlack"><?php echo $b['phone'];?></span></strong>
                                            <br>
                                            <span class="pdl-15">
                                                <?php echo nl2br(format_address($b,false,true));?>
                                            </span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 mdsmc p0 mtb-10">
                                            <a class="btn btn-default width-50 pull-right edit_address radius-0 option-button" rel="<?php echo $a['id'];?>">Edit</a>
                                            <a class="btn btn-red width-50 pull-right select-address option-button" data-id="<?php echo $a['id'];?>" onclickAbon="populate_address(<?php echo $a['id'];?>);">Deliver Here</a>
                                        </div>
                                    </label>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                            <?php endif; ?>

                            <?php if(isset($customer['id'])){ ?>

                            <?php } else { ?>

                                <!-- CUSTOM HIDDEN -->
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        <?php
                                        // Restore previous selection, if we are on a validation page reload
                                        $zone_id = set_value('zone_id');

                                        echo "\$('#zone_id').val($zone_id);\n";
                                        ?>
                                    });
                                </script>
                                <script type="text/javascript">
                                    function populate_zone_menu(value)
                                    {
                                        $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:99}, function(data) {
                                            $('#zone_id').html(data);
                                        });
                                    }
                                </script>

                            <?php
                            if(!empty($customer['bill_address']['country_id']))
                            {
                                $zone_menu  = $this->Location_model->get_zones_menu($customer['bill_address']['country_id']);
                            }
                            else
                            {
                                $zone_menu = array(''=>'--Select State--')+$this->Location_model->get_zones_menu(99);
                            }

                            //form elements

                            $address1   = array('placeholder'=>'Flat House No., Building, Company, Apartment', 'class'=>'address col-md-8 form-control', 'name'=>'address1', 'value'=> set_value('address1', @$customer['bill_address']['address1']));
                            $address2   = array('placeholder'=>'Area, Colony, Street, Sector, Village', 'class'=>'address col-md-8 form-control', 'name'=>'address2', 'value'=>  set_value('address2', @$customer['bill_address']['address2']));
                            /*$landmark   = array('placeholder'=>'Landmark: e.g.: Near JLN Stadium', 'class'=>'address col-md-8 form-control', 'name'=>'landmark', 'value'=>  set_value('landmark', @$customer['bill_address']['address2']));*/
                            $first      = array('placeholder'=>lang('address_firstname'), 'class'=>'address col-md-4 form-control', 'name'=>'firstname', 'value'=>  set_value('firstname', @$customer['bill_address']['firstname']));
                            $last       = array('placeholder'=>lang('address_lastname'), 'class'=>'address col-md-4 form-control form-control', 'name'=>'lastname', 'value'=>  set_value('lastname', @$customer['bill_address']['lastname']));
                            $email      = array('placeholder'=>lang('address_email'), 'class'=>'address col-md-4 form-control', 'name'=>'email', 'value'=> set_value('email', @$customer['bill_address']['email']));
                            $phone      = array('id'=>'f_phone','placeholder'=>lang('address_phone_placeholder'), 'maxlength'=>'10', 'class'=>'address col-md-4 form-control', 'name'=>'phone', 'value'=> set_value('phone', @$customer['bill_address']['phone']));
                            $city       = array('placeholder'=>lang('address_city'), 'class'=>'address col-md-4 form-control', 'id'=>'f_city', 'name'=>'city', 'value'=> set_value('city', @$customer['bill_address']['city']));
                            $zip        = array('placeholder'=>lang('address_zip_placeholder'), 'maxlength'=>'6', 'id'=>'f_zip', 'class'=>'address col-md-4 form-control', 'name'=>'zip', 'value'=> set_value('zip', @$customer['bill_address']['zip']));

                            ?>

                                <div class="bkgWhite">

                                    <form id="guest-address-form" method="POST" action="<?php echo site_url('checkout/select_address');?>" class="clearfix">

                                        <div class="alert alert-danger" style="display: none;" id="guest-address-notifiation">

                                        </div>

                                        <div class="checkout_form_input first_name col-sm-12">
                                            <label><?php echo lang('address_firstname');?> <span class="color_red">*</span></label>
                                            <?php echo form_input($first);?>
                                        </div>

                                        <div class="checkout_form_input last_name col-sm-6 hidden">
                                            <label><?php echo lang('address_lastname');?> <span class="color_red">*</span></label>
                                            <?php echo form_input($last);?>
                                        </div>

                                        <div class="checkout_form_input phone col-sm-6">
                                            <label>Mobile Number <span class="color_red">*</span></label>
                                            <?php echo form_input($phone);?>
                                        </div>

                                        <div class="checkout_form_input last E-mail col-sm-6">
                                            <label><?php echo lang('address_email');?> <span class="color_red">*</span></label>
                                            <?php echo form_input($email);?>
                                        </div>

                                        <hr class="clear">
                                        <div class="checkout_form_input2 adress col-sm-6">
                                            <label><?php echo lang('address');?><span class="color_red">*</span></label>
                                            <?php echo form_input($address1);?>
                                        </div>

                                        <div class="checkout_form_input2 last adress col-sm-6">
                                            <label>Address Line 2</label>
                                            <?php echo form_input($address2);?>
                                        </div>
                                        <div  class="checkout_form_input2 last adress col-sm-6">
                                            <label>Landmark</label>
                                            <input type="text" name="landmark" value="" placeholder="Landmark: e.g.: Near JLN Stadium" class="address col-md-8 form-control">
                                        </div>
                                        <div class="checkout_form_input last postcode col-sm-6">
                                            <label><?php echo lang('address_zip');?> <span class="color_red">*</span></label>
                                            <?php echo form_input($zip);?>
                                        </div>
										<div class="checkout_form_input sity col-sm-6">
                                            <label><?php echo lang('address_city');?> <span class="color_red">*</span></label>
                                            <?php echo form_input($city);?>
                                        </div>
										
                                        <div class="checkout_form_input country col-sm-6">
                                            <label><?php echo lang('address_state');?> <span class="color_red">*</span></label>
                                            <?php echo form_dropdown('zone_id',$zone_menu, @$customer['bill_address']['zone_id'], 'id="zone_id" class="address basic form-control" ');?>
                                        </div>
										<div class="checkout_form_input country col-sm-12">
                                            <label><?php echo lang('address_type');?> <span class="color_red">*</span></label>
                                            <?php echo form_dropdown('address_type',array('Home'=>'Home ( Delivery between 7 AM to 10 PM )','Work'=>'Work ( Delivery between 9 AM to 5 PM )'), @$customer['bill_address']['address_type'], 'id="address_type" class="address basic form-control" ');?>
                                            <?php //echo form_radio(array('name'=>'address_type', 'value'=>'yes', 'id'=>'address_type', 'checked'=>'yes'));?>
                                        </div>
                                        

                                        <div class="checkout_form_input hidden col-sm-6">
                                            <div class="pull-left">
                                                <?php echo form_checkbox(array('name'=>'use_shipping', 'value'=>'yes', 'id'=>'use_shipping', 'checked'=>'yes')) ?>
                                                <label for="use_shipping"><?php echo lang('ship_to_address') ?></label>
                                            </div>
                                        </div>

                                        <div class="checkout_form_input country hidden col-sm-6">
                                            <label><?php echo lang('address_country');?> <span class="color_red">*</span></label>
                                            <?php echo form_dropdown('country_id',array('99'=>'India'), '99', 'id="country_id" class="address basic form-control"');?>
                                        </div>


                                        <div class="clear"></div>

                                        <div class="checkout_form_input2 col-sm-6">
                                            <div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div>

                                            <input class="btn btn-red pull-right mtb-10" type="submit" value="<?php echo lang('form_continue');?>"/>
                                        </div>
                                    </form>
                                </div>
							<script>
							
							
							$("#f_phone").on('keyup keypress', function() {
										$('.error_validate').remove();
										var phone=$(this).val();
										if(Math.floor(phone) == phone && $.isNumeric(phone)){ 
											if(phone.length != 10) {
												$('#f_phone').after('<div class="error_validate" style="color:#FF0000">The length of the mobile no. should be 10</div>');
											}
										}else{
											$('#f_phone').after('<div class="error_validate" style="color:#FF0000">'+(phone.length==0?"This Field is required":"Invalid Number")+'</div>');
										}
									});
							
							
							</script>
                            <?php } ?>


                            <div class="">
                                <div id="addAddress">

                                </div>
                            </div>
                            <div class="">
                                <div class="paymentWrapper">
                                    <h3 class="checkout-h f-500">Choose Payment Method


                                    </h3>
                                    <div>
                                        <div id='payment-methods'></div>
                                    </div>
                                </div>
                            </div>







                </div>
                <div class="col-md-4">
                    <div>
                        <div id="cart-summary-total-container">

                        </div>
                    </div>

                    <?php //echo form_open('cart/apply_coupon', array('id'=>'update_coupon_form','style'=>'display:block;'));?>

                        <!-- <input class="btn hidden" type="submit" value="<?php echo lang('form_update_cart');?>"/>
                        <input type="hidden" name="redirect" value="ajax"/>

                        <div class="coupon_form input-group pull-left col-md-12">
                            <input class="form-control" type="text" name="coupon_code" placeholder="Enter Coupon Code" />
                            <span class="input-group-addon cinput"><input value="Apply" class="btn btn-red" type="submit"></span>
                        </div>

                        <div class="alert alert-info" style="display: block;" id="cart-notifiation-coupon">

                        </div>
                    </form> -->

                   <!-- <div class="cart_navigation fl-n">

                        <div class="checkbox" style="margin-top: 40px;">
                            <label style="padding-left: 0;"><input type="checkbox" value="true" id="checkbox-agree" checked="checked"> I agree to <a class="blue-link" target="_blank" href="<?php /*echo site_url('terms-and-conditions');*/?>">Terms and conditions</a></label>
                        </div>

                        <a class="next-btn btn-block text-center fl-n" id="place-order-btn" href="javascript:;">Place Order</a>
                    </div>-->



                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="address-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="address-form-container">

        </div>
    </div>
</div>

<div class="modal fade" id="otpVerificationGuestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Enter Verification Code</h4>
            </div>
            <div class="modal-body">
                <div class="row lg">
                    <div class="col-md-12">
                        <div class="text-center" style="margin-bottom: 15px;">Since you are placing a COD order, a verification code has been sent to your mobile number.</div>
                    	<form id="otp-verification-guest-form" type="POST" class="form-horizontal">
                    		<div class="form-group">
                    			<label class="col-md-4 control-label" for="otp-verification-guest-form-phone">OTP</label>
                    			<div class="col-md-8">
                    				<input name="otp" id="otp-verification-guest-form-phone" placeholder="" class="form-control input-md" type="text">
                    				<!--  |
                                    <a href="javascript:;" id="change-otp-guest-btn">Change Number</a> -->
                    			</div>
                    		</div>

                    		<div class="form-group">
                    			<div class="col-md-8 col-md-offset-4">
                    				<button type="submit" class="btn btn-primary">Submit</button>

                    				<div style="margin-top: 5px;"><a href="javascript:;" id="resend-otp-guest-btn">Resend OTP</a></div>

                                    <div id="guest-opt-resent-msg" style="margin-top: 5px;color:#009966;"></div>
                    			</div>
                    		</div>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($this->Customer_model->is_logged_in(false,false)): ?>
    <script>
        $(function(){
            $('#selected-login-info').html('<?php echo $this->Customer_model->logged_customer_first_name();?> - <?php echo $this->Customer_model->logged_customer_email();?>');
            $('#login-change-btn').show();

            $("#collapseZero").collapse("hide");
            $("#collapseOne").collapse("show");
        });
    </script>
<?php endif; ?>

<script>
$(function(){
    $("#f_zip").on('keyup keypress', function() {
        if($(this).val().length == 6) {
            $.post('<?php echo site_url('locations/getZip');?>',{zip:$('#f_zip').val()}, function(data) {
                data = JSON.parse(data);
                if(data.success){
                  $('#f_city').val(data.data.city);
                  $('#zone_id').val(data.data.zone_id);
                }
            });
        }
    });
});
</script>

<script>

$(function(){

    function goto_address(userDetailString){
        $('#selected-login-info').html(userDetailString);
        $('#login-change-btn').show();

        $("#collapseZero").collapse("hide");
        $("#collapseOne").collapse("show");
    }

    $('.vertical-menu-content').css('display','none');

    $('#guest-checkout-btn').on('click',function(){
        goto_address('Guest');
    });

    function get_cart_summary(){
        $.post('<?php echo site_url('cart/get_cart_summary');?>', function(data) {
            $('#summary-container').html(data);
            get_cart_summary_total();
        });
    }

    function get_cart_summary_total(){
        $.post('<?php echo site_url('cart/get_cart_summary_total');?>', function(data) {
            $('#cart-summary-total-container').html(data);
        });
    }
    get_cart_summary_total();

    $('#update_cart_form').on('submit',function(e){
        e.preventDefault();
        $('#cart-notifiation').hide();
        var ele = $('#update_cart_form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(data){
            data = JSON.parse(data);
            if(data.success){
                if(data.data !== false){
                    if(data.data.message != ""){
                        $('#cart-notifiation').html(data.data.message);
                        alertify.alert(data.data.message);
                    } else if(data.data.error != ""){
                        $('#cart-notifiation').html(data.data.error);
                        alertify.alert(data.data.error);
                    }
                    $('#cart-notifiation').show();
                }

                get_cart_summary();
            } else {
                alertify.alert('Something went wrong!');
            }
        });
    });

    $(document).on('submit','#update_coupon_form', function(e){
        e.preventDefault();
        $('#cart-notifiation-coupon').hide();
        var ele = $('#update_coupon_form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(data){
            data = JSON.parse(data);
            if(data.success){
                if(data.data !== false){
                    if(data.data.message != ""){


                        $('#cart-notifiation-coupon').html(data.data.message);
                        $('#cart-notifiation-coupon').show();
                        alertify.alert(data.data.message);
                    } else if(data.data.error != ""){

                        $('#cart-notifiation-coupon').html(data.data.error);
                        $('#cart-notifiation-coupon').show();
                        alertify.alert(data.data.error);
                    }
                    $('#cart-notifiation-coupon').show();
                }
                else{

                    $('#cart-notifiation-coupon').html("There was no coupon with that code. Check to make sure you entered it correctly.");
                    $('#cart-notifiation-coupon').show();
                }
                get_cart_summary();
            } else {
                alertify.alert('Something went wrong!');
            }
        });
    });

    $('.edit_address').on('click', function(){
        $.post('<?php echo site_url('secure/address_form'); ?>/'+$(this).attr('rel'),
            function(data){
                $('#address-form-container').html(data);
                $('#address-form-modal').modal('show');
                //$('#addAddress').html(data);
            }
        );
    });
    $(document).on('click','#closeAddress',function(){
        $('#addAddress').html('');
    });

    $('.select-address').on('click', function(){
        $('#addAddress').html('');
        var address_id = $(this).attr('data-id');
        $.post('<?php echo site_url('checkout/select_address');?>',{id:address_id}, function(data) {
            data = JSON.parse(data);
            if(data.success){
                $('#selected-address').html(data.address_string);
                $('.address-label').removeClass('selected');
                $('#addr-'+data.address_id).addClass('selected');
                goto_summary();
            } else {
                alertify.alert(data.data);
            }
        });

    });

    $('#guest-address-form').on('submit',function(e){
        e.preventDefault();
        $('#guest-address-notifiation').hide();
        var ele = $('#guest-address-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(data){
            data = JSON.parse(data);
            if(data.success){
                $('#selected-address').html(data.address_string);

                //verify otp  dont go to summary
                //$('#guest-opt-resent-msg').html('');
                //$('#otpVerificationGuestModal').modal('show');

                goto_summary();
            } else {
            	alertify.alert(data.data);
                $('#guest-address-notifiation').html(data.data);
                $('#guest-address-notifiation').show();
            }
        });
    });

    $('#resend-otp-guest-btn').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url : '<?php echo site_url("secure/resend_guest_otp");?>',
            method: 'POST'
        }).done(function(data){
            $('#guest-opt-resent-msg').html('OTP has been resent.');
        });
    });

    $('#change-otp-guest-btn').on('click',function(e){
        $('#otpVerificationGuestModal').modal('hide');
    });

    $('#otp-verification-guest-form').on('submit',function(e){
		e.preventDefault();
        $.ajax({
            data : {
            	id: tempUId,
            	otp : $('#otp-verification-guest-form-phone').val()
            },
            url : '<?php echo site_url("secure/verify_otp_guest");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
            	/*$('#otp-verification-guest-form-phone').val('');
            	document.body.scrollTop = 0;
			    document.documentElement.scrollTop = 0;
            	$('#otpVerificationGuestModal').modal('hide');*/
            	//goto_summary();

                window.location = "<?php echo site_url('checkout/success');?>";

            } else if(getdata.code == 404){
            	alertify.alert(getdata.error);
            } else {
                alertify.alert("Incorrect OTP.");
            }
        });
	});

    function goto_summary(){
        // if() TODO: if backend recheck all set

        get_cart_summary();

        $('#shipping-continue-btn').hide();
        $('#shipping-change-btn').show();

        $("#collapseOne").collapse("hide");
        $("#collapseTwo").collapse("show");
        //$("#collapseThree").collapse("hide");

        /*setTimeout(function(){
            $('html, body').animate({scrollTop:$('#headingTwo').position().top}, 'slow');
        }, 500);*/
    }

    $("#collapseOne").on('show.bs.collapse', function(){
        //$('#shipping-continue-btn').show();
        $('#shipping-change-btn').hide();

        $('#summary-change-btn').hide();
        $('#summary-continue-btn').hide();
    });

    $('#shipping-change-btn').on('click', function(){
        $("#collapseOne").collapse("show");
        $("#collapseTwo").collapse("hide");
        $("#collapseThree").collapse("hide");
    });

    $('#shipping-continue-btn').on('click', function(){
        goto_summary();
    });

    $('#summary-change-btn').on('click', function(){
        $("#collapseOne").collapse("hide");
        $("#collapseTwo").collapse("show");
        $("#collapseThree").collapse("hide");
    });

    $("#collapseTwo").on('show.bs.collapse', function(){
        $('#summary-change-btn').hide();
        $('#summary-continue-btn').show();
    });

    $("#collapseThree").on('show.bs.collapse', function(){
        $('#summary-change-btn').show();
        $('#summary-continue-btn').hide();
    });

    $('#continue-summary, #summary-continue-btn').on('click', function(){
        goto_payment_options();
    });

    function goto_payment_options(){

        $.post('<?php echo site_url('checkout/get_payment_methods');?>', function(data) {
            $('#payment-methods').html(data);
            /*$("#collapseOne").collapse("hide");
            $("#collapseTwo").collapse("hide");
            $("#collapseThree").collapse("show");*/
            //$('#place-order-btn').css('display','block');

            //$('input:radio[name="payment-method-select"]').attr('checked',false);
        });
    }
    goto_payment_options();
    $(document).on('change', 'input:radio[name="payment-method-select"]', function(){
        if (this.checked) {
            var method = this.value;
            $.post('<?php echo site_url('checkout/set_payment_method');?>',{module:method}, function(data) {
                data = JSON.parse(data);
                if(data.success){

                    get_cart_summary_total();
                    
                    $('#place-payment-btn').css('display','block');
                    $('#place-order-btn').css('display','block');
                } else {
                    $('#place-payment-btn').css('display','none');
            		$('#place-order-btn').css('display','none');
                    alertify.alert('An unexpected error occured!');
                }
            });
        }
    });

    function togggle_order_btn(){
        if($("#checkbox-agree").is(':checked') && order_show_btn) {
            $('#place-payment-btn').css('display','block');
            $('#place-order-btn').css('display','block');
        } else {
            $('#place-payment-btn').css('display','none');
            $('#place-order-btn').css('display','none');
        }
    }

    $(document).on('click','#place-payment-btn, #place-order-btn', function(e){
    	e.preventDefault();

        /*$.post('<?php echo site_url('checkout/check_otp_verification');?>', function(data) {
            data = JSON.parse(data);
            if(!data.success){
                alertify.alert(data.data);
            }
        });*/
        
        if($("#checkbox-agree").is(':checked')) {

            // check if otp needed

            $('#otpVerificationGuestModal').modal('show');
            $('#otpVerificationGuestModal > .modal-dialog').css('display','none');

            $.post('<?php echo site_url('checkout/check_otp_verification');?>', function(data) {
                data = JSON.parse(data);
                if(data.success){
                	$('#otpVerificationGuestModal').modal('hide');
                    window.location = "<?php echo site_url('checkout/success');?>";

                } else if(data.hasOwnProperty('data')) {
                    $('#otpVerificationGuestModal').modal('hide');
                    alertify.alert(data.data);
                } else {
                    $('#guest-opt-resent-msg').html('');
                    $('#otpVerificationGuestModal > .modal-dialog').css('display','block');
                    //$('#otpVerificationGuestModal').modal('show');
                }
            });
        } else {
            alertify.alert('You must agree to the terms and conditions.');
        }

    });

    /*if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {
	   	var styleEl = document.createElement('style'), styleSheet;
	   	document.head.appendChild(styleEl);
	   	styleSheet = styleEl.sheet;
		styleSheet.insertRule(".modal { position:absolute; bottom:auto; }", 0);
	}*/

});
</script>
