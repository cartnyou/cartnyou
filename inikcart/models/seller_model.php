<?php
Class Seller_model extends CI_Model{
	
	function get_seller($id){
		$this->db->select('merchants.*');
		$this->db->where('id',$id);
		return $this->db->get('merchants')->row();
	}

	function get_seller_by_slug($slug){
		$this->db->select('merchants.*');
		$this->db->where('slug',$slug);
		return $this->db->get('merchants')->row();
	}

	function get_seller_slug($id){
		$this->db->select('merchants.slug');
		$this->db->where('id',$id);
		return $this->db->get('merchants')->row()->slug;
	}

	function add_review($save){
		$this->db->insert('seller_ratings', $save);
	}

	function get_seller_reviews($id){
		$result	= $this->db->select('seller_ratings.sr_id,((rating_item + rating_communication + rating_time + rating_charges) / 4.0) AS rating,review,created_at,customers.id AS customer_id,firstname,lastname')->where('sr_enabled',1)->where('seller_ratings.merchant_id',$id)->join('customers','customers.id=seller_ratings.customer_id')->get('seller_ratings')->result();
		if(!$result){
			return false;
		}
		return $result;
	}

	function get_seller_avg_rating($seller_id,$rating_only=false){
		if($rating_only){
			$this->db->select('((AVG(rating_item) + AVG(rating_communication) + AVG(rating_time) + AVG(rating_charges)) / 4.0) AS rating_seller');
		} else {
			$this->db->select('((AVG(rating_item) + AVG(rating_communication) + AVG(rating_time) + AVG(rating_charges)) / 4.0) AS rating_seller, COUNT(sr_id) AS count');
		}
		$this->db->where('sr_enabled',1);
		$result	= $this->db->where('merchant_id',$seller_id)->get('seller_ratings')->row();
		if(!$result){
			return false;
		}
		if($rating_only){
			return $result->rating_seller;
		} else {
			return $result;
		}
	}

	function get_seller_split_rating($seller_id,$rating_only=false){
		$this->db->select('AVG(rating_item) as rating_item_avg, AVG(rating_communication) as rating_communication_avg, AVG(rating_time) as rating_time_avg, AVG(rating_charges) as rating_charges_avg');
		$this->db->where('sr_enabled',1);
		$result	= $this->db->where('merchant_id',$seller_id)->get('seller_ratings')->row();
		return $result;
	}

}