<?php

class Dashboard extends Merchant_Controller {

	function __construct()
	{
		parent::__construct();

		if($this->auth->check_access_merchant('Orders'))
		{
			redirect($this->config->item('merchant_folder').'/orders');
		}
		
		$this->load->model('Order_model');
		$this->load->helper(array('formatting','date'));
		
		$this->lang->load('dashboard');
	}
	
	function index()
	{
		$merchant_id 		= $this->session->userdata('merchant')['id'];
		$data['active_nav'] = 'dashboard';
		$data['page_title']	=  lang('dashboard');
		
		$this->load->model('Product_model');

		// get 5 latest orders
		$data['orders']					= $this->Order_model->get_orders(false, '' , 'DESC', 5);

		$data['lifetime_sales']			= $this->Order_model->get_lifetime_sales($merchant_id);
		$data['total_orders']			= $this->Order_model->get_total_orders($merchant_id);

		$data['total_products_value']	= $this->Product_model->get_total_products_value($merchant_id);
		$data['pending_orders_count']	= $this->Order_model->get_pending_orders(false,'','DESC',0,0,$merchant_id,true);

		$data['popular_products']		= $this->Product_model->merchant_popular_products(10,false,false,$merchant_id);
		$data['most_viewed']			= $this->Product_model->most_viewed(7,false,false,$merchant_id);

		$data['recent_orders']			= $this->Order_model->get_orders(false,'order_number','desc',10,0,$merchant_id);
		$data['pending_orders']			= $this->Order_model->get_pending_orders(false,'order_number','desc',10,0,$merchant_id);

		$this->view($this->config->item('merchant_folder').'/dashboard', $data);
	}

	function get_gross_monthly_sales($year=false){
		$merchant_id 		= $this->session->userdata('merchant')['id'];

		$return 	= array();
		if($year){
			$dataset 	= $this->Order_model->get_gross_monthly_sales($year,$merchant_id);
			if(!empty($dataset)){
				// previous months
				if($dataset[0]->month != 1){
					$i = 1;
					while ( $i < $dataset[0]->month) {
						$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
						$i++;
					}
				}

				foreach ($dataset as $key => $row) {
					if($key != 0){
						if(($row->month - $dataset[$key-1]->month) > 1) {
							$i = $dataset[$key-1]->month + 1;
							while ( $i < $row->month) {
								$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
								$i++;
							}
						}
					}
					$return[] = array( date('M', mktime(0, 0, 0, $row->month, 10)) , $row->total);
				}

				// last months
				if($year < date('Y')){
					if(end($dataset)->month != 12){
						$i = end($dataset)->month + 1;
						while ( $i <= 12) {
							$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
							$i++;
						}
					}
				}
			}
		}
		die(json_encode($return));
	}

	function get_gross_monthly_orders($year=false){
		$merchant_id 		= $this->session->userdata('merchant')['id'];

		$return 	= array();
		if($year){
			$dataset 	= $this->Order_model->get_gross_monthly_orders($year,$merchant_id);
			if(!empty($dataset)){
				
				// previous months
				if($dataset[0]->month != 1){
					$i = 1;
					while ( $i < $dataset[0]->month) {
						$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
						$i++;
					}
				}

				foreach ($dataset as $key => $row) {
					if($key != 0){
						if(($row->month - $dataset[$key-1]->month) > 1) {
							$i = $dataset[$key-1]->month + 1;
							while ( $i < $row->month) {
								$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
								$i++;
							}
						}
					}
					$return[] = array( date('M', mktime(0, 0, 0, $row->month, 10)) , $row->total);
				}

				// last months
				if($year < date('Y')){
					if(end($dataset)->month != 12){
						$i = end($dataset)->month + 1;
						while ( $i <= 12) {
							$return[] = array( date('M', mktime(0, 0, 0, $i, 10)) , 0);
							$i++;
						}
					}
				}

			}
		}
		die(json_encode($return));
	}

	function get_order_stats($year=false){
		$merchant_id 		= $this->session->userdata('merchant')['id'];

		$return 	= array();
		$statuses 	= array();
		if($year){
			$dataset 	= $this->Order_model->get_order_stats($year,$merchant_id);
			if(!empty($dataset)){
				foreach ($dataset as $order) {
					if(in_array($order->latest_status, $statuses)) {
						if(!empty($return)){
							foreach ($return as &$ret) {
								if($ret['label'] == $order->latest_status){
									$ret['data']++;
								}
							}
						}

					} else {
						$return[] = array("label"=> $order->latest_status, "data"=>1);
						$statuses[] = $order->latest_status;
					}
				}
			}
		}
		die(json_encode($return));
	}

	function contact()
	{
		$data['active_nav'] = 'dashboard';
		$data['page_title']	=  'Edit Profile';
		
		$this->view($this->config->item('merchant_folder').'/contact', $data);
	}

}