<?php
Class Shipping_model extends CI_Model{
	function __construct(){
			parent::__construct();
	}
	
	function get_amount($merchant_id){
		$result = $this->db->query('SELECT COALESCE(SUM(amount), 0) AS amount FROM shipping_transactions WHERE merchant_id="'.$merchant_id.'"');
		return $result->row()->amount;
	}

	function get_transactions($data,$count=false, $type=false){
		$this->db->where('merchant_id', $data['merchant_id']);

		if(!empty($data['rows'])){
			$this->db->limit($data['rows']);
		}
		
		if(!empty($data['page'])){
			$this->db->offset($data['page']);
		}

		if($type == 'recharge'){
			$this->db->where('amount > ',0);
		} else if ($type == 'deduction') {
			$this->db->where('amount < ',0);
		}
		
		$this->db->order_by('created_at','desc');

		if($count){
			return $this->db->count_all_results('shipping_transactions');
		} else {
			$result	= $this->db->get('shipping_transactions');
			return $result->result();
		}
	}
	
	function add_transaction($save){
		$insert	= array('merchant_id'=>$save['merchant_id'], 'txn_id'=>$save['txn_id'], 'order_id'=>$save['order_id'], 'weight'=>$save['weight'],'amount'=>$save['amount'],'created_at'=>$save['created_at'],'updated_at'=>$save['updated_at']);
		$this->db->insert('shipping_transactions', $insert);
		return $this->db->insert_id();
	}
	
	//delete any settings having to do with this particular code
	function delete_settings($code)
	{
		$this->db->where('code', $code);
		$this->db->delete('settings');
	}
	
	//this deletes a specific setting
	function delete_setting($code, $setting_key)
	{
		$this->db->where('code', $code);
		$this->db->where('setting_key', $setting_key);
		$this->db->delete('settings');
	}

	function get_custom_shipping_list($rows=false, $page=false, $count=false){
		if($count){
			return $this->db->count_all_results('pincodes_custom');
		} else {
			$this->db->distinct();
			$this->db->select('company,company_code,count(id) AS pincodes');
			$this->db->group_by('company_code');
			if($rows){
				$this->db->limit($rows);
			}
			if($page){
				$this->db->offset($page);
			}

			$this->db->order_by('id','desc');
			$result	= $this->db->get('pincodes_custom');
			return $result->result();
		}
	}

	function custom_delete($id)
    {
        $this->db->where('company_code', $id);
        $this->db->delete('pincodes_custom');
    }
}