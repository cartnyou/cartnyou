<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Order History</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

        	<div class="column col-xs-12 col-sm-12">
        		<?php if ($this->session->flashdata('message')):?>
		            <div class="alert alert-info">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('message');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if ($this->session->flashdata('error')):?>
		            <div class="alert alert-danger">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('error');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if(validation_errors()):?>
		        	<div class="alert alert-danger">
		        		<a class="close" data-dismiss="alert">×</a>
		        		<?php echo validation_errors();?>
		        	</div>
		        <?php endif;?>
        	</div>

            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module mt-40">
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li ><span></span><a href="<?php echo site_url('secure/my-account');?>">Profile</a></li>
                                    <li class="active"><span></span><a href="<?php echo site_url('secure/orders');?>">Orders</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/addresses');?>">My Address</a></li>
                                    <li><span></span><a href="<?php echo site_url('cart/wishlist');?>">My Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- Banner silebar -->

                <!-- ./Banner silebar -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My Orders</span>
                </h2>
                <!-- Content page -->

                <?php
                if($orders):
                    foreach($orders as $order):
                ?>
                        <div class="orderBox mt-27">
                            <div class="row pdlr-20 borderB orderHeader">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <p class="mb-5">
                                        <a href="<?php echo site_url().'secure/order/'.$order->order_number;?>" class="linkSt white valignmiddle"><b>Order Id: #<?php echo $order->order_number;?></b></a>
                                        <a href="<?php echo site_url().'secure/order/'.$order->order_number;?>" class="btn btn-red-invert pull-right">Details</a>
                                    </p>
                                    
                                </div>
                            </div>

                            <?php
                            if(!empty($order->items)){
                                foreach ($order->items as $item) {
                                ?>
                                
                                    <div class="row ptb10 pdlr-20 borderB">
                                        <div class="col-md-2">
                                            <div class="cart-product text-center">
                                                <a href="<?php echo site_url().$item['slug'];?>" target="_blank">
                                                    <?php
                                                    $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                                                    $item['images'] = array_values((array)json_decode($item['images']));
                                            
                                                    if(!empty($item['images'][0]))
                                                    {
                                                        $primary    = $item['images'][0];
                                                        foreach($item['images'] as $photo)
                                                        {
                                                            if(isset($photo->primary))
                                                            {
                                                                $primary    = $photo;
                                                            }
                                                        }

                                                        $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" />';
                                                    }
                                                    echo $photo;
                                                    ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <a href="<?php echo site_url().$item['slug'];?>" target="_blank"><?php echo ucwords($item['name']);?></a><br>

                                            <?php
                                            if(isset($item['options']))
                                            {
                                                foreach($item['options'] as $name=>$value)
                                                {
                                                    $name = explode('-', $name);
                                                    $name = trim($name[0]);
                                                    if(is_array($value))
                                                    {
                                                        echo '<p>'.$name.':<br/>';
                                                        foreach($value as $item)
                                                        {
                                                            echo '- '.$item.'<br/>';
                                                        }   
                                                        echo "</p>";
                                                    }
                                                    else
                                                    {
                                                        echo '<p>'.$name.': '.$value.'</p>';
                                                    }
                                                }
                                            }
                                            ?>
                                            <p>Seller: <a target="_blank" href="<?php echo site_url().'seller/info/'.$item['merchant_slug'];?>" class="linkSt"><?php echo $item['store_name'];?></a></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p><?php echo format_currency($item['price']);?></p>
                                            <?php
                                            if($item['quantity'] > 1){
                                                echo '<p><b>Quantity:</b> '.$item['quantity'].'</p>';
                                            }
                                            ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <p class="pull-right"><?php if(!empty($item['latest_status'])){ echo $item['latest_status']->ots_status;} ?></p>
                                        </div>
                                    </div>

                            <?php
                                }
                            }
                            ?>

                            <div class="row pdlr-20 orderFooter">
                                <div class="col-md-4 col-sm-6 col-xs-8">
                                    <p>Ordered On: 
                                        <?php
                                        $d = format_date($order->ordered_on); 
                                        $d = explode(' ', $d);
                                        echo $d[1].' '.$d[0].', '.$d[3];
                                        ?>
                                    </p>
                                </div>
                                <div class="col-md-4 col-xs-12 pull-right">
                                    <p class="pull-right"><strong>Order Total</strong>: <?php echo format_currency($order->total);?></p>
                                </div>
                            </div>
                        </div>

                    <?php
                    endforeach;
                    echo '<div class="sortPagiBar">';
                    echo $orders_pagination;
                    echo '</div>';
                else:
                    echo lang('no_order_history');
                endif;
                ?>

                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>
    $(function(){
        $('.vertical-menu-content').css('display','none');
    })
</script>