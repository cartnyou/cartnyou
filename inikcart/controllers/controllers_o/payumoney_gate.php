<?php

class payumoney_gate extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->add_package_path(APPPATH.'packages/payment/payumoney/');
		//$this->load->library(array('Payumoney','go_cart'));
		$this->load->library(array('go_cart','Payumoney'));
		$this->load->helper('form_helper');
	}
	

	function index()
	{
		//we don't have a default landing page
		redirect('');
		
	}

	/*function payment_process(){
		$args = $this->session->userdata("paymentDataArray");
		$this->load->view('payment_process', array('data'=>$args));
		$this->session->unset_userdata("paymentDataArray");
	}*/
		
	/* 
	   Receive postback confirmation from payumoney
	   to complete the customer's order.
	*/
	function payumoney_return()
	{
		$settings = $this->go_cart->CI->Settings_model->get_settings('payumoney');

		if ($this->go_cart->CI->input->post('order_number'))
		{
			foreach ($this->go_cart->CI->input->post() as $k => $v)
			{
				$response[$k] = $v;
			}
		}
		elseif ($this->go_cart->CI->input->get('order_number'))
		{
			foreach ($this->go_cart->CI->input->get() as $k => $v)
			{
				$response[$k] = $v;
			}
		}

		//payumoney breaks the hash on demo sales, we must do the same here so the hashes match.
		if (isset($response['demo']))
		{
			if ( $response['demo'] == 'Y' )
			{
				$response['order_number'] = 1;
			}
		}

		//Create hash
		$our_hash = strtoupper(md5($settings['secret'] . $settings['sid'] . $response['order_number'] . $response['total']));

		//Compare hashes to check the validity of the sale and print the response
		if ($our_hash == $response['key'])
		{            
			// The transaction is good. Finish order
			
			// set a confirmed flag in the inikcart payment property
			$this->go_cart->set_payment_confirmed();
			
			// send them back to the cart payment page to finish the order
			// the confirm flag will bypass payment processing and save up
			redirect('checkout/success/');
		}
		else
		{
			// Possible fake request; was not verified by payumoney. Could be due to a double page-get, should never happen under normal circumstances
			$this->session->set_flashdata('message', "<div>Payumoney did not validate your order. Either it has been processed already, or something else went wrong. If you believe there has been a mistake, please contact us.</div>");
			redirect('checkout');
		}
	}
	
	/* 
		Customer cancelled payumoney payment
		
	*/
	function payumoney_cancel()
	{
		//make sure they're logged in if the config file requires it
		if($this->config->item('require_login'))
		{
			$this->Customer_model->is_logged_in();
		}
	
		// User canceled using payumoney, send them back to the payment page
		$cart  = $this->session->userdata('cart');	
		$this->session->set_flashdata('message', "<div>Payumoney transaction canceled.</div>");
		redirect('checkout');
	}

}