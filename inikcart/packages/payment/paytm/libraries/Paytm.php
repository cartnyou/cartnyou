<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paytm
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->lang->load('paytm');
		
		$this->method_name	= lang('paytm');
	}
	
	/*
	checkout_form()
	this function returns an array, the first part being the name of the payment type
	that will show up beside the radio button the next value will be the actual form if there is no form, then it should equal false
	there is also the posibility that this payment method is not approved for this purchase. in that case, it should return a blank array 
	*/
	
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get_settings('paytm');
		$enabled	= $settings['enabled'];
		
		$form			= array();
		if($enabled)
		{
			$form['name'] = $this->method_name;
			
			$form['form'] = $this->CI->load->view('paytm_checkout', array(), true);
			
			return $form;
			
		} else return array();
		
	}
	
	
	function checkout_check()
	{
		// Nothing to check in this module
		return false;
	}
	
	function description()
	{
		return lang('paytm');
	}
	
	//back end installation functions
	function install()
	{
		
		$config['merchant_key'] = '';
		$config['merchant_salt'] = '';;
		//$config['currency'] = 'INR'; // default
		
		$config['enabled'] = "0";
		
		//not normally user configurable
		$config['return_url'] = "checkout/paytm_return";
		$config['cancel_url'] = "checkout/paytm_cancel";

		$this->CI->Settings_model->save_settings('paytm', $config);
	}
	
	function uninstall()
	{
		$this->CI->Settings_model->delete_settings('paytm');
	}

	//payment processor
	function process_payment(){
		if ( $settings = $this->CI->Settings_model->get_settings('paytm') ) {
			return false;
		} else {
			return true;
		}
	}
	
	//payment processor
	function complete_payment($args)
	{
		//print_r($args);exit;
		$customer = $this->CI->go_cart->customer();

		if ( $settings = $this->CI->Settings_model->get_settings('paytm') ) 
		{
			$PAYU_BASE_URL = 'https://securegw.paytm.in/theia/processTransaction';

			$merchant_key 				= $settings['merchant_key'];
			
			if(
			          empty($args['order_id'])
			) {
			  	// failure;
			    return lang('paytm_error');

			} else {
				$checkSum = "";
				$paramList = array();

				$ORDER_ID = $args["order_id"];
				$CUST_ID = $customer["id"];
				$INDUSTRY_TYPE_ID = $settings['industry_type_id'];
				$CHANNEL_ID = $settings['channel_id'];
				$TXN_AMOUNT = $this->CI->go_cart->total();

				$paramList["MID"] = $settings['mid'];
				$paramList["ORDER_ID"] = $ORDER_ID;
				$paramList["CUST_ID"] = $CUST_ID;
				$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
				$paramList["CHANNEL_ID"] = $CHANNEL_ID;
				$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
				$paramList["WEBSITE"] = $settings['website'];

				$this->CI->load->helper('crypto');
				$args['CHECKSUMHASH'] 	= getChecksumFromArray($paramList, $merchant_key);

				$paramList['action'] 	= $PAYU_BASE_URL;

			    $this->CI->session->set_userdata("paymentDataArray",$paramList);

			    //redirect(site_url('paytm_gate/payment_process'));
			    redirect(site_url('checkout/payment_process'));
			    //redirect(site_url($args['processor_function']));

				//redirect($action.'?'.http_build_query($args));
			}

		}
		else
		{
			return lang('paytm_error');
		}	
	}
	
	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->Settings_model->get_settings('paytm');
		}
		else
		{
			$settings = $post;
		}
		//retrieve form contents
		return $this->CI->load->view('paytm_form', array('settings'=>$settings), true);
	}
	
	function check()
	{	
		$error	= false;

		if($error)
		{
			return $error;
		}
		else
		{
			//we save the settings if it gets here
			$this->CI->Settings_model->save_settings('paytm', $_POST);
			
			return false;
		}
	}
}
