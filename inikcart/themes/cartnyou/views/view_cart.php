<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Your shopping cart</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading no-line">
            <span class="page-heading-title2"> Cart Summary</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content page-order">
            <div class="row">

            	<div class="col-sm-12">
	            	<?php if ($this->session->flashdata('message')):?>
			            <div class="alert alert-info">
			                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
			                <?php echo $this->session->flashdata('message');?>
			            </div>
			        <?php endif;?>

			        <?php if ($this->session->flashdata('error')):?>
			            <div class="alert alert-danger">
			                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
			                <?php echo $this->session->flashdata('error');?>
			            </div>
			        <?php endif;?>

			        <?php if (!empty($error)):?>
			            <div class="alert alert-danger">
			                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
			                <?php echo $error;?>
			            </div>
			        <?php endif;?>
		        </div>

		        <?php if ($this->go_cart->total_items()==0):?>
		        	<div class="col-sm-12">
					    <div class="alert alert-info">
					        <a class="close" data-dismiss="alert">×</a>
					        <?php echo lang('empty_view_cart');?>
					    </div>
				    </div>
				<?php else: ?>
					<?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
		                <div class="col-md-8">
		                    <h3 class="cartCardH3">Products<span class="pull-right"><small><a href="<?php echo site_url();?>">< Continue Shopping</span></a></small></h3>

		                	<?php include('checkout/summary.php');?>

							<input class="btn hidden" type="submit" value="<?php echo lang('form_update_cart');?>"/>
		                </div>
		                <div class="col-md-4">
		                    <h3 class="cartCardH3">Summary</h3>

		                    <div class="cartCard">
			                    <table class="bag_total table table-striped">
									<?php
									/**************************************************************
									Subtotal Calculations
									**************************************************************/
									?>
									<?php if($this->go_cart->group_discount() > 0)  : ?>
						        	<tr class="clearfix">
										<td><?php echo lang('group_discount');?></td>
										<td>-<?php echo format_currency($this->go_cart->group_discount()); ?></td>
									</tr>
									<?php endif; ?>
									<tr class="clearfix">
								    	<td><?php echo lang('subtotal');?></td>
										<td id="gc_subtotal_price"><?php echo format_currency($this->go_cart->subtotal()); ?></td>
									</tr>


									<?php if($this->go_cart->coupon_discount() > 0) {?>
								    <tr class="clearfix">
								    	<td><?php echo lang('coupon_discount');?></td>
										<td id="gc_coupon_discount">-<?php echo format_currency($this->go_cart->coupon_discount());?></td>
									</tr>
									<?php if($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?>
										<tr class="clearfix">
								    		<td><?php echo lang('discounted_subtotal');?></td>
											<td id="gc_coupon_discount"><?php echo format_currency($this->go_cart->discounted_subtotal());?></td>
										</tr>
										<?php
										}
									}

									/**************************************************************
									 Custom charges
									**************************************************************/
									$charges = $this->go_cart->get_custom_charges();
									if(!empty($charges))
									{
										foreach($charges as $name=>$price) : ?>

									<tr class="clearfix">
										<td><?php echo $name?></td>
										<td><?php echo format_currency($price); ?></td>
									</tr>

									<?php endforeach;
									}

									/**************************************************************
									Order Taxes
									**************************************************************/
									 // Show shipping cost if added before taxes
									if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
									<tr class="clearfix">
										<td><?php echo lang('shipping');?></td>
										<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
									</tr>
									<?php endif;
									if($this->go_cart->order_tax() > 0) :  ?>
								    <tr class="clearfix">
								    	<td><?php echo lang('tax');?></td>
										<td><?php echo format_currency($this->go_cart->order_tax());?></td>
									</tr>
									<?php endif;
									// Show shipping cost if added after taxes
									if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
									<tr class="clearfix">
										<td><?php echo lang('shipping');?></td>
										<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
									</tr>
									<?php endif ?>

									<?php
									/**************************************************************
									Gift Cards
									**************************************************************/
									if($this->go_cart->gift_card_discount() > 0) : ?>
									<tr class="clearfix">
										<td><?php echo lang('gift_card_discount');?></td>
										<td>-<?php echo format_currency($this->go_cart->gift_card_discount()); ?></td>
									</tr>
									<?php endif; ?>

									<?php
									/**************************************************************
									Grand Total
									**************************************************************/
									?>
									<tr class="total clearfix">
										<td><strong><?php echo lang('grand_total');?></strong></td>
										<td><strong><?php echo format_currency($this->go_cart->total()); ?></strong></td>
									</tr>

								</table>

								<?php
								$specialOfferEnabled = false;
								foreach ($this->go_cart->contents() as $item):
									if(isset($item['specialOfferApplied'])){
										if($item['specialOfferApplied']){
											$specialOfferEnabled 	= true;
											break;
										}
									}
								endforeach;
								
								if(!$specialOfferEnabled){
								?>
									<div class="coupon_form input-group mtb-10">
										<input class="form-control empty" value="" type="text" name="coupon_code" placeholder="Enter Coupon Code" onFocus="if (this.value == 'Enter Coupon Code') this.value = '';" onBlur="if (this.value == '') this.value = 'Enter Coupon Code';" />
										<span class="input-group-addon cinput"><input value="Apply" class="btn btn-red" type="submit"></span>
									</div>
									<?php if($gift_cards_enabled):?>
										<div class="coupon_form input-group mtb-10">
							                <input class="form-control" type="text" name="gc_code" style="margin:0px;">
							                <span class="input-group-addon cinput"><input value="<?php echo lang('apply_gift_card');?>" class="btn btn-red" type="submit"></span>
						                </div>
						            <?php endif;?>
					        	<?php
					        	}
						        ?>

					            <input id="redirect_path" type="hidden" name="redirect" value=""/>

					            <?php if(!$this->Customer_model->is_logged_in(false,false)): ?>
					            	<a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-green btn-block text-center fl-n mt10">Login/Sign Up</a>
					            	<!-- <input class="btn btn-green btn-block text-center fl-n mt10" type="submit" onclick="$('#redirect_path').val('checkout/login');" value="<?php echo lang('login');?>"/> -->
					            	<!-- <input class="btn inactive" type="submit" onclick="$('#redirect_path').val('checkout/register');" value="<?php echo lang('register_now');?>"/> -->
					            <?php endif; ?>

					            <?php if ($this->Customer_model->is_logged_in(false,false)): ?>
					            	<div class="cart_navigation fl-n">
			                            <input class="btn btn-red btn-block text-center fl-n" type="submit" onclick="$('#redirect_path').val('checkout');" value="<?php echo lang('form_checkout');?>"/>
					            	</div>
					            <?php endif; ?>

					            <?php if (!$this->config->item('require_login') && !($this->Customer_model->is_logged_in(false,false))): ?>
					            	<div class="cart_navigation fl-n">
			                            <input class="btn btn-red btn-block text-center fl-n" type="submit" onclick="$('#redirect_path').val('checkout');" value="Checkout as Guest"/>
					            	</div>
					            <?php endif; ?>

					            <div class="mt-12 hidden">
									<a class="inactive" href="<?php echo site_url();?>" ><i class="	fa fa-angle-left" aria-hidden="true"></i> Continue shopping</a>
								</div>

		                    </div>



		                </div>
	                </form>
	            <?php endif;?>

            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
        $('.vertical-menu-content').css('display','none');
    })
</script>
