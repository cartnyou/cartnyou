<div class="container" style="padding-top: 150px;">

    <div class="row">
        <div class="about-container col-md-9">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{THEMEASSETS}}/img/about.jpg" alt="" class="abt-img">
                </div>
                <div class="abt-text col-md-7">
                    <p>
                        CartNYou is a metaphor for the largest ecommerce in India. We cater our customers
                        with the series of verified vendors which offers apparels, footwear, accessories,
                        electronic goods and anything you wish for.
                    </p>
                    <p>
                        We completely ADHERES to the needs of online customers and catch your eye and
                        arrest it forever in our online shopping arena.
                    </p>



                </div>
                <div class="col-md-12">
                    <h3 class="mt-12 mb-5">Start Up</h3>

                    <p>
                        Cartnyou started with a team of 10 young professionals on July 2015. The co-founders at Cartnyou possess over a long decade of experience in marketing and are determined to take online shopping to the next level beyond just shopping. We started our business with various categories and received an extensive response from our customers. We were able to achieve a vast customer base on the first day itself. We have expanded our business to all categories for men, women, kids, spy gadgets and many more.

                    </p>

                    <h3 class="mt-12 mb-5">Our Journey</h3>

                    <p>
                        Today Cartnyou has more than 1000 products and a 100k strong customer base. We started with a team of only ten members; however, today we have a strong, talented and motivated team of employees with us. On an average, we have 50000 visitors on our website every day. We have more than 1000000 registered users and hundreds of sellers with us. We have been able to grow exponentially in terms of our sales as well as customer base in a short span of 2 years and intend to scale up the trajectory at an increasing rate.

                    </p>
                    <h3 class="mt-12 mb-5">Working at Cartnyou</h3>

                    <p>
                        We have a team of young and energetic team members. The working environment at Cartnyou is professional and friendly at the same time. We believe to work in an open culture where team members have close interaction with their superiors. Our team members are not just employees of the company; we always encourage them to share their thoughts and ideas with the boss! The employees are free to contact the human resource team in case they face any problem while working at Cartnyou.


                    </p>
                    <h3 class="mt-12 mb-5">Fun at Cartnyou
                    </h3>

                    <p>
                        Working at Cartnyou is full of joy, excitement and fun. We enjoy fun activities, birthday celebrations and get together with the team every month. We love to motivate champions of the team with surprising gifts.
                    </p>
                    <h3 class="mt-12 mb-5">Why Cartnyou?

                    </h3>

                    <p>
                        We have exclusive products available for you inside. We have designed an easy to navigate interface for you with multiple options to select your product from. We value your money and have “easy to return” policy and COD option for our customers. Our executives are just a phone call away from you for any support. We launched with various categories and expanded our business into all categories for men, women, kids, mobile and tablets, home and kitchen, power banks and spy gadgets. Also, do not forget to visit our exclusive store for gizmos, stationery, Home furnishing, bags and luggage.
                    </p>

                </div>
            </div>
        </div>
        <div class="col-md-3 abt-usp">
            <h3 class="usp-title">Why Choose us</h3>
            <ul class="usp-list">
                <li>Free Shipping</li>
                <li>7 Day Return</li>
                <li>Secured Payments</li>
                <li>Shipping & Return</li>
                <li>Secure Shopping</li>
            </ul>


        </div>
    </div>


</div>

