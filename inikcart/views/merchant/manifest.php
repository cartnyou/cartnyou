<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>

<div style="font-size:12px; font-family:arial, verdana, sans-serif;">

    <table style="border:0px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="border:0;text-align:left;">Courier Company: DELHIVERY</td>
            <td style="border:0;text-align:left;">Date: <?php echo date('d/m/Y');?></td>
            <td style="border:0;text-align:left;">Store: <?php echo isset($orders[0]['seller_name'])?$orders[0]['seller_name']:'';?></td>
        </tr>
    </table>
    
    <style type="text/css">
        .tb th,td{
            text-align: center;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
        }
        .tb th:last-child,td:last-child{
            border-right: 1px solid #000;
        }
        .tb tr:last-child td{
            border-bottom: 1px solid #000;
        }
    </style>

    <table class="tb" style="width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th>S.No.</th>
                <th>Airwaybill</th>
                <th>Order Number</th>
                <th>Invoice Number</th>
                <th>Payment Mode</th>
                <th>Attention</th>
                <th>City/State</th>
                <th>Contents</th>
                <th>Weight(gms)</th>
                <th>Barcode - Airwaybill number</th>
            </tr>
        </thead>

        <tbody>
            <?php
            if(!empty($orders)){
                foreach ($orders as $k => $order) {
            ?>
                <tr>
                    <td><?php echo ($k+1);?></td>
                    <td><?php echo $order['waybill'];?></td>
                    <td><?php echo $order['order_number'];?></td>
                    <td><?php echo $order['invoice_number'];?></td>
                    <td><?php echo $order['payment_mode'];?></td>
                    <td><?php echo $order['attention'];?></td>
                    <td><?php echo $order['city'];?></td>
                    <td><?php echo $order['contents'];?></td>
                    <td><?php echo $order['weight'];?></td>
                    <td><img src="<?php echo $order['barcode'];?>"></td>
                </tr>
            <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
</body>
</html>