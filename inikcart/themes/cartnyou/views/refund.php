<style>
    ul.dotted-ul li {
        list-style-type: disc;
    }

    ol.alpha-ol li {
        list-style-type: lower-alpha;
    }

    ul.dotted-ul {
        margin-left: 40px;
    }

    ol.alpha-ol {
        margin-left: 40px;
    }
</style>
<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url() ?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Refund Policy</span>
        </div>
        <div class="row">
            <div class="col-md-12 center_column " id="center_column">
                <h2 class="page-heading">
                    <span class="page-heading-title2">Refund Policy</span>
                </h2>

                <div class="content-text clearfix">
                    <p>
                        However, please note that due to logistics issues, orders cannot be cancelled once paid for.
                    </p>
                    <p>
                        No cancellations will also be entertained for those products that CartNYou marketing team has obtained on special occasions like Pongal, Diwali, Valentine’s Day etc. These are limited occasion offers and therefore cancellations are not possible.
                    </p>
                    <p>
                        Kindly view all product descriptions and product videos(If Available) where available before purchase.
                    </p>
                    <p>
                        CartNYou items are not available for trial and return. We understand that other vendors may be supplying products which can be tried and returned. These vendors may then recycle the returned products to other customers. Which means that you may also be receiving used and recycled merchandise when buying from such vendors.
                    </p>
                    <p>
                        CartNYou does not recycle products, hence, we are unable to accept used products back for a refund, immaterial of the reason. All CartNYou items are sold with a replacement warranty, and if there is any warranty issue with the product, they will be replaced promptly.
                    </p>
                    <p>
                        Note that products cannot be returned or exchanged if they are as per description and delivered in working condition. Exchange is at the sole discretion of the CartNyou and can be exercised only under special and genuine circumstances.
                    </p>
                    <p>
                        In any case, refunds are not possible for items once they are shipped. Products may be replaced or exchanged only.
                    </p>
                    <p>
                        The Merchant shall not be liable for any losses, direct or indirect incurred by the customer towards usage, inability to use or failure of the product. The customer agrees to buy and use the product at his own responsibility and risk and use necessary discretion and care when operating electronic products. The maximum liability of the merchant will be limited to the cost of the product purchased. It is the customers responsibility to use the product correctly and as described, and to consult the merchant in case of any doubt.
                    </p>
                    <p>
                        In case of complaints regarding products that come with a warranty from manufacturers, please refer the issue to them.
                    </p>
                    <p>
                        For any replacement, the product needs to be returned first. In case of replacement for transit damage or warranty claim, postage costs will be reimbursed for the return after verification of the claim. Return of the product is mandatory to initiate a claim.
                    </p>
                    <h3 class="mt-12 mb-5">Refund Policy for out of stock items:</h3>
                    <p>
                        For any reason, if the product you have ordered is not available in stock, we will give you the option to change your order to another item of equivalent value you. However, if you so wish, a full refund of your payment will be processed in this scenario.
                    </p>
                    <p>
                        Processing time for refunds can take upto 7 days.
                    </p>
                    <p>
                        No refunds will be given for items purchased using loyalty points, referral credits or coupons of any kind. Only actual monetary payments can be refunded.
                    </p>
                    <h3 class="mt-12 mb-5">Damage/Loss in transit:</h3>
                    <p>
                        Please note that all items are insured with the shipper. If your item was damaged or lost in transit, a replacement will be arranged. However, please note that the maximum period for reporting a damaged or lost item received is 48 hours.
                    </p>

                </div>

            </div>
        </div>

    </div>
</div>