<div class="col-left-slide left-module">
    <ul class="owl-carousel owl-style2" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1" data-autoplay="true">
        <?php
		foreach($banners as $banner):?>
			<li>
				<?php
				if($banner->link){
                    $target=false;
                    if($banner->new_window){
                        $target=' target="_blank"';
                    }
                    echo '<a href="'.$banner->link.'"'.$target.'>';
                }
                ?>
				<img src="<?php echo base_url('uploads/'.$banner->image);?>" alt="<?php echo $banner->name;?>">
				<?php
				if($banner->link){
                    echo '</a>';
                }
                ?>
			</li>
		<?php
		endforeach;
		?>
    </ul>
</div>