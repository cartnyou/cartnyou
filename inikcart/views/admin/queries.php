<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<div class="row">
			<div class="span4">
				<?php echo $this->pagination->create_links();?>	&nbsp;
			</div>
			<div class="span8">
				<form action="<?php echo site_url($this->config->item('admin_folder'));?>/queries/index" class="form-inline" style="float:right">
					<fieldset>
						<input type="text" class="span2" name="term" placeholder="Ticket#/Phone/Email" value="<?php echo $term;?>" /> 
						<button class="btn"><?php echo lang('search')?></button>
						<a class="btn" href="<?php echo site_url($this->config->item('admin_folder').'/queries/index');?>">Reset</a>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<table class="table table-striped">
    <thead>
		<tr>
			<th>Ticket #</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Raised At</th>
			<th>Last Updated At</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo $row->ticket_number;?></td>
					<td><?php echo ucwords($row->name);?></td>
					<td><?php echo $row->email;?></td>
					<td><?php echo $row->phone;?></td>
					<td><?php echo $row->created_at;?></td>
					<td><?php echo $row->updated_at;?></td>
					<td><?php echo ($row->enabled == '1') ? 'Closed' : 'Open';?></td>
					<td>
						<div class="btn-group" style="float:right">
							<a class="btn" href="<?php echo site_url('admin/queries/detail/'.$row->q_id);?>" target="_blank">Details</a>

							<?php if($row->enabled == '1'){ ?>
								<a class="btn btn-info" href="<?php echo site_url(ADMIN_FOLDER.'/queries/query_status/'.$row->q_id);?>">Open</a>
							<?php } else { ?>
								<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/queries/query_status/'.$row->q_id);?>">Close</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="7">No data found.</td></tr>';
		}
		?>
	</tbody>
</table>