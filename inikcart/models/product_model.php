<?php
Class Product_model extends CI_Model
{
	
	function product_autocomplete($name, $limit, $merchant_id=false, $offset=false, $count=false)
	{
		if($merchant_id){
			$this->db->select('products.id, products.name, products.images, products_merchants.id AS pm_id');
			$this->db->join('products_merchants','products_merchants.product_id=products.id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		} else {
			$this->db->select('products.id, products.name, products.images');
		}
		$this->db->like('name', $name);
		if($count){
			return $this->db->count_all_results('products');
		} else {
			$this->db->limit($limit);
			if($offset){
				$this->db->offset($offset);
			}
			return $this->db->get('products')->result();
		}
	}
	
	function products($data=array(), $return_count=false)
	{
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			if(!empty($data['merchant_id'])){
				$this->get_all_products($data['merchant_id']);
			} else {
				$this->get_all_products();
			}
		}
		else
		{
			$select_str = 'products.*';
			
			if(!empty($data['merchant_id'])){
				$select_str .= ',products_merchants.id as pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled as variant_enabled,products_merchants.payment_mode,products_merchants.live_on';
				$this->db->join('products_merchants', 'products_merchants.product_id=products.id', 'right');
				$this->db->where('products_merchants.merchant_id',$data['merchant_id']);
			}
			$this->db->select($select_str);

			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->where('(name LIKE "%'.$search->term.'%"');
					$this->db->or_where('description LIKE "%'. $search->term.'%"');
					$this->db->or_where('specification LIKE "%'. $search->term.'%"');
					
					if(!empty($data['merchant_id'])){
						$this->db->or_where('excerpt LIKE "%'. $search->term.'%"');
						$this->db->or_where('products_merchants.sku LIKE "%'.$search->term.'%")');
					} else {
						$this->db->or_where('excerpt LIKE "%'. $search->term.'%")');
						//$this->db->or_where('excerpt LIKE "%'. $search->term.'%"');
						//$this->db->or_where('products_merchants.sku LIKE "%'.$search->term.'%")');
					}
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function get_all_products($merchant_id=false)
	{
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id', 'right');
		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}

		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('products');

		return $result->result();
	}
	
	function get_filtered_products($product_ids, $limit = false, $offset = false)
	{
		
		if(count($product_ids)==0)
		{
			return array();
		}
		
		$this->db->select('id, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('products');
		
		if(count($product_ids)>1)
		{
			$querystr = '';
			foreach($product_ids as $id)
			{
				$querystr .= 'id=\''.$id.'\' OR ';
			}
		
			$querystr = substr($querystr, 0, -3);
			
			$this->db->where($querystr, null, false);
			
		} else {
			$this->db->where('id', $product_ids[0]);
		}
		
		$result	= $this->db->limit($limit)->offset($offset)->get()->result();

		//die($this->db->last_query());

		$contents	= array();
		$count		= 0;
		foreach ($result as $product)
		{

			$contents[$count]	= $this->get_product($product->id);
			$count++;
		}

		return $contents;
		
	}
	
	/*function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1));

			$this->db->order_by($by, $sort);
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();
			
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}*/

	function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false, $filters=array(),$count=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category_id,true);
			//print_r($subCategories);exit;
			
			$this->db->distinct();
			if($count){
				$this->db->select('products.id, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price');
			} else {
				$this->db->select('category_products.*, products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled,products_merchants.payment_mode,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name, count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price', false);
			}
			$this->db->from('category_products');
			$this->db->join('products', 'category_products.product_id=products.id');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->join('brands', 'products.brand = brands.id');

			if(!$count){
				$this->db->join('product_offers po', 'po.product_id = products.id AND po.offer_enabled=1', 'left outer');
			}

			$this->db->where('products_merchants.enabled',1);
			
			if(!empty($subCategories)){
				$this->db->where('(category_products.category_id = '.$category_id);
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$this->db->or_where('category_products.category_id = '.$subCategory.')');
					} else {
						$this->db->or_where('category_products.category_id = '.$subCategory);
					}
				}
			} else {
				$this->db->where('category_products.category_id',$category_id);
			}
			//$this->db->where('category_id',$category_id);

			if(isset($filters['min_price'])){
				$this->db->having('sort_price >=', $filters['min_price']);
			}
			if(isset($filters['max_price'])){
				$this->db->having('sort_price <=', $filters['max_price']);
			}
			if(isset($filters['brand_checked'])){
				if(!empty($filters['brand_checked'])){
					$countSql = count($filters['brand_checked']);
					foreach ($filters['brand_checked'] as $k => $brand_id) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(products.brand = '.$brand_id);
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('products.brand = '.$brand_id.')');
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else {
							$this->db->or_where('products.brand = '.$brand_id);
						}
					}
				}
			}

			if(isset($filters['facets'])){
				if(!empty($filters['facets'])){
					
					$this->db->join('options', 'options.product_id = products.id');
					$this->db->join('option_values', 'option_values.option_id = options.id');

					$facets_keys 	= array();
					$facets_values 	= array();
					
					foreach ($filters['facets'] as $facet_key => $facet_value) {
						$facets_keys[] = $facet_key;

						if(!empty($facet_value)){
							$countSql = count($facet_value);
							foreach ($facet_value as $k => $facet_value_single) {
								$facets_values[] = $facet_value_single;
							}
						}
					}

					if(!empty($facets_keys)){
						$countSql = count($facets_keys);
						foreach ($facets_keys as $k => $facets_keys_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(options.name = "'.$facets_keys_single.'"');
								} else {
									$this->db->where('(options.name = "'.$facets_keys_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('options.name = "'.$facets_keys_single.'")');
								} else {
									$this->db->where('(options.name = "'.$facets_keys_single.'")');
								}
							} else {
								$this->db->or_where('options.name = "'.$facets_keys_single.'"');
							}
						}
					}

					if(!empty($facets_values)){
						$countSql = count($facets_values);
						foreach ($facets_values as $k => $facet_value_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(option_values.name = "'.$facet_value_single.'"');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('option_values.name = "'.$facet_value_single.'")');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else {
								$this->db->or_where('option_values.name = "'.$facet_value_single.'"');
							}
						}
					}
				}
			}

			$this->db->group_by('products.id');

			if($count){
				$result	= $this->db->count_all_results();
			} else {
				if($by && $sort){
					$this->db->order_by($by, $sort);
				}
				
				$result	= $this->db->limit($limit)->offset($offset)->get()->result();

				if($result){
					foreach ($result as &$p){
						$p->rating	= $this->get_avg_rating($p->id,true);
					}
				}
			}

			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}

	function get_products_filter_options($category_id=false){
		if ($category_id)
		{
			$this->db->distinct();
			$this->db->select('options.name, option_values.name as ov_name, option_values.value as ov_value, option_values.image as ov_image', false);
			
			$this->db->from('category_products');
			$this->db->join('products', 'category_products.product_id=products.id');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->join('options', 'options.product_id = products.id');
			$this->db->join('option_values', 'option_values.option_id = options.id');

			$this->db->where('products_merchants.enabled',1);
			
			$this->db->where('category_id',$category_id);

			if(isset($filters['min_price'])){
				$this->db->having('sort_price >=', $filters['min_price']);
			}
			if(isset($filters['max_price'])){
				$this->db->having('sort_price <=', $filters['max_price']);
			}
			if(isset($filters['brand_checked'])){
				if(!empty($filters['brand_checked'])){
					$countSql = count($filters['brand_checked']);
					foreach ($filters['brand_checked'] as $k => $brand_id) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(products.brand = '.$brand_id);
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('products.brand = '.$brand_id.')');
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else {
							$this->db->or_where('products.brand = '.$brand_id);
						}
					}
				}
			}

			$result	= $this->db->get()->result();
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}
	
	function count_all_products()
	{
		return $this->db->count_all_results('products');
	}
	
	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	function get_product($id, $related=true, $merchant_id='', $all_merchnats=false,$except_merchant_id='',$pm_id=false, $withOffers=false)
	{
		$pm_join = false;
		if($pm_id){
			$this->db->where('products_merchants.id',$pm_id);
			$pm_join = true;
		}

		if($merchant_id != ''){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			$pm_join = true;
		}

		if($except_merchant_id != ''){
			$this->db->where('products_merchants.merchant_id !=',$except_merchant_id);
			$pm_join = true;
		}

		$select_str = 'products.*,brands.name as brand_name,brands.slug as brand_slug';
		$this->db->where('products.id',$id);
		if($pm_join || $all_merchnats){
			$select_str .= ',products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled,products_merchants.payment_mode,products_merchants.live_on, merchants.id AS store_id,merchants.store_name,merchants.city AS item_location,merchants.slug AS merchant_slug';
			$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
			$this->db->join('merchants', 'merchants.id = products_merchants.merchant_id');
		}
		$this->db->select($select_str);
		$this->db->join('brands', 'products.brand = brands.id');

		$this->db->where('products.enabled', 1);
		//$this->db->where('products_merchants.enabled', 1);

		if($all_merchnats){
			$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);
			$result	= $this->db->get('products')->result();
			if(!$result)
			{
				return false;
			}

			if($withOffers){
				$result[0]->offers = $this->getProductOffers($id);
			}

			$related	= json_decode($result[0]->related_products);
			
			if(!empty($related))
			{
				/*$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled, brands.name as brand_name');
				$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
				$this->db->join('brands', 'products.brand = brands.id');

				//build the where
				$where = array();
				foreach($related as $r)
				{
					$where[] = '`products`.`id` = '.$r;
				}

				$this->db->where('('.implode(' OR ', $where).')', null);
				$this->db->where('products_merchants.enabled', 1);

				$this->db->group_by('products.id');
				$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);

				$result[0]->related_products	= $this->db->get('products')->result();*/

				$relateds = array();
				foreach($related as $r)
				{
					$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled,products_merchants.live_on, brands.name as brand_name');
					$this->db->where('products.id',$r);
					$this->db->where('products.enabled',1);
					$this->db->where('products_merchants.enabled',1);
					$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
					$this->db->join('brands', 'products.brand = brands.id');
					$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);

					$p = $this->db->get('products')->row();
					if(!empty($p)){

						$p->rating	= $this->get_avg_rating($p->id,true);
						$relateds[] = $p;
					}
				}
				$result[0]->related_products = $relateds;
				//print_r($result[0]->related_products);exit;

			}
			else
			{
				$result[0]->related_products	= array();
			}
			$result[0]->categories			= $this->get_product_categories($result[0]->id);

			return $result;

		} else {
			$result	= $this->db->get('products')->row();
			if(!$result)
			{
				return false;
			}

			if($withOffers){
				$this->db->where('product_id', $id);
				$this->db->get('product_offers');
				$result->offers = $offers;
			}

			$related	= json_decode($result->related_products);
			
			if(!empty($related))
			{
				//build the where
				$where = array();
				foreach($related as $r)
				{
					$where[] = '`id` = '.$r;
				}

				$this->db->where('('.implode(' OR ', $where).')', null);
				$this->db->where('enabled', 1);

				$result->related_products	= $this->db->get('products')->result();
			}
			else
			{
				$result->related_products	= array();
			}
			$result->categories			= $this->get_product_categories($result->id);

			return $result;
		}
	}

	function get_product_by_variant($variant_id)
	{
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->where('products_merchants.id',$variant_id);
		$result	= $this->db->get('products')->row();
		if(!$result)
		{
			return false;
		}

		$related	= json_decode($result->related_products);
		
		if(!empty($related))
		{
			//build the where
			$where = array();
			foreach($related as $r)
			{
				$where[] = '`id` = '.$r;
			}

			$this->db->where('('.implode(' OR ', $where).')', null);
			$this->db->where('enabled', 1);

			$result->related_products	= $this->db->get('products')->result();
		}
		else
		{
			$result->related_products	= array();
		}
		$result->categories			= $this->get_product_categories($result->id);

		return $result;
	}

	function get_product_categories($id)
	{
		return $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
	}

	function get_slug($id,$pm_id=false)
	{
		$this->db->select('products_merchants.slug');
		$this->db->where('products.id',$id);
		$this->db->where('products_merchants.id',$pm_id);
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		return $this->db->get('products')->row()->slug;

		//return $this->db->get_where('products', array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false, $productMerchant=array(), $productMerchantEdit=true)
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}
		else
		{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		if($productMerchantEdit){
			$this->db->where('product_id', $id);
			$this->db->where('merchant_id', $productMerchant['merchant_id']);
			$countPM = $this->db->count_all_results('products_merchants');

			$productMerchant['product_id'] = $id;
			if($countPM > 0){
				$this->db->where('product_id', $id);
				$this->db->where('merchant_id', $productMerchant['merchant_id']);
				$this->db->update('products_merchants', $productMerchant);
			} else {
				if(isset($productMerchant['merchant_id'])){
					if($productMerchant['merchant_id'] != 0){
						$this->db->insert('products_merchants', $productMerchant);
					}
				}
			}
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c)
				{
					$ids[]	= $c->id;
				}

				//eliminate categories that products are no longer in
				foreach($ids as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $ids))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		
		//return the product id
		return $id;
	}
	
	function save_offers($product_id, $inHouseOffer, $extraOffer, $specialOffer,$productPrice,$update)
	{
		$offerCount=0;$offers=array();
		if($inHouseOffer)
		{
			foreach($inHouseOffer as $key=>$value){
				if($this->input->post('offer_discount')[$key]!=''&&$value!=''){
					$offers[$offerCount]=array('product_id'=>$product_id,'offer_name'=>$value,'offer_description'=>$this->input->post('offer_description')[$key],
												'offer_type'=>0,'offer_price'=>$productPrice-$this->input->post('offer_discount')[$key],'offer_discount'=>$this->input->post('offer_discount')[$key],
												'offer_start_at'=>'0000-00-00 00:00:00','offer_expire_at'=>'0000-00-00 00:00:00',
												'offer_payment_mode'=>$this->input->post('offer_payment_mode')[$key],'offer_enabled'=>$this->input->post('offer_status')[$key],'offer_created_at'=>date('Y-m-d H:i:s'));
					$offerCount++;
				}
			}
		}
		if($extraOffer)
		{
			foreach($extraOffer as $key=>$value){
				if($this->input->post('extra_offer_discount')[$key]!=''&&$value!=''){
					$offers[$offerCount]=array('product_id'=>$product_id,'offer_name'=>$value,'offer_description'=>$this->input->post('extra_offer_description')[$key],
												'offer_type'=>1,'offer_price'=>$productPrice-$this->input->post('extra_offer_discount')[$key],'offer_discount'=>$this->input->post('extra_offer_discount')[$key],
												'offer_start_at'=>'0000-00-00 00:00:00','offer_expire_at'=>'0000-00-00 00:00:00',
												'offer_payment_mode'=>$this->input->post('extra_offer_pay_mode')[$key],'offer_enabled'=>$this->input->post('extra_offer_status')[$key],'offer_created_at'=>date('Y-m-d H:i:s'));
					$offerCount++;
				}
			}
		}
		if($specialOffer!=''){
			if($this->input->post('so_price')!=''&&$specialOffer!=''){
				$offers[$offerCount]=array('product_id'=>$product_id,'offer_name'=>$specialOffer,'offer_description'=>$this->input->post('so_description'),
												'offer_type'=>2,'offer_price'=>$productPrice-$this->input->post('so_price'),'offer_discount'=>$this->input->post('so_price'),
												'offer_start_at'=>date("Y-m-d H:i:s",strtotime($this->input->post('so_start_at'))),'offer_expire_at'=>date("Y-m-d H:i:s",strtotime($this->input->post('so_expire_at'))),
												'offer_payment_mode'=>'','offer_enabled'=>$this->input->post('so_status'),'offer_created_at'=>date('Y-m-d H:i:s'));
				$offerCount++;
			}
		}
		if(!empty($offers))
		{
			if($update)
			{
				//delete all the previous product offers
				$this->db->delete('product_offers', array('product_id'=>$product_id));
				
				//new offers add them all
				$this->db->insert_batch('product_offers', $offers);
			}
			else
			{
				//new offers add them all
				$this->db->insert_batch('product_offers', $offers);
			}
		}else{
			$this->db->delete('product_offers', array('product_id'=>$product_id));
		}
		//return the product id
		return $product_id;
	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

		// delete merchant reference
		$this->db->where('product_id', $id);
		$this->db->delete('products_merchants');

		// delete rating reference
		$this->db->where('product_id', $id);
		$this->db->delete('products_ratings');

	}

	function save_variant($product_variant)
	{
		if ($product_variant['id'])
		{
			$this->db->where('id', $product_variant['id']);
			$this->db->update('products_merchants', $product_variant);

			$id	= $product_variant['id'];
		}
		else
		{
			if(isset($product_variant['merchant_id'])){
				if($product_variant['merchant_id'] != 0){
					$this->db->insert('products_merchants', $product_variant);
				}
			}
			$id	= $this->db->insert_id();
		}

		return $id;
	}

	function delete_variant($id)
	{
		// delete variant 
		$this->db->where('id', $id);
		$this->db->delete('products_merchants');
	}

	function disable_variant($id)
	{
		$sql = "UPDATE products_merchants SET enabled = IF(enabled=1, 0, 1) WHERE id=".$id;
		$this->db->query($sql);
	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	function search_products($term, $limit=false, $offset=false, $by=false, $sort=false,$filters=array(),$count=false){
		$term = trim($term);
		if($filters['category'] != 0) {
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($filters['category'],true);	
		}
		
		/*$this->db->select('category_products.*, products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, brands.id AS brand_id, brands.name AS brand_name', false);
		$this->db->from('category_products');
		$this->db->join('products', 'category_products.product_id=products.id');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		$this->db->where('products_merchants.enableds',1);
		$this->db->where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.specification LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products_merchants.sku LIKE "%'.$term.'%")');*/

		$this->db->distinct();
		if($count){
			$this->db->select('products.id, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price');
		} else {
			$this->db->select('products.*,MATCH (products.name,products.description,products.specification,products.excerpt) AGAINST ("'.$term.'" IN BOOLEAN MODE) AS relavance, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, products_merchants.payment_mode,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name, count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price', false);
		}
		$this->db->from('products');
		$this->db->join('category_products', 'category_products.product_id=products.id');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		
		if(!$count){
			$this->db->join('product_offers po', 'po.product_id = products.id AND po.offer_enabled=1', 'left outer');
			//$this->db->join('product_offers pfs', 'pfs.product_id = products.id AND pfs.offer_type=2 AND pfs.offer_enabled=1', 'left outer');
		}

		$this->db->where('products_merchants.enabled',1);
		$this->db->where('(MATCH (products.name,products.description,products.specification,products.excerpt) AGAINST ("'.$term.'" IN BOOLEAN MODE)', NULL, FALSE);
		$this->db->or_where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.specification LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products_merchants.sku LIKE "%'.$term.'%"))');
		//$this->db->or_where('strcmp(soundex(products.name), soundex("'.$term.'")) = 0)    ');
		
		if($filters['category'] != 0) {
			if(!empty($subCategories)){
				$this->db->where('(category_id = '.$filters['category']);
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$this->db->or_where('category_id = '.$subCategory.')');
					} else {
						$this->db->or_where('category_id = '.$subCategory);
					}
				}
			} else {
				$this->db->where('category_id',$filters['category']);
			}
		}
		//if($filters['category'] != 0) {
		//	$this->db->where('category_id',$filters['category']);
		//}

		if(isset($filters['min_price'])){
			$this->db->having('sort_price >=', $filters['min_price']);
		}
		if(isset($filters['max_price'])){
			$this->db->having('sort_price <=', $filters['max_price']);
		}
		if(isset($filters['brand_checked'])){
			if(!empty($filters['brand_checked'])){
				$countSql = count($filters['brand_checked']);
				foreach ($filters['brand_checked'] as $k => $brand_id) {
					if($k == 0){
						if($countSql > 1){
							$this->db->where('(products.brand = '.$brand_id);
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else if($k == $countSql-1){
						if($countSql > 1){
							$this->db->or_where('products.brand = '.$brand_id.')');
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else {
						$this->db->or_where('products.brand = '.$brand_id);
					}
				}
			}
		}

		if(isset($filters['facets'])){
			if(!empty($filters['facets'])){
				
				$this->db->join('options', 'options.product_id = products.id');
				$this->db->join('option_values', 'option_values.option_id = options.id');

				$facets_keys 	= array();
				$facets_values 	= array();
				
				foreach ($filters['facets'] as $facet_key => $facet_value) {
					$facets_keys[] = $facet_key;

					if(!empty($facet_value)){
						$countSql = count($facet_value);
						foreach ($facet_value as $k => $facet_value_single) {
							$facets_values[] = $facet_value_single;
						}
					}
				}

				if(!empty($facets_keys)){
					$countSql = count($facets_keys);
					foreach ($facets_keys as $k => $facets_keys_single) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(options.name = "'.$facets_keys_single.'"');
							} else {
								$this->db->where('(options.name = "'.$facets_keys_single.'")');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('options.name = "'.$facets_keys_single.'")');
							} else {
								$this->db->where('(options.name = "'.$facets_keys_single.'")');
							}
						} else {
							$this->db->or_where('options.name = "'.$facets_keys_single.'"');
						}
					}
				}

				if(!empty($facets_values)){
						$countSql = count($facets_values);
						foreach ($facets_values as $k => $facet_value_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(option_values.name = "'.$facet_value_single.'"');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('option_values.name = "'.$facet_value_single.'")');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else {
								$this->db->or_where('option_values.name = "'.$facet_value_single.'"');
							}
						}
					}

				/*foreach ($filters['facets'] as $facet_key => $facet_value) {
					$this->db->where('options.name', $facet_key);

					if(!empty($facet_value)){
						$countSql = count($facet_value);
						foreach ($facet_value as $k => $facet_value_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(option_values.name = "'.$facet_value_single.'"');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('option_values.name = "'.$facet_value_single.'")');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else {
								$this->db->or_where('option_values.name = "'.$facet_value_single.'"');
							}
						}
					}

				}*/
			}
		}

		$this->db->group_by('products.id');

		if($count){
			$result	= $this->db->count_all_results();
		} else {
			if($by && $sort){
				$this->db->order_by($by, $sort);
			}
			$this->db->order_by('relavance', 'DESC');
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();

			if($result){
				foreach ($result as &$p){
					$p->rating	= $this->get_avg_rating($p->id,true);
				}
			}
		}

		return $result;
	}

	/*function search_products($term, $limit=false, $offset=false, $by=false, $sort=false)
	{
		$results		= array();
		
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one counts the total number for our pagination
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR specification LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		$results['count']	= $this->db->count_all_results('products');


		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one gets just the ones we need.
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR specification LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		
		return $results;
	}*/

	function search_products_filter_options($term, $filters){
		$term = trim($term);
		
		//$this->db->distinct();
		$this->db->select('options.name, option_values.name as ov_name, option_values.value as ov_value, option_values.image as ov_image', false);
		
		$this->db->from('products');
		$this->db->join('category_products', 'category_products.product_id=products.id');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('options', 'options.product_id = products.id');
		$this->db->join('option_values', 'option_values.option_id = options.id');
		$this->db->where('products_merchants.enabled',1);
		$this->db->where('(MATCH (products.name,products.description,products.specification,products.excerpt) AGAINST ("'.$term.'" IN BOOLEAN MODE)', NULL, FALSE);
		$this->db->or_where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.specification LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products_merchants.sku LIKE "%'.$term.'%"))');
		
		if($filters['category'] != 0) {
			$this->db->where('category_id',$filters['category']);
		}

		if(isset($filters['min_price'])){
			$this->db->having('sort_price >=', $filters['min_price']);
		}
		if(isset($filters['max_price'])){
			$this->db->having('sort_price <=', $filters['max_price']);
		}
		if(isset($filters['brand_checked'])){
			if(!empty($filters['brand_checked'])){
				$countSql = count($filters['brand_checked']);
				foreach ($filters['brand_checked'] as $k => $brand_id) {
					if($k == 0){
						if($countSql > 1){
							$this->db->where('(products.brand = '.$brand_id);
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else if($k == $countSql-1){
						if($countSql > 1){
							$this->db->or_where('products.brand = '.$brand_id.')');
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else {
						$this->db->or_where('products.brand = '.$brand_id);
					}
				}
			}
		}
		
		$result	= $this->db->get()->result();
		return $result;
	}

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false,$pm_id=false)
	{
		//$product	= $this->db->get_where('products', array('id'=>$id))->row();

		$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled,products_merchants.payment_mode,products_merchants.live_on, merchants.store_name,merchants.slug AS merchant_slug, brands.name as brand_name');
		$this->db->where('products.id',$id);
		$this->db->where('products_merchants.id',$pm_id);
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('merchants', 'merchants.id = products_merchants.merchant_id');
		$this->db->join('brands', 'products.brand = brands.id');
		
		$product	= $this->db->get('products')->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product)
		{
			return false;
		}
		
		$product->base_price	= $product->price;
		
		if ($product->saleprice != 0.00)
		{ 
			$product->price	= $product->saleprice;
		}
		
		
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1)
		{
			$product->quantity = 1;
		}
		else
		{
			$product->quantity = $quantity;
		}
		
		
		// attach list of associated downloadables
		$product->file_list	= $this->Digital_Product_model->get_associations_by_product($id);
		
		return (array)$product;
	}

	function add_review($save){
		$this->db->insert('products_ratings', $save);
	}

	function get_product_reviews($product_id){
		$result	= $this->db->select('products_ratings.id AS pr_id,((rating_price + rating_value + rating_quality) / 3.0) AS rating,review,created_at,products_ratings.customer_id AS customer_id,firstname,lastname,pr_name,pr_email,pr_phone')->where('pr_enabled',1)->where('products_ratings.product_id',$product_id)->join('customers','customers.id=products_ratings.customer_id', 'left outer')->get('products_ratings')->result();
		if(!$result){
			return false;
		}
		return $result;
	}

	function get_avg_rating($product_id,$rating_only=false){
		if($rating_only){
			$this->db->select('((AVG(rating_price) + AVG(rating_value) + AVG(rating_quality)) / 3.0) AS rating');
		} else {
			$this->db->select('((AVG(rating_price) + AVG(rating_value) + AVG(rating_quality)) / 3.0) AS rating, COUNT(id) AS count');
		}
		$this->db->where('pr_enabled',1);
		$result	= $this->db->where('product_id',$product_id)->get('products_ratings')->row();
		if(!$result){
			return false;
		}
		if($rating_only){
			return $result->rating;
		} else {
			return $result;
		}
	}

	function get_product_split_rating($product_id){
		$this->db->select('AVG(rating_price) as rating_price_avg, AVG(rating_value) as rating_value_avg, AVG(rating_quality) as rating_quality_avg');
		$this->db->where('pr_enabled',1);
		$result	= $this->db->where('product_id',$product_id)->get('products_ratings')->row();
		return $result;
	}

	function popular_products($limit = 10, $offset = false, $category = false, $merchant_id=false){
		/*$this->db->select('products.*, COUNT(order_items.product_id) as ordered_quantity,products_merchants.id AS pm_id, LEAST(IFNULL(NULLIF(products_merchants.saleprice, 0), products_merchants.price), products_merchants.price) as sort_price', false);
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('order_items', 'order_items.product_id=products.id');
		$this->db->group_by('products.id');
		$this->db->where(array('products_merchants.enabled'=>1));
		$this->db->order_by('ordered_quantity','DESC');*/

		/*$this->db->select('products.*, COUNT(order_items.product_id) as ordered_quantity, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.shipping_charges,products_merchants.quantity,products_merchants.enabled');
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('order_items', 'order_items.product_id=products.id');
		$this->db->where('products_merchants.enabled',1);
		$this->db->group_by('products.id');

		$this->db->order_by('ordered_quantity','DESC');
		$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);
		

		$result	= $this->db->limit($limit)->offset($offset)->get()->result();*/

		//$result = $this->db->query("SELECT subUp.* FROM (SELECT sub.*, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled FROM ( SELECT products.*,COUNT(order_items.product_id) as ordered_quantity FROM products JOIN order_items ON order_items.product_id=products.id GROUP by products.id ORDER BY ordered_quantity DESC LIMIT 10) AS sub JOIN products_merchants ON products_merchants.product_id = sub.id WHERE products_merchants.enabled = 1 ORDER BY (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp GROUP BY subUp.id ");

		$query 	= "SELECT subUp.* FROM (SELECT sub.*, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity,products_merchants.live_on, products_merchants.enabled AS pm_enabled FROM ( SELECT products.*,COUNT(order_items.product_id) as ordered_quantity,poj.offers_count,poj.offer_price FROM products LEFT OUTER JOIN (SELECT po.product_id,count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price FROM product_offers po) poj ON products.id = poj.product_id JOIN order_items ON order_items.product_id=products.id ";

		if($category !== false){
			//$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' ";
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category,true);

			$query .= "JOIN category_products ON category_products.product_id=products.id ";

			if(!empty($subCategories)){
				$query .= "WHERE  (category_products.category_id = ".$category." ";
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$query .= " OR category_products.category_id = ".$subCategory.") ";
					} else {
						$query .= " OR category_products.category_id = ".$subCategory." ";
					}
				}
			} else {
				$query .= "WHERE  (category_products.category_id = ".$category.") ";
			}
		}

		$query .= "AND products.enabled=1 GROUP by products.id ORDER BY ordered_quantity DESC LIMIT ".$limit.") AS sub JOIN products_merchants ON products_merchants.product_id = sub.id WHERE products_merchants.enabled = 1 ";
		if($merchant_id){
			$query .= "AND products_merchants.merchant_id=".$merchant_id." ";
		}
		$query .= "ORDER BY (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp";
		
		$result = $this->db->query($query);
		
		/*if($result){
			foreach ($result as &$p){
				$p->images	= (array)json_decode($p->images);
				//$p->options	= $this->Option_model->get_product_options($p->id);
			}
		}*/
		$return = $result->result();
		if($return){
			foreach ($return as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}
		return $return;
	}

	function sale_products($limit = false,$category=false){
		$query 	= "SELECT subUp.* FROM (SELECT products.*, (CASE WHEN products_merchants.saleprice > 0 THEN (products_merchants.price - products_merchants.saleprice) ELSE 0 END) AS discount ,products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity,products_merchants.live_on, products_merchants.enabled AS pm_enabled,poj.offers_count,poj.offer_price FROM products LEFT OUTER JOIN (SELECT po.product_id,count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price FROM product_offers po) poj ON products.id = poj.product_id JOIN products_merchants ON products_merchants.product_id = products.id ";
		
		if($category !== false){
			//$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' AND ";
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category,true);

			$query .= "JOIN category_products ON category_products.product_id=products.id ";

			if(!empty($subCategories)){
				$query .= "WHERE  (category_products.category_id = ".$category." ";
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$query .= " OR category_products.category_id = ".$subCategory.") AND ";
					} else {
						$query .= " OR category_products.category_id = ".$subCategory." ";
					}
				}
			} else {
				$query .= "WHERE  (category_products.category_id = ".$category.") AND ";
			}
		} else {
			$query .= "WHERE ";
		}

		$query .= "products_merchants.enabled = 1 AND products.enabled = 1 HAVING discount>0 ORDER BY discount desc, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp LIMIT ".$limit;
		$result = $this->db->query($query);
		$return = $result->result();

		if($return){
			foreach ($return as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}
		return $return;
	}

	function most_viewed_home($limit = false,$category=false){
		
		$query 	= "SELECT products.*,products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity,products_merchants.live_on, products_merchants.enabled AS pm_enabled,poj.offers_count,poj.offer_price FROM products LEFT OUTER JOIN (SELECT po.product_id,count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price FROM product_offers po) poj ON products.id = poj.product_id JOIN products_merchants ON products_merchants.product_id = products.id ";

		if($category !== false){
			//$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' AND ";
		
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category,true);

			$query .= "JOIN category_products ON category_products.product_id=products.id ";

			if(!empty($subCategories)){
				$query .= "WHERE  (category_products.category_id = ".$category." ";
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$query .= " OR category_products.category_id = ".$subCategory.") AND ";
					} else {
						$query .= " OR category_products.category_id = ".$subCategory." ";
					}
				}
			} else {
				$query .= "WHERE  (category_products.category_id = ".$category.") AND ";
			}

		} else {
			$query .= "WHERE ";
		}

		$query .= "products_merchants.enabled = 1 AND products.enabled = 1 ORDER BY products.views desc, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC LIMIT ".$limit;
		$result = $this->db->query($query);
		$return = $result->result();

		if($return){
			foreach ($return as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}
		return $return;
	}

	function new_products($limit = false,$category=false){
		
		$query 	= "SELECT products.*,products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity,products_merchants.live_on, products_merchants.enabled AS pm_enabled,poj.offers_count,poj.offer_price FROM products LEFT OUTER JOIN (SELECT po.product_id,count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price FROM product_offers po) poj ON products.id = poj.product_id JOIN products_merchants ON products_merchants.product_id = products.id ";

		if($category !== false){
			//$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' AND ";
		
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category,true);

			$query .= "JOIN category_products ON category_products.product_id=products.id ";

			if(!empty($subCategories)){
				$query .= "WHERE  (category_products.category_id = ".$category." ";
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$query .= " OR category_products.category_id = ".$subCategory.") AND ";
					} else {
						$query .= " OR category_products.category_id = ".$subCategory." ";
					}
				}
			} else {
				$query .= "WHERE  (category_products.category_id = ".$category.") AND ";
			}

		} else {
			$query .= "WHERE ";
		}

		$query .= "products_merchants.enabled = 1 AND products.enabled = 1 ORDER BY products.created_at desc, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC LIMIT ".$limit;
		$result = $this->db->query($query);
		$return = $result->result();

		if($return){
			foreach ($return as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}
		return $return;
	}

	function merchant_popular_products($limit = 10, $offset = false, $category = false, $merchant_id=false){
		$query 	= "SELECT products.*,
                       Count(order_items.product_id) AS ordered_quantity, products_merchants.id AS pm_id,
               products_merchants.merchant_id,
               products_merchants.sku,
               products_merchants.slug,
               products_merchants.route_id,
               products_merchants.price,
               products_merchants.saleprice,
               products_merchants.free_shipping,
               products_merchants.shipping_charges,
               products_merchants.quantity,
               products_merchants.live_on,
               products_merchants.enabled AS pm_enabled 
               FROM products JOIN order_items ON order_items.product_id=products.id ";
        $query .= "JOIN products_merchants ON products_merchants.product_id = products.id ";

		if($category !== false){
			$this->load->model('category_model');
			$subCategories = $this->category_model->get_all_sub_categories($category,true);

			$query .= "JOIN category_products ON category_products.product_id=products.id ";
			
			if(!empty($subCategories)){
				$query .= "WHERE  (category_products.category_id = ".$category." ";
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$query .= " OR category_products.category_id = ".$subCategory.") ";
					} else {
						$query .= " OR category_products.category_id = ".$subCategory." ";
					}
				}
			} else {
				$query .= "WHERE  (category_products.category_id = ".$category.") ";
			}
		}

		$query .= "AND products.enabled=1 AND products_merchants.enabled = 1 AND products_merchants.merchant_id = ".$merchant_id." GROUP by products.id ORDER BY ordered_quantity DESC LIMIT ".$limit." ";
		
		$result = $this->db->query($query);
		
		/*if($result){
			foreach ($result as &$p){
				$p->images	= (array)json_decode($p->images);
				//$p->options	= $this->Option_model->get_product_options($p->id);
			}
		}*/
		$return = $result->result();
		if($return){
			foreach ($return as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}
		return $return;
	}

	function get_products_brands($category=false,$count=true){
		$this->db->distinct();
		$this->db->select('brands.id, brands.name, COUNT(products.id) AS count');
		if($category !== false){
			$this->db->join('category_products', 'category_products.product_id=products.id');
			$this->db->where('category_products.category_id',$category);
		}
		$this->db->join('brands','brands.id=products.brand');
		$this->db->group_by('brands.id');
		$this->db->order_by('brands.name','ASC');
		$result = $this->db->get('products');
		return $result->result();
	}

	function get_merchant_products_brands($merchant_id=false,$count=true){
		$this->db->distinct();
		$this->db->select('brands.id, brands.name, COUNT(products.id) AS count');
		if($merchant_id !== false){
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		$this->db->join('brands','brands.id=products.brand');
		$this->db->group_by('brands.id');
		$this->db->order_by('brands.name','ASC');
		$result = $this->db->get('products');
		return $result->result();
	}

	function add_to_wishlist($save){
		$this->db->select('id');
		$this->db->from('customer_wishlist');
		$this->db->where('customer_id', $save['customer_id']);
		$this->db->where('product_id', $save['product_id']);
		$this->db->where('pm_id', $save['pm_id']);
		$count = $this->db->count_all_results();

		if ($count > 0){
			return false;
		} else {
			$this->db->insert('customer_wishlist', $save);
			return $this->db->insert_id();
		}		
	}

	function remove_from_wishlist($data){
		$this->db->where('customer_id', $data['customer_id']);
		$this->db->where('product_id', $data['product_id']);
		$this->db->where('pm_id', $data['pm_id']);
		$this->db->delete('customer_wishlist');
		return true;		
	}

	/*function get_customer_wishlist($customer_id){
		$this->db->select('products.*,(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, brands.id AS brand_id, brands.name AS brand_name', false);
		$this->db->from('customer_wishlist');
		$this->db->join('products', 'products.id=customer_wishlist.product_id');
		$this->db->join('products_merchants', 'products_merchants.id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		$this->db->where('customer_wishlist.pm_id','products_merchants.id');
		$this->db->where('customer_wishlist.customer_id',$customer_id);
		$this->db->where('products_merchants.enabled',1);
		$this->db->order_by('customer_wishlist.created_at', 'DESC');
		
		$result	= $this->db->get()->result();

		if($result){
			foreach ($result as &$p){
				$p->rating	= $this->get_avg_rating($p->id,true);
			}
		}

		return $result;
	}*/

	function get_customer_wishlist($customer_id){
		$this->db->select('products.*,(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name', false);
		$this->db->from('customer_wishlist');
		$this->db->join('products_merchants', 'products_merchants.id=customer_wishlist.pm_id');
		$this->db->join('products', 'products.id=products_merchants.product_id');
		$this->db->join('brands', 'products.brand = brands.id');
		$this->db->where('customer_wishlist.customer_id',$customer_id);
		$this->db->where('products_merchants.enabled',1);
		$this->db->order_by('customer_wishlist.created_at', 'DESC');
		
		$result	= $this->db->get()->result();
		return $result;
	}

	function get_product_sold_times($id){
		$this->db->where('product_id',$id);
		return $this->db->count_all_results('order_items');
	}

	function get_merchant_products($merchant_id, $limit = false, $offset = false, $by=false, $sort=false, $filters=array(),$count=false)
	{
		$this->db->distinct();
		if($count){
			$this->db->select('products.id, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price');
		} else {
			//$this->db->select('products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, products_merchants.payment_mode,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name, count(po.offer_id) as offers_count, MIN(po.offer_price) as offer_price', false);
			$this->db->select('products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, products_merchants.payment_mode,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name', false);
		}
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		/*if(!$count){
			$this->db->join('product_offers po', 'po.product_id = products.id AND po.offer_enabled=1', 'left outer');
		}*/

		if(isset($filters['category'])){
			if($filters['category'] != 0){
				$this->db->join('category_products', 'category_products.product_id=products.id');
				$this->db->where('category_id',$category);
			}
		}

		$this->db->where('products_merchants.merchant_id',$merchant_id);
		$this->db->where('products_merchants.enabled',1);

		if(isset($filters['min_price'])){
			$this->db->having('sort_price >=', $filters['min_price']);
		}
		if(isset($filters['max_price'])){
			$this->db->having('sort_price <=', $filters['max_price']);
		}
		if(isset($filters['brand_checked'])){
			if(!empty($filters['brand_checked'])){
				$countSql = count($filters['brand_checked']);
				foreach ($filters['brand_checked'] as $k => $brand_id) {
					if($k == 0){
						if($countSql > 1){
							$this->db->where('(products.brand = '.$brand_id);
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else if($k == $countSql-1){
						if($countSql > 1){
							$this->db->or_where('products.brand = '.$brand_id.')');
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else {
						$this->db->or_where('products.brand = '.$brand_id);
					}
				}
			}
		}

		if($count){
			$result	= $this->db->count_all_results();
		} else {
			if($by && $sort){
				$this->db->order_by($by, $sort);
			}
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();

			if($result){
				foreach ($result as &$p){
					$p->rating	= $this->get_avg_rating($p->id,true);
				}
			}
		}

		return $result;
	}

	function get_product_tax_by_category($id){
		$this->db->select('categories.tax_rate AS tax_rate');
		$row = $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->limit(1)->get('category_products')->row();
		if(!empty($row)){
			return $row->tax_rate;
		} else {
			return 0;
		}
	}

	function get_product_commission_by_category($id){
		$this->db->select('categories.commission AS commission');
		$row = $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->limit(1)->get('category_products')->row();
		if(!empty($row)){
			return $row->commission;
		} else {
			return 0;
		}
	}

	function increase_view_count($id){
		$this->db->where('id', $id);
		$this->db->set('views', 'views+1', FALSE);
		$this->db->update('products');
		return true;
	}

	function get_product_ids_category($id){
		$this->db->select('product_id');
		$this->db->where('category_id',$id);
		$result = $this->db->get('category_products');

		if($result->num_rows > 0){
			return $this->db->get('category_products')->result();	
		}
		return array();
	}

	function get_products_brand($brand_id = false, $limit = false, $offset = false, $by=false, $sort=false, $filters=array(),$count=false)
	{
		if ($brand_id)
		{
			$this->db->distinct();
			if($count){
				$this->db->select('products.id, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price');
			} else {
				$this->db->select('products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled,products_merchants.payment_mode,products_merchants.live_on', false);
			}
			$this->db->from('products');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->where('products_merchants.enabled',1);
			
			$this->db->where('products.brand',$brand_id);
			
			if(isset($filters['min_price'])){
				$this->db->having('sort_price >=', $filters['min_price']);
			}
			if(isset($filters['max_price'])){
				$this->db->having('sort_price <=', $filters['max_price']);
			}
			if(isset($filters['brand_checked'])){
				if(!empty($filters['brand_checked'])){
					$countSql = count($filters['brand_checked']);
					foreach ($filters['brand_checked'] as $k => $brand_id) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(products.brand = '.$brand_id);
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('products.brand = '.$brand_id.')');
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else {
							$this->db->or_where('products.brand = '.$brand_id);
						}
					}
				}
			}

			if(isset($filters['facets'])){
				if(!empty($filters['facets'])){
					
					$this->db->join('options', 'options.product_id = products.id');
					$this->db->join('option_values', 'option_values.option_id = options.id');

					$facets_keys 	= array();
					$facets_values 	= array();
					
					foreach ($filters['facets'] as $facet_key => $facet_value) {
						$facets_keys[] = $facet_key;

						if(!empty($facet_value)){
							$countSql = count($facet_value);
							foreach ($facet_value as $k => $facet_value_single) {
								$facets_values[] = $facet_value_single;
							}
						}
					}

					if(!empty($facets_keys)){
						$countSql = count($facets_keys);
						foreach ($facets_keys as $k => $facets_keys_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(options.name = "'.$facets_keys_single.'"');
								} else {
									$this->db->where('(options.name = "'.$facets_keys_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('options.name = "'.$facets_keys_single.'")');
								} else {
									$this->db->where('(options.name = "'.$facets_keys_single.'")');
								}
							} else {
								$this->db->or_where('options.name = "'.$facets_keys_single.'"');
							}
						}
					}

					if(!empty($facets_values)){
						$countSql = count($facets_values);
						foreach ($facets_values as $k => $facet_value_single) {
							if($k == 0){
								if($countSql > 1){
									$this->db->where('(option_values.name = "'.$facet_value_single.'"');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else if($k == $countSql-1){
								if($countSql > 1){
									$this->db->or_where('option_values.name = "'.$facet_value_single.'")');
								} else {
									$this->db->where('(option_values.name = "'.$facet_value_single.'")');
								}
							} else {
								$this->db->or_where('option_values.name = "'.$facet_value_single.'"');
							}
						}
					}
				}
			}

			$this->db->group_by('products.id');

			if($count){
				$result	= $this->db->count_all_results();
			} else {
				if($by && $sort){
					$this->db->order_by($by, $sort);
				}
				
				$result	= $this->db->limit($limit)->offset($offset)->get()->result();

				if($result){
					foreach ($result as &$p){
						$p->rating	= $this->get_avg_rating($p->id,true);
					}
				}
			}

			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}

	function get_products_brand_filter_options($brand_id=false){
		if ($brand_id)
		{
			$this->db->distinct();
			$this->db->select('options.name, option_values.name as ov_name, option_values.value as ov_value, option_values.image as ov_image', false);
			
			$this->db->from('products');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->join('options', 'options.product_id = products.id');
			$this->db->join('option_values', 'option_values.option_id = options.id');

			$this->db->where('products_merchants.enabled',1);
			
			$this->db->where('brand',$brand_id);

			if(isset($filters['min_price'])){
				$this->db->having('sort_price >=', $filters['min_price']);
			}
			if(isset($filters['max_price'])){
				$this->db->having('sort_price <=', $filters['max_price']);
			}
			if(isset($filters['brand_checked'])){
				if(!empty($filters['brand_checked'])){
					$countSql = count($filters['brand_checked']);
					foreach ($filters['brand_checked'] as $k => $brand_id) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(products.brand = '.$brand_id);
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('products.brand = '.$brand_id.')');
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else {
							$this->db->or_where('products.brand = '.$brand_id);
						}
					}
				}
			}

			$result	= $this->db->get()->result();
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}

	function get_products_ratings($count=false, $rows=false, $page=false){
        if($count){
            return $this->db->count_all_results('products_ratings');
        } else {
            $this->db->select('products.name, customers.firstname AS customer_firstname, customers.lastname AS customer_lastname, products_ratings.*');
            $this->db->join('products','products.id=products_ratings.product_id');
            $this->db->join('customers','customers.id=products_ratings.customer_id', 'left outer');
            $this->db->order_by('products_ratings.created_at','desc');

            if($rows){
                $this->db->limit($rows);
            }
            if($page){
                $this->db->offset($page);
            }
            return $this->db->get('products_ratings')->result();
        }
    }

    function rating_change($pr_id){
        $this->db->query('UPDATE products_ratings SET pr_enabled = IF(pr_enabled=1, 0, 1) WHERE id='.$pr_id);
        return $this->db->affected_rows();
    }

    function is_merchant_associated_to_ship($merchant_id){
    	$this->db->where('id',$merchant_id);
    	$service = $this->db->get('merchants')->row()->service;
    	if($service == 'Sell at Cartnyou/ Shipping Panel for COD'){
    		return true;
    	}
    	return false;
    }

    function set_merchant_products_prepaid($merchant_id){
    	$this->db->where('merchant_id', $merchant_id);
		$this->db->update('products_merchants', array('payment_mode'=>1));
		return true;
    }

    function get_total_products_value($merchant_id=false){
		$this->db->select('SUM(products_merchants.price) as val');
		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);	
		}
		$result = $this->db->get('products_merchants');
		return $result->row()->val;
	}

	function most_viewed($limit = 10, $offset = false, $category=false, $merchant_id=false, $count=false){
		$this->db->distinct();
		if($count){
			$this->db->select('products.id, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price');
		} else {
			$this->db->select('products.*, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.shipping_charges, products_merchants.quantity, products_merchants.enabled AS pm_enabled, products_merchants.payment_mode,products_merchants.live_on, brands.id AS brand_id, brands.name AS brand_name', false);
		}
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');

		if($category){
			$this->db->join('category_products', 'category_products.product_id=products.id');
			$this->db->where('category_id',$category);
		}

		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		$this->db->where('products_merchants.enabled',1);

		if($count){
			$result	= $this->db->count_all_results();
		} else {
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();

			/*if($result){
				foreach ($result as &$p){
					$p->rating	= $this->get_avg_rating($p->id,true);
				}
			}*/
		}

		return $result;
	}

	function get_searches(){
		return $this->db->get('search')->result();
	}

	function increase_product_stock($id, $add_quantity){
		$this->db->where('id', $id);
		$this->db->set('quantity', 'quantity+'.$add_quantity, FALSE);
		$this->db->update('products_merchants');
		return true;
	}

	function getProductOffers($id, $payment_mode=false, $admin=false,$isArray=false){
		if($payment_mode){
			return $this->db->query("SELECT * FROM (`product_offers`) WHERE `product_id` = '".$id."' AND `offer_payment_mode` = '".$payment_mode."' AND `offer_type` = 0 AND `offer_enabled` = 1 AND CASE WHEN offer_type=2 THEN (offer_start_at <= NOW() AND offer_expire_at >= NOW()) ELSE 1=1 END  ORDER BY `offer_price` ASC")->row();
		} else if($admin){
			return $this->db->query("SELECT * FROM (`product_offers`) WHERE `product_id` = '".$id."'")->result();
		} else if($isArray) {
			return $this->db->query("SELECT * FROM (`product_offers`) WHERE `product_id` = '".$id."' AND `offer_enabled` = 1 AND CASE WHEN offer_type=2 THEN (offer_start_at <= NOW() AND offer_expire_at >= NOW()) ELSE 1=1 END  ORDER BY `offer_price` ASC")->result_array();
		}else {
			return $this->db->query("SELECT * FROM (`product_offers`) WHERE `product_id` = '".$id."' AND `offer_enabled` = 1 AND CASE WHEN offer_type=2 THEN (offer_start_at <= NOW() AND offer_expire_at >= NOW()) ELSE 1=1 END  ORDER BY `offer_price` ASC")->result();
		}
	}

}