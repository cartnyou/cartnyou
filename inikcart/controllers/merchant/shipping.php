<?php

class Shipping extends Merchant_Controller {
	
	function __construct(){
		parent::__construct();

		$this->auth->check_access_merchant('Merchant', true);
		$this->load->model('Shipping_model');
		$this->lang->load('settings');
		$this->load->helper('inflector');
	}

	function index(){
		redirect(site_url('merchant/shipping/transactions'));
	}
	
	function transactions($page=0,$rows=15){
		$merchant_id = $this->session->userdata('merchant')['id'];

        $data['page_title'] 		= 'Shipping';
        $data['active_nav'] 		= 'shipping';
        $data['active_sub_menu']	= 'transactions';

        $this->load->library('pagination');
		
		$data['transactions'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id,'rows'=>$rows, 'page'=>$page));
		$data['total'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id),true);

		$config['base_url']			= site_url($this->config->item('merchant_folder').'/shipping/transactions');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 4;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination pull-right"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);

        $this->view($this->config->item('merchant_folder').'/shipping', $data);
	}

	function recharge_logs($page=0,$rows=15){
		$merchant_id = $this->session->userdata('merchant')['id'];

        $data['page_title'] 		= 'Recharge Logs';
        $data['active_nav'] 		= 'shipping';
        $data['active_sub_menu']	= 'recharge_logs';

        $this->load->library('pagination');
		
		$data['transactions'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id,'rows'=>$rows, 'page'=>$page), false,'recharge');
		$data['total'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id),true,'recharge');

		$config['base_url']			= site_url($this->config->item('merchant_folder').'/shipping/recharge_logs');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 4;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination pull-right"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);

        $this->view($this->config->item('merchant_folder').'/shipping', $data);
	}

	function courier_logs($page=0,$rows=15){
		$merchant_id = $this->session->userdata('merchant')['id'];

        $data['page_title'] 		= 'Courier Logs';
        $data['active_nav'] 		= 'shipping';
        $data['active_sub_menu']	= 'courier_logs';

        $this->load->library('pagination');
		
		$data['transactions'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id,'rows'=>$rows, 'page'=>$page), false,'deduction');
		$data['total'] = $this->Shipping_model->get_transactions(array('merchant_id'=>$merchant_id),true, 'deduction');

		$config['base_url']			= site_url($this->config->item('merchant_folder').'/shipping/courier_logs');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 4;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination pull-right"><ul class="pagination pagination-sm">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);

        $this->view($this->config->item('merchant_folder').'/shipping', $data);
	}
	
	function add_transaction($save){
		$time = time();
		$save['merchant_id'] = $this->session->userdata('merchant')['id'];
		//$save['amount'] = $this->input->post('amount');
		$save['order_id'] = 0;
		$save['weight'] = 0;
		$save['created_at'] = date('Y-m-d h:i:s',$time);
		$save['updated_at'] = date('Y-m-d h:i:s',$time);

		$shipping_transaction_id = $this->Shipping_model->add_transaction($save);
		if($shipping_transaction_id){
			return $shipping_transaction_id;
		} else {
			return false;
		}
	}

	function complete_payment(){
		$amount = trim($this->input->post('amount'));
		$merchant_id = $this->session->userdata('merchant')['id'];
		$this->load->model('Merchant_model');
		$merchant = $this->Merchant_model->get_merchant($merchant_id);

		$payment['module'] = 'payumoney';
		$this->load->add_package_path(APPPATH.'packages/payment/'.$payment['module'].'/');
		$this->load->library($payment['module']);
		
		$args = array();
		$args['cart_order_id'] = 'Shipping Recharge';
		$args['purchase_step'] = 'payment-method';

		$args['total'] = $amount;
		$args['furl'] = site_url('merchant/shipping/payumoney_cancel');
		$args['surl'] = site_url('merchant/shipping/payumoney_return');
		$args['firstname'] = $merchant->firstname;
		$args['lastname'] = $merchant->lastname;
		$args['address1'] = '';
		$args['address2'] = '';
		$args['city'] = '';
		$args['state'] = '';
		$args['zipcode'] = '';
		$args['country'] = 'India';
		$args['phone'] = $merchant->phone;
		$args['email'] = $merchant->email;

		$args['productinfo'] = 'Shipping Recharge';
		$args['amount'] = $amount;

		$args['processor_function'] = 'merchant/shipping/payment_process';

		$this->{$payment['module']}->process_payment($args);
	}

	function payment_process(){
		$args = $this->session->userdata("paymentDataArray");
		$this->load->view('payment_process', array('data'=>$args));
		$this->session->unset_userdata("paymentDataArray");
	}

	/*  Receive postback confirmation from payumoney to complete the customer's order.*/
	function payumoney_return(){
		$merchant_id = $this->session->userdata('merchant')['id'];
		$settings = $this->go_cart->CI->Settings_model->get_settings('payumoney');

		$status			= $_POST["status"];
		$firstname		= $_POST["firstname"];
		$amount 		= $_POST["amount"];
		$txnid 			= $_POST["txnid"];
		$posted_hash	= $_POST["hash"];
		$key			= $_POST["key"];
		$productinfo	= $_POST["productinfo"];
		$email			= $_POST["email"];
		$salt 			= $settings['merchant_salt'];

		if (isset($_POST["additionalCharges"])) {
		    $additionalCharges=$_POST["additionalCharges"];
		    $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		} else {	  
	        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        }
				
		$hash = hash("sha512", $retHashSeq);
				 
       	if ($hash != $posted_hash) {
	    	// Possible fake request; was not verified by payumoney. Could be due to a double page-get, should never happen under normal circumstances
			$this->session->set_flashdata('message', "<div>Transaction ID ".$txnid.". Payumoney did not validate your transaction. Either it has been processed already, or something else went wrong. If you believe there has been a mistake, please contact us.</div>");
		} else {
        	// The transaction is good
			$shipping_transaction_id = $this->add_transaction(array('amount'=>$amount,'txn_id'=>$txnid));
			if($shipping_transaction_id){

				$time = time();
				$transaction_save = array(
					'user_id'		=>	$merchant_id,
					'user_type'		=>	'merchant',
					'amount'		=>	$amount,
					'details'		=>	'Shipping Recharge',
					'internal_id'	=>	$shipping_transaction_id,
					'txn_id'		=>	$txnid,
					'status'		=>	'success',
					'created_at'	=>	date('Y-m-d h:i:s',$time),
					'updated_at'	=>	date('Y-m-d h:i:s',$time)
				);
				$this->load->model('Transaction_model');
				$merchant = $this->Transaction_model->add_transaction($transaction_save);

				$this->session->set_flashdata('message', "<div>Transaction ID ".$txnid.". Recharged successfully for <i class='fa fa-inr'></i> ".$amount."</div>");
			} else {
				$this->session->set_flashdata('message', "<div>Transaction ID ".$txnid.". Something went wrong!, Please contact us.</div>");
			}
		}
		redirect($this->config->item('merchant_folder').'/shipping');
	}
	
	function payumoney_cancel(){
		// User canceled using payumoney, send them back to the payment page
		$this->session->set_flashdata('message', "<div>Payumoney transaction canceled.</div>");
		redirect($this->config->item('merchant_folder').'/shipping');
	}

	//this is an alias of install
	function uninstall($module)
	{
		$this->install($module);
	}
	
	function settings($module)
	{
		$this->load->helper('form');
		$this->load->add_package_path(APPPATH.'packages/shipping/'.$module.'/');
		$this->load->library($module);
		
		//ok, in order for the most flexibility, and in case someone wants to use javascript or something
		//the form gets pulled directly from the library.
	
		if(count($_POST) >0)
		{
			$check	= $this->$module->check();
			if(!$check)
			{
				$this->session->set_flashdata('message', sprintf(lang('settings_updated'), $module));
				redirect($this->config->item('merchant_folder').'/shipping');
			}
			else
			{
				//set the error data and form data in the flashdata
				$this->session->set_flashdata('message', $check);
				$this->session->set_flashdata('post', $_POST);
				redirect($this->config->item('merchant_folder').'/shipping/settings/'.$module);
			}
		}
		elseif($this->session->flashdata('post'))
		{
			$data['form']		= $this->$module->form($this->session->flashdata('post'));
		}
		else
		{
			$data['form']		= $this->$module->form();
		}
		$data['module']		= $module;
		$data['page_title']	= sprintf(lang('shipping_settings'), humanize($module));
		$this->view($this->config->item('merchant_folder').'/shipping_module_settings', $data);
	}

	function rate_calculator($page=0,$rows=15){
		$merchant_id = $this->session->userdata('merchant')['id'];

        $data['page_title'] 		= 'Rate Calculator';
        $data['active_nav'] 		= 'shipping';
        $data['active_sub_menu']	= 'rate_calculator';

        $this->view($this->config->item('merchant_folder').'/rate_calculator', $data);
	}

	function calculate(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		//$this->form_validation->set_rules('pt​', 'Payment mode', 'trim|required');
		$this->form_validation->set_rules('cod', 'COD amount', 'trim');
		//$this->form_validation->set_rules('gm​', 'Weight', 'trim|required|numeric|floatval');
		$this->form_validation->set_rules('o_pin', 'Pickup Area Pincode', 'trim|required|regex_match[/^[0-9]{6}$/]');
		$this->form_validation->set_rules('d_pin', 'Delivery Area Pincode', 'trim|required|regex_match[/^[0-9]{6}$/]');
		
		if ($this->form_validation->run() == FALSE){
			$errors = array();
			foreach ($this->input->post() as $key => $value){
				if(form_error($key) != ''){
					$errors[$key] = form_error($key);
				}
			}

			$response['error'] = array_filter($errors);
			$response['result'] = FALSE;
			die(json_encode($response));
		} else {

			$data = array();
			$data['pt'] 	= trim($this->input->post('pt'));
			$data['gm'] 	= trim($this->input->post('gm'));
			$data['o_pin'] 	= trim($this->input->post('o_pin'));
			$data['d_pin'] 	= trim($this->input->post('d_pin'));

			$data['cl'] 	= 'GOJOJODEALS%20EXPRESS';
			$data['zn'] 	= 'A';
			$data['ss'] 	= 'Delivered';
			$data['md'] 	= 'E';

			if($this->input->post('cod')){
				$data['cod'] = trim($this->input->post('cod'));
			}

			$this->load->model('Generalmodel');
			$responseCharge = $this->Generalmodel->hit_delhivery('get','kinko/v1/invoice/charges',$data);
			$responseCharge = json_decode($responseCharge, true);


			if(isset($responseCharge[0]['total_amount'])){
			    $amount = $responseCharge[0]['total_amount'];
				$amount = $amount + (($amount * floatval(config_item('gojojo_delhivery_commission')))/100);
				die(json_encode(array('result'=>true,'data'=>$amount)));
			} else {
			    $response['data'] = 'Please fill all the fields properly.';
				$response['result'] = FALSE;
				die(json_encode($response));
			}
			/*if(isset($responseCharge['response'])){
				$amount = $responseCharge['response']['delivery_charges'];
				$amount = $amount + (($amount * floatval(config_item('gojojo_delhivery_commission')))/100);
				die(json_encode(array('result'=>true,'data'=>$amount)));
			} else {
				$response['data'] = 'Please fill all the fields properly.';
				$response['result'] = FALSE;
				die(json_encode($response));
			}*/
		}
	}

}
