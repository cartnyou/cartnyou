<div class="columns-container">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
        </div>
        <h2 class="page-heading">
            <span class="page-heading-title2">My Account</span>
        </h2>
        <div class="page-content">
            <div class="row">

            	<div class="col-md-12">
            		<?php if ($this->session->flashdata('message')):?>
						<div class="alert alert-info">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('message');?>
						</div>
					<?php endif;?>

					<?php if ($this->session->flashdata('error')):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('error');?>
						</div>
					<?php endif;?>

					<?php if (!empty($error)):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $error;?>
						</div>
					<?php endif;?>
				</div>

                <div class="col-sm-12">
                    <div class="box-authentication">
                        <h3><?php echo lang('forgot_password');?><span class="qs">?</span></h3>
						<?php echo form_open('secure/forgot_password', 'class="login_form"'); ?>
							<label>Enter the email address associated with your Gojojo Account</label>
							<input type="text" name="email" value="" class="form-control" />
							<button type="submit" class="button" name="submit"/><i class="fa fa-lock"></i> <?php echo lang('reset_password');?></button>
              <p>Has your email address changed<span class="qs">?</span> If you no longer use this e-mail address associated with your Gojojo account, you may contact customer service to help you restore access to your account></p>

							<div class="clearfix">
								<div class="pull-right"><a class="forgot_pass" href="#" data-toggle="modal" data-target="#loginModal"><?php echo lang('return_to_login');?></a></div>
							</div>

							<input type="hidden" value="submitted" name="submitted"/>

						</form>
                    </div>
                </div>

                <!-- <div class="col-sm-6">
                    <div class="box-authentication">
                    	<h3>NEW CUSTOMERS</h3>
						<p>Register with us to enjoy seemless experience, including:</p>
						<ul>
							<li>—  Online Order Status</li>
							<li>—  Sign up to receive exclusive news and offers</li>
							<li>—  Quick and easy checkout</li>
						</ul>
                        <a class="btn btn-theme-grey mt16" href="<?php echo site_url('secure/register'); ?>"><i class="fa fa-user"></i> Create an account</a>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
</div>
