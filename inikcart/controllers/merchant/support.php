<?php

class Support extends Merchant_Controller {

	function __construct()
	{
		parent::__construct();

		$this->auth->is_merchant_logged_in();
		
		$this->load->helper('date');
	}
	
	function index()
	{
		redirect(site_url('merchant/support/contact'));
	}

	function contact()
	{
		$data['page_title']			= 'Contact Gojojo Team';
		$data['active_nav'] 		= 'support';
		$data['active_sub_menu']	= 'contact';
		
		$this->view($this->config->item('merchant_folder').'/contact', $data);
	}

	function request_category()
	{
		$data['page_title']			= 'Request Selling in new Catgeory';
		$data['active_nav'] 		= 'support';
		$data['active_sub_menu']	= 'request_category';

		$merchant_id = $this->session->userdata('merchant')['id'];

		$this->load->model('Category_model');
		$data['categories']					= $this->Category_model->get_categories_tiered();
		$data['merchant_categories'] 		= array();
		$merchant_categories				= $this->Category_model->get_merchant_categories($merchant_id, true);
		if(!empty($merchant_categories)){
			foreach($merchant_categories as $merchant_category){
				$data['merchant_categories'][$merchant_category->category_id] 	= $merchant_category;
			}
		}

		$this->view($this->config->item('merchant_folder').'/request_category', $data);
	}

	function request_category_save()
	{
		$merchant_id 	= $this->session->userdata('merchant')['id'];
		$categories		= $this->input->post('categories');
		$merchant_id	= $merchant_id;
		$this->Category_model->save_merchant_request($categories, $merchant_id);
		$this->session->set_flashdata('message', 'Selling Categories updated successfully.');
		redirect(site_url('merchant/support/request_category'));
	}

}