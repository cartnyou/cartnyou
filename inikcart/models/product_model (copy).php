<?php
Class Product_model extends CI_Model
{
	
	function product_autocomplete($name, $limit)
	{
		return	$this->db->like('name', $name)->get('products', $limit)->result();
	}
	
	function products($data=array(), $return_count=false)
	{
		if(empty($data))
		{
			//if nothing is provided return the whole shabang
			if(!empty($data['merchant_id'])){
				$this->get_all_products($data['merchant_id']);
			} else {
				$this->get_all_products();
			}
		}
		else
		{
			$this->db->select('products.*, products_merchants.id as pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled as variant_enabled');

			$this->db->join('products_merchants', 'products_merchants.product_id=products.id', 'right');
			if(!empty($data['merchant_id'])){
				$this->db->where('products_merchants.merchant_id',$data['merchant_id']);
			}

			//grab the limit
			if(!empty($data['rows']))
			{
				$this->db->limit($data['rows']);
			}
			
			//grab the offset
			if(!empty($data['page']))
			{
				$this->db->offset($data['page']);
			}
			
			//do we order by something other than category_id?
			if(!empty($data['order_by']))
			{
				//if we have an order_by then we must have a direction otherwise KABOOM
				$this->db->order_by($data['order_by'], $data['sort_order']);
			}
			
			//do we have a search submitted?
			if(!empty($data['term']))
			{
				$search	= json_decode($data['term']);
				//if we are searching dig through some basic fields
				if(!empty($search->term))
				{
					$this->db->like('name', $search->term);
					$this->db->or_like('description', $search->term);
					$this->db->or_like('specification', $search->term);
					$this->db->or_like('excerpt', $search->term);
					$this->db->or_like('products_merchants.sku', $search->term);
				}
				
				if(!empty($search->category_id))
				{
					//lets do some joins to get the proper category products
					$this->db->join('category_products', 'category_products.product_id=products.id', 'right');
					$this->db->where('category_products.category_id', $search->category_id);
					$this->db->order_by('sequence', 'ASC');
				}
			}
			
			if($return_count)
			{
				return $this->db->count_all_results('products');
			}
			else
			{
				return $this->db->get('products')->result();
			}
			
		}
	}
	
	function get_all_products($merchant_id=false)
	{
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id', 'right');
		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}

		//sort by alphabetically by default
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('products');

		return $result->result();
	}
	
	function get_filtered_products($product_ids, $limit = false, $offset = false)
	{
		
		if(count($product_ids)==0)
		{
			return array();
		}
		
		$this->db->select('id, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('products');
		
		if(count($product_ids)>1)
		{
			$querystr = '';
			foreach($product_ids as $id)
			{
				$querystr .= 'id=\''.$id.'\' OR ';
			}
		
			$querystr = substr($querystr, 0, -3);
			
			$this->db->where($querystr, null, false);
			
		} else {
			$this->db->where('id', $product_ids[0]);
		}
		
		$result	= $this->db->limit($limit)->offset($offset)->get()->result();

		//die($this->db->last_query());

		$contents	= array();
		$count		= 0;
		foreach ($result as $product)
		{

			$contents[$count]	= $this->get_product($product->id);
			$count++;
		}

		return $contents;
		
	}
	
	/*function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$this->db->select('category_products.*, products.*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1));

			$this->db->order_by($by, $sort);
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();
			
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}*/

	function get_products($category_id = false, $limit = false, $offset = false, $by=false, $sort=false, $filters=array())
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			//$this->load->model('category_model');
			//$subCategories = $this->category_model->get_all_sub_categories($category_id,true);
			
			$this->db->select('category_products.*, products.*, (CASE WHEN products_merchants.saleprice > 0 THEN (products_merchants.price - products_merchants.saleprice) ELSE 0 END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled, brands.id AS brand_id, brands.name AS brand_name', false);
			$this->db->from('category_products');
			$this->db->join('products', 'category_products.product_id=products.id');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$this->db->join('brands', 'products.brand = brands.id');
			$this->db->where('products_merchants.enabled',1);
			
			// if(!empty($subCategories)){
			// 	$this->db->where('(category_id = '.$category_id);
			// 	foreach ($subCategories as $k => $subCategory) {
			// 		if($k == count($subCategories)-1){
			// 			$this->db->or_where('category_id = '.$subCategory.')');
			// 		} else {
			// 			$this->db->or_where('category_id = '.$subCategory);
			// 		}
			// 	}
			// } else {
			// 	$this->db->where('category_id',$category_id);
			// }
			$this->db->where('category_id',$category_id);

			if(isset($filters['min_price'])){
				$this->db->having('sort_price >=', $filters['min_price']);
			}
			if(isset($filters['max_price'])){
				$this->db->having('sort_price <=', $filters['max_price']);
			}
			if(isset($filters['brand_checked'])){
				if(!empty($filters['brand_checked'])){
					$countSql = count($filters['brand_checked']);
					foreach ($filters['brand_checked'] as $k => $brand_id) {
						if($k == 0){
							if($countSql > 1){
								$this->db->where('(products.brand = '.$brand_id);
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else if($k == $countSql-1){
							if($countSql > 1){
								$this->db->or_where('products.brand = '.$brand_id.')');
							} else {
								$this->db->where('(products.brand = '.$brand_id.')');
							}
						} else {
							$this->db->or_where('products.brand = '.$brand_id);
						}
					}
				}
			}

			$this->db->order_by($by, $sort);
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();
			
			return $result;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('name', 'ASC');
			$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
			$result	= $this->db->get('products');

			return $result->result();
		}
	}
	
	function count_all_products()
	{
		return $this->db->count_all_results('products');
	}
	
	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	function get_product($id, $related=true, $merchant_id='', $all_merchnats=false)
	{
		$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled, brands.name as brand_name, merchants.store_name');
		$this->db->where('products.id',$id);
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('merchants', 'merchants.id = products_merchants.merchant_id');
		$this->db->join('brands', 'products.brand = brands.id');

		if($merchant_id != ''){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}

		if($all_merchnats){
			$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);
			$result	= $this->db->get('products')->result();
			if(!$result)
			{
				return false;
			}

			$related	= json_decode($result[0]->related_products);
			
			if(!empty($related))
			{
				/*$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled, brands.name as brand_name');
				$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
				$this->db->join('brands', 'products.brand = brands.id');

				//build the where
				$where = array();
				foreach($related as $r)
				{
					$where[] = '`products`.`id` = '.$r;
				}

				$this->db->where('('.implode(' OR ', $where).')', null);
				$this->db->where('products_merchants.enabled', 1);

				$this->db->group_by('products.id');
				$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);

				$result[0]->related_products	= $this->db->get('products')->result();*/

				$relateds = array();
				foreach($related as $r)
				{
					$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled, brands.name as brand_name');
					$this->db->where('products.id',$r);
					$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
					$this->db->join('brands', 'products.brand = brands.id');
					$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);

					$p = $this->db->get('products')->row();
					if(!empty($p)){
						$relateds[] = $p;
					}
				}
				$result[0]->related_products = $relateds;
				//print_r($result[0]->related_products);exit;

			}
			else
			{
				$result[0]->related_products	= array();
			}
			$result[0]->categories			= $this->get_product_categories($result[0]->id);

			return $result;

		} else {
			$result	= $this->db->get('products')->row();
			if(!$result)
			{
				return false;
			}

			$related	= json_decode($result->related_products);
			
			if(!empty($related))
			{
				//build the where
				$where = array();
				foreach($related as $r)
				{
					$where[] = '`id` = '.$r;
				}

				$this->db->where('('.implode(' OR ', $where).')', null);
				$this->db->where('enabled', 1);

				$result->related_products	= $this->db->get('products')->result();
			}
			else
			{
				$result->related_products	= array();
			}
			$result->categories			= $this->get_product_categories($result->id);

			return $result;
		}
	}

	function get_product_by_variant($variant_id)
	{
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->where('products_merchants.id',$variant_id);
		$result	= $this->db->get('products')->row();
		if(!$result)
		{
			return false;
		}

		$related	= json_decode($result->related_products);
		
		if(!empty($related))
		{
			//build the where
			$where = array();
			foreach($related as $r)
			{
				$where[] = '`id` = '.$r;
			}

			$this->db->where('('.implode(' OR ', $where).')', null);
			$this->db->where('enabled', 1);

			$result->related_products	= $this->db->get('products')->result();
		}
		else
		{
			$result->related_products	= array();
		}
		$result->categories			= $this->get_product_categories($result->id);

		return $result;
	}

	function get_product_categories($id)
	{
		return $this->db->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
	}

	function get_slug($id,$pm_id=false)
	{
		$this->db->select('products_merchants.slug');
		$this->db->where('products.id',$id);
		$this->db->where('products_merchants.id',$pm_id);
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		return $this->db->get('products')->row()->slug;

		//return $this->db->get_where('products', array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false, $productMerchant=array())
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}
		else
		{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		$this->db->where('product_id', $id);
		$this->db->where('merchant_id', $productMerchant['merchant_id']);
		$countPM = $this->db->count_all_results('products_merchants');

		$productMerchant['product_id'] = $id;
		if($countPM > 0){
			$this->db->where('product_id', $id);
			$this->db->where('merchant_id', $productMerchant['merchant_id']);
			$this->db->update('products_merchants', $productMerchant);
		} else {
			$this->db->insert('products_merchants', $productMerchant);
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//generate cat_id array
				$ids	= array();
				foreach($cats as $c)
				{
					$ids[]	= $c->id;
				}

				//eliminate categories that products are no longer in
				foreach($ids as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $ids))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		
		//return the product id
		return $id;
	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

	}

	function delete_variant($id)
	{
		// delete variant 
		$this->db->where('id', $id);
		$this->db->delete('products_merchants');
	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	function search_products($term, $limit=false, $offset=false, $by=false, $sort=false,$filters=array(),$count=false){
		// if($filters['category'] != 0) {
		// 	$this->load->model('category_model');
		// 	$subCategories = $this->category_model->get_all_sub_categories($filters['category'],true);	
		// }
		
		/*$this->db->select('category_products.*, products.*, (CASE WHEN products_merchants.saleprice > 0 THEN (products_merchants.price - products_merchants.saleprice) ELSE 0 END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled, brands.id AS brand_id, brands.name AS brand_name', false);
		$this->db->from('category_products');
		$this->db->join('products', 'category_products.product_id=products.id');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		$this->db->where('products_merchants.enableds',1);
		$this->db->where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.specification LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products_merchants.sku LIKE "%'.$term.'%")');*/

		$this->db->distinct();
		if($count){
			$this->db->select('products.id');
		} else {
			$this->db->select('products.*, (CASE WHEN products_merchants.saleprice > 0 THEN (products_merchants.price - products_merchants.saleprice) ELSE 0 END) as sort_price, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled, brands.id AS brand_id, brands.name AS brand_name', false);
		}
		$this->db->from('products');
		$this->db->join('category_products', 'category_products.product_id=products.id');
		$this->db->join('products_merchants', 'products_merchants.product_id=products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		$this->db->where('products_merchants.enabled',1);
		$this->db->where('(products.name LIKE "%'.$term.'%" OR products.description LIKE "%'.$term.'%" OR products.specification LIKE "%'.$term.'%" OR products.excerpt LIKE "%'.$term.'%" OR products_merchants.sku LIKE "%'.$term.'%")');
		
		/*if($filters['category'] != 0) {
			if(!empty($subCategories)){
				$this->db->where('(category_id = '.$filters['category']);
				foreach ($subCategories as $k => $subCategory) {
					if($k == count($subCategories)-1){
						$this->db->or_where('category_id = '.$subCategory.')');
					} else {
						$this->db->or_where('category_id = '.$subCategory);
					}
				}
			} else {
				$this->db->where('category_id',$filters['category']);
			}
		}*/
		if($filters['category'] != 0) {
			$this->db->where('category_id',$filters['category']);
		}

		if(isset($filters['min_price'])){
			$this->db->having('sort_price >=', $filters['min_price']);
		}
		if(isset($filters['max_price'])){
			$this->db->having('sort_price <=', $filters['max_price']);
		}
		if(isset($filters['brand_checked'])){
			if(!empty($filters['brand_checked'])){
				$countSql = count($filters['brand_checked']);
				foreach ($filters['brand_checked'] as $k => $brand_id) {
					if($k == 0){
						if($countSql > 1){
							$this->db->where('(products.brand = '.$brand_id);
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else if($k == $countSql-1){
						if($countSql > 1){
							$this->db->or_where('products.brand = '.$brand_id.')');
						} else {
							$this->db->where('(products.brand = '.$brand_id.')');
						}
					} else {
						$this->db->or_where('products.brand = '.$brand_id);
					}
				}
			}
		}

		if($count){
			$result	= $this->db->count_all_results();
		} else {
			if($by && $sort){
				$this->db->order_by($by, $sort);
			}
			
			$result	= $this->db->limit($limit)->offset($offset)->get()->result();
		}
		
		return $result;
	}

	/*function search_products($term, $limit=false, $offset=false, $by=false, $sort=false)
	{
		$results		= array();
		
		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one counts the total number for our pagination
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR specification LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		$results['count']	= $this->db->count_all_results('products');


		$this->db->select('*, LEAST(IFNULL(NULLIF(saleprice, 0), price), price) as sort_price', false);
		//this one gets just the ones we need.
		$this->db->where('enabled', 1);
		$this->db->where('(name LIKE "%'.$term.'%" OR description LIKE "%'.$term.'%" OR specification LIKE "%'.$term.'%" OR excerpt LIKE "%'.$term.'%" OR sku LIKE "%'.$term.'%")');
		
		if($by && $sort)
		{
			$this->db->order_by($by, $sort);
		}
		
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		
		return $results;
	}*/

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false,$pm_id=false)
	{
		//$product	= $this->db->get_where('products', array('id'=>$id))->row();

		$this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled, brands.name as brand_name');
		$this->db->where('products.id',$id);
		$this->db->where('products_merchants.id',$pm_id);
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('brands', 'products.brand = brands.id');
		$product	= $this->db->get('products')->row();
		
		//unset some of the additional fields we don't need to keep
		if(!$product)
		{
			return false;
		}
		
		$product->base_price	= $product->price;
		
		if ($product->saleprice != 0.00)
		{ 
			$product->price	= $product->saleprice;
		}
		
		
		// Some products have n/a quantity, such as downloadables
		//overwrite quantity of the product with quantity requested
		if (!$quantity || $quantity <= 0 || $product->fixed_quantity==1)
		{
			$product->quantity = 1;
		}
		else
		{
			$product->quantity = $quantity;
		}
		
		
		// attach list of associated downloadables
		$product->file_list	= $this->Digital_Product_model->get_associations_by_product($id);
		
		return (array)$product;
	}

	function add_review($product_id,$review,$rating,$customer_id){
		$time = date('Y-m-d H:i:s',time());
		$this->db->insert('products_ratings', array('product_id'=>$product_id,'customer_id'=>$customer_id,'rating'=>$rating,'review'=>$review,'created_at'=>$time,'updated_at'=>$time));
	}

	function get_product_reviews($product_id){
		$result	= $this->db->select('products_ratings.id AS pr_id,rating,review,created_at,customers.id AS customer_id,firstname,lastname')->where('products_ratings.product_id',$product_id)->join('customers','customers.id=products_ratings.customer_id')->get('products_ratings')->result();
		if(!$result){
			return false;
		}
		return $result;
	}

	function get_avg_rating($product_id){
		$result	= $this->db->select('AVG(rating) AS rating, COUNT(id) AS count')->where('product_id',$product_id)->get('products_ratings')->row();
		if(!$result){
			return false;
		}
		return $result;
	}

	function popular_products($limit = false, $offset = false, $category = false){
		/*$this->db->select('products.*, COUNT(order_items.product_id) as ordered_quantity,products_merchants.id AS pm_id, LEAST(IFNULL(NULLIF(products_merchants.saleprice, 0), products_merchants.price), products_merchants.price) as sort_price', false);
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('order_items', 'order_items.product_id=products.id');
		$this->db->group_by('products.id');
		$this->db->where(array('products_merchants.enabled'=>1));
		$this->db->order_by('ordered_quantity','DESC');*/

		/*$this->db->select('products.*, COUNT(order_items.product_id) as ordered_quantity, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled');
		$this->db->from('products');
		$this->db->join('products_merchants', 'products_merchants.product_id = products.id');
		$this->db->join('order_items', 'order_items.product_id=products.id');
		$this->db->where('products_merchants.enabled',1);
		$this->db->group_by('products.id');

		$this->db->order_by('ordered_quantity','DESC');
		$this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);
		

		$result	= $this->db->limit($limit)->offset($offset)->get()->result();*/

		//$result = $this->db->query("SELECT subUp.* FROM (SELECT sub.*, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled FROM ( SELECT products.*,COUNT(order_items.product_id) as ordered_quantity FROM products JOIN order_items ON order_items.product_id=products.id GROUP by products.id ORDER BY ordered_quantity DESC LIMIT 10) AS sub JOIN products_merchants ON products_merchants.product_id = sub.id WHERE products_merchants.enabled = 1 ORDER BY (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp GROUP BY subUp.id ");

		$query 	= "SELECT subUp.* FROM (SELECT sub.*, products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled FROM ( SELECT products.*,COUNT(order_items.product_id) as ordered_quantity FROM products JOIN order_items ON order_items.product_id=products.id ";

		if($category !== false){
			$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' ";
		}

		$query .= "GROUP by products.id ORDER BY ordered_quantity DESC LIMIT 10) AS sub JOIN products_merchants ON products_merchants.product_id = sub.id WHERE products_merchants.enabled = 1 ORDER BY (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp";
		
		$result = $this->db->query($query);
		
		/*if($result){
			foreach ($result as &$p){
				$p->images	= (array)json_decode($p->images);
				//$p->options	= $this->Option_model->get_product_options($p->id);
			}
		}*/
		return $result->result();
	}

	function sale_products($limit = false,$category=false){
		$query 	= "SELECT subUp.* FROM (SELECT products.*, (CASE WHEN products_merchants.saleprice > 0 THEN (products_merchants.price - products_merchants.saleprice) ELSE 0 END) AS discount ,products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled FROM products JOIN products_merchants ON products_merchants.product_id = products.id ";
		
		if($category !== false){
			$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' AND ";
		} else {
			$query .= "WHERE ";
		}

		$query .= "products_merchants.enabled = 1 HAVING discount>0 ORDER BY discount desc, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC) AS subUp LIMIT ".$limit;
		$result = $this->db->query($query);
		return $result->result();
	}

	function new_products($limit = false,$category=false){
		$query 	= "SELECT products.*,products_merchants.id AS pm_id, products_merchants.merchant_id, products_merchants.sku, products_merchants.slug, products_merchants.route_id, products_merchants.price, products_merchants.saleprice, products_merchants.free_shipping, products_merchants.quantity, products_merchants.enabled AS pm_enabled FROM products JOIN products_merchants ON products_merchants.product_id = products.id ";

		if($category !== false){
			$query .= "JOIN category_products ON category_products.product_id=products.id WHERE category_products.category_id='".$category."' AND ";
		} else {
			$query .= "WHERE ";
		}

		$query .= "products_merchants.enabled = 1 ORDER BY products.created_at desc, (CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC LIMIT ".$limit;
		$result = $this->db->query($query);
		return $result->result();
	}

	function get_products_brands($category=false,$count=true){
		$this->db->distinct();
		$this->db->select('brands.id, brands.name, COUNT(products.id) AS count');
		if($category !== false){
			$this->db->join('category_products', 'category_products.product_id=products.id');
			$this->db->where('category_products.category_id',$category);
		}
		$this->db->join('brands','brands.id=products.brand');
		$this->db->group_by('brands.id');
		$this->db->order_by('brands.name','ASC');
		$result = $this->db->get('products');
		return $result->result();
	}
}