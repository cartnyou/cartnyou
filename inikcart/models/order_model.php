<?php
Class order_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/*function get_gross_monthly_sales($year)
	{
		$this->db->select('SUM(coupon_discount) as coupon_discounts');
		$this->db->select('SUM(gift_card_discount) as gift_card_discounts');
		$this->db->select('SUM(subtotal) as product_totals');
		$this->db->select('SUM(shipping) as shipping');
		$this->db->select('SUM(tax) as tax');
		$this->db->select('SUM(total) as total');
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->select('MONTH(ordered_on) as month');
		$this->db->group_by(array('MONTH(ordered_on)'));
		$this->db->order_by("ordered_on", "desc");
		$this->db->where('YEAR(ordered_on)', $year);
		
		return $this->db->get('orders')->result();
	}*/

	function get_gross_monthly_sales($year, $merchant_id=false){
		$this->db->select('SUM(order_items.subtotal) as total, YEAR(ordered_on) as year, MONTH(ordered_on) as month');
		if($merchant_id){
			$this->db->join('products_merchants', 'products_merchants.id = order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);	
		}
		$this->db->join('orders', 'order_items.order_id = orders.id');
		$this->db->group_by(array('MONTH(ordered_on)'));
		$this->db->order_by("ordered_on", "asc");
		$this->db->where('YEAR(ordered_on)', $year);
		$result = $this->db->get('order_items');
		return $result->result();
	}

	function get_gross_monthly_orders($year, $merchant_id=false){
		$this->db->select('COUNT(DISTINCT order_items.order_id) as total, YEAR(ordered_on) as year, MONTH(ordered_on) as month');
		if($merchant_id){
			$this->db->join('products_merchants', 'products_merchants.id = order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);	
		}
		$this->db->join('orders', 'order_items.order_id = orders.id');
		$this->db->group_by(array('MONTH(ordered_on)'));
		$this->db->order_by("ordered_on", "asc");
		$this->db->where('YEAR(ordered_on)', $year);
		$result = $this->db->get('order_items');
		return $result->result();
	}
	
	function get_sales_years()
	{
		$this->db->order_by("ordered_on", "desc");
		$this->db->select('YEAR(ordered_on) as year');
		$this->db->group_by('YEAR(ordered_on)');
		$records	= $this->db->get('orders')->result();
		$years		= array();
		foreach($records as $r)
		{
			$years[]	= $r->year;
		}
		return $years;
	}

	function get_order_stats($year, $merchant_id)
	{		
		$this->db->select('orders.id');
		$this->db->join('order_items','order_items.order_id=orders.id');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		$this->db->where('products_merchants.merchant_id',$merchant_id);
		$this->db->group_by(array('orders.id'));
		$this->db->where('YEAR(ordered_on)', $year);
		$result = $this->db->get('orders')->result();

		if($result){
			foreach ($result as &$o){
				$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
			}
		}
		return $result;
	}
	
	/*function get_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false)
	{		
		$this->db->select('orders.*');	
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";
					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}

		if($merchant_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			$this->db->group_by(array('orders.id'));
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		
		$result = $this->db->get('orders')->result();

		if($merchant_id){
			if($result){
				foreach ($result as &$o){
					$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
				}
			}
		}
		return $result;
	}
	
	function get_orders_count($search=false,$merchant_id=false)
	{			
		$this->db->select('orders.id');
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}

		if($merchant_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			$this->db->group_by(array('orders.id'));
		}
		
		//return $this->db->get('orders');
		return $this->db->get('orders')->num_rows();
	}*/
	
	//shahwaz
	
	function get_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false)
	{	
		$this->db->select('orders.*,merchants.store_name,order_items.product_id,order_items.total as product_total,order_items.tax as product_tax,order_items.subtotal as product_subtotal,
						order_items.shipping as product_shipping,order_items.coupon_discount as product_coupon_discount,order_items.id as item_id,order_items.pm_id,order_items_status.ots_status,
						order_items.contents,order_items.quantity as product_quantity,order_items.waybill_method,order_items_status.notes as product_notes,
						products.pc,products.il,products.ml,products.cl,products.pl,categories.name as category_name');	
		$this->db->from('orders');
		$this->db->join('order_items','order_items.order_id=orders.id');
		$this->db->join('order_items_status','order_items_status.order_item_id=order_items.id AND order_items_status.ots_id=(SELECT MAX(ots_id) from order_items_status where order_item_id=order_items.id)');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		$this->db->join('products','products.id=order_items.product_id');
		$this->db->join('merchants','merchants.id=products_merchants.merchant_id');
		$this->db->join('category_products','category_products.product_id=order_items.product_id');
		$this->db->join('categories','categories.id=category_products.category_id');
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( orders.order_number ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." orders.bill_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.bill_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.status ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." merchants.store_name ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." order_items.product_id ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." orders.notes ".$not."LIKE '%".$t."%' )";
					$this->db->where($like);
				}	
			}
			if(!empty($search->search_status))
			{
				$this->db->where('order_items_status.ots_status',$search->search_status);
			}
			if(!empty($search->start_date))
			{
				$this->db->where('orders.ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('orders.ordered_on <',$search->end_date);
			}
			
		}

		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			//$this->db->group_by(array('orders.id'));
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		//$this->db->group_by('category_products.product_id');
		$result = $this->db->get()->result();

		if($merchant_id){
			if($result){
				foreach ($result as &$o){
					$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
				}
			}
		}
		return $result;
	}
	
	function get_orders_count($search=false,$merchant_id=false)
	{			
		$this->db->select('orders.id');
		$this->db->from('orders');
		$this->db->join('order_items','order_items.order_id=orders.id');
		$this->db->join('order_items_status','order_items_status.order_item_id=order_items.id AND order_items_status.ots_id=(SELECT MAX(ots_id) from order_items_status where order_item_id=order_items.id)');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		$this->db->join('products','products.id=order_items.product_id');
		$this->db->join('merchants','merchants.id=products_merchants.merchant_id');
		$this->db->join('category_products','category_products.product_id=order_items.product_id');
		$this->db->join('categories','categories.id=category_products.category_id');
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( orders.order_number ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." orders.bill_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.bill_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.status ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." merchants.store_name ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." order_items.product_id ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." orders.notes ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->search_status))
			{
				$this->db->where('order_items_status.ots_status',$search->search_status);
			}
			if(!empty($search->start_date))
			{
				$this->db->where('orders.ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				$this->db->where('orders.ordered_on <',$search->end_date);
			}
			
		}

		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			//$this->db->group_by(array('orders.id'));
		}
		
		//return $this->db->get('orders');
		//$this->db->group_by('category_products.product_id');
		return $this->db->get()->num_rows();
	}
	
	function get_orders_filter($search=false,$merchant_id=false)
	{		
		$this->db->select('order_items.id');
		$this->db->from('order_items');
		$this->db->join('order_items_status','order_items_status.order_item_id=order_items.id AND order_items_status.ots_id=(SELECT MAX(ots_id) from order_items_status where order_item_id=order_items.id)');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		if ($search)
		{
			if(!empty($search->search_status))
			{
				$this->db->where('order_items_status.ots_status',$search->search_status);
			}
			if(!empty($search->search_last))
			{
				$this->db->where('order_items_status.created_at >',$search->search_last);
			}
			if(!empty($search->search_before))
			{
				$this->db->where('order_items_status.created_at <=',$search->search_before);
			}
			
		}
		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		
		return $this->db->get()->num_rows();
	}
	
	function get_orders_sales($filter=false,$merchant_id=false)
	{
		$this->db->select('SUM(order_items.total) as sales');
		$this->db->from('order_items');
		$this->db->join('order_items_status','order_items_status.order_item_id=order_items.id AND order_items_status.ots_id=(SELECT MAX(ots_id) from order_items_status where order_item_id=order_items.id)');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		if ($filter)
		{
			if(!empty($filter->search_status))
			{
				$this->db->where('order_items_status.ots_status',$filter->search_status);
			}
			if(isset($filter->date_from))
			{
				$this->db->where('order_items_status.created_at >',$filter->date_from);
			}
			if(isset($filter->date_to))
			{
				$this->db->where('order_items_status.created_at <=',$filter->date_to);
			}
			
		}
		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		$query=$this->db->get();
		if($query->row()->sales!=null){
			return $query->row()->sales;
		}else{
			return 0;
		}
	}
	
	function get_orders_export($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false)
	{	
		$this->db->select('orders.*,merchants.store_name,order_items.product_id,order_items.total as product_total,order_items.tax as product_tax,order_items.subtotal as product_subtotal,
						order_items.shipping as product_shipping,order_items.coupon_discount as product_coupon_discount,order_items.id as item_id,order_items.pm_id,order_items_status.ots_status,
						order_items.waybill_method,order_items_status.notes as product_notes');	
		$this->db->from('orders');
		$this->db->join('order_items','order_items.order_id=orders.id');
		$this->db->join('order_items_status','order_items_status.order_item_id=order_items.id AND order_items_status.ots_id=(SELECT MAX(ots_id) from order_items_status where order_item_id=order_items.id)');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		$this->db->join('merchants','merchants.id=products_merchants.merchant_id');
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( orders.order_number ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." orders.bill_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.bill_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_firstname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.ship_lastname ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." orders.status ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." merchants.store_name ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." order_items.product_id ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." orders.notes ".$not."LIKE '%".$t."%' )";
					$this->db->where($like);
				}	
			}
			if(!empty($search->search_status))
			{
				$this->db->where('order_items_status.ots_status',$search->search_status);
			}
			if(!empty($search->start_date))
			{
				$this->db->where('orders.ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('orders.ordered_on <',$search->end_date);
			}
			
		}

		if($merchant_id){
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			//$this->db->group_by(array('orders.id'));
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		$result = $this->db->get()->result();

		if($merchant_id){
			if($result){
				foreach ($result as &$o){
					$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
				}
			}
		}
		return $result;
	}
	
	//ends here

	
	
	//get an individual customers orders
	function get_customer_orders($id, $offset=0, $limit=10)
	{
		$this->db->order_by('ordered_on', 'DESC');
		$this->db->where('customer_id',$id);
		$this->db->limit($limit)->offset($offset);
		return $this->db->get('orders')->result();
		//return $this->db->get_where('orders', array('customer_id'=>$id), 15, $offset)->result();
	}
	
	function count_customer_orders($id)
	{
		$this->db->where(array('customer_id'=>$id));
		return $this->db->count_all_results('orders');
	}
	
	function get_order($id,$customer_id=false,$merchant_id=false)
	{
		$this->db->select('orders.*');
		$this->db->where('orders.id', $id);
		if($customer_id){
			$this->db->where('customer_id', $customer_id);
		}
		if($merchant_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		$result 			= $this->db->get('orders');

		if($result->num_rows() > 0){
		
			$order				= $result->row();
			$order->contents	= $this->get_items($order->id,$merchant_id,true);
			
			return $order;
		} else {
			return false;
		}
	}

	function get_order_by_item_package($item_id)
	{
		$this->db->select('orders.*, products_merchants.merchant_id');
		$this->db->where('order_items.id', $item_id);
		$this->db->join('order_items','order_items.order_id=orders.id');
		$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
		$result 			= $this->db->get('orders');

		if($result->num_rows() > 0){
		
			$order				= $result->row();
			$order->contents	= $this->get_items($order->id,$order->merchant_id);
			
			return $order;
		} else {
			return false;
		}
	}

	function get_package_ready_order_item($id,$customer_id=false,$merchant_id=false, $order_item_id=false)
	{
		$this->db->select('orders.*, merchants.id as merchant_id,merchants.store_name,merchants.firstname as merchant_firstname,merchants.lastname as merchant_lastname,merchants.phone as merchant_phone,merchants.address as merchant_address,merchants.city as merchant_city,merchants.state as merchant_state,merchants.zip as merchant_zip,merchants.tin as merchant_tin,merchants.cst as merchant_cst,merchants.sign as merchant_sign,merchants.pan_number as merchant_pan');
		$this->db->where('orders.id', $id);
		if($customer_id){
			$this->db->where('customer_id', $customer_id);
		}
		if($merchant_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->join('merchants','merchants.id=products_merchants.merchant_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
			$this->db->where('merchants.id', $merchant_id);
		} else if($order_item_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->join('merchants','merchants.id=products_merchants.merchant_id');
			$this->db->where('order_items.id', $order_item_id);
		}
		$result 			= $this->db->get('orders');

		if($result->num_rows() > 0){
		
			$order				= $result->row();
			$order->contents	= $this->get_items($order->id,$merchant_id,false,$order_item_id);
			return $order;
		} else {
			return false;
		}
	}

	function get_order_by_order_number($id,$customer_id=false,$merchant_id=false)
	{
		$this->db->where('order_number', $id);
		if($customer_id){
			$this->db->where('customer_id', $customer_id);
		}
		if($merchant_id){
			$this->db->join('order_items','order_items.order_id=orders.id');
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}
		$result 			= $this->db->get('orders');
		
		$order				= $result->row();
		$order->contents	= $this->get_items($order->id,$merchant_id, true);
		
		return $order;
	}	
	
	function get_items($id,$merchant_id=false, $with_latest_status=false,$order_item_id=false)
	{
		$this->db->select('order_id, order_items.id AS order_item_id, contents, order_items.merchant_invoice_number, order_items.waybill,order_items.waybill_method');
		$this->db->where('order_id', $id);
		
		if($merchant_id){
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}else if($order_item_id){
			$this->db->where('order_items.id',$order_item_id);
		}

		$result	= $this->db->get('order_items');

		$items	= $result->result_array();
		
		$return	= array();
		$count	= 0;
		foreach($items as $item)
		{

			$item_content	= unserialize($item['contents']);
			
			//remove contents from the item array
			unset($item['contents']);
			$return[$count]	= $item;
			
			//merge the unserialized contents with the item array
			$return[$count]	= array_merge($return[$count], $item_content);

			if($with_latest_status){
				$return[$count]['latest_status'] = $this->get_order_item_statuses($item['order_item_id'],true);
			}
			
			$count++;
		}
		return $return;
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('orders');
		
		//now delete the order items
		$this->db->where('order_id', $id);
		$this->db->delete('order_items');
	}
	
	function save_order($data, $contents = false)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('orders', $data);
			$id = $data['id'];
			
			// we don't need the actual order number for an update
			$order_number = $id;
		}
		else
		{
			$this->db->insert('orders', $data);
			$id = $this->db->insert_id();
			
			//create a unique order number
			//unix time stamp + unique id of the order just submitted.
			$order	= array('order_number'=> date('U').$id);
			
			//update the order with this order id
			$this->db->where('id', $id);
			$this->db->update('orders', $order);
						
			//return the order id we generated
			$order_number = $order['order_number'];
		}
		
		//if there are items being submitted with this order add them now
		if($contents)
		{
			// clear existing order items
			$this->db->where('order_id', $id)->delete('order_items');
			// update order items
			foreach($contents as $item)
			{
				$save					= array();
				$save['contents']		= $item;
				
				$item					= unserialize($item);
				$save['product_id'] 	= $item['id'];
				$save['pm_id'] 			= $item['pm_id'];
				$save['quantity'] 		= $item['quantity'];
				$save['order_id']		= $id;
				$save['tax']			= $item['tax'];
				$save['total']			= $item['total'];
				$save['subtotal']		= $item['subtotal'];
				$save['coupon_discount']= $item['coupon_discount']*$item['quantity'];
				$save['shipping']		= $item['shipping'];

				$this->db->insert('order_items', $save);
				$temp_id = $this->db->insert_id();

				// push default status
				$temp_arr					= array();
				$temp_arr['order_item_id']	= $temp_id;
				$temp_arr['ots_status']		= 'Order Placed';
				$temp_arr['notes']			= '';
				$this->status_update($temp_arr);

			}
		}
		
		return $order_number;

	}

	function status_update($data)
	{
		$this->db->insert('order_items_status', $data);
		$id = $this->db->insert_id();
		return $id;
	}
	
	function get_best_sellers($start, $end)
	{
		if(!empty($start))
		{
			$this->db->where('ordered_on >=', $start);
		}
		if(!empty($end))
		{
			$this->db->where('ordered_on <',  $end);
		}
		
		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				if(isset($items[$i['product_id']]))
				{
					$items[$i['product_id']]	+= $i['quantity'];
				}
				else
				{
					$items[$i['product_id']]	= $i['quantity'];
				}
				
			}
		}
		arsort($items);
		
		// don't need this anymore
		unset($orders);
		
		$return	= array();
		foreach($items as $key=>$quantity)
		{
			$product				= $this->db->where('id', $key)->get('products')->row();
			if($product)
			{
				$product->quantity_sold	= $quantity;
			}
			else
			{
				$product = (object) array('sku'=>'Deleted', 'name'=>'Deleted', 'quantity_sold'=>$quantity);
			}
			
			$return[] = $product;
		}
		
		return $return;
	}

	function get_order_item_statuses($order_item_id,$latest=false)
	{
		$this->db->order_by("created_at", "desc");
		$this->db->where('order_item_id', $order_item_id);

		if($latest){
			$this->db->limit(1);
		}

		if($latest){
			return $this->db->get('order_items_status')->row();
		} else {
			return $this->db->get('order_items_status')->result();
		}
	}

	function get_order_latest_status($order_id,$merchant_id){
		$this->db->select('order_items_status.ots_status as status');
		$this->db->join('order_items','order_items_status.order_item_id = order_items.id');
		$this->db->join('orders','order_items.order_id = orders.id');
		$this->db->join('products_merchants','order_items.pm_id = products_merchants.id');
		$this->db->where('orders.id', $order_id);
		$this->db->where('products_merchants.merchant_id', $merchant_id);
		$this->db->order_by("order_items_status.created_at", "desc");
		$this->db->limit(1);
		$row = $this->db->get('order_items_status')->row();
		if($row){
			return $row->status;
		}
		return 'Pending';
	}

	function update_shipping_info($data){
		$this->db->where('id', $data['id']);
		$this->db->where('order_id', $data['order_id']);
		$this->db->update('order_items', $data);
		return true;
	}

	function get_waybill_number($id,$merchant_id=false)
	{
		$this->db->select('order_items.waybill');
		$this->db->where('order_id', $id);
		
		if($merchant_id){
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}

		$this->db->limit(1);
		$result	= $this->db->get('order_items');
		$item	= $result->row();
		
		if($item)
		{
			return $item->waybill;
		}
		return false;
	}

	function get_waybill_number_with_method($id=false,$merchant_id=false, $order_item_id=false)
	{
		$this->db->select('order_items.waybill, order_items.waybill_method');

		if($id){
			$this->db->where('order_id', $id);
		}
		if($order_item_id){
			$this->db->where('id', $order_item_id);
		}
		if($merchant_id){
			$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);
		}

		$this->db->limit(1);
		$result	= $this->db->get('order_items');
		$item	= $result->row();
		
		if($item)
		{
			return array('waybill'=>$item->waybill, 'waybill_method'=>$item->waybill_method);
		}
		return false;
	}

	function get_waybill_number_by_item($item_id)
	{
		$this->db->select('waybill');
		$this->db->where('id', $item_id);

		$this->db->limit(1);
		$result	= $this->db->get('order_items');
		$item	= $result->row();
		
		if($item)
		{
			return $item->waybill;
		}
		return false;
	}

	function save_ab_cart($cart_contents){
		$customer = $this->go_cart->customer();
		$data 					= array();
		$data['cart_contents'] 	= json_encode($cart_contents);
		$data['updated_at']		= date('Y-m-d H:i:s',time());
		if(!empty($customer['id'])){
			//if($this->db->count_all_results('cart_ab') > 0){
			if($this->db->get_where('cart_ab',array('customer_id'=>$customer['id']))->num_rows() > 0){
				$this->db->where('customer_id',$customer['id']);
				$this->db->update('cart_ab', $data);
			} else {
				$data['customer_id'] 	= $customer['id'];
				$this->db->insert('cart_ab', $data);
				return $this->db->insert_id();
			}
			return true;
		}else{
			if($this->db->get_where('cart_ab',array('customer_id'=>'guest_'.session_id()))->num_rows() > 0){
				$this->db->where('customer_id','guest_'.session_id());
				$this->db->update('cart_ab', $data);
			} else {
				$data['customer_id'] 	= 'guest_'.session_id();
				$this->db->insert('cart_ab', $data);
				return $this->db->insert_id();
			}
			return true;
		}
		//return false;
	}

	function get_lifetime_sales($merchant_id=false){
		$this->db->select('SUM(order_items.subtotal) as val');
		if($merchant_id){
			$this->db->join('products_merchants', 'products_merchants.id = order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);	
		}
		$result = $this->db->get('order_items');
		return $result->row()->val;
	}

	function get_total_orders($merchant_id=false){
		$this->db->select('COUNT(DISTINCT order_items.order_id) as val');
		if($merchant_id){
			$this->db->join('products_merchants', 'products_merchants.id = order_items.pm_id');
			$this->db->where('products_merchants.merchant_id',$merchant_id);	
		}
		$result = $this->db->get('order_items');
		return $result->row()->val;
	}

	function get_pending_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false, $count=false) {
		if($count){
			$query = $this->db->query("SELECT COUNT(org.id) AS val FROM (SELECT `orders`.id FROM (`orders`) JOIN `order_items` ON `order_items`.`order_id`=`orders`.`id` JOIN `products_merchants` ON `products_merchants`.`id`=`order_items`.`pm_id` JOIN ( SELECT order_item_id,COUNT(ots_id) FROM `order_items_status` GROUP BY order_item_id HAVING COUNT(ots_id)=1 ) os ON order_items.id = os.order_item_id WHERE `products_merchants`.`merchant_id` = '3' GROUP BY `orders`.`id`) org");
			return $query->row()->val;
		} else {
			$this->db->select('orders.*');
			
			if ($search)
			{
				if(!empty($search->term))
				{
					//support multiple words
					$term = explode(' ', $search->term);

					foreach($term as $t)
					{
						$not		= '';
						$operator	= 'OR';
						if(substr($t,0,1) == '-')
						{
							$not		= 'NOT ';
							$operator	= 'AND';
							//trim the - sign off
							$t		= substr($t,1,strlen($t));
						}

						$like	= '';
						$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
						$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
						$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

						$this->db->where($like);
					}	
				}
				if(!empty($search->start_date))
				{
					$this->db->where('ordered_on >=',$search->start_date);
				}
				if(!empty($search->end_date))
				{
					//increase by 1 day to make this include the final day
					//I tried <= but it did not function. Any ideas why?
					$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
					$this->db->where('ordered_on <',$search->end_date);
				}
				
			}

			if($merchant_id){
				$this->db->join('order_items','order_items.order_id=orders.id');
				$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
				$this->db->where('products_merchants.merchant_id',$merchant_id);

				$this->db->join('(SELECT order_item_id,COUNT(ots_id) FROM `order_items_status` GROUP BY order_item_id HAVING COUNT(ots_id)=1) AS os','order_items.id = os.order_item_id', 'INNER JOIN', NULL);

				$this->db->group_by(array('orders.id'));
			}
			
			if($limit>0)
			{
				$this->db->limit($limit, $offset);
			}
			if(!empty($sort_by))
			{
				$this->db->order_by($sort_by, $sort_order);
			}
			
			$result = $this->db->get('orders')->result();

			if($merchant_id){
				if($result){
					foreach ($result as &$o){
						$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
					}
				}
			}
			return $result;

		}
	}

	function get_cancelled_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false, $count=false) {
		if($count){
			$query = $this->db->query("SELECT COUNT(org.id) AS val FROM (SELECT `orders`.id FROM (`orders`) JOIN `order_items` ON `order_items`.`order_id`=`orders`.`id` JOIN `products_merchants` ON `products_merchants`.`id`=`order_items`.`pm_id` JOIN ( SELECT order_item_id,ots_status FROM `order_items_status` WHERE ots_status='Cancelled' GROUP BY order_item_id ORDER BY order_items_status.created_at DESC LIMIT 1 ) AS os ON order_items.id = os.order_item_id WHERE `products_merchants`.`merchant_id` = '3' GROUP BY `orders`.`id`) org");
			return $query->row()->val;
		} else {
			$this->db->select('orders.*');
			
			if ($search)
			{
				if(!empty($search->term))
				{
					//support multiple words
					$term = explode(' ', $search->term);

					foreach($term as $t)
					{
						$not		= '';
						$operator	= 'OR';
						if(substr($t,0,1) == '-')
						{
							$not		= 'NOT ';
							$operator	= 'AND';
							//trim the - sign off
							$t		= substr($t,1,strlen($t));
						}

						$like	= '';
						$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
						$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
						$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

						$this->db->where($like);
					}	
				}
				if(!empty($search->start_date))
				{
					$this->db->where('ordered_on >=',$search->start_date);
				}
				if(!empty($search->end_date))
				{
					//increase by 1 day to make this include the final day
					//I tried <= but it did not function. Any ideas why?
					$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
					$this->db->where('ordered_on <',$search->end_date);
				}
				
			}

			if($merchant_id){
				$this->db->join('order_items','order_items.order_id=orders.id');
				$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
				$this->db->where('products_merchants.merchant_id',$merchant_id);

				$this->db->join('(SELECT order_item_id,ots_status FROM `order_items_status` WHERE ots_status="Cancelled" GROUP BY order_item_id ORDER BY order_items_status.created_at DESC LIMIT 1 ) AS os','order_items.id = os.order_item_id', 'INNER JOIN', NULL);

				$this->db->group_by(array('orders.id'));
			}
			
			if($limit>0)
			{
				$this->db->limit($limit, $offset);
			}
			if(!empty($sort_by))
			{
				$this->db->order_by($sort_by, $sort_order);
			}
			
			$result = $this->db->get('orders')->result();

			if($merchant_id){
				if($result){
					foreach ($result as &$o){
						$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
					}
				}
			}
			return $result;

		}
	}

	function get_returns_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $merchant_id=false, $count=false) {
		if($count){
			$query = $this->db->query("SELECT COUNT(org.id) AS val FROM (SELECT `orders`.id FROM (`orders`) JOIN `order_items` ON `order_items`.`order_id`=`orders`.`id` JOIN `products_merchants` ON `products_merchants`.`id`=`order_items`.`pm_id` JOIN ( SELECT order_item_id,ots_status FROM `order_items_status` WHERE ots_status LIKE '%Return%' GROUP BY order_item_id ORDER BY order_items_status.created_at DESC LIMIT 1 ) AS os ON order_items.id = os.order_item_id WHERE `products_merchants`.`merchant_id` = '3' GROUP BY `orders`.`id`) org");
			return $query->row()->val;
		} else {
			$this->db->select('orders.*');
			
			if ($search)
			{
				if(!empty($search->term))
				{
					//support multiple words
					$term = explode(' ', $search->term);

					foreach($term as $t)
					{
						$not		= '';
						$operator	= 'OR';
						if(substr($t,0,1) == '-')
						{
							$not		= 'NOT ';
							$operator	= 'AND';
							//trim the - sign off
							$t		= substr($t,1,strlen($t));
						}

						$like	= '';
						$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
						$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
						$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
						$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

						$this->db->where($like);
					}	
				}
				if(!empty($search->start_date))
				{
					$this->db->where('ordered_on >=',$search->start_date);
				}
				if(!empty($search->end_date))
				{
					//increase by 1 day to make this include the final day
					//I tried <= but it did not function. Any ideas why?
					$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
					$this->db->where('ordered_on <',$search->end_date);
				}
				
			}

			if($merchant_id){
				$this->db->join('order_items','order_items.order_id=orders.id');
				$this->db->join('products_merchants','products_merchants.id=order_items.pm_id');
				$this->db->where('products_merchants.merchant_id',$merchant_id);

				$this->db->join('(SELECT order_item_id,ots_status FROM `order_items_status` WHERE ots_status LIKE "%Return%" GROUP BY order_item_id ORDER BY order_items_status.created_at DESC LIMIT 1 ) AS os','order_items.id = os.order_item_id', 'INNER JOIN', NULL);

				$this->db->group_by(array('orders.id'));
			}
			
			if($limit>0)
			{
				$this->db->limit($limit, $offset);
			}
			if(!empty($sort_by))
			{
				$this->db->order_by($sort_by, $sort_order);
			}
			
			$result = $this->db->get('orders')->result();

			if($merchant_id){
				if($result){
					foreach ($result as &$o){
						$o->latest_status	= $this->get_order_latest_status($o->id,$merchant_id);
					}
				}
			}
			return $result;

		}
	}

	public function getPendingOrderItemsByWaybillMethod($method){
		$this->db->select('order_items.id, order_items.waybill, order_items_status.ots_status');
		$this->db->join('order_items_status', 'order_items_status.order_item_id=order_items.id');
		$this->db->order_by("order_items_status.created_at", "desc");
		$this->db->where('waybill_method', $method);
		$this->db->where('waybill != ', '');

		return $this->db->get('order_items')->result();
	}

	/*public function getPastOrdersForReview(){
		/*$this->db->select('order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email, AVG(rating_item) as rating_item_avg, AVG(rating_communication) as rating_communication_avg, AVG(rating_time) as rating_time_avg, AVG(rating_charges) as rating_charges_avg, AVG(rating_price) as rating_price_avg,AVG(rating_value) as rating_value_avg,AVG(rating_quality) as rating_quality_avg');
		//$this->db->select('order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email, COALESCE(AVG(rating_item),0) as rating_item_avg, COALESCE(AVG(rating_communication),0) as rating_communication_avg, COALESCE(AVG(rating_time),0) as rating_time_avg, COALESCE(AVG(rating_charges),0) as rating_charges_avg');
		$this->db->join('order_items_status', 'order_items_status.order_item_id=order_items.id');
		$this->db->join('orders', 'orders.id=order_items.order_id');
		
		$this->db->join('products_merchants', 'products_merchants.id=order_items.pm_id');
		$this->db->join('seller_ratings', 'seller_ratings.merchant_id=products_merchants.merchant_id');

		$this->db->join('products_ratings', 'products_ratings.product_id=products_merchants.product_id');

		$this->db->order_by("order_items_status.created_at", "desc");
		$this->db->where("order_items_status.created_at > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND order_items_status.created_at <= NOW()");
		
		return $this->db->get('order_items')->result();*


		//$sql  = "SELECT order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email, COALESCE(AVG(rating_item),0) as rating_item_avg, COALESCE(AVG(rating_communication),0) as rating_communication_avg, COALESCE(AVG(rating_time),0) as rating_time_avg, COALESCE(AVG(rating_charges),0) as rating_charges_avg, COALESCE(AVG(rating_price),0) as rating_price_avg,COALESCE(AVG(rating_value),0) as rating_value_avg,COALESCE(AVG(rating_quality),0) as rating_quality_avg";
		//$sql  = "SELECT order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email, COALESCE(AVG(rating_item),0) as rating_item_avg, COALESCE(AVG(rating_communication),0) as rating_communication_avg, COALESCE(AVG(rating_time),0) as rating_time_avg, COALESCE(AVG(rating_charges),0) as rating_charges_avg";
		$sql  = "SELECT order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email";
		
		$sql .= " FROM order_items";
		$sql .= " JOIN order_items_status ON order_items_status.order_item_id=order_items.id";
		$sql .= " JOIN orders ON orders.id=order_items.order_id";
		$sql .= " JOIN products_merchants ON products_merchants.id=order_items.pm_id";
		$sql .= " JOIN seller_ratings ON seller_ratings.merchant_id=products_merchants.merchant_id";
		//$sql .= " JOIN products_ratings ON products_ratings.product_id=products_merchants.product_id";

		$sql .= " WHERE order_items_status.created_at > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND order_items_status.created_at <= NOW()";

		$sql .= " ORDER BY order_items_status.created_at desc";
		
		return $this->db->query($sql)->result();
	}*/

	public function getPastOrdersForReview(){
		$this->db->select('order_items.id, order_items_status.ots_status, order_items_status.created_at AS delivered_at, order_items.contents, orders.order_number,orders.ship_firstname,orders.ship_lastname,orders.ship_email, products_merchants.merchant_id');
		$this->db->join('order_items_status', 'order_items_status.order_item_id=order_items.id');
		$this->db->join('orders', 'orders.id=order_items.order_id');
		
		$this->db->join('products_merchants', 'products_merchants.id=order_items.pm_id');
		
		$this->db->order_by("order_items_status.created_at", "desc");
		$this->db->where("order_items_status.created_at > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND order_items_status.created_at <= NOW()");
		
		return $this->db->get('order_items')->result();
	}

}