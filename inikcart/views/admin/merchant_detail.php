<div class="row">
	<div class="span12">
		<strong>First Name: </strong><?php echo $row->firstname;?><br>
		<strong>Last Name: </strong><?php echo $row->lastname;?><br>
		<strong>Company/Store Name: </strong><?php echo $row->store_name;?><br>
		<strong>Phone: </strong><?php echo $row->phone;?><br>
		<strong>Landline: </strong><?php echo $row->landline;?><br>
		<strong>Email: </strong><?php echo $row->email;?><br>
		<strong>Service: </strong><?php echo $row->service;?><br>
		<strong>Primary Category: </strong><?php echo $row->primary_category;?><br>
		<strong>Otehr Info: </strong><?php echo $row->description;?><br>
		<strong>GST Reg No.: </strong><?php echo $row->cst;?><br>
		<strong>Business: </strong><?php echo $row->business;?><br>
		<strong>Revenue: </strong><?php echo $row->revenue;?><br>
		<strong>Website: </strong><?php echo $row->website;?><br>
		<strong>Monthly Website Visitors: </strong><?php echo $row->visitors;?><br>
		<strong>Primary Clients: </strong><?php echo $row->clients;?><br>
		<strong>Collaborated with anyone else: </strong><?php echo $row->other_ecommerce;?><br>
		<strong>Web Link of any other ecommerce: </strong><?php echo $row->web_link;?><br>
		<strong>Number of Stores: </strong><?php echo $row->stores;?><br>
		<strong>Number of Products in Stock: </strong><?php echo $row->stock;?><br>
		<strong>Query Sent At: </strong><?php echo $row->created_at;?><br>
		<br>
		<strong>Password: </strong>
		<?php echo preg_replace('/\s+/', '', strtolower($row->store_name)).substr($row->phone,0,5).'#';?> (for first time use only)
		<br><br>
	</div>
	<div class="span12">
		<strong>Status: </strong>
		<?php echo ($row->enabled == '9') ? 'Pending' : '';?>
		<?php if($row->enabled == '9'){ ?>
			<a class="btn btn-info" href="<?php echo site_url('admin/merchants/merchant_status/'.$row->id.'/enable');?>">Approve</a>
			<a class="btn btn-danger" href="<?php echo site_url('admin/merchants/merchant_status/'.$row->id.'/disable');?>">Disable</a>
		<?php } ?>
	</div>
</div>