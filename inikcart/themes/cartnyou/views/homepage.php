<?php echo theme_js('jquery.matchHeight-min.js', true);?>

<div class="pdt-129-header">
    <?php $this->banners->show_collection(1, 10, 'home_slider');?>
</div>

<?php $this->banners->show_collection(15, 4, 'adaptive_box_row');?>

<div class="content-page">
	<div class="<?php echo $this->isMobile ? '' : 'container';?> ">

		<?php
		$fcColorIndex = 0;
		/*$fcColorArray = array('red','green','orange','blue','blue2','gray');*/
        $fcColorArray = array('red','red','red','red','red','red');
		if(!empty($this->featured_categories)){
			for ($fc=0 ; $fc<count($this->featured_categories) ; $fc++) {
				$featured_category = $this->featured_categories[$fc];
				if($fcColorIndex > 5){
					$fcColorIndex = 0;
				}
				?>
				<div class="pos-r category-featured <?php echo (($fc%2!=0)?'category-featured-right':"") ?> ">
					<nav class="navbar nav-menu nav-menu-<?php echo $fcColorArray[$fcColorIndex];?> show-brand">
						<div class="container">
							<div class="navbar-brand">
								<a href="<?php echo site_url($featured_category['slug']);?>">
									<?php if($featured_category['icon'] != ''){ ?>
										<!--<img class="category-sm-icon" src="<?php /*echo base_url('uploads/images/thumbnails').'/'.$featured_category['icon'];*/?>">-->
									<?php } ?>
                                    <i class="fa fa-bars mrr-20" aria-hidden="true"></i>
                                    <img src="<?php echo theme_assets('img/shopping-cart.png') ?>" data-src="<?php echo theme_assets('img/shopping-cart.png') ?>" class="visible-xs nbr-img lazy" alt="">
                                    <?php echo $featured_category['name'];?>
								</a>
							</div>
							<span class="toggle-menu"></span>
							<div class="collapse navbar-collapse">
								<ul class="nav navbar-nav pull-right">
									<li class="active"><a data-toggle="tab" href="#tab-featured-category-best-seller-<?php echo $featured_category['id'];?>" class="featured-category-tabs preload-products" data-mode="best-seller" data-id="<?php echo $featured_category['id'];?>">Best Seller</a></li>
									<li><a data-toggle="tab" href="#tab-featured-category-on-sale-<?php echo $featured_category['id'];?>" class="featured-category-tabs" data-mode="on-sale" data-id="<?php echo $featured_category['id'];?>">On Sale</a></li>
									<li><a data-toggle="tab" href="#tab-featured-category-most-viewed-<?php echo $featured_category['id'];?>" class="featured-category-tabs" data-mode="most-viewed" data-id="<?php echo $featured_category['id'];?>">Most Viewed</a></li>
									<li><a data-toggle="tab" href="#tab-featured-category-new-arrivals-<?php echo $featured_category['id'];?>" class="featured-category-tabs" data-mode="new-arrivals" data-id="<?php echo $featured_category['id'];?>">New Arrivals<!-- <span class="notify notify-right">2</span> --></a></li>
									<?php
									if(!empty($featured_category['child'])){
										foreach ($featured_category['child'] as $child) { ?>
											<li><a data-toggle="tab" href="#tab-featured-category-category-<?php echo $child['id'];?>" class="featured-category-tabs" data-mode="category" data-id="<?php echo $child['id'];?>"><?php echo $child['name'];?></a></li>
										<?php
										}
									}
									?>
								</ul>
							</div>
						</div>
						<div id="elevator-<?php echo $featured_category['id'];?>" class="floor-elevator">
							<?php
							if($fc==0){
								echo '<a href="#" class="btn-elevator up disabled fa fa-angle-up"></a>';
							} else {
								echo '<a href="#elevator-'.$this->featured_categories[$fc-1]['id'].'" class="btn-elevator up fa fa-angle-up"></a>';
							}
							if($fc==(count($this->featured_categories) - 1)){
								echo '<a href="#" class="btn-elevator down disabled fa fa-angle-down"></a>';
							} else {
								echo '<a href="#elevator-'.$this->featured_categories[$fc+1]['id'].'" class="btn-elevator down fa fa-angle-down"></a>';
							}
							?>
						</div>
					</nav>
					<!-- <div class="category-banner">
						<div class="col-sm-6 banner">
							<a href="#"><img alt="ads2" class="img-responsive" src="<?php echo theme_img('/ads2.jpg');?>"/></a>
						</div>
						<div class="col-sm-6 banner">
							<a href="#"><img alt="ads2" class="img-responsive" src="<?php echo theme_img('/ads3.jpg');?>"/></a>
						</div>
					</div> -->
					<div class="product-featured clearfix">
						<div class="banner-featured">
							<div class="featured-text"><span>featured</span></div>
							<div class="banner-img">
								<a href="<?php echo site_url($featured_category['slug']);?>"><img class="match-item lazy" data-src="<?php echo ($featured_category['image']) ? base_url('uploads/images/small/'.$featured_category['image']) : theme_img('/f1.jpg')   ;?>" alt="<?php echo $featured_category['name'];?>" src="<?php echo ($featured_category['image']) ? base_url('uploads/images/small/'.$featured_category['image']) : theme_img('/f1.jpg')   ;?>"/></a>
							</div>
						</div>
						<div class="product-featured-content">
							<div class="product-featured-list">
								<div class="tab-container">

									<!-- Tabs start-->
									<div class="tab-panel active" id="tab-featured-category-best-seller-<?php echo $featured_category['id'];?>">
										<ul class="product-list">
										</ul>
									</div>
									<div class="tab-panel" id="tab-featured-category-on-sale-<?php echo $featured_category['id'];?>">
										<ul class="product-list">
										</ul>
									</div>
									<div class="tab-panel" id="tab-featured-category-most-viewed-<?php echo $featured_category['id'];?>">
										<ul class="product-list">
										</ul>
									</div>
									<div class="tab-panel" id="tab-featured-category-new-arrivals-<?php echo $featured_category['id'];?>">
										<ul class="product-list">
										</ul>
									</div>
									<?php
									if(!empty($featured_category['child'])){
										foreach ($featured_category['child'] as $child) { ?>
											<div class="tab-panel" id="tab-featured-category-category-<?php echo $child['id'];?>">
<!--                                                get_feautred_products function-->
												<ul class="product-list">
												</ul>
											</div>
										<?php
										}
									}
									?>
									<!-- Tabs end-->

								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				$fcColorIndex++;
			}
		}
		?>


		<!-- Baner bottom -->
		
		<!-- end banner bottom -->
	</div>
</div>

<?php $this->banners->show_collection(16, 4, 'adaptive_box_row');?>

<?php echo theme_css('raterater.css', true);?>
<?php echo theme_js('raterater.jquery.js', true);?>

<script>
$(function(){

	setTimeout(function(){
		$('.brand-match-height').matchHeight({
		    byRow: true,
		    property: 'height',
		    target: null,
		    remove: false
		});
	}, 500);

	$('.bon-tabs').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href");
		var mode = $(e.target).attr("data-mode");

		if(mode == 'popular-products'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_products('popular-products', $(target).children('.product-list'));
			}
		} else if(mode == 'on-sale'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_products('on-sale', $(target).children('.product-list'));
			}
		} else if(mode == 'most-viewed'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_products('most-viewed', $(target).children('.product-list'));
			}
		} else if(mode == 'new-products'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_products('new-products', $(target).children('.product-list'));
			}
		}

	});
//    featured-list
    function get_featured_products($slider){

        var url = '<?php echo site_url("cart/popular_products/html");?>';

        $slider.html('<div class="loader-partial"></div>');
        $.ajax({
            url : url,
            method: 'GET'
        }).done(function(getdata){

            $slider.html(getdata);
            $slider.owlCarousel({
                items: 1,
                autoPlay: 1000,
                dots: false,
                loop: true,
                nav: true,
                margin: 5,
                autoplayTimeout: 1000,
                autoplayHoverPause: true,
                responsive: {
                    0: {items:2},
                    600: {items:1},
                    1000: {items:1}
                },
                navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                smartSpeed: "300"
            });
            $('.ratebox').raterater( {
                submitFunction: 'rateAlert',
                allowChange: false,
                starWidth: 16,
                spaceWidth: 1,
                numStars: 5,
                isStatic: true,
            });
            setTimeout(function(){
                $('.match-item').matchHeight({
                    byRow: true,
                    property: 'height',
                    target: null,
                    remove: false
                });
            }, 500);

        });
        setTimeout(function(){
            $("img.lazy").Lazy();
        },1000);
    }
    get_featured_products($('.featured-list'));

	function get_products($mode, $slider){
		var url = '';
		if($mode == 'popular-products'){
			url = '<?php echo site_url("cart/popular_products/html");?>';
		} else if($mode == 'on-sale'){
			url = '<?php echo site_url("cart/sale_products");?>';
		} else if($mode == 'most-viewed'){
			url = '<?php echo site_url("cart/most_viewed");?>';
		} else if($mode == 'new-products'){
			url = '<?php echo site_url("cart/new_products");?>';
		}

		$slider.html('<div class="loader-partial"></div>');
		$.ajax({
            url : url,
            method: 'GET'
        }).done(function(getdata){

        	$slider.html(getdata);
			$slider.owlCarousel({
				items: 2,
				autoPlay: 1000,
				dots: false,
				loop: true,
				nav: true,
				margin: 5,
				autoplayTimeout: 1000,
				autoplayHoverPause: true,
				responsive: {
					0: {items:1},
					600: {items:3},
					1000: {items:4}
				},
          		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
         		smartSpeed: "300"
			});
			$('.ratebox').raterater( { 
		        submitFunction: 'rateAlert', 
		        allowChange: false,
		        starWidth: 16,
		        spaceWidth: 1,
		        numStars: 5,
		        isStatic: true,
		    });
			setTimeout(function(){
				$('.match-item').matchHeight({
				    byRow: true,
				    property: 'height',
				    target: null,
				    remove: false
				});
			}, 500);

        });
        setTimeout(function(){
            $("img.lazy").Lazy();
        },1000);
	}

	//on page load
	get_products('popular-products',$('#tab-popular-products').children('.product-list'));


	$('.featured-category-tabs').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href");
		var mode = $(e.target).attr("data-mode");
		var id = $(e.target).attr("data-id");

		if(mode == 'best-seller'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_featured_category_products('popular-products', $(target).children('.product-list'), id);
			}
		} else if(mode == 'on-sale'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_featured_category_products('on-sale', $(target).children('.product-list'), id);
			}
		} else if(mode == 'most-viewed'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_featured_category_products('most-viewed', $(target).children('.product-list'), id);
			}
		} else if(mode == 'new-arrivals'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_featured_category_products('new-arrivals', $(target).children('.product-list'), id);
			}
		}  else if(mode == 'category'){
			if($(target).children('.product-list').children('.owl-stage-outer').children('.owl-stage').children('.owl-item').length == 0){
				get_featured_category_products('category', $(target).children('.product-list'), id);
			}
		}
	});

});

function get_featured_category_products($mode, $slider, $category_id){
	var url = '';
	if($mode == 'best-seller'){
		url = 'cart/popular_products/html/'+$category_id;
	} else if($mode == 'on-sale'){
		url = 'cart/sale_products/'+$category_id;
	} else if($mode == 'most-viewed'){
		url = 'cart/most_viewed/'+$category_id;
	} else if($mode == 'new-arrivals'){
		url = 'cart/new_products/'+$category_id;
	} else if($mode == 'category'){
		url = 'cart/sale_products/'+$category_id;
	} else {
		return;
	}
	url = '<?php echo site_url();?>'+url;

	$slider.html('<div class="loader-partial"></div>');
	$.ajax({
        url : url,
        method: 'GET'
    }).done(function(getdata){

    	$slider.html(getdata);
		$slider.owlCarousel({
			autoPlay: 1000,
			dots: false,
			loop: true,
			nav: true,
			margin: 5,
			autoplayTimeout: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {items:2},
				600: {items:3},
				1000: {items:4}
			},
      		navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
     		smartSpeed: "300"
		});
		$('.ratebox').raterater( { 
	        submitFunction: 'rateAlert', 
	        allowChange: false,
	        starWidth: 16,
	        spaceWidth: 1,
	        numStars: 5,
	        isStatic: true,
	    });
		setTimeout(function(){
			$('.match-item').matchHeight({
			    byRow: true,
			    property: 'height',
			    target: null,
			    remove: false
			});
		}, 500);
    });
    setTimeout(function(){
        $("img.lazy").Lazy();
    },1000);
}
</script>

<?php
if(!empty($this->featured_categories)){
	foreach ($this->featured_categories as $featured_category) { ?>
		<script>
		//on page load
		get_featured_category_products('best-seller',$('#tab-featured-category-best-seller-<?php echo $featured_category['id'];?>').children('.product-list'), '<?php echo $featured_category['id']	;?>');
		</script>
	<?php }
}
?>