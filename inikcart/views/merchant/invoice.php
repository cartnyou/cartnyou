<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
    tr.noBorder-top td {
      border-top: 0;
    }
    tr.noBorder-bottom td {
      border-bottom: 0;
    }
</style>
</head>

<body>

<div style="font-size:12px; font-family:arial, verdana, sans-serif;">
    
    <h2><img alt="Cartnyou" src="<?php echo theme_img('logo-cartnyou.png');?>" style="max-width: 100px;" /></h2>
    
    <table style="border:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:50%; vertical-align:top;">
                <strong>Sold By:</strong><br/>
                <?php echo $order->store_name;?><br>
                <?php echo $order->merchant_address;?><br>
                <?php echo $order->merchant_city;?><br>
                <?php echo $order->merchant_state;?><br>
                <?php echo $order->merchant_zip;?>
            </td>
            <td style="width:50%; vertical-align:top; text-align: right;" class="packing">
                <strong>Billing Address:</strong><br/>     
                <?php echo (!empty($order->ship_company))?$order->ship_company.'<br/>':'';?>
                <?php echo $order->ship_firstname.' '.$order->ship_lastname;?> <br/>
                <?php echo $order->ship_address1;?><br>
                <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
                <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
                <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
                <?php echo $order->ship_country;?><br/>

                <?php echo $order->ship_email;?><br/>
                <?php echo $order->ship_phone;?>

            <br/>
            </td>
        </tr>
        <tr>
            <td style="width:40%; vertical-align:top;">
                <strong>GST Registration No.:</strong><br>
                <?php echo $order->merchant_cst;?><br>
                <strong>Pan No.:</strong><br>
                <?php echo $order->merchant_pan;?>
            </td>
            <td style="width:40%; vertical-align:top; text-align: right;" class="packing">
                <strong>Shipping Address:</strong><br/>     
                <?php echo (!empty($order->ship_company))?$order->ship_company.'<br/>':'';?>
                <?php echo $order->ship_firstname.' '.$order->ship_lastname;?> <br/>
                <?php echo $order->ship_address1;?><br>
                <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
                <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
                <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
                <?php echo $order->ship_country;?>

            <br/>
            </td>
        </tr>
        
        <tr>
            <td style="border-top:1px solid #000;">
                <strong>Order Number: </strong> <?php echo $order->order_number; ?><br>
                <strong>Order Date: </strong> <?php echo $order->ordered_on; ?>
            </td>
            <td style="border-top:1px solid #000;text-align: right;">
                <strong>Invoice Number: </strong> <br>
                <?php echo $order->contents[0]['merchant_invoice_number']; ?>
                <!-- <strong>Invoice Date: </strong> <?php echo $order->payment_info; ?> -->
            </td>
        </tr>
        
        <?php if(!empty($order->gift_message)):?>
        <tr>
            <td colspan="3" style="border-top:1px solid #000;">
                <strong><?php echo lang('gift_note');?></strong>
                <?php echo $order->gift_message;?>
            </td>
        </tr>
        <?php endif;?>
        
        <?php if(!empty($order->shipping_notes)):?>
            <tr>
                <td colspan="3" style="border-top:1px solid #000;">
                    <strong><?php echo lang('shipping_notes');?></strong><br/><?php echo $order->shipping_notes;?>
                </td>
            </tr>
        <?php endif;?>
    </table>
    
    <table border="1" style="width:100%; margin-top:10px; border-color:#000; font-size:13px; border-collapse:collapse;" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th width="2%" class="packing">
                    S.<br>No.
                </th>
				<th width="14%" class="packing" >Seller</th>
                <th width="14%" class="packing" >
                    <?php echo lang('description');?>
                </th>
                <th width="10%" class="packing" >
                    <?php echo lang('price');?>
                </th>
                <th width="10%" class="packing" >
                    <?php echo lang('quantity');?>
                </th>
                <th width="10%" class="packing">Net Amount</th>
                <th width="10%" class="packing">Tax Rate</th>
                <th width="10%" class="packing">Tax Type</th>
                <th width="10%" class="packing">Tax Amount</th>
                <th width="10%" class="packing">Total Amount</th>
            </tr>
        </thead>
        <tbody>
    <?php $items = $order->contents; ?>

<?php
$product_count = 1;
$subtotal = 0;
?>
<?php foreach($order->contents as $orderkey=>$product):

        $img_count = 1;

        $product_total = $product['price']*$product['quantity'];
        $net_total = ($product_total*100) / (100 + $product['tax_rate']);
        $net_tax = $product_total - $net_total;

        $subtotal += $product_total;
?>
        <tr class="noBorder-bottom">
            <td class="packing">
                <?php echo $product_count;?>
            </td>
			<td class="packing">
                <?php echo $order->store_name;?>
            </td>
            <td class="packing">
                <?php echo ucwords($product['name']);?>
                <?php echo (trim($product['sku']) != '')?'<br/><small>sku: '.$product['sku'].'</small><br>':'';?>

                <?php
                if(isset($product['options']))
                {
                    foreach($product['options'] as $name=>$value)
                    {
                        $name = explode('-', $name);
                        $name = trim($name[0]);
                        if(is_array($value))
                        {
                            echo '<div>'.$name.':<br/>';
                            foreach($value as $item)
                            {
                                echo '- '.$item.'<br/>';
                            }   
                            echo "</div>";
                        }
                        else
                        {
                            echo '<div>'.$name.': '.$value.'</div>';
                        }
                    }
                }

                ?>
            </td>
            <td>
                Rs <?php echo format_currency($net_total/ $product['quantity'],true,1);?>
            </td>
            <td><?php echo $product['quantity'];?></td>
            <td>
                Rs <?php echo format_currency($net_total,true,1);?>
            </td>
            <td><?php echo round(($product['tax_rate']/2),2);?>%<br><?php echo round(($product['tax_rate']/2),2);?>%</td>
            <td>SGST<br>CGST</td>
            <td>Rs <?php echo format_currency($net_tax/2,true,1);?><br>Rs <?php echo format_currency($net_tax/2,true,1);?></td>
            <td>Rs <?php echo format_currency($product['price']*$product['quantity'],true,1);?></td>
        </tr>

        <?php
        if($product['shipping'] != 0) { 
            $net_total_tax = ($product['shipping']*100) / (100 + $product['tax_rate']);
            $net_tax_tax = $product['shipping'] - $net_total_tax;

            $subtotal += $product['shipping'];
        ?>
        <tr class="noBorder-top">
            <td></td>
            <td class="packing">
                
            </td>
			<td class="packing">
                <?php echo (trim($product['shipping']) != 0)?'<br/>Shipping Charges':'';?>
            </td>
            <td>
                Rs <?php echo format_currency($net_total_tax    /$product['quantity'],true,1);?>
            </td>
            <td><?php echo $product['quantity'];?></td>
            <td>
                Rs <?php echo format_currency($net_total_tax,true,1);?>
            </td>
            <td><?php echo round(($product['tax_rate']/2),2);?>%<br><?php echo round(($product['tax_rate']/2),2);?>%</td>
            <td>SGST<br>CGST</td>
            <td>Rs <?php echo format_currency($net_tax_tax/2,true,1);?><br>Rs <?php echo format_currency($net_tax_tax/2,true,1);?></td>
            <td>Rs <?php echo format_currency($product['shipping'],true,1);?></td>
        </tr>
        <?php } ?>

        <?php $product_count++;?>
<?php   endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="9"><strong><?php echo lang('subtotal');?></strong></td>
                <td>Rs <?php echo format_currency($subtotal,true,1); ?></td>
            </tr>
            
            <?php 
            $charges = @$order->custom_charges;
            if(!empty($charges))
            {
                foreach($charges as $name=>$price) : ?>
                    
            <tr>
                <td colspan="9"><strong><?php echo $name?></strong></td>
                <td>Rs <?php echo format_currency($price,true,1); ?></td>
            </tr>   
                    
            <?php endforeach;
            }
            ?>
            <!-- <tr>
                <td colspan="8"><strong><?php echo lang('shipping');?></strong></td>
                <td>Rs <?php echo format_currency($order->shipping,true,1); ?></td>
            </tr> -->

            <?php if($order->coupon_discount > 0):?>
            <tr>
                <td colspan="9"><strong><?php echo lang('coupon_discount');?></strong></td>
                <td>Rs <?php echo format_currency(0-$order->coupon_discount,true,1); ?></td>
            </tr>
            <?php endif;?>
            
            <!-- <tr>
                <td colspan="8"><strong><?php echo lang('tax');?></strong></td>
                <td>Rs <?php echo format_currency($order->tax,true,1); ?></td>
            </tr> -->
            <?php if($order->gift_card_discount > 0):?>
            <tr>
                <td colspan="9"><strong><?php echo lang('giftcard_discount');?></strong></td>
                <td>Rs <?php echo format_currency(0-$order->gift_card_discount,true,1); ?></td>
            </tr>
            <?php endif;?>
            <tr>
                <td colspan="9"><strong>Total</strong></td>
                <td><strong>Rs <?php echo format_currency($subtotal - $order->coupon_discount,true,1); ?></strong></td>
            </tr>

            <tr>
                <td colspan="10">
                    <strong>
                        Amount in Words:<br>
                        <?php echo convert_number_to_words($subtotal - $order->coupon_discount);?>
                    </strong>
                </td>
            </tr>

            <tr>
               <td colspan="10" style="text-align: right;">
                   <strong>For <?php echo $order->store_name;?>:</strong><br><br><br>
                   <?php if($order->merchant_sign != ''){ ?>
                        <img src="<?php echo base_url('uploads/images/small/'.$order->merchant_sign);?>" style="max-width: 100px;"><br>
                    <?php } ?>
                    <strong>Authorized Signatory</strong>
               </td> 
            </tr>
        </tfoot>
    </table>

    <hr style="border-top: dashed 1px;" >

    <center>
    	<h3>cartnyou.com Declaration Letter<br>To Whomsoever It May Concern</h3>
    </center>
    I, <?php echo $order->ship_firstname.' '.$order->ship_lastname;?>, have placed the order for

    <table border="1" style="width:100%; margin-top:10px; border-color:#000; font-size:13px; border-collapse:collapse;" cellpadding="5" cellspacing="0">
        <thead>
            <tr>
                <th width="20%" class="packing" >
                    <?php echo lang('quantity');?>
                </th>
                <th width="80%" class="packing" >
                    Product Details
                </th>
            </tr>
        </thead>
        <tbody>
			<?php foreach($items as $orderkey=>$product): ?>
		        <tr class="noBorder-bottom">
		        	<td><?php echo $product['quantity'];?></td>
		            <td class="packing">
		                <?php echo ucwords($product['name']);?>
		                <?php echo (trim($product['sku']) != '')?'<br/><small>sku: '.$product['sku'].'</small><br>':'';?>

		                <?php
		                if(isset($product['options']))
		                {
		                    foreach($product['options'] as $name=>$value)
		                    {
		                        $name = explode('-', $name);
		                        $name = trim($name[0]);
		                        if(is_array($value))
		                        {
		                            echo '<div>'.$name.':<br/>';
		                            foreach($value as $item)
		                            {
		                                echo '- '.$item.'<br/>';
		                            }   
		                            echo "</div>";
		                        }
		                        else
		                        {
		                            echo '<div>'.$name.': '.$value.'</div>';
		                        }
		                    }
		                }
		                ?>
		            </td>
		        </tr>
			<?php endforeach;?>
        </tbody>
    </table>

    <h3>Order ID: <?php echo $order->order_number; ?></h3>
    <?php echo $order->ship_firstname.' '.$order->ship_lastname;?> <br/>
    <?php echo $order->ship_address1;?><br>
    <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
    <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
    <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
    <?php echo $order->ship_country;?>
    <br><br><br>

    I hereby confirm that said above goods are being purchased for my internal or personal purpose and not for re-sale. I further understand and agree to Cartnyou Terms and Conditions available at www.cartnyou.com/terms-and-conditions or upon request.


</div>
</body>
</html>