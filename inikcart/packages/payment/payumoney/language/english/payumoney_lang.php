<?php

$lang['payumoney']				= 'Payumoney';
$lang['payumoney_error']				= 'There was an error processing your payment through Payumoney';
$lang['payumoney_desc']				= 'You will be directed to Payumoney to complete your payment. Once your payment is authorized, you will be directed back to our website and your order will be complete.';


//payumoney admin
$lang['merchant_key']				            = 'Payumoney Merchant Key';
$lang['merchant_salt']				            = 'Payumoney Salt Key Word';

$lang['currency_label']				    = 'INR, USD, EUR, etc.';