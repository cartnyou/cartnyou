<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Store/Merchant</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Landline</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo ucwords($row->store_name);?></td>
					<td><?php echo ucwords($row->firstname.' '.$row->lastname);?>
					<td><?php echo $row->email;?></td>
					<td><?php echo $row->phone;?></td>
					<td><?php echo $row->landline;?></td>
					<td>
						<div class="btn-group" style="float:right">
							<a class="btn" href="<?php echo site_url('admin/merchants/detail/'.$row->id);?>" target="_blank">Detail</a>

							<?php if($row->enabled == '9'){ ?>
								<a class="btn btn-info" href="<?php echo site_url('admin/merchants/merchant_status/'.$row->id.'/enable');?>">Approve</a>
								<a class="btn btn-danger" href="<?php echo site_url('admin/merchants/merchant_status/'.$row->id.'/disable');?>">Disable</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="5">No data found.</td></tr>';
		}
		?>
	</tbody>
</table>