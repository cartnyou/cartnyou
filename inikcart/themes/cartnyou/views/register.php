<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
        </div>
        <h3 class="page-heading">
            <span class="page-heading-title2">My Account</span>
        </h3>
        <div class="page-content">
            <div class="row">

            	<div class="col-md-12">
            		<?php if ($this->session->flashdata('message')):?>
						<div class="alert alert-info">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('message');?>
						</div>
					<?php endif;?>
					
					<?php if ($this->session->flashdata('error')):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('error');?>
						</div>
					<?php endif;?>
					
					<?php if (!empty($error)):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $error;?>
						</div>
					<?php endif;?>
				</div>

				<div class="col-sm-6">
                    <div class="box-authentication">

						<?php
						$company	= array('id'=>'bill_company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company'));
						$first		= array('id'=>'bill_firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'));
						$last		= array('id'=>'bill_lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'));
						$email		= array('id'=>'bill_email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'));
						$phone		= array('id'=>'bill_phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'));
						?>
			
						<h3>REGISTER</h3>

						<?php echo form_open('secure/register', 'class="login_form"'); ?>

							<input type="hidden" name="submitted" value="submitted" />
							<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

							<div class="row">
								<div class="col-sm-12">
									<label>First Name</label>
									<?php echo form_input($first);?>
								</div>
								<div class="col-sm-6 hidden">
									<label>Last Name</label>
									<?php echo form_input($last);?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<label>Email</label>
									<?php echo form_input($email);?>
								</div>
								<div class="col-sm-6">
									<label>Mobile Number</label>
									<?php echo form_input($phone);?>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<label>Password</label>
									<input class="form-control last" type="password" autocomplete="off" name="password" />
								</div>
								<div class="col-sm-6">
									<label>Confirm Password</label>
									<input class="form-control last" type="password" autocomplete="off" name="confirm" />
								</div>
							</div>

							<input name="email_subscribe" class="hidden" value="1" type="checkbox" id="categorymanufacturer1" <?php echo set_radio('email_subscribe', '1', TRUE); ?> />
									
							<div class="row">
								<div class="col-sm-12">
									<div class="center">
										<button type="submit" class="button"><i class="fa fa-user-plus"></i> <?php echo lang('form_register');?></button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>

				<div class="col-sm-6">
                    <div class="box-authentication">
                    	<h3>ALREADY A USER</h3>
                    	<p>If already registered with us, please login.</p>
                        <a class="btn btn-theme-grey mt16" href="<?php echo site_url('secure/login'); ?>"><i class="fa fa-user"></i> <?php echo lang('go_to_login');?></a>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>