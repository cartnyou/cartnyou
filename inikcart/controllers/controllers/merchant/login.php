<?php

class Login extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->lang->load('login');
	}

	function index()
	{

		//we check if they are logged in, generally this would be done in the constructor, but we want to allow customers to log out still
		//or still be able to either retrieve their password or anything else this controller may be extended to do
		$redirect	= $this->auth->is_merchant_logged_in(false, false);
		//if they are logged in, we send them back to the dashboard by default, if they are not logging in
		if ($redirect)
		{
			redirect($this->config->item('merchant_folder').'/dashboard');
		}
		
		
		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');
		$submitted 			= $this->input->post('submitted');
		if ($submitted)
		{
			$email	= $this->input->post('email');
			$password	= $this->input->post('password');
			$remember   = $this->input->post('remember');
			$redirect	= $this->input->post('redirect');
			$login		= $this->auth->login_merchant($email, $password, $remember);
			if ($login)
			{
				if ($redirect == '')
				{
					$redirect = $this->config->item('merchant_folder').'/dashboard';
				}
				redirect($redirect);
			}
			else
			{
				//this adds the redirect back to flash data if they provide an incorrect credentials
				$this->session->set_flashdata('redirect', $redirect);
				$this->session->set_flashdata('error', lang('error_authentication_failed'));
				redirect($this->config->item('merchant_folder').'/login');
			}
		}
		
		$this->load->model('Banner_model');
		$banners	= $this->Banner_model->banner_collection_banners(14, true, 5);
		$data['banners'] 	= array();
		if(!empty($banners)){
			foreach ($banners as $banner) {
				$data['banners'][] = base_url('uploads/'.$banner->image);
			}
		}

		$this->load->view($this->config->item('merchant_folder').'/login', $data);
	}
	
	function logout()
	{
		$this->auth->logout('merchant');
		
		//when someone logs out, automatically redirect them to the login page.
		$this->session->set_flashdata('message', lang('message_logged_out'));
		redirect($this->config->item('merchant_folder').'/login');
	}

	function forgot_password()
	{
		$redirect	= $this->auth->is_merchant_logged_in(false, false);
		if ($redirect)
		{
			redirect($this->config->item('merchant_folder').'/dashboard');
		}
		
		$this->load->helper('form');
		$this->load->helper('string');
		$email = trim($this->input->post('email'));

		if($email == ''){
			$this->session->set_flashdata('error', 'Please enter a valid email.');
		}
		
		$this->load->model('Merchant_model');
		$reset = $this->Merchant_model->reset_password($email);

		if ($reset)
		{						
			$this->session->set_flashdata('message', 'A new password has been generated and sent to your email.');
		}
		else
		{
			$this->session->set_flashdata('error', 'No account found with entered email.');
		}
		redirect($this->config->item('merchant_folder').'/login');
	}

}
