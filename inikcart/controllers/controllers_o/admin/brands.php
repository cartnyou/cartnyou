<?php

class Brands extends Admin_Controller { 
    
    function __construct(){       
        parent::__construct();
        
        $this->auth->check_access('Admin', true);;
        $this->load->model('Brand_model');
    }

    function index(){
        $data['page_title'] = 'Brands';
        $data['brands'] = $this->Brand_model->get_brands();
        
        $this->view($this->config->item('admin_folder').'/brands', $data);
    }

    function form($id = false)
    {
        $data['active_nav'] = 'products';
        $data['active_sub_menu'] = 'product_brand'; 
        $data['page_title']     = "Brand Form";

        $config['upload_path']      = 'uploads/images/full';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = $this->config->item('size_limit');
        $config['max_width']        = '1024';
        $config['max_height']       = '768';
        $config['encrypt_name']     = true;
        $this->load->library('upload', $config);
        
        
        $this->brand_id  = $id;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        
        //default values are empty if the customer is new
        $data['id']             = '';
        $data['name']           = '';
        $data['slug']           = '';
        $data['description']    = '';
        $data['image']          = '';
        $data['seo_title']      = '';
        $data['meta']           = '';
        $data['enabled']        = '';
        $data['error']          = '';
        
        //create the photos array for later use
        $data['photos']     = array();
        
        if ($id)
        {   
            $brand       = $this->Brand_model->get_brand($id);

            if (!$brand)
            {
                $this->session->set_flashdata('error', lang('error_not_found'));
                redirect($this->config->item('admin_folder').'/brands');
            }
            
            //helps us with the slug generation
            $this->name    = $this->input->post('slug', $brand->slug);
            
            //set values to db values
            $data['id']             = $brand->id;
            $data['name']           = $brand->name;
            $data['slug']           = $brand->slug;
            $data['description']    = $brand->description;
            $data['image']          = $brand->image;
            $data['seo_title']      = $brand->seo_title;
            $data['meta']           = $brand->meta;
            $data['enabled']        = $brand->enabled;
            
        }
        
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[64]');
        $this->form_validation->set_rules('slug', 'lang:slug', 'trim');
        $this->form_validation->set_rules('description', 'lang:description', 'trim');
        $this->form_validation->set_rules('image', 'lang:image', 'trim');
        $this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
        $this->form_validation->set_rules('meta', 'lang:meta', 'trim');
        $this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
        
        
        // validate the form
        if ($this->form_validation->run() == FALSE)
        {
            $this->view($this->config->item('admin_folder').'/brand_form', $data);
        }
        else
        {
            
            $uploaded   = $this->upload->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($data['image'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$data['image'];
                        $file[] = 'uploads/images/medium/'.$data['image'];
                        $file[] = 'uploads/images/small/'.$data['image'];
                        $file[] = 'uploads/images/thumbnails/'.$data['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                $data['error']  = $this->upload->display_errors();
                if($_FILES['image']['error'] != 4)
                {
                    $data['error']  .= $this->upload->display_errors();
                    $this->view($this->config->item('admin_folder').'/brand_form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image          = $this->upload->data();
                $save['image']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image'];
                $config['new_image']    = 'uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }
            
            $this->load->helper('text');
            
            //first check the slug field
            $slug = $this->input->post('slug');
            
            //if it's empty assign the name field
            if(empty($slug) || $slug=='')
            {
                $slug = $this->input->post('name');
            }
            
            $slug   = url_title(convert_accented_characters($slug), 'dash', TRUE);
            
            //validate the slug
            $this->load->model('Routes_model');
            if($id)
            {
                $slug   = $this->Routes_model->validate_slug($slug, $brand->route_id);
                $route_id   = $brand->route_id;
            }
            else
            {
                $slug   = $this->Routes_model->validate_slug($slug);
                
                $route['slug']  = $slug;    
                $route_id   = $this->Routes_model->save($route);
            }
            
            $save['id']             = $id;
            $save['name']           = $this->input->post('name');
            $save['description']    = $this->input->post('description');
            $save['seo_title']      = $this->input->post('seo_title');
            $save['meta']           = $this->input->post('meta');
            $save['enabled']        = $this->input->post('enabled');
            $save['route_id']       = intval($route_id);
            $save['slug']           = $slug;
            
            $brand_id    = $this->Brand_model->save($save);
            
            //save the route
            $route['id']    = $route_id;
            $route['slug']  = $slug;
            $route['route'] = 'cart/brand/'.$brand_id.'';
            
            $this->Routes_model->save($route);
            
            $this->session->set_flashdata('message', 'Brand Saved.');
            
            redirect($this->config->item('admin_folder').'/brands');
        }
    }

    function delete($id)
    {
        
        $brand   = $this->Brand_model->get_brand($id);
        if ($brand)
        {
            $this->load->model('Routes_model');
            
            $this->Routes_model->delete($brand->route_id);
            $this->Brand_model->delete($id);
            
            $this->session->set_flashdata('message', 'Brand deleted successfully..');
        }
        else
        {
            $this->session->set_flashdata('error', lang('error_not_found'));
        }
        redirect($this->config->item('admin_folder').'/brands');
    }
    
    
}