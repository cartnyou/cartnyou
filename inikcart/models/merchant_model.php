<?php
Class Merchant_model extends CI_Model
{

    var $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI =& get_instance();
        $this->CI->load->database(); 
        $this->CI->load->helper('url');
    }
    
    function get_customers($limit=0, $offset=0, $order_by='id', $direction='DESC')
    {
        $this->db->order_by($order_by, $direction);
        if($limit>0)
        {
            $this->db->limit($limit, $offset);
        }

        $result = $this->db->get('customers');
        return $result->result();
    }
    
    function count_customers()
    {
        return $this->db->count_all_results('customers');
    }
    
    function get_merchant($id)
    {
        
        $result = $this->db->get_where('merchants', array('id'=>$id));
        return $result->row();
    }
    
    function get_subscribers()
    {
        $this->db->where('email_subscribe','1');
        $res = $this->db->get('customers');
        return $res->result_array();
    }
    
    function get_address_list($id)
    {
        $addresses = $this->db->where('customer_id', $id)->get('customers_address_bank')->result_array();
        // unserialize the field data
        if($addresses)
        {
            foreach($addresses as &$add)
            {
                $add['field_data'] = unserialize($add['field_data']);
            }
        }
        
        return $addresses;
    }
    
    function get_address($address_id)
    {
        $address= $this->db->where('id', $address_id)->get('customers_address_bank')->row_array();
        if($address)
        {
            $address_info           = unserialize($address['field_data']);
            $address['field_data']  = $address_info;
            $address                = array_merge($address, $address_info);
        }
        return $address;
    }
    
    function save_address($data)
    {
        // prepare fields for db insertion
        $data['field_data'] = serialize($data['field_data']);
        // update or insert
        if(!empty($data['id']))
        {
            $this->db->where('id', $data['id']);
            $this->db->update('customers_address_bank', $data);
            return $data['id'];
        } else {
            $this->db->insert('customers_address_bank', $data);
            return $this->db->insert_id();
        }
    }
    
    function delete_address($id, $customer_id)
    {
        $this->db->where(array('id'=>$id, 'customer_id'=>$customer_id))->delete('customers_address_bank');
        return $id;
    }
    
    function save($save)
    {
        if ($save['id'])
        {
            $this->db->where('id', $save['id']);
            $this->db->update('merchants', $save);
            return $save['id'];
        }
        else
        {
            $this->db->insert('merchants', $save);
            return $this->db->insert_id();
        }
    }
    
    function deactivate($id)
    {
        $customer   = array('id'=>$id, 'active'=>0);
        $this->save_customer($customer);
    }
    
    function delete($id)
    {
        /*
        deleting a customer will remove all their orders from the system
        this will alter any report numbers that reflect total sales
        deleting a customer is not recommended, deactivation is preferred
        */
        
        //this deletes the customers record
        $this->db->where('id', $id);
        $this->db->delete('customers');
        
        // Delete Address records
        $this->db->where('customer_id', $id);
        $this->db->delete('customers_address_bank');
        
        //get all the orders the customer has made and delete the items from them
        $this->db->select('id');
        $result = $this->db->get_where('orders', array('customer_id'=>$id));
        $result = $result->result();
        foreach ($result as $order)
        {
            $this->db->where('order_id', $order->id);
            $this->db->delete('order_items');
        }
        
        //delete the orders after the items have already been deleted
        $this->db->where('customer_id', $id);
        $this->db->delete('orders');
    }
    
    function check_email($str, $id=false)
    {
        $this->db->select('email');
        $this->db->from('merchants');
        $this->db->where('email', $str);
        if ($id)
        {
            $this->db->where('id !=', $id);
        }
        $count = $this->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function check_phone($str, $id=false)
    {
        $this->db->select('phone');
        $this->db->from('merchants');
        $this->db->where('phone', $str);
        if ($id)
        {
            $this->db->where('id !=', $id);
        }
        $count = $this->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    /*
    these functions handle logging in and out
    */
    function logout()
    {
        $this->CI->session->unset_userdata('customer');
        //force expire the cookie
        $this->generateCookie('[]', time()-3600);
        $this->go_cart->destroy(false);
    }

    private function generateCookie($data, $expire)
    {
        setcookie('InikCartCustomer', $data, $expire, '/', $_SERVER['HTTP_HOST']);
    }
    
    function login($email, $password, $remember=false)
    {
        $this->db->select('*');
        $this->db->where('email', $email);
        $this->db->where('active', 1);
        $this->db->where('password',  sha1($password));
        $this->db->limit(1);
        $result = $this->db->get('customers');
        $customer   = $result->row_array();
        
        if ($customer)
        {
            
            // Retrieve customer addresses
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_billing_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address)
            {
                $fields = unserialize($address['field_data']);
                $customer['bill_address'] = $fields;
                $customer['bill_address']['id'] = $address['id']; // save the addres id for future reference
            }
            
            $this->db->where(array('customer_id'=>$customer['id'], 'id'=>$customer['default_shipping_address']));
            $address = $this->db->get('customers_address_bank')->row_array();
            if($address)
            {
                $fields = unserialize($address['field_data']);
                $customer['ship_address'] = $fields;
                $customer['ship_address']['id'] = $address['id'];
            } else {
                 $customer['ship_to_bill_address'] = 'true';
            }
            
            
            // Set up any group discount 
            if($customer['group_id']!=0) 
            {
                $group = $this->get_group($customer['group_id']);
                if($group) // group might not exist
                {
                    $customer['group'] = $group;
                }
            }
            
            if($remember)
            {
                $loginCred = json_encode(array('email'=>$email, 'password'=>$password));
                $loginCred = base64_encode($this->aes256Encrypt($loginCred));
                //remember the user for 6 months
                $this->generateCookie($loginCred, strtotime('+6 months'));
            }
            
            // put our customer in the cart
            $this->go_cart->save_customer($customer);

        
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function is_logged_in($redirect = false, $default_redirect = 'secure/login/')
    {
        
        //$redirect allows us to choose where a customer will get redirected to after they login
        //$default_redirect points is to the login page, if you do not want this, you can set it to false and then redirect wherever you wish.
        
        $customer = $this->go_cart->customer();
        if (!isset($customer['id']))
        {
            //check the cookie
            if(isset($_COOKIE['InikCartCustomer']))
            {
                //the cookie is there, lets log the customer back in.
                $info = $this->aes256Decrypt(base64_decode($_COOKIE['InikCartCustomer']));
                $cred = json_decode($info, true);

                if(is_array($cred))
                {
                    if( $this->login($cred['email'], $cred['password']) )
                    {
                        return $this->is_logged_in($redirect, $default_redirect);
                    }
                }
            }

            //this tells inikcart where to go once logged in
            if ($redirect)
            {
                $this->session->set_flashdata('redirect', $redirect);
            }
            
            if ($default_redirect)
            {   
                redirect($default_redirect);
            }
            
            return false;
        }
        else
        {
            return true;
        }
    }

    function logged_customer_first_name(){
        $customer = $this->go_cart->customer();
        return $customer['firstname'];
    }
    
    function reset_password($email)
    {
        $this->load->library('encrypt');
        $merchant = $this->get_merchant_by_email($email);

        if ($merchant)
        {
            $this->load->helper('string');
            $this->load->library('email');
            
            $new_password       = random_string('alnum', 8);
            $merchant['password']   = sha1($new_password);
            $this->save($merchant);
            
            $this->load->library('email');
            $config_mail = Array(
                'protocol'  => 'smtp',
                'smtp_host' => 'ssl://smtp.zoho.com',
                'smtp_port' => '465',
                'smtp_user' => 'auto-order-confirmation@gojojo.in',
                'smtp_pass' => 'Gojojo123#',
                'mailtype'  => 'html',
                'newline'   => "\r\n"
            );
            $this->email->initialize($config_mail);

            $this->email->from('auto-order-confirmation@gojojo.in', $this->config->item('site_name'));
            $this->email->to($email);
            $this->email->subject($this->config->item('site_name').': Password Reset');
            $this->email->message('Your password has been reset to <strong>'. $new_password .'</strong>.');
            $this->email->send();

            //echo $this->email->print_debugger();exit;
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function get_customer_by_email($email)
    {
        $result = $this->db->get_where('customers', array('email'=>$email));
        return $result->row_array();
    }

    function get_merchant_by_email($email)
    {
        $result = $this->db->get_where('merchants', array('email'=>$email));
        return $result->row_array();
    }

    private function aes256Encrypt($data)
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
    }
    private function aes256Decrypt($data)
    {
        $key = config_item('encryption_key');
        if(32 !== strlen($key))
        {
            $key = hash('SHA256', $key, true);
        }
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
        $padding = ord($data[strlen($data) - 1]); 
        return substr($data, 0, -$padding); 
    }

    // Customer groups functions
    function get_groups()
    {
        return $this->db->get('customer_groups')->result();     
    }
    
    function get_group($id)
    {
        return $this->db->where('id', $id)->get('customer_groups')->row();      
    }
    
    function delete_group($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('customer_groups');
    }
    
    function save_group($data)
    {
        if(!empty($data['id'])) 
        {
            $this->db->where('id', $data['id'])->update('customer_groups', $data);
            return $data['id'];
        } else {
            $this->db->insert('customer_groups', $data);
            return $this->db->insert_id();
        }
    }

    function get_merchants_ratings($count=false, $rows=false, $page=false){
        if($count){
            return $this->db->count_all_results('seller_ratings');
        } else {
            $this->db->select('merchants.store_name, customers.firstname AS customer_firstname, customers.lastname AS customer_lastname, seller_ratings.*');
            $this->db->join('merchants','merchants.id=seller_ratings.merchant_id');
            $this->db->join('customers','customers.id=seller_ratings.customer_id');
            $this->db->order_by('seller_ratings.created_at','desc');

            if($rows){
                $this->db->limit($rows);
            }
            if($page){
                $this->db->offset($page);
            }
            return $this->db->get('seller_ratings')->result();
        }
    }

    function rating_change($sr_id){
        $this->db->query('UPDATE seller_ratings SET sr_enabled = IF(sr_enabled=1, 0, 1) WHERE sr_id='.$sr_id);
        return $this->db->affected_rows();
    }

    function get_merchants($count=false, $rows=false, $page=false, $status=false, $name=false){
        if($status){
            $this->db->where('enabled',$status);
        }

        if($count){
            return $this->db->count_all_results('merchants');
        } else {
            $this->db->select('merchants.*');
            if($name){
                $this->db->like('store_name',$name);
            }
            $this->db->order_by('merchants.created_at','desc');

            if($rows){
                $this->db->limit($rows);
            }
            if($page){
                $this->db->offset($page);
            }
            return $this->db->get('merchants')->result();
        }
    }

    function merchant_status($id, $status=false){
        if($status){
            if($status == 'enable'){
                $status = 1;
            } else if($status == 'disable'){
                $status = 0;
            }
            $this->db->query('UPDATE merchants SET enabled = '.$status.' WHERE id='.$id);
        } else {
            $this->db->query('UPDATE merchants SET enabled = IF(enabled=1, 0, 1) WHERE id='.$id);
        }
        return $this->db->affected_rows();
    }

    function check_slug($slug, $id=false)
    {
        if($id)
        {
            $this->db->where('id !=', $id);
        }
        $this->db->where('slug', $slug);
        
        return (bool) $this->db->count_all_results('merchants');
    }
    
    function validate_slug($slug, $id=false, $count=false)
    {
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count  = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }

    function login_via_id($merchant_id, $password){
        if(!$merchant_id){
            return false;
        }

        $this->db->select('id');
        $this->db->where('id', $merchant_id);
        $this->db->where('password',  sha1($password));
        $this->db->limit(1);
        $result = $this->db->get('merchants');
        $result = $result->row_array();
        
        if (sizeof($result) > 0){
            return true;
        }
        return false;
    }
    //shahwaz
    public function sellar_registration($save,$images,$categories){
        
        $this->db->trans_begin();
        $this->db->insert('merchants', $save);
        $merchant_id=$this->db->insert_id();
        
        $data=array(
                    "md_merchant_id"=>$merchant_id,
                    "md_address_is_same"=>$this->input->post('is_same')==''?0:1,
                    "md_address"=>$this->input->post('c_address'),
                    "md_address_one"=>$this->input->post('c_address_one'),
                    "md_address_two"=>$this->input->post('c_address_two'),
                    "md_city"=>$this->input->post('c_city'),
                    "md_state"=>$this->input->post('c_state'),
                    "md_zip"=>$this->input->post('c_pincode'),
                    "md_name"=>$this->input->post('oo_name'),
                    "md_email"=>$this->input->post('oo_phone'),
                    "md_phone"=>$this->input->post('oo_email'),
                    "md_catalogue_size"=>$this->input->post('catalogue_size'),
                    "md_years"=>$this->input->post('year_of_operation'),
                    "md_sla"=>$this->input->post('products_sla'),
                    "md_company_number"=>$this->input->post('company_registration'),
                    "md_cancelled_cheque"=>empty($images)?'':$images[2],
                    "md_company_details"=>empty($images)?'':$images[3],
                    "md_company_gst"=>empty($images)?'':$images[4],
                );
        $this->db->insert('merchant_details',$data);
        $push=array();
        foreach($categories as $key=>$value){
            $push[$key]=array(
                            "merchant_id"=>$merchant_id,
                            "category_id"=>$value,
                            "enabled"=>0
                            );
        }
        $this->db->insert_batch('merchant_categories',$push);
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return $merchant_id;
        }
        
    }
    //ends here

}