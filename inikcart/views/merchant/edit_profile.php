<style type="text/css">
    .pagination {
        margin:0px;
    }
</style>

<?php echo form_open_multipart($this->config->item('merchant_folder').'/profile/index', array('class'=>'form-horizontal form-row-seperated') ); ?>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>First Name</label>
            <?php
            $data   = array('name'=>'firstname', 'value'=>set_value('firstname', $firstname), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Company Name</label>
            <?php
            $data   = array('name'=>'store_name', 'value'=>set_value('store_name', $store_name), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Mobile Number</label>
            <?php
            $data   = array('name'=>'phone', 'value'=>set_value('phone', $phone), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Address</label>
            <?php
            $data   = array('name'=>'address', 'value'=>set_value('address', $address), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>State</label>
            <?php
            $data   = array('name'=>'state', 'value'=>set_value('state', $state), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Which Service You are Interested in</label>
            <?php 
            $services = array("Sell at Gojojo" => "Sell at Gojojo", "Sell at Gojojo/ Shipping Panel for COD" => "Sell at Gojojo/ Shipping Panel for COD");
            echo form_dropdown('service', $services, $service, 'class="form-control" ');
            ?>
        </div>

        <div class="form-group">
            <label>Bank Name</label>
            <?php
            $data   = array('name'=>'bank_name', 'value'=>set_value('bank_name', $bank_name), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>IFSC</label>
            <?php
            $data   = array('name'=>'ifsc', 'value'=>set_value('ifsc', $ifsc), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>

        <div class="form-group">
            <label>
                Description
            </label>
            <?php
            $data   = array('name'=>'description', 'class'=>'form-control', 'value'=>set_value('description', $description));
            echo form_textarea($data);
            ?>
        </div>
        <div class="form-group">
            <label>
                Upload Pancard (gif/jpg/png)
            </label>
            <?php if($pan != ''){ ?>
                <br><img src="<?php echo base_url('uploads/images/thumbnails/'.$pan);?>" alt="">
            <?php } ?>
            <input type="file" class="form-control" name="pan">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Last Name</label>
            <?php
            $data   = array('name'=>'lastname', 'value'=>set_value('lastname', $lastname), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Email</label>
            <?php
            $data   = array('name'=>'email', 'value'=>set_value('email', $email), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Landline Number</label>
            <?php
            $data   = array('name'=>'landline', 'value'=>set_value('landline', $landline), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>City</label>
            <?php
            $data   = array('name'=>'city', 'value'=>set_value('city', $city), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Zip</label>
            <?php
            $data   = array('name'=>'zip', 'value'=>set_value('zip', $zip), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>

        <!-- <div class="form-group">
            <label>VAT and CST(or TIN) Registration</label>
            <?php
            $data   = array('name'=>'cst', 'value'=>set_value('cst', $cst), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div> -->
        <div class="form-group">
            <label>GST Registration No.</label>
            <?php
            $data   = array('name'=>'cst', 'value'=>set_value('cst', $cst), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Account Number</label>
            <?php
            $data   = array('name'=>'account_number', 'value'=>set_value('account_number', $account_number), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>
        <div class="form-group">
            <label>Pan Number</label>
            <?php
            $data   = array('name'=>'pan_number', 'value'=>set_value('pan_number', $pan_number), 'class'=>'form-control');
            echo form_input($data);
            ?>
        </div>

        <div class="form-group">
            <label>
                Upload Logo (gif/jpg/png)
            </label>
            <?php if($logo != ''){ ?>
                <br><img src="<?php echo base_url('uploads/images/thumbnails/'.$logo);?>" alt="">
            <?php } ?>
            <input type="file" class="form-control" name="logo">
        </div>
        <div class="form-group">
            <label>
                Upload Signature (gif/jpg/png)
            </label>
            <?php if($sign != ''){ ?>
                <br><img src="<?php echo base_url('uploads/images/thumbnails/'.$sign);?>" alt="">
            <?php } ?>
            <input type="file" class="form-control" name="sign">
        </div>

        <!-- <div class="form-group">
            <label>
                Upload Brand Certificates [Multiple]
            </label>
            <input type="file" class="form-control" name="brand_certificates" multiple="multiple">
        </div> -->

    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="form-group">
            <button class="btn btn-primary" type="submit" name="submit" value="submit">Submit</button>
        </div>
    </div>
</div>

</form>