<div class="row">
	<div class="container">
		<?php foreach($banners as $banner):?>
		<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 banner-circle-div">

			<?php		
			if($banner->link != '')
			{
				$target	= false;
				if($banner->new_window)
				{
					$target = 'target="_blank"';
				}
				echo '<a href="'.$banner->link.'" '.$target.' >';
			}
			?>
			<div class="banner-circle">
				<div class="image">
					<img class="animate-scale" src="<?php echo base_url('uploads/'.$banner->image);?>" />
				</div>
				<div class="title">
					<span><?php echo $banner->name;?></span>
				</div>
			</div>

			<?php		
			if($banner->link != ''){
				echo '</a>';
			} ?>

		</div>
		<?php endforeach;?>
	</div>
</div>