<?php if(($this->uri->segment(2) == 'view_cart') || ($this->uri->segment(2) == 'view-cart')): ?>
	<div class="cartCard cart-summary-items-parent">
<?php else: ?>
	<div class="cart-summary-items-parent">
<?php endif; ?>
	<?php
	$subtotal = 0;

	foreach ($this->go_cart->contents() as $cartkey=>$product):?>
		<?php //echo $cartkey;?>
		<?php //unset($product['meta']);unset($product['quantity']);unset($product['subtotal']);unset($product['total']);unset($product['description']);unset($product['specification']);print_r(json_encode($product));?>
	    <div class="row cart-summary-item">
	        <div class="col-md-12 cBox posr ">
	            <div class="col-md-2 col-sm-2 col-xs-4 pl0 pr0">
	                <div class="cart-product text-center">
	                    <a href="<?php echo site_url($product['slug']); ?>">
	                    	<?php
							$photo  = theme_img('no_picture.png', lang('no_image_available'));
							
							//if(!empty($product['images'])){
							if($product['images'] && !empty($product['images']) && ($product['images'] != 'false') ){
								$product['images']    = array_values(json_decode($product['images'], true));
							} else {
								$product['images']    = json_decode($product['images'], true);
							}

							if(!empty($product['images'][0]))
							{
								$primary    = $product['images'][0];
								foreach($product['images'] as $photo)
								{
									if(isset($photo['primary']))
									{
										$primary    = $photo;
									}
								}

								$photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/small/'.$primary['filename']).'" width="100px" />';
							}
							echo $photo;
							?>
	                    </a>
	                </div>
	            </div>
	            <div class="col-md-10 col-sm-10 col-xs-8 pr0">
	                <div class="col-md-6 col-sm-6 mb-5 pl0 pr0">
	                    <p class="mtb-3"><a href="<?php echo site_url($product['slug']);?>" class="color-red"><strong><?php echo ucwords(strtolower($product['name']));?></strong></a></p>
	                    
						<?php echo $product['excerpt'];?>
						<ul class="variation">
							<?php
							if(isset($product['options'])) {
								foreach ($product['options'] as $name=>$value)
								{
									if(is_array($value))
									{
										echo '<li class="variation-Color">'.$name.':<br/>';
										foreach($value as $item)
											echo '- '.$item.'<br/>';
										echo '</li>';
									} 
									else 
									{
										echo '<li class="variation-Color">'.$name.': <span>'.$value.'</span></li>';
									}
								}
							}
							?>
						</ul>
						<?php echo "<b>Seller:</b> ".$product['store_name'];?>
	                </div>
	                <div class="col-md-3 col-sm-6 col-xs-6 pull-left mb-5 pr0">
	                    <?php //if($this->uri->segment(1) == 'cart'): ?>
							<?php if(!(bool)$product['fixed_quantity']):?>
								<div class="control-group text-center">
									<div class="controls">
										<div class="input-append">
											<input class="update-cart-quantity-input <?php echo $cartkey;?> hidden" name="cartkey[<?php echo $cartkey;?>]"  value="<?php echo $product['quantity'] ?>" size="1" type="text">
											
											<span class="iga">
												<?php if($product['quantity'] > 1): ?>
						                        	<i class="fa fa-minus arcart update-cart-btn" data-type="minus" data-quantity="<?php echo $product['quantity'];?>" data-cartkey="<?php echo $cartkey;?>"></i>
						                    	<?php else: ?>
						                    		<i class="fa fa-minus arcart"></i>
						                    	<?php endif;?>
						                    </span>
						                    <span class="prodNum">
						                        <?php echo $product['quantity'];?>
						                    </span>
						                    <span class="iga">
						                        <i class="fa fa-plus arcart update-cart-btn" data-type="plus" data-quantity="<?php echo $product['quantity'];?>" data-cartkey="<?php echo $cartkey;?>"></i>
						                    </span>

						                    <div class="text-center">
												<button class="btn-remove btn btn-red btn-sm" type="button" onclick="if(confirm('<?php echo lang('remove_item');?>')){window.location='<?php echo site_url('cart/remove_item/'.$cartkey);?>';}">
													Remove
												</button>
											</div>
										</div>
									</div>
								</div>
							<?php else:?>
								<div class="text-center">
									<?php echo $product['quantity'] ?>
									<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>

									<div class="text-center">
										<button class="btn-remove btn btn-red btn-sm" type="button" onclick="if(confirm('<?php echo lang('remove_item');?>')){window.location='<?php echo site_url('cart/remove_item/'.$cartkey);?>';}">
											Remove
										</button>
									</div>
								</div>
							<?php endif;?>
						<?php //else: ?>
							<?php //echo $product['quantity'] ?>
						<?php //endif;?>
	                    
	                </div>
	                <div class="col-md-3 col-sm-6 mb-5 pr0">
	                	<span class="pull-right text-center">
	                    	<?php echo format_currency($product['price']*$product['quantity']); ?>
	                    	<br>
	                    	<?php
							if($product['shipping'] > 0){
								/*echo '<b>Shipping:</b> '.format_currency($product['shipping']);*/
							} else {
								/*echo '<b>Free Shipping</b>';*/
							}
	                    	?>
	                    	<br>
	                    	<?php
							if($product['coupon_discount'] > 0){
								echo '<b>Discount:</b> '.format_currency($product['coupon_discount']*$product['quantity']);
							}
	                    	?>
	                    </span>
	                </div>
	            </div>

	        </div>
	    </div>
    <?php endforeach;?>

</div>

<script>
$(function(){
	$('.update-cart-btn').on('click',function(){
		var quantity = parseInt($(this).attr('data-quantity'));
		var type = $(this).attr('data-type');
		var cartkey = $(this).attr('data-cartkey');

		if(type == 'minus'){
			if(quantity > 1){
				quantity--;
			}
		} else {
			quantity++;
		}
		$('.'+cartkey).val(quantity);
		$('#update_cart_form').submit();
	});
});
</script>