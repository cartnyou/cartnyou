<?php if(($this->uri->segment(2) == 'view_cart') || ($this->uri->segment(2) == 'view-cart')): ?>
<div class="cartCard cart-summary-items-parent">
    <?php else: ?>
    <div class="cart-summary-items-parent">
        <?php endif; ?>
        <?php
        $subtotal = 0;

        foreach ($this->go_cart->contents() as $cartkey=>$product):?>
            <div class="row cart-summary-item">
                <div class="col-md-12 cBox posr ">
                    <div class="col-md-4 col-sm-4 col-xs-4 pl0 pr0">
                        <div class="cart-product text-center">
                            <a href="<?php echo site_url($product['slug']); ?>">
                                <?php
                                $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                if($product['images'] && !empty($product['images']) && ($product['images'] != 'false') ){
                                    $product['images']    = array_values(json_decode($product['images'], true));
                                } else {
                                    $product['images']    = json_decode($product['images'], true);
                                }

                                if(!empty($product['images'][0]))
                                {
                                    $primary    = $product['images'][0];
                                    foreach($product['images'] as $photo)
                                    {
                                        if(isset($photo['primary']))
                                        {
                                            $primary    = $photo;
                                        }
                                    }

                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/small/'.$primary['filename']).'" width="100px" />';
                                }
                                echo $photo;
                                ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 pr0">
                        <div class="col-md-12 col-sm-12 mb-5 pl0 pr0">
                            <p class="mtb-3"><a href="<?php echo site_url($product['slug']);?>" class="color-red"><span><?php echo ucwords(strtolower($product['name']));?></span></a></p>

                            <?php echo $product['excerpt'];?>
                            <ul class="variation">
                                <?php
                                if(isset($product['options'])) {
                                    foreach ($product['options'] as $name=>$value)
                                    {
                                        if(is_array($value))
                                        {
                                            echo '<li class="variation-Color">'.$name.':<br/>';
                                            foreach($value as $item)
                                                echo '- '.$item.'<br/>';
                                            echo '</li>';
                                        }
                                        else
                                        {
                                            echo '<li class="variation-Color">'.$name.': <span>'.$value.'</span></li>';
                                        }
                                    }
                                }
                                ?>
                            </ul>
                            <?php echo "<b>Seller:</b> ".$product['store_name'];?>
                        </div>

                    </div>

                </div>
            </div>
        <?php endforeach;?>

    </div>

