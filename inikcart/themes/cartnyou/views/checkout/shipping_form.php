<!-- BREADCRUMBS -->
<section class="breadcrumb parallax margbot30"></section>
<!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

	<!-- CONTAINER -->
	<div class="container border0 margbot0">
		<h3 class="pull-left"><b>Checkout</b></h3>

		<div class="pull-right">
			<a href="<?php echo site_url();?>" >Back shopping bag<i class="fa fa-angle-right"></i></a>
		</div>
	</div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->


<!-- CHECKOUT PAGE -->
<section class="checkout_page">

	<!-- CONTAINER -->
	<div class="container">

		<?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('message');?>
            </div>
        <?php endif;?>
        
        <?php if ($this->session->flashdata('error')):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if (!empty($error)):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $error;?>
            </div>
        <?php endif;?>

        <?php if (validation_errors()):?>
        	<div class="alert alert-danger">
        		<a class="close" data-dismiss="alert">×</a>
        		<?php echo validation_errors();?>
        	</div>
        <?php endif;?>

		<!-- CHECKOUT BLOCK -->
		<div class="checkout_block">
			<ul class="checkout_nav">
				<li class="done_step">1. Shipping Address</li>
				<li class="active_step">2. Delivery</li>
				<li>3. Payment</li>
				<li class="last">4. Confirm Order</li>
			</ul>
			
			<?php include('order_details.php');?>
			<hr>

			<div class="checkout_delivery clearfix">

				<h2>SHIPPING METHOD</h2>

				<?php echo form_open('checkout/step_2');?>
					<ul>
						<?php
						foreach($shipping_methods as $key=>$val):
							$ship_encoded	= md5(json_encode(array($key, $val)));
						
							$checkedHTML = '';
							if($ship_encoded == $shipping_code)
							{
								$checked = true;
								$checkedHTML = 'checked="checked"';
							}
							else
							{
								$checked = false;
							}
						?>
							<li>
								<input id="s<?php echo $ship_encoded;?>" type="radio" name="shipping_method" hidden value="<?php echo $ship_encoded;?>" <?php echo $checkedHTML;?> />
								<label for="s<?php echo $ship_encoded;?>"><?php echo $key;?> <b><?php echo $val['str'];?></b></label>
							</li>
							<!-- <li>
								<?php echo form_radio('shipping_method', $ship_encoded, set_radio('shipping_method', $ship_encoded, $checked), 'id="s'.$ship_encoded.'"', 'hidden');?>
								<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><?php echo $key;?></td>
								<td onclick="toggle_shipping('s<?php echo $ship_encoded;?>');"><strong><?php echo $val['str'];?></strong></td>
							</li> -->

						<?php endforeach;?>
					</ul>

					<div class="alert alert-danger" id="shipping_error_box" style="display:none"><div class="checkout_delivery_note"><i class="fa fa-exclamation-circle"></i></div></div>

					<h3><?php echo lang('shipping_instructions')?></h3>
					<?php echo form_textarea(array('name'=>'shipping_notes', 'value'=>set_value('shipping_notes', $this->go_cart->get_additional_detail('shipping_notes')), 'class'=>'col-md-12', 'style'=>'height:75px;'));?>

					<input class="btn active pull-right checkout_block_btn" type="submit" value="<?php echo lang('form_continue');?>"/>

				</form>
			</div>
		</div><!-- //CHECKOUT BLOCK -->
	</div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->

<script>
	function toggle_shipping(key)
	{
		var check = $('#'+key);
		if(!check.attr('checked'))
		{
			check.attr('checked', true);
		}
	}
</script>