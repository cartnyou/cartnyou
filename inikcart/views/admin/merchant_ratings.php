<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Store/Merchant</th>
			<th>Customer</th>
			<th>Rating(5)</th>
			<th>Review</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo ucwords($row->store_name);?></td>
					<td><?php echo ucwords($row->customer_firstname.' '.$row->customer_lastname);?></td>
					<td>
						<b>Item as described:</b> <?php echo $row->rating_item;?><br>
						<b>Communication:</b> <?php echo $row->rating_communication;?><br>
						<b>Shipping time:</b> <?php echo $row->rating_time;?><br>
						<b>Shipping charges:</b> <?php echo $row->rating_charges;?>
					</td>
					<td><?php echo $row->review;?></td>
					<td><?php echo ($row->sr_enabled == '1') ? lang('enabled') : lang('disabled'); ?></td>
					<td>
						<div class="btn-group" style="float:right">
							<?php if($row->sr_enabled == '1'){ ?>
								<a class="btn" href="<?php echo  site_url(ADMIN_FOLDER.'/merchants/rating_change/'.$row->sr_id);?>"><i class="icon-remove"></i> Disable</a>
							<?php } else { ?>
								<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/merchants/rating_change/'.$row->sr_id);?>"><i class="icon-ok"></i> Enable</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="4">No rating found.</td></tr>';
		}
		?>
	</tbody>
</table>