<!-- BREADCRUMBS -->
<section class="breadcrumb men parallax margbot30" style="background-image:url(<?php echo theme_img('breadcrumb_bg2.jpg');?>">
    <div class="container">
        <h2>Search Results</h2>
    </div>
</section>
<!-- //BREADCRUMBS -->


<!-- SHOP BLOCK -->
<section class="shop">
    
    <!-- CONTAINER -->
    <div class="container">
        
        <?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('message');?>
            </div>
        <?php endif;?>
        
        <?php if ($this->session->flashdata('error')):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if (!empty($error)):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $error;?>
            </div>
        <?php endif;?>
    
        <!-- ROW -->
        <div class="row">
            
            <!-- SHOP PRODUCTS -->
            <div class="col-lg-12 col-sm-12 padbot20">
                
                <!-- SORTING TOVAR PANEL -->
                <div class="sorting_options clearfix">
                    
                    <!-- COUNT TOVAR ITEMS -->
                    <div class="count_tovar_items">
                        <span><?php echo count($products);?> Items</span>
                    </div><!-- //COUNT TOVAR ITEMS -->

                    <!-- TOVAR FILTER -->
                    <?php if(count($products) > 0):?>
                        <div class="product_sort">
                            <p>SORT BY</p>
                            <select class="basic" id="sort_products" onchange="window.location='<?php echo site_url(uri_string());?>/'+$(this).val();">
                                <option value=''><?php echo lang('default');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="?by=name/asc"><?php echo lang('sort_by_name_asc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="?by=name/desc"><?php echo lang('sort_by_name_desc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="?by=price/asc"><?php echo lang('sort_by_price_asc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="?by=price/desc"><?php echo lang('sort_by_price_desc');?></option>
                            </select>
                        </div>
                    <?php endif;?>
                    <!-- //TOVAR FILTER -->
                </div><!-- //SORTING TOVAR PANEL -->
                

                <!-- ROW -->
                <div class="row shop_block">

                    <?php if(count($products) == 0):?>
                        <h2 style="margin:50px 0px; text-align:center;">
                            <?php echo lang('no_products');?>
                        </h2>
                    <?php elseif(count($products) > 0):?>

                        <?php
                        foreach($products as $product): ?>

                            <div class="tovar_wrapper col-xs-6 col-ss-12 padbot40 mb40">
                                <div class="tovar_item clearfix">
                                    <div class="tovar_img equal-height-img">
                                        <div class="tovar_img_wrapper">
                                            <?php
                                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                            $product->images    = array_values($product->images);
                                
                                            if(!empty($product->images[0]))
                                            {
                                                $primary    = $product->images[0];
                                                foreach($product->images as $photo)
                                                {
                                                    if(isset($photo->primary))
                                                    {
                                                        $primary    = $photo;
                                                    }
                                                }

                                                $photo  = '<img class="img" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                            }
                                            echo $photo;
                                            ?>
                                        </div>
                                        <div class="tovar_item_btns">
                                            <!--
                                            <div class="open-project-link"><a class="open-project tovar_view" href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>" ><span>quick</span> view</a></div>
                                            <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                            <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            -->
                                            <a class="tovar_view" href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>" ><i class="fa fa-shopping-cart"></i> View</a>
                                        </div>
                                    </div>
                                    <div class="tovar_description clearfix">
                                        <a class="tovar_title text-center" href="<?php echo site_url(implode('/', $base_url).'/'.$product->slug); ?>" >
                                            <?php echo $product->name;?>
                                        </a>

                                        <!-- <?php if($this->session->userdata('admin')): ?>
                                            <a class="btn" title="<?php echo lang('edit_product'); ?>" href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$product->id); ?>"><i class="fa fa-pencil"></i></a>
                                        <?php endif; ?> -->

                                        <?php if($product->excerpt != ''): ?>
                                            <div class="excerpt"><?php echo $product->excerpt; ?></div>
                                        <?php endif; ?>

                                        <span class="tovar_price text-center">
                                            <?php if($product->saleprice > 0):?>
                                                <span class="price-slash"><?php echo format_currency($product->price); ?></span>
                                                <span class="price-sale"><?php echo format_currency($product->saleprice); ?></span>
                                            <?php else: ?>
                                                <span class="price-reg"><?php echo format_currency($product->price); ?></span>
                                            <?php endif; ?>
                                        </span>

                                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                                            <div class="stock_msg text-center"><?php echo lang('out_of_stock');?></div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                        <?php
                        endforeach;
                    endif;
                    ?>

                </div><!-- //ROW -->
                
                <hr>
                <div class="clearfix text-center">
                    <!-- PAGINATION -->
                    <?php echo $this->pagination->create_links();?>
                    <!-- //PAGINATION -->
                </div>
                
            </div><!-- //SHOP PRODUCTS -->
        </div><!-- //ROW -->
    </div><!-- //CONTAINER -->
</section><!-- //SHOP -->

<script>
$(function(){ 
    //$('.equal-height-img img').equalHeights();
    $('.tovar_item').equalHeights();
});
</script>