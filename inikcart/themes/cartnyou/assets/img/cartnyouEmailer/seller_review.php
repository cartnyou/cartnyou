<!DOCTYPE html>
<html>
  <head>
    <style media="screen">
    .rate{
      width: 60px;
      height: 16px;
      display: inline-block;
      background: url(http://35.167.215.182/gojojoEmailer/img/st.png) 0 0 no-repeat;
      background-position-x: 0px;
      background-position-y: 0px;
    }
    .rate-0 {
      background-position: -60px 0;
    }
      .rate-1{
        background-position: -47px 0;
      }
      .rate-2{
        background-position: -35px 0;
      }
      .rate-3{
        background-position: -23px 0;
      }
      .rate-4{
        background-position: -11px 0;
      }
      .rate-5{
        background-position: 0 0;
      }

    </style>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">
  <table style="background: #fff;padding: 0 10px;" width="600" cellspacing="0" cellpadding="0" border="0">
      <tbody>
      <tr style="background: #fff;">
          <td width="300" style="padding: 30px 0px 8px 10px;width: 130px;"><a href="<?php echo site_url();?>"><img style="width: 130px;max-width: 200px;" border="0" src="http://35.167.215.182/gojojoEmailer/img/logo.png" alt="" width="130" /></a></td>
          <td width="300" style="padding: 30px 10px 20px 0px;text-align: right">
              <a target="_blank" href="<?php echo site_url('secure/orders');?>" style="color:#0071bb;text-decoration: none; border-right: 1px solid #fff;padding-right: 5px;">Your Orders</a>
              <a target="_blank" href="<?php echo site_url('secure/my-account');?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Your Account</a>
              <a target="_blank" href="<?php echo site_url();?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Gojojo.in</a>
          </td>
      </tr>





      </tbody>
  </table>

  <table width="600" style="padding: 0 10px;" cellspacing="0" cellpadding="0" border="0">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td width="600" style="width:600px">
              <p style="text-align: left; border-bottom:2px solid #adadad; padding-bottom:15px;">
                Hi <?php echo '<b>'.$ship_firstname.' '.$ship_lastname.'</b>';?>, can you please take out a minute to give us your valuable feedback!
              </p>
          </td>
      </tr>
      <tr>
          <td width="600" align="left" style="width:600px">
            <span style="color:#1970ca;text-align:left;font-weight: bold;">You Purchansed:</span> <br>
            <!-- <?php
                //$subtotal = 0;
                //foreach($contents as $cartkey=>$product):
                ?> -->
              <p style="text-align: left; border-bottom:2px solid #adadad; padding-bottom:15px; vertical-align:top;">
                <!-- <?php //echo ucwords($product['name']);?>  --> <img src="http://35.167.215.182/gojojoEmailer/img/logo.png" style="vertical-align: top;margin-right: 13px;margin-bottom: 10px;" width="100" alt="">  Aloe Vera Facewash <br>
                <span style="color:#737373;">Delivered on: </span>
              </p>
              	<!-- <?php endforeach;?> -->
          </td>
      </tr>
      <tr>
          <td width="600" align="left" style="width:600px">
              <p style="text-align: left;padding-bottom:15px; font-weight:bold;font-size:16px;">
                Please Select a rating based on these questions.

              </p>
              <p style="margin-top:0;margin-bottom:5px;">Was the item delivered to you as described by the seller?</p>
              <p style="margin-top:0;margin-bottom:5px;">  Prompt an Courteous service? "if you contacted the seller"</p>
              <p style="margin-top:0;margin-bottom:5px;">Was the item delivered to you in time?</p>

              <p style="margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;">
                  Item as described
                  <span style=" margin-left:60px;" class="rate-5 rate"></span>
                </span> <br>
              </p>
              <p style="margin-top:0;margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;">
                  Communication
                  <span style=" margin-left:69px;" class="rate-3 rate"></span>
                </span> <br>
              </p>
              <p style="margin-top:0;margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;"> Shipping time
                  <span style=" margin-left:82px;" class="rate-4 rate"></span>
                 </span>
              </p>
              <p style="margin-top:0;margin-bottom:5px;">
                <span style="color:#1970ca;font-weight: bold;"> Shipping charges
                  <span style=" margin-left:60px;" class="rate-2 rate"></span>
                </span> <br> <br>
              </p>

              <p style="text-align: left;margin-bottom:3px; font-weight:bold;font-size:16px;">
                Does the seller meet your expectations?
              </p>
              <p style="margin-top:0;margin-bottom:5px; font-weight:bold;font-size:16px;margin-bottom:20px;">
                <a style="color:#1970ca;" href="<?php echo site_url().$product['slug'];?>?review=5">Click Here to Rate & Review the Seller</a>
              </p>
          </td>
      </tr>
      </tbody>
  </table>

    <table width="600" style="background-color: #fff;" cellspacing="0" cellpadding="0" border="0">
      <tbody>

      </tbody>
    </table>

  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;padding: 0 10px;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td align="left" width="600" style="">
              <h3 style="font-size: 18px;margin-bottom: 5px;">Thank You!</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">Team Gojojo</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">T: +91-8283004545</h3>

          </td>
      </tr>
      <tr>
          <td align="left" width="600">
              <p style="color: #666;">
                  Check out our <a href="<?php echo site_url('return-policy');?>">Easy Return Policy </a> | <a href="<?php echo site_url('terms-and-conditions');?>">Terms & Conditions</a>
              </p>
              <p style="color: #666;">
                  If you have any queries about your order please <a href="<?php echo site_url('contact-us');?>">Contact Us</a>
              </p>
          </td>
      </tr>
      <tr>
          <td width="600" style="text-align: center;padding: 20px 0;">
              <img src="http://35.167.215.182/gojojoEmailer/img/logo.png" width="150" alt="">
          </td>

      </tr>
      <tr>
          <td>
              <ul style="padding-left: 0;text-align: center;">
                  <li style="list-style: none;display: inline-block;padding:0 10px 5px 10px;">
                      <a href="https://www.facebook.com/Gojojodeals/"><img src="<?php echo theme_assets('gojojoEmailer/img/facebook.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">
                      <a href="https://twitter.com/Gojojodeals/"><img src="<?php echo theme_assets('gojojoEmailer/img/twitter.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">

                      <a href="https://www.instagram.com/gojojo.in/"><img src="<?php echo theme_assets('gojojoEmailer/img/instagram.png');?>" width="32" alt="alt"></a>
                  </li>

              </ul>
          </td>
      </tr>

      </tbody>
  </table>
  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td style="border-bottom: 5px solid red;"></td>
      </tr>
      </tbody>
  </table>

  </body>
</html>
