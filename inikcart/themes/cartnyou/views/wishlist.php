<?php echo theme_js('jquery.matchHeight-min.js', true);?>

<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Wishlist</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

            <div class="column col-xs-12 col-sm-12">
                <?php if ($this->session->flashdata('message')):?>
                    <div class="alert alert-info">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php endif;?>
                
                <?php if ($this->session->flashdata('error')):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif;?>
                
                <?php if(validation_errors()):?>
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert">×</a>
                        <?php echo validation_errors();?>
                    </div>
                <?php endif;?>
            </div>

            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module mt-40">
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li ><span></span><a href="<?php echo site_url('secure/my-account');?>">Profile</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/orders');?>">Orders</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/addresses');?>">My Address</a></li>
                                    <li class="active"><span></span><a href="<?php echo site_url('cart/wishlist');?>">My Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- Banner silebar -->

                <!-- ./Banner silebar -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2">My Wishlist (<?php echo count($products);?> Product<?php echo (count($products) > 1)?'s':'';?>)</span>
                </h2>
                <!-- Content page -->


                <ul class="row product-list grid">

                    <?php if(count($products) == 0):?>
                        <h2 style="margin:50px 0px; text-align:center;">
                            There are no items in your list.
                        </h2>
                    <?php elseif(count($products) > 0):
                        $used = array();
                        foreach($products as $product):
                            if(!in_array($product->id, $used)){
                                $used[] = $product->id;
                            } else {
                                continue;
                            }
                            ?>
                            <li class="col-sx-12 col-sm-4 match-item">
                                <div class="product-container">
                                    <div class="left-block">

                                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                                            <div class="group-price">
                                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                            </div>
                                        <?php } ?>
                                        
                                        <a href="<?php echo site_url($product->slug); ?>">
                                            <?php
                                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                            
                                            $product->images = array_values((array)json_decode($product->images));
                                    
                                            if(!empty($product->images[0]))
                                            {
                                                $primary    = $product->images[0];
                                                foreach($product->images as $photo)
                                                {
                                                    if(isset($photo->primary))
                                                    {
                                                        $primary    = $photo;
                                                    }
                                                }

                                                //$photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                                $no_image = (bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled');
                                                $photo  = '<img class="img img-responsive '.($no_image?"greyImage":"").'" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                            }
                                            echo $photo;
                                            ?>
                                        </a>

                                        <div class="quick-view">

                                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <?php if(time() > strtotime($product->live_on)){ ?>
                                                <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                                            <?php } else { ?>
                                                <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="right-block">
                                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>
                                        <div class="product-star">
                                            <a title="Remove from wishlist" class="btn btn-red btn-sm remove-from-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" >Remove</a>
                                        </div>
                                        <div class="content_price">
                                            <?php if($product->saleprice > 0):?>
                                                <span class="price product-price"><?php echo format_currency($product->saleprice); ?></span>
                                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                                            <?php else: ?>
                                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                                            <?php endif; ?>
                                            
                                        </div>
                                        <p class="fs-12 mt-10"> <span class="colorGreen">Offers:</span> <span class="spofr">Special Price & 1 More</span></p>

                                    </div>
                                </div>
                            </li>
                    <?php
                        endforeach;
                    endif;
                    ?>

                </ul>


                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<script>
    $(function(){
        $('.vertical-menu-content').css('display','none');
        $('.match-item').matchHeight();
    })
</script>