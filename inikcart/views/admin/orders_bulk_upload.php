
<div id="message"></div>

<div class="row">
	<div class="span12">

		<?php echo form_open_multipart($this->config->item('admin_folder').'/orders/import_orders', 'id="import_order" class="form-inline"');?>

			<h5>Orders CSV File <span style="color: #F00;">* (Only CSV File will work)</span></h5>
			<input type="file" name="file" placeholder="CSV File" /><br><br>
			<input type="submit" class="btn btn-success" name="submit" value="Submit" />
		</form>
		<a href="<?php echo site_url('uploads/sample_orders.csv');?>">Download Sample CSV</a>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("submit", '#import_order', function(e){ 
			e.preventDefault();
			show_animation();
			var ele = $('#import_order'); 
			var formData = new FormData(this);
			var url = ele.attr('action');
			$.ajax({
				data : formData,
				url : url,
				cache:false,
				contentType: false,
				processData: false,
				method: 'POST',
			}).done(function(getdata){
				hide_animation();
				$('#message').html(getdata);
			});
		});
	});
	
	function show_animation(){
		$('#saving_container').css('display', 'block');
		$('#saving').css('opacity', '.8');
	}

	function hide_animation(){
		$('#saving_container').fadeOut();
	}
</script>
<div id="saving_container" style="display:none;">
	<div id="saving" style="background-color:#000; position:fixed; width:100%; height:100%; top:0px; left:0px;z-index:100000"></div>
	<img id="saving_animation" src="<?php echo base_url('assets/img/storing_animation.gif');?>" alt="saving" style="z-index:100001; margin-left:-32px; margin-top:-32px; position:fixed; left:50%; top:50%"/>
	<div id="saving_text" style="text-align:center; width:100%; position:fixed; left:0px; top:50%; margin-top:40px; color:#fff; z-index:100001"><?php echo lang('saving');?></div>
</div>