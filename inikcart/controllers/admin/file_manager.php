<?php

class File_Manager extends Admin_Controller {	
	
	function __construct(){		
		parent::__construct();
        
		$this->auth->check_access('Admin', true);
	}

	function index(){
		$data['page_title']	= 'File Manager';
		
		$this->view($this->config->item('admin_folder').'/filemanager', $data);
	}
	
}
