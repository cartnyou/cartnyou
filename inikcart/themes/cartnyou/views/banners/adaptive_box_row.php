<div class="container">
	<div class="row adaptive-banner-container">
		<?php
		$bannersPerCount = count($banners);
		$bannersPerClass = "";
		if($bannersPerCount == 1){
			$bannersPerClass = "col-xs-12";
		} else if($bannersPerCount == 2){
			$bannersPerClass = "col-xs-6";
		} else if($bannersPerCount == 3){
			$bannersPerClass = "col-xs-4";
		} else if($bannersPerCount == 4){
			$bannersPerClass = "col-xs-3";
		}

		foreach($banners as $banner):?>
			<div class="<?php echo $bannersPerClass;?>">
				<?php
				
				$box_image	= '<img class="img-responsive lazy" data-src="'.base_url('uploads/'.$banner->image).'" src="'.theme_img('loading.gif').'" />';
				if($banner->link != '')
				{
					$target	= false;
					if($banner->new_window)
					{
						$target = 'target="_blank"';
					}
					echo '<a href="'.$banner->link.'" '.$target.' >'.$box_image.'</a>';
				}
				else
				{
					echo $box_image;
				}
				?>
			</div>
		<?php endforeach;?>
	</div>
</div>