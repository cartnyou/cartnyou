<?php
Class Category_model extends CI_Model
{
    var $allSubCategories    = array();

    function get_categories($parent = false)
    {
        if ($parent !== false)
        {
            $this->db->where('parent_id', $parent);
        }
        $this->db->select('id');
        $this->db->order_by('categories.sequence', 'ASC');
        
        //this will alphabetize them if there is no sequence
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get('categories');
        
        $categories = array();
        foreach($result->result() as $cat)
        {
            $categories[]   = $this->get_category($cat->id);
        }
        
        return $categories;
    }

    function get_featured_categories($parent=0, $limit=5)
    {
        $this->db->where('parent_id', $parent);
        
        //$this->db->select('id');
        $this->db->order_by('categories.sequence', 'ASC');
        $this->db->order_by('name', 'ASC');
        $this->db->where('featured',1);
        $result = $this->db->get('categories')->result_array();

        /*$categories = array();
        foreach($result->result() as $cat)
        {
            $categories[]   = $this->get_category($cat->id);
        }
        return $categories;
        */

        foreach($result as &$cat)
        {
            $cat['child']  = $this->get_featured_categories($cat['id'],10);
        }
        
        return $result;
    }
    
    function get_categories_tiered($admin = false)
    {
        if(!$admin) $this->db->where('enabled', 1);
        
        $this->db->order_by('sequence');
        $this->db->order_by('name', 'ASC');
        $categories = $this->db->get('categories')->result();
        
        $results    = array();
        foreach($categories as $category) {

            // Set a class to active, so we can highlight our current category
            if($this->uri->segment(1) == $category->slug) {
                $category->active = true;
            } else {
                $category->active = false;
            }

            $results[$category->parent_id][$category->id] = $category;
        }
        
        return $results;
    }

    function get_merchant_categories_tiered($merchant_id, $admin=false)
    {
        if(!$admin) $this->db->where('merchant_categories.enabled', 1);

        $this->db->where('merchant_id',$merchant_id);
        $this->db->join('categories','categories.id=merchant_categories.category_id');
        $this->db->order_by('sequence');
        $this->db->order_by('name', 'ASC');
        $categories = $this->db->get('merchant_categories')->result();
        
        $results    = array();
        foreach($categories as $category) {

            // Set a class to active, so we can highlight our current category
            if($this->uri->segment(1) == $category->slug) {
                $category->active = true;
            } else {
                $category->active = false;
            }

            $results[$category->parent_id][$category->id] = $category;
        }
        
        return $results;
    }

    function parent_special_products_finder($parent_id){
        if($parent_id != 0){
            $row = $this->db->get_where('categories', array('id'=>$parent_id))->row();
            $sp = $row->special_products;
            if($sp == ''){
                $this->parent_special_products_finder($row->parent_id);
            } else {
                //echo $sp.'<br>';
                return $sp;
            }
        } else {
            return false;
        }
    }
    
    function get_category($id, $spl_hack=false)
    {
        $row = $this->db->get_where('categories', array('id'=>$id))->row();

        if($spl_hack){
            if($row->special_products == ''){
                $special_products_return    = $this->parent_special_products_finder($row->parent_id);
                //echo $special_products_return.'-<br>';
                if($special_products_return){
                    $special_products       = json_decode($special_products_return);
                }
            } else {
                $special_products           = json_decode($row->special_products);
            }
        } else {
            $special_products               = json_decode($row->special_products);
        }
        //exit;
   
        if(!empty($special_products)){
            $relateds = array();
            $this->load->model('Product_model');

            foreach($special_products as $r)
            {
                $this->db->select('products.*, products_merchants.id AS pm_id, products_merchants.merchant_id,products_merchants.sku,products_merchants.slug,products_merchants.route_id,products_merchants.price,products_merchants.saleprice,products_merchants.free_shipping,products_merchants.quantity,products_merchants.enabled,products_merchants.live_on, brands.name as brand_name');
                $this->db->where('products.id',$r);
                $this->db->where('products.enabled',1);
                $this->db->where('products_merchants.enabled',1);
                $this->db->join('products_merchants', 'products_merchants.product_id = products.id');
                $this->db->join('brands', 'products.brand = brands.id');
                $this->db->order_by("(CASE WHEN products_merchants.saleprice > 0 THEN products_merchants.saleprice ELSE products_merchants.price END) ASC",FALSE);

                $p = $this->db->get('products')->row();
                if(!empty($p)){

                    $p->rating  = $this->Product_model->get_avg_rating($p->id,true);
                    $relateds[] = $p;
                }
            }
            $row->special_products = $relateds;
        } else {
            $row->special_products    = array();
        }
        return $row;
    }
    
    function get_category_products_admin($id)
    {
        $this->db->order_by('sequence', 'ASC');
        $result = $this->db->get_where('category_products', array('category_id'=>$id));
        $result = $result->result();
        
        $contents   = array();
        foreach ($result as $product)
        {
            $result2    = $this->db->get_where('products', array('id'=>$product->product_id));
            $result2    = $result2->row();
            
            $contents[] = $result2; 
        }
        
        return $contents;
    }
    
    function get_category_products($id, $limit, $offset)
    {
        $this->db->order_by('sequence', 'ASC');
        $result = $this->db->get_where('category_products', array('category_id'=>$id), $limit, $offset);
        $result = $result->result();
        
        $contents   = array();
        $count      = 1;
        foreach ($result as $product)
        {
            $result2    = $this->db->get_where('products', array('id'=>$product->product_id));
            $result2    = $result2->row();
            
            $contents[$count]   = $result2;
            $count++;
        }
        
        return $contents;
    }
    
    function organize_contents($id, $products)
    {
        //first clear out the contents of the category
        $this->db->where('category_id', $id);
        $this->db->delete('category_products');
        
        //now loop through the products we have and add them in
        $sequence = 0;
        foreach ($products as $product)
        {
            $this->db->insert('category_products', array('category_id'=>$id, 'product_id'=>$product, 'sequence'=>$sequence));
            $sequence++;
        }
    }
    
    function save($category)
    {
        if ($category['id'])
        {
            $this->db->where('id', $category['id']);
            $this->db->update('categories', $category);
            
            $this->load->model('Product_model');
            
            //update tax and commision in products of category
            $products = $this->Product_model->get_product_ids_category($category['id']);
            if(!empty($products)){
                $dataUpdate = array();

                $i = 0;
                foreach ($products as $product) {
                    $dataUpdate[$i]['id']           = $product->product_id;
                    $dataUpdate[$i]['tax_rate']     = $category['tax_rate'];
                    $dataUpdate[$i]['commission']    = $category['commission'];
                    $i++;
                }
                $this->db->update_batch('products', $dataUpdate, 'id');
            }
            
            return $category['id'];
        }
        else
        {
            $this->db->insert('categories', $category);
            return $this->db->insert_id();
        }
    }
    
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('categories');
        
        //delete references to this category in the product to category table
        $this->db->where('category_id', $id);
        $this->db->delete('category_products');
    }

    function get_slider_banner_id($id)
    {
        $this->db->order_by('id', 'ASC');
        $result = $this->db->get_where('category_sliders', array('category_id'=>$id));

        if($result->num_rows > 0)
            return $result->row()->banner_id;
        else
            return false;
    }

    function get_all_sub_categories($id)
    {
        return $this->get_sub_categories($id);
    }

    function get_sub_categories($id){
        $this->db->where('enabled', 1);
        $this->db->where('parent_id',$id);
        $categories = $this->db->get('categories')->result();
        
        if(!empty($categories)){
            foreach($categories as $category) {
                $this->allSubCategories[] = $category->id;
                $this->get_sub_categories($category->id);
            }
        }
        
        return $this->allSubCategories;
    }

    function get_merchant_categories($merchant_id, $all=false){
        $this->db->where('merchant_id', $merchant_id);
        if(!$all){
            $this->db->where('enabled', 1);
        }
        return $this->db->get('merchant_categories')->result();
    }

    function save_merchant_request($categories,$merchant_id){
        if($categories !== false)
        {
            $cats   = $this->get_merchant_categories($merchant_id);
            $ids    = array();
            foreach($cats as $c)
            {
                $ids[]  = $c->category_id;
            }

            //eliminate categories that products are no longer in
            foreach($ids as $c)
            {
                if(!in_array($c, $categories))
                {
                    $this->db->delete('merchant_categories', array('merchant_id'=>$merchant_id,'category_id'=>$c));
                }
            }
            
            //add products to new categories
            foreach($categories as $c)
            {
                if(!in_array($c, $ids))
                {
                    $this->db->where(array('merchant_id'=>$merchant_id,'category_id'=>$c));
                    $count_this = $this->db->count_all_results('merchant_categories');
                    if($count_this == 0){
                        $this->db->insert('merchant_categories', array('merchant_id'=>$merchant_id,'category_id'=>$c));
                    }
                }
            }
        }
        return true;
    }

    function get_category_commission($id){
        $this->db->select('commission');
        return $this->db->where('id', $id)->get('categories')->row()->commission;
    }

    function get_primary_categories_basic()
    {
        $this->db->select('id, name');
        $this->db->where('parent_id', 0);
        $this->db->order_by('categories.sequence', 'ASC');
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get('categories');        
        return $result->result();
    }

    function get_merchants_categories($count=false, $rows=false, $page=false){
        if($count){
            return $this->db->count_all_results('merchant_categories');
        } else {
            $this->db->select('merchants.id AS merchant_id, merchants.store_name, categories.id AS category_id, categories.name, merchant_categories.*');
            $this->db->join('merchants','merchants.id=merchant_categories.merchant_id');
            $this->db->join('categories','categories.id=merchant_categories.category_id');
            $this->db->order_by('merchant_categories.mc_id','desc');

            if($rows){
                $this->db->limit($rows);
            }
            if($page){
                $this->db->offset($page);
            }
            return $this->db->get('merchant_categories')->result();
        }
    }

    function assign_change($mc_id){
        $this->db->query('UPDATE merchant_categories SET enabled = IF(enabled=1, 0, 1) WHERE mc_id='.$mc_id);
        return $this->db->affected_rows();
    }

}