<?php

Class Transaction_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
	}

	function add_transaction($save){
		$insert	= array(
			'user_id'		=> $save['user_id'],
			'user_type'		=> $save['user_type'],
			'amount'		=> $save['amount'],
			'details'		=> $save['details'],
			'internal_id'	=> $save['internal_id'],
			'txn_id'		=> $save['txn_id'],
			'status'		=> $save['status'],
			'created_at'	=> $save['created_at'],
			'updated_at'	=> $save['updated_at']
		);
		$this->db->insert('transactions', $insert);
		return true;
	}
	
	
}