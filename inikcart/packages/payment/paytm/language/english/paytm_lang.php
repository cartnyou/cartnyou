<?php

$lang['paytm']				= 'Paytm';
$lang['paytm_error']				= 'There was an error processing your payment through Paytm';
$lang['paytm_desc']				= 'You will be directed to Paytm to complete your payment. Once your payment is authorized, you will be directed back to our website and your order will be complete.';


//paytm admin
$lang['merchant_key']				            = 'Paytm Merchant Key';
$lang['merchant_salt']				            = 'Paytm Salt Key Word';

$lang['currency_label']				    = 'INR, USD, EUR, etc.';