<?php

class Categories extends Admin_Controller { 
    
    function __construct()
    {       
        parent::__construct();
        
        $this->auth->check_access('Admin', true);
        $this->lang->load('category');
        $this->load->model('Category_model');
    }
    
    function index()
    {
        //we're going to use flash data and redirect() after form submissions to stop people from refreshing and duplicating submissions
        //$this->session->set_flashdata('message', 'this is our message');
        
        $data['page_title'] = lang('categories');
        $data['categories'] = $this->Category_model->get_categories_tiered(true);
        
        $this->view($this->config->item('admin_folder').'/categories', $data);
    }
    
    function organize($id = false)
    {
        $this->load->helper('form');
        $this->load->helper('formatting');
        
        if (!$id)
        {
            $this->session->set_flashdata('error', lang('error_must_select'));
            redirect($this->config->item('admin_folder').'/categories');
        }
        
        $data['category']       = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the category list with an error
        if (!$data['category'])
        {
            $this->session->set_flashdata('error', lang('error_not_found'));
            redirect($this->config->item('admin_folder').'/categories');
        }
            
        $data['page_title']     = sprintf(lang('organize_category'), $data['category']->name);
        
        $data['category_products']  = $this->Category_model->get_category_products_admin($id);
        
        $this->view($this->config->item('admin_folder').'/organize_category', $data);
    }
    
    function process_organization($id)
    {
        $products   = $this->input->post('product');
        $this->Category_model->organize_contents($id, $products);
    }
    
    function form($id = false)
    {
        
        $config['upload_path']      = 'uploads/images/full';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = $this->config->item('size_limit');
        $config['max_width']        = '1024';
        $config['max_height']       = '768';
        $config['encrypt_name']     = true;
        $this->load->library('upload', $config);
        
        
        $this->category_id  = $id;
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $data['categories']     = $this->Category_model->get_categories();

        $this->load->model('Banner_model');
        $data['banners']        = $this->Banner_model->banner_collections();
        $data['page_title']     = lang('category_form');
        
        //default values are empty if the customer is new
        $data['id']             = '';
        $data['name']           = '';
        $data['slug']           = '';
        $data['description']    = '';
        $data['excerpt']        = '';
        $data['sequence']       = '';
        $data['tax_rate']       = 0.0;
        $data['commission']     = 0.0;
        $data['image']          = '';
        $data['icon']           = '';
        $data['seo_title']      = '';
        $data['meta']           = '';
        $data['parent_id']      = 0;
        $data['slider_banner']  = 0;
        $data['side_banner']    = 0;
        $data['enabled']        = '';
        $data['featured']       = '';
        $data['error']          = '';
        $data['special_products']   = array();
        
        //create the photos array for later use
        $data['photos']     = array();
        
        if ($id)
        {   
            $category       = $this->Category_model->get_category($id);

            //if the category does not exist, redirect them to the category list with an error
            if (!$category)
            {
                $this->session->set_flashdata('error', lang('error_not_found'));
                redirect($this->config->item('admin_folder').'/categories');
            }
            
            //helps us with the slug generation
            $this->category_name    = $this->input->post('slug', $category->slug);
            
            //set values to db values
            $data['id']             = $category->id;
            $data['name']           = $category->name;
            $data['slug']           = $category->slug;
            $data['description']    = $category->description;
            $data['excerpt']        = $category->excerpt;
            $data['sequence']       = $category->sequence;
            $data['tax_rate']       = $category->tax_rate;
            $data['commission']     = $category->commission;
            $data['parent_id']      = $category->parent_id;
            $data['slider_banner']  = $category->slider_banner;
            $data['side_banner']  	= $category->side_banner;
            $data['image']          = $category->image;
            $data['icon']          	= $category->icon;
            $data['seo_title']      = $category->seo_title;
            $data['meta']           = $category->meta;
            $data['enabled']        = $category->enabled;
            $data['featured']       = $category->featured;
            $data['special_products']   = $category->special_products;
            
        }

        //if $data['special_products'] is not an array, make it one.
        if(!is_array($data['special_products']))
        {
            $data['special_products']   = array();
        }
        
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[64]');
        $this->form_validation->set_rules('slug', 'lang:slug', 'trim');
        $this->form_validation->set_rules('description', 'lang:description', 'trim');
        $this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
        $this->form_validation->set_rules('sequence', 'lang:sequence', 'trim|integer');
        $this->form_validation->set_rules('tax_rate', 'Tax Rate', 'trim|numeric|floatval');
        $this->form_validation->set_rules('commission', 'Commission', 'trim|numeric|floatval');
        $this->form_validation->set_rules('parent_id', 'parent_id', 'trim');
        $this->form_validation->set_rules('slider_banner', 'Slider Banner', 'trim');
        $this->form_validation->set_rules('side_banner', 'Side Banner', 'trim');
        $this->form_validation->set_rules('image', 'lang:image', 'trim');
        $this->form_validation->set_rules('icon', 'Icon', 'trim');
        $this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
        $this->form_validation->set_rules('meta', 'lang:meta', 'trim');
        $this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
        $this->form_validation->set_rules('featured', 'Featured', 'trim|numeric');
        
        
        // validate the form
        if ($this->form_validation->run() == FALSE)
        {
            $this->view($this->config->item('admin_folder').'/category_form', $data);
        }
        else
        {
            
            
            $uploaded   = $this->upload->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($data['image'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$data['image'];
                        $file[] = 'uploads/images/medium/'.$data['image'];
                        $file[] = 'uploads/images/small/'.$data['image'];
                        $file[] = 'uploads/images/thumbnails/'.$data['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                $data['error']  = $this->upload->display_errors();
                if($_FILES['image']['error'] != 4)
                {
                    $data['error']  .= $this->upload->display_errors();
                    $this->view($this->config->item('admin_folder').'/category_form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image          = $this->upload->data();
                $save['image']  = $image['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['image'];
                $config['new_image']    = 'uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['image'];
                $config['new_image']    = 'uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['image'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }

            //upload icon
            $uploaded_icon   = $this->upload->do_upload('icon');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded_icon)
                {
                    
                    if($data['icon'] != '')
                    {
                        $file = array();
                        $file[] = 'uploads/images/full/'.$data['icon'];
                        $file[] = 'uploads/images/medium/'.$data['icon'];
                        $file[] = 'uploads/images/small/'.$data['icon'];
                        $file[] = 'uploads/images/thumbnails/'.$data['icon'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded_icon)
            {
                $data['error']  = $this->upload->display_errors();
                if($_FILES['icon']['error'] != 4)
                {
                    $data['error']  .= $this->upload->display_errors();
                    $this->view($this->config->item('admin_folder').'/category_form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image_icon          = $this->upload->data();
                $save['icon']  = $image_icon['file_name'];
                
                $this->load->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/full/'.$save['icon'];
                $config['new_image']    = 'uploads/images/medium/'.$save['icon'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/medium/'.$save['icon'];
                $config['new_image']    = 'uploads/images/small/'.$save['icon'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                $this->image_lib->initialize($config); 
                $this->image_lib->resize();
                $this->image_lib->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/images/small/'.$save['icon'];
                $config['new_image']    = 'uploads/images/thumbnails/'.$save['icon'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                $this->image_lib->initialize($config);  
                $this->image_lib->resize(); 
                $this->image_lib->clear();
            }

            
            $this->load->helper('text');
            
            //first check the slug field
            $slug = $this->input->post('slug');
            
            //if it's empty assign the name field
            if(empty($slug) || $slug=='')
            {
                $slug = $this->input->post('name');
            }
            
            $slug   = url_title(convert_accented_characters($slug), 'dash', TRUE);
            
            //validate the slug
            $this->load->model('Routes_model');
            if($id)
            {
                $slug   = $this->Routes_model->validate_slug($slug, $category->route_id);
                $route_id   = $category->route_id;
            }
            else
            {
                $slug   = $this->Routes_model->validate_slug($slug);
                
                $route['slug']  = $slug;    
                $route_id   = $this->Routes_model->save($route);
            }
            
            $save['id']             = $id;
            $save['name']           = $this->input->post('name');
            $save['description']    = $this->input->post('description');
            $save['excerpt']        = $this->input->post('excerpt');
            $save['parent_id']      = intval($this->input->post('parent_id'));
            $save['slider_banner']  = intval($this->input->post('slider_banner'));
            $save['side_banner']  	= intval($this->input->post('side_banner'));
            $save['sequence']       = intval($this->input->post('sequence'));
            $save['tax_rate']       = floatval($this->input->post('tax_rate'));
            $save['commission']     = floatval($this->input->post('commission'));
            $save['seo_title']      = $this->input->post('seo_title');
            $save['meta']           = $this->input->post('meta');
            $save['enabled']        = $this->input->post('enabled');
            $save['featured']       = $this->input->post('featured');
            $save['route_id']       = intval($route_id);
            $save['slug']           = $slug;

            if($this->input->post('special_products'))
            {
                $save['special_products'] = json_encode($this->input->post('special_products'));
            }
            else
            {
                $save['special_products'] = '';
            }

            $category_id    = $this->Category_model->save($save);
            
            //save the route
            $route['id']    = $route_id;
            $route['slug']  = $slug;
            $route['route'] = 'cart/category/'.$category_id.'';
            
            $this->Routes_model->save($route);
            
            $this->session->set_flashdata('message', lang('message_category_saved'));
            
            //go back to the category list
            redirect($this->config->item('admin_folder').'/categories');
        }
    }

    function delete($id)
    {
        
        $category   = $this->Category_model->get_category($id);
        //if the category does not exist, redirect them to the customer list with an error
        if ($category)
        {
            $this->load->model('Routes_model');
            
            $this->Routes_model->delete($category->route_id);
            $this->Category_model->delete($id);
            
            $this->session->set_flashdata('message', lang('message_delete_category'));  
        }
        else
        {
            $this->session->set_flashdata('error', lang('error_not_found'));
        }
        redirect($this->config->item('admin_folder').'/categories');
    }

    function assign($page=0){
        $data['page_title'] = 'Assign Category';
        $rows = 20;
        $data['data'] = $this->Category_model->get_merchants_categories(false, $rows, $page);
        $data['total'] = $this->Category_model->get_merchants_categories(true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/categories/assign/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        $this->view($this->config->item('admin_folder').'/category_assign', $data);
    }

    function assign_change($mc_id){
        $affected_rows   = $this->Category_model->assign_change($mc_id);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/categories/assign');
    }
}