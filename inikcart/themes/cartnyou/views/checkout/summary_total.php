<h3 class="cartCardH3">Summary</h3>

<div class="cartCard">

    <?php include('summary_product.php');?>
    <table class="bag_total table table-striped">
		<?php
		/**************************************************************
		Subtotal Calculations
		**************************************************************/
		?>
		<?php if($this->go_cart->group_discount() > 0)  : ?> 
    	<tr class="clearfix">
			<td><?php echo lang('group_discount');?></td>
			<td>-<?php echo format_currency($this->go_cart->group_discount()); ?></td>
		</tr>
		<?php endif; ?>
		<tr class="clearfix">
	    	<td><?php echo lang('subtotal');?></td>
			<td id="gc_subtotal_price"><?php echo format_currency($this->go_cart->subtotal()); ?></td>
		</tr>
			
			
		<?php if($this->go_cart->coupon_discount() > 0) {?>
	    <tr class="clearfix">
	    	<td><?php echo lang('coupon_discount');?></td>
			<td id="gc_coupon_discount">-<?php echo format_currency($this->go_cart->coupon_discount());?></td>
		</tr>
		<?php if($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?> 
			<tr class="clearfix">
	    		<td><?php echo lang('discounted_subtotal');?></td>
				<td id="gc_coupon_discount"><?php echo format_currency($this->go_cart->discounted_subtotal());?></td>
			</tr>
			<?php
			}
		} 

		/**************************************************************
		 Custom charges
		**************************************************************/
		$charges = $this->go_cart->get_custom_charges();
		if(!empty($charges))
		{
			foreach($charges as $name=>$price) : ?>
				
		<tr class="clearfix">
			<td><?php echo $name?></td>
			<td><?php echo format_currency($price); ?></td>
		</tr>	
				
		<?php endforeach;
		}	
		
		/**************************************************************
		Order Taxes
		**************************************************************/
		 // Show shipping cost if added before taxes
		if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
		<tr class="clearfix">
			<td><?php echo lang('shipping');?></td>
			<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
		</tr>
		<?php endif;
		if($this->go_cart->order_tax() > 0) :  ?>
	    <tr class="clearfix">
	    	<td><?php echo lang('tax');?></td>
			<td><?php echo format_currency($this->go_cart->order_tax());?></td>
		</tr>
		<?php endif; 
		// Show shipping cost if added after taxes
		if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
		<tr class="clearfix">
			<td><?php echo lang('shipping');?></td>
			<td><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
		</tr>
		<?php endif ?>
		
		<?php
		/**************************************************************
		Gift Cards
		**************************************************************/
		if($this->go_cart->gift_card_discount() > 0) : ?>
		<tr class="clearfix">
			<td><?php echo lang('gift_card_discount');?></td>
			<td>-<?php echo format_currency($this->go_cart->gift_card_discount()); ?></td>
		</tr>
		<?php endif; ?>
		
		<?php
		/**************************************************************
		Grand Total
		**************************************************************/
		?>
		<tr class="total clearfix">
			<td><strong><?php echo lang('grand_total');?></strong></td>
			<td><strong><?php echo format_currency($this->go_cart->total()); ?></strong></td>
		</tr>

	</table>
    <div>
    	<?php
		$specialOfferEnabled = false;
		foreach ($this->go_cart->contents() as $item):
			if(isset($item['specialOfferApplied'])){
				if($item['specialOfferApplied']){
					$specialOfferEnabled 	= true;
					break;
				}
			}
		endforeach;
		
		if(!$specialOfferEnabled){
		?>
	        <?php echo form_open('cart/apply_coupon', array('id'=>'update_coupon_form','style'=>'display:block;'));?>

	        <input class="btn hidden" type="submit" value="<?php echo lang('form_update_cart');?>"/>
	        <input type="hidden" name="redirect" value="ajax"/>

	        <div class="coupon_form input-group pull-left col-md-12">
	            <input class="form-control" type="text" name="coupon_code" placeholder="Enter Coupon Code" />
	            <span class="input-group-addon cinput"><input value="Apply" class="btn btn-red" type="submit"></span>
	        </div>

	        <div class="alert alert-info" style="display: block;" >

	        </div>
	        </form>
	    <?php } ?>
	    
        <div class="cart_navigation fl-n mt-0">

            <div class="checkbox" style="margin-top: 10px;">
                <label style="padding-left: 0;"><input type="checkbox" value="true" id="checkbox-agree" checked="checked"> I agree to <a class="blue-link" target="_blank" href="<?php echo site_url('terms-and-conditions');?>">Terms and conditions</a></label>
            </div>

            <a class="next-btn btn-block text-center fl-n" id="place-order-btn" style="display: block;" href="javascript:;">Place Order</a>
        </div>
    </div>

	<?php //if($gift_cards_enabled):?>
		<!-- <div class="coupon_form input-group">
            <input class="form-control" type="text" name="gc_code" style="margin:0px;">
            <span class="input-group-addon"><input value="<?php echo lang('apply_gift_card');?>" class="btn btn-red" type="submit"></span>
        </div> -->
    <?php //endif;?>

</div>