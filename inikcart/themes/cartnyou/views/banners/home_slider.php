<div id="home-slider">
    <div class="container-fluid">
        <div class="row">
<!--            <div class="col-sm-3 col-md-3 slider-left"></div>-->
            <div class="col-sm-12 header-top-right pdlr-0">
                <div class="homeslider">
                    <div class="content-slide">
                        <ul id="contenhomeslider">
                        	<?php
                            if(!empty($banners)){
                                foreach($banners as $banner):
                                    echo '<li>';
                                        if($banner->link){
                                            $target=false;
                                            if($banner->new_window){
                                                $target=' target="_blank"';
                                            }
                                            echo '<a href="'.$banner->link.'"'.$target.'>';
                                        }

                                        $banner_name = '';
                                        if($banner->name):
                                            $banner_name = $banner->name;
                                        endif;
                                            echo '<img class="img-responsive" alt="'.$banner_name.'" src="'.base_url('uploads/'.$banner->image).'" title="'.$banner_name.'" />';
                                        if($banner->link){
                                            echo '</a>';
                                        }
                                    echo '</li>';
                                endforeach;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
<!--            <div class="col-sm-3 col-md-3 col-sm-2-5 slider-side-banner hidden-xs hidden-sm">-->
<!--                -->
<!--                --><?php //$this->banners->show_collection(12, 2, 'featured_near_slider');?>
<!--            </div>-->
        </div>
    </div>
</div>

<script>
    $(function(){
        setTimeout(function(){
            $('.bx-pager-link').html('');
        },5000)
    })
</script>