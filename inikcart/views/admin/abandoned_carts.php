<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Customer Type</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Last Updated At</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo ($row->id!=null)?'Customer':'Guest';?></td>
					<td><?php echo ucwords($row->firstname.' '.$row->lastname);?></td>
					<td><?php echo $row->email;?></td>
					<td><?php echo $row->phone;?></td>
					<td><?php echo $row->updated_at;?></td>
					<td>
						<div class="btn-group" style="float:right">
							<a class="btn" href="<?php echo site_url('admin/customers/abandoned_cart_detail/'.$row->customer_id);?>" target="_blank">Details</a>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="4">No data found.</td></tr>';
		}
		?>
	</tbody>
</table>