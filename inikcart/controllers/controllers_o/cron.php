<?php

class Cron extends Front_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('generalmodel');

        //if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
    }

    public function updateDelhiveryWaybillStatus(){
    	$this->load->model('order_model');

    	$orderItemsIds = $this->order_model->getPendingOrderItemsByWaybillMethod('Delhivery');

    	$processingIds = array();
    	$notProcessingIds = array();

    	if(!empty($orderItemsIds)){
    		foreach ($orderItemsIds as $orderItemsId) {
    			if(!in_array($orderItemsId->id, $notProcessingIds)){
	    			if($orderItemsId->ots_status == 'Delivered' || $orderItemsId->ots_status == 'Return Requested' || $orderItemsId->ots_status == 'Reverse Pickup generated' || $orderItemsId->ots_status == 'Returned' || $orderItemsId->ots_status == 'Cancelled'){
	    				$notProcessingIds[] = $orderItemsId->id;
	    			}
	    			if($orderItemsId->ots_status == 'Shipped'){
	    				$processingIds[$orderItemsId->id] = $orderItemsId->waybill;
	    			}
	    		}
    		}
    	}

    	if(!empty($processingIds)){
    		foreach ($processingIds as $otId => $waybill) {
    			$data = array();
				$data['waybill'] 	= $waybill;
				$data['verbose'] 	= 0;

    			$responseCharge = $this->generalmodel->hit_delhivery('get','packages',$data);
				$responseCharge = json_decode($responseCharge, true);

				if(isset($responseCharge['ShipmentData'][0]['Shipment']['Status']['Status'])){
					if($responseCharge['ShipmentData'][0]['Shipment']['Status']['Status'] == 'Delivered'){
						//insert status delivered
						$save					= array();
						$save['order_item_id']	= $otId;
						$save['ots_status']		= 'Delivered';
						$save['created_at']		= date('Y-m-d H:i:s', strtotime($responseCharge['ShipmentData'][0]['Shipment']['Status']['StatusDateTime']));;
						$save['notes']			= $responseCharge['ShipmentData'][0]['Shipment']['Status']['Instructions'];
						
						$this->Order_model->status_update($save);

					} else if($responseCharge['ShipmentData'][0]['Shipment']['Status']['Status'] == 'RTO'){
						//insert status returned
						$save					= array();
						$save['order_item_id']	= $otId;
						$save['ots_status']		= 'Returned';
						$save['created_at']		= date('Y-m-d H:i:s', strtotime($responseCharge['ShipmentData'][0]['Shipment']['Status']['StatusDateTime']));;
						$save['notes']			= $responseCharge['ShipmentData'][0]['Shipment']['Status']['Instructions'];
						
						$this->Order_model->status_update($save);
					}
				}
    		}
    	}
    }

    public function sendSellerReviewMail(){
    	$this->load->model('order_model');
    	$orderItemsIds = $this->order_model->getPastOrdersForReview();

    	$processingIds 			= array();
    	$processingOrderItems 	= array();

    	if(!empty($orderItemsIds)){
    		foreach ($orderItemsIds as $orderItemsId) {
    			if(!in_array($orderItemsId->id, $processingIds)){
	    			if($orderItemsId->ots_status == 'Delivered'){
	    				$processingIds[] 		= $orderItemsId->id;
	    				$processingOrderItems[] = $orderItemsId;
	    			}
	    		}
    		}
    	}

    	if(!empty($processingOrderItems)){
    		$this->load->library('email');
    		$this->load->model('Messages_model');
    		$this->load->model('Seller_model');
    		$this->load->model('Product_model');

    		$rowReview = $this->Messages_model->get_message(12);
    		$proReview = $this->Messages_model->get_message(11);

    		foreach ($processingOrderItems as $ot) {
    			echo $ot->id.'<br>';

    			// product review email
    			$ot->product_rating = $this->Product_model->get_product_split_rating($ot->merchant_id);
	            $proReview['content'] = $this->load->view('order_review', $ot, true);

	            $proReview['subject'] = str_replace('{customer_name}', $ot->ship_firstname.' '.$ot->ship_lastname, $proReview['subject']);
				$proReview['subject'] = str_replace('{url}', $this->config->item('base_url'), $proReview['subject']);
				$proReview['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $proReview['subject']);
				$proReview['subject'] = str_replace('{order_number}', $ot->order_number, $proReview['subject']);
				
				$this->load->library('email');
				
				$config = Array(
		        	'protocol'  => 'smtp',
		        	'smtp_host' => 'ssl://smtp.zoho.com',
		        	'smtp_port' => '465',
		        	'smtp_user' => 'auto-order-confirmation@gojojo.in',
		        	'smtp_pass' => 'Gojojo123#',
		        	'mailtype'  => 'html',
				    'newline'   => "\r\n"
		        );
				$this->email->initialize($config);
				$this->email->from('auto-order-confirmation@gojojo.in', $this->config->item('company_name'));
				$this->email->to($ot->ship_email);
				$this->email->subject($proReview['subject']);
				$this->email->message($proReview['content']);
				$this->email->send();

				// ./ product review email

				$ot->seller_rating = $this->Seller_model->get_seller_split_rating($ot->merchant_id);
				$rowReview['content'] = $this->load->view('seller_review', $ot, true);

    			$rowReview['subject'] = str_replace('{customer_name}', $ot->ship_firstname.' '.$ot->ship_lastname, $rowReview['subject']);
				$rowReview['subject'] = str_replace('{url}', $this->config->item('base_url'), $rowReview['subject']);
				$rowReview['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $rowReview['subject']);
				$rowReview['subject'] = str_replace('{order_number}', $ot->order_number, $rowReview['subject']);

    			$config = Array(
		        	'protocol'  => 'smtp',
		        	'smtp_host' => 'ssl://smtp.zoho.com',
		        	'smtp_port' => '465',
		        	'smtp_user' => 'auto-order-confirmation@gojojo.in',
		        	'smtp_pass' => 'Gojojo123#',
		        	'mailtype'  => 'html',
				    'newline'   => "\r\n"
		        );
				$this->email->initialize($config);
				$this->email->from('auto-order-confirmation@gojojo.in', $this->config->item('company_name'));
				$this->email->to($ot->ship_email);
				$this->email->subject($rowReview['subject']);
				$this->email->message($rowReview['content']);
				$this->email->send();
    		}
    	}
    }

}