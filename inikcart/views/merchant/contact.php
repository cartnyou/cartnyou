<form class="form-horizontal" method="post" action="<?php echo site_url('cart/contact_us_post/merchant')?>">
    <div class="row">
        <div class="form-group col-md-6">
          <label for="name">Name</label>  
          <input id="name" name="name" class="form-control input-md" requireds="" type="text">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
          <label for="phone">Phone No.</label>  
          <input id="phone" name="phone" class="form-control input-md" requireds="" type="text">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
          <label for="email">Email</label>  
          <input id="email" name="email" class="form-control input-md" requireds="" type="text">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
          <label for="query">Query</label>
          <textarea class="form-control" id="query" name="query" requireds=""></textarea>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
          <label for=""></label>
          <div class="col-md-8">
            <button class="btn btn-info" type="submit">Submit</button>
          </div>
        </div>
    </div>
</form>