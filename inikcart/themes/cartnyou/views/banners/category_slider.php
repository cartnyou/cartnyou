<div class="category-slider">
    <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
        <?php
		foreach($banners as $banner):?>
			<li>
				<?php
				if($banner->link){
                    $target=false;
                    if($banner->new_window){
                        $target=' target="_blank"';
                    }
                    echo '<a href="'.$banner->link.'"'.$target.'>';
                }
                ?>
				<img src="<?php echo base_url('uploads/'.$banner->image);?>" alt="<?php echo $banner->name;?>">
				<?php
				if($banner->link){
                    echo '</a>';
                }
                ?>
			</li>
		<?php
		endforeach;
		?>
    </ul>
</div>