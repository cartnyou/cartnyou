<div class="row">
	<div class="col-md-12 border-bottom-gray">
		<div class="row">
			<div class="col-sm-6">
				<h3>Get approval for an exsiting brand</h3>
				
				<?php echo form_open($this->config->item('merchant_folder').'/brands/search', array("class"=>"form-inline","id"=>"search-form"));?>
					<input type="text" class="col-md-2 form-control sm-70" name="name" placeholder="Search by brand name" />
					<button class="btn btn-success table-group-action-submit" name="submit" value="search"><i class="fa fa-search"></i> Find</button>
				</form>
			
				<div class="light-text">
					Choose from list of brands and get approval.
				</div>
			</div>
			<div class="col-sm-1 or-section">
				<span>or</span>
			</div>
			<div class="col-sm-5">
				<h3>Sell a New Brand</h3>
				<a class="btn btn-sm btn-success" href="<?php echo site_url($this->config->item('merchant_folder').'/brands/form');?>"><i class="fa fa-plus-circle"></i> Add New Brand</a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 hide" id="search-result-div">
		<h3>Search Results</h3>
		<div id="search-result">
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$(document).on("submit", '#search-form', function(e){
		e.preventDefault();
		$('#search-result').html('');
		$('#search-result-div').removeClass('hide');
		var ele = $('#search-form');
		var data = ele.serializeArray(); 
		var url = ele.attr('action');
		data.push({name: 'limit', value: 10});
		$.ajax({
			data : data,
			url : url,
			method: 'POST'
		}).done(function(getdata){
			getdata = JSON.parse(getdata);
			var html = '';
			if(getdata.length != 0){
				$.each(getdata, function(key, row) {
					html += '<div class="col-md-12 border-bottom-gray"><div class="col-md-8"><h4 class="theme-text">'+row.name+'</h4></div><div class="col-md-4"><a class="btn btn-sm btn-success action-btn" href="javascript:void(0);" data-id="'+row.id+'">Start Selling This Brand</a></div></div>';
			 	});
			} else {
				html = 'No brand found. Please try another name.';
			}
			$('#search-result').html(html);
		});
  	});

  	$(document).on('click','.action-btn', function(e){
  		e.preventDefault();
  		var data = [];
  		var url = "<?php echo site_url($this->config->item('merchant_folder').'/brands/request');?>";
  		data.push({name: 'brand_id', value: $(this).data('id')});
  		$.ajax({
			data : data,
			url : url,
			method: 'POST'
		}).done(function(getdata){
			getdata = JSON.parse(getdata);
			if(getdata.success){
				toastr.success("Request sent.", "Success");
			} else {
				toastr.error(getdata.data, "Oops...");
			}
		});
  	});
});
</script>