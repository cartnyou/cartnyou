<section class="padbot0">
	<?php foreach($banners as $banner):?>
		
		<div class="parallax-div" style="background-image: url('<?php echo base_url('uploads/'.$banner->image);?>');">
			<div class="parallax-content">
				<div class="parallax-title">
					<?php if($banner->name != ''){ ?>
						
							<?php echo $banner->name;?>
						
					<?php } ?>

					<?php
					if($banner->link != ''){
						$target	= false;
						if($banner->new_window)
						{
							$target = 'target="_blank"';
						}
						echo '<br><a class="parallax-button" href="'.$banner->link.'" '.$target.' >Explore</a>';
					}
					?>
				</div>
			</div>
		</div>
	<?php endforeach;?>
</section>