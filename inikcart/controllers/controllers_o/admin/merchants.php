<?php

class Merchants extends Admin_Controller { 
    
    function __construct(){
        parent::__construct();
        
        $this->auth->check_access('Admin', true);
        $this->load->model('Merchant_model');
    }
    
    function index($page=0){
        $data['page_title'] = 'Merchants';
        $rows = 10;
        $data['data'] = $this->Merchant_model->get_merchants(false, $rows, $page);
        $data['total'] = $this->Merchant_model->get_merchants(true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/merchants/index/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        
        $this->view($this->config->item('admin_folder').'/merchants_list', $data);
    }

    function requests($page=0){
        $data['page_title'] = 'Merchant Requests';
        $rows = 10;
        $data['data'] = $this->Merchant_model->get_merchants(false, $rows, $page, 9);
        $data['total'] = $this->Merchant_model->get_merchants(true, false, false, 9);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/merchants/requests/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        
        $this->view($this->config->item('admin_folder').'/merchants_requests_list', $data);
    }
    
    function ratings($page=0){
        $data['page_title'] = 'Merchant Ratings';
        $rows = 10;
        $data['data'] = $this->Merchant_model->get_merchants_ratings(false, $rows, $page);
        $data['total'] = $this->Merchant_model->get_merchants_ratings(true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/merchants/ratings/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        $this->view($this->config->item('admin_folder').'/merchant_ratings', $data);
    }

    function rating_change($sr_id){
        $affected_rows   = $this->Merchant_model->rating_change($sr_id);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/merchants/ratings');
    }

    function merchant_status($id, $status=false){
        $affected_rows   = $this->Merchant_model->merchant_status($id, $status);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/merchants');
    }

    function detail($id=false){
        if(!$id){
            redirect(site_url('admin'));
        }

        $data['row'] = $this->Merchant_model->get_merchant($id);

        $data['page_title'] = 'Merchant: '.$data['row']->store_name;
        $this->view($this->config->item('admin_folder').'/merchant_detail', $data);
    }
}