<!DOCTYPE html>
<html>
  <head>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">
  <table style="background: #f9f9f9;padding: 0 10px;" width="600" cellspacing="0" cellpadding="0" border="0">
      <tbody>
      <tr style="background: #f9f9f9;">
          <td width="150" style="padding: 30px 10px 20px 30px;text-align: left;border-bottom: 2px solid #ddd;">
              <a target="_blank" href="<?php echo site_url('secure/orders');?>" style="color:#0071bb;text-decoration: none; border-right: 1px solid #fff;padding-right: 5px;">Your Orders</a>
          </td>
          <td width="300" style="padding: 30px 0px 8px 10px;width: 130px;border-bottom: 2px solid #ddd;"><a href="<?php echo site_url();?>"><img style="width: 150px;max-width: 200px;margin: 0 auto;" border="0" src="<?php echo theme_img('cartnyouEmailer/img/logo.png');?>" alt="" width="150" /></a></td>
          <td width="150" style="padding: 30px 30px 20px 0px;text-align: right;border-bottom: 2px solid #ddd;">
              <a target="_blank" href="<?php echo site_url('secure/my-account');?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Your Account</a>

          </td>
      </tr>





      </tbody>
  </table>

  <table width="600" style="padding: 0 10px;background: #f9f9f9" cellspacing="0" cellpadding="0" border="0">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td width="600" style="width:600px">
              <h3 style="text-align: center;color: #FDA007;font-size: 27px;">Order Delivered</h3>
              
          </td>
      </tr>
      </tbody>
  </table>

    <table width="600" style="background-color: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="50">

          </td>
          <td width="500" style="background-color: #f9f9f9;padding: 15px;">
            <table>
              <tbody>
                <tr>
                  <td width="500" style="border-bottom: 1px solid #828080;color: #102327;padding: 15px 0px;">
                    <table width="500">
                      <tr>
                      </tr>
                      <tr>
                        <td align="left" style="font-family: Arial;">Order #: <?php echo $order_number ?></td>
                        <td align="right" style="text-align: right;font-family: Arial;">Order date: <?php echo $ordered_on;?></td>
                      </tr>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td width="500" style="border-bottom: 1px solid #828080;color: #102327;padding: 15px 0px;">
                    <table width="500">
                    	<thead>
	                      <tr>
	                        <td align="left" style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px"  width="180">ITEM</td>
	                        <td align="left" style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px"  width="80">DESCRIPTION</td>
	                        <td align="left" style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px"  width="80">PRICE</td>
	                        <td align="left" style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px"  width="80">QTY</td>
	                        <td align="left" style="font-family: Arial;color:#828080;font-size:12px;padding-bottom:15px" width="80">TOTAL</td>
	                      </tr>
                      	</thead>

                      	<tfoot>
                        <tr>
                            <td align="left" colspan="4" style="border-top: 1px solid #828080;"><?php echo lang('subtotal');?></td>
                            <td align="left" style="border-top: 1px solid #828080;""><?php echo format_currency_rs($subtotal); ?></td>
                        </tr>
                        <tr>
            							<td align="left" colspan="4"><?php echo lang('shipping');?></td>
            							<td align="left"><?php echo format_currency_rs($shipping) ?></td>
            						<tr>

                        <?php if($coupon_discount > 0)  : ?>
                            <tr>
                                <td align="left" colspan="4"><?php echo lang('coupon_discount');?></td>
                                <td align="left"><?php echo format_currency_rs(0-$coupon_discount); ?></td>
                            </tr>
                            <?php
            						endif;
            						?>


			           	<?php if($gift_card_discount != 0) : ?>
				         	<tr>
								<td align="left" colspan="4"><?php echo lang('gift_card');?></td>

								<td align="left"><?php echo format_currency_rs($gift_card_discount); ?></td>
							</tr>
			          	<?php endif;   ?>
			            <tr>
							<td align="left" colspan="4">
								<strong><?php echo lang('grand_total');?></strong>
							</td>
							<td align="left">
								<strong><?php echo format_currency_rs($total); ?></strong>
							</td>
						</tr>
                        </tfoot>

                      	<tbody>
                      	<?php
						$subtotal = 0;
						foreach($contents as $cartkey=>$product):
						?>
                        	<tr>
                                <td align="left" style="padding:7px 0;vertical-align: top;"  width="180"><?php echo ucwords($product['name']);?></td>
                                <td align="left" style="padding:7px 0;vertical-align: top;"  width="80"><?php echo $product['excerpt'];
                                    if(isset($product['options'])) {
                                        foreach ($product['options'] as $name=>$value)
                                        {
                                            if(is_array($value))
                                            {
                                                echo '<div>'.$name.':<br/>';
                                                foreach($value as $item)
                                                    echo '- '.$item.'<br/>';
                                                echo '</div>';
                                            }
                                            else
                                            {
                                                echo '<div>'.$name.': '.$value.'</div>';
                                            }
                                        }
                                    }
                                    echo "<b>Seller:</b> <br>".$product['store_name'];
                                    ?>
                                </td>
                                <td align="left" style="padding:7px 0;vertical-align: top;" width="80"><?php echo format_currency_rs($product['price']);?></td>
                                <td align="left" style="padding:7px 0;vertical-align: top;" width="80"><?php echo $product['quantity'];?></td>
                                <td align="left" style="padding:7px 0;vertical-align: top;" width="80"><?php echo format_currency_rs($product['price']*$product['quantity']); ?></td>
                            </tr>
                      	<?php endforeach;?>
                      	</tbody>
                    </table>
                  </td>
                </tr>

                <tr>
                  <td align="left" width="500" style="font-family: Arial;font-size: 20px;color:#FDA007;padding:15px 0px 5px;">
                      Shipping Address
                  </td>
                </tr>
                <tr>
                  <td align="left" width="500" style="font-family: Arial;color:#102327;">
                  	<?php
				  	echo '<b>'.$ship_firstname.' '.$ship_lastname.'</b><br>';
				  	echo $ship_email.'<br>';
				  	echo $ship_phone.'<br>';
				  	echo $ship_address1.'<br>';
				  	if(!empty($ship_address2)) echo $ship_address2.'<br>';
				  	echo $ship_city.', '.$ship_zone.' '.$ship_zip;
				  	?>
                  </td>
                </tr>

                <tr>
                  <td align="left" width="500" style="font-family: Arial;font-size: 20px;color:#FDA007;padding:15px 0px 5px;">
                      Payment Method
                  </td>
                </tr>
                <tr>
                  <td align="left" width="500" style="font-family: Arial;color:#102327;">
                  	<?php echo $payment_info; ?>
                  </td>
                </tr>

              </tbody>
            </table>

          </td>
          <td width="50">

          </td>
        </tr>
      </tbody>
    </table>

  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;padding: 0 10px;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td align="left" width="600" style="">
              <h3 style="font-size: 18px;margin-bottom: 5px;">Thank You!</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">Team CartNYou</h3>
              <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">T: +91-8283004545</h3>

          </td>
      </tr>
      <tr>
          <td align="left" width="600">
              <p style="color: #666;">
                  Check out our <a href="<?php echo site_url('return-policy');?>">Easy Return Policy </a> | <a href="<?php echo site_url('terms-and-conditions');?>">Terms & Conditions</a>
              </p>
              <p style="color: #666;">
                  If you have any queries about your order please <a href="<?php echo site_url('contact-us');?>">Contact Us</a>
              </p>
          </td>
      </tr>
      <tr>
          <td width="600" style="text-align: center;padding: 20px 0;">
              <img src="http://35.167.215.182/cartnyouEmailer/img/logo.png" width="150" alt="">
          </td>

      </tr>
      <tr>
          <td>
              <ul style="padding-left: 0;text-align: center;">
                  <li style="list-style: none;display: inline-block;padding:0 10px 5px 10px;">
                      <a href="https://www.facebook.com/CartnYou-277729052962135/"><img src="<?php echo theme_assets('cartnyouEmailer/img/facebook.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">
                      <a href="https://twitter.com/CartnYou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/twitter.png');?>" width="32" alt=""></a>
                  </li>
                  <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">

                      <a href="https://www.instagram.com/cartnyou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/instagram.png');?>" width="32" alt="alt"></a>
                  </li>

              </ul>
          </td>
      </tr>

      </tbody>
  </table>
  <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;">
      <tbody width="600" cellspacing="0" cellpadding="0" border="0">
      <tr>
          <td style="border-bottom: 5px solid #445268;"></td>
      </tr>
      </tbody>
  </table>

  </body>
</html>
