<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-briefcase fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo format_currency($lifetime_sales);?> </div>
                <div class="desc"> Lifetime Sales </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo $total_orders;?> </div>
                <div class="desc"> Total Orders </div>
            </div>
            <a class="more" href="<?php echo site_url('merchant/orders');?>"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo ($total_orders != 0) ? format_currency($lifetime_sales/$total_orders) : format_currency(0) ;?> </div>
                <div class="desc"> Average Order Value </div>
            </div>
            <a class="more" href="<?php echo site_url('merchant/orders');?>"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow">
            <div class="visual">
                <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo $pending_orders_count;?> </div>
                <div class="desc"> Pending Orders </div>
            </div>
            <a class="more" href="<?php echo site_url('merchant/orders/pending');?>"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-list fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo format_currency($total_products_value);?> </div>
                <div class="desc"> Total Products </div>
            </div>
            <a class="more" href="<?php echo site_url('merchant/products');?>"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-inr fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo format_currency(0);?> </div>
                <div class="desc"> Refund </div>
            </div>
            <a class="more" href="<?php echo site_url('merchant/orders');?>"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <!-- Begin: life time stats -->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Overview</span>
                    <span class="caption-helper">report overview...</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#overview_1" data-toggle="tab"> Top Selling </a>
                        </li>
                        <li>
                            <a href="#overview_2" data-toggle="tab"> Most Viewed </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> Orders
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="#overview_4" data-toggle="tab">
                                        <i class="icon-bell"></i> Latest 10 Orders </a>
                                </li>
                                <li>
                                    <a href="#overview_5" data-toggle="tab">
                                        <i class="icon-info"></i> Pending 10 Orders </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="overview_1">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> Price </th>
                                            <th> Sold </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($popular_products as $product) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo site_url().$product->slug;?>" target="_blank"><?php echo format_product_name($product->name);?></a>
                                                </td>
                                                <td>
                                                    <?php
                                                    if($product->saleprice > 0):
                                                        echo format_currency($product->saleprice);
                                                    else:
                                                        echo format_currency($product->price);
                                                    endif;?>
                                                </td>
                                                <td><?php echo $product->ordered_quantity;?></td>
                                                <td>
                                                    <a href="<?php echo site_url().$product->slug;?>" target="_blank" class="btn btn-sm btn-default">
                                                        <i class="fa fa-search"></i> View </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="overview_2">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Product Name </th>
                                            <th> Price </th>
                                            <th> Views </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($most_viewed as $product) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo site_url().$product->slug;?>" target="_blank"><?php echo format_product_name($product->name);?></a>
                                                </td>
                                                <td>
                                                    <?php
                                                    if($product->saleprice > 0):
                                                        echo format_currency($product->saleprice);
                                                    else:
                                                        echo format_currency($product->price);
                                                    endif;?>
                                                </td>
                                                <td><?php echo $product->views;?></td>
                                                <td>
                                                    <a href="<?php echo site_url().$product->slug;?>" target="_blank" class="btn btn-sm btn-default">
                                                        <i class="fa fa-search"></i> View </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="overview_4">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Order ID </th>
                                            <th> Customer Name </th>
                                            <th> Date </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($recent_orders as $order) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo site_url($this->config->item('merchant_folder').'/orders/order/'.$order->id);?>" target="_blank"><?php echo $order->order_number;?></a>
                                                </td>
                                                <td><?php echo ucwords($order->ship_firstname.' '.$order->ship_lastname); ?></td>
                                                <td><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
                                                <td>
                                                    <a href="<?php echo site_url($this->config->item('merchant_folder').'/orders/order/'.$order->id);?>" target="_blank" class="btn btn-sm btn-default">
                                                        <i class="fa fa-search"></i> View </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="overview_5">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th> Order ID </th>
                                            <th> Customer Name </th>
                                            <th> Date </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pending_orders as $order) { ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo site_url($this->config->item('merchant_folder').'/orders/order/'.$order->id);?>" target="_blank"><?php echo $order->order_number;?></a>
                                                </td>
                                                <td><?php echo ucwords($order->ship_firstname.' '.$order->ship_lastname); ?></td>
                                                <td><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
                                                <td>
                                                    <a href="<?php echo site_url($this->config->item('merchant_folder').'/orders/order/'.$order->id);?>" target="_blank" class="btn btn-sm btn-default">
                                                        <i class="fa fa-search"></i> View </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    <div class="col-md-6">
        <!-- Begin: life time stats -->
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-red"></i>
                    <span class="caption-subject font-red bold uppercase">Revenue</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#portlet_ecommerce_tab_1" data-toggle="tab" class="ographhack"> Amounts </a>
                    </li>
                    <li>
                        <a href="#portlet_ecommerce_tab_2" id="statistics_orders_tab" data-toggle="tab" class="ographhack"> Orders </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div>
                    <select id="graph-year-select" class="form-control pull-right" style="width: 100px;">
                        <?php
                        echo '<option>'.date('Y').'</option>';
                        echo '<option>'.date('Y',strtotime("-1 year")).'</option>';
                        echo '<option>'.date('Y',strtotime("-2 year")).'</option>';
                        ?>
                    </select>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="portlet_ecommerce_tab_1">
                        <div id="statistics_1" class="chart"> </div>
                    </div>
                    <div class="tab-pane" id="portlet_ecommerce_tab_2">
                        <div id="statistics_2" class="chart"> </div>
                    </div>
                </div>
                <!-- <div class="well margin-top-20">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-success"> Revenue: </span>
                            <h3>$1,234,112.20</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-info"> Tax: </span>
                            <h3>$134,90.10</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-danger"> Shipment: </span>
                            <h3>$1,134,90.10</h3>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                            <span class="label label-warning"> Orders: </span>
                            <h3>235090</h3>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- End: life time stats -->

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-red"></i>
                    <span class="caption-subject font-red bold uppercase">Orders Stats</span>
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <select id="pie-year-select" class="form-control pull-right" style="width: 100px;">
                        <?php
                        echo '<option>'.date('Y').'</option>';
                        echo '<option>'.date('Y',strtotime("-1 year")).'</option>';
                        echo '<option>'.date('Y',strtotime("-2 year")).'</option>';
                        ?>
                    </select>
                </div>
                <div id="piechart_1" class="chart"> </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    
    // $("<div id='tooltip'></div>").css({
    //         position: "absolute",
    //         display: "none",
    //         border: "1px solid #fdd",
    //         padding: "2px",
    //         "background-color": "#fee",
    //         opacity: 0.80
    //     }).appendTo("body");

    $('#graph-year-select').on('change', function(){
        get_graph_data($(this).val());
    });

    function get_graph_data(graph_year){
        $.ajax({
            url: '<?php echo site_url();?>merchant/dashboard/get_gross_monthly_sales/'+graph_year,
            type: "GET",
            dataType: "json",
            success: function(data){
                var data_amount = [
                    { label: "Sales (INR)", data: data }
                ];
                $.plot("#statistics_1", data_amount , {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 0.3
                        },
                        points: {
                            show: true
                        }
                    },
                    xaxis: {
                        mode: "categories",
                        min: 0
                    },
                    yaxis: {
                        min: 0
                    },
                    grid: {
                        borderWidth: 1
                    },
                    colors: ["#52CC8E", "#ED1C24"]
                });
            }
        });

        $.ajax({
            url: '<?php echo site_url();?>merchant/dashboard/get_gross_monthly_orders/'+graph_year,
            type: "GET",
            dataType: "json",
            success: function(data){
                var data_amount = [
                    { label: "Orders", data: data }
                ];
                $.plot("#statistics_2", data_amount , {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 0.3
                        },
                        points: {
                            show: true
                        }
                    },
                    xaxis: {
                        mode: "categories",
                        min: 0
                    },
                    yaxis: {
                        min: 0
                    },
                    grid: {
                        borderWidth: 1
                    },
                    colors: ["#52CC8E", "#ED1C24"]
                });
            }
        });
    }

    $('.ographhack').on('shown.bs.tab', function (e) {
        get_graph_data($('#graph-year-select').val());
    });

    //init
    get_graph_data('<?php echo date("Y");?>');
    
    // $("#statistics_1").bind("plothover", function (event, pos, item) {
    //     console.log(item);
    //     if (item) {
    //         var x = item.datapoint[0].toFixed(2),
    //             y = item.datapoint[1].toFixed(2);

    //         $("#tooltip").html(item.series.label + " of " + x + " = " + y)
    //             .css({top: item.pageY+5, left: item.pageX+5})
    //             .fadeIn(200);
    //     } else {
    //         $("#tooltip").hide();
    //     }
    // });

	function get_pie_data(graph_year){
        $.ajax({
            url: '<?php echo site_url();?>merchant/dashboard/get_order_stats/'+graph_year,
            type: "GET",
            dataType: "json",
            success: function(data){
            	if(data.length > 0){
				    $.plot('#piechart_1', data, {
						series: {
							pie: { 
								show: true,
								radius: 1,
								label: {
									show: true,
									radius: 3/4,
									formatter: function(label, series) {
										return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
									},
									background: {
										opacity: 0.5
									}
								}
							}
						},
						legend: {
							show: true
						}
					});
				} else {
					$('#piechart_1').html('<div class="alert alert-info">No data found.</div>');
				}
            }
        });
    }
    get_pie_data('<?php echo date("Y");?>');

    $('#pie-year-select').on('change', function(){
        get_pie_data($(this).val());
    });
});
</script>