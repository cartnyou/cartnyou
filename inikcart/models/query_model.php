<?php
class Query_model extends CI_Model{
	
    public function __construct(){
        parent::__construct();
    }

    public function add_query($save) {
        $this->db->insert('queries',$save);
        $id = $this->db->insert_id();

        $data  = array('ticket_number'=> date('U').$id);
        $this->db->where('q_id', $id);
        $this->db->update('queries', $data);
                        
        return $order_number = $data['ticket_number'];
    }

}