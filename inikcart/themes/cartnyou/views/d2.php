<div class="container">
    <form method="post" action="cart/seller_registration_do">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-group">
                        <label>First Name <span class="red">*</span></label>
                        <input type="text" class="form-control" name="fname">
                    </div>
                    <div class="form-group">
                        <label>Company Name <span class="red">*</span></label>
                        <input type="text" class="form-control" name="company_name">
                    </div>
                    <div class="form-group">
                        <label>Mobile Number <span class="red">*</span></label>
                        <input type="text" class="form-control" name="phone">
                    </div>
                    <div class="form-group">
                        <label>Which Service You are Interested in <span class="red">*</span></label>
                        <select class="form-control" name="service">
                            <option value="Sell at Gojojo">Sell at Gojojo</option>
                            <option value="Sell at Gojojo/ Shipping Panel for COD">Sell at Gojojo/ Shipping Panel for COD shippments</option>
                        </select>
                    </div>
                    <label>Your Business is about</label>
                    <select class="form-control" name="business">
                        <option value="">--select--</option>
                        <option value="Mobile Accessories">Mobile Accessories</option>
                        <option value="Tablet Accessories">Tablet Accessories</option>
                        <option value="Gadgets">Gadgets</option>
                        <option value="Electronics">Electronics</option>
                        <option value="Camera Accessories">Camera Accessories</option>
                        <option value="Video Games and accessories">Video Games and accessories</option>
                        <option value="Security Systems">Security Systems</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Approximate Annual Revenue</label>
                    <input type="text" class="form-control" name="revenue">
                </div>
                <div class="form-group">
                    <label>Website Url</label>
                    <input type="text" class="form-control" name="website">
                </div>
                <div class="form-group">
                    <label>Your Primary Clients</label>
                    <input type="text" class="form-control" name="clients">
                </div>
                <div class="form-group">
                    <label>
                        Have you collaborated with anyone else? Eg. Ebay, Amazon, Flipkart, Jabong,

                        Shopclues?
                    </label>
                    <label><input type="radio" name="other_ecommerce" value="Yes"> Yes</label>
                    <label><input type="radio" name="other_ecommerce" value="No"> No</label>
                </div>

                <div class="form-group">
                    <label>If Yes, then please provide the Web Link</label>
                    <input type="text" class="form-control" name="web_link">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Last Name <span class="red">*</span></label>
                    <input type="text" class="form-control" name="lname">
                </div>
                <div class="form-group">
                    <label>Email Address <span class="red">*</span></label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label>Landline Number</label>
                    <input type="text" class="form-control" name="landline">
                </div>
                <div class="form-group">
                    <label>Primary Category</label>
                    <select class="form-control" name="category" id="category-select-dropdown">
                        <option value="">--select--</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Number of Products in Stock</label>
                    <input type="text" class="form-control" name="stock">
                </div>
                <div class="form-group">
                    <label>Monthly Vistors to your site</label>
                    <input type="text" class="form-control" name="visitors">
                </div>
                <div class="form-group">
                    <label>Number of Stores</label>
                    <input type="text" class="form-control" name="stores">
                </div>
                <div class="form-group">
                    <label>GST Registration Number <span class="red">*</span></label>
                    <input type="text" class="form-control" name="cst">
                </div>
                <div class="form-group">
                    <label>
                        Any Other Information?
                    </label>
                    <textarea cols="30" rows="4" class="form-control" name="other_info"></textarea>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <button class="btn btn-red width-100">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(function(){

        $.ajax({
            url : '{{SITEURL}}'+'cart/get_primary_categories_basic',
            method: 'GET'
        }).done(function(getdata){
            getdata = JSON.parse(getdata);
            var selectDropdown = document.getElementById("category-select-dropdown");
            if (getdata.success) {
                $.each(getdata.data, function(key, val){
                    var option = document.createElement("option");
                    option.text = val.name;
                    option.value = val.name;
                    selectDropdown.appendChild(option);
                });
            }
        });
    });
</script>