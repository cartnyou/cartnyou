<?php

class Secure extends Front_Controller {
	
	var $customer;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('location_model'));
		$this->customer = $this->go_cart->customer();
	}
	
	function index()
	{
		show_404();
	}
	
	function login($ajax = false)
	{
		//find out if they're already logged in, if they are redirect them to the my account page
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default, if they are not logging in
		/*if ($redirect)
		{
			redirect('secure/my_account/');
		}*/
		
		$data['page_title']	= 'Login';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');
		$submitted 		= $this->input->post('submitted');
		if ($submitted)
		{
			$email		= $this->input->post('email');
			$password	= $this->input->post('password');
			$remember   = $this->input->post('remember');
			$redirect	= $this->input->post('redirect');

			//check phone number verified
			$customerData = $this->Customer_model->get_customer_by_email_password($email,$password);
			if($customerData){
				if($customerData['confirmed'] == 1){
					$login		= $this->Customer_model->login($email, $password, $remember);
					if ($login)
					{
						if ($redirect == '')
						{
							//if there is not a redirect link, send them to the my account page
							$redirect = 'secure/my-account';
						}
						//to login via ajax
						if($ajax)
						{
							die(json_encode(array('result'=>true)));
						}
						else
						{
							redirect($redirect);
						}
						
					}
					else
					{
						//this adds the redirect back to flash data if they provide an incorrect credentials
						
						
						//to login via ajax
						if($ajax)
						{
							die(json_encode(array('result'=>false)));
						}
						else
						{
							$this->session->set_flashdata('redirect', $redirect);
							$this->session->set_flashdata('error', lang('login_failed'));
							
							redirect('secure/login');
						}
					}
				} else {
					// verification progress
					die(json_encode(array('result'=>false,'code'=>501,'id'=>$customerData['id'])));
				}
			} else {
				die(json_encode(array('result'=>false,'code'=>404)));
			}
		}
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->helper('directory');
	
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
		//$data['banners']	= $this->banner_model->get_banners();
		//$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered(0);
			
		$this->view('login', $data);
	}

	function social_login(){
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		/*if ($redirect){
			redirect('secure/my_account/');
		}*/

		$this->load->helper('form');
		$data['redirect']	= $this->session->flashdata('redirect');

		$email		= $this->input->post('email');

		// check already a user
		if($this->check_email($email,true))
		{
			//register
			$save['id']		= false;
			
			$save['firstname']			= $this->input->post('firstname');
			$save['lastname']			= $this->input->post('lastname');
			$save['email']				= $email;
			$save['phone']				= '';
			$save['company']			= 0;
			$save['provider']			= $this->input->post('provider');
			$save['provider_id']		= $this->input->post('provider_id');
			$save['active']				= $this->config->item('new_customer_status');
			$save['email_subscribe']	= 1;
			
			$id = $this->Customer_model->save($save);

			/* send an email */
			// get the email template
			$res = $this->db->where('id', '6')->get('canned_messages');
			$row = $res->row_array();
	
			$row['subject'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['subject']);
			$row['content'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['content']);
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
			$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
			$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
			
			$this->load->library('email');
			
			
			$config = Array(
	        	'protocol'  => 'smtp',
	        	'smtp_host' => 'ssl://smtp.zoho.com',
	        	'smtp_port' => '465',
	        	'smtp_user' => 'brijesh@inikworld.com',
	        	'smtp_pass' => 'Brijesh123#',
	        	'mailtype'  => 'html',
		    	'newline'   => "\r\n"
	        );
			$this->email->initialize($config);
	
			$this->email->from('brijesh@inikworld.com', $this->config->item('company_name'));
			$this->email->to($save['email']);
			$this->email->bcc('brijesh@inikworld.com');
			$this->email->subject($row['subject']);
			$this->email->message(html_entity_decode($row['content']));
			
			$this->email->send();
			
			
			$this->session->set_flashdata('message', sprintf( lang('registration_thanks'), $this->input->post('firstname') ) );
			die(json_encode(array('result'=>false,'code'=>501,'id'=>$id)));

		} else {
			//check phone number verified
			$customerData = $this->Customer_model->get_customer_by_email($email);
			if($customerData['confirmed'] == 1){
				//login
				$login		= $this->Customer_model->login($email, '', true, true);
				if ($login){
					die(json_encode(array('result'=>true)));
				} else {
					die(json_encode(array('result'=>false)));
				}
			} else {
				// verification progress
				die(json_encode(array('result'=>false,'code'=>501,'id'=>$customerData['id'])));
			}
		}
	}
	
	function logout()
	{
		$this->Customer_model->logout();
		//redirect('secure/login');
		redirect('/');
	}
	
	function register($ajax=false)
	{
	
		$redirect	= $this->Customer_model->is_logged_in(false, false);
		//if they are logged in, we send them back to the my_account by default
		/*if ($redirect)
		{
			redirect('secure/my_account');
		}*/
		
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		/*
		we're going to set this up early.
		we can set a redirect on this, if a customer is checking out, they need an account.
		this will allow them to register and then complete their checkout, or it will allow them
		to register at anytime and by default, redirect them to the homepage.
		*/
		$data['redirect']	= $this->session->flashdata('redirect');
		
		$data['page_title']	= lang('account_registration');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		//default values are empty if the customer is new

		$data['company']	= '';
		$data['firstname']	= '';
		$data['lastname']	= '';
		$data['email']		= '';
		$data['phone']		= '';
		$data['address1']	= '';
		$data['address2']	= '';
		$data['landmark']	= '';
		$data['city']		= '';
		$data['state']		= '';
		$data['zip']		= '';



		$this->form_validation->set_rules('company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'lang:address_firstname', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('lastname', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:address_email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		//$this->form_validation->set_rules('phone', 'lang:address_phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone');
		$this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]|sha1');
		//$this->form_validation->set_rules('confirm', 'lang:confirm_password', 'required|matches[password]');
		$this->form_validation->set_rules('email_subscribe', 'lang:account_newsletter_subscribe', 'trim|numeric|max_length[1]');

		
		if ($this->form_validation->run() == FALSE)
		{
			if($ajax){
				$errors = array();
				foreach ($this->input->post() as $key => $value){
					if(form_error($key) != ''){
						$errors[$key] = form_error($key);
					}
				}

				$response['error'] = array_filter($errors);
				$response['result'] = FALSE;
				die(json_encode($response));
			} else {
				//if they have submitted the form already and it has returned with errors, reset the redirect
				if ($this->input->post('submitted'))
				{
					$data['redirect']	= $this->input->post('redirect');				
				}
				
				// load other page content 
				//$this->load->model('banner_model');
				$this->load->helper('directory');
			
				$data['categories']	= $this->Category_model->get_categories_tiered(0);
				
				$data['error'] = validation_errors();
				
				$this->view('register', $data);
			}
		}
		else
		{
			
			
			$save['id']		= false;
			
			$save['firstname']			= $this->input->post('firstname');
			$save['lastname']			= $this->input->post('lastname');
			$save['email']				= $this->input->post('email');
			//$save['phone']				= $this->input->post('phone');
			$save['company']			= $this->input->post('company');
			$save['active']				= $this->config->item('new_customer_status');
			$save['email_subscribe']	= intval((bool)$this->input->post('email_subscribe'));
			
			$save['password']			= $this->input->post('password');
			
			$redirect					= $this->input->post('redirect');
			
			//if we don't have a value for redirect
			if ($redirect == '')
			{
				$redirect = 'secure/my-account';
			}
			
			// save the customer info and get their new id
			$id = $this->Customer_model->save($save);

			/* send an email */
			// get the email template
			$res = $this->db->where('id', '6')->get('canned_messages');
			$row = $res->row_array();
			
			// set replacement values for subject & body
			
			// {customer_name}
			$row['subject'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['subject']);
			$row['content'] = str_replace('{customer_name}', $this->input->post('firstname').' '. $this->input->post('lastname'), $row['content']);
			
			// {url}
			$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
			$row['content'] = str_replace('{url}', $this->config->item('base_url'), $row['content']);
			
			// {site_name}
			$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
			$row['content'] = str_replace('{site_name}', $this->config->item('company_name'), $row['content']);
			$data['message'] = $row['content'];

            $row['content'] = $this->load->view('thank_u_email', $data, true);
			
			$this->load->library('email');
			
			$config = Array(
	        	'protocol'  => 'smtp',
	        	'smtp_host' => 'ssl://smtp.zoho.com',
	        	'smtp_port' => '465',
	        	'smtp_user' => 'brijesh@inikworld.com',
	        	'smtp_pass' => 'Brijesh123#',
	        	'mailtype'  => 'html',
		    	'newline'   => "\r\n"
	        );
			$this->email->initialize($config);
	
			$this->email->from('brijesh@inikworld.com', $this->config->item('company_name'));
			$this->email->to($save['email']);
			$this->email->bcc('brijesh@inikworld.com');
			$this->email->subject($row['subject']);
			$this->email->message($row['content']);
			
			$this->email->send();
			
			$this->session->set_flashdata('message', sprintf( lang('registration_thanks'), $this->input->post('firstname') ) );
			
			//lets automatically log them in
			//$this->Customer_model->login($save['email'], $this->input->post('confirm'));
			
			//we're just going to make this secure regardless, because we don't know if they are
			//wanting to redirect to an insecure location, if it needs to be secured then we can use the secure redirect in the controller
			//to redirect them, if there is no redirect, the it should redirect to the homepage.
			//redirect($redirect);

			die(json_encode(array('result'=>true,'code'=>501,'id'=>$id)));
		}
	}
	
	function check_email($str,$validationOnly=false)
	{
		if(!empty($this->customer['id']))
		{
			$email = $this->Customer_model->check_email($str, $this->customer['id']);
		}
		else
		{
			$email = $this->Customer_model->check_email($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_email', lang('error_email'));
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function check_phone($str,$validationOnly=false)
	{
		if(!empty($this->customer['id']))
		{
			$email = $this->Customer_model->check_phone($str, $this->customer['id']);
		}
		else
		{
			$email = $this->Customer_model->check_phone($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_phone', 'The requested phone number is already in use.');
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function forgot_password()
	{
		$data['page_title']	= lang('forgot_password');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$submitted = $this->input->post('submitted');
		if ($submitted)
		{
			$this->load->helper('string');
			$email = $this->input->post('email');
			
			$reset = $this->Customer_model->reset_password($email);
			
			if ($reset)
			{						
				$this->session->set_flashdata('message', lang('message_new_password'));
			}
			else
			{
				$this->session->set_flashdata('error', lang('error_no_account_record'));
			}
			redirect('secure/forgot-password');
		}
		
		// load other page content 
		//$this->load->model('banner_model');
		$this->load->helper('directory');
	
		//if they want to limit to the top 5 banners and use the enable/disable on dates, add true to the get_banners function
		//$data['banners']	= $this->banner_model->get_banners();
		//$data['ads']		= $this->banner_model->get_banners(true);
		$data['categories']	= $this->Category_model->get_categories_tiered();
		
		
		$this->view('forgot_password', $data);
	}
	
	function my_account($offset=0)
	{
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_account/');
	
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->Customer_model->get_address_list($this->customer['id']);
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		$data['customer_addresses']	= $this->Customer_model->get_address_list($data['customer']['id']);
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'lang:address_firstname', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('lastname', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:address_email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'lang:address_phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone');
		$this->form_validation->set_rules('email_subscribe', 'lang:account_newsletter_subscribe', 'trim|numeric|max_length[1]');
		
		if($this->input->post('password') != '' || $this->input->post('confirm') != '')
		{
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|sha1');
			$this->form_validation->set_rules('confirm', 'Confirm Password', 'required|matches[password]');
		}
		else
		{
			$this->form_validation->set_rules('password', 'Password');
			$this->form_validation->set_rules('confirm', 'Confirm Password');
		}


		if ($this->form_validation->run() == FALSE)
		{
			$this->view('my_account', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			if($this->input->post('password') != '')
			{
				$customer['password']			= $this->input->post('password');
			}
						
			$this->go_cart->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', lang('message_account_updated'));
			
			redirect('secure/my-account');
		}
	
	}

	function orders($offset=0)
	{
		$this->Customer_model->is_logged_in('secure/my_account/');
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		$data['page_title']			= 'Orders';
		
		$this->load->model('order_model');
		$this->load->helper('directory');
		$this->load->helper('date');
		
		$this->load->library('pagination');

		$config['base_url'] = site_url('secure/orders');
		$config['total_rows'] = $this->order_model->count_customer_orders($this->customer['id']);
		$config['per_page'] = '10'; 
	
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		
		$this->pagination->initialize($config); 
		
		$data['orders_pagination'] = $this->pagination->create_links();

		$orders		= $this->order_model->get_customer_orders($this->customer['id'], $offset, $config['per_page']);
		
		if(!empty($orders)){
			foreach ($orders as $order) {
				$order->items = $this->order_model->get_items($order->id,false,true);;
			}
		}

		$data['orders'] = $orders;

		$this->view('orders', $data);
	}

	function order($id=false)
	{
		$this->Customer_model->is_logged_in('secure/my_account/');
		if(!$id){
			redirect(site_url('secure/orders'));
		}
		$data['page_title']			= 'Order Details';
		
		$this->load->model('order_model');
		$this->load->helper('directory');
		$this->load->helper('date');
		
		$data['order']	= $this->order_model->get_order_by_order_number($id, $this->customer['id']);
		if(empty($data['order'])){
			redirect(site_url('secure/orders'));
		}

		$this->view('order', $data);
	}
	
	function track_order()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('order_id', 'Order Id', 'trim|required');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
			redirect(site_url('track-order'));
		} else {
			$data['page_title']			= 'Order Details';
			$this->load->model('order_model');
			$this->load->helper('directory');
			$this->load->helper('date');
			$data['order']	= $this->order_model->get_order_by_order_number($this->input->post('order_id'));
			if(empty($data['order'])){
				$this->session->set_flashdata('error', 'Order Not Found');
				redirect(site_url('track-order'));
			}
			$this->view('track_order', $data);
		}
	}

	function addresses()
	{
		//make sure they're logged in
		$this->Customer_model->is_logged_in('secure/my_account/');
	
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['customer']			= (array)$this->Customer_model->get_customer($this->customer['id']);
			
		$data['addresses'] 			= $this->Customer_model->get_address_list($this->customer['id']);
		
		$data['page_title']			= 'Welcome '.$data['customer']['firstname'].' '.$data['customer']['lastname'];
		$data['customer_addresses']	= $this->Customer_model->get_address_list($data['customer']['id']);
		
		$this->load->helper('directory');
		$this->load->helper('date');
		
		
		
		//if they're logged in, then we have all their acct. info in the cookie.
		
		/*
		This is for the customers to be able to edit their account information
		*/

		$this->load->library('form_validation');
		$this->form_validation->set_rules('company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'lang:address_firstname', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('lastname', 'lang:address_lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'lang:address_email', 'trim|required|valid_email|max_length[128]|callback_check_email');
		$this->form_validation->set_rules('phone', 'lang:address_phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone');
		$this->form_validation->set_rules('email_subscribe', 'lang:account_newsletter_subscribe', 'trim|numeric|max_length[1]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->view('addresses', $data);
		}
		else
		{
			$customer = array();
			$customer['id']						= $this->customer['id'];
			$customer['company']				= $this->input->post('company');
			$customer['firstname']				= $this->input->post('firstname');
			$customer['lastname']				= $this->input->post('lastname');
			$customer['email']					= $this->input->post('email');
			$customer['phone']					= $this->input->post('phone');
			$customer['email_subscribe']		= intval((bool)$this->input->post('email_subscribe'));
			
						
			$this->go_cart->save_customer($this->customer);
			$this->Customer_model->save($customer);
			
			$this->session->set_flashdata('message', lang('message_account_updated'));
			
			redirect('secure/addresses');
		}
	
	}
	
	
	function my_downloads($code=false)
	{
		
		if($code!==false)
		{
			$data['downloads'] = $this->Digital_Product_model->get_downloads_by_code($code);
		} else {
			$this->Customer_model->is_logged_in();
			
			$customer = $this->go_cart->customer();
			
			$data['downloads'] = $this->Digital_Product_model->get_user_downloads($customer['id']);
		}
		
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		$data['page_title'] = lang('my_downloads');
		
		$this->view('my_downloads', $data);
	}
	
	
	function download($link)
	{
		$filedata = $this->Digital_Product_model->get_file_info_by_link($link);
		
		// missing file (bad link)
		if(!$filedata)
		{
			show_404();
		}
		
		// validate download counter
		if($filedata->max_downloads > 0)
		{
			if(intval($filedata->downloads) >= intval($filedata->max_downloads))
			{
				show_404();
			}
		}
		
		
		// increment downloads counter
		$this->Digital_Product_model->touch_download($link);
		
		// Deliver file
		$this->load->helper('download');
		force_download('uploads/digital_uploads/', $filedata->filename);
	}
	
	
	function set_default_address()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');
	
		$customer = $this->go_cart->customer();
		$save['id'] = $customer['id'];
		
		if($type=='bill')
		{
			$save['default_billing_address'] = $id;

			$customer['bill_address'] = $this->Customer_model->get_address($id);
			$customer['default_billing_address'] = $id;
		} else {

			$save['default_shipping_address'] = $id;

			$customer['ship_address'] = $this->Customer_model->get_address($id);
			$customer['default_shipping_address'] = $id;
		} 
		
		//update customer db record
		$this->Customer_model->save($save);
		
		//update customer session info
		$this->go_cart->save_customer($customer);
		
		echo "1";
	}
	
	function address_form($id = 0)
	{
		
		$customer = $this->go_cart->customer();
		
		//grab the address if it's available
		$data['id']				= false;
		$data['company']		= $customer['company'];
		$data['firstname']		= $customer['firstname'];
		$data['lastname']		= $customer['lastname'];
		$data['email']			= $customer['email'];
		$data['phone']			= $customer['phone'];
		$data['address_type']	= isset($customer['address_type'])?$customer['address_type']:'';
		$data['address1']		= '';
		$data['address2']		= '';
		$data['landmark']		= '';
		$data['city']			= '';
		$data['country_id'] 	= '';
		$data['zone_id']		= '';
		$data['zip']			= '';
		

		if($id != 0)
		{
			$a	= $this->Customer_model->get_address($id);
			if($a['customer_id'] == $this->customer['id'])
			{
				//notice that this is replacing all of the data array
				//if anything beyond this form data needs to be added to
				//the data array, do so after this portion of code
				$data		= $a['field_data'];
				$data['id']	= $id;
			} else {
				redirect('/'); // don't allow cross-customer editing
			}
			
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}
		
		//get the countries list for the dropdown
		$data['countries_menu']	= $this->location_model->get_countries_menu();
		
		if($id==0)
		{
			//if there is no set ID, the get the zones of the first country in the countries menu
			$cmArray = array_keys($data['countries_menu']);
			$data['zones_menu']	= $this->location_model->get_zones_menu(array_shift($cmArray));
		} else {
			$data['zones_menu']	= $this->location_model->get_zones_menu($data['country_id']);
		}

		$this->load->library('form_validation');	
		$this->form_validation->set_rules('company', 'lang:address_company', 'trim|max_length[128]');
		$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|max_length[32]');
		//$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('address1', 'Address', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('address2', 'Address Line 2', 'trim|max_length[255]');
		$this->form_validation->set_rules('landmark', 'Landmark', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|numeric');
		$this->form_validation->set_rules('zone_id', 'State', 'trim|required|numeric');
		$this->form_validation->set_rules('zip', 'Pincode', 'trim|required|numeric|min_length[6]|max_length[6]');
		$this->form_validation->set_rules('address_type', 'Address Type', 'trim|required|in_list[Home,Work]');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			if(validation_errors() != '')
			{
				echo validation_errors();
			}
			else
			{
				$this->partial('address_form', $data);
			}
		}
		else
		{
			$a = array();
			$a['id']							= ($id==0) ? 0 : $id;
			$a['customer_id']					= $this->customer['id'];
			$a['field_data']['company']			= $this->input->post('company');
			$a['field_data']['firstname']		= $this->input->post('firstname');
			$a['field_data']['lastname']		= $this->input->post('lastname');
			$a['field_data']['email']			= $this->input->post('email');
			$a['field_data']['phone']			= $this->input->post('phone');
			$a['field_data']['address1']		= $this->input->post('address1');
			$a['field_data']['address2']		= $this->input->post('address2');
			$a['field_data']['landmark']		= $this->input->post('landmark');
			$a['field_data']['city']			= $this->input->post('city');
			$a['field_data']['zip']				= $this->input->post('zip');
			$a['field_data']['address_type']	= $this->input->post('address_type');
			
			// get zone / country data using the zone id submitted as state
			$country = $this->location_model->get_country(set_value('country_id'));	
			$zone    = $this->location_model->get_zone(set_value('zone_id'));		
			if(!empty($country))
			{
				$a['field_data']['zone']		= $zone->code;  // save the state for output formatted addresses
				$a['field_data']['country']		= $country->name; // some shipping libraries require country name
				$a['field_data']['country_code']   = $country->iso_code_2; // some shipping libraries require the code 
				$a['field_data']['country_id']  = $this->input->post('country_id');
				$a['field_data']['zone_id']		= $this->input->post('zone_id');  
			}
			
			$aid = $this->Customer_model->save_address($a);

			//make this current billing address
            $customer['bill_address'] = $a['field_data'];
            $customer['bill_address']['id'] = $aid;

            $customer['ship_address'] = $a['field_data'];
            $customer['ship_address']['id'] = $aid;

            // Use as shipping address
			$customer['ship_address']	= $customer['bill_address'];
			
			/* save customer details*/
			$this->go_cart->save_customer($customer);

			$this->session->set_flashdata('message', lang('message_address_saved'));
			echo 1;
		}
	}
	
	function delete_address()
	{
		$id = $this->input->post('id');
		// use the customer id with the addr id to prevent a random number from being sent in and deleting an address
		$customer = $this->go_cart->customer();
		$this->Customer_model->delete_address($id, $customer['id']);
		echo $id;
	}

	function update_phone(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|regex_match[/^[0-9]{10}$/]');
		
		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('result'=>false,'code'=>404,'error'=>form_error('phone'))));
		} else {

			$id = $this->input->post('id');
			$phone = $this->input->post('phone');

			if($this->Customer_model->check_phone($phone, $id)) {
				die(json_encode(array('result'=>false,'code'=>501)));
			} else {
				$otp = mt_rand(100000, 999999);
				if($this->Customer_model->update_phone($id,$phone, $otp)) {
					$this->load->model('Generalmodel');
					$this->Generalmodel->send_sms($phone, 'Your OTP is '.$otp);
					die(json_encode(array('result'=>true)));
				} else {
					die(json_encode(array('result'=>false)));
				}
			}
		}
	}

	function verify_otp(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('otp', 'OTP', 'trim|required|regex_match[/^[0-9]{6}$/]');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('result'=>false,'code'=>404,'error'=>form_error('otp'))));
		} else {

			$id = $this->input->post('id');
			$otp = $this->input->post('otp');

			$user_email = $this->Customer_model->verify_otp($id,$otp);

			if($user_email) {
				$this->session->set_flashdata('message', 'Thank you for verifying phone number.' );
				$this->Customer_model->login($user_email, '',true,true);
				die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false)));
			}
		}
	}

	function resend_guest_otp(){
		$customer				= $this->go_cart->customer();
		// temp OTP
		$otp 										= mt_rand(100000, 999999);
		$customer['bill_address']['otp']			= $otp;
		$this->load->model('Generalmodel');
		$this->Generalmodel->send_sms($customer['bill_address']['phone'], 'Your COD Confirmation Code is '.$otp.' Note: until you verify this order, it will not be processed.');
		
		$customer['ship_address']	= $customer['bill_address'];
		$this->go_cart->save_customer($customer);

		die(json_encode(array('result'=>true,'otp'=>$otp)));
	}

	function verify_otp_guest(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('otp', 'OTP', 'trim|required|regex_match[/^[0-9]{6}$/]');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('result'=>false,'code'=>404,'error'=>form_error('otp'))));
		} else {

			$otp = trim($this->input->post('otp'));

			$customer				= $this->go_cart->customer();
			if($otp == $customer['bill_address']['otp']){
				$this->go_cart->save_customer_bill_address_key('is_order_otp_verified', true);
				die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false)));
			}
		}
	}

	function cancel_order(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('order_item_id', 'Order Item', 'trim|required|numeric');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('result'=>false, 'error'=>'Something went wrong.')));
		} else {

			$this->load->model('order_model');
			$item_id 	= $this->input->post('order_item_id');
			
			//get last order status -> check if not delivered
			$last_status = $this->Order_model->get_order_item_statuses($item_id, true);

			if($last_status->ots_status != 'Delivered' || $last_status->ots_status != 'Cancelled' || $last_status->ots_status != 'On Hold'){
				$save			= array();
				$save['order_item_id']	= $this->input->post('order_item_id');
				$save['ots_status']		= 'Cancelled';
				$save['notes']			= '';
				
				if($this->Order_model->status_update($save)){

					$this->load->model('Generalmodel');

					$waybill 	= $this->Order_model->get_waybill_number_by_item($item_id);
					$response 	= $this->Generalmodel->hit_delhivery('post','p/edit/',array('waybill'=>$waybill, 'cancellation'=>"true") );
					//$return 	= json_decode($response);

					$orderInfo = $this->Order_model->get_order_by_item_package($item_id);
					$pm_id = 0;
					$pm_quantity = 0;
					$products_list = array();
					foreach($orderInfo->contents as $product):
						$products_list[] = ucwords(strtolower($this->Generalmodel->shorten_string($product['name'], 4, false)));
						if($product['order_item_id'] == $item_id){
							$pm_id = $product['pm_id'];
							$pm_quantity = $product['quantity'];
						}
					endforeach;

					//increase stock
					if($pm_id > 0 && $pm_quantity > 0){
						$this->Product_model->increase_product_stock($pm_id, $pm_quantity);
					}

					$tracking_url = 'http://www.delhivery.com/track/package/'.$orderInfo->contents[0]['waybill'];
					$trackingShortUrl = $this->Generalmodel->url_shortener($tracking_url);
					if(!$trackingShortUrl){
						$trackingShortUrl = $tracking_url;
					}

					$order_page_url = site_url().'secure/order/'.$orderInfo->order_number;
					$orderPageShortUrl = $this->Generalmodel->url_shortener($order_page_url);
					if(!$orderPageShortUrl){
						$orderPageShortUrl = $order_page_url;
					}

					$this->load->model('Messages_model');
					$row = $this->Messages_model->get_message(9);
		            $row['content'] = $this->load->view('order_cancelled_email', $orderInfo, true);
		            $sms_content 	= 'Your package '.implode(', ', $products_list).' ref to order '.$orderInfo->order_number.' is cancelled if you have already paid, refund will be initiated shortly. Details: '.$orderPageShortUrl;
		        
					$row['subject'] = str_replace('{customer_name}', $orderInfo->ship_firstname.' '.$orderInfo->ship_lastname, $row['subject']);
					$row['subject'] = str_replace('{url}', $this->config->item('base_url'), $row['subject']);
					$row['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $row['subject']);
					$row['subject'] = str_replace('{order_number}', $orderInfo->order_number, $row['subject']);
					
					$this->load->library('email');
					
					$config = Array(
			        	'protocol'  => 'smtp',
			        	'smtp_host' => 'ssl://smtp.zoho.com',
			        	'smtp_port' => '465',
			        	'smtp_user' => 'brijesh@inikworld.com',
			        	'smtp_pass' => 'Brijesh123#',
			        	'mailtype'  => 'html',
					    'newline'   => "\r\n"
			        );
					$this->email->initialize($config);

					$this->email->from('brijesh@inikworld.com', $this->config->item('company_name'));
					
					$this->email->to($orderInfo->ship_email);
					
					//email the admin
					$this->email->bcc('brijesh@inikworld.com');
					
					$this->email->subject($row['subject']);
					$this->email->message($row['content']);
					
					$this->email->send();

					//send sms to user
					$this->Generalmodel->send_sms($orderInfo->ship_phone, $sms_content);

					$this->session->set_flashdata('message', 'Product cancellation request has been successfully sent.' );
					die(json_encode(array('result'=>true)));
				} else {
					die(json_encode(array('result'=>false, 'error'=>'This product cant be cancelled at this stage.')));
				}
			} else {
				die(json_encode(array('result'=>false, 'error'=>'This product cant be cancelled at this stage.')));
			}
		}
	}

	function return_order(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('order_item_id', 'Order Item', 'trim|required|numeric');
		$this->form_validation->set_rules('return_reason', 'Return Reason', 'trim|required');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('result'=>false, 'data'=>'Please fill all the fields properly.')));
		} else {

			$this->load->model('order_model');
			$item_id 	= $this->input->post('order_item_id');
			
			//get last order status -> check if not delivered
			$last_status = $this->Order_model->get_order_item_statuses($item_id, true);

			if($last_status->ots_status == 'Delivered'){

				$image_uploaded = false;
				if(isset($_FILES)){
					$this->load->library('upload');
					if($_FILES['return_image']['name'] != ''){

						$config = array(
					        'allowed_types' => 'jpg|jpeg|png|gif|JPG|JPEG',
					        'max_size'      => $this->config->item('size_limit'),
					        'upload_path'	=> 'uploads/return',
					        'encrypt_name'	=> true,
					        'remove_spaces'	=> true
				      	);
				      	$this->upload->initialize($config);

				      	if ( ! $this->upload->do_upload('return_image')) :
				        	$return['data'] 		= $this->upload->display_errors();
							$return['result'] 		= false;

				      	else :
				        	$upload_data = $this->upload->data();
				        	//$return['data']			= $upload_data['file_name'];
							$return['result'] 		= true;
							$image_uploaded 		= $upload_data['file_name'];

							$video_uploaded = '';
							if($_FILES['return_video']['name'] != ''){
								$config = array(
							        'allowed_types' => 'mpeg4|mp4|avi|wmv|3gp|mpeg|mov',
							        'max_size'      => $this->config->item('size_limit'),
							        'upload_path'	=> 'uploads/return',
							        'encrypt_name'	=> true,
							        'remove_spaces'	=> true
						      	);
						      	$this->upload->initialize($config);

						      	if ( ! $this->upload->do_upload('return_video')) :
						        	$return['data'] 		= $this->upload->display_errors();
									$return['result'] 		= false;

						      	else :
						        	$upload_data = $this->upload->data();
						        	//$return['data']			= $upload_data['file_name'];
									$return['result'] 		= true;
									$video_uploaded 		= $upload_data['file_name'];
								endif;
							}
						endif;
					} else {
						$return['data'] 		= 'Image is required.';
						$return['result'] 		= false;
					}


					if($image_uploaded && $return['result']){
						$save						= array();
						$save['order_item_id']		= $item_id;
						$save['ots_status']			= 'Return Requested';
						$save['notes']				= $this->input->post('return_reason');

						$extra_data['image'] 		= $image_uploaded;
						if($video_uploaded != ''){
							$extra_data['video']	= $video_uploaded;
						}
						if($this->input->post('return_description')){
							$extra_data['comments']	= $this->input->post('return_description');
						}
						$save['data']				= json_encode($extra_data);
						
						if($this->Order_model->status_update($save)){

							//$this->load->model('Generalmodel');

							//$waybill 	= $this->Order_model->get_waybill_number_by_item($item_id);
							//$response 	= $this->Generalmodel->hit_delhivery('post','p/edit/',array('waybill'=>$waybill, 'cancellation'=>"true") );
								//$return 	= json_decode($response);

							$this->session->set_flashdata('message', 'Product return request has been successfully sent.' );
							die(json_encode($return));
						} else {
							die(json_encode(array('result'=>false, 'data'=>'This product cant be returned at this stage.')));
						}
					} else {
						die(json_encode($return));
					}

				} else {
					$return['data'] 		= 'Image is required.';
					$return['result'] 		= false;
				}
			} else {
				die(json_encode(array('result'=>false, 'data'=>'This product cant be returned at this stage.')));
			}
		}
	}

	function invoice($order_id=false, $order_item_id=false){
		$this->Customer_model->is_logged_in('secure/my_account/');
		if(!$order_id || !$order_item_id){
			redirect(site_url('secure/orders'));
		}
		
		$this->load->helper('date');
		$data['order'] 		= $this->Order_model->get_package_ready_order_item($order_id,$this->customer['id'], false, $order_item_id);

		$this->load->view($this->config->item('merchant_folder').'/invoice.php', $data);
	}

	function get_order_item_statuses($order_item_id, $latest=false){
		die(json_encode($this->Order_model->get_order_item_statuses($order_item_id, $latest)));
	}

	function getOrderItemLiveStatuses($order_item_id, $latest=false){
		$waybillDetails = $this->Order_model->get_waybill_number_with_method(false, false, $order_item_id);

		if($waybillDetails){
			$this->load->model('Generalmodel');
			$data = array();

			if($waybillDetails['waybill_method'] == 'Delhivery'){

				$data['waybill'] 	= $waybillDetails['waybill'];
				$data['verbose'] 	= 1;

				$responseCharge = $this->Generalmodel->hit_delhivery('get','packages',$data);
				$responseCharge = json_decode($responseCharge, true);

				if(isset($responseCharge['ShipmentData'][0]['Shipment']['Status']['Status'])){
					$response = array();
					$response['status'] = array(
										'status'		=> $responseCharge['ShipmentData'][0]['Shipment']['Status']['Status'],
										'timestamp'		=> date('Y-m-d H:i:s', strtotime($responseCharge['ShipmentData'][0]['Shipment']['Status']['StatusDateTime'])),
										'instructions'	=> $responseCharge['ShipmentData'][0]['Shipment']['Status']['Instructions'],
					);

					if(!empty($responseCharge['ShipmentData'][0]['Shipment']['Scans'])){
						foreach ($responseCharge['ShipmentData'][0]['Shipment']['Scans'] as $scan) {
							$response['scans'][] = array(
										'timestamp'		=> date('Y-m-d H:i:s', strtotime($scan['ScanDetail']['StatusDateTime'])),
										'location'		=> $scan['ScanDetail']['ScannedLocation'],
										'instructions'	=> $scan['ScanDetail']['Instructions']
							); 
						}
						$response['scans'] = array_reverse($response['scans']);
					}

					die(json_encode(array('status'=>true, 'data'=> $response)));
				} else {
					die(json_encode(array('status'=>false)));
				}
			} else {
				die(json_encode(array('status'=>false)));
			}

		} else {
			die(json_encode(array('status'=>false)));
		}
	}

}