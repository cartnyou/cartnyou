<?php

class Promotions extends Merchant_Controller {

	function __construct()
	{
		parent::__construct();

		$this->auth->is_merchant_logged_in();
		
		$this->load->helper('date');
	}
	
	function index()
	{
		$data['active_nav'] = 'promotions';
		$data['page_title']	= 'Promotion Plans';
		
		$this->view($this->config->item('merchant_folder').'/promotions', $data);
	}

}