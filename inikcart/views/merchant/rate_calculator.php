<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ86mGEjj4ucCxJ53vCl6RhSFnxonIJwA"></script>
<style type="text/css">
    #map_canvas {
        width: 100%;
        height: 400px;
    }
    #pickup-pincode, #delivery-pincode{
    	border-right: 0;
    }
</style>

<div class="row">
    <div class="col-sm-6 col-xs-12 margin-bottom-10">
    	<form method="POST" action="<?php echo site_url('merchant/shipping/calculate');?>" id="calculator-form">
	        <div class="row">
	            <div class="col-md-6">
	                <div class="form-group">
	                    <label>Pickup Area Pincode</label>
	                    <div class="input-group">
	                        <input type="text" name="o_pin" class="form-control" placeholder="6 Digit Pick-up Area Pincode" id="pickup-pincode">
	                        <span class="input-group-addon bkgWhite" id="pickup-pincode-tick"></span>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="form-group">
	                    <label>Delivery Area Pincode</label>
	                    <div class="input-group">
		                    <input type="text" name="d_pin" class="form-control" placeholder="6 Digit Delivery Area Pincode" id="delivery-pincode">
		                	<span class="input-group-addon bkgWhite" id="delivery-pincode-tick"></span>
	                	</div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="form-group">
	                    <label>Approximate Weight</label>
	                    <div class="input-group">
	                        <span class="input-group-addon bkgWhite">grams</span>
	                        <input type="text" name="gm" class="form-control" placeholder="For eg. 500">
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="form-group">
	                    <label>COD</label>
	                    <select class="form-control" name="pt" id="pt-mode">
                            <option value="Pre-paid">No</option>
	                        <option value="COD">Yes</option>
	                    </select>
	                </div>
	            </div>
	            <div class="col-md-6" id="cod" style="display: none;">
	                <div class="form-group">
	                    <label>Declared Value in INR</label>
	                    <div class="input-group">
	                        <span class="input-group-addon bkgWhite"><i class="fa fa-inr"></i></span>
	                        <input type="text" name="cod" class="form-control" placeholder="Declared Value" id="cod-input">
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row mtb-20">
	            <div class="col-md-12">
	                <button class="btn btn-primary" type="submit" id="submit-btn"><i class="fa fa-calculator"></i> Calculate</button>
	            </div>
	        </div>

            <div id="calculator-result-msg" class="alert alert-danger" style="display: none;"></div>
	        <div id="calculator-result" style="display: none;" class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Courier Provider</th>
                            <th>Rate (INR)</th>
                        </tr>
                    </thead>
                    <tbody id="calculator-result-body">
                        
                    </tbody>
                </table>   
            </div>
	    </form>

	    <style type="text/css">
	    	.sm-font-ul p {
	    		font-size: 12px;
	    	}
	    </style>
        <div class="row mtb-20">
            <div class="col-md-12">
                <h3>Important terms</h3>
                <ul class="sm-font-ul">
                    <li><p>Above prices are inclusive of Service tax</p></li>
                    <li><p>The above pricing are subject to change based on fuel surcharges and courier company base rates.</p></li>
                    <li>
                        <p>
                            Freight is calculated on the basis of volumetric weight or actual weight whichever is higher. Dead/Dry weight or volumetric weight whichever is higher should be taken into consideration while calculating the rates. Volumetric weight is calculated LxBxH/5000 (length, breath, height has to be taken in Centimeters and divided by 5000, this will give the value in Kilograms)
                        </p>
                    </li>
                    <li>
                        <p>
                            Other Charges like Octroi charges, state entry tax and fees, address correction charges if applicable shall be charged extra.
                        </p>
                    </li>
                    <li>
                        <p>
                            RTO (return to origin) shipment shall be charged same as the forward delivery rate.
                        </p>
                    </li>
                    <li>
                        <p>
                            For any queries a ticket has to be raised on info@gojojo.in
                        </p>
                    </li>
                    <li>
                        <p>
                            The maximum liability if any is limited to whatever compensation the logistics partner offers to Company in event of a claim by the Merchant, provided such claim is raised by the Merchant within one (1) month from the date of such damage or loss or theft.
                        </p>
                    </li>
                    <li>
                        <p>
                            Gojojo shall not assist in shipping goods that come under the category of prohibited, dangerous goods or restricted good.
                        </p>
                    </li>
                    <li>
                        <p>
                            Detailed terms and conditions can be review on <a href="<?php echo site_url('terms-and-conditions');?>">www.gojojo.in/terms-and-conditions</a>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-6 hidden-xs">
        <div id="map_canvas"></div>
    </div>
</div>


<script type="text/javascript">
$(function(){

	$('#pt-mode').on('change', function() {
		var val = this.value;
        $('#cod-input').val('');
		if(val == 'cod'){
			$('#cod').show();
		} else {
			$('#cod').hide();
		}
	});

	$(document).on("submit", '#calculator-form', function(e){
        e.preventDefault();
        $('.error').remove();
        $('#calculator-result').hide();
        $('#calculator-result-msg').hide();
        $('#submit-btn').prop('disabled',true);

        var ele = $('#calculator-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
        	
            if (getdata.result) {
            	//var data = JSON.parse(getdata.data);
            	var html = '';

            	//html += 'Delivery Charges: <i class="fa fa-inr"></i>'+data.response.delivery_charges;
            	//html += '<br>Pickup Charges: <i class="fa fa-inr"></i>'+data.response.pickup_charges;
            	//html += '<br>Return Charges: <i class="fa fa-inr"></i>'+data.response.return_charges;

                $('#calculator-result tbody').html('');

                var row = $("<tr>");
                row.append($("<td>1</td>"))
                     .append($("<td>Delhivery</td>"))
                     .append($("<td><i class='fa fa-inr'></i>"+getdata.data+"</td>"));
                 
                $("#calculator-result tbody").append(row);
                $('#calculator-result').show();

                showMap();

            } else if(getdata.hasOwnProperty('error')) {
                $.each(getdata.error, function(key, val) {
                    $('#calculator-form input[name='+key+']').after(val);
                });
            } else {
                $('#calculator-result-msg').html(getdata.data);
                $('#calculator-result-msg').show();
            }
            $('#submit-btn').prop('disabled',false);
        });
    });

    $('#pickup-pincode').on('blur',function(){
    	var pin = $(this).val().trim();
    	var isValidZip = /(^[1-9][0-9]{5}$)/.test(pin);
    	
    	if(isValidZip){
    		$('#pickup-pincode-tick').html('<i class="fa fa-check" style="color:#116a12;"></i>');
    	} else {
    		$('#pickup-pincode-tick').html('<i class="fa fa-times" style="color:#f30c10;"></i>');
    	}
    	$('#pickup-pincode-tick').show();
    });

    $('#delivery-pincode').on('blur',function(){
    	var pin = $(this).val().trim();
    	var isValidZip = /(^[1-9][0-9]{5}$)/.test(pin);
    	
    	if(isValidZip){
    		$('#delivery-pincode-tick').html('<i class="fa fa-check" style="color:#116a12;"></i>');
    	} else {
    		$('#delivery-pincode-tick').html('<i class="fa fa-times" style="color:#f30c10;"></i>');
    	}
    	$('#delivery-pincode-tick').show();
    });

    var myOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        center: {lat: 24.018067, lng: 79.334030},
        zoom: 4
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

    function showMap(){
        var pickup_pincode = $('#pickup-pincode').val();
        var delivery_picode = $('#delivery-pincode').val();

        var geocoderp = new google.maps.Geocoder();
        geocoderp.geocode( { 'address': pickup_pincode, 'componentRestrictions': {'country':'IN'} }, function(resultsp, statusp) {
            if (statusp == google.maps.GeocoderStatus.OK) {
                var pickup_latitude = resultsp[0].geometry.location.lat();
                var pickup_longitude = resultsp[0].geometry.location.lng();

                var geocoderd = new google.maps.Geocoder();
                geocoderd.geocode( { 'address': delivery_picode, 'componentRestrictions': {'country':'IN'} }, function(resultsd, statusd) {
                    if (statusd == google.maps.GeocoderStatus.OK) {
                        var delivery_latitude = resultsd[0].geometry.location.lat();
                        var delivery_longitude = resultsd[0].geometry.location.lng();

                        var markers = [
                            [pickup_pincode, pickup_latitude, pickup_longitude],
                            [delivery_picode, delivery_latitude, delivery_longitude]
                        ];

                        /*var myOptions = {
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false
                        };
                        var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);*/
                        var infowindow = new google.maps.InfoWindow(); 
                        var marker, i;
                        var bounds = new google.maps.LatLngBounds();

                        for (i = 0; i < markers.length; i++) { 
                            var pos = new google.maps.LatLng(markers[i][1], markers[i][2]);
                            bounds.extend(pos);
                            marker = new google.maps.Marker({
                                position: pos,
                                map: map,
                                animation: google.maps.Animation.DROP
                            });
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(markers[i][0]);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        }
                        map.fitBounds(bounds);

                        // add polyline
                        var trackpathCoordinates = [
                            {lat: pickup_latitude, lng: pickup_longitude},
                            {lat: delivery_latitude, lng: delivery_longitude}
                        ];

                        var trackpath = new google.maps.Polyline({
                          path: trackpathCoordinates,
                          geodesic: true,
                          strokeColor: '#FF0000',
                          strokeOpacity: 0.5,
                          strokeWeight: 2
                        });

                        trackpath.setMap(map);
                        
                    } 
                }); 
            } 
        });
    }

});
</script>