<?php

class Profile extends Merchant_Controller {	
	
	function __construct(){		
		parent::__construct();
        
		$this->auth->check_access_merchant('Merchant', true);
		
		$this->load->model('Merchant_model');
		$this->load->helper('form');
		$this->merchant_id = $this->session->userdata('merchant')['id'];
	}

	function index(){
		$data['active_nav'] = 'dashboard';
		$data['page_title']	= 'Profile';
		
		$profile = $this->Merchant_model->get_merchant($this->merchant_id);

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		$this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|min_length[3]|max_length[128]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('store_name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Mobile Number', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone');
		$this->form_validation->set_rules('landline', 'Landline', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_check_email');
		$this->form_validation->set_rules('description', 'Description', 'trim|max_length[512]');
		$this->form_validation->set_rules('service', 'Service interested in', 'trim');
		$this->form_validation->set_rules('cst', 'GST Registration No.', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[300]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('state', 'State', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|regex_match[/^[0-9]{6}$/]');
		$this->form_validation->set_rules('account_number', 'Account Number', 'trim|max_length[100]');
		$this->form_validation->set_rules('pan_number', 'Pan Number', 'trim|max_length[50]');
		$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|max_length[100]');
		$this->form_validation->set_rules('ifsc', 'IFSC', 'trim|max_length[100]');
		
		$data['id'] 			= $profile->id;
		$data['firstname'] 		= $profile->firstname;
		$data['lastname'] 		= $profile->lastname;
		$data['store_name'] 	= $profile->store_name;
		$data['email'] 			= $profile->email;
		$data['phone'] 			= $profile->phone;
		$data['address'] 		= $profile->address;
		$data['city'] 			= $profile->city;
		$data['state'] 			= $profile->state;
		$data['zip'] 			= $profile->zip;
		$data['description'] 	= $profile->description;
		$data['logo'] 			= $profile->logo;
		$data['tin'] 			= $profile->tin;
		$data['cst'] 			= $profile->cst;
		$data['sign'] 			= $profile->sign;
		$data['pan'] 			= $profile->pan;
		$data['landline'] 		= $profile->landline;
		$data['service'] 		= $profile->service;
		$data['account_number'] = $profile->account_number;
		$data['pan_number'] 	= $profile->pan_number;
		$data['bank_name'] 		= $profile->bank_name;
		$data['ifsc'] 			= $profile->ifsc;

		if($this->input->post('submit')){
			$data['firstname'] 	= $this->input->post('firstname');
			$data['lastname'] 	= $this->input->post('lastname');
			$data['store_name'] = $this->input->post('store_name');
			$data['email'] 		= $this->input->post('email');
			$data['phone'] 		= $this->input->post('phone');
			$data['address'] 	= $this->input->post('address');
			$data['city'] 		= $this->input->post('city');
			$data['state'] 		= $this->input->post('state');
			$data['zip'] 		= $this->input->post('zip');
			$data['description']= $this->input->post('description');
			$data['tin'] 		= $this->input->post('tin');
			$data['cst'] 		= $this->input->post('cst');
			$data['landline'] 	= $this->input->post('landline');
			$data['service'] 	= $this->input->post('service');
			$data['account_number'] = $this->input->post('account_number');
			$data['pan_number'] = $this->input->post('pan_number');
			$data['bank_name'] 	= $this->input->post('bank_name');
			$data['ifsc'] 		= $this->input->post('ifsc');
		}

		if ($this->form_validation->run() == FALSE){
			$this->view($this->config->item('merchant_folder').'/edit_profile', $data);
		} else {

			$fall_img 	= false;
			$fall_error = '';

			$logo = $profile->logo;
			if(isset($_FILES['logo'])){
				if($_FILES['logo']['name'] != ''){
					$upload_return = $this->image_upload('logo');
					if($upload_return['success']){
						$logo = $upload_return['file_name'];
					} else {
						$fall_img = true;
						$fall_error .= $upload_return['error'];
					}
				}
			}
			$sign = $profile->sign;
			if(isset($_FILES['sign'])){
				if($_FILES['sign']['name'] != ''){
					$upload_return = $this->image_upload('sign');
					if($upload_return['success']){
						$sign = $upload_return['file_name'];
					} else {
						$fall_img = true;
						$fall_error .= $upload_return['error'];
					}
				}
			}
			$pan = $profile->pan;
			if(isset($_FILES['pan'])){
				if($_FILES['pan']['name'] != ''){
					$upload_return = $this->image_upload('pan');
					if($upload_return['success']){
						$pan = $upload_return['file_name'];
					} else {
						$fall_img = true;
						$fall_error .= $upload_return['error'];
					}
				}
			}

			if(!$fall_img){

				$save['id']			= $this->merchant_id;
				$save['firstname'] 	= $this->input->post('firstname');
				$save['lastname'] 	= $this->input->post('lastname');
				$save['store_name'] = $this->input->post('store_name');
				$save['email'] 		= $this->input->post('email');
				$save['phone'] 		= $this->input->post('phone');
				$save['address'] 	= $this->input->post('address');
				$save['city'] 		= $this->input->post('city');
				$save['state'] 		= $this->input->post('state');
				$save['zip'] 		= $this->input->post('zip');
				$save['description']= $this->input->post('description');
				$save['tin'] 		= $this->input->post('tin');
				$save['cst'] 		= $this->input->post('cst');
				$save['logo'] 		= $logo;
				$save['sign'] 		= $sign;
				$save['pan'] 		= $pan;
				$save['landline'] 	= $this->input->post('landline');
				$save['service'] 	= $this->input->post('service');
				$save['account_number'] 	= $this->input->post('account_number');
				$save['pan_number'] 	= $this->input->post('pan_number');
				$save['bank_name'] 	= $this->input->post('bank_name');
				$save['ifsc'] 	= $this->input->post('ifsc');
				
				//$post_images				= $this->input->post('images');

				// save info 
				$merchant_id_return = $this->Merchant_model->save($save);

				//service prepaid updation in products
				if($save['service'] == 'Sell at Gojojo'){
					$this->load->model('Product_model');
					$this->Product_model->set_merchant_products_prepaid($this->merchant_id);
				}
				
				$this->session->set_flashdata('message', 'Profile Saved Successfully.');

				redirect($this->config->item('merchant_folder').'/profile');
			} else {
				$data['error'] = $fall_error;
				$this->view($this->config->item('merchant_folder').'/edit_profile', $data);
			}
		}
	}

	function image_upload($field)
	{
		$data['file_name'] 	= false;
		$data['error']		= false;
		$data['success'] 	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload($field))
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');

			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 600;
			$config['height'] = 500;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['success'] 	= true;
			$data['file_name']	= $upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['success'] 	= false;
			$data['error'] 		= ucwords($field).': '.$this->upload->display_errors();
		}
		return $data;
	}

	function check_email($str,$validationOnly=false)
	{
		if(!empty($this->merchant_id))
		{
			$email = $this->Merchant_model->check_email($str, $this->merchant_id);
		}
		else
		{
			$email = $this->Merchant_model->check_email($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_email', 'The requested email is already in use.');
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function check_phone($str,$validationOnly=false)
	{
		if(!empty($this->merchant_id))
		{
			$email = $this->Merchant_model->check_phone($str, $this->merchant_id);
		}
		else
		{
			$email = $this->Merchant_model->check_phone($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_phone', 'The requested phone number is already in use.');
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function change_password(){
		$data['active_nav'] = 'dashboard';
		$data['page_title']	= 'Change Password';
        $this->view($this->config->item('merchant_folder').'/change_password', $data);
    }
    
    public function change_password_do(){
    	$this->load->library('form_validation');
    	$config = array(
    				array(
				'field' => 'password',
				'label' => 'Current Password',
				'rules' => 'trim|required|max_length[255]' 
			),      array(
				'field' => 'new_password',
				'label' => 'New Password',
				'rules' => 'trim|required|min_length[6]|max_length[255]' 
			),      array(
				'field' => 'new_password_confirm',
				'label' => 'New Password Confirmation',
				'rules' => 'trim|required|min_length[6]|max_length[255]' 
			)
    	);

    	$this->form_validation->set_rules($config);
    	$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    	if ($this->form_validation->run() == FALSE){
    		$errors = array();
    		foreach ($this->input->post() as $key => $value)
    		{
    			$errors[$key] = form_error($key);
    		}

    		$response['errors'] = array_filter($errors);
    		$response['status'] = FALSE;
    	} else {
	        $user_id = $this->session->userdata('user_id');
	        $password = $this->input->post('password');
	        $response_get = $this->Merchant_model->login_via_id($this->merchant_id,$password);
	        if($response_get){
	            $new_password = $this->input->post('new_password');
	            $new_password_confirm = $this->input->post('new_password_confirm');

	            if($new_password == $new_password_confirm){
	            	$save = array();
	            	$save['id'] = $this->merchant_id;
	            	$save['password'] = sha1($new_password);
	                $this->Merchant_model->save($save);
	                $response['status'] = TRUE;
	            } else {
	                $response['status'] = FALSE;
	                $response['data'] = "Both of new passwords do not match!";
	            }

	        }else{
	            $response['status'] = FALSE;
	            $response['data'] = "Current password do not match!";
	        }
	    }
        header('Content-type: application/json');
        exit(json_encode($response));
    }

}
