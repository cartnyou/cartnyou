<?php

class Locations extends Base_Controller {	
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('Location_model');
		
	}
	
	function get_zone_menu()
	{
		$id	= $this->input->post('id');
		$zones	= $this->Location_model->get_zones_menu($id);
		
		foreach($zones as $id=>$z):?>
		
		<option value="<?php echo $id;?>"><?php echo $z;?></option>
		
		<?php endforeach;
	}

	function getZip()
	{
		$zip	= $this->input->post('zip');
		$result = $this->Location_model->getZip($zip);

		if($result){
			$return['success'] = true;
        	$return['data'] = $result;
		} else {
			$return['success'] = false;
		}
		die(json_encode($return));
	}
}