<link rel="stylesheet" href="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.css">
<style type="text/css">
    .review .emojioneemoji{
        max-width: 20px;
    }
    #product .pb-left-column .product-image .owl-next {
        right: -20px;
    }
    #product .pb-left-column .product-image .owl-prev {
        left: -20px;
    }
    .left-block img{
        max-height: 400px;
    }
</style>
<?php echo theme_js('jquery.matchHeight-min.js', true);?>
<?php
//print_r($product);exit;
//$other_sellers = $product;
//array_splice($other_sellers, 0, 1);
$product = $product[0];
?>

<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->

        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <div class="center_column col-xs-12 col-sm-12" id="center_column">

                <?php if ($this->session->flashdata('message')):?>
                    <div class="alert alert-info">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php endif;?>

                <?php if ($this->session->flashdata('error')):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif;?>

                <?php if (!empty($error)):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $error;?>
                    </div>
                <?php endif;?>

                <!-- Product -->
                <div id="product">
                    <?php echo form_open('cart/add_to_cart', 'class="form-horizontal"');?>
                    <div class="primary-box row">
                        <div class="pb-left-column col-xs-12 col-md-4 col-sm-5">
                            <!-- product-imge-->
                            <div class="product-image pos-r">

                                <div class="product-full">

                                    <div class="form-action wishp-container">

                                        <a href="javascript:;" class="add-to-wishlist wishp" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?>>
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </a>

                                    </div>

                                    <?php if(!empty($product->images)): ?>
                                        <ul class="banner-owl owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "5" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":1},"1000":{"items":1}}'>

                                            <?php foreach($product->images as $relate):?>
                                                <li>
                                                    <div class="product-container">
                                                        <div class="left-block">
                                                            <?php
                                                            $photo  = '<img style="max-width:100%;margin: 0 auto;width:unset;" class="img" data-zoom-image="'.base_url('uploads/images/full/'.$relate->filename).'" src="'.base_url('uploads/images/medium/'.$relate->filename).'" alt=""/>';

                                                            echo $photo;
                                                            ?>

                                                        </div>

                                                    </div>
                                                </li>
                                            <?php endforeach;?>

                                        </ul>

                                    <?php else:
                                        $mainProductImage = theme_img('logo.png');
                                        echo theme_img('no_picture.png', lang('no_image_available'));
                                    endif; ?>

                                </div>

                            </div>
                            <div class="button-group adcrt mt-10">
                                <?php if(time() > strtotime($product->live_on)){ ?>
                                    <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>
                                        <button class="add_bag btn-add-cart btn-add-cart-xs" type="submit" value="submit"><i class="fa fa-cart-plus" aria-hidden="true"></i> <?php echo lang('form_add_to_cart');?></button>
                                        <button class="add_bag btn-buy-now" type="submit" value="buy_now" name="buy_now_redirect"><i class="fa fa-bolt" aria-hidden="true"></i> Buy Now</button>
                                    <?php else:?>
                                        <button class="add_bag add_bag_empty btn" disabled="disabled">OUT OF STOCK</button>
                                    <?php endif;?>
                                <?php } else { ?>
                                    <a class="product-coming-soon-big add_bag_empty btn"> Coming Soon</a>
                                <?php } ?>
                            </div>
                            <!-- product-imge-->
                        </div>
                        <div class="pb-right-column col-xs-12 col-md-8 col-sm-7">
                            <div class="breadcrumb clearfix hidden-xs pdb-5">
                                <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
                                <?php if(!empty($category)){ ?>
                                    <span class="navigation-pipe">&nbsp;</span>
                                    <a class="home" href="<?php echo site_url($category->slug);?>" title="<?php echo ucwords($category->name);?>"><?php echo ucwords($category->name);?></a>
                                <?php } ?>
                                <span class="navigation-pipe">&nbsp;</span>
                                <span class="navigation_page" title="<?php echo ucwords($product->name);?>"><?php echo ucwords($product->name);?></span>
                            </div>
                            <h1 class="product-name f-600"><?php echo ucwords($product->name);?></h1>

                            <div class="info-orther">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if($avg_rating->rating > 0){ ?>
                                            <span class="badge rate-badge badge-success"><?php echo round($avg_rating->rating,1); ?> <i class="fa fa-star"></i></span>
                                        <?php } ?>
                                        <span class="text-capitalize views  <?php echo ($avg_rating->rating > 0)?'viewsBr':'viewspddmr' ?>"><?php echo rand(100,150);?> Viewing right now</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>



                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>

                            <?php
                            $specialOfferAvailable = false;
                            $specialPrice = 0.00;
                            if(!empty($product->offers)){
                                foreach ($product->offers as $offer) {
                                    if($offer->offer_type == 2){
                                        $specialOfferAvailable = true;
                                        $specialPrice = $offer->offer_price;
                                        echo '<div class="special"><p class="colorGreen fs-12 f-600">Special Price ends in less than <span id="countDown" class="colorGreen" data-time="'.$offer->offer_expire_at.'"></span></p></div>';
                                        break;
                                    }
                                }
                            }
                            ?>

                            <div class="product-price-group mb-5">
                                <?php $minPrice = (min(array_filter(array($product->saleprice, $specialPrice)))); ?>

                                <?php if($minPrice > 0):?>
                                    <?php $mainPrice = $minPrice;?>
                                    <?php //echo ($specialOfferAvailable) ? '<i class="fa fa-bolt special-price-bolt"></i>' : '';?>
                                    <span class="price"><?php echo format_currency($minPrice); ?></span>
                                    <span class="old-price"></i><?php echo format_currency($product->price); ?></span>
                                <?php else: ?>
                                    <?php $mainPrice = $product->price;?>
                                    <span class="price"><?php echo format_currency($product->price); ?></span>
                                <?php endif;?>
                                <?php
                                if($minPrice > 0){
                                    $discount_percent = 0;
                                    $discount = 0;
                                    if($product->price > 0){
                                        $discount = $product->price - $minPrice;
                                        $discount_percent = ($discount * 100) / $product->price;
                                    }
                                    if($discount_percent > 0){
                                        echo '<span class="discount">'.ceil($discount_percent).'% off</span>';
                                    }
                                }
                                ?>
                            </div>

                            <?php if(!$specialOfferAvailable){ ?>
                                <?php if(!empty($product->offers)){ ?>
									<?php
										/*$offers=array();
										foreach($product->offers as $key=>$value){ 
											if($value->offer_type!=1){
												$offers[$key]=(array)$value; ?>
													<div class="prepaid">
														<p class="colorGreen f-600 fs-12">Prepaid Price</p>
														<div class="product-price-group">
															<?php
															
															$minPrice = min(array_column($offers, 'offer_price'));

															echo '<span class="price">'.format_currency($minPrice).'</span>';

															$discount_percent = 0;
															$discount = 0;
															if($product->price > 0){
																$discount = $product->price - $minPrice;
																$discount_percent = ($discount * 100) / $product->price;
															}
															if($discount_percent > 0){
																echo '(<span class="discount fs-12 pdlr-0">'.ceil($discount_percent).'% off</span>)';
															}
															?>
														</div>
													</div>
											<?php }
										}*/?>
										
									<div class="prepaid">
                                        <p class="colorGreen f-600 fs-12">Prepaid Price</p>
                                        <div class="product-price-group">
                                            <?php
											$offers=array();
											foreach($product->offers as $key=>$value){ 
												//if($value->offer_type!=1){
													$offers[$key]=(array)$value;
												//}
											}
                                            //$minPrice = min(array_column($product->offers, 'offer_price'));
                                            $minPrice = min(array_column($offers, 'offer_price'));

                                            echo '<span class="price">'.format_currency($minPrice).'</span>';

                                            $discount_percent = 0;
                                            $discount = 0;
                                            if($product->price > 0){
                                                $discount = $product->price - $minPrice;
                                                $discount_percent = ($discount * 100) / $product->price;
                                            }
                                            if($discount_percent > 0){
                                                echo '(<span class="discount fs-12 pdlr-0">'.ceil($discount_percent).'% off</span>)';
                                            }
                                            ?>
                                        </div>
                                    </div>
										
									
                                    <div class="offers" id="offers-wrap">
                                        <?php
                                        foreach ($product->offers as $offer) {
                                            echo '<p class="" data-toggle="tooltip" title="'.$offer->offer_description.'"><i class="fa fa-tag colorGreen" aria-hidden="true"></i> '.$offer->offer_name.'</p>';
                                        }
                                        ?>
                                    </div>
                                    <?php if(count($product->offers) > 2){ ?>
                                        <div class="viewMore">
                                            <a href="javascript:;" class="moreOffers f-500">View all offers</a>
                                            <a href="javascript:;" class="hideOffers moreOffers hidden f-500">Hide offers</a>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                            <form id="form-pincode">
                                <div class="pincodeAvail row">
                                    <div class="col-md-12 col-xs-12">

                                        <div class="chkpinr">
                                            <div class="form-group col-md-5 col-sm-6 col-xs-12 mb0 p0 mr-0">
                                                <div class="input-group pin">
                                                    <span class="input-group-addon checkBtn"><i class="fa fa-map-marker"></i></span>
                                                    <input type="text" class="form-control" placeholder="Enter Delivery Pincode" id="input-pincode" name="zip">
                                                    <span class="input-group-addon checkBtn"><button type="button" id="btn-pincode">Check</button></span>
                                                </div>
                                                <p id="result-pincode"></p>
                                                <!--<p class="fs-12 f-500 mb-0" id="deliverytext"></p>-->
                                            </div>
                                        </div>


                                    </div>



                                </div>
                            </form>


                                <input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
                                <input type="hidden" name="id" value="<?php echo $product->id?>"/>
                                <input type="hidden" name="pm_id" value="<?php echo $product->pm_id?>"/>




                                <div class="form-option">
                                    <?php if(count($options) > 0): ?>

                                        <div class="row">
                                            <?php foreach($options as $option):
                                                $required   = '';
                                                if($option->required)
                                                {
                                                    $required = '<span class="required">*</span>';
                                                }
                                                ?>
                                                <?php
                                                $extra_class = "";
                                                if($option->html_class != ''):
                                                    if(($option->html_class == 'power-type') || ($option->html_class == 'color') || ($option->html_class == 'size')):
                                                        $extra_class = "hidden";
                                                    endif;
                                                endif;
                                                ?>

                                                <?php
                                                $curOptionError = false;
                                                if ($this->session->flashdata('optionsRequired')):
                                                    if(in_array($option->id, $this->session->flashdata('optionsRequired'))){
                                                        $curOptionError = true;
                                                    }
                                                endif;
                                                ?>

                                                <div class="col-md-12 col-sm-12">
                                                    <div class="attributes <?php echo ($curOptionError)?'optionError':'';?>">
                                                        <div class="attribute-label col-sm-3 col-xs-4">
                                                            <p>
                                                                <span class="<?php echo ($curOptionError)?'red':'';?>"><?php echo $option->name.$required;?></span>
                                                                <?php if($option->hint !== ""){ ?>
                                                                    &nbsp;[<button class="product-option-hint" data-toggle="modal" data-target="#hintModal" data-heading="<?php echo $option->name;?> Details" data-details="<?php echo $option->hint;?>">Details</button>]
                                                                <?php } ?> :
                                                                
                                                            </p>

                                                        </div>
                                                        <div class="attribute-list col-sm-9 col-xs-8">
                                                            <?php
                                                            if($option->type == 'checklist')
                                                            {
                                                                $value  = array();
                                                                if($posted_options && isset($posted_options[$option->id]))
                                                                {
                                                                    $value  = $posted_options[$option->id];
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if(isset($option->values[0]))
                                                                {
                                                                    $value  = $option->values[0]->value;
                                                                    if($posted_options && isset($posted_options[$option->id]))
                                                                    {
                                                                        $value  = $posted_options[$option->id];
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    $value = false;
                                                                }
                                                            }
                                                            if($option->type == 'textfield'):?>
                                                                <div class="controls">

                                                                            <input type="text" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>" class="col-md-12 form-control <?php if($option->html_class != '') echo $option->html_class;?>"/>
                                                                            <span class=""><?php echo($option->values[0]->price != 0)?' (+'.format_currency($option->values[0]->price).') ':'';?></span>
                                                                </div>
                                                            <?php elseif($option->type == 'textarea'):?>
                                                                <div class="controls">
                                                                    <textarea class="col-md-12 <?php if($option->html_class != '') echo $option->html_class;?>" name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
                                                                </div>
                                                            <?php elseif($option->type == 'droplist'):?>
                                                                <div class="controls">
                                                                    <select name="option[<?php echo $option->id;?>]" class="basic form-control <?php if($option->html_class != '') echo $option->html_class;?>">
                                                                        <option value=""><?php echo lang('choose_option');?></option>

                                                                    <?php foreach ($option->values as $values):
                                                                        $selected   = '';
                                                                        if($value == $values->id)
                                                                        {
                                                                            $selected   = ' selected="selected"';
                                                                        }?>

                                                                        <option<?php echo $selected;?> value="<?php echo $values->id;?>">
                                                                            <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                                        </option>

                                                                    <?php endforeach;?>
                                                                    </select>
                                                                </div>
                                                            <?php elseif($option->type == 'radiolist'):?>
                                                                <div class="controls">
                                                                    <ul class="listInline">
                                                                    <?php foreach ($option->values as $values):

                                                                        $checked = '';
                                                                        if($value == $values->id)
                                                                        {
                                                                            $checked = ' checked="checked"';
                                                                        }?>
                                                                        <li>
                                                                        <?php if($values->image != ''){ ?>
                                                                            <label class="radio radio-option-img">
                                                                        <?php } else { ?>
                                                                            <label class="radio">
                                                                        <?php } ?>
                                                                            <?php if($values->image != ''){ ?>

                                                                                <?php if (preg_match('/^#[a-f0-9]{6}$/i', $values->image)) { ?>
                                                                                    <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>" class="hidden option-img-radio <?php if($option->html_class != '') echo $option->html_class;?>" />
                                                                                    <div class="option-img" style="background-color: <?php echo $values->image;?>"></div>
                                                                                <?php } else { ?>
                                                                                    <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>" class="hidden option-img-radio <?php if($option->html_class != '') echo $option->html_class;?>" data-smallImage="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" data-largeImage="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" />
                                                                                    <img src="<?php echo base_url('uploads/thumbs/options').'/'.$values->image;?>" class="option-img" />
                                                                                <?php } ?>

                                                                            <?php } else { ?>
                                                                                <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>" class="<?php if($option->html_class != '') echo $option->html_class;?>" /><span class="radio-inside"><?php echo $values->name;?></span>
                                                                            <?php } ?>

                                                                            <?php echo($values->price != 0)?'(+'.format_currency($values->price).') ':''; //echo $values->name;?>
                                                                        </label>
                                                                            </li>
                                                                    <?php endforeach;?>
                                                                    </ul>
                                                                </div>
                                                            <?php elseif($option->type == 'checklist'):?>
                                                                <div class="controls">
                                                                    <ul class="listInline">
                                                                    <?php foreach ($option->values as $values):

                                                                        $checked = '';
                                                                        if(in_array($values->id, $value))
                                                                        {
                                                                            $checked = ' checked="checked"';
                                                                        }?>

                                                                        <li>
                                                                        <label class="checkbox">
                                                                            <input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>" class="<?php if($option->html_class != '') echo $option->html_class;?>" />
                                                                            <?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
                                                                        </label>
                                                                        </li>


                                                                    <?php endforeach; ?>
                                                                    </ul>
                                                                </div>
                                                            <?php endif;?>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endforeach;?>
                                        </div>
                                    <?php endif;?>

                                    <div class="row">
                                        <?php if(time() > strtotime($product->live_on)){ ?>
                                            <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>
                                                <?php if(!$product->fixed_quantity) : ?>
                                                    <div class="col-md-12">
                                                        <div class="attributes">
                                                            <div class="attribute-label col-md-3 col-xs-4">Qty:</div>
                                                            <div class="attribute-list product-qty">
                                                                <div class="qty">
                                                                    <input id="option-product-qty"  type="number" min="1" value="1" name="quantity">
                                                                </div>
                                                                <!--<div class="btn-plus">
                                                                    <a href="javascript:;" class="btn-plus-up">
                                                                        <i class="fa fa-caret-up"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="btn-plus-down">
                                                                        <i class="fa fa-caret-down"></i>
                                                                    </a>
                                                                </div>-->
                                                            </div>
                                                            <!-- <div class="attribute-list col-md-9 col-xs-8">
                                                                <select class="basic" name="quantity">
                                                                    <option value="">QTY</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                </select>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif;?>
                                        <?php } ?>
                                        <div class="col-md-6 col-sm-6 hidden-xs">
                                            <a href="javascript:;"  class="linkSt toSpecs" id="seeMoreSpecs">See More Specification</a>
                                        </div>

                                    </div>
                                </div>
                                <div class="">

                                    <div class="pdb-10">
                                        <span class="text-light f-500 v-top rofsecl">Sold By:</span>
                                        <div href="javascript:;" class="linkSt f-500 lh1 rofsecr">
                                            <a class="colorBlue" target="_blank" href="<?php echo site_url().'seller/info/'.$product->merchant_slug;?>"><?php echo $product->store_name.($product->payment_mode == 1?' (Prepaid Only)':($product->payment_mode == 0?' (Cash on Delivery Available)':''));?></a>
                                            <?php if($other_sellers){
                                                if(count($other_sellers) > 1){
                                            ?>
                                                <div class="fs-12 linkSt v-top otsl pd-0"><a href="<?php echo site_url().'seller/seller_list/'.$product->id?>" class="colorBlue">View <?php echo count($other_sellers);?> sellers starting from <i class="fa fa-inr"></i> <?php echo ($other_sellers[0]->saleprice > 0) ? $other_sellers[0]->saleprice : $other_sellers[0]->price;?></a></div>
                                            <?php
                                                }
                                            }
                                            ?>

                                        </div>



                                    </div>
                                </div>

                                <?php if($product->quantity < 4 && $product->quantity > 0){ ?>
                                <div class="red">
                                    Only <?php echo $product->quantity;?> Piece<?php echo ($product->quantity > 1)?'s':'';?> left
                                </div>
                                <?php } ?>


                            </form>


                            <div class="form-share">
                                <!--<div class="sendtofriend-print">
                                    <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                    <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                </div>
                                <div class="network-share">
                                </div>-->
                                <div class="socialShare">
                                    <a href="whatsapp://send?text=<?php echo $product->name.' '.site_url($product->slug);?>" class="btn btn-primary btn-sm whatsapp-btn visible-xs" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i> Share</a>
                                    <a href="https://www.facebook.com/sharer.php?u=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm facebook-btn open-popup"><i class="fa fa-facebook"></i> Share</a>
                                    <a href="https://plus.google.com/share?url=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm googlebtn open-popup"><i class="fa fa-google-plus"></i> Share</a>
                                    <a href="https://twitter.com/share?text=<?php echo $product->name;?>&url=<?php echo site_url($product->slug);?>" class="btn btn-primary btn-sm twitter-btn open-popup"><i class="fa fa-twitter"></i> Tweet</a>


                                </div>
                            </div>

                            <div class="descSection">
                                <div class="specification">
                                    <h3 class="s-h f-600">Specification</h3>
                                    <!--class="spItems-->
                                    <div class="pdlr-15 more">
                                        <?php echo $product->specification;?>
                                    </div>
                                    <!--<div class="showSpItems pdlr-15">

                                        <a href="javascript:;" class="f-700 colorBlue toggleSpecs"><span class="rm colorBlue ">Read More</span> <span class="sl hidden colorBlue ">Show Less</span></a>
                                    </div>-->
                                </div>
                                <div class="descSection">
                                    <div class="specification">
                                        <h3 class="s-h f-600">Description</h3>
                                        <!--class="descitems-->
                                        <div class="more pdlr-15">
                                            <?php echo $product->description; ?>
                                        </div>
                                        <!--<div class="showSpItems pdlr-15">

                                            <a href="javascript:;" class="f-700 colorBlue toggleDesc"><span class="rmdsc colorBlue ">Read More</span> <span class="sldsc hidden colorBlue ">Show Less</span></a>
                                        </div>-->
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>

                    <!-- tab product -->

                    <!-- ./tab product -->



                    <?php if(!empty($product->related_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">Similar Products</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "5" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":3},"1000":{"items":4}}'>

                                <?php foreach($product->related_products as $relate):?>
                                <li>
                                    <div class="product-container match-item">
                                        <div class="left-block">

                                            <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                <div class="group-price">
                                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                </div>
                                            <?php } ?>

                                            <a href="<?php echo site_url($relate->slug); ?>">
                                                <?php
                                                $photo  = theme_img('no_picture.png', lang('no_image_available'));


                                                $relate->images = array_values((array)json_decode($relate->images));

                                                if(!empty($relate->images[0]))
                                                {
                                                    $primary    = $relate->images[0];
                                                    foreach($relate->images as $photo)
                                                    {
                                                        if(isset($photo->primary))
                                                        {
                                                            $primary    = $photo;
                                                        }
                                                    }

                                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                }
                                                echo $photo;
                                                ?>
                                            </a>

                                            <div class="quick-view">
                                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->pm_id;?>" ></a>

                                                <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <?php if(time() > strtotime($relate->live_on)){ ?>
                                                    <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $relate->id;?> data-pm-id=<?php echo $relate->pm_id;?> >Add to Cart</a>
                                                <?php } else { ?>
                                                    <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>

                                            <div class="content_price">
                                                <?php if($relate->saleprice > 0):?>
                                                    <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                    <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php else: ?>
                                                    <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php endif; ?>

                                            </div>
                                            <?php if($relate->rating > 0){ ?>
                                                <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($relate->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                                </div>
                                            <?php } ?>
                                            <p class="fs-12 mt-10"> <span class="colorGreen">Offers:</span> <span class="spofr">Special Price & 1 More</span></p>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>
                    <?php if(!empty($product->related_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">Recently Viewed</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "5" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":3},"1000":{"items":4}}'>

                                <?php foreach($product->related_products as $relate):?>
                                    <li>
                                        <div class="product-container match-item">
                                            <div class="left-block">

                                                <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                    <div class="group-price">
                                                        <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                    </div>
                                                <?php } ?>

                                                <a href="<?php echo site_url($relate->slug); ?>">
                                                    <?php
                                                    $photo  = theme_img('no_picture.png', lang('no_image_available'));

                                                    /*$relate->images = array_values((array)json_decode($relate->images));

                                                    if($relate->images !== 'false'){
                                                        $relate->images    = array_values(json_decode($relate->images, true));
                                                    } else {
                                                        $relate->images    = json_decode($relate->images, true);
                                                    }*/

                                                    if(!empty($relate->images[0]))
                                                    {
                                                        $primary    = $relate->images[0];
                                                        foreach($relate->images as $photo)
                                                        {
                                                            if(isset($photo->primary))
                                                            {
                                                                $primary    = $photo;
                                                            }
                                                        }

                                                        $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                    }
                                                    echo $photo;
                                                    ?>
                                                </a>

                                                <div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->pm_id;?>" ></a>

                                                    <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                                </div>
                                                <div class="add-to-cart">
                                                    <?php if(time() > strtotime($relate->live_on)){ ?>
                                                        <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $relate->id;?> data-pm-id=<?php echo $relate->pm_id;?> >Add to Cart</a>
                                                    <?php } else { ?>
                                                        <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>

                                                <div class="content_price">
                                                    <?php if($relate->saleprice > 0):?>
                                                        <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                        <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                    <?php else: ?>
                                                        <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                    <?php endif; ?>

                                                </div>
                                                <?php if($relate->rating > 0){ ?>
                                                    <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($relate->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                                    </div>
                                                <?php } ?>
                                                <p class="fs-12 mt-10"> <span class="colorGreen">Offers:</span> <span class="spofr">Special Price & 1 More</span></p>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>
                    <?php if(!empty($product->related_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">Hot Selling</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "5" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":3},"1000":{"items":4}}'>

                                <?php foreach($product->related_products as $relate):?>
                                    <li>
                                        <div class="product-container match-item">
                                            <div class="left-block">

                                                <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                    <div class="group-price">
                                                        <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                    </div>
                                                <?php } ?>

                                                <a href="<?php echo site_url($relate->slug); ?>">
                                                    <?php
                                                    $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                                    //print_r($relate->images);exit;
                                                    //$relate->images = array_values((array)json_decode($relate->images));
                                                    /*if($relate->images !== false){
                                                        $relate->images    = array_values(json_decode($relate->images, true));
                                                    } else {
                                                        $relate->images    = json_decode($relate->images, true);
                                                    }*/

                                                    if(!empty($relate->images[0]))
                                                    {
                                                        $primary    = $relate->images[0];
                                                        foreach($relate->images as $photo)
                                                        {
                                                            if(isset($photo->primary))
                                                            {
                                                                $primary    = $photo;
                                                            }
                                                        }

                                                        $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                    }
                                                    echo $photo;
                                                    ?>
                                                </a>

                                                <div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->pm_id;?>" ></a>

                                                    <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                                </div>
                                                <div class="add-to-cart">
                                                    <?php if(time() > strtotime($relate->live_on)){ ?>
                                                        <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $relate->id;?> data-pm-id=<?php echo $relate->pm_id;?> >Add to Cart</a>
                                                    <?php } else { ?>
                                                        <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>

                                                <div class="content_price">
                                                    <?php if($relate->saleprice > 0):?>
                                                        <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                        <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                    <?php else: ?>
                                                        <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                    <?php endif; ?>

                                                </div>
                                                <?php if($relate->rating > 0){ ?>
                                                    <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($relate->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                                    </div>
                                                <?php } ?>
                                                <p class="fs-12 mt-10"> <span class="colorGreen">Offers:</span> <span class="spofr">Special Price & 1 More</span></p>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>

                    <?php if(!empty($category->special_products)):?>
                        <div class="page-product-box">
                            <h3 class="heading">You may also like</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "5" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":2},"600":{"items":3},"1000":{"items":4}}'>

                                <?php foreach($category->special_products as $relate):?>
                                <li>
                                    <div class="product-container match-item">
                                        <div class="left-block">

                                            <?php if((bool)$relate->track_stock && $relate->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                <div class="group-price">
                                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                </div>
                                            <?php } ?>

                                            <a href="<?php echo site_url($relate->slug); ?>">
                                                <?php
                                                $photo  = theme_img('no_picture.png', lang('no_image_available'));

                                                $relate->images = array_values((array)json_decode($relate->images));

                                                if(!empty($relate->images[0]))
                                                {
                                                    $primary    = $relate->images[0];
                                                    foreach($relate->images as $photo)
                                                    {
                                                        if(isset($photo->primary))
                                                        {
                                                            $primary    = $photo;
                                                        }
                                                    }

                                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$relate->seo_title.'"/>';
                                                }
                                                echo $photo;
                                                ?>
                                            </a>

                                            <div class="quick-view">
                                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $relate->id;?>" data-pm-id="<?php echo $relate->pm_id;?>" ></a>

                                                <a title="Quick view" class="search" href="<?php echo site_url($relate->slug); ?>"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <?php if(time() > strtotime($relate->live_on)){ ?>
                                                    <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $relate->id;?> data-pm-id=<?php echo $relate->pm_id;?> >Add to Cart</a>
                                                <?php } else { ?>
                                                    <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php echo site_url($relate->slug); ?>"><?php echo ucwords($relate->name);?></a></h5>

                                            <div class="content_price">
                                                <?php if($relate->saleprice > 0):?>
                                                    <span class="price product-price"><?php echo format_currency($relate->saleprice); ?></span>
                                                    <span class="price old-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php else: ?>
                                                    <span class="price product-price"><?php echo format_currency($relate->price); ?></span>
                                                <?php endif; ?>

                                            </div>
                                            <?php if($relate->rating > 0){ ?>
                                                <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($relate->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                                </div>
                                            <?php } ?>
                                            <p class="fs-12 mt-10"> <span class="colorGreen">Offers:</span> <span class="spofr">Special Price & 1 More</span></p>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;?>

                            </ul>
                        </div>
                    <?php endif;?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="rateSection">
                            <div class="specification">
                                <h3 class="s-h f-600 borderNone">
                                    Rating & Review
                                    <?php if($avg_rating->count > 0){ ?><span class="pull-right colorBlack rnum"><?php echo round($avg_rating->rating, 1);?> <i class="fa fa-star fs-20"></i> </span><?php } ?>
                                </h3>
                                <div class="pdlr-15 pdtb-10 rb brd-b-1 pos-r">
                                    <button type="button" class="btn btn-default rate-btn">Rate & Review Product</button>
                                    <?php if($avg_rating->count > 0){ ?><span class="pull-right trs"><?php echo $avg_rating->count;?> Ratings</span><?php } ?>
                                </div>
                                <div class="reviewItems pdlr-15">
                                    <div class="product-comments-block-tab">

                                        <?php
                                        $review_count = count($reviews);
                                        $review_shown_count = 1;
                                        if(!empty($reviews)){
                                            foreach ($reviews as $review) {
                                                ?>
                                                <div class="comment row review <?php echo ($review_count > 3 && $review_shown_count > 3) ?'hidden':'';?>">
                                                    <div class="col-sm-12 author">
                                                        <div class="grade">

                                                                    <span class="reviewRating">
                                                    <span class="badge badge-success rate-badge"><?php echo round($review->rating,1);?> <i class="fa fa-star"></i> </span>
                                                </span>
                                                            <span class="r-name"><strong>
                                                    <?php
                                                    if($review->customer_id != 0){
                                                        echo ucwords($review->firstname.' '.$review->lastname).' <i class="fa fa-check-circle colorGreen" data-toggle="tooltip" data-placement="top" title="Verified"></i>';
                                                    } else {
                                                        echo ucwords($review->pr_name);
                                                    }
                                                    ?>
                                                </strong></span>

                                                        </div>

                                                    </div>
                                                    <div class="col-sm-12 commnet-dettail">
                                                        <?php echo nl2br($review->review);?>
                                                    </div>
                                                    <div class="info-author col-sm-12">
                                                        <em><?php echo date('j M Y, g:i A', strtotime($review->created_at));?></em>
                                                    </div>

                                                </div>
                                                <?php
                                                $review_shown_count++;
                                            }
                                        }
                                        ?>


                                        <div class="br"></div>
                                        <!-- <a class="btn-comment" href="#">Write your review !</a> -->
                                        <div class="writeReview hidden">
                                            <h3 id="write-review">WRITE A REVIEW</h3>

                                            <div class="clearfix">
                                                <?php if(!$this->Customer_model->is_logged_in(false, false)):?>
                                                    <!-- You need to login to review this product.<br>
                                                    <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-red mt-12">Login Now</a> -->
                                                    <?php echo form_open('cart/add_review/1', 'class=""');?>
                                                    <div class="row" style="    padding: 10px 15px 0 15px;">
                                                        <div class="form-group col-sm-4 pl0xs"><input type="text" class="form-control" placeholder="Name" name="name"></div>
                                                        <div class="form-group col-sm-4 p0"><input type="email" class="form-control" placeholder="Email" name="email"></div>
                                                        <div class="form-group col-sm-4 pr0xs"><input type="text" class="form-control" placeholder="Phone" name="phone"></div>
                                                    </div>
                                                <?php else: ?>
                                                    <?php echo form_open('cart/add_review', 'class="form-horizontal"');?>
                                                <?php endif; ?>

                                                <input type="hidden" name="product_id" value="<?php echo $product->id?>" />
                                                <input type="hidden" name="pm_id" value="<?php echo $product->pm_id?>"/>

                                                <div class="form-group mlr0"><textarea id="review-textarea" name="review" rows="4" class="width-100 form-control" placeholder="Review"></textarea></div>

                                                <!--<input id="rating-input-price" name="rating_price" class="hidden form-control">
                                                <div> <span class="rsl">Price:</span> <span class="ratebox-input mt5" data-id="rating-input-price" data-rating="0"></span></div>

                                                <input id="rating-input-value" name="rating_value" class="hidden form-control">
                                                <div><span class="rsl">Value:</span> <span class="ratebox-input mt5" data-id="rating-input-value" data-rating="0"></span></div>

                                                <input id="rating-input-quality" name="rating_quality" class="hidden form-control">
                                                <div><span class="rsl">Quality:</span> <span class="ratebox-input mt5" data-id="rating-input-quality" data-rating="0"></span></div>-->
                                                <input id="rating-input-price"  type="range" min="0" max="5" value="0" step="0.5" name="rating_price" class="hidden form-control">
                                                <div> <span class="rsl">Price:</span> <span class="rateit mt5" data-rateit-backingfld="#rating-input-price"></span></div>

                                                <input id="rating-input-value" type="range" min="0" max="5" value="0" step="0.5" name="rating_value" class="hidden form-control">
                                                <div><span class="rsl">Value:</span> <span class="rateit mt5" data-rateit-backingfld="#rating-input-value"></span></div>

                                                <input id="rating-input-quality" type="range" min="0" max="5" value="0" step="0.5" name="rating_quality" class="hidden form-control">
                                                <div><span class="rsl">Quality:</span> <span class="rateit mt5" data-rateit-backingfld="#rating-input-quality"></span></div>


                                                <input type="submit" class="btn btn-red mt-12 mb-10" value="Submit Review">
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <?php if($review_count > 3){ ?>
                                    <div class="showSpItems pdlr-15">
                                        <a href="javascript:;" class="f-700 colorBlue" id="expand-reviews"><span class="colorBlue ">Read All Reviews</span> </a>
                                    </div>
                                <?php    } ?>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<div class="modal fade" id="hintModal" tabindex="-1" role="dialog" aria-labelledby="hintModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="hintModalLabel">Details</h4>
      </div>
      <div class="modal-body">
        <div id="hintDetails"></div>
      </div>
    </div>
  </div>
</div>

<?php echo theme_css('raterater.css', true);?>
<?php echo theme_js('raterater.jquery.js', true);?>
<script type="text/javascript" src="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.1/rateit.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.1/jquery.rateit.min.js"></script>
<style>
    .rateit .rateit-selected {
        background-position: left -32px!important;
    }
</style>
<script>
function ratingChanged(id, rating){
    console.log(id,rating);
    $('#'+id).val(rating);
    $('.rsl').click();
}
$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";


    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = '<div class="moreellipses">' +c +  ellipsestext+ '</div><div class="morecontent"><span>' + c + h + '</span><a href="" class="morelink f-700 colorBlue">' + moretext + '</a></div>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});

$(function(){
    $(window).on('resize scroll load',function(){
        var $el = $('.pb-right-column');
        if($($el).length){
            var scrollTop = $(this).scrollTop(),
                scrollBot = scrollTop + $(this).height(),
                elTop = $el.offset().top,
                elBottom = elTop + $el.outerHeight(),
                visibleTop = elTop < scrollTop ? scrollTop : elTop,
                visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;

            if($(window).width() > 767){
                var bennerowl = $('.banner-owl').owlCarousel();
                bennerowl.on(' initialized.owl.carousel',function(property){
                    var current = property.item.index;
                    console.log("init");
                    $(".owl-item").find("img").removeAttr("id");
                    $(property.target).find(".owl-item").eq(current).find("img").attr("id","zoomimage");

                    $('#zoomimage').ezPlus({
                        zoomType: 'inner',
                        cursor: 'crosshair'
                    });
                });
                $('.banner-owl').find('.owl-item.active').find("img").attr("id","zoomimage");

                $('#zoomimage').ezPlus({
                    zoomType: 'inner',
                    cursor: 'crosshair'
                });
                bennerowl.on('changed.owl.carousel', function(property) {
                    var current = property.item.index;
                    $(".owl-item").find("img").removeAttr("id");
                    $(property.target).find(".owl-item").eq(current).find("img").attr("id","zoomimage");

                    $('#zoomimage').ezPlus({
                        zoomType: 'inner',
                        cursor: 'crosshair'
                    });

                });
            }
            if( $(window).width()>991){







                if($(document).scrollTop() > 1 && ($('.pb-left-column').height()+150 <= (visibleBottom-visibleTop))){
                    var width = $('.pb-left-column').width();
                    $('.pb-left-column').css("position","fixed")
                        .css("top","146px").css("padding","0 15px").css('max-width','400px');
                    $('.pb-right-column').css("margin-left",width+30);
                }
                else{
                    $('.pb-left-column').css("position","relative").css("top","0px");
                    $('.pb-right-column').css("margin-left",0);
                }
            }

            if($('.page-product-box').length){
                if($('.page-product-box').offset().top-50 <= visibleBottom){
                    console.log($('.page-product-box').offset().top);
                    $('.adcrt').addClass('nocart');
                    $('.scroll_top').css('display','block');
                }
                else{
                    $('.adcrt').removeClass('nocart');
                    $('.scroll_top').css('display','none');
                }
            }

        }
    });

    $("#seeMoreSpecs").click(function() {
        $('html, body').animate({
            scrollTop: $(".descSection").offset().top-135
        }, 2000);
    });
    $('.moreOffers').on('click',function(){
        $('.offers').toggleClass('heightAuto');
        //$('.hideOffers').toggleClass('hidden');
        $('.moreOffers').toggleClass('hidden');
    });



    $('.toggleSpecs').on('click',function(){
        $('.spItems').toggleClass('full');
        $('.rm').toggleClass('hidden');
        $('.sl').toggleClass('hidden');
    });
    $('.toggleDesc').on('click',function(){
        $('.descitems').toggleClass('full');
        $('.rmdsc').toggleClass('hidden');
        $('.sldsc').toggleClass('hidden');
    });

    $('.rate-btn').on('click',function(){
        $('.writeReview').toggleClass('hidden');
    })
    $('.vertical-menu-content').css('display','none');

    /*$(".option-img").elevateZoom({
        zoomWindowPosition: 2,
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 500,
        lensFadeIn: 500,
        lensFadeOut: 500,
        zoomWindowWidth:200,
        zoomWindowHeight:200
    });*/

    $('#hintModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var hintHeading = button.data('heading');
        var hintDetails = button.data('details');
        var modal = $(this);
        modal.find('.modal-title').text(hintHeading);
        modal.find('.modal-body #hintDetails').html(hintDetails);
    });

    $('.ratebox').raterater( {
        submitFunction: 'rateAlert',
        allowChange: false,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
        isStatic: true,
    });

    $('.ratebox-input').raterater( {
        submitFunction: 'ratingChanged',
        allowChange: true,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
    });
    $('.rateit').rateit();

    $('.ratebox-input-seller').raterater( {
        submitFunction: 'ratingChanged',
        allowChange: true,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
    });

    $(document).on("click","#btn-pincode", function(e){
        e.preventDefault();
        $('#result-pincode').html('');
        $('#deliverytext').html('');
        var val = $('#input-pincode').val().trim();

        if(val){
            if(val.length == 6){
                $.ajax({
                    data : {
                        zip: val
                    },
                    url : '<?php echo site_url();?>cart/check_service_availability',
                    method: 'POST'
                }).done(function(getdata){
                    getdata = JSON.parse(getdata);
                    if (getdata.success) {
                        var pinresult = getdata.data+'<span class="fs-12 f-500 mb-0" id="deliverytext"></span>';
                        $('#result-pincode').html(pinresult);
                        $('#deliverytext').html('. Usually Delivers within 3-5 Days*');
                        var elem = $('#deliverytext');
                        var count = 1;
                        var intervalId = setInterval(function() {
                            if (elem.css('visibility') == 'hidden') {
                                elem.css('visibility', 'visible');
                                if (count++ === 5) {
                                    clearInterval(intervalId);
                                }
                            } else {
                                elem.css('visibility', 'hidden');
                            }
                        }, 500);
                    } else {
                        $('#result-pincode').html(getdata.data);
                    }
                });
            }
        }
    });

    setTimeout(function(){
        $('.match-item').matchHeight({
            byRow: false,
            property: 'height',
            target: null,
            remove: false
        });
    }, 500);

    $('.option-img-radio').on('change',function() {
        if($(this).val() != '') {
            var ez =   $('#product-zoom').data('elevateZoom');
            var smallImage = $(this).attr('data-smallImage');
            var largeImage = $(this).attr('data-largeImage');
            ez.swaptheimage(smallImage, largeImage);
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var el = $(e.target).attr('href');
        if(el == '#reviews'){
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover',
                placement: 'top',
                animate: true,
                delay: 500,
                container: 'body'
            });
        }
    });

    $('#expand-reviews').on('click',function(){
        $('.review').removeClass('hidden');
        $('#expand-reviews').addClass('hidden');
    });

    $("#review-textarea").emojioneArea({
        pickerPosition: "bottom",
        tonesStyle: "bullet",
        saveEmojisAs:'image'
    });

    $('.open-popup').on('click.open', function(e) {
        e.preventDefault();
        window.open($(this).attr('href'));
    });

    // ./countdown
    if($('#countDown').length){
        var $countDown = $('#countDown');
        var countDownDate = new Date($countDown.data('time')).getTime();
        var countDownTimer = setInterval(function() {
            var now = new Date().getTime();
            var distance = countDownDate - now;

            if (distance < 0) {
                clearInterval(countDownTimer);
                location.reload();
            }
            
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            var countString = "";
            if(days > 0){ countString += days+"d "; }
            if(hours > 0){
                countString += hours+"h:";
                countString += minutes+"m:";
                countString += seconds+"s";
            } else {
                if(minutes > 0){
                    countString += minutes+"m:";
                    countString += seconds+"s";
                } else {
                    if(seconds > 0){countString += seconds+"s"; }
                }
            }

            $countDown.html(countString);
        }, 1000);
    }

});
</script>
<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>



<div itemscope itemtype="http://schema.org/Product" class="hidden">
    <meta itemprop="description" content="<?php echo $product->excerpt; ?>">
    <link itemprop="url" href="<?php echo site_url($product->slug);?>" rel="author"/>
    <a itemprop="url" href="<?php echo site_url($product->slug);?>">
        <span itemprop="name" style="display:block;">
            <strong><?php echo $product->name;?></strong>
        </span>
    </a>
    <span itemscope itemtype="http://schema.org/Brand"  style="display:block;"><span itemprop="name"><?php echo $product->brand_name;?></span></span>
    <span style="display:block;">Product ID: <span itemprop="productID"><?php echo $product->pm_id?></span></span>
    <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <span itemprop="price">
            <?php echo $mainPrice;?>
        </span>
        <span itemprop="priceCurrency">INR</span>
    </span>
</div>

<?php
if(isset($_GET['review'])){
    if($_GET['review'] == 1 || $_GET['review'] == 2 || $_GET['review'] == 3 || $_GET['review'] == 4 || $_GET['review'] == 5){
        ?>
        <script type="text/javascript">
            $(function(){
                $reviewRate = "<?php echo $_GET['review'];?>";

                $('.nav-tab a[href="#reviews"]').tab('show');
                $('html, body').stop().animate({
                    'scrollTop': $('#write-review').offset().top-120
                }, 500);

                $('#rating-input-price').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-price"').attr('data-rating', $reviewRate);

                $('#rating-input-value').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-value"').attr('data-rating', $reviewRate);

                $('#rating-input-quality').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-quality"').attr('data-rating', $reviewRate);

                $('.ratebox-input').raterater( {
                    submitFunction: 'ratingChanged',
                    allowChange: true,
                    starWidth: 16,
                    spaceWidth: 1,
                    numStars: 5,
                });

            });
        </script>
        <?php
    }
}
?>