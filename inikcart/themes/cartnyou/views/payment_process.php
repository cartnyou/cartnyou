<center>
	<h2><font color="#3e96d3">Please wait while we are processing your request to payumoney...</font></h2>
	<h4>Do not press back or refresh the window.</h4>
</center>

<form action="<?php echo $data['action']; ?>" method="post" style="display:none" name="form">
<?php
foreach ($data as $key => $value) {
	echo '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
}
?>
</form>

<script type="text/javascript">
window.onload=function(){
    document.forms["form"].submit();
}
</script>