<?php echo theme_js('jquery.matchHeight-min.js', true);?>

<style>
    .product-list li:hover img {
        -webkit-transform: scale(1,1)!important;
        -webkit-transform-origin: inherit!important;
        -moz-transform: scale(1,1)!important;
        -moz-transform-origin: inherit!important;
        -o-transform: scale(1,1)!important;
        -o-transform-origin: top right!important;
        transform: scale(1,1)!important;
        transform-origin: inherit!important;
    }
    .tree-menu > li>span:before {
        position: absolute!important;
        left: 17px!important;
    }
</style>

<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><?php echo $category->name;?></span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <?php if ($this->session->flashdata('message')):?>
                    <div class="alert alert-info">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('message');?>
                    </div>
                <?php endif;?>
                
                <?php if ($this->session->flashdata('error')):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $this->session->flashdata('error');?>
                    </div>
                <?php endif;?>
                
                <?php if (!empty($error)):?>
                    <div class="alert alert-danger">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo $error;?>
                    </div>
                <?php endif;?>
            </div>

            <!-- Left colunm -->
            <div class="col-md-12 visible-xs">
                <h3 class="text-center mb10"><?php echo $category->name;?></h3>
                   
                <div class="row fs-container">
                    <div class="col-xs-6 p0"> 
                        <button class="mobile-filter-btn width-100 collapsed mb-10"  data-toggle="collapse" data-target="#left_column"><i class="fa fa-filter"></i> &nbsp;  Filter</button>
                    </div>
                    <div class="col-xs-6 p0"> 
                    <?php if(count($products) > 0):?>
                        <div class="sort-product pull-right">
                            <select class="basic nice-select sort-select" id="sort_products" onchange="sort_products($(this).val());">
                                <option value=''>Sort By</option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="name/asc"><?php echo lang('sort_by_name_asc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="name/desc"><?php echo lang('sort_by_name_desc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="price/asc"><?php echo lang('sort_by_price_asc');?></option>
                                <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="price/desc"><?php echo lang('sort_by_price_desc');?></option>
                            </select>
                        </div>
                    <?php endif;?>
                    </div>
                </div>
                
            </div>

            <div class="column col-xs-12 col-sm-3 filter-xs hide pd-0" id="left_column">
                <div class="text-left">
                    <h3 class="xsflth visible-xs" data-toggle="collapse" data-target="#left_column"><i   class="fa fa-close"></i> Filters</h3>
                </div>

                <!-- block category -->
                <div class="scrollY">


                <?php if(count($products) > 0): ?>
                    <?php if(isset($category)): ?>
                        <?php if(isset($this->categories[$category->id] ) && count($this->categories[$category->id]) > 0): ?>
                            <div class="block left-module">
                                <p class="title_block">Category Filter</p>
                                <div class="block_content">
                                    <!-- layered -->
                                    <div class="layered layered-category">
                                        <div class="layered-content">
                                            <ul class="tree-menu">

                                                <?php
                                                $all_categories = $this->categories;
                                                function get_category_tree($id,$all_categories,$base_url){
                                                    if(isset($all_categories[$id] ) && count($all_categories[$id]) > 0):
                                                        echo '<ul>';
                                                            foreach($all_categories[$id] as $subcategory):
                                                                echo '<li>';
                                                                    echo '<span></span><a href="'.site_url(implode('/', $base_url).'/'.$subcategory->slug).'">'.$subcategory->name.'</a>';
                                                                    get_category_tree($subcategory->id,$all_categories,$base_url);
                                                                echo '</li>';
                                                            endforeach;
                                                        echo '</ul>';
                                                    endif;
                                                }
                                                ?>
                                                
                                                <?php foreach($this->categories[$category->id] as $subcategory):?>
                                                    <li>
                                                        <span></span><a href="<?php echo site_url(implode('/', $base_url).'/'.$subcategory->slug); ?>"><?php echo $subcategory->name;?></a>
                                                        <?php get_category_tree($subcategory->id,$this->categories,$base_url);?>
                                                    </li>
                                                <?php endforeach;?>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- ./layered -->
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
                <!-- ./block category  -->
                <!-- block filter -->
                <div class="block left-module">
                    <p class="title_block">Filter selection</p>
                    <div class="block_content p15">
                        <!-- layered -->
                        <div class="layered layered-filter-price">

                            <div class="layered_subtitle">price</div>
                            <div class="layered-content slider-range">
                                <div data-label-reasult="" data-min="<?php echo $filters['min_price'];?>" data-max="<?php echo $filters['max_price'];?>" data-unit="<i class='fa fa-inr'></i>" class="slider-range-price" data-value-min="<?php echo $filters['min_price'];?>" data-value-max="<?php echo $filters['max_price'];?>"></div>
                                <div class="amount-range-price"><i class='fa fa-inr'></i><?php echo $filters['min_price'];?> - <i class='fa fa-inr'></i><?php echo $filters['max_price'];?></div>
                                <div class="priceText">
                                <span  id="afto">
                                    <input type="number" class="ptxt" id="min_price_filter" placeholder="Price From" value="<?php echo $filters['min_price'];?>" min="<?php echo $filters['min_price'];?>" max="<?php echo $filters['max_price'];?>">
                                </span>

                                    <input type="number" class="ptxt" id="max_price_filter" placeholder="Price To" value="<?php echo $filters['max_price'];?>" min="<?php echo $filters['min_price'];?>" max="<?php echo $filters['max_price'];?>">
                                    <a href="javascript:;" class="flink btn btn-primary yellow-btn"><i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>

                            <!-- ./filter price -->
                            <!-- filter color -->

                            <?php
                            if(!empty($filter_options)){
                                foreach ($filter_options as $filter_option_name => $filter_option) {
                                    if(!empty($filter_option)){
                                    ?>
                                    <div class="layered_subtitle"><?php echo $filter_option_name;?></div>
                                    <div class="layered-content <?php if(strtolower($filter_option_name) == 'colour' || strtolower($filter_option_name) == 'colours' || strtolower($filter_option_name) == 'color' || strtolower($filter_option_name) == 'colors'){ echo 'filter-color'; }?>">
                                        <ul class="check-box-list">

                                            <?php if(strtolower($filter_option_name) == 'colour' || strtolower($filter_option_name) == 'colours' || strtolower($filter_option_name) == 'color' || strtolower($filter_option_name) == 'colors'){ ?>
                                                
                                                <?php foreach ($filter_option as $filter_option_key => $filter_option_value) { ?>
                                                    <?php if($filter_option_value['value'] != ''){ ?>
                                                        <li>
                                                            <input id="filter-option-<?php echo $filter_option_name.'-'.$filter_option_value['value'];?>" <?php echo ($filter_option_value['checked']) ? 'checked' : '' ;?> onchange="selectFilterOption(this,'<?php echo $filter_option_name;?>','<?php echo $filter_option_value['value'];?>')" type="checkbox" name="<?php echo $filter_option_name;?>[]" value="<?php echo $filter_option_value['value'];?>" /> 
                                                            <?php
                                                            if (preg_match('/^#[a-f0-9]{6}$/i', $filter_option_value['image'])) {
                                                                $color_to_fill = $filter_option_value['image'];
                                                            } else {
                                                                $color_to_fill = $filter_option_key;;
                                                            }
                                                            ?>
                                                            <label title="<?php echo $filter_option_key;?>" style="background:<?php echo $color_to_fill;?>" for="filter-option-<?php echo $filter_option_name.'-'.$filter_option_value['value'];?>">
                                                                <span class="button"></span>
                                                                <?php //echo $filter_option_key;?>
                                                            </label>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>

                                            <?php } else { ?>

                                                <?php foreach ($filter_option as $filter_option_key => $filter_option_value) { ?>
                                                    <?php if($filter_option_value['value'] != ''){ ?>
                                                        <li>
                                                            <input id="filter-option-<?php echo $filter_option_name.'-'.$filter_option_value['value'];?>" <?php echo ($filter_option_value['checked']) ? 'checked' : '' ;?> onchange="selectFilterOption(this,'<?php echo $filter_option_name;?>','<?php echo $filter_option_value['value'];?>')" type="checkbox" name="<?php echo $filter_option_name;?>[]" value="<?php echo $filter_option_value['value'];?>" /> 
                                                            <label for="filter-option-<?php echo $filter_option_name.'-'.$filter_option_value['value'];?>">
                                                                <span class="button"></span>
                                                                <?php echo $filter_option_key;?>
                                                            </label>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>

                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php
                                    }
                                }
                            }
                            ?>
                            <!-- ./filter color -->
                            <!-- ./filter brand -->
                            <?php if(!empty($filters['brands'])){ ?>
                                <div class="layered_subtitle">Brand</div>
                                <div class="layered-content filter-brand">
                                    <ul class="check-box-list">
                                        <?php foreach($filters['brands'] as $brand){ ?>
                                            <li>
                                                <input type="checkbox" <?php if(in_array($brand->id, $filters['brand_checked'])){ echo 'checked'; }?> id="brand-<?php echo $brand->id;?>" name="cc" onchange="selectBrand(this,<?php echo $brand->id;?>)" />
                                                <label for="brand-<?php echo $brand->id;?>">
                                                <span class="button"></span>
                                                <?php echo $brand->name;?><!-- <span class="count">(<?php echo $brand->count;?>)</span> -->
                                                </label>   
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                            <!-- ./filter brand -->
                            <!-- ./filter size -->
                            <!-- <div class="layered_subtitle">Size</div>
                            <div class="layered-content filter-size">
                                <ul class="check-box-list">
                                    <li>
                                        <input type="checkbox" id="size1" name="cc" />
                                        <label for="size1">
                                        <span class="button"></span>X
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size2" name="cc" />
                                        <label for="size2">
                                        <span class="button"></span>XXL
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size3" name="cc" />
                                        <label for="size3">
                                        <span class="button"></span>XL
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size4" name="cc" />
                                        <label for="size4">
                                        <span class="button"></span>XXL
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size5" name="cc" />
                                        <label for="size5">
                                        <span class="button"></span>M
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size6" name="cc" />
                                        <label for="size6">
                                        <span class="button"></span>XXS
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size7" name="cc" />
                                        <label for="size7">
                                        <span class="button"></span>S
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size8" name="cc" />
                                        <label for="size8">
                                        <span class="button"></span>XS
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size9" name="cc" />
                                        <label for="size9">
                                        <span class="button"></span>34
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size10" name="cc" />
                                        <label for="size10">
                                        <span class="button"></span>36
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size11" name="cc" />
                                        <label for="size11">
                                        <span class="button"></span>35
                                        </label>   
                                    </li>
                                    <li>
                                        <input type="checkbox" id="size12" name="cc" />
                                        <label for="size12">
                                        <span class="button"></span>37
                                        </label>   
                                    </li>
                                </ul>
                            </div> -->
                            <!-- ./filter size -->
                        </div>
                        <!-- ./layered -->

                    </div>
                </div>
                <!-- ./block filter  -->
                
                <!-- left silide -->
                <?php
                if($category->side_banner != 0){
                    $this->banners->show_collection($category->side_banner, 6, 'category_side_slider');
                }
                ?>

                <!--./left silde-->
                <!-- SPECIAL -->
                <?php if(!empty($category->special_products)){ ;?>
                <div class="block left-module hidden">
                    <p class="title_block">SPECIAL PRODUCTS</p>
                    <div class="block_content">
                        <ul class="products-block">
                            <?php foreach ($category->special_products as $special_product) { ?>
                                <li>
                                    <div class="products-block-left">
                                        <a href="<?php echo site_url($special_product->slug);?>">
                                            <?php
                                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                            
                                            $special_product->images = array_values((array)json_decode($special_product->images));
                                    
                                            if(!empty($special_product->images[0]))
                                            {
                                                $primary    = $special_product->images[0];
                                                foreach($special_product->images as $photo)
                                                {
                                                    if(isset($photo->primary))
                                                    {
                                                        $primary    = $photo;
                                                    }
                                                }

                                                $photo  = '<img class="img img-responsive lazy" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$special_product->seo_title.'"/>';
                                            }
                                            echo $photo;
                                            ?>
                                        </a>
                                    </div>
                                    <div class="products-block-right">
                                        <p class="product-name">
                                            <a href="<?php echo site_url($special_product->slug);?>"><?php echo $special_product->name;?></a>
                                        </p>
                                        <p class="product-price">
                                            <?php if($special_product->saleprice > 0):?>
                                                <?php echo format_currency($special_product->saleprice); ?>
                                            <?php else: ?>
                                                <?php echo format_currency($special_product->price); ?>
                                            <?php endif; ?>
                                        </p>
                                        <?php if($special_product->rating > 0){ ?>
                                            <div class="product-star">
                                                <div class="clearfix ratebox" data-id="<?php echo $special_product->id;?>" data-rating="<?php echo $special_product->rating;?>"></div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- <div class="products-block">
                            <div class="products-block-bottom">
                                <a class="link-all" href="#">All Products</a>
                            </div>
                        </div> -->
                    </div>
                </div>
                <?php } ?>
                <!-- ./SPECIAL -->
                <!-- TAGS -->
                <!-- <div class="block left-module">
                    <p class="title_block">TAGS</p>
                    <div class="block_content">
                        <div class="tags">
                            <a href="#"><span class="level1">actual</span></a>
                            <a href="#"><span class="level2">adorable</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level4">consider</span></a>
                            <a href="#"><span class="level3">phenomenon</span></a>
                            <a href="#"><span class="level4">span</span></a>
                            <a href="#"><span class="level1">spanegs</span></a>
                            <a href="#"><span class="level5">spanegs</span></a>
                            <a href="#"><span class="level1">actual</span></a>
                            <a href="#"><span class="level2">adorable</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level4">consider</span></a>
                            <a href="#"><span class="level2">gives</span></a>
                            <a href="#"><span class="level3">change</span></a>
                            <a href="#"><span class="level2">gives</span></a>
                            <a href="#"><span class="level1">good</span></a>
                            <a href="#"><span class="level3">phenomenon</span></a>
                            <a href="#"><span class="level4">span</span></a>
                            <a href="#"><span class="level1">spanegs</span></a>
                            <a href="#"><span class="level5">spanegs</span></a>
                        </div>
                    </div>
                </div> -->
                <!-- ./TAGS -->
                <!-- Testimonials -->
                <!-- <div class="block left-module">
                    <p class="title_block">Testimonials</p>
                    <div class="block_content">
                        <ul class="testimonials owl-carousel" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplay="true" data-autoplayHoverPause = "true" data-items="1">
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> -->
                <!-- ./Testimonials -->
                </div>
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- category-slider -->
                <?php
                if($category->slider_banner != 0){
                    $this->banners->show_collection($category->slider_banner, 6, 'category_slider');
                }
                ?>
                <!-- ./category-slider -->

                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading mb10 hidden-xs">
                        <span class="page-heading-title"><?php echo $category->name;?></span>
                    
                        <?php if(count($products) > 0):?>
                            <div class="sort-product pull-right">
                                <select class="basic nice-select" id="sort_products" onchange="sort_products($(this).val());">
                                    <option value=''>Sort By</option>
                                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="name/asc"><?php echo lang('sort_by_name_asc');?></option>
                                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="name/desc"><?php echo lang('sort_by_name_desc');?></option>
                                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="price/asc"><?php echo lang('sort_by_price_asc');?></option>
                                    <option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="price/desc"><?php echo lang('sort_by_price_desc');?></option>
                                </select>
                            </div>
                        <?php endif;?>

                    </h2>
                    <!-- <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul> -->
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">
                        <?php if(count($products) == 0):?>
                            <h2 style="margin:50px 0px; text-align:center;">
                                <?php echo lang('no_products');?>
                            </h2>
                        <?php elseif(count($products) > 0):
                            $used = array();
                            foreach($products as $product):
                                if(!in_array($product->id, $used)){
                                    $used[] = $product->id;
                                } else {
                                    continue;
                                }
                                ?>
                                <li class="col-sx-12 col-sm-4 col-xs-6 col-md-3">
                                    <div class="product-container match-item" id="p-<?php echo $product->id; ?>">
                                        <div class="left-block">

                                            <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                                                <div class="group-price">
                                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                                </div>
                                            <?php } ?>
                                            
                                            <a href="<?php echo site_url($product->slug); ?>">


                                                    <?php

                                                    $photo  = theme_img('no_picture.png', lang('no_image_available'));

                                                    $product->images = array_values($product->images);

                                                    $no_image = (bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled');

                                                    if(!empty($product->images[0]))
                                                    {
                                                        if($this->isMobile){
                                                             echo '<ul>';
                                                        } else if(count($product->images) > 1){   
                                                            echo '<ul class="pruductSliderInner owl-carousel" data-autoplay="true" data-dots="true" data-loop="true" data-nav = "false" data-margin = "0" data-autoplayTimeout="100" data-autoplaySpeed="100"  data-autoplayHoverPause = "false"  data-responsive='.'{"0":{"items":1},"600":{"items":1},"1000":{"items":1}}'.'>';
                                                        } else {
                                                            echo '<ul>';
                                                        }

                                                        $primary    = $product->images[0];

                                                        foreach($product->images as $photo)
                                                        {

                                                            if(isset($photo->primary))
                                                            {
                                                                $primary    = $photo;
                                                            }

                                                            $photo  = '<img class="img img-responsive lazy'.($no_image?" greyImage":"").'" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$photo->filename).'" alt="'.$product->seo_title.'"/>';
                                                            echo '<li>'.$photo.'</li>';

                                                            if($this->isMobile) break;

                                                        }
                                                        echo '</ul>';
                                                    }
                                                    else {
                                                        echo '<ul><li>'.$photo.'</li></ul>';
                                                    }

                                                    ?>

                                            </a>

                                            <div class="quick-view">
                                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" ></a>

                                                <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <?php if(time() > strtotime($product->live_on)){ ?>
                                                    <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                                                <?php } else { ?>
                                                    <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>

                                            <div class="content_price">
                                                <?php if(($product->saleprice > 0) || ($product->offer_price > 0)):?>
                                                    <span class="price product-price"><?php echo format_currency(min(array_filter(array($product->saleprice, $product->offer_price)))); ?></span>
                                                    <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                                                <?php else: ?>
                                                    <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                                                <?php endif; ?>
                                                
                                            </div>
                                            <?php if($product->rating > 0){ ?>
                                                <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($product->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                                </div>
                                            <?php } ?>
                                            <?php if($product->offers_count>0){
                                                echo '<p class="fs-12 mt-10"><span class="colorGreen">'.$product->offers_count.' offer'.(($product->offers_count > 1)?'s':'').' available</span></p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </li>
                        <?php
                            endforeach;
                        endif;
                        ?>
                        
                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>

                <!-- ./view-product-list-->
                <div class="sortPagiBar">
                    <?php echo $this->pagination->create_links();?>
                </div>

                <div class="col-md-12 mt-20">
                    <?php
                    /*function shorten_string($string, $wordsreturned)
                    {
                      $retval = $string;
                      $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
                      $string = str_replace("\n", " ", $string);
                      $array = explode(" ", $string);
                      if (count($array)<=$wordsreturned)
                      {
                        $retval = $string;
                      }
                      else
                      {
                        array_splice($array, $wordsreturned);
                        $retval = implode(" ", $array)." ... <a href='javascript:;' id='show-more-category-description'>show more</a>";
                      }
                      return $retval;
                    }

                    echo shorten_string($category->description, 40);*/

                    /*$wcount = str_word_count($category->description);
                    if($wcount > 40){
                        echo $category->description.' <a href="javascript:;" id="show-more-category-description">Show more</a>';
                    } else {
                        echo $category->description;
                    }*/
                    echo $category->description;
                    ?>

                </div>

            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<?php echo theme_js('jqueryui.custom.min.js', true);?>
<?php echo theme_css('raterater.css', true);?>
<?php echo theme_js('raterater.jquery.js', true);?>

<script type="text/javascript">
    $(function(){


        /*$(document).on('mouseenter','.product-container',function(e){

            /!*$('.owl-carousel').attr('data-autoplay', true);*!/
            /!*$('.owl-carousel').trigger('play.owl.autoplay');*!/

            setTimeout(function(){
                $(this).find('.owl-carousel').trigger('play.owl.autoplay');
                console.log("mouseenter",$(this).closest('.owl-carousel').trigger('play.owl.autoplay'));
            },1100);


        });*/

        var NO_TARGET = 'none';
        var targetCalloutId = NO_TARGET;
        var inMouseEnterProcessing = false;
        var targetId = '';
        $(document).mousemove(function(e) {

            if (targetCalloutId == NO_TARGET) {
                console.log(targetCalloutId,"1",targetId);
                $('.owl-carousel').trigger('stop.owl.autoplay')
                return;
            }
            if (inMouseEnterProcessing) {
                console.log(targetCalloutId,"2",targetId);
                $('.owl-carousel').trigger('stop.owl.autoplay')
                return;
            }
             targetId = e.target.id;
            if (targetCalloutId == targetId) {
                console.log(targetCalloutId,"3",targetId);
                return;
            }
            var parents = $(e.target).parents("*");
            for (var i = parents.length - 1; i >= 0; i--) {
                var parent = parents[i];
                if (targetCalloutId == parent.id) {
                    //console.log('mouse over parent ' + targetCalloutId);
                    console.log(targetCalloutId,"4",targetId);
                    $('.owl-carousel').trigger('stop.owl.autoplay')
                    return;
                }
            }
            // DO THE MOUSE OUT EVENT PROCESSING
            console.log($('.owl-carousel').trigger('stop.owl.autoplay'));
            // RESET THE TARGET ID
            targetCalloutId = NO_TARGET;
        });
        setTimeout(function(){
            $('.owl-carousel').trigger('stop.owl.autoplay');
        },1000);
        function slider() {
            this.currentObj = {};
        }
        var tmout = null ;

        $(document).on('mouseenter','.product-container',function(e){
            if (inMouseEnterProcessing) return;
            inMouseEnterProcessing = true;
            // SET THE TARGET ID FOR MOUSE OUT EVENT DETECTION
             targetId = e.target.id;
            targetCalloutId = targetId;
            // DO THE MOUSE ENTER EVENT PROCESSING

            // CLEAR THE FLAG THAT INDICATES MOUSE MOVE EVENTS ARE IN THE
            // MOUSE ENTER EVENT PROCESSING
            inMouseEnterProcessing = false;
            var $var = $(this);

            slider.prototype.currentObj = $(this);
           /* if(tmout == null){

            }
            else{
                clearTimeout(tmout);
            }*/
            tmout = setTimeout(function($var){

                tmout = null;

                /*console.log("MouseEnter", slider.prototype.currentObj.find('.owl-carousel').trigger('play.owl.autoplay'));*/
                slider.prototype.currentObj.find('.owl-carousel').trigger('play.owl.autoplay')
            },1500);



        });

        $(document).on('mouseleave','.product-container',function(e){
             targetId = 'abcd';
            slider.prototype.currentObj = $(this);
            slider.prototype.currentObj.find('.owl-carousel').trigger('to.owl.carousel', 0);
            setTimeout(function(){

                console.log("MouseLeave",$('.owl-carousel').trigger('stop.owl.autoplay'));

                /* clearTimeout(tmout);*/


            },1500);
            /*console.log("mouseout",$('.owl-carousel').trigger('stop.owl.autoplay'));*/





        });

    });
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function sort_products(by){
    make_url_string('by',by);
}

function selectBrand(checkboxElem,id){
    var val = id;
    var qval = getParameterByName('brand');
    if (checkboxElem.checked) {
        if(qval){
            val = qval+','+id;
        }
    } else {
        if(qval){
            var qAr = qval.split(",");
            qAr = qAr.map(Number);
            qAr.remove(id);
            val = qAr.join();
        }
    }
    make_url_string('brand',val);
}

function selectFilterOption(checkboxElem, option_name, option_value){
    var val = option_value;
    var qval = getParameterByName('facets['+option_name+']');
    if (checkboxElem.checked) {
        if(qval){
            val = qval+','+option_value;
        }
    } else {
        if(qval){
            var qAr = qval.split(",");
            qAr.remove(option_value);
            val = qAr.join();
        }
    }
    make_url_string('facets['+option_name+']',val);
}

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
}

function updateQueryStringParameter(uri, key, value) {
    var final_str = uri;
    //var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    //var re = new RegExp("([?&])" + escape(key + "=") + ".*?(&|$)", "i");
    key = key.replace("[", "\\[");
    key = key.replace("]", "\\]");
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    key = key.replace("\\[", "[");
    key = key.replace("\\]", "]");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        if(JSON.parse(JSON.stringify(value).trim()) != ''){
            final_str = uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
            //final_str = uri.replace(re, '$1');
            final_str = uri.replace(re, '$1' + '$2');

            //return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
    } else {
        final_str = uri + separator + key + "=" + value;
        //return (uri + separator + key + "=" + value).replace('?&', '?');
    }

    if (final_str.endsWith('?')) {
        final_str = final_str.slice(0, -1);
    }
    final_str = final_str.replace('&&', '&');
    final_str = final_str.replace('?&', '?');
    if(final_str.indexOf("filters_changed=true") >0){
        return final_str;
    }
    else{
        return final_str+'&filters_changed=true';
    }

}

function make_url_string(field,value){
    var url = window.location.href;
    /*if(value == ''){
        window.location = removeURLParameter(url,field);
        return;
    }*/
    var filterChanged = "";
    if(url.indexOf("filters_changed=true") >0){
        filterChanged = "";
    }
    else{
        filterChanged = "&filters_changed=true";
    }
    if(url.indexOf("?") >= 0){

        
        if(url.indexOf('?' + field + '=') != -1){
            window.location = updateQueryStringParameter(url,field,value);
        } else if(url.indexOf('&' + field + '=') != -1){
            window.location = window.location = updateQueryStringParameter(url,field,value);
        } else {
            window.location = '<?php echo current_full_url();?>&'+field+'='+value+filterChanged;
        }

    } else {
        window.location = '<?php echo site_url(uri_string());?>/?'+field+'='+value+filterChanged;
    }
}
    function make_url_string_array(filterArray){

        var url = window.location.href;
        var param ="" ;
        var filterChanged = "";
        for(var i = 0; i<filterArray.length; i++){
            if(url.indexOf("?") >= 0){
                if(url.indexOf("filters_changed=true") >0){
                    filterChanged = "";
                }
                else{
                    filterChanged = "&filters_changed=true";
                }
                if(url.indexOf('?' + filterArray[i].field + '=') != -1){
                    url = updateQueryStringParameter(url,filterArray[i].field,filterArray[i].value);
                    console.log(url,'1');
                } else if(url.indexOf('&' + filterArray[i].field + '=') != -1){
                    url  = updateQueryStringParameter(url,filterArray[i].field,filterArray[i].value);
                    console.log(url,'2');
                } else {
                    //url = '<?php echo current_full_url();?>&'+filterArray[i].field+'='+filterArray[i].value+'&filters_changed=true';
                    url = url+'&'+filterArray[i].field+'='+filterArray[i].value+filterChanged;
                    console.log(url,'3');
                }
                //url = url + param;


            } else {
                if(url.indexOf("filters_changed=true") >0){
                    filterChanged = "";
                }
                else{
                    filterChanged = "&filters_changed=true";
                }
                url = '<?php echo site_url(uri_string());?>/?'+filterArray[i].field+'='+filterArray[i].value+filterChanged;
                console.log(url,'4');
                //url = url + param;
            }


        }
        //console.log(url);

        window.location =   url;
    }
    $('.flink').on('click',function(){

        var filter = [
            {'field':'min_price','value':$('#min_price_filter').val()},
            {'field':'max_price','value':$('#max_price_filter').val()}
        ];

        make_url_string_array(filter);

    });

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function(){
    $('.slider-range-price').each(function(){
            var min             = $(this).data('min');
            var max             = $(this).data('max');
            var unit            = $(this).data('unit');
            var value_min       = $(this).data('value-min');
            var value_max       = $(this).data('value-max');
            var label_reasult   = $(this).data('label-reasult');
            var t               = $(this);
            $( this ).slider({
              range: true,
              min: parseInt(min),
              max: parseInt(max),
              values: [ parseInt(value_min), parseInt(value_max) ],
              slide: function( event, ui ) {
                var result = label_reasult +" "+ unit + ui.values[ 0 ] +' - '+ unit +ui.values[ 1 ];
                t.closest('.slider-range').find('.amount-range-price').html(result);
              },
              change: function( event, ui ) {
                /*if(ui.values[1] < ui.values[0]){
                    ui.values[0] = 0;
                }*/
                console.log("ui values",ui.values[0]);
                if (ui.handle.nextSibling) {
                    make_url_string('min_price',ui.values[0]);
                } else {
                    make_url_string('max_price',ui.values[1]);
                }
              }
            });

            ;
        });
    if ($(window).width() < 768) {
        $('.filter-xs').addClass('collapse');
        $('.filter-xs').removeClass('hide');
    }
    else {
        $('.filter-xs').removeClass('collapse in');
        $('.filter-xs').removeClass('hide');
    }
    $(window).resize(function() {
        if ($(window).width() < 768) {
            $('.filter-xs').addClass('collapse');
            $('.filter-xs').removeClass('hide');
        }
        else {
            $('.filter-xs').removeClass('collapse in');
            $('.filter-xs').removeClass('hide');
        }
    });
});

$('.ratebox').raterater( { 
    submitFunction: 'rateAlert', 
    allowChange: false,
    starWidth: 16,
    spaceWidth: 1,
    numStars: 5,
    isStatic: true,
});

setTimeout(function(){
    $('.match-item').matchHeight({
        byRow: false,
        property: 'height',
        target: null,
        remove: false
    });
}, 500);

/*$("#show-more-category-description").on("click", function() {
    var $this = $(this); 
    var $content = $this.parent().;
    var linkText = $this.text().toUpperCase();    

    console.log($content.text());

    if(linkText === "SHOW MORE"){
        linkText = "show less";
        $content.switchClass("hideContent", "showContent", 400);
    } else {
        linkText = "show more";
        $content.switchClass("showContent", "hideContent", 400);
    };

    $this.text(linkText);
});*/
</script>