<?php

class Cart extends Front_Controller {

	function index()
	{
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['homepage']			= true;
		//$data['popular_products'] = $this->Product_model->popular_products(10);

		$data['featured_categories'] = $this->Category_model->get_featured_categories(0,10);

		$this->load->model('Brand_model');
		$brands = $this->Brand_model->get_brand_showcase(10);
		if(!empty($brands)){
			foreach ($brands as &$brand) {
				$brand->products = $this->Product_model->search_products('', 4, false, false, false,$filters=array('category'=>0,'brand_checked'=>array($brand->id)),false);
			}
		}
		$data['brands_showcase'] = $brands;
		
		$data['body_class'] = 'home';
		$this->view('homepage', $data);
	}

	function page($id = false)
	{
		//if there is no page id provided redirect to the homepage.
		$data['page']	= $this->Page_model->get_page($id);
		if(!$data['page'])
		{
			show_404();
		}
		$this->load->model('Page_model');
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;

		$data['page_title']			= $data['page']->title;
		
		$data['meta']				= $data['page']->meta;
		$data['seo_title']			= (!empty($data['page']->seo_title))?$data['page']->seo_title:$data['page']->title;
		
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('page', $data);
	}
	
	
	function search($code=false, $page = 0)
	{
		$this->load->model('Search_model');
		
		//check to see if we have a search term
		if(!$code)
		{
			//if the term is in post, save it to the db and give me a reference
			$term		= $this->input->post('term', true);
			$code		= $this->Search_model->record_term($term);
			
			// no code? redirect so we can have the code in place for the sorting.
			// I know this isn't the best way...
			redirect('cart/search/'.$code.'/'.$page);
		}
		else
		{
			//if we have the md5 string, get the term
			$term	= $this->Search_model->get_term($code);
		}
		
		if(empty($term))
		{
			//if there is still no search term throw an error
			$this->session->set_flashdata('error', lang('search_error'));
			redirect('cart');
		}

		$data['page_title']			= lang('search');
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		//fix for the category view page.
		$data['base_url']			= array();
		
		$sort_array = array(
							'name/asc' => array('by' => 'name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>false, 'sort'=>false);
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$data['page_title']	= lang('search');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
	
		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= base_url().'cart/search/'.$code.'/';
		$config['uri_segment']	= 4;
		$config['per_page']		= 24;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';


		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$brands = array();
		$filters['brand_checked'] = array();
		$filters['category'] = 0;

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['brand'])){
			$filters['brand_checked'] = explode(',',urldecode($_GET['brand']));
		}
		
		//$brands = $this->Product_model->get_products_brands($data['category']->id,true);

		$data['products'] = $this->Product_model->search_products($term, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		$config['total_rows']	= $this->Product_model->search_products($term, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);
		$this->pagination->initialize($config);

		//$brands_used = array();
		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}

			/*if(in_array($p->brand_id, $brands_used)){
				$brands[$p->brand_id]['count'] = $brands[$p->brand_id]['count'] + 1;
			} else {
				$brands[$p->brand_id] = array('name'=>$p->brand_name, 'count'=>1);
			}
			$brands_used[] = $p->brand_id;*/
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$filters['brands'] = $brands;

		$data['filters'] = $filters;

		$data['term'] = $term;
		$this->view('search', $data);
	}

	function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	function get_hot_search_keywords(){
		$this->load->model('Search_model');
		$result	= $this->Search_model->get_hot_search_keywords(20);

		$return_terms = array();
		if(!empty($result)){
			foreach ($result as &$row) {
				//if($this->isJson($row->term)){
				//	$row->term = json_decode($row->term)->term;
				//}
				//if(!$this->isJson($row->term)){
					if($row->term != ''){
						$return_terms[] = $row;
					}
				//}
			}
			$return['data'] = $return_terms;
			$return['success'] = TRUE;
		} else {
			$return['success'] = FALSE;
		}
		die(json_encode($return));
	}
	

	
	function category($id)
	{
		
		//get the category
		$data['category'] = $this->Category_model->get_category($id);
				
		if (!$data['category'] || $data['category']->enabled==0)
		{
			show_404();
		}
				
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['category']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$data['meta']		= $data['category']->meta;
		$data['seo_title']	= (!empty($data['category']->seo_title))?$data['category']->seo_title:$data['category']->name;
		$data['page_title']	= $data['category']->name;
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'sequence', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$brands = array();
		$filters['brand_checked'] = array();

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['brand'])){
			$filters['brand_checked'] = explode(',',urldecode($_GET['brand']));
		}
		
		$brands = $this->Product_model->get_products_brands($data['category']->id,true);

		$config['per_page']		= 24;
		
		$data['products']	= $this->Product_model->get_products($data['category']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		
		$product_count = $this->Product_model->get_products($data['category']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);

		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
				
		$this->pagination->initialize($config);

		//$brands_used = array();
		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}

			/*if(in_array($p->brand_id, $brands_used)){
				$brands[$p->brand_id]['count'] = $brands[$p->brand_id]['count'] + 1;
			} else {
				$brands[$p->brand_id] = array('name'=>$p->brand_name, 'count'=>1);
			}
			$brands_used[] = $p->brand_id;*/
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$filters['brands'] = $brands;

		$data['filters'] = $filters;

		$this->view('category', $data);
	}
	
	function product($id)
	{
		//get the product
		$data['product']	= $this->Product_model->get_product($id,true,'',true);

		$any_enabled = false;
		if($data['product']){
			foreach ($data['product'] as $pm) {
				if($pm->enabled==1){
					$any_enabled = true;
				}
			}
		} else {
			show_404();
		}
		if(!$any_enabled){
			show_404();
		}
		
		$data['base_url']			= $this->uri->segment_array();
		
		// load the digital language stuff
		$this->lang->load('digital_product');
		
		$data['options']	= $this->Option_model->get_product_options($data['product'][0]->id);
		
		$related			= $data['product'][0]->related_products;
		$data['related']	= array();
		

				
		$data['posted_options']	= $this->session->flashdata('option_values');

		$data['page_title']			= $data['product'][0]->name;
		$data['meta']				= $data['product'][0]->meta;
		$data['meta_description']	= $data['product'][0]->meta_description;
		$data['seo_title']			= (!empty($data['product'][0]->seo_title))?$data['product'][0]->seo_title:$data['product'][0]->name;
			
		if($data['product'][0]->images == 'false')
		{
			$data['product'][0]->images = array();
		}
		else
		{
			$data['product'][0]->images	= array_values((array)json_decode($data['product'][0]->images));
		}

		$data['gift_cards_enabled'] = $this->gift_cards_enabled;

		$data['reviews'] = $this->Product_model->get_product_reviews($id);

		$data['avg_rating'] = $this->Product_model->get_avg_rating($id);
		
		$this->view('product', $data);
	}
	
	
	function add_to_cart($ajax=false)
	{
		// Get our inputs
		$product_id		= $this->input->post('id');
		$quantity 		= $this->input->post('quantity');
		$post_options 	= $this->input->post('option');
		$pm_id		= $this->input->post('pm_id');
		
		// Get a cart-ready product array
		$product = $this->Product_model->get_cart_ready_product($product_id, $quantity,$pm_id);
		
		//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
		if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
		{
			$stock	= $this->Product_model->get_product($product_id);
			
			//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
			$items		= $this->go_cart->contents();
			$qty_count	= $quantity;
			foreach($items as $item)
			{
				if(intval($item['id']) == intval($product_id))
				{
					$qty_count = $qty_count + $item['quantity'];
				}
			}
			
			if($stock->quantity < $qty_count)
			{
				//we don't have this much in stock
				$this->session->set_flashdata('error', sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity));
				$this->session->set_flashdata('quantity', $quantity);
				$this->session->set_flashdata('option_values', $post_options);

				if($ajax){
					die(json_encode(array('success'=>false,'data'=>$this->Product_model->get_slug($product_id,$pm_id))));
				} else {
					redirect($this->Product_model->get_slug($product_id,$pm_id));
				}
			}
		}

		// Validate Options 
		// this returns a status array, with product item array automatically modified and options added
		//  Warning: this method receives the product by reference
		$status = $this->Option_model->validate_product_options($product, $post_options);
		
		// don't add the product if we are missing required option values
		if( ! $status['validated'])
		{
			$this->session->set_flashdata('quantity', $quantity);
			$this->session->set_flashdata('error', $status['message']);
			$this->session->set_flashdata('option_values', $post_options);
		
			redirect($this->Product_model->get_slug($product_id,$pm_id));

			if($ajax){
				die(json_encode(array('success'=>false,'data'=>$this->Product_model->get_slug($product_id,$pm_id))));
			} else {
				redirect($this->Product_model->get_slug($product_id,$pm_id));
			}
		
		} else {
		
			//Add the original option vars to the array so we can edit it later
			$product['post_options']	= $post_options;
			
			//is giftcard
			$product['is_gc']			= false;
			
			// Add the product item to the cart, also updates coupon discounts automatically
			$this->go_cart->insert($product);
		
			if($ajax){
				die(json_encode(array('success'=>true)));
			} else {
				// go go gadget cart!
				if($this->input->post('buy_now_redirect')){
					redirect('checkout');
				}
				redirect('cart/view_cart');
			}
		}
	}
	
	function view_cart()
	{
		
		$data['page_title']	= 'View Cart';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('view_cart', $data);
	}
	
	function remove_item($key)
	{
		//drop quantity to 0
		$this->go_cart->update_cart(array($key=>0));
		
		redirect('cart/view_cart');
	}
	
	function update_cart($redirect = false)
	{
		//if redirect isn't provided in the URL check for it in a form field
		if(!$redirect)
		{
			$redirect = $this->input->post('redirect');
		}
		
		// see if we have an update for the cart
		$item_keys		= $this->input->post('cartkey');
		$coupon_code	= $this->input->post('coupon_code');
		$gc_code		= $this->input->post('gc_code');
		
		if($coupon_code)
		{
			$coupon_code = strtolower($coupon_code);
		}
			
		//get the items in the cart and test their quantities
		$items			= $this->go_cart->contents();
		$new_key_list	= array();
		//first find out if we're deleting any products
		foreach($item_keys as $key=>$quantity)
		{
			if(intval($quantity) === 0)
			{
				//this item is being removed we can remove it before processing quantities.
				//this will ensure that any items out of order will not throw errors based on the incorrect values of another item in the cart
				$this->go_cart->update_cart(array($key=>$quantity));
			}
			else
			{
				//create a new list of relevant items
				$new_key_list[$key]	= $quantity;
			}
		}
		$response	= array();
		foreach($new_key_list as $key=>$quantity)
		{
			$product	= $this->go_cart->item($key);
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
			if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
			{
				$stock	= $this->Product_model->get_product($product['id']);
			
				//loop through the new quantities and tabluate any products with the same product id
				$qty_count	= $quantity;
				foreach($new_key_list as $item_key=>$item_quantity)
				{
					if($key != $item_key)
					{
						$item	= $this->go_cart->item($item_key);
						//look for other instances of the same product (this can occur if they have different options) and tabulate the total quantity
						if($item['id'] == $stock->id)
						{
							$qty_count = $qty_count + $item_quantity;
						}
					}
				}
				if($stock->quantity < $qty_count)
				{
					if(isset($response['error']))
					{
						$response['error'] .= '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
					}
					else
					{
						$response['error'] = '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
					}
				}
				else
				{
					//this one works, we can update it!
					//don't update the coupons yet
					$this->go_cart->update_cart(array($key=>$quantity));
				}
			}
			else
			{
				$this->go_cart->update_cart(array($key=>$quantity));
			}
		}
		
		//if we don't have a quantity error, run the update
		if(!isset($response['error']))
		{
			//update the coupons and gift card code
			$response = $this->go_cart->update_cart(false, $coupon_code, $gc_code);
			// set any messages that need to be displayed
		}
		else
		{
			$response['error'] = '<p>'.lang('error_updating_cart').'</p>'.$response['error'];
		}
		
		
		//check for errors again, there could have been a new error from the update cart function
		if(isset($response['error']))
		{
			$this->session->set_flashdata('error', $response['error']);
		}
		if(isset($response['message']))
		{
			$this->session->set_flashdata('message', $response['message']);
		}
		
		if($redirect == 'ajax'){
			die(json_encode(array('success'=>true,'data'=>$response)));
		} else if($redirect){
			redirect($redirect);
		} else {
			redirect('cart/view_cart');
		}
	}

	function get_cart_summary(){
		$this->partial('checkout/summary');
	}

	function get_cart_summary_total(){
		$this->partial('checkout/summary_total');
	}

	
	/***********************************************************
			Gift Cards
			 - this function handles adding gift cards to the cart
	***********************************************************/
	
	function giftcard()
	{
		if(!$this->gift_cards_enabled) redirect('/');
		
		// Load giftcard settings
		$gc_settings = $this->Settings_model->get_settings("gift_cards");
				
		$this->load->library('form_validation');
		
		$data['allow_custom_amount']	= (bool) $gc_settings['allow_custom_amount'];
		$data['preset_values']			= explode(",",$gc_settings['predefined_card_amounts']);
		
		if($data['allow_custom_amount'])
		{
			$this->form_validation->set_rules('custom_amount', 'lang:custom_amount', 'numeric');
		}
		
		$this->form_validation->set_rules('amount', 'lang:amount', 'required');
		$this->form_validation->set_rules('preset_amount', 'lang:preset_amount', 'numeric');
		$this->form_validation->set_rules('gc_to_name', 'lang:recipient_name', 'trim|required');
		$this->form_validation->set_rules('gc_to_email', 'lang:recipient_email', 'trim|required|valid_email');
		$this->form_validation->set_rules('gc_from', 'lang:sender_email', 'trim|required');
		$this->form_validation->set_rules('message', 'lang:custom_greeting', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['error']				= validation_errors();
			$data['page_title']			= lang('giftcard');
			$data['gift_cards_enabled']	= $this->gift_cards_enabled;
			$this->view('giftcards', $data);
		}
		else
		{
			
			// add to cart
			
			$card['price'] = set_value(set_value('amount'));
			
			$card['id']				= -1; // just a placeholder
			$card['sku']			= lang('giftcard');
			$card['base_price']		= $card['price']; // price gets modified by options, show the baseline still...
			$card['name']			= lang('giftcard');
			$card['code']			= generate_code(); // from the string helper
			$card['excerpt']		= sprintf(lang('giftcard_excerpt'), set_value('gc_to_name'));
			$card['weight']			= 0;
			$card['quantity']		= 1;
			$card['shippable']		= false;
			$card['taxable']		= 0;
			$card['fixed_quantity'] = true;
			$card['is_gc']			= true; // !Important
			$card['track_stock']	= false; // !Imporortant
			
			$card['gc_info'] = array("to_name"	=> set_value('gc_to_name'),
									 "to_email"	=> set_value('gc_to_email'),
									 "from"		=> set_value('gc_from'),
									 "personal_message"	=> set_value('message')
									 );
			
			// add the card data like a product
			$this->go_cart->insert($card);
			
			redirect('cart/view_cart');
		}
	}

	function add_review(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('product_id', 'Product', 'required');
		$this->form_validation->set_rules('review', 'Review', 'trim|required');
		$this->form_validation->set_rules('rating', 'Rating', 'numeric');
		
		$product_id		= $this->input->post('product_id');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {
			$customer	= $this->go_cart->customer();

			if(isset($customer['id'])){
				$review 		= $this->input->post('review');
				$rating 		= $this->input->post('rating');

				$this->Product_model->add_review($product_id,$review,$rating,$customer['id']);
				$this->session->set_flashdata('message', 'Your review has been submitted.');
			} else {
				$this->session->set_flashdata('error', 'You must login to submit a review.');
			}	
		}
		
		$pm_id		= $this->input->post('pm_id');
		redirect($this->Product_model->get_slug($product_id,$pm_id));
	}

	function contact_us_post(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('query', 'Query', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {

			$this->load->library('email');
			$config_mail = Array(
	        	'protocol'  => 'smtp',
	        	'smtp_host' => 'ssl://smtp.zoho.com',
	        	'smtp_port' => '465',
	        	'smtp_user' => 'reachus@gojojo.com',
	        	'smtp_pass' => 'qwerty1!',
	        	'mailtype'  => 'html',
		    	'newline'   => "\r\n"
	        );
			$this->email->initialize($config_mail);

			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$email = $this->input->post('email');
			$query = $this->input->post('query');

			$content  = '<b>Name:</b> '.$name;
			$content .= '<br><b>Phone:</b> '.$phone;
			$content .= '<br><b>Email:</b> '.$email;
			$content .= '<br><b>Query:</b> '.$query;

			$this->email->from($email, $name);
			$this->email->to($this->config->item('email'));
			$this->email->subject('gojojo - Contact Us');
			$this->email->message(html_entity_decode($content));
			$this->email->send();

			$this->session->set_flashdata('message', 'Your query has been submitted. We will revert you ASAP.');
		}
		redirect(site_url('contact-us'));
	}

	function careers_post(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {

			$config['upload_path']          = './uploads/resume/';
            $config['allowed_types']        = 'doc|docx|pdf';
	        $config['encrypt_name'] = TRUE;
	        
	        $this->load->library('upload', $config);

	        if ( ! $this->upload->do_upload('resume')){
	            $this->session->set_flashdata('error', $this->upload->display_errors());
	        } else {
	            $upload_data = $this->upload->data();
	            $file_name = $upload_data['file_name'];

	            $this->load->library('email');
				$config_mail = Array(
		        	'protocol'  => 'smtp',
		        	'smtp_host' => 'ssl://smtp.zoho.com',
		        	'smtp_port' => '465',
		        	'smtp_user' => 'reachus@gojojo.com',
		        	'smtp_pass' => 'qwerty1!',
		        	'mailtype'  => 'html',
		    		'newline'   => "\r\n"
		        );
				$this->email->initialize($config_mail);

				$name = $this->input->post('name');
				$phone = $this->input->post('phone');
				$email = $this->input->post('email');
				$resume = site_url('uploads/resume/'.$file_name);

				$content  = '<b>Name:</b> '.$name;
				$content .= '<br><b>Phone:</b> '.$phone;
				$content .= '<br><b>Email:</b> '.$email;
				$content .= '<br><b>Resume:</b> '.$resume;

				$this->email->from($email, $name);
				$this->email->to($this->config->item('email'));
				$this->email->subject('gojojo - Careers');
				$this->email->message(html_entity_decode($content));
				$this->email->send();

				$this->session->set_flashdata('message', 'Your resume has been sent. We will connect with you in short time.');
	        }	
		}
		redirect(site_url('careers'));
	}

	function popular_products($html=false,$category=false){
		$products = $this->Product_model->popular_products(20,false,$category);

		if(count($products) > 0){
			if(!$html){
				die(json_encode(array('result'=>true,'data'=>$products)));
			} else {
				$used = array();
				foreach($products as $product):
					if(!in_array($product->id, $used)){
						$used[] = $product->id;
					} else {
						continue;
					}
					?>
	                <li class="owl-item match-item">
                        <div class="left-block">

                            <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                                <div class="group-price">
                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                </div>
                            <?php } ?>
                            
                            <a href="<?php echo site_url($product->slug); ?>">
                                <?php
                                $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                
                                $product->images = array_values((array)json_decode($product->images));
                        
                                if(!empty($product->images[0]))
                                {
                                    $primary    = $product->images[0];
                                    foreach($product->images as $photo)
                                    {
                                        if(isset($photo->primary))
                                        {
                                            $primary    = $photo;
                                        }
                                    }

                                    $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                }
                                echo $photo;
                                ?>
                            </a>

                            <div class="quick-view">
                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                                <a title="Add to compare" class="compare add-to-compare" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                                <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                            </div>
                            <div class="add-to-cart">
                                <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                            </div>
                        </div>
                        <div class="right-block">
                            <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>
                            <?php if($product->rating > 0){ ?>
                                <div class="product-star">
                                    <div class="clearfix ratebox" data-id="<?php echo $product->id;?>" data-rating="<?php echo $product->rating;?>"></div>
                                </div>
                            <?php } ?>
                            <div class="content_price">
                                <?php if($product->saleprice > 0):?>
                                    <span class="price product-price"><?php echo format_currency($product->saleprice); ?></span>
                                    <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                                <?php else: ?>
                                    <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                                <?php endif; ?>
                                
                            </div>
                        </div>
	                </li>
	            <?php endforeach;
			}
		} else{
			if(!$html){
				die(json_encode(array('result'=>false)));
			} else {
				echo 'No Product Found.';
			}
		}
	}

	function sale_products($category=false){
		$products = $this->Product_model->sale_products(20,$category);

		if(count($products) > 0){
			$used = array();
			foreach($products as $product):
				if(!in_array($product->id, $used)){
					$used[] = $product->id;
				} else {
					continue;
				}
				?>
                <li class="owl-item match-item">
                    <div class="left-block">

                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                            <div class="group-price">
                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                            </div>
                        <?php } ?>
                        
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            $product->images = array_values((array)json_decode($product->images));
                    
                            if(!empty($product->images[0]))
                            {
                                $primary    = $product->images[0];
                                foreach($product->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                            }
                            echo $photo;
                            ?>
                        </a>

                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                            <a title="Add to compare" class="compare add-to-compare" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                        </div>
                        <div class="add-to-cart">
                            <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>
                        <?php if($product->rating > 0){ ?>
	                        <div class="product-star">
	                            <div class="clearfix ratebox" data-id="<?php echo $product->id;?>" data-rating="<?php echo $product->rating;?>"></div>
	                        </div>
	                    <?php } ?>
                        <div class="content_price">
                            <?php if($product->saleprice > 0):?>
                                <span class="price product-price"><?php echo format_currency($product->saleprice); ?></span>
                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                            <?php else: ?>
                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </li>
            <?php endforeach;
		} else {
			echo 'No Product Found.';
		}
	}

	function new_products($category=false){
		$products = $this->Product_model->new_products(20,$category);

		if(count($products) > 0){
			$used = array();
			foreach($products as $product):
				if(!in_array($product->id, $used)){
					$used[] = $product->id;
				} else {
					continue;
				}
				?>
                <li class="owl-item match-item">
                    <div class="left-block">

                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                            <div class="group-price">
                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                            </div>
                        <?php } ?>
                        
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            $product->images = array_values((array)json_decode($product->images));
                    
                            if(!empty($product->images[0]))
                            {
                                $primary    = $product->images[0];
                                foreach($product->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img class="img img-responsive" src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                            }
                            echo $photo;
                            ?>
                        </a>

                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                            <a title="Add to compare" class="compare add-to-compare" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->id;?>" ></a>
                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                        </div>
                        <div class="add-to-cart">
                            <a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>
                        <?php if($product->rating > 0){ ?>
                            <div class="product-star">
                                <div class="clearfix ratebox" data-id="<?php echo $product->id;?>" data-rating="<?php echo $product->rating;?>"></div>
                            </div>
                        <?php } ?>
                        <div class="content_price">
                            <?php if($product->saleprice > 0):?>
                                <span class="price product-price"><?php echo format_currency($product->saleprice); ?></span>
                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                            <?php else: ?>
                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </li>
            <?php endforeach;
		} else {
			echo 'No Product Found.';
		}
	}

	public function get_mini_cart(){
		$return = array();
		$return['items'] = $this->go_cart->total_items();
		$return['items_string'] = $this->go_cart->total_items().' item';
		if($this->go_cart->total_items()>1){
			$return['items_string'] = $return['items_string'].'s';
		}
		$return['total'] = format_currency($this->go_cart->total());
		$return['items_total_string'] = $return['items_string'].' - '.$return['total'];
		die(json_encode($return));
	}
}