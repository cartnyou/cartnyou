<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payumoney
{
	var $CI;
	
	//this can be used in several places
	var	$method_name;
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
		$this->CI->lang->load('payumoney');
		
		$this->method_name	= lang('payumoney');
	}
	
	/*
	checkout_form()
	this function returns an array, the first part being the name of the payment type
	that will show up beside the radio button the next value will be the actual form if there is no form, then it should equal false
	there is also the posibility that this payment method is not approved for this purchase. in that case, it should return a blank array 
	*/
	
	//these are the front end form and check functions
	function checkout_form($post = false)
	{
		$settings	= $this->CI->Settings_model->get_settings('payumoney');
		$enabled	= $settings['enabled'];
		
		$form			= array();
		if($enabled)
		{
			$form['name'] = $this->method_name;
			
			$form['form'] = $this->CI->load->view('payumoney_checkout', array(), true);
			
			return $form;
			
		} else return array();
		
	}
	
	
	function checkout_check()
	{
		// Nothing to check in this module
		return false;
	}
	
	function description()
	{
		return lang('payumoney');
	}
	
	//back end installation functions
	function install()
	{
		
		$config['merchant_key'] = '';
		$config['merchant_salt'] = '';;
		//$config['currency'] = 'INR'; // default
		
		$config['enabled'] = "0";
		
		//not normally user configurable
		$config['return_url'] = "checkout/payumoney_return";
		$config['cancel_url'] = "checkout/payumoney_cancel";

		$this->CI->Settings_model->save_settings('payumoney', $config);
	}
	
	function uninstall()
	{
		$this->CI->Settings_model->delete_settings('payumoney');
	}
	
	//payment processor
	function process_payment($args)
	{
		//print_r($args);exit;
		if ( $settings = $this->CI->Settings_model->get_settings('payumoney') ) 
		{
			$PAYU_BASE_URL = 'https://secure.payu.in';

			$args['key'] = $settings['merchant_key'];
			
			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

			$args['service_provider'] = 'payu_paisa';
			$args['txnid'] = $txnid;

			$hash = '';
			// Hash Sequence
			$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

			if(
			          empty($args['key'])
			          || empty($args['txnid'])
			          || empty($args['amount'])
			          || empty($args['firstname'])
			          || empty($args['email'])
			          || empty($args['productinfo'])
					  || empty($args['service_provider'])
			) {
			  	// failure;
			    return lang('payumoney_error');

			} else {
			    $hashVarsSeq = explode('|', $hashSequence);
			    $hash_string = '';	
				foreach($hashVarsSeq as $hash_var) {
			      $hash_string .= isset($args[$hash_var]) ? $args[$hash_var] : '';
			      $hash_string .= '|';
			    }

			    $hash_string .= $settings['merchant_salt'];


			    $hash = strtolower(hash('sha512', $hash_string));
			    $action = $PAYU_BASE_URL . '/_payment';

			    $args['action'] = $action;
			    $args['hash'] = $hash;

			    $this->CI->session->set_userdata("paymentDataArray",$args);

			    //redirect(site_url('payumoney_gate/payment_process'));
			    //redirect(site_url('checkout/payment_process'));
			    redirect(site_url($args['processor_function']));

				//redirect($action.'?'.http_build_query($args));
			}

		}
		else
		{
			return lang('payumoney_error');
		}	
	}
	
	//admin end form and check functions
	function form($post	= false)
	{
		//this same function processes the form
		if(!$post)
		{
			$settings	= $this->CI->Settings_model->get_settings('payumoney');
		}
		else
		{
			$settings = $post;
		}
		//retrieve form contents
		return $this->CI->load->view('payumoney_form', array('settings'=>$settings), true);
	}
	
	function check()
	{	
		$error	= false;

		if($error)
		{
			return $error;
		}
		else
		{
			//we save the settings if it gets here
			$this->CI->Settings_model->save_settings('payumoney', $_POST);
			
			return false;
		}
	}
}
