<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure, you want to delete this brand?');
}
</script>

<div style="text-align:right">
	<a class="btn" href="<?php echo site_url($this->config->item('admin_folder').'/brands/form'); ?>"><i class="icon-plus-sign"></i> Add New Brand</a>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Name</th>
			<th>Type</th>
			<th><?php echo lang('enabled');?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($brands)) {
			foreach ($brands as $brand) { ?>
				<tr>
					<td><?php echo  $brand->name; ?></td>
					<td><?php echo  $brand->type; ?></td>
					<td><?php echo ($brand->enabled == '1') ? lang('enabled') : lang('disabled'); ?></td>
					<td>
						<div class="btn-group" style="float:right">

							<a class="btn" href="<?php echo  site_url(ADMIN_FOLDER.'/brands/form/'.$brand->id);?>"><i class="icon-pencil"></i> <?php echo lang('edit');?></a>

							<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/brands/delete/'.$brand->id);?>" onclick="return areyousure();"><i class="icon-trash icon-white"></i> <?php echo lang('delete');?></a>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="4">No Brand available</td></tr>';
		}
		?>
	</tbody>
</table>