<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><?php echo $page->title;?></span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->

            <!--
            <div class="column col-xs-12 col-sm-3" id="left_column">

                <?php
                $footer_pages_general = array();
                $footer_pages_support = array();
                $footer_pages_other = array();
                if(isset($this->pages[0])) {
                    foreach ($this->pages[0] as $menu_page) {
                        if($menu_page->type == 'general'){
                            $footer_pages_general[] = array('id'=>$menu_page->id,'slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        } else if($menu_page->type == 'support'){
                            $footer_pages_support[] = array('id'=>$menu_page->id,'slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        } else if($menu_page->type == 'other'){
                            $footer_pages_other[] = array('id'=>$menu_page->id,'slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        }
                        if(isset($this->pages[$menu_page->id])) {
                            foreach($this->pages[$menu_page->id] as $sub_page){
                                if($menu_page->type == 'general'){
                                    $footer_pages_general[] = array('id'=>$menu_page->id,'slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                } else if($menu_page->type == 'support'){
                                    $footer_pages_support[] = array('id'=>$menu_page->id,'slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                } else if($menu_page->type == 'other'){
                                    $footer_pages_other[] = array('id'=>$menu_page->id,'slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                }
                            }
                        }
                    }
                }
                ?>

                <div class="block left-module">
                    <p class="title_block">Infomation</p>
                    <div class="block_content">
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php
                                    if(!empty($footer_pages_general)){
                                        foreach ($footer_pages_general as $footer_page) { ?>
                                            <li <?php echo ($footer_page['id']==$page->id) ? 'class="active"' : '';?> ><span></span><a href="<?php echo site_url($footer_page['slug']);?>" <?php if($footer_page['new_window'] ==1){echo 'target="_blank"';}?> ><?php echo $footer_page['menu_title'];?></a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block left-module">
                    <p class="title_block">Support</p>
                    <div class="block_content">
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <?php
                                    if(!empty($footer_pages_support)){
                                        foreach ($footer_pages_support as $footer_page) { ?>
                                            <li <?php echo ($footer_page['id']==$page->id) ? 'class="active"' : '';?> ><span></span><a href="<?php echo site_url($footer_page['slug']);?>" <?php if($footer_page['new_window'] ==1){echo 'target="_blank"';}?> ><?php echo $footer_page['menu_title'];?></a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            -->

            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-12" id="center_column">
                <!-- page heading-->
                <h2 class="page-heading">
                    <span class="page-heading-title2"><?php echo $page->title;?></span>
                </h2>
                <!-- Content page -->
                
                <div style="margin-top: 15px;">
                    <?php if ($this->session->flashdata('message')):?>
                        <div class="alert alert-info">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    <?php endif;?>
                    
                    <?php if ($this->session->flashdata('error')):?>
                        <div class="alert alert-danger">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $this->session->flashdata('error');?>
                        </div>
                    <?php endif;?>
                    
                    <?php if (!empty($error)):?>
                        <div class="alert alert-danger">
                            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                            <?php echo $error;?>
                        </div>
                    <?php endif;?>
                </div>

                <div class="content-text clearfix">
                    <?php echo str_replace('{{SITEURL}}', site_url(), str_replace('{{THEMEASSETS}}', theme_assets(), $page->content)); ?>

                </div>
                <!-- ./Content page -->
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>