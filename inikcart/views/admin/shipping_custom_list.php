<script type="text/javascript">
function areyousure()
{
	return confirm('Are you sure, you want to delete?');
}
</script>

<?php if(isset($successCount)):?>
	<div class="alert alert-success">
		<?php echo $successCount;?> Products Uploaded
	</div>
<?php endif;?>

<div class="row">
	<div class="span12">
		<?php echo form_open_multipart($this->config->item('admin_folder').'/shipping/custom_list', 'class="form-horizontal"');?>
			<label>Service Name *</label>
			<input name="company" type="text" class="form-control">
			<label>Service Code *</label>
			<input name="company_code" type="text" class="form-control">
			<h5>Data CSV File <span style="color: #F00;">* (Only CSV File will work)</span></h5>
			<input type="file" name="file" placeholder="CSV File" /><br><br>
			<input type="submit" class="btn btn-success" name="submit" value="Submit" />
		</form>
		<a href="<?php echo site_url('uploads/sample_custom_pincode.csv');?>">Download Sample CSV</a><br><br>
	</div>
</div>

<?php if(count($shipping_modules) >0): ?>
	<hr>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>S.No.</th>
				<th>Name</th>
				<th>Code</th>
				<th>Pincodes</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($shipping_modules as $k=>$module): ?>
			<tr>
				<td><b><?php echo $k+1;?></b></td>
				<td><?php echo humanize($module->company); ?></td>
				<td><?php echo $module->company_code; ?></td>
				<td><?php echo $module->pincodes; ?></td>
				<td>
					<a class="btn btn-danger" href="<?php echo site_url($this->config->item('admin_folder').'/shipping/custom_delete/'.$module->company_code);?>" onclick="return areyousure();"><i class=" icon-trash icon-white"></i> Delete</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>