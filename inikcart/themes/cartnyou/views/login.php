<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
        </div>
        <h2 class="page-heading">
            <span class="page-heading-title2">My Account</span>
        </h2>
        <div class="page-content">
            <div class="row">

            	<div class="col-md-12">
            		<?php if ($this->session->flashdata('message')):?>
						<div class="alert alert-info">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('message');?>
						</div>
					<?php endif;?>
					
					<?php if ($this->session->flashdata('error')):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $this->session->flashdata('error');?>
						</div>
					<?php endif;?>
					
					<?php if (!empty($error)):?>
						<div class="alert alert-danger">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<?php echo $error;?>
						</div>
					<?php endif;?>
				</div>

                <div class="col-sm-6">
                    <div class="box-authentication">
                        <h3>ALREADY REGISTERED! LOGIN</h3>

                        <?php echo form_open('secure/login', 'class="login_form"'); ?>
	                        <label for="emmail_login">Email</label>
	                        <input id="emmail_login" name="email" type="text" class="form-control">
	                        <label for="password_login">Password</label>
	                        <input id="password_login" name="password" type="password" class="form-control">

							<input class="hidden" name="remember" value="true" type="checkbox" />

	                        <p class="forgot-pass"><a href="<?php echo site_url('secure/forgot-password'); ?>">Forgot your password?</a></p>
	                        <button class="button" type="submit"><i class="fa fa-lock"></i> Sign in</button>

	                        <input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
							<input type="hidden" value="submitted" name="submitted"/>
						</form>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="box-authentication">
                    	<h3>NEW CUSTOMERS</h3>
						<p>Register with us to enjoy seemless experience, including:</p>
						<ul>
							<li>—  Online Order Status</li>
							<li>—  Sign up to receive exclusive news and offers</li>
							<li>—  Quick and easy checkout</li>
						</ul>
                        <a class="btn btn-theme-grey mt16" href="<?php echo site_url('secure/register'); ?>"><i class="fa fa-user"></i> Create an account</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>