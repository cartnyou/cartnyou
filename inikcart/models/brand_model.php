<?php
Class Brand_model extends CI_Model{

    function get_brands(){
        $this->db->select('id, name, enabled,type');
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get('brands');
        return $result->result();
    }

    function get_brand($id){
        $this->db->where('id',$id);
        $result = $this->db->get('brands');
        return $result->row();
    } 

    function brand_autocomplete($name, $limit){
        return $this->db->like('name', $name)->get('brands', $limit)->result();
    }

    function request($save){
        $this->db->where('brand_id', $save['brand_id']);
        $this->db->where('merchant_id', $save['merchant_id']);
        $count = $this->db->count_all_results('merchant_brands');

        $return = array();
        if ($count > 0){
            $return['success'] = false;
            $return['data'] = 'Already registered to this brand.';
        } else {
            $this->db->insert('merchant_brands', $save);
            $this->db->insert_id();
            $return['success'] = true;
            $return['data'] = '';
        }
        return $return;
    }

    function save($brand){
        if ($brand['id'])
        {
            $this->db->where('id', $brand['id']);
            $this->db->update('brands', $brand);
            
            return $brand['id'];
        } else {
            $this->db->insert('brands', $brand);
            return $this->db->insert_id();
        }
    }

    function get_brand_showcase($limit=10){
        $this->db->where('enabled', 1);
        return $this->db->get('brands', $limit)->result();
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('brands');
    }

}