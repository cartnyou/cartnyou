			</div>
		</div>
	</div>
</div>

	<!--[if lt IE 9]>
	<script src="<?php echo base_url('assets/global/plugins/respond.min.js');?>"></script>
	<script src="<?php echo base_url('assets/global/plugins/excanvas.min.js');?>"></script> 
	<script src="<?php echo base_url('assets/global/plugins/ie8.fix.min.js');?>"></script> 
	<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/js.cookie.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url('assets/global/plugins/jquery.blockui.min.js');?>" type="text/javascript"></script> -->
    <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <?php if(!$this->auth->is_merchant_logged_in(false, false)):?>
	    <!-- BEGIN PAGE LEVEL PLUGINS -->
	    <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js');?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/global/plugins/backstretch/jquery.backstretch.min.js');?>" type="text/javascript"></script>
	<?php else: ?>
		<!-- BEGIN PAGE LEVEL PLUGINS -->
	    <script src="<?php echo base_url('assets/global/plugins/moment.min.js');?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');?>" type="text/javascript"></script>
	    <!-- <script src="<?php echo base_url('assets/global/plugins/fullcalendar/fullcalendar.min.js');?>" type="text/javascript"></script> -->
	    <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.min.js');?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.resize.min.js');?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.categories.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/global/plugins/flot/jquery.flot.pie.min.js');?>" type="text/javascript"></script>
	   <!-- END PAGE LEVEL PLUGINS -->
    <?php endif;?>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url('assets/global/scripts/app.min.js');?>" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url('assets/global/plugins/bootstrap-toastr/toastr.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/pages/scripts/ui-toastr.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/global/plugins/bootbox/bootbox.min.js');?>" type="text/javascript"></script>

    <?php if(!$this->auth->is_merchant_logged_in(false, false)):?>
	    <script src="<?php echo base_url('assets/pages/scripts/login-5.min.js');?>" type="text/javascript"></script>
	<?php else: ?>
		<script src="<?php echo base_url('assets/layouts/layout/scripts/layout.min.js');?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/layouts/layout/scripts/demo.min.js');?>" type="text/javascript"></script>
	<?php endif;?>


            <div class="modal fade" id="returnImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel">Product Images</h4>
                        </div>
                        <div class="modal-body">
                           <div class="row">
                               <div class="col-md-6">
                                   <img src="<?php echo base_url('assets/img/logo.png'); ?>"
                                        alt="">
                               </div>
                               <div class="col-md-6">
                                   <img src="<?php echo base_url('assets/img/logo.png'); ?>"
                                        alt="">
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="priceBreakModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center fw-b" id="myModalLabel">Price Breakup</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row mb-20">
                               <div class="col-md-12">
                                   <h3 class="pname">Apple iPhone 6S Plus Tempered Glass Screen Guard by GOJOJO</h3>
                                   <p class="m-0">SKU: <span>5656</span></p>
                               </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Inventory</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Selling Price</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon"  data-toggle="modal" data-target="#rateCard"><i class="fa fa-calculator cp"></i></span>
                                    </div>
                                    <p class="smallFont">Price at which the SUPC will be listed on Gojojo.com</p>
                                </div>
                                <div class="col-md-8 col-sm-10 col-sm-offset-1 bkgGrey col-md-offset-2 mtb-10 collapse" id="rateCard">
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Net Seller Payable (NSP):</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <label>Snapdeal Margin Amount</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                           <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Collection Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Logistic Cost</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Fulfillment Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Processing Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Packaging Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Special Packaging Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>

                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Service Tax</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6">
                                            <label>Swachh Bharat Cess</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5 ">
                                        <div class="col-md-6">
                                            <label>Krishi Kalyan Cess</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5 ">
                                        <div class="col-md-6">
                                            <label><strong>Selling Price</strong></label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><strong><i class="fa fa-inr"></i>186.60</strong></span>
                                        </div>
                                    </div>
                                    <div class="row mtb-10 text-right">
                                        <button class="btn btn-primary">Save</button>
                                        <button class="btn btn-danger">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Net Seller Payable (NSP):</label>
                                </div>
                                <div class="col-md-6">
                                    <span class="pull-left"><i class="fa fa-inr"></i> 512</span>
                                    <span class="pull-right"><a href="javascript:;" data-toggle="modal" data-target="#breakup">Show Breakup</a></span>
                                </div>
                            </div>
                            <div class="row mtb-10 collapse" id="breakup">
                                <div class="col-md-8 col-sm-10 col-sm-offset-1 bkgGrey col-md-offset-2 mtb-10">
                                    <div class="row mt-15">
                                        <div class="col-md-6 text-left">
                                            <label>Snapdeal Margin Amount (21.85%)</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Collection Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Logistic Cost</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Fulfillment Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Processing Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Packaging Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Special Packaging Charges</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>

                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Service Tax</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5">
                                        <div class="col-md-6 text-left">
                                            <label>Swachh Bharat Cess</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5 ">
                                        <div class="col-md-6 text-left">
                                            <label>Krishi Kalyan Cess</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><i class="fa fa-inr"></i>186.60</span>
                                        </div>
                                    </div>
                                    <div class="row mtb-5 ">
                                        <div class="col-md-6 text-left">
                                            <label><strong>Net Seller Payable (NSP):</strong></label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <span><strong><i class="fa fa-inr"></i>186.60</strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Tax Class</label>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control">
                                        <option value="">1</option>
                                        <option value="">2</option>

                                    </select>
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Tax rate</label>
                                </div>
                                <div class="col-md-6">
                                   14.3%
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Minimum Selling Price (MSP):</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                        <input type="text" value="214" class="form-control">
                                    </div>
                                    <p class="smallFont">Selling Price update below this amount will be restricted.</p>
                                </div>
                            </div>
                            <div class="row mtb-10">
                                <div class="col-md-4">
                                    <label>Maximum Retail Price (MRP):</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                        <input type="text" value="214" class="form-control">
                                    </div>
                                    <p class="smallFont">Selling Price update above this amount will be restricted.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <form action="<?php echo site_url('merchant/shipping/complete_payment'); ?>" method="post" style="display:none" name="form" id="recharge-form">
                <input type="hidden" name="amount" id="amount-field" />
            </form>

            <script type="text/javascript">
                $(document).ready(function(){
                    $('.recharge').on('click', function(){
                        bootbox.prompt({
                            title: "Upgrade Your Shipping Limit",
                            inputType: 'select',
                            inputOptions: [
                                {
                                    text: 'Please choose the amount to recharge your account...',
                                    value: '',
                                },
                                {
                                    text: 'Rs. 200',
                                    value: '200',
                                },
                                {
                                    text: 'Rs. 500',
                                    value: '500',
                                },
                                {
                                    text: 'Rs. 2500',
                                    value: '2500',
                                },
                                {
                                    text: 'Rs. 5000',
                                    value: '5000',
                                },
                                {
                                    text: 'Rs. 10000',
                                    value: '10000',
                                },
                                {
                                    text: 'Rs. 25000',
                                    value: '25000',
                                },
                                {
                                    text: 'Rs. 50000',
                                    value: '50000',
                                },
                                {
                                    text: 'Rs. 100000',
                                    value: '100000',
                                }
                            ],
                            buttons: {
                                confirm: {
                                    label: 'Recharge',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if((parseInt(result)) > 0){
                                    $('#amount-field').val(result);
                                    $('#recharge-form').submit();
                                    //window.location = "<?php echo site_url($this->config->item('merchant_folder').'/shipping/complete_payment/');?>"+"/"+result;
                                    //       var data = [];
                                    // 		var url = "<?php echo site_url($this->config->item('merchant_folder').'/shipping/add_transaction');?>";
                                    // 		data.push({name: 'amount', value: result});
                                    // 		$.ajax({
                                    // 	data : data,
                                    // 	url : url,
                                    // 	method: 'POST'
                                    // }).done(function(getdata){
                                    // 	getdata = JSON.parse(getdata);
                                    // 	if(getdata.success){
                                    // 		location.reload();
                                    // 	} else {
                                    // 		toastr.error(getdata.data, "Oops...");
                                    // 	}
                                    // });
                                }
                            }
                        });
                    });
                });
            </script>
</body>
</html>