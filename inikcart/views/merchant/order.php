<div class="row">
    <div class="col-md-12">

    	<?php
        $waybill        = '';
        $waybill_method = '';
        foreach($order->contents as $product):
			if($product['waybill']){
				$waybill        = $product['waybill'];
                $waybill_method = $product['waybill_method'];
				break;
			}
        endforeach;
        ?>

        <?php if($waybill != ''){ ?>
        <div class="btn-group pull-left">
        	<h4>Waybill: <?php echo $waybill.' ('.$waybill_method.')';?></h4>
        </div>
        <?php } ?>

        <div class="btn-group pull-right">
            
            <!-- <a class="btn btn-sm btn-info" title="<?php echo lang('send_email_notification');?>" onclick="$('#notification_form').slideToggle();"><i class="fa fa-envelope"></i> <?php echo lang('send_email_notification');?></a> -->
            
            <?php if($waybill != ''){ ?>
            	<a class="btn btn-sm btn-warning" href="<?php echo site_url('merchant/orders/invoice/'.$order->id);?>" target="_blank"><i class="fa fa-file-text-o"></i> Invoice</a>
	        	<a class="btn btn-sm btn-danger" href="<?php echo site_url('merchant/orders/packing_slip/'.$order->id);?>" target="_blank"><i class="fa fa-file"></i> <?php echo lang('packing_slip');?></a>
        	<?php } else { ?>
        		<a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#shipping-modal" href="javascript:;">Generate Pickup</a>
        	<?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#content_editor').redactor({
        minHeight: 200,
        imageUpload: 'http://labs.gocartdv.com/gc2test/admin/wysiwyg/upload_image',
        fileUpload: 'http://labs.gocartdv.com/gc2test/admin/wysiwyg/upload_file',
        imageGetJson: 'http://labs.gocartdv.com/gc2test/admin/wysiwyg/get_images',
        imageUploadErrorCallback: function(json)
        {
            alert(json.error);
        },
        fileUploadErrorCallback: function(json)
        {
            alert(json.error);
        }
    }); 
});

// store message content in JS to eliminate the need to do an ajax call with every selection
var messages = <?php
    $messages   = array();
    foreach($msg_templates as $msg)
    {
        $messages[$msg['id']]= array('subject'=>$msg['subject'], 'content'=>$msg['content']);
    }
    echo json_encode($messages);
    ?>;
//alert(messages[3].subject);
// store customer name information, so names are indexed by email
var customer_names = <?php 
    echo json_encode(array(
        $order->email=>$order->firstname.' '.$order->lastname,
        $order->ship_email=>$order->ship_firstname.' '.$order->ship_lastname,
        $order->bill_email=>$order->bill_firstname.' '.$order->bill_lastname
    ));
?>;
// use our customer names var to update the customer name in the template
function update_name()
{
    if($('#canned_messages').val().length>0)
    {
        set_canned_message($('#canned_messages').val());
    }
}

function set_canned_message(id)
{
    // update the customer name variable before setting content 
    $('#msg_subject').val(messages[id]['subject'].replace(/{customer_name}/g, customer_names[$('#recipient_name').val()]));
    $('#content_editor').redactor('insertHtml', messages[id]['content'].replace(/{customer_name}/g, customer_names[$('#recipient_name').val()]));
}   
</script>

<div id="notification_form" class="row" style="display:none;">
    <div class="col-md-12">
        <?php echo form_open($this->config->item('merchant_folder').'/orders/send_notification/'.$order->id);?>
            <fieldset>
                <label><?php echo lang('message_templates');?></label>
                <select id="canned_messages" onchange="set_canned_message(this.value)" class="col-md-12">
                    <option><?php echo lang('select_canned_message');?></option>
                    <?php foreach($msg_templates as $msg)
                    {
                        echo '<option value="'.$msg['id'].'">'.$msg['name'].'</option>';
                    }
                    ?>
                </select>

                <label><?php echo lang('recipient');?></label>
                <select name="recipient" onchange="update_name()" id="recipient_name" class='col-md-12'>
                    <?php 
                        if(!empty($order->email))
                        {
                            echo '<option value="'.$order->email.'">'.lang('account_main_email').' ('.$order->email.')';
                        }
                        if(!empty($order->ship_email))
                        {
                            echo '<option value="'.$order->ship_email.'">'.lang('shipping_email').' ('.$order->ship_email.')';
                        }
                        if($order->bill_email != $order->ship_email)
                        {
                            echo '<option value="'.$order->bill_email.'">'.lang('billing_email').' ('.$order->bill_email.')';
                        }
                    ?>
                </select>

                <label><?php echo lang('subject');?></label>
                <input type="text" name="subject" size="40" id="msg_subject" class="col-md-12"/>

                <label><?php echo lang('message');?></label>
                <textarea id="content_editor" name="content"></textarea>

                <div class="form-actions">
                    <input type="submit" class="btn btn-primary" value="<?php echo lang('send_message');?>" />
                </div>
            </fieldset>
        </form>
    </div>
</div>

<div class="row" style="margin-top:10px;">
    <div class="col-md-4">
        <h3><?php echo lang('account_info');?></h3>
        <p>
        <?php echo (!empty($order->company))?$order->company.'<br>':'';?>
        <?php echo $order->firstname;?> <?php echo $order->lastname;?> <br/>
        <?php echo $order->email;?> <br/>
        <?php echo $order->phone;?>
        </p>
    </div>
    <!-- <div class="col-md-4">
        <h3><?php echo lang('billing_address');?></h3>
        <?php echo (!empty($order->bill_company))?$order->bill_company.'<br/>':'';?>
        <?php echo $order->bill_firstname.' '.$order->bill_lastname;?> <br/>
        <?php echo $order->bill_address1;?><br>
        <?php echo (!empty($order->bill_address2))?$order->bill_address2.'<br/>':'';?>
        <?php echo $order->bill_city.', '.$order->bill_zone.' '.$order->bill_zip;?><br/>
        <?php echo $order->bill_country;?><br/>
        
        <?php echo $order->bill_email;?><br/>
        <?php echo $order->bill_phone;?>
    </div> -->
    <div class="col-md-4">
        <h3><?php echo lang('shipping_address');?> <a href="javascript:;" id="edit-address-btn"><i class="fa fa-pencil"></i></a></h3>
        <?php echo (!empty($order->ship_company))?$order->ship_company.'<br/>':'';?>
        <?php echo $order->ship_firstname.' '.$order->ship_lastname;?> <br/>
        <?php echo $order->ship_address1;?><br>
        <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
        <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
        <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
        <?php echo $order->ship_country;?><br/>
        
        <?php echo $order->ship_email;?><br/>
        <?php echo $order->ship_phone;?>
    </div>
</div>

<div class="row" style="margin-top:20px;">
    <div class="col-md-4">
        <h3><?php echo lang('order_details');?></h3>
        <p>
        <?php if(!empty($order->referral)):?>
            <strong><?php echo lang('referral');?>: </strong><?php echo $order->referral;?><br/>
        <?php endif;?>
        <?php if(!empty($order->is_gift)):?>
            <strong><?php echo lang('is_gift');?></strong>
        <?php endif;?>
        
        <?php if(!empty($order->gift_message)):?>
            <strong><?php echo lang('gift_note');?></strong><br/>
            <?php echo $order->gift_message;?>
        <?php endif;?>
        </p>
    </div>
    <div class="col-md-4">
        <h3><?php echo lang('payment_method');?></h3>
        <p><?php echo $order->payment_info; ?></p>
    </div>
    <div class="col-md-4">
        <h3><?php echo lang('shipping_details');?></h3>
        <?php echo $order->shipping_method; ?>
        <?php if(!empty($order->shipping_notes)):?><div style="margin-top:10px;"><?php echo $order->shipping_notes;?></div><?php endif;?>
    </div>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th><?php echo lang('name');?></th>
            <th><?php echo lang('description');?></th>
            <th><?php echo lang('price');?></th>
            <th><?php echo lang('quantity');?></th>
            <th><?php echo lang('total');?></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $subtotal           = 0;
        $coupon_discount    = 0;
        $shipping           = 0;
        $total              = 0;
        $coupon_used        = '';
        ?>
        <?php foreach($order->contents as $orderkey=>$product): ?>

            <?php
            $subtotal           += $product['price']*$product['quantity'];
            //$coupon_discount  += $product['total_coupon_discount'];
            if(!isset($product['coupon_discount'])){
                $product['coupon_discount'] = 0;
            }
            $coupon_discount    += $product['coupon_discount']*$product['quantity'];
            $shipping           += $product['shipping'];
            $total              += $product['total'] - ($product['coupon_discount']*$product['quantity']);

            if(isset($product['coupon_code'])){
                $coupon_used = $product['coupon_code'];
            }
            ?>
        <tr>
            <td>
                <?php echo ucwords($product['name']);?>
                <?php echo (trim($product['sku']) != '')?'<br/><small>'.lang('sku').': '.$product['sku'].'</small>':'';?>
                
            </td>
            <td>
                <?php //echo $product['excerpt'];?>
                <?php
                
                // Print options
                if(isset($product['options']))
                {
                    foreach($product['options'] as $name=>$value)
                    {
                        $name = explode('-', $name);
                        $name = trim($name[0]);
                        if(is_array($value))
                        {
                            echo '<div>'.$name.':<br/>';
                            foreach($value as $item)
                            {
                                echo '- '.$item.'<br/>';
                            }   
                            echo "</div>";
                        }
                        else
                        {
                            echo '<div>'.$name.': '.$value.'</div>';
                        }
                    }
                }
                
                if(isset($product['gc_status'])) echo $product['gc_status'];
                ?>
            </td>
            <td><?php echo format_currency($product['price']);?></td>
            <td><?php echo $product['quantity'];?></td>
            <td><?php echo format_currency($product['price']*$product['quantity']);?></td>
            <td>
            	<a class="btn btn-primary btn-xs update-status-btn" data-order-item-id="<?php echo $product['order_item_id'];?>">Update Status</a><br>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
        <?php if($order->coupon_discount > 0):?>
        
        <tr>
            <td><strong><?php echo lang('subtotal');?></strong></td>
            <td colspan="3"></td>
            <td><?php echo format_currency($subtotal); ?></td>
        </tr>
        
        <tr>
            <td><strong><?php echo lang('coupon_discount');?> <?php echo ($coupon_used != '') ? '('.$coupon_used.')' : '' ;?></strong></td>
            <td colspan="3"></td>
            <td><?php echo format_currency(0-$coupon_discount); ?></td>
        </tr>
        <?php endif;?>

        <?php 
        $charges = @$order->custom_charges;
        if(!empty($charges))
        {
            foreach($charges as $name=>$price) : ?>
                
        <tr>
            <td><strong><?php echo $name?></strong></td>
            <td colspan="3"></td>
            <td><?php echo format_currency($price); ?></td>
        </tr>   
                
        <?php endforeach;
        }
        ?>
        <tr>
            <td><strong><?php echo lang('shipping');?></strong></td>
            <td colspan="3"><?php echo $order->shipping_method; ?></td>
            <td><?php echo format_currency($shipping); ?></td>
        </tr>
        
        <!-- <tr>
            <td><strong><?php echo lang('tax');?></strong></td>
            <td colspan="3"></td>
            <td><?php echo format_currency($order->tax); ?></td>
        </tr> -->
        <?php if($order->gift_card_discount > 0):?>
        <tr>
            <td><strong><?php echo lang('giftcard_discount');?></strong></td>
            <td colspan="3"></td>
            <td><?php echo format_currency(0-$order->gift_card_discount); ?></td>
        </tr>
        <?php endif;?>
        <tr>
            <td><h3><?php echo lang('total');?></h3></td>
            <td colspan="3"></td>
            <td><strong><?php echo format_currency(($subtotal+$shipping)-$coupon_discount); ?></strong></td>
        </tr>
    </tfoot>
</table>

<div id="status-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Status</h4>
      </div>
      <div class="modal-body">

        <?php echo form_open($this->config->item('merchant_folder').'/orders/status_update/'.$order->id, 'class="form-inline"');?>
            <input type="hidden" name="item_id" id="status-order-item-id">
            <fieldset>
                <div class="row">
                    <div class="col-md-8">
                        <h3>Add Note</h3>
                        <textarea name="notes" class="form-control" style="width: 100%;"><?php echo $order->notes;?></textarea>
                    </div>

                
                    <div class="col-md-4">
                        <h3><?php echo lang('status');?></h3>
                        <?php
                        echo form_dropdown('status', $this->config->item('order_statuses'), $order->status, 'class="form-control"');
                        ?>
                        
                    </div>
                </div>
                
                <div class="form-actions">
                    <input type="submit" class="btn btn-primary" value="<?php echo lang('update_order');?>"/>
                </div>
            </fieldset>
        </form>

        <hr>
        <div id="previous_statuses">
        </div>

      </div>
    </div>
  </div>
</div>

<div id="edit-address-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Address</h4>
      </div>
      <div class="modal-body">

        <?php echo form_open($this->config->item('merchant_folder').'/orders/address_update/'.$order->id, 'class="form-horizontal" id="edit-address-form"');?>
            <div class="form-group">
                <label>Firstname</label>
                <input type="text" name="firstname" id="firstname" class="form-control">
            </div>
            <div class="form-group">
                <label>Lastname</label>
                <input type="text" name="lastname" id="lastname" class="form-control">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" id="phone" class="form-control">
            </div>
            <div class="form-group">
                <label>Address Line 1</label>
                <input type="text" name="address1" id="address1" class="form-control">
            </div>
            <div class="form-group">
                <label>Address Line 2</label>
                <input type="text" name="address2" id="address2" class="form-control">
            </div>
            <div class="form-group">
                <label>Landmark</label>
                <input type="text" name="landmark" id="landmark" class="form-control">
            </div>
            <div class="form-group">
                <label>City</label>
                <input type="text" name="city" id="city" class="form-control">
            </div>
            <div class="form-group">
                <label>State</label>
                <select name="zone_id" id="zone_id" class="form-control"></select>
            </div>
            <div class="form-group">
                <label>Zip</label>
                <input type="text" name="zip" id="zip" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Update Address</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="shipping-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Courier Service</h4>
      </div>
      <div class="modal-body">

        <?php echo form_open(site_url().'merchant/orders/generate_pickup', 'class="form-horizontal" id="shipping-form"');?>
            <input type="hidden" name="order_id" value="<?php echo $order->id;?>">
            <div class="radio">
                <label><input type="radio" name="method" value="Delhivery">Delhivery</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="method" value="Other">Other</label>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <input name="company" placeholder="Courier Company" class="form-control">
                </div>
                <div class="col-md-6">
                    <input name="awb" placeholder="AWB Number" class="form-control">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Proceed</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
    $('.update-status-btn').on('click',function(e){
        e.preventDefault();
        var id = $(this).attr('data-order-item-id');
        $('#previous_statuses').html('');
        $('#previous_statuses').hide();
        if(id){
            $.ajax({
                url : '<?php echo site_url();?>'+'merchant/orders/get_order_item_statuses/'+id,
                method: 'GET'
            }).done(function(getdata){
                getdata = JSON.parse(getdata);
                var append_per = '';
                $.each(getdata, function(row,i){
                    append_per = '<b>'+i.ots_status+': </b><em>'+i.created_at+'</em><br>'+i.notes;
                    if(i.data){
                        di = JSON.parse(i.data);
                        append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Image: <a target="_blank" href="<?php echo site_url();?>uploads/return/'+di.image+'">'+di.image+'</a>';
                        if(di.video){
                            append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Video: <a target="_blank" href="<?php echo site_url();?>uploads/return/'+di.video+'">'+di.video+'</a>';
                        }
                        if(di.comments){
                            append_per += '<br>&nbsp;&nbsp;&nbsp;&nbsp;- Comments: '+di.comments;
                        }
                        
                        if(i.ots_status == 'Return Requested'){
                            append_per += '<br><a class="btn btn-primary" href="<?php echo site_url();?>merchant/orders/reverse_order_pickup/<?php echo $order->id;?>/'+id+'">Accept Return Request</a>';
                        }
                    }

                    if(i.ots_status == 'Reverse Pickup generated'){
                        append_per += '<br><a class="btn btn-primary" href="<?php echo site_url();?>merchant/orders/product_received/<?php echo $order->id;?>/'+id+'">Product Received</a>';                            
                    }
                    $('#previous_statuses').append(append_per+'<hr>');
                });
                $('#previous_statuses').show();
            });
            $('#status-order-item-id').val(id);
            $('#status-modal').modal('show');
        }
    });

    $.post('<?php echo site_url('locations/get_zone_menu');?>',{id:99}, function(data) {
        $('#zone_id').html(data);
    });
    
    $('#edit-address-btn').on('click',function(e){
    	e.preventDefault();
        $.ajax({
            url : '<?php echo site_url();?>'+'merchant/orders/get_order_ship_address/<?php echo $order->id;?>',
            method: 'GET'
        }).done(function(getdata){
            getdata = JSON.parse(getdata);
            if(getdata.success){
            	$('#firstname').val(getdata.data.ship_firstname);
            	$('#lastname').val(getdata.data.ship_lastname);
            	$('#email').val(getdata.data.ship_email);
            	$('#phone').val(getdata.data.ship_phone);
            	$('#address1').val(getdata.data.ship_address1);
            	$('#address2').val(getdata.data.ship_address2);
                $('#landmark').val(getdata.data.ship_landmark);
            	$('#city').val(getdata.data.ship_city);
            	$('#zone_id').val(getdata.data.ship_zone_id);
            	$('#zip').val(getdata.data.ship_zip);
            	$('#edit-address-modal').modal('show');
        	}
        });
    });

    $(document).on("submit", '#edit-address-form', function(e){
    	e.preventDefault();
    	var ele = $('#edit-address-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            url : url,
            method: 'POST',
            data : data
        }).done(function(getdata){
            getdata = JSON.parse(getdata);
            if (getdata.success) {
                location.reload();
            } else {
                alert(getdata.data);
            }
        });
    });
});
</script>