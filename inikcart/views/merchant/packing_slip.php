<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
<div style="font-size:12px; font-family:arial, verdana, sans-serif;">
    <table style="border:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:50%; vertical-align:top;border-right:1px solid #000;">
                <img src="http://gojojo.in/inikcart/themes/gojojo/assets/img/logo-gojojo.png" style="max-width:100%">
            </td>
            <td style="width:50%; vertical-align:top;float: right;">
                <?php
                if($order->contents[0]['waybill_method'] == 'Delhivery'){
                    echo '<img src="https://freshservice.com/files/2414/8301/5365/delhivery-2x.png" style="max-width:100%">';
                } else {
                    echo '<h3>'.$order->contents[0]['waybill_method'].'</h3>';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;">
                <img src="http://generator.barcodetools.com/barcode.png?gen=0&data=<?php echo $order->contents[0]['waybill'];?>&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=14&bred=0&w2n=2.5&xdim=2&w=&h=120&debug=1&btype=7&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0">
            </td>
            <td style="width:40%; vertical-align:top;border-top:1px solid #000;">
                <table>
                    <tr>
                        <td><?php echo $order->contents[0]['waybill'];?></td>
                    </tr>
                    <!-- <tr>
                        <td><h4><?php echo $order->sort_code;?></h4></td>
                    </tr> -->
                </table>
            </td>
        </tr>
    </table>

    <table style="border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:70%; vertical-align:top;border-right:1px solid #000;">
                Shipping Address:
                <h3 style="margin-top:5px;margin-bottom:5px;"><?php echo $order->ship_firstname.' '.$order->ship_lastname;?></h3>
                <h4 style="margin-top:5px;margin-bottom:5px;">Phone: <?php echo $order->ship_phone;?></h4>
                <?php echo $order->ship_address1;?><br>
                <?php echo (!empty($order->ship_address2))?$order->ship_address2.'<br/>':'';?>
                <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
                <?php echo (!empty($order->ship_landmark))?$order->ship_landmark.'<br/>':'';?>
                <?php echo $order->ship_city.', '.$order->ship_zone.' '.$order->ship_zip;?><br/>
                <?php echo $order->ship_country;?>
            </td>
            <td style="width:30%; vertical-align:top;">
                <h2>
                    <?php
                    if(($order->payment_info == 'Charge on Delivery') || ($order->payment_info == 'Cash on Delivery')){
                        echo 'COD';
                    } else if($order->payment_info == 'prepaid'){
                        echo 'Prepaid';
                    }
                    ?>
                </h2>
            </td>
        </tr>
    </table>

    <table style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:60%; vertical-align:top;border-right:1px solid #000;">
                Seller: <?php echo $order->store_name;?><br>
                Address: <?php echo $order->merchant_address;?>, 
                <?php echo $order->merchant_city;?>, 
                <?php echo $order->merchant_state;?> - 
                <?php echo $order->merchant_zip;?>
            </td>
            <td style="width:40%; vertical-align:top;">
                <!-- Tin: <?php echo $order->tin;?><br> -->
                IGST: <?php echo $order->merchant_cst;?><br>
                Invoice No.: <?php echo $order->contents[0]['merchant_invoice_number'];?><br>
                <!--Invoice Date: <?php //echo str_replace('T',' ', substr($order->sid, 0, 19));?>-->
            </td>
        </tr>
    </table>

    <table style="border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                Product
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                Price
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                Total
            </td>
        </tr>

        <?php
        $i                  = 0;
        $weight             = 0;
        $product_desc       = '';
        $product_total      = 0.00;
        $product_quantity   = 0;
        $cod_amount         = 0.00;

        foreach($order->contents as $orderkey=>$product):
            $weight = $weight + (floatval($product['weight']) * intval($product['quantity']));
            $product_desc .= '('.($orderkey+1).') '.ucwords( str_replace('&','',preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $product['name'])) ).'<br>';
            $product_quantity  = $product_quantity + intval($product['quantity']);

            if(($order->payment_info == 'Charge on Delivery') || ($order->payment_info == 'Cash on Delivery')){
                $payment_mode   = 'COD';
                $cod_amount     = $cod_amount + floatval($product['total']);
            } else if($order->payment_info == 'prepaid'){
                $payment_mode   = 'Prepaid';
                $cod_amount     = 0;
            }
            $product_total  = $product_total + floatval($product['total']);

        endforeach;
        ?>

        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                <?php echo $product_desc;?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($product_total);?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($product_total);?>
            </td>
        </tr>

        <!-- <?php 
        $items = $order->contents;
        $product_count = 1;
        ?>
        <?php foreach($order->contents as $orderkey=>$product): ?>
        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                (<?php echo $product_count;?>) <?php echo ucwords($product['name']);?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($product['total']/ $product['quantity']);?>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                Rs <?php echo format_currency($product['total']);?>
            </td>
        </tr>
        <?php $product_count++;?>
        <?php endforeach;?> -->


        <tr>
            <td style="width:60%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;">
                <strong>Total</strong>
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;border-right:1px solid #000;text-align: center;">
                <!-- <strong>Rs <?php echo format_currency($order->rs);?></strong> -->
            </td>
            <td style="width:20%; vertical-align:top;border-top:1px solid #000;text-align: center;">
                <strong>Rs <?php echo format_currency($order->total);?></strong>
            </td>
        </tr>
    </table>

    <table style="border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; width:100%; font-size:13px;" cellpadding="5" cellspacing="0">
        <tr>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                <img src="http://generator.barcodetools.com/barcode.png?gen=0&data=<?php echo $order->order_number.'/'.$order->merchant_id;?>&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=11&bred=0&w2n=2.5&xdim=2&w=300&h=60&debug=1&btype=7&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0">
            </td>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                Return Address:
            </td>
            <td style="width:30%; vertical-align:top;border-top:1px solid #000;">
                <?php echo $order->store_name;?><br>
                Phone: <?php echo $order->merchant_phone;?><br>
                <?php echo $order->merchant_address;?><br>
                <?php echo $order->merchant_city;?><br>
                <?php echo $order->merchant_state;?><br>
                <?php echo $order->merchant_zip;?>
            </td>
        </tr>
        
    </table>

</div>
</body>
</html>