<style>
    ul.dotted-ul li {
        list-style-type: disc;
    }

    ol.alpha-ol li {
        list-style-type: lower-alpha;
    }

    ul.dotted-ul {
        margin-left: 40px;
    }

    ol.alpha-ol {
        margin-left: 40px;
    }
</style>
<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url() ?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Shipping Policy</span>
        </div>
        <div class="row">
            <div class="col-md-12 center_column " id="center_column">
                <h2 class="page-heading">
                    <span class="page-heading-title2">Shipping Policy</span>
                </h2>

               <div class="content-text clearfix">
                   <h3 class="mt-12 mb-5">What are the delivery charges?</h3>
                   <p>
                       Delivery is free for most of the products purchased with CartNYou. Shipping charges, if any, are displayed on the website. Delivery charges are subject to vary from product to product. We recommend reviewing the final price before proceeding to payment.
                   </p>

                   <h3 class="mt-12 mb-5">Do I need to pay any hidden charges or tax?</h3>
                   <p>
                       There are no hidden charges on the purchase made with CartNYou. The ‘selling price’ displayed on our website is the final price which you need to pay. Shipping charges, if any, are not considered as hidden cost as shipping charges are displayed at the time of placing order.
                   </p>

                   <h3 class="mt-12 mb-5">What is the expected delivery time?</h3>
                   <p>
                       Delivery time is different for each product as it depends on the availability of the product and the shipping location. The standard delivery time is 5 to 7 working days from the date of order. However, the products are usually delivered within a maximum time span of 3 to 4 working days.
                   </p>
                   <h3 class="mt-12 mb-5">Why estimated delivery time is different for each product?</h3>
                   <p>
                       We have thousands of sellers registered with us. Delivery schedule may vary from product to product as every product is dispatched for delivery by different sellers. Also, delivery time is influenced by the location where the product needs to be delivered and availability of the product with the seller.
                   </p>

                   <h3 class="mt-12 mb-5">Why product delivery is not available in my area?</h3>
                   <p>
                       We are expanding our business to capture the market and deliver products all over India. <br> There are certain limitations which restrict delivery to some locations, such as:
                   </p>
                   <ul class="careerList">
                       <li>
                           Courier partner is not providing service to your location.
                       </li>
                       <li>
                           Seller does not ship products to the specific area.
                       </li>
                       <li>
                           Reliable courier partners are not available in a specific location.
                       </li>
                       <li>
                           Delivery of a specific product is restricted by government in the area.
                       </li>
                       <li>
                           Cash on delivery option is not available in my area, why?
                       </li>
                       <li>
                           Cash on delivery option may not be available in some specific locations as our courier partners do not accept cash as payment in some specific locations.
                       </li>
                   </ul>
                   <h3 class="mt-12 mb-5">Why my order was not delivered within the expected delivery schedule?</h3>
                   <p>
                       We pack and ship the products for delivery as soon as we receive order confirmation from our customers.
                   </p>
                   <p>
                       There are a few possible causes due to which your product delivery may get delayed:
                   </p>
                   <ul class="careerList">
                       <li>
                           Our courier partners have non-operational hours or holiday on the day when we received your order request.
                       </li>
                       <li>
                           No or low availability of the product with seller
                       </li>
                       <li>
                           Exceptions are subject to happen in some scenarios such as product lost in transit, product damaged in transit etc.
                       </li>
                   </ul>
               </div>

            </div>
        </div>

    </div>
</div>