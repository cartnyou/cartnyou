<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Store/Merchant</th>
			<th>UserInfo</th>
			<th>Address</th>
			<th>Logo</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo ucwords($row->store_name);?></td>
					<td><?php echo ucwords($row->firstname.' '.$row->lastname).'<br>'.$row->email.'<br>'.$row->phone.'<br>'.$row->landline;?></td>
					<td><?php echo ucwords($row->address.'<br>'.$row->city.'<br>'.$row->state.'<br>'.$row->zip);?></td>
					<td>
						<?php if($row->logo != ''){ ?>
			                <br><img src="<?php echo base_url('uploads/images/thumbnails/'.$row->logo);?>" alt="">
			            <?php } ?>
					</td>
					<td><?php echo ($row->enabled == '1') ? lang('enabled') : lang('disabled'); ?></td>
					<td>
						<div class="btn-group" style="float:right">
							<a class="btn" href="<?php echo  site_url('seller/info/'.$row->slug);?>" target="_blank">Storefront</a>

							<?php if($row->enabled == '1'){ ?>
								<a class="btn" href="<?php echo  site_url(ADMIN_FOLDER.'/merchants/merchant_status/'.$row->id);?>"><i class="icon-remove"></i> Disable</a>
							<?php } else { ?>
								<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/merchants/merchant_status/'.$row->id);?>"><i class="icon-ok"></i> Enable</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="5">No data found.</td></tr>';
		}
		?>
	</tbody>
</table>