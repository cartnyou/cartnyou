<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="<?php echo site_url('manifest.json') ?>">

	<title><?php echo (!empty($seo_title)) ? $seo_title .' - ' : ''; ?>CartNYou Online Mobile Accessories & Electronics Store</title>
	<meta property="og:title" content="<?php echo (!empty($seo_title)) ? $seo_title .' - ' : '';?>CartNYou Online Mobile Accessories & Electronics Store" />

	<link rel="shortcut icon" href="<?php echo theme_img('favicon.ico');?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo theme_img('apple-icon-57x57.png');?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo theme_img('apple-icon-60x60.png');?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo theme_img('apple-icon-72x72.png');?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo theme_img('apple-icon-76x76.png');?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo theme_img('apple-icon-114x114.png');?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo theme_img('apple-icon-120x120.png');?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo theme_img('apple-icon-144x144.png');?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo theme_img('apple-icon-152x152.png');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_img('apple-icon-180x180.png');?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo theme_img('android-icon-192x192.png');?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_img('favicon-32x32.png');?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo theme_img('favicon-96x96.png');?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo theme_img('favicon-16x16.png');?>">
	<meta name="keywords" content="<?php echo (isset($meta)) ? $meta : 'CartNYou,Online,Mobile Accessories,Electronics,Electronics Store';?>">
	<meta name="description" content="<?php echo (isset($meta_description)) ? $meta_description : 'CartNYou is a one-stop E-SHOP !! Online Mobile Accessories & Electronics Store !! This is your ultimate destination when it comes to satiating your accessory and gadget related desires!';?>">

	<meta property="og:description" content="<?php echo (isset($meta_description)) ? $meta_description : 'CartNYou is a one-stop E-SHOP !! Online Mobile Accessories & Electronics Store !! This is your ultimate destination when it comes to satiating your accessory and gadget related desires!';?>" />
	<meta property="og:image" content="<?php echo theme_img('logo.png');?>" />

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet">
	<?php echo theme_css('bootstrap.min.css', true);?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<?php echo theme_css('select2.min.css', true);?>
	<?php echo theme_css('jquery.bxslider.css', true);?>
	<?php echo theme_css('owl.carousel.css', true);?>
	<?php echo theme_css('jquery-ui.css', true);?>
	<?php echo theme_css('animate.css', true);?>
	<?php echo theme_css('reset.css', true);?>
	<?php echo theme_css('style.css', true);?>
	<?php echo theme_css('responsive.css', true);?>
    <?php echo theme_css('megamenu.css', true);?>
    <?php echo theme_css('webslidemenu.css', true);?>


	<?php
	if(isset($additional_header_info))
	{
		echo $additional_header_info;
	}

	?>

	<?php echo theme_js('jquery-1.11.2.min.js', true);?>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
    <?php echo theme_js('webslidemenu.js', true);?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-xxxx-77359281-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<!-- Facebook Pixel Code -->
	<!--<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '1053588698049080');
	fbq('track', 'PageView');
	</script>-->
	<!--<noscript>
	<img height="1" width="1"
	src="https://www.facebook.com/tr?id=1053588698049080&ev=PageView
	&noscript=1"/>
	</noscript>-->
	<!-- End Facebook Pixel Code -->


    <!--Main Menu File-->
   <!-- <link rel="stylesheet" type="text/css" media="all" href="http://webslidemenu.uxwing.com/demo/webslide/mobile-drawer-style-menu/top-fixed-navigation-bar/css/webslidemenu.css" />
    <script type="text/javascript" src="http://webslidemenu.uxwing.com/demo/webslide/mobile-drawer-style-menu/top-fixed-navigation-bar/js/webslidemenu.js"></script>-->
    <!--Main Menu File-->


</head>
<body class="<?php echo (!empty($body_class)) ? $body_class : 'category-page'; ?>">
<!-- TOP BANNER -->
<!--<div id="top-banner" class="top-banner">
    <div class="bg-overlay"></div>
    <div class="container">
        <h1>Special Offer!</h1>
        <h2>Additional 40% OFF For Men & Women Clothings</h2>
        <span>This offer is for online only 7PM to middnight ends in 30th July 2015</span>
        <span class="btn-close"></span>
    </div>
</div>-->
<!-- HEADER -->
<div id="header" class="header">

	<!--/.top-header -->
	<!-- MAIN HEADER -->
    <div class="top-main">
        <nav class="navbar navbar-default hidden-sm">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <a class="navbar-brand" href="<?php echo site_url();?>">
                        <img alt="CartNYou" width="120" class="mainLogo" src="<?php echo theme_img('logo.png');?>" />
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="col-md-6 col-sm-6">
                        <?php echo form_open('cart/search', 'class="form-inline navbar-form navbar-left width-100"');?>
                            <div class="form-group searchf width-100">
                                <div class="input-group width-100">

                                    <input type="text" name="term" class="form-control" placeholder="Search products here...">
                                    <span class="input-group-addon pd-0">
                                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                        </span>
                                </div>
                            </div>
                        </form>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <?php if(!$this->Customer_model->is_logged_in(false, false)):?>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#loginModal">
                                <span class="navIc"><i class="fa fa-user"></i></span>
                                <span class="navLink">Account <br> <span class="fs-10">Sign In</span></span>
                            </a>
                        </li>
                        <?php else: ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="navIc"><i class="fa fa-user"></i></span>
                                <span class="navLink">Account <br> <span class="fs-10">Hello! <?php echo $this->Customer_model->logged_customer_first_name();?></span></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('secure/my-account');?>"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;My Account</a></li>
                                <li><a href="<?php echo site_url('secure/orders');?>"><i class="fa fa-shopping-bag"></i>&nbsp;&nbsp;Orders</a></li>
                                <li><a href="<?php echo site_url('secure/addresses');?>"><i class="fa fa-address-book-o"></i>&nbsp;&nbsp;Addresses</a></li>
                                <li><a href="<?php echo site_url('cart/wishlist');?>"><i class="fa fa-magic"></i>&nbsp;&nbsp;Wishlist</a></li>
                                <li><a href="<?php echo site_url('secure/logout');?>"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;<?php echo lang('logout');?></a></li>
                            </ul>
                        </li>  
                        <?php endif; ?>

                        <li>
                            <a href="<?php echo site_url('sell-at-cartnyou');?>" target="_blank">
                                <span class="navIc"><i class="fa fa-inr"></i></span>
                                <span class="navLink">Sell <br> <span class="fs-10">on CartnYou</span></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('cart/view-cart');?>">
                                <span class="navIc"><i class="fa fa-shopping-bag"></i><i class="cart-items-count mini-cart-count">0</i></span>
                                <span class="navLink ml10">Cart <br> <span class="fs-10 cart-items-total"><i class="fa fa-inr"> </i> 0.00</span></span>
                            </a>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="wsmenucontainer clearfix">
            <div id="overlapblackbg"></div>

            <div class="wsmobileheader clearfix">
                <div class="">
                    <div class="col-xs-2 p0 mt-5">
                        <a id="wsnavtoggle" class="animated-arrow">
                            <!--<img src="<?php /*echo theme_assets('images/menu.png')  */?>" class="img-responsive" alt="">-->
                            <span></span>
                        </a>
                    </div>

                    <div class="col-xs-8 p0">
                        <a class="navbar-brand" href="<?php echo site_url();?>">
                            <img alt="CartNYou" class="mainLogo" src="<?php echo theme_img('logo.png');?>" />
                        </a>
                    </div>
                    
                    <div class="col-xs-2 wsoffcanvasopener-hidden p0">
                        <a href="<?php echo site_url('cart/view-cart');?>">
                            <span class="navIc pull-right navIc-mini-cart"><i class="fa fa-shopping-bag"></i><i class="cart-items-count mini-cart-count">0</i></span>
                        </a>
                    </div>
                </div>

                <div class="">
                    <div class="col-xs-12 wsoffcanvasopener-hidden p0">
                        <?php echo form_open('cart/search', 'class="form-inline navbar-form navbar-left width-100"');?>
                            <div class="form-group searchf width-100">
                                <div class="input-group width-100">

                                    <input type="text" name="term" class="form-control" placeholder="Search products here...">
                                    <span class="input-group-addon">
                                            <button type="submit" class="pull-right btn-search"><i class="fa fa-search"></i></button>
                                        </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="header">
              <div class="wrapper clearfix bigmegamenu">
                    <!--Main Menu HTML Code-->
                    <nav class="wsmenu clearfix">
                      <ul class="mobile-sub wsmenu-list">

                          <li class="visible-xs">
                              <div class="user-xs">
                                  <div class="user-img">
                                      <img src="<?php echo theme_img('user.png') ?>" alt="">
                                  </div>
                                  <div class="user-wel">
                                      <p>Hello</p>
                                      <?php if($this->Customer_model->is_logged_in(false, false)){ ?>
                                          <p> <?php echo $this->Customer_model->logged_customer_first_name();?></p>
                                      <?php } else {                        ?>
                                          <p id="clogin">Login to <a href="javascript:;" >CartNYou</a></p>
                                      <?php } ?>

                                  </div>
                              </div>
                          </li>
                          <?php if($this->Customer_model->is_logged_in(false, false)){ ?>
                              <div class="set-xs visible-xs">
                                  <div class="ac-xs">
                                      <a class="d-block text-center" href="<?php echo site_url('secure/my-account');?>">
                                          <img src="<?php echo theme_img('account.png') ?>" alt="" class="mb-5 mt-5"> <br>
                                          My Account
                                      </a>
                                  </div>
                                  <div class="or-xs">
                                      <a class="d-block text-center" href="<?php echo site_url('secure/orders');?>">
                                          <img src="<?php echo theme_img('order.png') ?>" alt="" class="mb-5 mt-5"> <br>
                                          My Order
                                      </a>
                                  </div>
                              </div>

                          <?php }                        ?>

                          <li class="visible-xs"><a href="<?php echo site_url();?>">Home</a></li>

                        <?php
                        if(isset($this->categories[0])):

                            function getSubCategoriesList($id,$all_categories, $mainSub=false){
                                if(isset($all_categories[$id])){
                                    if(!empty($all_categories[$id])){

                                        foreach($all_categories[$id] as $subcategory):

                                            if($mainSub){
                                                echo '<ul class="col-lg-3 col-md-4 col-xs-12 link-list">';
                                                $addClass = 'megamenu-subcategory';
                                            } else {
                                                $addClass = '';
                                            }

                                                echo '<li class="'.$addClass.'"><a href="'.site_url($subcategory->slug).'">'.$subcategory->name;
                                                    if($mainSub){ echo ' <i class="fa fa-chevron-right"></i>'; }
                                                echo '</a></li>';
                                                getSubCategoriesList($subcategory->id,$all_categories);

                                            if($mainSub){
                                                echo '</ul>';
                                            }

                                        endforeach;

                                    }
                                }
                            }

                            foreach($this->categories[0] as $cat_menu):
                            ?>
                                <li class="<?php echo $cat_menu->active ? 'active' : false;?>">
                                    <a href="#subcategory-<?php echo $cat_menu->slug;?>">
                                        <?php if($cat_menu->icon != ''){ ?>
                                            <!--<img class="category-sm-icon" src="<?php /*echo base_url('uploads/images/thumbnails').'/'.$cat_menu->icon;*/?>">-->
                                        <?php } ?>
                                        <?php echo $cat_menu->name;?>
                                        <span class="arrow"></span>
                                    </a>

                                    <div class="megamenu clearfix">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div id="subcategory-<?php echo $cat_menu->slug;?>" class="<?php echo $cat_menu->active ? 'active' : false;?>">
                                                    <div class="lnt-subcategory col-sm-12 col-md-12">
                                                        <!-- <h3 class="lnt-category-name"><?php echo $cat_menu->name;?></h3> -->

                                                        <?php getSubCategoriesList($cat_menu->id, $this->categories, true);?>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                </li>
                            <?php
                            endforeach;
                        endif;
                        ?>
                          <?php if($this->Customer_model->is_logged_in(false, false)){ ?>
                          <li class="visible-xs mobilemenu-invert"><a href="<?php echo site_url('secure/logout');?>"><?php echo lang('logout');?></a></li>
                          <?php }                         ?>


                      </ul>
                    </nav>
                    <!--Menu HTML Code-->
                </div>
            </div>
        </div>



    </div>


</div>
<script>
    $(function () {
        $('.toggle-menu').click(function(){
            $('.exo-menu').toggleClass('display');

        });

    });
</script>

