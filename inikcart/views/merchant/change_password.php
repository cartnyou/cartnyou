<form id="change-password-form" action="<?php echo site_url();?>merchant/profile/change_password_do">
    <fieldset class="row no-gutters">

            <div class="form-group col-md-6 pdlr-4">
                <label>Current Password</label>
                <input type="password" name="password" class="form-control">
            </div>
    </fieldset>
    <fieldset class="row no-gutters">
            <div class="form-group col-md-6 pdlr-4">
                <label>New Password</label>
                <input type="password" name="new_password" class="form-control">
            </div>
    </fieldset>
    <fieldset class="row no-gutters">
            <div class="form-group col-md-6 pdlr-4">
                <label>Confirm New Password</label>
                <input type="password" name="new_password_confirm" class="form-control">
            </div>                        

       <div class="col-md-12">
           <button class="btn btn-info" type="submit"><span>Change Password</span></button>
       </div>

    </fieldset>
</form>

<script>
$(function () {
    $(document).on("submit", '#change-password-form', function(e){
        e.preventDefault();
        $('.error').remove();
        var ele = $('#change-password-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(getdata){
            if (getdata.status == true) {
                bootbox.alert('Password Changed Successfully.', function(){
                    window.location.href = "<?php echo site_url();?>merchant/login/logout";
                });
            } else {
                if(getdata.hasOwnProperty('errors')) {
                    $.each(getdata.errors, function(key, val) {
                       $('#change-password-form input[name='+key+']').after(val);
                   });
                } else {
                   bootbox.alert(getdata.data);
                }
            }
        });
    });
});
</script>