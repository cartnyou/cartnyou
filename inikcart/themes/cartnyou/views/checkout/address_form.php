<!-- BREADCRUMBS -->
<section class="breadcrumb parallax margbot30"></section>
<!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

	<!-- CONTAINER -->
	<div class="container border0 margbot0">
		<h3 class="pull-left"><b>Checkout</b></h3>

		<div class="pull-right">
			<a href="<?php echo site_url();?>" >Back shopping bag<i class="fa fa-angle-right"></i></a>
		</div>
	</div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->


<!-- CHECKOUT PAGE -->
<section class="checkout_page">

	<!-- CONTAINER -->
	<div class="container">

		<?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('message');?>
            </div>
        <?php endif;?>
        
        <?php if ($this->session->flashdata('error')):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if (!empty($error)):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $error;?>
            </div>
        <?php endif;?>

        <?php if (validation_errors()):?>
        	<div class="alert alert-danger">
        		<a class="close" data-dismiss="alert">×</a>
        		<?php echo validation_errors();?>
        	</div>
        <?php endif;?>

        <script type="text/javascript">
		$(document).ready(function(){
			<?php
			// Restore previous selection, if we are on a validation page reload
			$zone_id = set_value('zone_id');

			echo "\$('#zone_id').val($zone_id);\n";
			?>
		});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#country_id').change(function(){
					populate_zone_menu();
				});	

			});
			// context is ship or bill
			function populate_zone_menu(value)
			{
				$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
					$('#zone_id').html(data);
				});
			}
			</script>
			<?php /* Only show this javascript if the user is logged in */ ?>
			<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
				<script type="text/javascript">	
					<?php
					$add_list = array();
					foreach($customer_addresses as $row) {
					// build a new array
						$add_list[$row['id']] = $row['field_data'];
					}
					$add_list = json_encode($add_list);
					echo "eval(addresses=$add_list);";
					?>
					
					function populate_address(address_id)
					{
						if(address_id == '')
						{
							return;
						}

					// - populate the fields
					$.each(addresses[address_id], function(key, value){
						
						$('.address[name='+key+']').val(value);

						// repopulate the zone menu and set the right value if we change the country
						if(key=='zone_id')
						{
							zone_id = value;
						}
					});
					
					// repopulate the zone list, set the right value, then copy all to billing
					$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#country_id').val()}, function(data) {
						$('#zone_id').html(data);
						$('#zone_id').val(zone_id);
					});		
				}
				
			</script>
		<?php endif;?>

		<!-- CHECKOUT BLOCK -->
		<div class="checkout_block">
			<ul class="checkout_nav">
				<li class="active_step">1. Shipping Address</li>
				<li>2. Delivery</li>
				<li>3. Payment</li>
				<li class="last">4. Confirm Order</li>
			</ul>

			<?php
			$countries = $this->Location_model->get_countries_menu();

			if(!empty($customer[$address_form_prefix.'_address']['country_id']))
			{
				$zone_menu	= $this->Location_model->get_zones_menu($customer[$address_form_prefix.'_address']['country_id']);
			}
			else
			{
				$zone_menu = array(''=>'')+$this->Location_model->get_zones_menu(array_shift(array_keys($countries)));
			}

			//form elements

			$address1	= array('placeholder'=>lang('address1'), 'class'=>'address span8', 'name'=>'address1', 'value'=> set_value('address1', @$customer[$address_form_prefix.'_address']['address1']));
			$address2	= array('placeholder'=>lang('address2'), 'class'=>'address span8', 'name'=>'address2', 'value'=>  set_value('address2', @$customer[$address_form_prefix.'_address']['address2']));
			$first		= array('placeholder'=>lang('address_firstname'), 'class'=>'address span4', 'name'=>'firstname', 'value'=>  set_value('firstname', @$customer[$address_form_prefix.'_address']['firstname']));
			$last		= array('placeholder'=>lang('address_lastname'), 'class'=>'address span4', 'name'=>'lastname', 'value'=>  set_value('lastname', @$customer[$address_form_prefix.'_address']['lastname']));
			$email		= array('placeholder'=>lang('address_email'), 'class'=>'address span4', 'name'=>'email', 'value'=> set_value('email', @$customer[$address_form_prefix.'_address']['email']));
			$phone		= array('placeholder'=>lang('address_phone'), 'class'=>'address span4', 'name'=>'phone', 'value'=> set_value('phone', @$customer[$address_form_prefix.'_address']['phone']));
			$city		= array('placeholder'=>lang('address_city'), 'class'=>'address span3', 'name'=>'city', 'value'=> set_value('city', @$customer[$address_form_prefix.'_address']['city']));
			$zip		= array('placeholder'=>lang('address_zip'), 'maxlength'=>'10', 'class'=>'address span2', 'name'=>'zip', 'value'=> set_value('zip', @$customer[$address_form_prefix.'_address']['zip']));

			?>

			<?php
			//post to the correct place.
			echo ($address_form_prefix == 'bill')?form_open('checkout/step_1', array('class'=>'checkout_form clearfix')):form_open('checkout/shipping_address', array('class'=>'checkout_form clearfix'));?>

				<div class="row">
					<h2 class="col-md-2 col-xs-12">
						<?php echo ($address_form_prefix == 'bill')?lang('address'):lang('shipping_address');?>
					</h2>
					<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
						<div class="col-md-4 pull-right col-xs-12">
							<button class="btn btn-inverse pull-right" onclick="$('#address_manager').modal('show');" type="button"><i class="icon-envelope icon-white"></i> <?php echo lang('choose_address');?></button>
						</div>
					<?php endif; ?>
				</div>

				<div class="checkout_form_input first_name">
					<label><?php echo lang('address_firstname');?> <span class="color_red">*</span></label>
					<?php echo form_input($first);?>
				</div>

				<div class="checkout_form_input last_name hidden">
					<label><?php echo lang('address_lastname');?> <span class="color_red">*</span></label>
					<?php echo form_input($last);?>
				</div>

				<div class="checkout_form_input phone">
					<label><?php echo lang('address_phone');?> <span class="color_red">*</span></label>
					<?php echo form_input($phone);?>
				</div>

				<div class="checkout_form_input last E-mail">
					<label><?php echo lang('address_email');?> <span class="color_red">*</span></label>
					<?php echo form_input($email);?>
				</div>

				<hr class="clear">

				<div class="checkout_form_input country">
					<label><?php echo lang('address_country');?> <span class="color_red">*</span></label>
					<?php echo form_dropdown('country_id',$countries, @$customer[$address_form_prefix.'_address']['country_id'], 'id="country_id" class="address basic"');?>
				</div>

				<div class="checkout_form_input sity">
					<label><?php echo lang('address_city');?> <span class="color_red">*</span></label>
					<?php echo form_input($city);?>
				</div>

				<div class="checkout_form_input country">
					<label><?php echo lang('address_state');?> <span class="color_red">*</span></label>
					<?php echo form_dropdown('zone_id',$zone_menu, @$customer[$address_form_prefix.'_address']['zone_id'], 'id="zone_id" class="address basic" ');?>
				</div>

				<div class="checkout_form_input last postcode">
					<label><?php echo lang('address_zip');?> <span class="color_red">*</span></label>
					<?php echo form_input($zip);?>
				</div>

				<div class="checkout_form_input2 adress">
					<label><?php echo lang('address1');?> <span class="color_red">*</span></label>
					<?php echo form_input($address1);?>
				</div>

				<div class="checkout_form_input2 last adress">
					<label><?php echo lang('address2');?></label>
					<?php echo form_input($address2);?>
				</div>

				<?php if($address_form_prefix=='bill') : ?>
				<div class="checkout_form_input">
					<div class="pull-left">
						<?php echo form_checkbox(array('name'=>'use_shipping', 'value'=>'yes', 'id'=>'use_shipping', 'checked'=>$use_shipping)) ?>
						<label for="use_shipping"><?php echo lang('ship_to_address') ?></label>
					</div>
				</div>
				<?php endif ?>


				<div class="clear"></div>

				<div class="checkout_form_note">All fields marked with (<span class="color_red">*</span>) are required</div>

				<input class="btn active pull-right" type="submit" value="<?php echo lang('form_continue');?>"/>
				<?php if($address_form_prefix=='ship') : ?>
					<input style="margin-right:8px;border:0;padding:10px 52px;" class="btn active pull-right" type="button" value="<?php echo lang('form_previous');?>" onclick="window.location='<?php echo base_url('checkout/step_1') ?>'"/>
				<?php endif; ?>
				
			</form>
		</div><!-- //CHECKOUT BLOCK -->
	</div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->

<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
	<div class="modal fade" id="address_manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><?php echo lang('your_addresses');?></h4>
	      </div>
	      <div class="modal-body">
	      	<table class="table table-striped">
	      		<?php
	      		$c = 1;
	      		foreach($customer_addresses as $a):?>
	      		<tr>
	      			<td>
	      				<?php
	      				$b	= $a['field_data'];
	      				echo nl2br(format_address($b));
	      				?>
	      			</td>
	      			<td style="width:100px;"><input type="button" class="btn btn-primary choose_address pull-right" onclick="populate_address(<?php echo $a['id'];?>);" data-dismiss="modal" value="<?php echo lang('form_choose');?>" /></td>
	      		</tr>
	      		<?php endforeach;?>
	      	</table>
	  	  </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
<?php endif;?>