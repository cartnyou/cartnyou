<?php include('header.php');?>

<script type="text/javascript">

<?php if( $this->input->post('submit') ):?>
$(window).ready(function(){
	$('#iframe_uploader', window.parent.document).height($('body').height());	
});
<?php endif;?>

<?php
if(!empty($file_name)):
	foreach ($file_name as $file_name_per) {
?>
	parent.add_product_image('<?php echo $file_name_per;?>');
<?php 
	}
endif;
?>

</script>

<?php
if (!empty($error)):
	foreach ($error as $error_per) {
?>
	<div class="alert alert-error">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $error_per; ?>
	</div>
<?php
	}
endif;
?>

<?php
if ($successCount > 0):
?>
	<div class="alert alert-success">
		<a class="close" data-dismiss="alert">×</a>
		<?php echo $successCount; ?> Files Uploaded
	</div>
<?php
endif;
?>

<div class="row-fluid">
	<div class="span12">
		<?php echo form_open_multipart($this->config->item('admin_folder').'/products/product_image_upload_multiple', 'class="form-inline"');?>
			<input name="upl_files[]" value="" id="userfile" class="input-file" type="file" multiple="multiple" accept="image/*">
			<input class="btn btn-info" name="submit" type="submit" value="<?php echo lang('upload');?>" />
		</form>
		<span style="color: red;">Best Resolution: 500x500 px | Max Size: 2MB | Allowed FileType: jpg,jpeg,png,gif</span>
	</div>
</div>

<?php include('footer.php');