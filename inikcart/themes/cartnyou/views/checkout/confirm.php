<!-- BREADCRUMBS -->
<section class="breadcrumb parallax margbot30"></section>
<!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

	<!-- CONTAINER -->
	<div class="container border0 margbot0">
		<h3 class="pull-left"><b>Checkout</b></h3>

		<div class="pull-right">
			<a href="<?php echo site_url();?>" >Back shopping bag<i class="fa fa-angle-right"></i></a>
		</div>
	</div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->


<!-- CHECKOUT PAGE -->
<section class="checkout_page">

	<!-- CONTAINER -->
	<div class="container">

		<?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('message');?>
            </div>
        <?php endif;?>
        
        <?php if ($this->session->flashdata('error')):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if (!empty($error)):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $error;?>
            </div>
        <?php endif;?>

        <?php if (validation_errors()):?>
        	<div class="alert alert-danger">
        		<a class="close" data-dismiss="alert">×</a>
        		<?php echo validation_errors();?>
        	</div>
        <?php endif;?>

		<!-- CHECKOUT BLOCK -->
		<div class="checkout_block">
			<ul class="checkout_nav">
				<li class="done_step2">1. Shipping Address</li>
				<li class="done_step2">2. Delivery</li>
				<li class="done_step">3. Payment</li>
				<li class="active_step last">4. Confirm Order</li>
			</ul>
		</div><!-- //CHECKOUT BLOCK -->

		<!-- ROW -->
		<div class="row">
			<div class="col-md-12 padbot60">
				<div class="checkout_confirm_orded clearfix">
					<h2><?php echo lang('form_checkout');?></h2>

					<?php include('order_details.php');?>
					<hr>
					<?php include('summary.php');?>
					<hr>
					<a class="btn active pull-right checkout_block_btn mt0" href="<?php echo site_url('checkout/success');?>"><?php echo lang('submit_order');?></a>
					<a class="btn inactive pull-left" href="<?php echo site_url('checkout/step_3');?>">Go to previous step</a>
				</div>
			</div>

		</div><!-- //ROW -->
	</div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE -->