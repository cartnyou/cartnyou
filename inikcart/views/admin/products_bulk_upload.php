<?php if(isset($successCount)):?>
	<div class="alert alert-success">
		<?php echo $successCount;?> Products Uploaded
	</div>
<?php endif;?>

<div class="row">
	<div class="span12">

		<?php echo form_open_multipart($this->config->item('admin_folder').'/products/bulk_upload', 'class="form-inline"');?>

			<h5>Select Merchant</h5>
			<select class="select2-ajax" style="width: 20%;" name="merchant_id"></select>

			<script type="text/javascript">
				$(".select2-ajax").select2({
					tags: false,
					placeholder: "Search Merchant...",
					tokenSeparators: [',', ' '],
					minimumInputLength: 1,
					ajax: {
						url: "<?php echo site_url($this->config->item('admin_folder').'/merchants/merchant_autocomplete');?>",
						dataType: "json",
						type: "POST",
						data: function (params) {

							var queryParameters = {
								name: params.term,
								limit: 10
							}
							return queryParameters;
						},
						processResults: function (data) {
							return {
								results: $.map(data, function (item) {
									return {
										text: item.store_name,
										id: item.id
									}
								})
							};
						}
					}
				});
			</script>

			<h5>Product CSV File <span style="color: #F00;">* (Only CSV File will work)</span></h5>
			<input type="file" name="file" placeholder="CSV File" /><br><br>
			<input type="submit" class="btn btn-success" name="submit" value="Submit" />
		</form>
		<a href="<?php echo site_url('uploads/sample_products.csv');?>">Download Sample CSV</a>
	</div>
</div>