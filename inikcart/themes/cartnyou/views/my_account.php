<?php echo theme_js('jquery.matchHeight-min.js', true);?>

<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">My Account</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">

        	<div class="column col-xs-12 col-sm-12">
        		<?php if ($this->session->flashdata('message')):?>
		            <div class="alert alert-info">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('message');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if ($this->session->flashdata('error')):?>
		            <div class="alert alert-danger">
		                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
		                <?php echo $this->session->flashdata('error');?>
		            </div>
		        <?php endif;?>
		        
		        <?php if(validation_errors()):?>
		        	<div class="alert alert-danger">
		        		<a class="close" data-dismiss="alert">×</a>
		        		<?php echo validation_errors();?>
		        	</div>
		        <?php endif;?>
        	</div>

            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">
                <!-- block category -->
                <div class="block left-module">
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active"><span></span><a href="<?php echo site_url('secure/my-account');?>">Profile</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/orders');?>">Orders</a></li>
                                    <li><span></span><a href="<?php echo site_url('secure/addresses');?>">My Address</a></li>
                                    <li><span></span><a href="<?php echo site_url('cart/wishlist');?>">My Wishlist</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- Banner silebar -->

                <!-- ./Banner silebar -->
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                
                <div class="profile">
                    <h2 class="page-heading">
                        <span class="page-heading-title2">My Profile</span>
                    </h2>

                    <?php
					$first		= array('id'=>'firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname', $customer['firstname']));
					$last		= array('id'=>'lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname', $customer['lastname']));
					$email		= array('id'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=> set_value('email', $customer['email']));
					$phone		= array('id'=>'phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone', $customer['phone']));

					$password	= array('id'=>'password', 'class'=>'form-control', 'name'=>'password', 'value'=>'');
					$confirm	= array('id'=>'confirm', 'class'=>'form-control', 'name'=>'confirm', 'value'=>'');
					?>
                    
                    <?php echo form_open('secure/my_account'); ?>
                    	<div class="row">
	                        <div class="form-group col-md-12">
	                            <label>First Name</label>
	                            <?php echo form_input($first);?>
	                        </div>
	                        <div class="form-group col-md-6 hidden">
	                            <label>Last Name</label>
	                            <?php echo form_input($last);?>
	                        </div>
	                    </div>
	                    <div class="row">
	                        <div class="form-group col-md-6">
	                            <label>Email</label>
	                            <?php echo form_input($email);?>
	                        </div>
	                        <div class="form-group col-md-6">
	                            <label>Phone</label>
	                            <?php echo form_input($phone);?>
	                        </div>
	                    </div>
	                    
	                    <input type="hidden" name="email_subscribe" id="email_subscribe" value="1" <?php if((bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/>
	                    
	                    <div class="row">
	                    	<div class="col-md-12">
								<div style="margin:30px 0px 10px; text-align:center;">
									<strong><?php echo lang('account_password_instructions');?></strong>
								</div>
							</div>
	                        <div class="form-group col-md-6">
	                            <label>Password</label>
	                            <?php echo form_password($password);?>
	                        </div>
	                        <div class="form-group col-md-6">
	                            <label>Confirm Password</label>
	                            <?php echo form_password($confirm);?>
	                        </div>
	                    </div>
                        <div class="form-group text-center">
                            <button class="btn btn-red" type="submit">Update</button>
                        </div>

                    </form>
                </div>

            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>

<div class="modal fade" id="address-form-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content" id="address-form-container">

		</div>
	</div>
</div>

<script>
    $(function(){
    	$('.match-item').matchHeight();
        $('.vertical-menu-content').css('display','none');
    });
</script>