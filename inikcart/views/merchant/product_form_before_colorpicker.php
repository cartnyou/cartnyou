<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
<script>
var global_tax_rate = 0;
var global_commission = 0;

$(document).ready(function() {
	$('.iframe-btn').fancybox({	
		'width'		: 900,
		'height'	: 600,
		'type'		: 'iframe',
	    'autoScale' : false
    });

    $('.remove-option-image').on('click', function(){
    	var field_id = $(this).data('field-id');
    	$('#'+field_id).val('');
		$('#'+field_id+'-image').attr('src', '<?php echo base_url("uploads/options");?>'+'/');
		$('#'+field_id+'-image').addClass('hidden');
		$('#remove-option-image-'+field_id).addClass('hidden');
    });
});

function responsive_filemanager_callback(field_id){
	var url=jQuery('#'+field_id).val();
	//alert('update '+field_id+" with "+url);
	//your code
	jQuery('#'+field_id+'-image').attr('src',url);
	jQuery('#'+field_id+'-image').removeClass('hidden');
	jQuery('#remove-option-image-'+field_id).removeClass('hidden');
}
</script>


<?php $GLOBALS['option_value_count'] = 0;?>
<style type="text/css">
	.sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	.sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 18px; }
	.sortable li>span { position: absolute; margin-left: -1.3em; margin-top:.4em; }
	.hidden{
		display: none !important;
	}
	.option-sm-image{
		width: 40px;
		height: 40px;
	}
	.fancybox-inner, .fancybox-image, .fancybox-iframe{
		min-height: 400px;
	}
</style>

<script type="text/javascript">
//<![CDATA[

$(document).ready(function() {
	$(".sortable").sortable();
	$(".sortable > span").disableSelection();
	//if the image already exists (phpcheck) enable the selector

	<?php if($id) : ?>
	//options related
	var ct	= $('#option_list').children().size();
	// set initial count
	option_count = <?php echo count($product_options); ?>;
	<?php endif; ?>

	photos_sortable();
});

function add_product_image(data)
{
	p	= data.split('.');
	
	var photo = '<?php add_image("'+p[0]+'", "'+p[0]+'.'+p[1]+'", '', '', '', base_url('uploads/images/thumbnails'));?>';
	$('#gc_photos').append(photo);
	$('#gc_photos').sortable('destroy');
	photos_sortable();
}

function remove_image(img)
{
	if(confirm('<?php echo lang('confirm_remove_image');?>'))
	{
		var id	= img.attr('rel')
		$('#gc_photo_'+id).remove();
	}
}

function photos_sortable()
{
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}

function remove_option(id)
{
	if(confirm('<?php echo lang('confirm_remove_option');?>'))
	{
		$('#option-'+id).remove();
	}
}

//]]>
</script>


<?php echo form_open($this->config->item('merchant_folder').'/products/form/'.$id, array('class'=>'form-horizontal form-row-seperated') ); ?>
<div class="row">
	<div class="col-md-12">

		<div class="portlet">
			<div class="portlet-title">
	            <div class="caption">
	                <i class="fa fa-shopping-cart"></i><?php echo ($name)?$name:'Add New Product';?>
	            </div>
	            <div class="actions btn-set">
	                <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> <?php echo lang('save');?></button>
	                <!-- <div class="btn-group">
	                    <a class="btn purple dropdown-toggle" href="javascript:;" data-toggle="dropdown">
	                        <i class="fa fa-share"></i> More
	                        <i class="fa fa-angle-down"></i>
	                    </a>
	                    <div class="dropdown-menu pull-right">
	                        <li>
	                            <a href="<?php echo site_url($this->config->item('merchant_folder').'/products/form/'.$id.'/1');?>"> Duplicate </a>
	                        </li>
	                        <li>
	                            <a href="<?php echo site_url($this->config->item('merchant_folder').'/products/delete/'.$id);?>"> Delete </a>
	                        </li>
	                    </div>
	                </div> -->
	            </div>
	        </div>

			<div class="tabbable-bordered">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#product_info" data-toggle="tab"><?php echo lang('details');?></a></li>
					<li><a href="#tab_meta" data-toggle="tab"><?php echo lang('header_information');?></a></li>
					<li><a href="#tab_inventory" data-toggle="tab"><?php echo lang('inventory');?></a></li>
					<?php //if there aren't any files uploaded don't offer the client the tab
					if (count($file_list) > 0):?>
					<li><a href="#product_downloads" data-toggle="tab"><?php echo lang('digital_content');?></a></li>
					<?php endif;?>
					<li><a href="#product_brand" data-toggle="tab">Brand</a></li>
					<li><a href="#product_options" data-toggle="tab"><?php echo lang('options');?></a></li>
					<li><a href="#product_related" data-toggle="tab"><?php echo lang('related_products');?></a></li>
					<li><a href="#product_photos" data-toggle="tab"><?php echo lang('images');?></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="product_info">
						<div class="form-body">
							<div class="form-group">
		                        <label class="col-md-2 control-label">Name:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('placeholder'=>lang('name'), 'name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
									echo form_input($data);
									?>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Description:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'description', 'class'=>'redactor form-control', 'value'=>set_value('description', $description));
									echo form_textarea($data);
									?>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Specification:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'specification', 'class'=>'redactor form-control', 'value'=>set_value('specification', $specification));
									echo form_textarea($data);
									?>
		                        </div>
		                    </div>

		                    <!-- <div class="form-group">
		                        <label class="col-md-2 control-label">Attributes:
		                        </label>
		                        <div class="col-md-10">
		                        	<div class="row">
		                        		<div class="col-md-6">
		                            		<input class="form-control" placeholder="Attribute Name">
		                            	</div>
		                            	<div class="col-md-6">
		                            		<input class="form-control" placeholder="Attribute Value">
		                            	</div>
		                            </div>

		                            <a class="btn btn-info" id="add-attribute-btn"><i class="fa fa-plus"></i> Add More</a>
		                        </div>
		                    </div> -->

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Short Description:
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'excerpt', 'value'=>set_value('excerpt', $excerpt), 'class'=>'form-control', 'rows'=>5);
									echo form_textarea($data);
									?>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Seller SKU:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
									<?php
									$data	= array('name'=>'sku', 'value'=>set_value('sku', $sku), 'class'=>'form-control');
									echo form_input($data);?>
								</div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">HNS Code:
		                        </label>
		                        <div class="col-md-10">
									<?php
									$data	= array('name'=>'hns', 'value'=>set_value('hns', $hns), 'class'=>'form-control');
									echo form_input($data);?>
								</div>
		                    </div>

		                     <div class="form-group">
		                        <label class="col-md-2 control-label">
		                        	<?php echo lang('weight');?>(gms):
		                        	<span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'weight', 'value'=>set_value('weight', $weight), 'class'=>'form-control');
									echo form_input($data);?>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Tax Rate:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10" style="margin-top: 5px;">
		                        	<span class="tax_rate_container"></span>%
		                            <?php
								 	//$options = array(	18.00	=> '18%',28.00	=> '28%');
									//echo form_dropdown('tax_rate', $options, set_value('tax_rate',$tax_rate), 'id="tax-input" class="table-group-action-input form-control input-medium"');
									
									?>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label"><?php echo lang('price');?>:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'price', 'value'=>set_value('price', $price), 'id'=>'price-input', 'class'=>'form-control');
									echo form_input($data);?>

									<span class="pull-left"><a href="javascript:;" id="show-breakup-btn">Show Breakup</a></span>
									<div class="row mtb-10" id="breakup-container" style="display: none;">
		                                <div class="col-md-8 col-sm-10 col-sm-offset-1 bkgGrey col-md-offset-2 mtb-10">
		                                    <div class="row mt-15">
		                                        <div class="col-md-6 text-left">
		                                            <label>Gojojo Margin Amount (<span class="commission_container">0</span>%)</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i><span id="commission_amount"></span></span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Collection Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Logistic Cost</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Fulfillment Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Processing Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Packaging Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Special Packaging Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>

		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>GST (<span class="tax_rate_container"></span>%)</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i><span id="gst_amount"></span></span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5 ">
		                                        <div class="col-md-6 text-left">
		                                            <label><strong>Net Seller Payable (NSP):</strong></label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><strong><i class="fa fa-inr"></i><span id="nsp_amount"></span></strong></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>

								</div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-md-2 control-label"><?php echo lang('saleprice');?>:
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'saleprice', 'value'=>set_value('saleprice', $saleprice), 'id'=>'price-input-2', 'class'=>'form-control');
									echo form_input($data);?>

									<span class="pull-left"><a href="javascript:;" id="show-breakup-btn-2">Show Breakup</a></span>
									<div class="row mtb-10" id="breakup-container-2" style="display: none;">
		                                <div class="col-md-8 col-sm-10 col-sm-offset-1 bkgGrey col-md-offset-2 mtb-10">
		                                    <div class="row mt-15">
		                                        <div class="col-md-6 text-left">
		                                            <label>Gojojo Margin Amount (<span class="commission_container">0</span>%)</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i><span id="commission_amount_2"></span></span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Collection Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Logistic Cost</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Fulfillment Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Processing Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Packaging Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>Special Packaging Charges</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i>0</span>
		                                        </div>
		                                    </div>

		                                    <div class="row mtb-5">
		                                        <div class="col-md-6 text-left">
		                                            <label>GST</label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><i class="fa fa-inr"></i><span id="gst_amount_2"></span></span>
		                                        </div>
		                                    </div>
		                                    <div class="row mtb-5 ">
		                                        <div class="col-md-6 text-left">
		                                            <label><strong>Net Seller Payable (NSP):</strong></label>
		                                        </div>
		                                        <div class="col-md-6 text-right">
		                                            <span><strong><i class="fa fa-inr"></i><span id="nsp_amount_2"></span></strong></span>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>

								</div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Payment Mode:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
								 	$options = array(	 '1'	=> 'Prepaid Only'
														,'0'	=> 'COD and Prepaid Both'
														);
									echo form_dropdown('payment_mode', $options, set_value('payment_mode',$payment_mode), 'class="table-group-action-input form-control input-medium"');
									?>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Shipping Charges:
		                            <span class="required"> <br>[0 for free shipping]* </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
									$data	= array('name'=>'shipping_charges', 'value'=>set_value('shipping_charges', $shipping_charges), 'class'=>'form-control');
									echo form_input($data);?>
								</div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Shipping:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
								 	$options = array(	 '1'	=> lang('shippable')
														,'0'	=> lang('not_shippable')
														);
									echo form_dropdown('shippable', $options, set_value('shippable',$shippable), 'class="table-group-action-input form-control input-medium"');
									?>
		                        </div>
		                    </div>

		                    <div class="form-group hide">
		                        <label class="col-md-2 control-label">Taxable:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
								 	$options = array(	 '1'	=> lang('taxable')
														,'0'	=> lang('not_taxable')
														);
									echo form_dropdown('taxable', $options, set_value('taxable',$taxable), 'class="table-group-action-input form-control input-medium"');
									?>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="col-md-2 control-label">Status:
		                            <span class="required"> * </span>
		                        </label>
		                        <div class="col-md-10">
		                            <?php
								 	$options = array(	 '0'	=> lang('disabled')
														,'1'	=> lang('enabled')
														);
									echo form_dropdown('enabled', $options, set_value('enabled',$enabled), 'class="table-group-action-input form-control input-medium"');
									?>
		                        </div>
		                    </div>

						</div>
					</div>

					<div class="tab-pane" id="tab_inventory">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
			                        <label class="col-md-2 control-label"><?php echo lang('track_stock');?>:
			                        </label>
			                        <div class="col-md-10">
			                            <?php
									 	$options = array(	 '1'	=> lang('yes')
															,'0'	=> lang('no')
															);
										echo form_dropdown('track_stock', $options, set_value('track_stock',$track_stock), 'class="col-md-3 form-control"');
										?>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <label class="col-md-2 control-label"><?php echo lang('fixed_quantity');?>:
			                            <span class="required"> * </span>
			                        </label>
			                        <div class="col-md-10">
			                            <?php
									 	$options = array(	 '0'	=> lang('no')
															,'1'	=> lang('yes')
															);
										echo form_dropdown('fixed_quantity', $options, set_value('fixed_quantity',$fixed_quantity), 'class="col-md-3 form-control"');
										?>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <label class="col-md-2 control-label"><?php echo lang('quantity');?>:
			                            <span class="required"> * </span>
			                        </label>
			                        <div class="col-md-10">
			                            <?php
										$data	= array('name'=>'quantity', 'value'=>set_value('quantity', $quantity), 'class'=>'col-md-2 form-control');
										echo form_input($data);
										?>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>
					
					<div class="tab-pane" id="tab_meta">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group hide">
			                        <label class="col-md-2 control-label"><?php echo lang('slug');?>:
			                        </label>
			                        <div class="col-md-10">
			                            <?php
										$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'form-control');
										echo form_input($data);?>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <label class="col-md-2 control-label"><?php echo lang('seo_title');?>:
			                        </label>
			                        <div class="col-md-10">
			                            <?php
										$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'form-control');
										echo form_input($data);
										?>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <label class="col-md-2 control-label"><?php echo lang('meta');?>:
			                        </label>
			                        <div class="col-md-10">
			                            <?php
										$data	= array('name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'form-control');
										echo form_textarea($data);
										?>
			                        </div>
			                    </div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="categories-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					            	<a href="javascript:;" id="save-category-btn" class="pull-right btn btn-primary" style="display: none;">Save</a>
					                <h4 class="modal-title" id="myModalLabel"><?php echo lang('select_a_category');?></h4>
					            </div>
					            <div class="modal-body">
					               	<div class="row">
					               		<div class="col-md-12">
						                	<?php if(isset($categories[0])):?>
												<table class="table table-striped">
													<?php
													function list_categories($parent_id, $cats, $sub='', $product_categories) {
									
														foreach ($cats[$parent_id] as $cat):

															if(isset($cats[$cat->id])){
										                    	$has_child_string = 'readonly="readonly" disabled="disabled" ';
										                  	} else {
										                    	$has_child_string = '';
										                  	}
														?>
														<tr>
															<td><?php echo  $sub.$cat->name; ?></td>
															<td>
																<input <?php echo $has_child_string;?> id="cat-<?php echo $cat->id;?>" data-parent="cat-<?php echo $parent_id;?>" class="category-checkbox" type="checkbox" name="categories[]" value="<?php echo $cat->id;?>" <?php echo(in_array($cat->id, $product_categories))?'checked="checked"':'';?>/>
															</td>
														</tr>
														<?php
														if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0)
														{
															$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
																$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
															list_categories($cat->id, $cats, $sub2, $product_categories);
														}
														endforeach;
													}
												
													list_categories(0, $categories, '', $product_categories);
													?>
												</table>
											<?php else:?>
												<div class="alert"><?php echo lang('no_available_categories');?></div>
											<?php endif;?> 
										</div>  
					               	</div>
					            </div>
					        </div>
					    </div>
					</div>

					<div class="tab-pane" id="product_downloads">
						<div class="alert alert-info">
							<?php echo lang('digital_products_desc'); ?>
						</div>
						<fieldset>
							<table class="table table-striped">
								<thead>
									<tr>
										<th><?php echo lang('filename');?></th>
										<th><?php echo lang('title');?></th>
										<th style="width:70px;"><?php echo lang('size');?></th>
										<th style="width:16px;"></th>
									</tr>
								</thead>
								<tbody>
								<?php echo (count($file_list) < 1)?'<tr><td style="text-align:center;" colspan="6">'.lang('no_files').'</td></tr>':''?>
								<?php foreach ($file_list as $file):?>
									<tr>
										<td><?php echo $file->filename ?></td>
										<td><?php echo $file->title ?></td>
										<td><?php echo $file->size ?></td>
										<td><?php echo form_checkbox('downloads[]', $file->id, in_array($file->id, $product_files)); ?></td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</fieldset>
					</div>
					
					<div class="tab-pane" id="product_options">
						<div class="row">
							<div class="col-md-12">
								<div class="pull-right">
									<div class="input-group">
										<select id="option_options" class="form-control">
											<option value=""><?php echo lang('select_option_type')?></option>
											<option value="checklist"><?php echo lang('checklist');?></option>
											<option value="radiolist"><?php echo lang('radiolist');?></option>
											<option value="droplist"><?php echo lang('droplist');?></option>
											<option value="textfield"><?php echo lang('textfield');?></option>
											<option value="textarea"><?php echo lang('textarea');?></option>
										</select>
										<span class="input-group-btn">
											<input id="add_option" class="btn btn-info pull-right" type="button" value="<?php echo lang('add_option');?>" style="margin:0px;"/>
										</span>
									</div>	
								</div>
							</div>
						</div>
						
						<script type="text/javascript">
						
						$( "#add_option" ).click(function(){
							if($('#option_options').val() != '')
							{
								add_option($('#option_options').val());
								$('#option_options').val('');
							}
						});
						
						function add_option(type)
						{
							//increase option_count by 1
							option_count++;
							
							<?php
							$value			= array(array('name'=>'', 'value'=>'', 'weight'=>'', 'price'=>'', 'limit'=>'', 'image'=>''));
							$js_textfield	= (object)array('name'=>'', 'type'=>'textfield', 'required'=>false, 'html_class'=>'', 'hint'=>'', 'values'=>$value);
							$js_textarea	= (object)array('name'=>'', 'type'=>'textarea', 'required'=>false, 'html_class'=>'', 'hint'=>'', 'values'=>$value);
							$js_radiolist	= (object)array('name'=>'', 'type'=>'radiolist', 'required'=>false, 'html_class'=>'', 'hint'=>'', 'values'=>$value);
							$js_checklist	= (object)array('name'=>'', 'type'=>'checklist', 'required'=>false, 'html_class'=>'', 'hint'=>'', 'values'=>$value);
							$js_droplist	= (object)array('name'=>'', 'type'=>'droplist', 'required'=>false, 'html_class'=>'', 'hint'=>'', 'values'=>$value);
							?>
							if(type == 'textfield')
							{
								$('#options_container').append('<?php add_option($js_textfield, "'+option_count+'");?>');
							}
							else if(type == 'textarea')
							{
								$('#options_container').append('<?php add_option($js_textarea, "'+option_count+'");?>');
							}
							else if(type == 'radiolist')
							{
								$('#options_container').append('<?php add_option($js_radiolist, "'+option_count+'");?>');
							}
							else if(type == 'checklist')
							{
								$('#options_container').append('<?php add_option($js_checklist, "'+option_count+'");?>');
							}
							else if(type == 'droplist')
							{
								$('#options_container').append('<?php add_option($js_droplist, "'+option_count+'");?>');
							}
						}
						
						function add_option_value(option)
						{
							
							option_value_count++;
							<?php
							$js_po	= (object)array('type'=>'radiolist');
							$value	= (object)array('name'=>'', 'value'=>'', 'weight'=>'', 'price'=>'', 'image'=>'');
							?>
							$('#option-items-'+option).append('<?php add_option_value($js_po, "'+option+'", "'+option_value_count+'", $value);?>');
						}
						
						$(document).ready(function(){
							$('body').on('click', '.option_title', function(){
								$($(this).attr('href')).slideToggle();
								return false;
							});
							
							$('body').on('click', '.delete-option-value', function(){
								if(confirm('<?php echo lang('confirm_remove_value');?>'))
								{
									$(this).closest('.option-values-form').remove();
								}
							});
							
							
							
							$('#options_container').sortable({
								axis: "y",
								items:'tr',
								handle:'.handle',
								forceHelperSize: true,
								forcePlaceholderSize: true
							});
							
							$('.option-items').sortable({
								axis: "y",
								handle:'.handle',
								forceHelperSize: true,
								forcePlaceholderSize: true
							});
						});
						</script>
						<style type="text/css">
							.option-form {
								display:none;
								margin-top:10px;
							}
							.option-values-form
							{
								background-color:#fff;
								padding:6px 3px 6px 6px;
								-webkit-border-radius: 3px;
								-moz-border-radius: 3px;
								border-radius: 3px;
								margin-bottom:5px;
								border:1px solid #ddd;
							}
							
							.option-values-form input {
								margin:0px;
							}
							.option-values-form a {
								margin-top:3px;
							}
						</style>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped"  id="options_container">
									<?php
									$counter	= 0;
									if(!empty($product_options))
									
									{
										foreach($product_options as $po)
										{
											$po	= (object)$po;
											if(empty($po->required)){$po->required = false;}

											add_option($po, $counter);
											$counter++;
										}
									}?>
										
								</table>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="product_related">
						<div class="row">
							<div class="col-md-8">
								<label><strong><?php echo lang('select_a_product');?></strong></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="text-align:center">
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" type="text" id="product_search" placeholder="Search Product..." />
										<script type="text/javascript">
										$('#product_search').keyup(function(){
											$('#product_list').html('');
											run_product_query();
										});
								
										function run_product_query()
										{
											$.post("<?php echo site_url($this->config->item('merchant_folder').'/products/product_autocomplete/');?>", { name: $('#product_search').val(), limit:10},
												function(data) {
											
													$('#product_list').html('');
											
													$.each(data, function(index, value){
											
														if($('#related_product_'+index).length == 0)
														{
															$('#product_list').append('<option id="product_item_'+index+'" value="'+index+'">'+value+'</option>');
														}
													});
											
											}, 'json');
										}
										</script>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<select class="form-control" id="product_list" size="5" style="margin:0px;"></select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top:8px;">
										<a href="#" onclick="add_related_product();return false;" class="btn purple" title="Add Related Product"><?php echo lang('add_related_product');?></a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<table class="table table-striped" style="margin-top:10px;">
									<tbody id="product_items_container">
									<?php
									foreach($related_products as $rel)
									{
										echo related_items($rel->id, $rel->name);
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="product_brand">
						<div class="row">
							<div class="col-md-12">
								<label class="control-label"><strong>Select a brand</strong><span class="required"> * </span></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<select class="select2-ajax" style="width: 100%;" name="brand">
									<option value="<?php echo $brand;?>" selected="selected"><?php echo $brand_name;?></option>
								</select>

								<script type="text/javascript">
									$(".select2-ajax").select2({
										tags: false,
										placeholder: "Search Brands...",
										tokenSeparators: [',', ' '],
										minimumInputLength: 1,
										ajax: {
											url: "<?php echo site_url($this->config->item('merchant_folder').'/brands/search');?>",
											dataType: "json",
											type: "POST",
											data: function (params) {

												var queryParameters = {
													name: params.term,
													limit: 10
												}
												return queryParameters;
											},
											processResults: function (data) {
												return {
													results: $.map(data, function (item) {
														return {
															text: item.name,
															id: item.id
														}
													})
												};
											}
										}
									});
								</script>
							</div>
						</div>
					</div>
					
					<div class="tab-pane" id="product_photos">
						<div class="row">
							<!-- <iframe id="iframe_uploader" src="<?php echo site_url($this->config->item('merchant_folder').'/products/product_image_form');?>" class="col-md-12" style="height:75px; border:0px;"></iframe> -->
							<iframe id="iframe_uploader" src="<?php echo site_url($this->config->item('merchant_folder').'/products/product_image_form_multiple');?>" class="col-md-12" style="height:75px; border:0px;"></iframe>
						</div>
						<div class="row">
							<hr>
							<div class="col-md-12">
								
								<div id="gc_photos">
									
								<?php
								foreach($images as $photo_id=>$photo_obj)
								{
									if(!empty($photo_obj))
									{
										$photo = (array)$photo_obj;
										add_image($photo_id, $photo['filename'], $photo['alt'], $photo['caption'], isset($photo['primary']));
									}

								}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</form>

<?php
function add_image($photo_id, $filename, $alt, $caption, $primary=false)
{

	ob_start();
	?>
	<div class="row gc_photo" id="gc_photo_<?php echo $photo_id;?>" style="background-color:#fff; border-bottom:1px solid #ddd; padding-bottom:20px; margin-bottom:20px;">
		<div class="col-md-3">
			<input type="hidden" name="images[<?php echo $photo_id;?>][filename]" value="<?php echo $filename;?>"/>
			<img class="gc_thumbnail" src="<?php echo base_url('uploads/images/thumbnails/'.$filename);?>" style="padding:5px; border:1px solid #ddd"/>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-4">
					<label><?php echo lang('alt_tag');?>:</label>
					<input name="images[<?php echo $photo_id;?>][alt]" value="<?php echo $alt;?>" class="form-control" placeholder="<?php echo lang('alt_tag');?>"/>
				</div>
				<div class="col-md-4">
					<input type="radio" name="primary_image" value="<?php echo $photo_id;?>" <?php if($primary) echo 'checked="checked"';?>/> <?php echo lang('primary');?>
				</div>
				<div class="col-md-4">
					<a onclick="return remove_image($(this));" rel="<?php echo $photo_id;?>" class="btn btn-danger" style="float:right; font-size:9px;"><i class="icon-trash icon-white"></i> <?php echo lang('remove');?></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label><?php echo lang('caption');?>:</label>
					<textarea name="images[<?php echo $photo_id;?>][caption]" class="form-control" rows="3"><?php echo $caption;?></textarea>
				</div>
			</div>
		</div>
	</div>

	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}


function add_option($po, $count)
{
	ob_start();
	?>
	<tr id="option-<?php echo $count;?>">
		<td>
			<div class="col-md-6 pl0">
				<a class="handle btn btn-mini"><i class="fa fa-align-justify"></i></a>
				<strong><a class="option_title" href="#option-form-<?php echo $count;?>"><?php echo $po->type;?> <?php echo (!empty($po->name))?' : '.$po->name:'';?></a></strong>
			</div>
			<div class="col-md-6 pr0">
				<div class="checkbox text-right">
					<label><input class="checkbox" type="checkbox" name="option[<?php echo $count;?>][required]" value="1" <?php echo ($po->required)?'checked="checked"':'';?>/><?php echo lang('required');?></label>
					&nbsp;&nbsp;
					<button type="button" class="btn btn-sm btn-danger pull-right" onclick="remove_option(<?php echo $count ?>);"><i class="icon-trash icon-white"></i></button>
				</div>
			</div>
			<input type="hidden" name="option[<?php echo $count;?>][type]" value="<?php echo $po->type;?>" />

			<div class="option-form col-md-10 col-md-offset-1" id="option-form-<?php echo $count;?>">
				<div class="row">
					<div class="col-md-12">
				
						<div class="form-group">
	                        <label class="col-md-2 control-label">Name:
	                            <span class="required"> * </span>
	                        </label>
	                        <div class="col-md-10">
	                            <input type="text" class="form-control" placeholder="<?php echo lang('option_name');?>" name="option[<?php echo $count;?>][name]" value="<?php echo $po->name;?>"/>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-md-2 control-label">HTML Class:
	                        </label>
	                        <div class="col-md-10">
	                            <input type="text" class="form-control" placeholder="HTML Class" name="option[<?php echo $count;?>][html_class]" value="<?php echo $po->html_class;?>"/>
	                        </div>
	                    </div>

						<div class="form-group">
	                        <label class="col-md-2 control-label">Details:
	                        </label>
	                        <div class="col-md-10">
	                            <textarea class="form-control" placeholder="Hint" name="option[<?php echo $count;?>][hint]"><?php echo $po->hint;?></textarea>
	                        </div>
	                    </div>	                    

                    </div>
                </div>
				

				<?php if($po->type!='textarea' && $po->type!='textfield'):?>
				<div class="row">
					<div class="col-md-12">
						<a class="btn purple" onclick="add_option_value(<?php echo $count;?>);"><?php echo lang('add_item');?></a>
					</div>
				</div>
				<?php endif;?>
				
				<div class="row" style="margin-top:10px;">
					<div class="col-md-12">
						<?php if($po->type!='textarea' && $po->type!='textfield'):?>
						<div class="col-md-1">&nbsp;</div>
						<?php endif;?>
						<div class="col-md-2"><strong>&nbsp;&nbsp;<?php echo lang('name');?></strong></div>
						<div class="col-md-2"><strong>&nbsp;<?php echo lang('value');?></strong></div>
						<div class="col-md-2"><strong>&nbsp;<?php echo lang('weight');?></strong></div>
						<div class="col-md-2"><strong>&nbsp;<?php echo lang('price');?></strong></div>

						<div class="col-md-2">
							<?php if($po->type == 'radiolist'):?>
								<strong>&nbsp;<?php echo lang('image');?></strong>
							<?php endif;?>
						</div>

						<div class="col-md-1"><strong>&nbsp;<?php echo ($po->type=='textfield')?lang('limit'):'';?></strong></div>
					</div>
				</div>
				<div class="option-items" id="option-items-<?php echo $count;?>">
				<?php if($po->values):?>
					<?php
					foreach($po->values as $value)
					{
						$value = (object)$value;
						add_option_value($po, $count, $GLOBALS['option_value_count'], $value);
						$GLOBALS['option_value_count']++;
					}?>
				<?php endif;?>
				</div>
			</div>
		</td>
	</tr>
	
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}

function add_option_value($po, $count, $valcount, $value)
{
	ob_start();
	?>
	<div class="option-values-form">
		<div class="row">
			<?php if($po->type!='textarea' && $po->type!='textfield'):?><div class="col-md-1"><a class="handle btn btn-mini" style="float:left;"><i class="fa fa-align-justify"></i></a></div><?php endif;?>
			<div class="col-md-2"><input type="text" class="col-md-12 form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][name]" value="<?php echo $value->name ?>" /></div>
			<div class="col-md-2"><input type="text" class="col-md-12 form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][value]" value="<?php echo $value->value ?>" /></div>
			<div class="col-md-2"><input type="text" class="col-md-12 form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][weight]" value="<?php echo $value->weight ?>" /></div>
			<div class="col-md-2"><input type="text" class="col-md-12 form-control" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][price]" value="<?php echo $value->price ?>" /></div>
			
			<div class="col-md-2">

				<?php if($po->type == 'radiolist' || $po->type == 'checklist' || $po->type == 'droplist'):?>

					<?php $img_field_name = 'option-img-'.$count.'-'.$valcount;?>
					<input class="hidden col-md-12" id="<?php echo $img_field_name;?>" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][image]" value="<?php echo $value->image ?>">
					
					<?php
					$option_sm_image_class = '';
					if($value->image == ''){
						$option_sm_image_class = 'hidden';
					}
					?>
					<img src="<?php echo base_url('uploads/options').'/'.$value->image ?>" id="<?php echo $img_field_name;?>-image" class="option-sm-image <?php echo $option_sm_image_class;?>">

					<?php $filemanager_short_url = 'assets/filemanager/dialog.php?merchant_id=3&type=1&field_id='.$img_field_name;?>
					
					<?php echo '<a href="'.base_url($filemanager_short_url);?>" class="btn iframe-btn btn-info btn-xs" type="button"><i class="icon-picture icon-white"></i></a>
					<a href="javascript:void(0);" id="remove-option-image-<?php echo $img_field_name;?>" class="btn btn-danger btn-xs remove-option-image <?php echo $option_sm_image_class;?>" type="button" data-field-id="<?php echo $img_field_name;?>"><i class="icon-trash icon-white"></i></a>
				
				<?php endif;?>

			</div>
			
			<div class="col-md-1">
			<?php if($po->type=='textfield'):?>
				<input class="form-control" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][limit]" value="<?php echo $value->limit ?>" />
			<?php elseif($po->type!='textarea' && $po->type!='textfield'):?>
				<a class="delete-option-value btn btn-danger btn-mini pull-right"><i class="icon-trash icon-white"></i></a>
			<?php endif;?>
			</div>
		</div>
	</div>
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();

	echo replace_newline($stuff);
}
//this makes it easy to use the same code for initial generation of the form as well as javascript additions
function replace_newline($string) {
  return trim((string)str_replace(array("\r", "\r\n", "\n", "\t"), ' ', $string));
}
?>
<script type="text/javascript">
//<![CDATA[
var option_count		= <?php echo $counter?>;
var option_value_count	= <?php echo $GLOBALS['option_value_count'];?>

function add_related_product()
{
	//if the related product is not already a related product, add it
	if($('#related_product_'+$('#product_list').val()).length == 0 && $('#product_list').val() != null)
	{
		<?php $new_item	 = str_replace(array("\n", "\t", "\r"),'',related_items("'+$('#product_list').val()+'", "'+$('#product_item_'+$('#product_list').val()).html()+'"));?>
		var related_product = '<?php echo $new_item;?>';
		$('#product_items_container').append(related_product);
		run_product_query();
	}
	else
	{
		if($('#product_list').val() == null)
		{
			alert('<?php echo lang('alert_select_product');?>');
		}
		else
		{
			alert('<?php echo lang('alert_product_related');?>');
		}
	}
}

function remove_related_product(id)
{
	if(confirm('<?php echo lang('confirm_remove_related');?>'))
	{
		$('#related_product_'+id).remove();
		run_product_query();
	}
}

function photos_sortable()
{
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}
//]]>
</script>
<script type="text/javascript">
function toggle_save_category_btn(){
	var btn = $('#save-category-btn');
	var atLeastOneIsChecked = $('.category-checkbox').is(':checked');
	if (atLeastOneIsChecked){
		btn.show();
	} else {
	   btn.hide();
	}
}

$(function(){
  $('.category-checkbox').on('click',function (event) {
    var ele = $(this);

    //uncheck all before doing so
    $('.category-checkbox').not(this).each(function(){
    	$(this).prop("checked", false);
    });

    if(this.checked) {
      this.checked = true;
      var parent_id = $(this).data('parent').substring(4);
      parent_selector(parent_id, 'check');
    } else {
      this.checked = false;
      var parent_id = $(this).data('parent').substring(4);
      parent_selector(parent_id, 'uncheck');
    }

    toggle_save_category_btn();
  });

  function parent_selector(parent_id, mode){
    if(parent_id != 0){
      var ele = $("#cat-"+parent_id);

      if(mode == 'check'){
        ele.prop('checked', true);
      } else {
        var childs = $('[data-parent="cat-'+parent_id+'"]');
        var stcheck = false;
        $.each(childs, function(i,row){
          if($(row).prop('checked') == true){
            stcheck = true;
          }
        });
        
        if(stcheck == false){
          ele.prop('checked', false);
        }  
      }
      
      var parent_id_this = ele.data('parent').substring(4);
      parent_selector(parent_id_this, mode);
    }
  }

});
</script>
<?php
function related_items($id, $name) {
	return '
			<tr id="related_product_'.$id.'">
				<td>
					<input type="hidden" name="related_products[]" value="'.$id.'"/>
					'.$name.'</td>
				<td>
					<a class="btn btn-danger pull-right btn-mini" href="#" onclick="remove_related_product('.$id.'); return false;"><i class="icon-trash icon-white"></i> '.lang('remove').'</a>
				</td>
			</tr>
		';
}
?>

<script type="text/javascript">
$(function(){
	$('#price-input').on('keyup',function(){
		breakup_calculator();
		breakup_calculator_2();
	});
	$('#show-breakup-btn').on('click',function(){
		breakup_calculator();
		breakup_calculator_2();
		$('#breakup-container').toggle();
	});

	function breakup_calculator(){
		var input = $('#price-input').val();

		if(input != ''){
			input = parseFloat(input);
		} else {
			input = 0;
		}

		var commission = parseFloat(global_commission);
		var commission_amount = (input*commission)/100;
		$('#commission_amount').html(commission_amount);

		var gst = parseFloat(global_tax_rate);
		var gst_amount = (input*gst)/100;
		$('#gst_amount').html(gst_amount);

		var nsp_amount = input - (commission_amount+gst_amount)
		$('#nsp_amount').html(nsp_amount);
	}

	$('#price-input-2').on('keyup',function(){
		breakup_calculator_2();
	});
	$('#show-breakup-btn-2').on('click',function(){
		breakup_calculator_2();
		$('#breakup-container-2').toggle();
	});

	function breakup_calculator_2(){
		var input = $('#price-input-2').val();

		if(input != ''){
			input = parseFloat(input);
		} else {
			input = 0;
		}

		if(input <= 0){
			input = $('#price-input').val();
			if(input != ''){
				input = parseFloat(input);
			} else {
				input = 0;
			}
			if(input <= 0){
				$('#breakup-container-2').hide();
			}
		}

		var commission = parseFloat(global_commission);
		var commission_amount = (input*commission)/100;
		$('#commission_amount_2').html(commission_amount);

		var gst = parseFloat(global_tax_rate);
		var gst_amount = (input*gst)/100;
		$('#gst_amount_2').html(gst_amount);

		var nsp_amount = input - (commission_amount+gst_amount)
		$('#nsp_amount_2').html(nsp_amount);
	}
});
</script>

<script type="text/javascript">
$(function(){
	$('.commission_container').html(global_commission);
	$('.tax_rate_container').html(global_tax_rate);

	$('#categories-modal').modal({
		backdrop: 'static',
    	keyboard: false
    });
    $('#save-category-btn').on('click',function(e){
    	e.preventDefault();

    	var idval = false;
    	$('.category-checkbox:checked').each(function(){
	    	idval = $(this).val();
	    });

    	if(idval){
	    	$.ajax({
	        	url  	: '<?php echo site_url();?>merchant/categories/get_category/'+idval,
	        	method	: 'GET'
	        }).done(function(getdata){
	        	getdata = JSON.parse(getdata);
	        	if(getdata.success){
	        		global_tax_rate = getdata.data.tax_rate;
	        		global_commission = getdata.data.commission;
	        		$('.commission_container').html(global_commission);
	        		$('.tax_rate_container').html(global_tax_rate);
	        		$('#categories-modal').modal('hide');
	        	} else {
	        		location.reload();
	        	}
	        });
	    }
    });
});
</script>