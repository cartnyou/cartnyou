<div class="columns-container pdt-129-header">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="<?php echo site_url();?>" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">Order Confirmed</span>
        </div>

        <div class="row">

            <div class="center_column col-xs-12 col-sm-12" id="center_column">

               	<h2><?php echo lang('order_number');?>: <?php echo $order_id;?></h2>
            	<hr>

            	<div class="alert alert-info">
	                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
	                <div>Your order has been placed with Order ID <?php echo $order_id;?></div>
	            </div>
		    
            	<?php
				// content defined in canned messages
				echo $download_section;
				?>

                <div class="row">
					<div class="col-md-4 mtb-10">
						<h3><?php echo lang('account_information');?></h3>
						<?php echo (!empty($customer['company']))?$customer['company'].'<br/>':'';?>
						<?php echo ucwords($customer['firstname']);?> <?php echo ucwords($customer['lastname']);?><br/>
						<?php echo $customer['phone'];?><br/>
						<?php echo $customer['email'];?>
					</div>
					<?php
					$ship = $customer['ship_address'];
					$bill = $customer['bill_address'];
					?>
					<div class="col-md-4 mtb-10">
						<h3><?php echo ($ship != $bill)?lang('shipping_information'):lang('shipping_and_billing');?></h3>
						<!-- <?php echo format_address($ship, TRUE);?><br/>
						<?php echo $ship['email'];?><br/>
						<?php echo $ship['phone'];?> -->

						<?php echo ucwords($ship['firstname'].' '.$ship['lastname']);?><br>
						<?php echo $ship['phone'];?><br>
						<?php echo format_address($ship, TRUE,TRUE);?>

					</div>
					<!-- <?php if($ship != $bill):?>
					<div class="col-md-4">
						<h3><?php echo lang('billing_information');?></h3>
						<?php echo format_address($bill, TRUE);?><br/>
						<?php echo $bill['email'];?><br/>
						<?php echo $bill['phone'];?>
					</div>
					<?php endif;?> -->

					<div class="col-md-4 mtb-10">
						<h3><?php echo lang('payment_information');?></h3>
						<?php echo $payment['description']; ?>
					</div>

				</div>

				<!-- <div class="row">
					<div class="col-md-4">
						<h3><?php echo lang('additional_details');?></h3>
						<?php
						if(!empty($referral)):?><div><strong><?php echo lang('heard_about');?></strong> <?php echo $referral;?></div><?php endif;?>
						<?php if(!empty($shipping_notes)):?><div><strong><?php echo lang('shipping_instructions');?></strong> <?php echo $shipping_notes;?></div><?php endif;?>
					</div>

					<div class="col-md-4">
						<h3 style="padding-top:10px;"><?php echo lang('shipping_method');?></h3>
						<?php echo $shipping['method']; ?>
					</div>
					
					<div class="col-md-4">
						<h3><?php echo lang('payment_information');?></h3>
						<?php echo $payment['description']; ?>
					</div>
					
				</div> -->

				<div class="table-responsive">
                    <table class="table table-bordered table-striped table-responsive" style="margin-top:20px;">
                        <thead>
                        <tr>
                            <!-- <th style="width:10%;"><?php echo lang('sku');?></th> -->
                            <th style="width:30%;">Product Name</th>
                            <th><?php echo lang('description');?></th>
                            <th style="width:10%;"><?php echo lang('price');?></th>
                            <th style="width:10%;"><?php echo lang('quantity');?></th>
                            <th style="width:8%;"><?php echo lang('totals');?></th>
                        </tr>
                        </thead>

                        <tfoot>
                        <?php if($go_cart['group_discount'] > 0)  : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('group_discount');?></strong></td>
                                <td><?php echo format_currency(0-$go_cart['group_discount']); ?></td>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <td colspan="4"><strong><?php echo lang('subtotal');?></strong></td>
                            <td><?php echo format_currency($go_cart['subtotal']); ?></td>
                        </tr>

                        <?php if($go_cart['coupon_discount'] > 0)  : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('coupon_discount');?></strong></td>
                                <td><?php echo format_currency(0-$go_cart['coupon_discount']); ?></td>
                            </tr>

                            <?php if($go_cart['order_tax'] != 0) : // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?>
                                <tr>
                                    <td colspan="4"><strong><?php echo lang('discounted_subtotal');?></strong></td>
                                    <td><?php echo format_currency($go_cart['discounted_subtotal']); ?></td>
                                </tr>
                            <?php endif;

                        endif; ?>
                        <?php // Show shipping cost if added before taxes
                        if($this->config->item('tax_shipping') && $go_cart['shipping_cost']>0) : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('shipping');?></strong></td>
                                <td><?php echo format_currency($go_cart['shipping_cost']); ?></td>
                            </tr>
                        <?php endif ?>

                        <?php if($go_cart['order_tax'] != 0) : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('taxes');?></strong></td>
                                <td><?php echo format_currency($go_cart['order_tax']); ?></td>
                            </tr>
                        <?php endif;?>

                        <?php // Show shipping cost if added after taxes
                        if(!$this->config->item('tax_shipping') && $go_cart['shipping_cost']>0) : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('shipping');?></strong></td>
                                <td><?php echo format_currency($go_cart['shipping_cost']); ?></td>
                            </tr>
                        <?php endif;?>

                        <?php if($go_cart['gift_card_discount'] != 0) : ?>
                            <tr>
                                <td colspan="4"><strong><?php echo lang('gift_card');?></strong></td>
                                <td><?php echo format_currency(0-$go_cart['gift_card_discount']); ?></td>
                            </tr>
                        <?php endif;?>
                        <tr>
                            <td colspan="4"><strong><?php echo lang('grand_total');?></strong></td>
                            <td><?php echo format_currency($go_cart['total']); ?></td>
                        </tr>
                        </tfoot>

                        <tbody>
                        <?php
                        $subtotal = 0;
                        foreach ($go_cart['contents'] as $cartkey=>$product):?>
                            <tr>
                                <!-- <td><?php echo $product['sku'];?></td> -->
                                <td><a href="<?php echo site_url().$product['slug'];?>" target="_blank"><?php echo ucwords($product['name']);?></a></td>
                                <td><?php echo $product['excerpt'];
                                    if(isset($product['options'])) {
                                        foreach ($product['options'] as $name=>$value)
                                        {
                                            if(is_array($value))
                                            {
                                                echo '<div><span class="gc_option_name">'.$name.':</span><br/>';
                                                foreach($value as $item)
                                                    echo '- '.$item.'<br/>';
                                                echo '</div>';
                                            }
                                            else
                                            {
                                                echo '<div><span class="gc_option_name">'.$name.':</span> '.$value.'</div>';
                                            }
                                        }
                                    }
                                    echo "<b>Seller:</b> ".$product['store_name'];
                                    ?>
                                </td>
                                <td><?php echo format_currency($product['price']);   ?></td>
                                <td><?php echo $product['quantity'];?></td>
                                <td><?php echo format_currency($product['price']*$product['quantity']); ?></td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

		    </div>
		</div>
    </div>
</div>

<?php
function getTransactionJs($order_id, $go_cart) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '$order_id',
  'affiliation': 'Gojojo',
  'revenue': '{$go_cart['total']}',
  'shipping': '{$go_cart['shipping_cost']}',
  'tax': '{$go_cart['order_tax']}',
  'currency': 'INR'
});
HTML;
}

function getItemJs($order_id, $product) {
  return <<<HTML
ga('ecommerce:addItem', {
  'id': '$order_id',
  'name': '{$product['name']}',
  'sku': '{$product['sku']}',
  'category': 'Category',
  'price': '{$product['price']}',
  'quantity': '{$product['quantity']}',
  'currency': 'INR'
});
HTML;
}
?>
<script>
ga('require', 'ecommerce');

<?php
echo getTransactionJs($order_id, $go_cart);

foreach ($go_cart['contents'] as $cartkey=>$product) {
  echo getItemJs($order_id, $product);
}
?>

ga('ecommerce:send');
</script>