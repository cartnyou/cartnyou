<script type="text/javascript">
$(function(){
$("#datepicker1").datepicker({dateFormat: 'dd-mm-yy', altField: '#datepicker1_alt', altFormat: 'yy-mm-dd'}).attr('readonly', 'readonly');
$("#datepicker2").datepicker({dateFormat: 'dd-mm-yy', altField: '#datepicker2_alt', altFormat: 'yy-mm-dd'}).attr('readonly', 'readonly');
});
</script>

<?php echo form_open($this->config->item('merchant_folder').'/coupons/form/'.$id, array('class'=>'form-horizontal')); ?>

<div class="alert alert-info" style="text-align:center;">
	<strong><?php echo sprintf(lang('times_used'), @$num_uses);?></strong>
</div>
<div class="row">
	<div class="col-md-4">
		<fieldset>
			<div class="form-group">
				<label for="code" class="col-md-12"><?php echo lang('coupon_code');?></label>
				<div class="col-md-12">
					<?php
					$data	= array('name'=>'code', 'value'=>set_value('code', $code), 'class'=>'form-control');
					echo form_input($data);
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="max_uses" class="col-md-12"><?php echo lang('max_uses');?></label>
				<div class="col-md-12">
					<?php
					$data	= array('name'=>'max_uses', 'value'=>set_value('max_uses', $max_uses), 'class'=>'form-control');
					echo form_input($data);
					?>
				</div>
			</div>
			
			<div class="form-group">
				<label for="max_product_instances" class="col-md-12"><?php echo lang('limit_per_order')?></label>
				<div class="col-md-12">
					<?php
					$data	= array('name'=>'max_product_instances', 'value'=>set_value('max_product_instances', $max_product_instances), 'class'=>'form-control');
					echo form_input($data);
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="min_total" class="col-md-12"><?php echo lang('min_total')?></label>
				<div class="col-md-12">
					<?php
					$data	= array('name'=>'min_total', 'value'=>set_value('min_total', $min_total), 'class'=>'form-control');
					echo form_input($data);
					?>
				</div>
			</div>
			
			<div class="form-group">
				<label for="start_date" class="col-md-12"><?php echo lang('enable_on');?></label>
				<div class="col-md-12">
					<?php
					$data	= array('id'=>'datepicker1', 'value'=>set_value('start_date', reverse_format($start_date)), 'class'=>'form-control');
					echo form_input($data);
					?>
					<input type="button" value="Clear" class="btn" onclick="$('#datepicker1_alt').val('');$('#datepicker1').val('');" />
					<input type="hidden" name="start_date" value="<?php echo set_value('start_date', $start_date) ?>" id="datepicker1_alt" readonly />
				</div>
			</div>

			<div class="form-group">
				<label for="end_date" class="col-md-12"><?php echo lang('disable_on');?></label>
				<div class="col-md-12">
					<?php
					$data	= array('id'=>'datepicker2', 'value'=>set_value('end_date', reverse_format($end_date)), 'class'=>'form-control');
					echo form_input($data);
					?>
					<input type="button" value="Clear"  class="btn" onclick="$('#datepicker2_alt').val('');$('#datepicker2').val('');" />
					<input type="hidden" name="end_date" value="<?php echo set_value('end_date', $end_date) ?>" id="datepicker2_alt" readonly />
				</div>
			</div>

			<div class="form-group hide">
				<label for="reduction_target" class="col-md-12"><?php echo lang('coupon_type');?></label>
				<div class="col-md-12">
					<?php
				 		$options = array(
						'price'  => lang('price_discount'),
						//'shipping' => lang('free_shipping')
		               	);
						echo form_dropdown('reduction_target', $options,  $reduction_target, 'id="gc_coupon_type" class="form-control"');
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="reduction_amount" class="col-md-12">Reduction Type</label>
				<div class="col-md-12">
				<?php	$options = array(
	                  'percent'  => lang('percentage'),
					  'fixed' => lang('fixed')
	               	);
					echo ' '.form_dropdown('reduction_type', $options,  $reduction_type, 'class="form-control"');
				?>
				</div>
			</div>

			<div class="form-group">
				<label for="reduction_amount" class="col-md-12"><?php echo lang('reduction_amount')?></label>
				<div class="col-md-12">
					<?php
						$data	= array('id'=>'reduction_amount', 'name'=>'reduction_amount', 'value'=>set_value('reduction_amount', $reduction_amount), 'class'=>'form-control');
						echo form_input($data);?>
				</div>
			</div>

		</fieldset>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
		</div>

	</div>
	<div class="col-md-6 col-md-offset-1 well">
		<b>Applicable to following products:</b>

		<table class="table table-striped" style="margin-top:10px;">
			<tbody id="product_items_container">
			<?php
			foreach($added as $rel)
			{
				echo related_items($rel['id'].'_'.$rel['pm_id'], $rel['name']);
			}
			?>
			</tbody>
		</table>

		<div class="">
			<input class="form-control" type="text" id="product_search" placeholder="Search Product..." />
			<script type="text/javascript">
			$('#product_search').on('keyup',function(){
				$('#product_list').html('');
				run_product_query();
			});

			function run_product_query()
			{
				$.post("<?php echo site_url($this->config->item('merchant_folder').'/products/product_autocomplete/1');?>", { name: $('#product_search').val(), limit:10},
					function(data) {
				
						$('#product_list').html('');
				
						$.each(data, function(index, value){
				
							if($('#related_product_'+index).length == 0)
							{
								$('#product_list').append('<option id="product_item_'+index+'" value="'+index+'">'+value+'</option>');
							}
						});
				
				}, 'json');
			}
			</script>
		</div>

		<div class="">
			<select class="form-control" id="product_list" size="5" style="margin:0px;"></select>
			<a href="#" onclick="add_related_product();return false;" class="btn purple" title="Add Product">Add Product</a>
		</div>

		<!-- <div id="gc_coupon_products">
			<table width="100%" border="0" style="margin-top:10px;" cellspacing="5" cellpadding="0">
				<?php echo $product_rows; ?>
			</table>
		</div> -->
	</div>
</div>

</form>

<script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});

$(document).ready(function(){
	$("#gc_tabs").tabs();
	
	if($('#gc_coupon_type').val() == 'shipping')
	{
		$('#gc_coupon_price_fields').hide();
	}
	
	$('#gc_coupon_type').bind('change keyup', function(){
		if($(this).val() == 'price')
		{
			$('#gc_coupon_price_fields').show();
		}
		else
		{
			$('#gc_coupon_price_fields').hide();
		}
	});
	
});

</script>
<script type="text/javascript">
function add_related_product()
{
	//if the related product is not already a related product, add it
	if($('#related_product_'+$('#product_list').val()).length == 0 && $('#product_list').val() != null)
	{
		<?php $new_item	 = str_replace(array("\n", "\t", "\r"),'',related_items("'+$('#product_list').val()+'", "'+$('#product_item_'+$('#product_list').val()).html()+'"));?>
		var related_product = '<?php echo $new_item;?>';
		$('#product_items_container').append(related_product);
		run_product_query();
	}
	else
	{
		if($('#product_list').val() == null)
		{
			alert('Please select atleast a product.');
		}
		else
		{
			alert('This product is already added.');
		}
	}
}

function remove_related_product(id)
{
	if(confirm('Are you sure you want to remove this item?'))
	{
		$('#related_product_'+id).remove();
		run_product_query();
	}
}
$(document).on('click','.remove-related-product', function(){
	if(confirm('Are you sure you want to remove this item?'))
	{
		var id = $(this).attr('data-id');
		$('#related_product_'+id).remove();
		run_product_query();
	}
});
</script>
<?php
function related_items($id, $name) {
	return '
			<tr id="related_product_'.$id.'">
				<td>
					<input type="hidden" name="product[]" value="'.$id.'"/>
					'.$name.'</td>
				<td>
					<a class="btn btn-danger pull-right btn-mini remove-related-product" data-id="'.$id.'" href="#" ><i class="icon-trash icon-white"></i> '.lang('remove').'</a>
				</td>
			</tr>
		';
}