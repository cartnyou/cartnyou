<!DOCTYPE html>
<html>
  <head>
  </head>
  <body style="margin: 0; padding: 0;" align="center" bgcolor="#f6f6f6">

    <table style="background: #f9f9f9;padding: 0 10px;" width="600" cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr style="background: #f9f9f9;">
            <td width="150" style="padding: 30px 10px 20px 30px;text-align: left;border-bottom: 2px solid #ddd;">
                <a target="_blank" href="<?php echo site_url('secure/orders');?>" style="color:#0071bb;text-decoration: none; border-right: 1px solid #fff;padding-right: 5px;">Your Orders</a>
            </td>
            <td width="300" style="padding: 30px 0px 8px 10px;width: 130px;border-bottom: 2px solid #ddd;"><a href="<?php echo site_url();?>"><img style="width: 150px;max-width: 200px;margin: 0 auto;" border="0" src="<?php echo theme_img('cartnyouEmailer/img/logo.png');?>" alt="" width="150" /></a></td>
            <td width="150" style="padding: 30px 30px 20px 0px;text-align: right;border-bottom: 2px solid #ddd;">
                <a target="_blank" href="<?php echo site_url('secure/my-account');?>"  style="color:#0071bb;text-decoration: none;    padding-left: 5px;">Your Account</a>

            </td>
        </tr>
        </tbody>
    </table>



    <table width="600" style="padding: 0 10px;background: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="600" style="width:600px">
                <h3 style="text-align: center;color: #666;font-size: 40px;margin-bottom: 15px;">THANK YOU!</h3>
            </td>
        </tr>
        </tbody>
    </table>

    <table width="600" style="background-color: #f9f9f9;" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td width="600" align="left" style="background-color: #f9f9f9;padding: 15px;">
              <?php echo $message ?>
          </td>
        </tr>
      </tbody>
    </table>

    <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;padding: 0 10px;">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td align="left" width="600" style="">
                <h3 style="font-size: 18px;margin-bottom: 5px;">Thank You!</h3>
                <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">Team CartNYou</h3>
                <h3 style="font-size: 15px;margin-bottom: 5px;margin-top: 5px;">T: +91-8283004545</h3>

            </td>
        </tr>
        <tr>
            <td align="left" width="600">
                <p style="color: #666;">
                    Check out our <a href="<?php echo site_url('return-policy');?>">Easy Return Policy </a> | <a href="<?php echo site_url('terms-and-conditions');?>">Terms & Conditions</a>
                </p>
                <p style="color: #666;">
                    If you have any queries about your order please  <a href="<?php echo site_url('contact-us');?>">Contact Us</a>
                </p>
            </td>
        </tr>
        <tr>
            <td width="600" style="text-align: center;padding: 20px 0;">
                <img src="http://35.167.215.182/cartnyouEmailer/img/logo.png" width="150" alt="">
            </td>

        </tr>
        <tr>
            <td>
                <ul style="padding-left: 0;text-align: center;">
                    <li style="list-style: none;display: inline-block;padding:0 10px 5px 10px;">
                        <a href="https://www.facebook.com/CartnYou-277729052962135/"><img src="<?php echo theme_assets('cartnyouEmailer/img/facebook.png');?>" width="32" alt=""></a>
                    </li>
                    <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">
                        <a href="https://twitter.com/CartnYou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/twitter.png');?>" width="32" alt=""></a>
                    </li>
                    <li style="list-style: none;display: inline-block;padding: 0 10px 5px 10px;">

                        <a href="https://www.instagram.com/cartnyou/"><img src="<?php echo theme_assets('cartnyouEmailer/img/instagram.png');?>" width="32" alt="alt"></a>
                    </li>

                </ul>
            </td>
        </tr>

        </tbody>
    </table>
    <table width="600" cellspacing="0" cellpadding="0" border="0" style="background: #f3f3f3;">
        <tbody width="600" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="border-bottom: 5px solid #445268;"></td>
        </tr>
        </tbody>
    </table>



  </body>
</html>