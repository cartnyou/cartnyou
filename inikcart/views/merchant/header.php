<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<title><?php echo $this->config->item('company_name');?> Seller<?php echo (isset($page_title))?' | '.$page_title:''; ?></title>

<!-- <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css');?>" rel="stylesheet" type="text/css" /> -->
<link type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css');?>" rel="stylesheet" />
<link type="text/css" href="<?php echo base_url('assets/css/redactor.css');?>" rel="stylesheet" />

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url('assets/global/css/components.min.css');?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url('assets/global/css/plugins.min.css');?>" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<link href="<?php echo base_url('assets/global/plugins/bootstrap-toastr/toastr.min.css');?>" rel="stylesheet" type="text/css" />

<?php if(!$this->auth->is_merchant_logged_in(false, false)):?>
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url('assets/pages/css/login-5.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
<?php else:?>
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo base_url('assets/layouts/layout/css/custom.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/layouts/layout/css/layout.min.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/layouts/layout/css/themes/darkblue.min.css');?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo base_url('assets/layouts/layout/css/custom.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
<?php endif;?>

<link rel="shortcut icon" href="favicon.ico" />

<script src="<?php echo base_url('assets/global/plugins/jquery.min.js');?>" type="text/javascript"></script>

<?php if($this->auth->is_merchant_logged_in(false, false)):?>
    <script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js');?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/redactor.min.js');?>"></script>
<?php endif;?>

<!-- <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js');?>"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script> -->

<?php if($this->auth->is_merchant_logged_in(false, false)):?>
    <script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
        
        $('.redactor').redactor({
                minHeight: 200,
                imageUpload: '<?php echo site_url(config_item('merchant_folder').'/wysiwyg/upload_image');?>',
                fileUpload: '<?php echo site_url(config_item('merchant_folder').'/wysiwyg/upload_file');?>',
                imageGetJson: '<?php echo site_url(config_item('merchant_folder').'/wysiwyg/get_images');?>',
                imageUploadErrorCallback: function(json)
                {
                    alert(json.error);
                },
                fileUploadErrorCallback: function(json)
                {
                    alert(json.error);
                }
          });
    });
    </script>
<?php endif;?>
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <?php if($this->auth->is_merchant_logged_in(false, false)):?>
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <?php $merchant_url = site_url($this->config->item('merchant_folder')).'/';?>
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo text-center">
                        <a href="<?php echo $merchant_url;?>">
                            <img src="<?php echo base_url('assets/img/logo-sm.png');?>" alt="logo" class="logo-default" />
                        </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 7 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold">12 pending</span> notifications</h3>
                                        <a href="page_user_profile_1.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                                </a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->

                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="lh-49">
                                <span class="colorWhite"><i class="fa fa-inr colorWhite"></i> <?php echo $remaining_amount;?>  </span>
                                <button class="btn btn-primary recharge"> Recharge Now</button>
                            </li>
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!-- <img alt="" class="img-circle" src="../assets/layouts/layout/img/avatar3_small.jpg" /> -->
                                    <span class="username"><i class="fa fa-user"></i> <?php echo $this->session->userdata('merchant')['firstname'];?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo site_url('merchant/profile');?>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('merchant/profile/change_password');?>">
                                            <i class="icon-lock"></i> Change Password </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url();?>" target="_blank">
                                            <i class="icon-home"></i> Visit Website
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                    <li>
                                        <a href="<?php echo $merchant_url.'login/logout';?>">
                                            <i class="icon-key"></i> <?php echo lang('common_log_out') ?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <div class="clearfix"> </div>
        <?php endif;?>
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php if($this->auth->is_merchant_logged_in(false, false)):?>
                <!-- BEGIN SIDEBAR -->
                    <div class="page-sidebar-wrapper">
                        <!-- BEGIN SIDEBAR -->
                        <div class="page-sidebar navbar-collapse collapse">
                            <!-- BEGIN SIDEBAR MENU -->
                            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                            <ul class="page-sidebar-menu page-sidebar-menu-light page-sidebar-menu-hover-submenu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                                <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                                <li class="sidebar-toggler-wrapper hide">
                                    <div class="sidebar-toggler">
                                        <span></span>
                                    </div>
                                </li>
                                <!-- END SIDEBAR TOGGLER BUTTON -->
                                <li class="nav-item <?php if($active_nav=='dashboard') echo 'active';?>">
                                    <a href="<?php echo $merchant_url;?>dashboard" class="nav-link">
                                        <i class="icon-home"></i>
                                        <span class="title">Dashboard</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo $merchant_url;?>orders" class="nav-link nav-toggle">
                                        <i class="icon-basket"></i>
                                        <span class="title">Orders</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item  ">
                                            <a href="<?php echo $merchant_url;?>orders/pending" class="nav-link ">
                                                <i class="icon-basket"></i>
                                                <span class="title">Pending Orders</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  ">
                                            <a href="<?php echo $merchant_url;?>orders" class="nav-link ">
                                                <i class="icon-basket"></i>
                                                <span class="title">Active Orders</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  ">
                                            <a href="<?php echo $merchant_url;?>orders/cancelled" class="nav-link ">
                                                <i class="icon-tag"></i>
                                                <span class="title">Cancelled Orders</span>
                                            </a>
                                        </li>
                                        <li class="nav-item  ">
                                            <a href="<?php echo $merchant_url;?>orders/returns" class="nav-link ">
                                                <i class="icon-tag"></i>
                                                <span class="title">Returns</span>
                                            </a>
                                        </li>
                                        <!-- <li class="nav-item  ">
                                            <a href="<?php echo $merchant_url;?>reports" class="nav-link ">
                                                <i class="icon-tag"></i>
                                                <span class="title">Reports</span>
                                            </a>
                                        </li> -->
                                    </ul>
                                </li>
                                <li class="nav-item <?php if($active_nav=='products') echo 'active open';?>">
                                    <a href="<?php echo $merchant_url;?>products" class="nav-link nav-toggle">
                                        <i class="icon-grid"></i>
                                        <span class="title">Products Listing</span>
                                        <?php if($active_nav=='products'){?>
                                            <span class="selected"></span>
                                            <span class="arrow open"></span>
                                        <?php } ?>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($active_sub_menu=='products_list') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>products" class="nav-link ">
                                                <i class="fa fa-list"></i>
                                                <span class="title">Products List</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='product_add') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>products/addProduct" class="nav-link ">
                                                <i class="fa fa-cart-plus"></i>
                                                <span class="title">Add New Product</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='product_brand') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>products/addBrand" class="nav-link ">
                                                <i class="fa fa-buysellads"></i>
                                                <span class="title">Add New Brand</span>
                                            </a>
                                        </li>
                                        <!-- <li class="nav-item <?php if($active_sub_menu=='product_category') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>products/addCategory" class="nav-link ">
                                                <i class="fa fa-tasks"></i>
                                                <span class="title">Add New Category</span>
                                            </a>
                                        </li> -->

                                    </ul>
                                </li>
                                <li class="nav-item <?php if($active_nav=='coupons') echo 'active';?>">
                                    <a href="<?php echo $merchant_url;?>coupons" class="nav-link">
                                        <i class="icon-home"></i>
                                        <span class="title">Coupon & Offers</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($active_nav=='shipping') echo 'active open';?>">
                                    <a href="<?php echo $merchant_url;?>shipping" class="nav-link nav-toggle">
                                        <i class="fa fa-ship"></i>
                                        <span class="title">Billing/Payments</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($active_sub_menu=='recharge_logs') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>shipping/recharge_logs" class="nav-link ">
                                                <i class="fa fa-list"></i>
                                                <span class="title">Recharge Logs</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='courier_logs') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>shipping/courier_logs" class="nav-link ">
                                                <i class="fa fa-calculator"></i>
                                                <span class="title">Courier Logs</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='transactions') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>shipping/transactions" class="nav-link ">
                                                <i class="fa fa-calculator"></i>
                                                <span class="title">Billing Details</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='rate_calculator') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>shipping/rate_calculator" class="nav-link ">
                                                <i class="fa fa-calculator"></i>
                                                <span class="title">COD Remittiance</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='rate_calculator') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>shipping/rate_calculator" class="nav-link ">
                                                <i class="fa fa-calculator"></i>
                                                <span class="title">Rate Calculator</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item <?php if($active_nav=='promotions') echo 'active';?>">
                                    <a href="<?php echo $merchant_url;?>promotions" class="nav-link">
                                        <i class="fa fa-plug"></i>
                                        <span class="title">Promotions</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($active_nav=='support') echo 'active open';?>">
                                    <a href="<?php echo $merchant_url;?>support" class="nav-link nav-toggle">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="title">Support</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php if($active_sub_menu=='contact') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>support/contact" class="nav-link ">
                                                <i class="fa fa-envelope"></i>
                                                <span class="title">Contact Gojojo</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if($active_sub_menu=='request_category') echo 'active';?>">
                                            <a href="<?php echo $merchant_url;?>support/request_category" class="nav-link ">
                                                <i class="fa fa-reply"></i>
                                                <span class="title">Request New Catagory</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- END SIDEBAR MENU -->
                            <!-- END SIDEBAR MENU -->
                        </div>
                        <!-- END SIDEBAR -->
                    </div>
                <!-- END SIDEBAR -->
            <?php endif;?>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

                <?php if(!empty($page_title)):?>
                    <h1 class="page-title"><?php echo $page_title; ?></h1>
                <?php endif;?>

                <?php if($this->auth->is_merchant_logged_in(false, false)):?>
                    <?php
                    //lets have the flashdata overright "$message" if it exists
                    if($this->session->flashdata('message'))
                    {
                        $message    = $this->session->flashdata('message');
                    }
                    
                    if($this->session->flashdata('error'))
                    {
                        $error  = $this->session->flashdata('error');
                    }
                    
                    if(function_exists('validation_errors') && validation_errors() != '')
                    {
                        $error  = validation_errors();
                    }
                    ?>
                    
                    <div id="js_error_container" class="alert alert-danger" style="display:none;"> 
                        <p id="js_error"></p>
                    </div>
                    
                    <div id="js_note_container" class="alert alert-info" style="display:none;">
                        
                    </div>
                    
                    <?php if (!empty($message)): ?>
                        <div class="alert alert-success">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php echo $message; ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($error)): ?>
                        <div class="alert alert-danger">
                            <a class="close" data-dismiss="alert">×</a>
                            <?php echo $error; ?>
                        </div>
                    <?php endif; ?>

                <?php endif;?>