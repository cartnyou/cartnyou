<?php

class Products extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->auth->check_access('Admin', true);
		
		$this->load->model(array('Product_model'));
		$this->load->helper('form');
		$this->lang->load('product');
	}

	function index($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=15)
	{
		
		$data['page_title']	= lang('products');
		
		$data['code']		= $code;
		$term				= false;
		$category_id		= false;
		
		//get the category list for the drop menu
		$data['categories']	= $this->Category_model->get_categories_tiered();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		if($post)
		{
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
		}
		elseif ($code)
		{
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search term
		$data['term']		= $term;
		$data['order_by']	= $order_by;
		$data['sort_order']	= $sort_order;
		
		$data['products']	= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page));

		//total number of products
		$data['total']		= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('admin_folder').'/products/index/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul>';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		
		$this->view($this->config->item('admin_folder').'/products', $data);
	}
	
	//basic category search
	function product_autocomplete()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$results	= $this->Product_model->product_autocomplete($name, $limit);
			
			$return		= array();
			
			foreach($results as $r)
			{
				$return[$r->id]	= $r->name;
			}
			echo json_encode($return);
		}
		
	}
	
	function bulk_save()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/products');
		}
				
		foreach($products as $id=>$product)
		{
			$product['id']	= $id;
			$this->Product_model->save($product);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/products');
	}
	
	function form($id = false, $duplicate = false)
	{
		$this->product_id	= $id;
		$this->load->library('form_validation');
		$this->load->model(array('Option_model', 'Category_model', 'Digital_Product_model'));
		$this->lang->load('digital_product');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data['categories']		= $this->Category_model->get_categories_tiered();
		$data['file_list']		= $this->Digital_Product_model->get_list();

		$data['page_title']		= lang('product_form');

		//default values are empty if the product is new
		$data['id']					= '';
		$data['hns']				= '';
		$data['name']				= '';
		$data['description']		= '';
		$data['specification']		= '';
		$data['excerpt']			= '';
		$data['weight']				= '';
		$data['track_stock'] 		= '';
		$data['seo_title']			= '';
		$data['meta']				= '';
		$data['shippable']			= '';
		$data['taxable']			= '';
		$data['tax_rate']			= 0.00;
		$data['fixed_quantity']		= '';
		$data['quantity']			= '';
		$data['enabled']			= '';
		$data['brand']				= '';
		$data['brand_name']			= '';
		$data['related_products']	= array();
		$data['product_categories']	= array();
		$data['images']				= array();
		$data['product_files']		= array();

		//create the photos array for later use
		$data['photos']		= array();

		if ($id)
		{	
			// get the existing file associations and create a format we can read from the form to set the checkboxes
			$pr_files 		= $this->Digital_Product_model->get_associations_by_product($id);
			foreach($pr_files as $f)
			{
				$data['product_files'][]  = $f->file_id;
			}
			
			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($id);
			$product					= $this->Product_model->get_product($id);
			
			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('admin_folder').'/products');
			}
			
			//set values to db values
			$data['id']					= $id;
			$data['hns']				= $product->hns;
			$data['name']				= $product->name;
			$data['seo_title']			= $product->seo_title;
			$data['meta']				= $product->meta;
			$data['description']		= $product->description;
			$data['specification']		= $product->specification;
			$data['excerpt']			= $product->excerpt;
			$data['weight']				= $product->weight;
			$data['shippable']			= $product->shippable;
			$data['taxable']			= $product->taxable;
			$data['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
			$data['fixed_quantity']		= $product->fixed_quantity;
			$data['enabled']			= $product->enabled;
			$data['brand']				= $product->brand;
			$data['brand_name']			= $product->brand_name;
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit'))
			{
				
				$data['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$data['product_categories'][] = $product_category->id;
				}
				
				$data['related_products']	= $product->related_products;
				$data['images']				= (array)json_decode($product->images);
			}
		}
		
		//if $data['related_products'] is not an array, make it one.
		if(!is_array($data['related_products']))
		{
			$data['related_products']	= array();
		}
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= array();
		}

		
		//no error checking on these
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');

		$this->form_validation->set_rules('hns', 'HNS', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta_data', 'trim');
		$this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('description', 'lang:description', 'trim|required');
		$this->form_validation->set_rules('specification', 'Specification', 'trim|required');
		$this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
		$this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric|floatval');
		$this->form_validation->set_rules('track_stock', 'lang:track_stock', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'lang:shippable', 'trim|numeric');
		$this->form_validation->set_rules('taxable', 'lang:taxable', 'trim|numeric');
		//$this->form_validation->set_rules('tax_rate', 'Tax Rate', 'trim|numeric|floatval|required');
		$this->form_validation->set_rules('fixed_quantity', 'lang:fixed_quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|numeric|required');
		$this->form_validation->set_rules('categories[]','Category', 'required');
		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		if($duplicate)
		{
			$data['id']	= false;
		}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$data['product_options']	= $this->input->post('option');
			$data['related_products']	= $this->input->post('related_products');
			$data['product_categories']	= $this->input->post('categories');
			$data['images']				= $this->input->post('images');
			$data['product_files']		= $this->input->post('downloads');
			
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->view($this->config->item('admin_folder').'/product_form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}

			$first_tax = 0;
			$first_commission = 0;
			if(!empty($categories)){
				foreach($categories as $c){
					$first_category_id 	= $c;
					$categoryInfo 		= $this->Category_model->get_category($c);
					$first_tax 			= $categoryInfo->tax_rate;
					$first_commission 	= $categoryInfo->commission;
					break;
				}
			}

			$save['id']					= $id;
			//$savePM['product_id']		= $id;
			$save['hns']				= $this->input->post('hns');
			$save['name']				= $this->input->post('name');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');
			$save['description']		= $this->input->post('description');
			$save['specification']		= $this->input->post('specification');
			$save['excerpt']			= $this->input->post('excerpt');
			$save['weight']				= $this->input->post('weight');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$save['enabled']			= $this->input->post('enabled');
			$save['shippable']			= $this->input->post('shippable');
			$save['taxable']			= $this->input->post('taxable');
			$save['tax_rate']			= $first_tax;
			$save['commission']			= $first_commission;
			$save['brand']				= $this->input->post('brand');
			$post_images				= $this->input->post('images');
			
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					foreach($post_images as $key => &$pi)
					{
						if($primary == $key)
						{
							$pi['primary']	= true;
							continue;
						}
					}	
				}
				
			}
			
			$save['images']				= json_encode($post_images);
			
			
			if($this->input->post('related_products'))
			{
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}
			
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	
			
			// save product 
			$product_id	= $this->Product_model->save($save, $options, $categories, array(), false);
			
			// add file associations
			// clear existsing
			$this->Digital_Product_model->disassociate(false, $product_id);
			// save new
			$downloads = $this->input->post('downloads');
			if(is_array($downloads))
			{
				foreach($downloads as $d)
				{
					$this->Digital_Product_model->associate($d, $product_id);
				}
			}			

			$this->session->set_flashdata('message', lang('message_saved_product'));

			//go back to the product list
			redirect($this->config->item('admin_folder').'/products');
		}
	}
	
	function product_image_form()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_form_multiple()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader_multiple', $data);
	}
	
	function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 600;
			$config['height'] = 500;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['file_name']	= $upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_upload_multiple()
	{
		$data['file_name'] 	= array();
		$data['error']		= array();	

		if(isset($_FILES['upl_files'])){
			$this->load->library('upload');

			$number_of_files_uploaded = count($_FILES['upl_files']['name']);

		    for ($i = 0; $i < $number_of_files_uploaded; $i++) :
		    	$_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
		    	$_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
		    	$_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
		      	$_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
		      	$_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];

		      	$config = array(
			        'allowed_types' => 'jpg|jpeg|png|gif|JPG|JPEG',
			        'max_size'      => $this->config->item('size_limit'),
			        'upload_path'	=> 'uploads/images/full',
			        'encrypt_name'	=> true,
			        'remove_spaces'	=> true
		      	);
		      	
		      	$this->upload->initialize($config);

		      	if ( ! $this->upload->do_upload()) :
		        	$data['error'][$i]	= $this->upload->display_errors();

		      	else :
		        	$upload_data = $this->upload->data();

		        	$this->load->library('image_lib');

					//this is the larger image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 600;
					$config['height'] = 500;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();

					//small image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 235;
					$config['height'] = 235;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$this->image_lib->clear();

					//cropped thumbnail
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 150;
					$config['height'] = 150;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					//card
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/card/'.$upload_data['file_name'];
					$config['maintain_ratio'] = FALSE;
					$config['width'] = 268;
					$config['height'] = 327;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					$data['file_name'][$i]	= $upload_data['file_name'];

		      	endif;
		    endfor;
		}

	    $this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader_multiple', $data);		
	}
	
	function delete($id = false)
	{
		if ($id)
		{	
			$product	= $this->Product_model->get_product($id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('admin_folder').'/products');
			}
			else
			{

				// remove the slug
				$this->load->model('Routes_model');
				$this->Routes_model->delete($product->route_id);

				//if the product is legit, delete them
				$this->Product_model->delete_product($id);

				$this->session->set_flashdata('message', lang('message_deleted_product'));
				redirect($this->config->item('admin_folder').'/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('admin_folder').'/products');
		}
	}

	function ratings($page=0){
        $data['page_title'] = 'Product Ratings';
        $rows = 10;
        $data['data'] = $this->Product_model->get_products_ratings(false, $rows, $page);
        $data['total'] = $this->Product_model->get_products_ratings(true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/products/ratings/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        $this->view($this->config->item('admin_folder').'/product_ratings', $data);
    }

    function rating_change($pr_id){
        $affected_rows   = $this->Product_model->rating_change($pr_id);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/products/ratings');
    }

}
