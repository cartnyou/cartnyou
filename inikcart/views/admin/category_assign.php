<div class="row">
	<div class="span12" style="border-bottom:1px solid #f5f5f5;">
		<?php echo $this->pagination->create_links();?>	&nbsp;
	</div>
</div>

<table class="table table-striped">
    <thead>
		<tr>
			<th>Store</th>
			<th>Category</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		
		if(!empty($data)) {
			foreach ($data as $row) { ?>
				<tr>
					<td><?php echo  $row->store_name; ?></td>
					<td><?php echo  $row->name; ?></td>
					<td><?php echo ($row->enabled == '1') ? lang('enabled') : lang('disabled'); ?></td>
					<td>
						<div class="btn-group" style="float:right">
							<?php if($row->enabled == '1'){ ?>
								<a class="btn" href="<?php echo  site_url(ADMIN_FOLDER.'/categories/assign_change/'.$row->mc_id);?>"><i class="icon-remove"></i> Disable</a>
							<?php } else { ?>
								<a class="btn btn-danger" href="<?php echo site_url(ADMIN_FOLDER.'/categories/assign_change/'.$row->mc_id);?>"><i class="icon-ok"></i> Enable</a>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php
			}
		} else {
			echo '<tr><td style="text-align:center;" colspan="4">No assignment found.</td></tr>';
		}
		?>
	</tbody>
</table>