<?php
Class Search_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/********************************************************************

	********************************************************************/
	
	function record_term($term)
	{
		$code	= md5($term);
		$this->db->where('code', $code);
		$exists	= $this->db->count_all_results('search');
		if ($exists < 1){
			$this->db->insert('search', array('code'=>$code, 'term'=>$term, 'count'=>1));
		} else {
			$this->db->where('code', $code);
			$this->db->set('count', 'count+1', FALSE);
			$this->db->update('search');
		}
		return $code;
	}
	
	function get_term($code)
	{
		$this->db->select('term');
		$result	= $this->db->get_where('search', array('code'=>$code));
		$result	= $result->row();
		return $result->term;
	}

	function get_hot_search_keywords($limit=10){
		$this->db->not_like('term','{','after');
		$this->db->order_by('count','DESC');
		$this->db->limit($limit);
		$result	= $this->db->get('search');
		return $result->result();
	}
}