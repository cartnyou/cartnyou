<?php

class Cart extends Front_Controller {

	function index()
	{
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['homepage']			= true;
		//$data['popular_products'] = $this->Product_model->popular_products(10);

		//$data['featured_categories'] = $this->Category_model->get_featured_categories(0,10);

		/*$this->load->model('Brand_model');
		$brands = $this->Brand_model->get_brand_showcase(10);
		if(!empty($brands)){
			foreach ($brands as &$brand) {
				$brand->products = $this->Product_model->search_products('', 4, false, false, false,$filters=array('category'=>0,'brand_checked'=>array($brand->id)),false);
			}
		}
		$data['brands_showcase'] = $brands;*/
		
		$data['body_class'] = 'home';
		$this->view('homepage', $data);
	}
    function refund()
    {

        $this->view('refund');
    }
    function dummy()
    {

        $this->view('dummy');
    }
    function shippingpolicy()
    {

        $this->view('shipping');
    }

	function page($id = false)
	{
		//if there is no page id provided redirect to the homepage.
		$data['page']	= $this->Page_model->get_page($id);
		if(!$data['page'])
		{
			show_404();
		}
		$this->load->model('Page_model');
		$data['base_url']			= $this->uri->segment_array();
		
		$data['fb_like']			= true;

		$data['page_title']			= $data['page']->title;
		
		$data['meta']				= $data['page']->meta;
		$data['seo_title']			= (!empty($data['page']->seo_title))?$data['page']->seo_title:$data['page']->title;
		
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('page', $data);
	}
	
	
	function search($code=false, $page = 0)
	{
		$this->load->model('Search_model');
		
		//check to see if we have a search term
		if(!$code)
		{
			//if the term is in post, save it to the db and give me a reference
			$term		= $this->input->post('term', true);
			$code		= $this->Search_model->record_term($term);
			
			// no code? redirect so we can have the code in place for the sorting.
			// I know this isn't the best way...
			redirect('cart/search/'.$code.'/'.$page);
		}
		else
		{
			//if we have the md5 string, get the term
			$term	= $this->Search_model->get_term($code);
		}
		
		if(empty($term))
		{
			//if there is still no search term throw an error
			$this->session->set_flashdata('error', lang('search_error'));
			redirect('cart');
		}

		$data['page_title']			= lang('search');
		$data['gift_cards_enabled']	= $this->gift_cards_enabled;
		
		//fix for the category view page.
		$data['base_url']			= array();
		
		$sort_array = array(
							'name/asc' => array('by' => 'name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>false, 'sort'=>false);
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$data['page_title']	= lang('search');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
	
		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= base_url().'cart/search/'.$code.'/';
		$config['uri_segment']	= 4;
		$config['per_page']		= 24;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';


		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$brands = array();
		$filters['brand_checked'] = array();
		$filters['category'] = 0;
		$filters['facets'] = array();

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['brand'])){
			$filters['brand_checked'] = explode(',',urldecode($_GET['brand']));
		}

		if(isset($_GET['facets'])){
			//print_r($_GET['facets']);
			if(!empty($_GET['facets'])){
				foreach ($_GET['facets'] as $facet_key => $facet_value) {
					$filters['facets'][$facet_key] = explode(',',urldecode($_GET['facets'][$facet_key]));
				}
			}
			//$filters['facets'] = explode(',',urldecode($_GET['facets']));
		}
		
		//$brands = $this->Product_model->get_products_brands($data['category']->id,true);

		$data['products'] 		= $this->Product_model->search_products($term, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		$filter_options_return	= $this->Product_model->search_products_filter_options($term, array('category'=>0));

		$filter_options = array();
		if(!empty($filter_options_return)){
			foreach ($filter_options_return as $filter_option) {
				$filter_option_name = strtolower($filter_option->name);

				$exploded_this_facets_values = array();
				if(isset($_GET['facets'][$filter_option_name])){
					$exploded_this_facets_values = explode(',', $_GET['facets'][$filter_option_name]);
				}

				if(isset($filter_options[$filter_option_name])){
					if(!in_array($filter_option->ov_name, $filter_options[$filter_option_name])){
						$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
						$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

						if(isset($_GET['facets'][$filter_option_name])){
							if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
								$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
							}
						}
					}
				} else {
					$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
					$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

					if(isset($_GET['facets'][$filter_option_name])){
						if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
							$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
						}
					}
				}
			}
		}
		$data['filter_options'] = $filter_options;

		$config['total_rows']	= $this->Product_model->search_products($term, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);
		
		unset($_GET['filters_changed']);

		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);

		$this->pagination->initialize($config);

		//$brands_used = array();
		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}

			/*if(in_array($p->brand_id, $brands_used)){
				$brands[$p->brand_id]['count'] = $brands[$p->brand_id]['count'] + 1;
			} else {
				$brands[$p->brand_id] = array('name'=>$p->brand_name, 'count'=>1);
			}
			$brands_used[] = $p->brand_id;*/
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$filters['brands'] = $brands;

		$data['filters'] = $filters;

		$data['term'] = $term;
		$this->view('search', $data);
	}

	function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	function get_hot_search_keywords(){
		$this->load->model('Search_model');
		$result	= $this->Search_model->get_hot_search_keywords(20);

		$return_terms = array();
		if(!empty($result)){
			foreach ($result as &$row) {
				//if($this->isJson($row->term)){
				//	$row->term = json_decode($row->term)->term;
				//}
				//if(!$this->isJson($row->term)){
					if($row->term != ''){
						$return_terms[] = $row;
					}
				//}
			}
			$return['data'] = $return_terms;
			$return['success'] = TRUE;
		} else {
			$return['success'] = FALSE;
		}
		die(json_encode($return));
	}
	

	
	function category($id)
	{
		
		//get the category
		$data['category'] = $this->Category_model->get_category($id, true);
				
		if (!$data['category'] || $data['category']->enabled==0)
		{
			show_404();
		}
				
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['category']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}

		if(isset($_GET['filters_changed'])){
			if(trim($_GET['filters_changed']) == true){
				$page = 0;
			}
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$data['meta_description']		= $data['category']->meta;
		$data['seo_title']	= (!empty($data['category']->seo_title))?$data['category']->seo_title:$data['category']->name;
		$data['page_title']	= $data['category']->name;
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'sequence', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$brands = array();
		$filters['brand_checked'] = array();
		$filters['facets'] = array();

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['brand'])){
			$filters['brand_checked'] = explode(',',urldecode($_GET['brand']));
		}
		if(isset($_GET['facets'])){
			if(!empty($_GET['facets'])){
				foreach ($_GET['facets'] as $facet_key => $facet_value) {
					$filters['facets'][$facet_key] = explode(',',urldecode($_GET['facets'][$facet_key]));
				}
			}
		}
		
		$brands = $this->Product_model->get_products_brands($data['category']->id,true);

		$config['per_page']		= 24;
		
		$data['products']		= $this->Product_model->get_products($data['category']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		$filter_options_return	= $this->Product_model->get_products_filter_options($data['category']->id);

		$filter_options = array();
		if(!empty($filter_options_return)){
			foreach ($filter_options_return as $filter_option) {
				$filter_option_name = strtolower($filter_option->name);

				$exploded_this_facets_values = array();
				if(isset($_GET['facets'][$filter_option_name])){
					$exploded_this_facets_values = explode(',', $_GET['facets'][$filter_option_name]);
				}

				if(isset($filter_options[$filter_option_name])){
					if(!in_array($filter_option->ov_name, $filter_options[$filter_option_name])){
						$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
						$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

						if(isset($_GET['facets'][$filter_option_name])){
							if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
								$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
							}
						}
					}
				} else {
					$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
					$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

					if(isset($_GET['facets'][$filter_option_name])){
						if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
							$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
						}
					}
				}
			}
		}
		$data['filter_options'] = $filter_options;

		$product_count = $this->Product_model->get_products($data['category']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);

		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		unset($_GET['filters_changed']);

		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				
		$this->pagination->initialize($config);

		//$brands_used = array();
		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}

			/*if(in_array($p->brand_id, $brands_used)){
				$brands[$p->brand_id]['count'] = $brands[$p->brand_id]['count'] + 1;
			} else {
				$brands[$p->brand_id] = array('name'=>$p->brand_name, 'count'=>1);
			}
			$brands_used[] = $p->brand_id;*/
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$filters['brands'] = $brands;

		$data['filters'] = $filters;

		$this->view('category', $data);
	}


	
	function product($id,$merchant_id='')
	{
		//get the product

		$data['product']		= $this->Product_model->get_product($id,true,$merchant_id,true,'',false,true);//print_r($data['product'][0]->offers);exit();
		//$data['other_sellers']	= $this->Product_model->get_product($id,true,'',true,$merchant_id);
		$data['other_sellers']	= $this->Product_model->get_product($id,false,'',true);
		
		$any_enabled = false;
		if($data['product']){
			foreach ($data['product'] as $pm) {
				if($pm->enabled==1){
					$any_enabled = true;
				}
			}
		} else {
			show_404();
		}
		if(!$any_enabled){
			show_404();
		}
		
		$data['base_url']			= $this->uri->segment_array();
		
		// load the digital language stuff
		$this->lang->load('digital_product');
		
		$data['options']	= $this->Option_model->get_product_options($data['product'][0]->id);
		
		$related			= $data['product'][0]->related_products;
		$data['related']	= array();
		

				
		$data['posted_options']	= $this->session->flashdata('option_values');

		$data['page_title']			= ucwords($data['product'][0]->name);
		$data['meta']				= $data['product'][0]->meta;
		$data['meta_description']	= $data['product'][0]->meta_description;
		$data['seo_title']			= ucwords((!empty($data['product'][0]->seo_title))?$data['product'][0]->seo_title:$data['product'][0]->name);
			
		if($data['product'][0]->images == 'false')
		{
			$data['product'][0]->images = array();
		}
		else
		{
			$data['product'][0]->images	= array_values((array)json_decode($data['product'][0]->images));
		}

		$data['gift_cards_enabled'] = $this->gift_cards_enabled;

		$data['reviews'] = $this->Product_model->get_product_reviews($id);

		$data['avg_rating'] = $this->Product_model->get_avg_rating($id);

		$this->load->model('Seller_model');
		$data['seller_avg_rating'] = $this->Seller_model->get_seller_avg_rating($data['product'][0]->merchant_id);

		if(!empty($data['other_sellers'])){
			foreach ($data['other_sellers'] as &$other_seller) {
				$other_seller->seller_avg_rating = $this->Seller_model->get_seller_avg_rating($other_seller->merchant_id);
			}
		}
		
		$data['sold_times'] = $this->Product_model->get_product_sold_times($id);

		$data['category'] = array();
		if(isset($data['product'][0]->categories[0])){
			$data['category'] = $this->Category_model->get_category($data['product'][0]->categories[0]->category_id);
		}

		// increase product view count
		$this->Product_model->increase_view_count($id);
		//print_r($data['product'][0]->payment_mode);exit();
		$this->view('product', $data);
	}
	
	
	function add_to_cart($ajax=false)
	{
		// Get our inputs
		$product_id		= $this->input->post('id');
		$quantity 		= $this->input->post('quantity');
		$post_options 	= $this->input->post('option');
		$pm_id			= $this->input->post('pm_id');
		
		// Get a cart-ready product array
		$product = $this->Product_model->get_cart_ready_product($product_id, $quantity,$pm_id);
		
		// coming soon product check
		if(strtotime($product['live_on']) > time()){
			if($ajax){
				die(json_encode(array('success'=>false,'data'=>$this->Product_model->get_slug($product_id,$pm_id))));
			} else {
				redirect($this->Product_model->get_slug($product_id,$pm_id));
			}
		}
		
		//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
		if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
		{
			$stock	= $this->Product_model->get_product($product_id,false,'',false,'',$pm_id);
			
			//loop through the products in the cart and make sure we don't have this in there already. If we do get those quantities as well
			$items		= $this->go_cart->contents();
			$qty_count	= $quantity;
			foreach($items as $item)
			{
				if(intval($item['id']) == intval($product_id))
				{
					$qty_count = $qty_count + $item['quantity'];
				}
			}
			
			if($stock->quantity < $qty_count)
			{
				//we don't have this much in stock
				$this->session->set_flashdata('error', sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity));
				$this->session->set_flashdata('quantity', $quantity);
				$this->session->set_flashdata('option_values', $post_options);

				if($ajax){
					die(json_encode(array('success'=>false,'data'=>$this->Product_model->get_slug($product_id,$pm_id))));
				} else {
					redirect($this->Product_model->get_slug($product_id,$pm_id));
				}
			}
		}

		// Validate Options 
		// this returns a status array, with product item array automatically modified and options added
		//  Warning: this method receives the product by reference
		$status = $this->Option_model->validate_product_options($product, $post_options);
		
		// don't add the product if we are missing required option values
		if( ! $status['validated'])
		{
			$this->session->set_flashdata('quantity', $quantity);
			//$this->session->set_flashdata('error', $status['message']);
			$this->session->set_flashdata('option_values', $post_options);
			$this->session->set_flashdata('optionsRequired', $status['optionsRequired']);
		
			if($ajax){
				die(json_encode(array('success'=>false,'data'=>$this->Product_model->get_slug($product_id,$pm_id))));
			} else {
				redirect($this->Product_model->get_slug($product_id,$pm_id));
			}
		
		} else {
		
			//iterate and make post_options false, if all empty, to remove duplicay from add to cart
			$post_option_check = false;
			if(!empty($post_options)){
				foreach ($post_options as $key=>$post_option) {
					if(!empty($post_option)){
						$post_option_check = true;
					}
				}
			}

			//Add the original option vars to the array so we can edit it later
			$product['post_options']	= $post_options;
			
			if(!$post_option_check){
				$product['post_options']	= false;
			}

			// apply any special offer if applicable
			$offers = $this->Product_model->getProductOffers($product_id);
			if(!empty($offers)){
				foreach ($offers as $offer) {
					if($offer->offer_type == 2){
						$specialOfferAvailable = true;
	                    $product['price'] = $offer->offer_price;
	                    $product['specialOfferApplied'] = true;
						break;
					}
				}
			}
			
			
			//is giftcard
			$product['is_gc']			= false;

			// add totals info
			if(!isset($product['coupon_discount'])){
				$product['coupon_discount'] = 0;
			}
			$product['tax']				= 0;	
			$product['subtotal']		= $product['price']*$product['quantity'];
			$product['coupon_discount']	= $product['coupon_discount']*$product['quantity'];
			$product['shipping']		= $product['shipping_charges']*$product['quantity'];
			$product['total']			= ($product['subtotal'] + $product['shipping']) - $product['coupon_discount'];
			//if(!isset($product['total_coupon_discount'])){
			//	$product['total_coupon_discount'] = 0;
			//}
			//$product['total']			= ($product['subtotal'] + $product['shipping']) - $product['total_coupon_discount'];

			// Add the product item to the cart, also updates coupon discounts automatically
			$this->go_cart->insert($product);
		
			if($ajax){
				die(json_encode(array('success'=>true, 'data'=>'<div class="row"><div class="col-md-12"><a class="hide-alertify pull-right">x</a></div></div><div class="text-center"><a class="red" href="'.$this->Product_model->get_slug($product_id,$pm_id).'">'.ucwords($product['name']).'</a> has been added to your cart.</div><div class="row"><div class="mt-10 col-xs-6"><a href="'.site_url('cart/view-cart').'" class="btn-red ptb7">View Cart</a></div><div class="mt-10 col-xs-6"><a href="'.site_url('checkout').'" class="btn-red ptb7">Checkout</a></div></div>')));
			} else {
				// go go gadget cart!
				if($this->input->post('buy_now_redirect')){
					redirect('checkout');
				}
				redirect('cart/view-cart');
			}
		}
	}

	function add_to_wishlist()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Product', 'required');
		$this->form_validation->set_rules('pm_id', 'PM ID', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('success'=>false,'data'=>'Fields Required')));
		} else {
			$customer	= $this->go_cart->customer();
			$product_id		= $this->input->post('id');
			$pm_id			= $this->input->post('pm_id');

			if(isset($customer['id'])){

				$save['customer_id'] = $customer['id'];
				$save['product_id'] = $product_id;
				$save['pm_id'] = $pm_id;

				if($this->Product_model->add_to_wishlist($save)){
					die(json_encode(array('success'=>true,'data'=>'Added to your wishlist.')));
				} else {
					die(json_encode(array('success'=>false,'data'=>'This product is already in your wishlist.')));
				}
			} else {
				die(json_encode(array('success'=>false,'data'=>'You need to login to add this product to wishlist.')));
			}	
		}
	}

	function remove_from_wishlist()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'Product', 'required');
		$this->form_validation->set_rules('pm_id', 'PM ID', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			die(json_encode(array('success'=>false,'data'=>'Fields Required')));
		} else {
			$customer	= $this->go_cart->customer();
			$product_id		= $this->input->post('id');
			$pm_id			= $this->input->post('pm_id');

			if(isset($customer['id'])){

				$data['customer_id'] = $customer['id'];
				$data['product_id'] = $product_id;
				$data['pm_id'] = $pm_id;

				if($this->Product_model->remove_from_wishlist($data)){
					die(json_encode(array('success'=>true,'data'=>'Removed from your wishlist.')));
				} else {
					die(json_encode(array('success'=>false,'data'=>'Unable to remove from wishlist.')));
				}
			} else {
				die(json_encode(array('success'=>false,'data'=>'You need to login to remove this product to wishlist.')));
			}	
		}
	}
	
	function wishlist()
	{
		$this->Customer_model->is_logged_in('secure/wishlist/');
		$data['page_title']	= 'Wishlist';

		$customer	= $this->go_cart->customer();
		$data['products'] = $this->Product_model->get_customer_wishlist($customer['id']);
		
		$this->view('wishlist', $data);
	}

	function view_cart()
	{
		
		$data['page_title']	= 'View Cart';
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$this->view('view_cart', $data);
	}
	
	function remove_item($key)
	{
		//drop quantity to 0
		$this->go_cart->update_cart(array($key=>0));
		
		redirect('cart/view-cart');
	}
	
	function update_cart($redirect = false)
	{
		//if redirect isn't provided in the URL check for it in a form field
		if(!$redirect)
		{
			$redirect = $this->input->post('redirect');
		}
		
		// see if we have an update for the cart
		$item_keys		= $this->input->post('cartkey');
		$coupon_code	= $this->input->post('coupon_code');
		$gc_code		= $this->input->post('gc_code');
		
		if($coupon_code)
		{
			$coupon_code = strtolower($coupon_code);
		}
			
		//get the items in the cart and test their quantities
		$items			= $this->go_cart->contents();
		$new_key_list	= array();
		//first find out if we're deleting any products
		foreach($item_keys as $key=>$quantity)
		{
			if(intval($quantity) === 0)
			{
				//this item is being removed we can remove it before processing quantities.
				//this will ensure that any items out of order will not throw errors based on the incorrect values of another item in the cart
				$this->go_cart->update_cart(array($key=>$quantity));
			}
			else
			{
				//create a new list of relevant items
				$new_key_list[$key]	= $quantity;
			}
		}

		$response	= array();
		foreach($new_key_list as $key=>$quantity)
		{
			$product	= $this->go_cart->item($key);
			//if out of stock purchase is disabled, check to make sure there is inventory to support the cart.
			if(!$this->config->item('allow_os_purchase') && (bool)$product['track_stock'])
			{
				$stock	= $this->Product_model->get_product($product['id'],false,'',false,'',$product['pm_id']);
			
				//loop through the new quantities and tabluate any products with the same product id
				$qty_count	= $quantity;
				foreach($new_key_list as $item_key=>$item_quantity)
				{
					if($key != $item_key)
					{
						$item	= $this->go_cart->item($item_key);
						//look for other instances of the same product (this can occur if they have different options) and tabulate the total quantity
						if($item['id'] == $stock->id)
						{
							$qty_count = $qty_count + $item_quantity;
						}
					}
				}

				if($stock->quantity < $qty_count)
				{
					if(isset($response['error']))
					{
						$response['error'] .= '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
					}
					else
					{
						$response['error'] = '<p>'.sprintf(lang('not_enough_stock'), $stock->name, $stock->quantity).'</p>';
					}
				}
				else
				{
					//this one works, we can update it!
					//don't update the coupons yet
					$this->go_cart->update_cart(array($key=>$quantity));
				}
			}
			else
			{
				$this->go_cart->update_cart(array($key=>$quantity));
			}
		}
		
		//if we don't have a quantity error, run the update
		if(!isset($response['error']))
		{
			//update the coupons and gift card code
			$response = $this->go_cart->update_cart(false, $coupon_code, $gc_code);
			// set any messages that need to be displayed
		}
		else
		{
			$response['error'] = '<p>'.lang('error_updating_cart').'</p>'.$response['error'];
		}
		
		
		//check for errors again, there could have been a new error from the update cart function
		if(isset($response['error']))
		{
			$this->session->set_flashdata('error', $response['error']);
		}
		if(isset($response['message']))
		{
			$this->session->set_flashdata('message', $response['message']);
		}
		
		if($redirect == 'ajax'){
			die(json_encode(array('success'=>true,'data'=>$response)));
		} else if($redirect){
			redirect($redirect);
		} else {
			redirect('cart/view-cart');
		}
	}

	function apply_coupon($redirect = false)
	{
		//if redirect isn't provided in the URL check for it in a form field
		if(!$redirect)
		{
			$redirect = $this->input->post('redirect');
		}
		
		$coupon_code	= $this->input->post('coupon_code');
		$gc_code		= $this->input->post('gc_code');
		
		if($coupon_code)
		{
			$coupon_code = strtolower($coupon_code);
		}
			
		$response	= array();
		
		$response = $this->go_cart->update_cart(false, $coupon_code, $gc_code);
		
		//check for errors again, there could have been a new error from the update cart function
		if(isset($response['error']))
		{
			$this->session->set_flashdata('error', $response['error']);
		}
		if(isset($response['message']))
		{
			$this->session->set_flashdata('message', $response['message']);
		}
		
		if($redirect == 'ajax'){
			die(json_encode(array('success'=>true,'data'=>$response)));
		} else if($redirect){
			redirect($redirect);
		} else {
			redirect('cart/view-cart');
		}
	}

	function get_cart_summary(){
		$this->partial('checkout/summary');
	}

	function get_cart_summary_total(){
		$this->partial('checkout/summary_total');
	}

	
	/***********************************************************
			Gift Cards
			 - this function handles adding gift cards to the cart
	***********************************************************/
	
	function giftcard()
	{
		if(!$this->gift_cards_enabled) redirect('/');
		
		// Load giftcard settings
		$gc_settings = $this->Settings_model->get_settings("gift_cards");
				
		$this->load->library('form_validation');
		
		$data['allow_custom_amount']	= (bool) $gc_settings['allow_custom_amount'];
		$data['preset_values']			= explode(",",$gc_settings['predefined_card_amounts']);
		
		if($data['allow_custom_amount'])
		{
			$this->form_validation->set_rules('custom_amount', 'lang:custom_amount', 'numeric');
		}
		
		$this->form_validation->set_rules('amount', 'lang:amount', 'required');
		$this->form_validation->set_rules('preset_amount', 'lang:preset_amount', 'numeric');
		$this->form_validation->set_rules('gc_to_name', 'lang:recipient_name', 'trim|required');
		$this->form_validation->set_rules('gc_to_email', 'lang:recipient_email', 'trim|required|valid_email');
		$this->form_validation->set_rules('gc_from', 'lang:sender_email', 'trim|required');
		$this->form_validation->set_rules('message', 'lang:custom_greeting', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['error']				= validation_errors();
			$data['page_title']			= lang('giftcard');
			$data['gift_cards_enabled']	= $this->gift_cards_enabled;
			$this->view('giftcards', $data);
		}
		else
		{
			
			// add to cart
			
			$card['price'] = set_value(set_value('amount'));
			
			$card['id']				= -1; // just a placeholder
			$card['sku']			= lang('giftcard');
			$card['base_price']		= $card['price']; // price gets modified by options, show the baseline still...
			$card['name']			= lang('giftcard');
			$card['code']			= generate_code(); // from the string helper
			$card['excerpt']		= sprintf(lang('giftcard_excerpt'), set_value('gc_to_name'));
			$card['weight']			= 0;
			$card['quantity']		= 1;
			$card['shippable']		= false;
			$card['taxable']		= 0;
			$card['fixed_quantity'] = true;
			$card['is_gc']			= true; // !Important
			$card['track_stock']	= false; // !Imporortant
			
			$card['gc_info'] = array("to_name"	=> set_value('gc_to_name'),
									 "to_email"	=> set_value('gc_to_email'),
									 "from"		=> set_value('gc_from'),
									 "personal_message"	=> set_value('message')
									 );
			
			// add the card data like a product
			$this->go_cart->insert($card);
			
			redirect('cart/view-cart');
		}
	}

	function add_review($guest=false){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('product_id', 'Product', 'required');
		$this->form_validation->set_rules('review', 'Review', 'trim|required');
		$this->form_validation->set_rules('rating_price', 'Price Rating', 'numeric|required');
		$this->form_validation->set_rules('rating_value', 'Value Rating', 'numeric|required');
		$this->form_validation->set_rules('rating_quality', 'Quality Rating', 'numeric|required');

		if($guest){
			$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[32]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required|regex_match[/^[0-9]{10}$/]');
		}
		
		$product_id		= $this->input->post('product_id');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {
			$customer	= $this->go_cart->customer();

			if(isset($customer['id'])){
				$save['customer_id'] 	= $customer['id'];
			} else {
				$save['customer_id']= 0;
				$save['pr_name'] 	= $this->input->post('name');
				$save['pr_email'] 	= $this->input->post('email');
				$save['pr_phone'] 	= $this->input->post('phone');
			}
			$time = date('Y-m-d H:i:s',time());
			$save['review'] 		= $this->input->post('review');
			$save['rating_price'] 	= $this->input->post('rating_price');
			$save['rating_value'] 	= $this->input->post('rating_value');
			$save['rating_quality'] = $this->input->post('rating_quality');
			$save['product_id'] 	= $product_id;
			$save['created_at'] 	= $time;
			$save['updated_at'] 	= $time;
			$this->Product_model->add_review($save);
			$this->session->set_flashdata('message', 'Your review has been submitted.');
		}
		
		$pm_id		= $this->input->post('pm_id');
		redirect($this->Product_model->get_slug($product_id,$pm_id));
	}

	function contact_us_post($merchant=false){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('query', 'Query', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {

			$time = date('Y-m-d H:i:s',time());
			$save['ticket_number'] = 0;
			if($merchant){
				$save['user_type'] = 'merchant';
				$save['user_id'] = $this->session->userdata('merchant')['id'];
			} else {
				$save['user_type'] = 'user';
				$customer	= $this->go_cart->customer();
				if(isset($customer['id'])){
					$save['user_id'] = $customer['id'];
				} else {
					$save['user_id'] = 0;
				}
			}
			$save['name']  = $this->input->post('name');
			$save['phone'] = $this->input->post('phone');
			$save['email'] = $this->input->post('email');
			$save['query'] = $this->input->post('query');
			$save['created_at'] = $time;
			$save['updated_at'] = $time;

			$this->load->model('Support_model');
			$ticket_number = $this->Support_model->add_query($save);

			$this->load->library('email');
			$config_mail = Array(
	        	'protocol'  => 'smtp',
	        	'smtp_host' => 'ssl://smtp.zoho.com',
	        	'smtp_port' => '465',
	        	'smtp_user' => 'reachus@gojojo.in',
	        	'smtp_pass' => 'qwerty1!',
	        	'mailtype'  => 'html',
		    	'newline'   => "\r\n"
	        );
			$this->email->initialize($config_mail);

			$content  = '<b>Name:</b> '.$save['name'];
			$content .= '<br><b>Phone:</b> '.$save['phone'];
			$content .= '<br><b>Email:</b> '.$save['email'];
			$content .= '<br><b>Query:</b> '.$save['query'];

			$this->email->from($email, $save['name']);
			$this->email->to($this->config->item('email'));
			$this->email->subject('Gojojo - Contact Us');
			$this->email->message(html_entity_decode($content));
			$this->email->send();

			$this->session->set_flashdata('message', 'Your query has been submitted with Ticket Number '.$ticket_number.'. We will revert you soon.');
		}
		if($merchant){
			redirect(site_url('merchant/support/contact'));
		} else {
			redirect(site_url('contact-us'));
		}
	}

	function careers_post(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('apply_for', 'Apply for', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('youtube_profile', 'Youtube Profile', 'trim');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {

			$config['upload_path']          = './uploads/resume/';
            $config['allowed_types']        = 'doc|docx|pdf';
	        $config['encrypt_name'] = TRUE;
	        
	        $this->load->library('upload', $config);

	        if ( ! $this->upload->do_upload('resume')){
	            $this->session->set_flashdata('error', $this->upload->display_errors());
	        } else {
	            $upload_data = $this->upload->data();
	            $file_name = $upload_data['file_name'];

	            $this->load->library('email');
				$config_mail = Array(
		        	'protocol'  => 'smtp',
		        	'smtp_host' => 'ssl://smtp.zoho.com',
		        	'smtp_port' => '465',
		        	'smtp_user' => 'reachus@gojojo.in',
		        	'smtp_pass' => 'qwerty1!',
		        	'mailtype'  => 'html',
		    		'newline'   => "\r\n"
		        );
				$this->email->initialize($config_mail);

				$apply_for = $this->input->post('apply_for');
				$name = $this->input->post('name');
				$phone = $this->input->post('phone');
				$email = $this->input->post('email');
				$youtube_profile = $this->input->post('youtube_profile');
				$resume = site_url('uploads/resume/'.$file_name);

				$content  = '<b>Apply For:</b> '.$apply_for;
				$content .= '<br><b>Name:</b> '.$name;
				$content .= '<br><b>Phone:</b> '.$phone;
				$content .= '<br><b>Email:</b> '.$email;
				$content .= '<br><b>Resume:</b> '.$resume;
				if($youtube_profile != ''){
					$content .= '<br><b>Youtube Profile:</b> '.$youtube_profile;
				}

				$this->email->from($email, $name);
				$this->email->to($this->config->item('email'));
				$this->email->subject('Gojojo - Careers');
				$this->email->message(html_entity_decode($content));
				$this->email->send();

				$this->session->set_flashdata('message', 'Your resume has been sent. We will connect with you soon.');
	        }	
		}
		redirect(site_url('careers'));
	}

	function check_email_merchant($str,$validationOnly=false)
	{
		if(!empty($this->merchant_id))
		{
			$email = $this->Merchant_model->check_email($str, $this->merchant_id);
		}
		else
		{
			$email = $this->Merchant_model->check_email($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_email_merchant', 'The requested email is already in use.');
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function check_phone_merchant($str,$validationOnly=false)
	{
		if(!empty($this->merchant_id))
		{
			$email = $this->Merchant_model->check_phone($str, $this->merchant_id);
		}
		else
		{
			$email = $this->Merchant_model->check_phone($str);
		}
		
        if ($email)
       	{
       		if(!$validationOnly){
				$this->form_validation->set_message('check_phone_merchant', 'The requested phone number is already in use.');
			}
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function seller_registration_do(){//print_r($_POST);exit();
		$this->load->model('Merchant_model');
		$this->load->library('form_validation');
		$this->load->library('upload');
		
		$this->form_validation->set_rules('c_address', 'Company Address', 'trim|required');
		$this->form_validation->set_rules('c_address_one', 'Address Line 1', 'trim|required');
		$this->form_validation->set_rules('c_address_two', 'Address Line 2', 'trim|required');
		$this->form_validation->set_rules('c_city', 'Company Address City', 'trim|required');
		$this->form_validation->set_rules('c_state', 'Company Address State', 'trim|required');
		$this->form_validation->set_rules('c_pincode', 'Company Address Pincode', 'trim|required|integer|min_length[6]');
		$this->form_validation->set_rules('is_same', 'Same as Registered Address', 'trim');
		$this->form_validation->set_rules('p_address_one', 'Pickup Address Line 1', 'trim|required');
		$this->form_validation->set_rules('p_address_two', 'Pickup Address Line 1', 'trim|required');
		$this->form_validation->set_rules('p_city', 'Pickup Address City', 'trim|required');
		$this->form_validation->set_rules('p_state', 'Pickup Address State', 'trim|required');
		$this->form_validation->set_rules('p_pincode', 'Pickup Address Pincode', 'trim|required|integer|min_length[6]');
		
		$this->form_validation->set_rules('oo_name', 'Orders/Operations Name', 'trim|required');
		$this->form_validation->set_rules('oo_phone', 'Orders/Operations Phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone_merchant');
		$this->form_validation->set_rules('oo_email', 'Orders/Operations Email', 'trim|required|valid_email|callback_check_email_merchant');
		$this->form_validation->set_rules('dh_name', 'Director/Head Name', 'trim|required');
		$this->form_validation->set_rules('dh_email', 'Director/Head Email', 'trim|required|valid_email|callback_check_email_merchant');
		$this->form_validation->set_rules('dh_phone', 'Director/Head Phone', 'trim|required|regex_match[/^[0-9]{10}$/]|callback_check_phone_merchant');
		
		$this->form_validation->set_rules('brand_name', 'Brand Name', 'trim|required');
		$this->form_validation->set_rules('catalogue_size', 'Catalogue Size', 'trim|required');
		$this->form_validation->set_rules('year_of_operation', 'Year of Operation', 'trim|required');
		$this->form_validation->set_rules('monthly_turnover', 'Monthly Turnover', 'trim|required');
		$this->form_validation->set_rules('products_sla', 'SLA for Products', 'trim|required');
		//$this->form_validation->set_rules('products_category', 'Products Category', 'trim|required|required');
		$this->form_validation->set_rules('business_nature', 'Nature Of Business', 'trim|required|required');
		$this->form_validation->set_rules('other_business', 'Please Specify If another', 'trim');
		//$this->form_validation->set_rules('online_presence', 'Online Presence', 'trim');
		
		$this->form_validation->set_rules('account_holder_name', 'Account Holder Name', 'trim|required');
		$this->form_validation->set_rules('account_number', 'Account Number', 'trim|required');
		$this->form_validation->set_rules('account_bank_name', 'Bank Name', 'trim|required');
		$this->form_validation->set_rules('account_bank_ifsc', 'IFSC Code', 'trim|required');
		$this->form_validation->set_rules('company_registration', 'Company Registration Number', 'trim|required');
		$this->form_validation->set_rules('pan_number', 'PAN Number', 'trim|required');
		$this->form_validation->set_rules('gst_number', 'GST Number', 'trim|required');
		//$this->form_validation->set_rules('userfile', 'Userfile', 'trim');
		$this->form_validation->set_rules('tnc', 'Terms And Conditions', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
		} else {
			$categories=$this->input->post('products_category')==''?array():$this->input->post('products_category');
			if(!empty($categories)){
				
				$this->load->helper('text');
				$slug 	= $this->input->post('brand_name');
				$slug	= url_title(convert_accented_characters(substr($slug,0,80)), 'dash', TRUE);
				$slug	= $this->Merchant_model->validate_slug($slug);
				
				
				
				$files = $_FILES;
				$images=array();$image_error=false;
				if(!empty($files)){
					$total=count($_FILES['userfile']['name']);
					for($i=0; $i<$total; $i++) {
						$var = $files['userfile']['name'][$i];
						$images[$i]='';
						if($var!=''){
							$_FILES['userfile']['name']= $files['userfile']['name'][$i];
							$_FILES['userfile']['type']= $files['userfile']['type'][$i];
							$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
							$_FILES['userfile']['error']= $files['userfile']['error'][$i];
							$_FILES['userfile']['size']= $files['userfile']['size'][$i];
							
							$config['upload_path'] = './uploads/';
							$config['allowed_types'] = 'gif|jpg|jpeg|png';
							$config['max_size'] = '5000';
							$config['overwrite']     = TRUE;
							$config['encrypt_name'] = TRUE;
							$this->upload->initialize($config);
							if($this->upload->do_upload())
							{
								$data = array('upload_data' => $this->upload->data());
								$images[$i]=$data['upload_data']['file_name'];
							}
							else
							{
								$image_error=true;
								$this->session->set_flashdata('message', $this->upload->display_errors());
							}
						}
					}
				}
				if(!$image_error){
					$time = date('Y-m-d H:i:s',time());
					//$name=explode(' ',$this->input->post('dh_name'));
					//$lname=array_shift($name);
					$save['store_name'] 	= ucwords($this->input->post('brand_name'));
					$save['firstname'] 		= ucwords($this->input->post('dh_name'));
					$save['lastname'] 		= ucwords($this->input->post('dh_name'));
					$save['email'] 			= $this->input->post('dh_email');
					$save['phone'] 			= $this->input->post('dh_phone');
					$save['password'] 		= sha1(preg_replace('/\s+/', '', strtolower($save['store_name'])).substr($save['phone'],0,5).'#');
					$save['access'] 		= 'Merchant';
					$save['created_at'] 	= $time;
					$save['updated_at'] 	= $time;
					$save['address'] 		= $this->input->post('p_address_one').','.$this->input->post('p_address_two');
					$save['city'] 			= $this->input->post('p_city');
					$save['state'] 			= $this->input->post('p_state');
					$save['zip'] 			= $this->input->post('p_pincode');
					$save['description'] 	= '';
					$save['logo'] 			= empty($images)?'':$images[0];
					$save['enabled'] 		= 0;
					$save['slug'] 			= $slug;
					$save['tin'] 			= '';
					$save['cst'] 			= $this->input->post('gst_number');
					$save['sign'] 			= empty($images)?'':$images[5];
					$save['pan'] 			= empty($images)?'':$images[1];
					$save['pan_number'] 	= $this->input->post('pan_number');
					$save['landline'] 		= '';
					$save['service'] 		= '';
					$save['account_holder'] = $this->input->post('account_holder_name');
					$save['account_number'] = $this->input->post('account_number');
					$save['bank_name'] 		= $this->input->post('account_bank_name');
					$save['ifsc'] 		    = $this->input->post('account_bank_ifsc');
					$save['business'] 		= $this->input->post('other_business')!=''?$this->input->post('other_business'):$this->input->post('business_nature');
					$save['revenue'] 		= $this->input->post('monthly_turnover');
					$save['website'] 		= '';
					$save['visitors'] 		= '';
					$save['clients'] 		= '';
					$save['other_ecommerce']= $this->input->post('online_presence')==''?'':implode(',',$this->input->post('online_presence'));
					$save['web_link'] 		= '';
					$save['stores'] 		= '';
					$save['stock'] 			= '';
					$save['primary_category']= '';
					// save info
					$merchant_id_return = $this->Merchant_model->sellar_registration($save,$images,$categories);

					$this->load->library('email');
					/*$config_mail = Array(
						'protocol'  => 'smtp',
						'smtp_host' => 'ssl://smtp.zoho.com',
						'smtp_port' => '465',
						'smtp_user' => 'reachus@gojojo.in',
						'smtp_pass' => 'qwerty1!',
						'mailtype'  => 'html',
						'newline'   => "\r\n"
					);
					$this->email->initialize($config_mail);

					$content  = '<b>First Name:</b> '.$save['firstname'];
					$content .= '<br><b>Last Name:</b> '.$save['lastname'];
					$content .= '<br><b>Company Name:</b> '.$save['store_name'];
					$content .= '<br><b>Phone:</b> '.$save['phone'];
					//$content .= '<br><b>Landline:</b> '.$save['landline'];
					$content .= '<br><b>Email:</b> '.$save['email'];
					//$content .= '<br><b>Service interested in:</b> '.$save['service'];
					//$content .= '<br><b>Primary Category:</b> '.$save['primary_category'];
					$content .= '<br><b>Business:</b> '.$save['business'];
					$content .= '<br><b>Approximate Annual Revenue:</b> '.$save['revenue'];
					//$content .= '<br><b>Website:</b> '.$save['website'];
					//$content .= '<br><b>Monthly Vistors to your site:</b> '.$save['visitors'];
					//$content .= '<br><b>Clients:</b> '.$save['clients'];
					$content .= '<br><b>Collaborated with anyone else:</b> '.$save['other_ecommerce'];
					//$content .= '<br><b>Other ecommerce store link:</b> '.$save['web_link'];
					//$content .= '<br><b>Number of Stores:</b> '.$save['stores'];
					//$content .= '<br><b>Number of Products in Stock:</b> '.$save['stock'];
					$content .= '<br><b>GST Registration No.:</b> '.$save['cst'];
					//$content .= '<br><b>Any other information:</b> '.$save['description'];

					$this->email->from($email, $name);
					$this->email->to($this->config->item('email'));
					$this->email->subject('Gojojo - New Merchant Registration');
					$this->email->message(html_entity_decode($content));
					$this->email->send();*/

					$this->session->set_flashdata('message', 'Your request has been sent. We will revert you soon.');
				}
			}else{
				$this->session->set_flashdata('message', 'Select Atleast One Product Category.');
			}
		}
		redirect(site_url('seller-registration'));
	}

	function popular_products($html=false,$category=false){
		$products = $this->Product_model->popular_products(20,false,$category);

		if(count($products) > 0){
			if(!$html){
				die(json_encode(array('result'=>true,'data'=>$products)));
			} else {
				$used = array();
				foreach($products as $product):
					if(!in_array($product->id, $used)){
						$used[] = $product->id;
					} else {
						continue;
					}
					?>
	                <li class="owl-item match-item">
                        <div class="left-block">

                            <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                                <div class="group-price">
                                    <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                                </div>
                            <?php } ?>
                            
                            <a href="<?php echo site_url($product->slug); ?>">
                                <?php
                                $photo  = theme_img('no_picture.png', lang('no_image_available'));
                                
                                $product->images = array_values((array)json_decode($product->images));
                        
                                if(!empty($product->images[0]))
                                {
                                    $primary    = $product->images[0];
                                    foreach($product->images as $photo)
                                    {
                                        if(isset($photo->primary))
                                        {
                                            $primary    = $photo;
                                        }
                                    }

                                    $photo  = '<img class="img img-responsive lazy" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                                }
                                echo $photo;
                                ?>
                            </a>

                            <div class="quick-view">
                                <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" ></a>

                                <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                            </div>
                            <div class="add-to-cart">
                            	<?php if(time() > strtotime($product->live_on)){ ?>
                                	<a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                            	<?php } else { ?>
                                    <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="right-block">
                            <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>

                            <div class="content_price">
                                <?php if(($product->saleprice > 0) || ($product->offer_price > 0)):?>
                                    <span class="price product-price"><?php echo format_currency(min(array_filter(array($product->saleprice, $product->offer_price)))); ?></span>
                                    <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                                <?php else: ?>
                                    <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                                <?php endif; ?>
                                
                            </div>
                            <?php if($product->rating > 0){ ?>
                                <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($product->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                                </div>
                            <?php } ?>
                            <?php if($product->offers_count>0){
                                echo '<p class="fs-12 mt-10"><span class="colorGreen">'.$product->offers_count.' offer'.(($product->offers_count > 1)?'s':'').' available</span></p>';
                            }
                            ?>
                        </div>
	                </li>
	            <?php endforeach;
			}
		} else{
			if(!$html){
				die(json_encode(array('result'=>false)));
			} else {
				echo 'No Product Found.';
			}
		}
	}

	function sale_products($category=false){
		$products = $this->Product_model->sale_products(20,$category);

		if(count($products) > 0){
			$used = array();
			foreach($products as $product):
				if(!in_array($product->id, $used)){
					$used[] = $product->id;
				} else {
					continue;
				}
				?>
                <li class="owl-item match-item">
                    <div class="left-block">

                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                            <div class="group-price">
                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                            </div>
                        <?php } ?>
                        
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            $product->images = array_values((array)json_decode($product->images));
                    
                            if(!empty($product->images[0]))
                            {
                                $primary    = $product->images[0];
                                foreach($product->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img class="img img-responsive lazy" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                            }
                            echo $photo;
                            ?>
                        </a>

                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" ></a>

                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                        </div>
                        <div class="add-to-cart">
                        	<?php if(time() > strtotime($product->live_on)){ ?>
                            	<a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                        	<?php } else { ?>
                                <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>

                        <div class="content_price">
                            <?php if(($product->saleprice > 0) || ($product->offer_price > 0)):?>
                                <span class="price product-price"><?php echo format_currency(min(array_filter(array($product->saleprice, $product->offer_price)))); ?></span>
                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                            <?php else: ?>
                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                            <?php endif; ?>
                            
                        </div>
                        <?php if($product->rating > 0){ ?>
                            <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($product->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span>

                            </div>
                        <?php } ?>
                        <?php if($product->offers_count>0){
                            echo '<p class="fs-12 mt-10"><span class="colorGreen">'.$product->offers_count.' offer'.(($product->offers_count > 1)?'s':'').' available</span></p>';
                        }
                        ?>
                    </div>
                </li>
            <?php endforeach;
		} else {
			echo 'No Product Found.';
		}
	}

	function most_viewed($category=false){
		$products = $this->Product_model->most_viewed_home(20,$category);

		if(count($products) > 0){
			$used = array();
			foreach($products as $product):
				if(!in_array($product->id, $used)){
					$used[] = $product->id;
				} else {
					continue;
				}
				?>
                <li class="owl-item match-item">
                    <div class="left-block">

                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                            <div class="group-price">
                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                            </div>
                        <?php } ?>
                        
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            $product->images = array_values((array)json_decode($product->images));
                    
                            if(!empty($product->images[0]))
                            {
                                $primary    = $product->images[0];
                                foreach($product->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img class="img img-responsive lazy" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                            }
                            echo $photo;
                            ?>
                        </a>

                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" ></a>

                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                        </div>
                        <div class="add-to-cart">
                        	<?php if(time() > strtotime($product->live_on)){ ?>
                            	<a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                        	<?php } else { ?>
                                <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>

                        <div class="content_price">
                            <?php if(($product->saleprice > 0) || ($product->offer_price > 0)):?>
                                <span class="price product-price"><?php echo format_currency(min(array_filter(array($product->saleprice, $product->offer_price)))); ?></span>
                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                            <?php else: ?>
                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                            <?php endif; ?>
                            
                        </div>
                        <?php if($product->rating > 0){ ?>
                            <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($product->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span> <br>


                            </div>
                        <?php } ?>
                        <?php if($product->offers_count>0){
                            echo '<p class="fs-12 mt-10"><span class="colorGreen">'.$product->offers_count.' offer'.(($product->offers_count > 1)?'s':'').' available</span></p>';
                        }
                        ?>
                    </div>
                </li>
            <?php endforeach;
		} else {
			echo 'No Product Found.';
		}
	}

	function new_products($category=false){
		$products = $this->Product_model->new_products(20,$category);

		if(count($products) > 0){
			$used = array();
			foreach($products as $product):
				if(!in_array($product->id, $used)){
					$used[] = $product->id;
				} else {
					continue;
				}
				?>
                <li class="owl-item match-item">
                    <div class="left-block">

                        <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')) { ?>
                            <div class="group-price">
                                <span class="product-stock"><?php echo lang('out_of_stock');?></span>
                            </div>
                        <?php } ?>
                        
                        <a href="<?php echo site_url($product->slug); ?>">
                            <?php
                            $photo  = theme_img('no_picture.png', lang('no_image_available'));
                            
                            $product->images = array_values((array)json_decode($product->images));
                    
                            if(!empty($product->images[0]))
                            {
                                $primary    = $product->images[0];
                                foreach($product->images as $photo)
                                {
                                    if(isset($photo->primary))
                                    {
                                        $primary    = $photo;
                                    }
                                }

                                $photo  = '<img class="img img-responsive lazy" src="'.theme_img('loading.gif').'" data-src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
                            }
                            echo $photo;
                            ?>
                        </a>

                        <div class="quick-view">
                            <a title="Add to my wishlist" class="heart add-to-wishlist" href="javascript:void(0);" data-id="<?php echo $product->id;?>" data-pm-id="<?php echo $product->pm_id;?>" ></a>

                            <a title="Quick view" class="search" href="<?php echo site_url($product->slug); ?>"></a>
                        </div>
                        <div class="add-to-cart">
                        	<?php if(time() > strtotime($product->live_on)){ ?>
                            	<a title="Add to Cart" class="add-to-cart-cart-btn" href="javascript:;" data-id=<?php echo $product->id;?> data-pm-id=<?php echo $product->pm_id;?> >Add to Cart</a>
                        	<?php } else { ?>
                                <a class="product-coming-soon-sm" href="javascript:;">Coming Soon</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="right-block">
                        <h5 class="product-name"><a href="<?php echo site_url($product->slug); ?>"><?php echo format_product_name($product->name);?></a></h5>

                        <div class="content_price">
                            <?php if(($product->saleprice > 0) || ($product->offer_price > 0)):?>
                                <span class="price product-price"><?php echo format_currency(min(array_filter(array($product->saleprice, $product->offer_price)))); ?></span>
                                <span class="price old-price"><?php echo format_currency($product->price); ?></span>
                            <?php else: ?>
                                <span class="price product-price"><?php echo format_currency($product->price); ?></span>
                            <?php endif; ?>
                            
                        </div>
                        <?php if($product->rating > 0){ ?>
                            <div class="">
                                                    <span class="reviewRating">
                                                        <span class="badge badge-success rate-badge"><?php echo round($product->rating,1);?> <i class="fa fa-star"></i> </span>
                                                    </span> <br>


                            </div>
                        <?php } ?>
                        <?php if($product->offers_count>0){
                            echo '<p class="fs-12 mt-10"><span class="colorGreen">'.$product->offers_count.' offer'.(($product->offers_count > 1)?'s':'').' available</span></p>';
                        }
                        ?>
                    </div>
                </li>
            <?php endforeach;
		} else {
			echo 'No Product Found.';
		}
	}

	public function get_mini_cart(){
		$return = array();
		$return['items'] = $this->go_cart->total_items();
		$return['items_string'] = $this->go_cart->total_items().' item';
		if($this->go_cart->total_items()>1){
			$return['items_string'] = $return['items_string'].'s';
		}
		$return['total'] = format_currency($this->go_cart->total());
		$return['items_total_string'] = $return['items_string'].' - '.$return['total'];
		die(json_encode($return));
	}

	function check_service_availability(){
		$zip = (int)trim($this->input->post('zip'));
		$return['success'] = false;
        $return['data'] = '<span class="red">Please enter a valid pincode.</span>';
		if($zip){
			if(strlen($zip) == 6){
				$this->load->model('Location_model');
				$result = $this->Location_model->getZip($zip, true);

				if($result){
					$return['success'] = true;
	            	$return['data'] = '<span class="colorGreen fs-12">Deliverable to '.$result->city.'</span>';
				} else {
					$return['success'] = false;
					$return['data'] = '<span class="red fs-12">Delivery not available.</span>';
				}
			}
        }
        die(json_encode($return));
	}

	function get_primary_categories_basic(){
		$categories = $this->Category_model->get_primary_categories_basic();
		$return = array();
		if(!empty($categories)){
			$return['data'] 	= $categories;
			$return['success'] 	= true;
		} else {
			$return['success'] 	= false;
		}
		die(json_encode($return));
	}

	function brand($id)
	{
		$this->load->model('Brand_model');
		//get the brand
		$data['brand'] = $this->Brand_model->get_brand($id);
				
		if (!$data['brand'] || $data['brand']->enabled==0)
		{
			show_404();
		}
				
		//set up pagination
		$segments	= $this->uri->total_segments();
		$base_url	= $this->uri->segment_array();
		
		if($data['brand']->slug == $base_url[count($base_url)])
		{
			$page	= 0;
			$segments++;
		}
		else
		{
			$page	= array_splice($base_url, -1, 1);
			$page	= $page[0];
		}
		
		$data['base_url']	= $base_url;
		$base_url			= implode('/', $base_url);
		
		$data['product_columns']	= $this->config->item('product_columns');
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		
		$data['seo_title']	= (!empty($data['brand']->seo_title))?$data['brand']->seo_title:$data['brand']->name;
		$data['page_title']	= $data['brand']->name;
		
		$sort_array = array(
							'name/asc' => array('by' => 'products.name', 'sort'=>'ASC'),
							'name/desc' => array('by' => 'products.name', 'sort'=>'DESC'),
							'price/asc' => array('by' => 'sort_price', 'sort'=>'ASC'),
							'price/desc' => array('by' => 'sort_price', 'sort'=>'DESC'),
							);
		$sort_by	= array('by'=>'name', 'sort'=>'ASC');
	
		if(isset($_GET['by']))
		{
			if(isset($sort_array[$_GET['by']]))
			{
				$sort_by	= $sort_array[$_GET['by']];
			}
		}
		
		$filters = array();
		$min_price = 0;
		$max_price = 0;
		$filters['facets'] = array();

		if(isset($_GET['min_price'])){
			$filters['min_price'] = $_GET['min_price'];
			$min_price = $filters['min_price'];
		}
		if(isset($_GET['max_price'])){
			$filters['max_price'] = $_GET['max_price'];
			$max_price = $filters['max_price'];
		}
		if(isset($_GET['facets'])){
			if(!empty($_GET['facets'])){
				foreach ($_GET['facets'] as $facet_key => $facet_value) {
					$filters['facets'][$facet_key] = explode(',',urldecode($_GET['facets'][$facet_key]));
				}
			}
		}
		
		$config['per_page']		= 24;
		
		$data['products']		= $this->Product_model->get_products_brand($data['brand']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters);
		$filter_options_return	= $this->Product_model->get_products_brand_filter_options($data['brand']->id);

		$filter_options = array();
		if(!empty($filter_options_return)){
			foreach ($filter_options_return as $filter_option) {
				$filter_option_name = strtolower($filter_option->name);

				$exploded_this_facets_values = array();
				if(isset($_GET['facets'][$filter_option_name])){
					$exploded_this_facets_values = explode(',', $_GET['facets'][$filter_option_name]);
				}

				if(isset($filter_options[$filter_option_name])){
					if(!in_array($filter_option->ov_name, $filter_options[$filter_option_name])){
						$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
						$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
						$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

						if(isset($_GET['facets'][$filter_option_name])){
							if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
								$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
							}
						}
					}
				} else {
					$filter_options[$filter_option_name][$filter_option->ov_name]['name'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['value'] = $filter_option->ov_name;
					$filter_options[$filter_option_name][$filter_option->ov_name]['image'] = $filter_option->ov_image;
					$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = false;

					if(isset($_GET['facets'][$filter_option_name])){
						if(in_array($filter_option->ov_name, $exploded_this_facets_values)){
							$filter_options[$filter_option_name][$filter_option->ov_name]['checked'] = true;
						}
					}
				}
			}
		}
		$data['filter_options'] = $filter_options;

		$product_count = $this->Product_model->get_products_brand($data['brand']->id, $config['per_page'], $page, $sort_by['by'], $sort_by['sort'], $filters,true);

		//set up pagination
		$this->load->library('pagination');
		$config['base_url']		= site_url($base_url);
		
		$config['uri_segment']	= $segments;
		$config['total_rows']	= $product_count;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="bottom-pagination"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['prev_link'] = '<span aria-hidden="true">&laquo; Prev</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '<span aria-hidden="true">Next &raquo;</span>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		unset($_GET['filters_changed']);

		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				
		$this->pagination->initialize($config);

		$usedProducts = array();
		foreach ($data['products'] as $key => &$p)
		{
			if(!in_array($p->id, $usedProducts)){
				$usedProducts[] = $p->id;
			} else {
				continue;
			}

			$p->images	= (array)json_decode($p->images);
			$p->options	= $this->Option_model->get_product_options($p->id);

			if($p->saleprice > 0):
                $pprice = $p->saleprice;
            else:
                $pprice = $p->price;
            endif;

            if($key == 0){
            	if($min_price == 0) $min_price=$pprice;
            	if($max_price == 0) $max_price=$pprice;
            } else {
				if($pprice < $min_price){
					$min_price=$pprice;
				}
				if($pprice > $max_price){
	            	$max_price=$pprice;
				}
			}
		}

		/*if($max_price < $min_price){
			$min_price = 0;
		}*/
		$filters['min_price'] = $min_price; 
		$filters['max_price'] = $max_price;

		$data['filters'] = $filters;

		$this->view('brand', $data);
	}

	function error_404(){
		$data['page_title']			= '404 Page Not Found';
		$data['seo_title']			= '404 Page Not Found';
		$this->view('error_404', $data);
	}

}