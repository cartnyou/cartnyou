<?php
//set "code" for searches
if(!$code)
{
	$code = '';
}
else
{
	$code = '/'.$code;
}
function sort_url($lang, $by, $sort, $sorder, $code, $merchant_folder)
{
	if ($sort == $by)
	{
		if ($sorder == 'asc')
		{
			$sort	= 'desc';
			$icon	= ' <i class="fa fa-chevron-up"></i>';
		}
		else
		{
			$sort	= 'asc';
			$icon	= ' <i class="fa fa-chevron-down"></i>';
		}
	}
	else
	{
		$sort	= 'asc';
		$icon	= '';
	}
		

	$return = site_url($merchant_folder.'/products/index/'.$by.'/'.$sort.'/'.$code);
	
	echo '<a href="'.$return.'">'.lang($lang).$icon.'</a>';

}

if(!empty($term)):
	$term = json_decode($term);
	if(!empty($term->term) || !empty($term->category_id)):?>
		<div class="alert alert-info">
			<?php echo sprintf(lang('search_returned'), intval($total));?>
		</div>
	<?php endif;?>
<?php endif;?>

<script type="text/javascript">
function areyousure()
{
	return confirm('<?php echo lang('confirm_delete_product');?>');
}
function areyousureDisable()
{
	return confirm('Are you sure you want to disable this product?');
}
</script>
<style type="text/css">
	.pagination {
		margin:0px;
		margin-top:-3px;
	}
</style>
<div class="row">
	<div class="col-md-12" style="border-bottom:1px solid #f5f5f5;">
		<div class="row">
			<div class="col-md-4">
				<a class="btn btn-sm btn-primary" style="font-weight:normal;" href="<?php echo site_url($this->config->item('merchant_folder').'/products/addProduct');?>"><i class="fa fa-plus-circle"></i> <?php echo lang('add_new_product');?></a>
			</div>
			<div class="col-md-8">
				<?php echo form_open($this->config->item('merchant_folder').'/products/index', 'class="form-inline" style="float:right"');?>
					<fieldset>
						<?php
						
						function list_categories($id, $categories, $sub='') {

							foreach ($categories[$id] as $cat):?>
							<option class="" value="<?php echo $cat->id;?>"><?php echo  $sub.$cat->name; ?></option>
							<?php
							if (isset($categories[$cat->id]) && sizeof($categories[$cat->id]) > 0)
							{
								$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
								$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
								list_categories($cat->id, $categories, $sub2);
							}
							endforeach;
						}
						
						if(!empty($categories))
						{
							echo '<select name="category_id" class="form-control">';
							echo '<option value="">-'.lang('filter_by_category').'-</option>';
							list_categories(0, $categories);
							echo '</select>';
							
						}?>
						
						<input type="text" class="col-md-2 form-control" name="term" placeholder="<?php echo lang('search_term');?>" /> 
						<button class="btn btn-sm btn-success table-group-action-submit" name="submit" value="search"><i class="fa fa-search"></i> <?php echo lang('search')?></button>
						<a class="btn btn-sm btn-default" href="<?php echo site_url($this->config->item('merchant_folder').'/products/index');?>">Reset</a>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="btn-group pull-right">
</div>

<?php echo form_open($this->config->item('merchant_folder').'/products/bulk_save', array('id'=>'bulk_form'));?>
	<table class="table table-hover">
		<thead>
			<tr>
				<th><?php echo sort_url('sku', 'sku', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th><?php echo sort_url('name', 'name', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th><?php echo sort_url('price', 'price', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th><?php echo sort_url('saleprice', 'saleprice', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th><?php echo sort_url('quantity', 'quantity', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th><?php echo sort_url('enabled', 'enabled', $order_by, $sort_order, $code, $this->config->item('merchant_folder'));?></th>
				<th>
					<button class="btn purple pull-right" href="#"><i class="fa fa-save"></i> <?php echo lang('bulk_save');?></button>
				</th>
			</tr>
		</thead>
		<tbody>
		<?php echo (count($products) < 1)?'<tr><td style="text-align:center;" colspan="7">'.lang('no_products').'</td></tr>':''?>
	<?php foreach ($products as $product):?>
			<tr>
				<td class="col-md-1"><?php echo form_input(array('name'=>'product['.$product->id.'][sku]','value'=>form_decode($product->sku), 'class'=>'col-md-1 form-control'));?></td>
				<td class="col-md-4"><?php echo form_input(array('name'=>'product['.$product->id.'][name]','value'=>form_decode($product->name), 'class'=>'col-md-2 form-control form-control form-control'));?></td>
				<td class="col-md-1"><?php echo form_input(array('name'=>'product['.$product->id.'][price]', 'value'=>set_value('price', $product->price), 'class'=>'col-md-1 form-control form-control'));?></td>
				<td class="col-md-1"><?php echo form_input(array('name'=>'product['.$product->id.'][saleprice]', 'value'=>set_value('saleprice', $product->saleprice), 'class'=>'col-md-1 form-control'));?></td>
				<td class="col-md-1"><?php echo ((bool)$product->track_stock)?form_input(array('name'=>'product['.$product->id.'][quantity]', 'value'=>set_value('quantity', $product->quantity), 'class'=>'col-md-1 form-control')):'N/A';?></td>
				<td>
					<?php
					 	$options = array(
			                  '1'	=> lang('enabled'),
			                  '0'	=> lang('disabled')
			                );

						echo form_dropdown('product['.$product->id.'][enabled]', $options, set_value('enabled',$product->variant_enabled), 'class="col-md-2 form-control"');
					?>
				</td>
				<td>
					<span class="btn-group pull-right">
						<!-- <a class="btn btn-sm btn-success" href="<?php echo  site_url($this->config->item('merchant_folder').'/products/form/'.$product->id.'/0/'.$this->session->userdata('merchant')['id']);?>"><i class="fa fa-pencil"></i>  <?php echo lang('edit');?></a> -->
						<a class="btn btn-sm btn-success" href="<?php echo  site_url($this->config->item('merchant_folder').'/products/variant/'.$product->id.'/0/'.$product->pm_id);?>"><i class="fa fa-pencil"></i>  <?php echo lang('edit');?></a>
						<!-- <a class="btn btn-sm yellow" href="<?php echo  site_url($this->config->item('merchant_folder').'/products/form/'.$product->id.'/1');?>"><i class="icon-share-alt"></i> <?php echo lang('copy');?></a> -->
						<!-- <a class="btn btn-sm btn-danger" href="<?php echo  site_url($this->config->item('merchant_folder').'/products/disable_variant/'.$product->pm_id);?>" onclick="return areyousureDisable();"><i class="fa fa-trash-o"></i> Disable</a>
						<a class="btn btn-sm btn-danger" href="<?php echo  site_url($this->config->item('merchant_folder').'/products/delete_variant/'.$product->pm_id);?>" onclick="return areyousureDisable();"><i class="fa fa-trash-o"></i> Delete</a>-->
					</span>
				</td>
			</tr>
	<?php endforeach; ?>
		</tbody>
	</table>
</form>

<?php echo $this->pagination->create_links();?>	&nbsp;