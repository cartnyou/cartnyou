<?php include('header.php'); ?>

<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset mt-login-5-bsfix">
            <div class="login-bg" style="background-image:url(<?php echo base_url('/assets/pages/img/login/bg1.jpg');?>">
                <img class="login-logo" src="<?php echo base_url('assets/img/logo.png');?>" />
            </div>
        </div>
        <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
            <div class="login-content">

                <h1><?php echo $this->config->item('company_name');?> Merchant Login</h1>
                <p>Be it a manufacturer, vendor or supplier, simply sell your products online on Gojojo and become a top ecommerce player with minimum investment.</p>

                <?php echo form_open($this->config->item('merchant_folder').'/login', array('class'=>'login-form','id'=>'login-form')) ?>
                	<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
        			<input type="hidden" value="submitted" name="submitted"/>

                    <?php if(!$this->auth->is_merchant_logged_in(false, false)):?>
			            <?php
			            //lets have the flashdata overright "$message" if it exists
			            if($this->session->flashdata('message'))
			            {
			                $message    = $this->session->flashdata('message');
			            }

			            if($this->session->flashdata('error'))
			            {
			                $error  = $this->session->flashdata('error');
			            }

			            if(function_exists('validation_errors') && validation_errors() != '')
			            {
			                $error  = validation_errors();
			            }
			            ?>

			            <div id="js_error_container" class="alert alert-danger" style="display:none;">
			                <p id="js_error"></p>
			            </div>

			            <div id="js_note_container" class="alert alert-info" style="display:none;">

			            </div>

			            <?php if (!empty($message)): ?>
			                <div class="alert alert-success">
			                    <a class="close" data-dismiss="alert">×</a>
			                    <?php echo $message; ?>
			                </div>
			            <?php endif; ?>

			            <?php if (!empty($error)): ?>
			                <div class="alert alert-danger">
			                    <a class="close" data-dismiss="alert">×</a>
			                    <?php echo $error; ?>
			                </div>
			            <?php endif; ?>

			        <?php endif;?>

                    <div class="row">
                        <div class="col-xs-6">
                        	<?php echo form_input(array('name'=>'email','class'=>'form-control form-control-solid placeholder-no-fix form-group','placeholder'=>lang('email'),'required'=>'required')); ?>
                        </div>
                        <div class="col-xs-6">
                        	<?php echo form_password(array('name'=>'password','class'=>'form-control form-control-solid placeholder-no-fix form-group','placeholder'=>lang('password'),'required'=>'required')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="rem-password">
                                <label class="rememberme mt-checkbox mt-checkbox-outline">
						            <?php echo form_checkbox(array('name'=>'remember', 'value'=>'true'))?>
						            <?php echo lang('stay_logged_in');?>
						            <span></span>
						        </label>
                            </div>
                        </div>
                        <div class="col-sm-8 text-right">
                            <div class="forgot-password">
                                <!-- <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a> -->
                            </div>
                            <button class="btn green" type="submit">Sign In</button>
                            <br>
                            <a href="javascript:;" id="forgot-password-open">Forgot Password?</a>
                        </div>
                    </div>
                <?php echo form_close(); ?>

                <!-- BEGIN FORGOT PASSWORD FORM -->
                <?php echo form_open($this->config->item('merchant_folder').'/login/forgot_password', array('class'=>'forget-form','id'=>'forget-form')) ?>
                    <h3 class="font-green">Forgot Password ?</h3>
                    <p> Enter your e-mail address below to reset your password. </p>
                    <div class="form-group">
                        <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" required="required" /> </div>
                    <div class="form-actions">
                        <button type="button" id="login-open" class="btn green btn-outline">Back</button>
                        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                    </div>
                </form>
                <!-- END FORGOT PASSWORD FORM -->
            </div>
            <!-- <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-5 bs-reset">
                        <ul class="login-social">
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-7 bs-reset">
                        <div class="login-copyright text-right">
                            <p>Copyright &copy; Keenthemes 2015</p>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>

<script type='text/javascript'>
<?php
$banners = json_encode($banners);
echo "var banners = ". $banners . ";\n";
?>
</script>
<script>
$(function(){
	$(".login-bg").backstretch(
		banners,
		{
	        fade: 1e3,
	        duration: 8e3
    	}
    );

    $('#forgot-password-open').on('click',function(){
    	$('#login-form').css('display','none');
    	$('#forget-form').css('display','block');
    });

    $('#login-open').on('click',function(){
    	$('#forget-form').css('display','none');
    	$('#login-form').css('display','block');
    });
});
</script>

<?php include('footer.php'); ?>
