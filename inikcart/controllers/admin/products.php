<?php

class Products extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
        
		$this->auth->check_access('Admin', true);
		
		$this->load->model(array('Product_model'));
		$this->load->helper('form');
		$this->lang->load('product');
	}

	function index($order_by="name", $sort_order="ASC", $code=0, $page=0, $rows=15)
	{
		
		$data['page_title']	= lang('products');
		
		$data['code']		= $code;
		$term				= false;
		$category_id		= false;
		
		//get the category list for the drop menu
		$data['categories']	= $this->Category_model->get_categories_tiered();

		$this->load->model('Brand_model');
		$data['brands']	= $this->Brand_model->get_brands();
		
		$post				= $this->input->post(null, false);
		$this->load->model('Search_model');
		if($post)
		{
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
		}
		elseif ($code)
		{
			$term			= $this->Search_model->get_term($code);
		}
		
		//store the search term
		$data['term']		= $term;
		$data['order_by']	= $order_by;
		$data['sort_order']	= $sort_order;
		
		$data['products']	= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order, 'rows'=>$rows, 'page'=>$page));

		//total number of products
		$data['total']		= $this->Product_model->products(array('term'=>$term, 'order_by'=>$order_by, 'sort_order'=>$sort_order), true);

		
		$this->load->library('pagination');
		
		$config['base_url']			= site_url($this->config->item('admin_folder').'/products/index/'.$order_by.'/'.$sort_order.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;
		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$config['full_tag_open']	= '<div class="pagination"><ul>';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
		
		$this->view($this->config->item('admin_folder').'/products', $data);
	}
	
	//basic category search
	function product_autocomplete()
	{
		$name	= trim($this->input->post('name'));
		$limit	= $this->input->post('limit');
		
		if(empty($name))
		{
			echo json_encode(array());
		}
		else
		{
			$results	= $this->Product_model->product_autocomplete($name, $limit);
			
			$return		= array();
			
			foreach($results as $r)
			{
				$return[$r->id]	= $r->name;
			}
			echo json_encode($return);
		}
		
	}
	
	function bulk_save()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/products');
		}
				
		foreach($products as $id=>$product)
		{
			$product['id']	= $id;
			$this->Product_model->save($product);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/products');
	}
	
	function form($id = false, $duplicate = false)
	{
		$this->product_id	= $id;
		$this->load->library('form_validation');
		$this->load->model(array('Option_model', 'Category_model', 'Digital_Product_model', 'Settings_model'));
		$this->lang->load('digital_product');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$data['categories']		= $this->Category_model->get_categories_tiered();
		$data['file_list']		= $this->Digital_Product_model->get_list();

		$data['page_title']		= lang('product_form');

		//default values are empty if the product is new
		$data['id']					= '';
		$data['hns']				= '';
		$data['name']				= '';
		$data['description']		= '';
		$data['specification']		= '';
		$data['excerpt']			= '';
		$data['weight']				= '';
		$data['price']				= '';
		$data['strike_price']		= '';
		$data['track_stock'] 		= '';
		$data['seo_title']			= '';
		$data['meta']				= '';
		$data['shippable']			= '';
		$data['taxable']			= '';
		$data['tax_rate']			= 0.00;
		$data['fixed_quantity']		= '';
		$data['quantity']			= '';
		$data['enabled']			= '';
		$data['brand']				= '';
		$data['brand_name']			= '';
		$data['related_products']	= array();
		$data['product_categories']	= array();
		$data['images']				= array();
		$data['product_files']		= array();
		$data['pc']					= '';
		$data['il']					= '';
		$data['ml']					= '';
		$data['cl']					= '';
		$data['pl']					= '';

		//create the photos array for later use
		$data['photos']		= array();
		$data['prepaid_payment_methods']	= $this->Settings_model->get_payment_methods(true);
		if ($id)
		{	
			// get the existing file associations and create a format we can read from the form to set the checkboxes
			$pr_files 		= $this->Digital_Product_model->get_associations_by_product($id);
			foreach($pr_files as $f)
			{
				$data['product_files'][]  = $f->file_id;
			}
			
			// get product & options data
			$data['product_options']			= $this->Option_model->get_product_options($id);
			$data['product_offers']				= $this->Product_model->getProductOffers($id, false, true);
			
			$product							= $this->Product_model->get_product($id);
			
			//if the product does not exist, redirect them to the product list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('admin_folder').'/products');
			}
			
			//set values to db values
			$data['id']					= $id;
			$data['hns']				= $product->hns;
			$data['name']				= $product->name;
			$data['seo_title']			= $product->seo_title;
			$data['meta']				= $product->meta;
			$data['description']		= $product->description;
			$data['specification']		= $product->specification;
			$data['excerpt']			= $product->excerpt;
			$data['weight']				= $product->weight;
			$data['price']				= $product->product_price;
			$data['strike_price']		= $product->product_strike_price;
			$data['shippable']			= $product->shippable;
			$data['taxable']			= $product->taxable;
			$data['tax_rate']			= $this->Product_model->get_product_tax_by_category($id);
			$data['fixed_quantity']		= $product->fixed_quantity;
			$data['enabled']			= $product->enabled;
			$data['brand']				= $product->brand;
			$data['brand_name']			= $product->brand_name;
			$data['pc']					= $product->pc;
			$data['il']					= $product->il;
			$data['ml']					= $product->ml;
			$data['cl']					= $product->cl;
			$data['pl']					= $product->pl;
			
			//make sure we haven't submitted the form yet before we pull in the images/related products from the database
			if(!$this->input->post('submit'))
			{
				
				$data['product_categories']	= array();
				foreach($product->categories as $product_category)
				{
					$data['product_categories'][] = $product_category->id;
				}
				
				$data['related_products']	= $product->related_products;
				$data['images']				= (array)json_decode($product->images);
			}
		}
		
		//if $data['related_products'] is not an array, make it one.
		if(!is_array($data['related_products']))
		{
			$data['related_products']	= array();
		}
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= array();
		}

		
		//no error checking on these
		$this->form_validation->set_rules('caption', 'Caption');
		$this->form_validation->set_rules('primary_photo', 'Primary');

		$this->form_validation->set_rules('hns', 'HNS', 'trim');
		$this->form_validation->set_rules('seo_title', 'lang:seo_title', 'trim');
		$this->form_validation->set_rules('meta', 'lang:meta_data', 'trim');
		$this->form_validation->set_rules('name', 'lang:name', 'trim|required|max_length[128]');
		$this->form_validation->set_rules('description', 'lang:description', 'trim|required');
		$this->form_validation->set_rules('specification', 'Specification', 'trim');
		$this->form_validation->set_rules('excerpt', 'lang:excerpt', 'trim');
		$this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric|floatval');
		$this->form_validation->set_rules('price', 'lang:price', 'trim|numeric|floatval|greater_than[0]');
		$this->form_validation->set_rules('strike_price', 'lang:strike_price', 'trim|numeric|floatval');
		$this->form_validation->set_rules('track_stock', 'lang:track_stock', 'trim|numeric');
		$this->form_validation->set_rules('shippable', 'lang:shippable', 'trim|numeric');
		$this->form_validation->set_rules('taxable', 'lang:taxable', 'trim|numeric');
		//$this->form_validation->set_rules('tax_rate', 'Tax Rate', 'trim|numeric|floatval|required');
		$this->form_validation->set_rules('fixed_quantity', 'lang:fixed_quantity', 'trim|numeric');
		$this->form_validation->set_rules('enabled', 'lang:enabled', 'trim|numeric');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|numeric|required');
		$this->form_validation->set_rules('categories[]','Category', 'required');

		$this->form_validation->set_rules('pc', 'PC', 'trim');
		$this->form_validation->set_rules('il', 'IL', 'trim');
		$this->form_validation->set_rules('ml', 'ML', 'trim');
		$this->form_validation->set_rules('cl', 'CL', 'trim');
		$this->form_validation->set_rules('pl', 'PL', 'trim');
		/*
		if we've posted already, get the photo stuff and organize it
		if validation comes back negative, we feed this info back into the system
		if it comes back good, then we send it with the save item
		
		submit button has a value, so we can see when it's posted
		*/
		
		if($duplicate)
		{
			$data['id']	= false;
		}
		if($this->input->post('submit'))
		{
			//reset the product options that were submitted in the post
			$data['product_options']	= $this->input->post('option');
			$data['related_products']	= $this->input->post('related_products');
			$data['product_categories']	= $this->input->post('categories');
			$data['images']				= $this->input->post('images');
			$data['product_files']		= $this->input->post('downloads');
			
		}
		if ($this->form_validation->run() == FALSE)
		{
			$this->view($this->config->item('admin_folder').'/product_form', $data);
		}
		else
		{
			$this->load->helper('text');
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}

			$first_tax = 0;
			$first_commission = 0;
			if(!empty($categories)){
				foreach($categories as $c){
					$first_category_id 	= $c;
					$categoryInfo 		= $this->Category_model->get_category($c);
					$first_tax 			= $categoryInfo->tax_rate;
					$first_commission 	= $categoryInfo->commission;
					break;
				}
			}

			$save['id']					= $id;
			//$savePM['product_id']		= $id;
			$save['hns']				= $this->input->post('hns');
			$save['name']				= $this->input->post('name');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');
			$save['description']		= $this->input->post('description');
			$save['specification']		= $this->input->post('specification');
			$save['excerpt']			= $this->input->post('excerpt');
			$save['weight']				= $this->input->post('weight');
			$save['product_price']		= $this->input->post('price');
			$save['product_strike_price']= $this->input->post('strike_price');
			$save['track_stock']		= $this->input->post('track_stock');
			$save['fixed_quantity']		= $this->input->post('fixed_quantity');
			$save['enabled']			= $this->input->post('enabled');
			$save['shippable']			= $this->input->post('shippable');
			$save['taxable']			= $this->input->post('taxable');
			$save['tax_rate']			= $first_tax;
			$save['commission']			= $first_commission;
			$save['brand']				= $this->input->post('brand');
			$post_images				= $this->input->post('images');
			$save['pc']					= $this->input->post('pc');
			$save['il']					= $this->input->post('il');
			$save['ml']					= $this->input->post('ml');
			$save['cl']					= $this->input->post('cl');
			$save['pl']					= $this->input->post('pl');
			//print_r($post_images);exit();
			if($primary	= $this->input->post('primary_image'))
			{
				if($post_images)
				{
					$sortImages=array();$toArray=array();
					foreach($post_images as $key => &$pi)
					{
						if($primary == $key)
						{
							$pi['primary']	= true;
							$sortImages[$key]=$pi;
							continue;
						}else{
							$toArray[$key]=$pi;
						}
					}
					$post_images=array_merge($sortImages,$toArray);
				}
			}
			$save['images']				= json_encode($post_images);
			
			
			if($this->input->post('related_products'))
			{
				$save['related_products'] = json_encode($this->input->post('related_products'));
			}
			else
			{
				$save['related_products'] = '';
			}
			
			//save categories
			$categories			= $this->input->post('categories');
			if(!$categories)
			{
				$categories	= array();
			}
			
			$inHouseOffer=$this->input->post('offer_name');
			$extraOffer=$this->input->post('extra_offer_name');
			$specialOffer=$this->input->post('so_name');
			
			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	
			
			// save product 
			$product_id	= $this->Product_model->save($save, $options, $categories, array(), false);
			
			//save offer
			$offers=$this->Product_model->save_offers($product_id, $inHouseOffer, $extraOffer, $specialOffer,$save['product_price'],$save['id']);
			
			// add file associations
			// clear existsing
			$this->Digital_Product_model->disassociate(false, $product_id);
			// save new
			$downloads = $this->input->post('downloads');
			if(is_array($downloads))
			{
				foreach($downloads as $d)
				{
					$this->Digital_Product_model->associate($d, $product_id);
				}
			}			

			$this->session->set_flashdata('message', lang('message_saved_product'));

			//go back to the product list
			redirect($this->config->item('admin_folder').'/products');
		}
	}
	
	
	
	function product_image_form()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_form_multiple()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$data['successCount'] = 0;
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader_multiple', $data);
	}
	
	function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 600;
			$config['height'] = 500;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 235;
			$config['height'] = 235;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			$data['file_name']	= $upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
		$this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader', $data);
	}

	function product_image_upload_multiple()
	{
		$data['file_name'] 	= array();
		$data['error']		= array();	
		$successCount		= 0;

		if(isset($_FILES['upl_files'])){
			$this->load->library('upload');

			$number_of_files_uploaded = count($_FILES['upl_files']['name']);

		    for ($i = 0; $i < $number_of_files_uploaded; $i++) :
		    	$_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
		    	$_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
		    	$_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
		      	$_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
		      	$_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];

		      	$config = array(
			        'allowed_types' => 'jpg|jpeg|png|gif|JPG|JPEG',
			        //'max_size'      => $this->config->item('size_limit'),
			        'upload_path'	=> 'uploads/images/full',
			        'encrypt_name'	=> false,
			        'remove_spaces'	=> false,
					'overwrite'     =>true
		      	);
		      	
		      	$this->upload->initialize($config);

		      	if ( ! $this->upload->do_upload()) :
		        	$data['error'][$i]	= $this->upload->display_errors();

		      	else :
		        	$upload_data = $this->upload->data();

		        	$this->load->library('image_lib');

					//this is the larger image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 600;
					$config['height'] = 500;
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();

					//small image
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 235;
					$config['height'] = 235;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$this->image_lib->clear();

					//cropped thumbnail
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 150;
					$config['height'] = 150;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					//card
					$config['image_library'] = 'gd2';
					$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
					$config['new_image']	= 'uploads/images/card/'.$upload_data['file_name'];
					$config['maintain_ratio'] = FALSE;
					$config['width'] = 268;
					$config['height'] = 327;
					$this->image_lib->initialize($config); 	
					$this->image_lib->resize();	
					$this->image_lib->clear();

					$data['file_name'][$i]	= $upload_data['file_name'];

					$successCount++;

		      	endif;
		    endfor;
		}

		$data['successCount'] = $successCount;

	    $this->load->view($this->config->item('admin_folder').'/iframe/product_image_uploader_multiple', $data);		
	}
	
	function delete($id = false)
	{
		if ($id)
		{	
			$product	= $this->Product_model->get_product($id);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('admin_folder').'/products');
			}
			else
			{

				// remove the slug
				$this->load->model('Routes_model');
				$this->Routes_model->delete($product->route_id);

				//if the product is legit, delete them
				$this->Product_model->delete_product($id);

				$this->session->set_flashdata('message', lang('message_deleted_product'));
				redirect($this->config->item('admin_folder').'/products');
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('admin_folder').'/products');
		}
	}

	function ratings($page=0){
        $data['page_title'] = 'Product Ratings';
        $rows = 10;
        $data['data'] = $this->Product_model->get_products_ratings(false, $rows, $page);
        $data['total'] = $this->Product_model->get_products_ratings(true);

        $this->load->library('pagination');
        
        $config['base_url']         = site_url($this->config->item('admin_folder').'/products/ratings/');
        $config['total_rows']       = $data['total'];
        $config['per_page']         = $rows;
        $config['uri_segment']      = 4;
        $config['first_link']       = 'First';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Last';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';

        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        
        $config['prev_link']        = '&laquo;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';

        $config['next_link']        = '&raquo;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        
        $this->pagination->initialize($config);
        $this->view($this->config->item('admin_folder').'/product_ratings', $data);
    }

    function rating_change($pr_id){
        $affected_rows   = $this->Product_model->rating_change($pr_id);

        if ($affected_rows > 0){
            $this->session->set_flashdata('message', 'Changes saved successfully.');  
        } else {
            $this->session->set_flashdata('error', 'An unknown error occured.');
        }
        redirect($this->config->item('admin_folder').'/products/ratings');
    }

    function bulk_upload(){
		$data['page_title']	= lang('products').' Bulk Upload';

		if(isset($_POST["submit"])) {

			if($this->input->post('merchant_id')){
				$merchant_id = trim($this->input->post('merchant_id'));

				$this->load->helper('text');
				$this->load->model('Routes_model');

			    $file = fopen($_FILES["file"]["tmp_name"], "r");
				
				$successCount = 0;
				$flag = true;
				while(! feof($file)){
					$row = fgetcsv($file);

					// skip header row
					if($flag) { $flag = false; continue; }

					$save = array();
					$savePM = array();

					$savePM['merchant_id']		= $merchant_id;

					$savePM['sku'] = $row[0];
					$save['name'] = $row[1];

					if($save['name'] == ''){
						break;
					}
					
					$slug = $row[2];
					if(empty($slug) || $slug==''){
						$slug = $row[1];
					}
					//add merchant id to slug
					$mEx = explode('mi'.$merchant_id, $slug);
					if (!end($mEx) == '') {
						$slug = $slug.'-mi'.$merchant_id;
					}
					$slug	= url_title(convert_accented_characters(substr($slug,0,80)), 'dash', TRUE);
					$slug	= $this->Routes_model->validate_slug($slug);
					
					$savePM['slug'] = $slug;

					$route = array();
					$route['slug']	= $slug;	
					$route_id	= $this->Routes_model->save($route);
					$savePM['route_id'] = $route_id;
					
					$save['description'] = $row[3];
					$save['specification'] = $row[4];
					$save['excerpt'] = $row[5];

					$save['taxable'] = 1;
					$save['tax_rate'] = $row[6];
					$save['commission'] = $row[7];

					$fixed_quantity = $row[8];
					if(empty($fixed_quantity) || $fixed_quantity==''){
						$fixed_quantity = 0;
					}
					$save['fixed_quantity'] = $fixed_quantity;
					$save['weight'] = $row[9];

					$relatedText = '';
					if($row[10] != ''){
						$relatedText = json_encode(explode('|', $row[10]));
					}
					$save['related_products'] = $relatedText;

					$imgText = '';
					if($row[11] != ''){
						$images = explode('|', $row[11]);
						$transformImgArray = array();
						foreach ($images as $k=>$image) {
							if($k==0){
								$transformImgArray[substr($image, 0, strrpos($image, '.'))] = array(
									'filename'=> $image,
									'alt'=> $row[1],
									'caption'=> '',
									'primary'=> true
								);
							} else {
								$transformImgArray[substr($image, 0, strrpos($image, '.'))] = array(
									'filename'=> $image,
									'alt'=> $row[1],
									'caption'=> '',
								);
							}
						}
						$imgText = json_encode($transformImgArray);
					}
					$save['images'] = $imgText;

					$save['brand'] = $row[12];
					$save['hns'] = $row[13];

					$savePM['price'] = $row[14];
					$savePM['saleprice'] = $row[15];

					$live_on = $row[16];
					if(empty($live_on) || $live_on==''){
						$live_on = date('Y-m-d G:i:s');
					}
					$savePM['live_on'] = $live_on;

					$shipping_charges = $row[17];
					if(empty($shipping_charges) || $shipping_charges==''){
						$shipping_charges = 0;
					}
					$savePM['shipping_charges'] = $shipping_charges;

					$save['shippable'] = 1;
					$save['taxable'] = 1;
					
					$save['track_stock'] = 1;
					$save['seo_title'] = $row[1];
					
					$savePM['quantity'] = $row[18];
					$save['meta'] = $row[19];
					$savePM['enabled'] = $row[20];

					$savePM['payment_mode'] = 1;
					
					$options = false;
					$save['id'] = false;

					if($row['22'] != ''){
						$opts = explode('|', $row['22']);
						if(!empty($opts)){
							foreach($opts as $opt){
								if (strpos($opt, ':') !== false) {
									list($opt_name, $vals) = explode(':',$opt);

									$values = array();
									$valsData = explode(',',$vals);
									if(!empty($valsData)){
									    foreach($valsData as $valData){
									        $values[] = array(
									            'name'=>$valData,
									            'value'=>$valData,
									            'weight'=>'',
									            'price'=>'',
									            'image'=>''
									        );
									    }
									}

									$options[] = array(
										'name'		=>	$opt_name,
										'type'		=>	'radiolist',
										'required'	=>	1,
										'values'	=>	$values
									);
								}
							}
						}
					}

					$id = $this->Product_model->save($save, $options, array(), $savePM);
					
					if($row['21'] != ''){
						$categories = explode('|', $row['21']);
						$categoriesData = array();
						foreach($categories as $c)
						{
							$categoriesData[] = array('product_id'=>$id,'category_id'=>$c);
						}
						$this->db->insert_batch('category_products', $categoriesData);
					}
					/*if($row['17'] != ''){
						$attributes = explode('|', $row['17']);
						if(!empty($attributes)){
							$attributesData = array();
							foreach ($attributes as $attribute) {
								$attrData = explode(':', $attribute);
								$attributesData[] = array('product_id'=>$id,'attribute_id'=>$attrData[0],'attribute_value'=>$attrData[1]);
							}
							$this->db->insert_batch('product_attributes', $attributesData);
						}
					}*/

					$route['id']	= $route_id;
					$route['slug']	= $slug;
					$route['route']	= 'cart/product/'.$id.'/'.$merchant_id;
					$this->Routes_model->save($route);

					$successCount++;
				}

				fclose($file);
				$data['successCount'] = $successCount;
			}
		}
		
		$this->view($this->config->item('admin_folder').'/products_bulk_upload', $data);
	}

	function bulk_image_upload(){
		$data['page_title']	= ' Bulk Product Image Upload';
		$this->view($this->config->item('admin_folder').'/bulk_image_upload', $data);
	}

}
