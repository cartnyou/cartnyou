<!--BREADCRUMBS -->
<section class="breadcrumb parallax margbot30"></section>
<!-- //BREADCRUMBS -->


<!-- PAGE HEADER -->
<section class="page_header">

	<!-- CONTAINER -->
	<div class="container border0 margbot0">
		<h3 class="pull-left"><b>Checkout</b></h3>

		<div class="pull-right">
			<a href="<?php echo site_url();?>" >Back shopping bag<i class="fa fa-angle-right"></i></a>
		</div>
	</div><!-- //CONTAINER -->
</section><!-- //PAGE HEADER -->


<!-- CHECKOUT PAGE -->
<section class="checkout_page">

	<!-- CONTAINER -->
	<div class="container">

		<?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('message');?>
            </div>
        <?php endif;?>
        
        <?php if ($this->session->flashdata('error')):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php endif;?>
        
        <?php if (!empty($error)):?>
            <div class="alert alert-danger">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <?php echo $error;?>
            </div>
        <?php endif;?>

        <?php if (validation_errors()):?>
        	<div class="alert alert-danger">
        		<a class="close" data-dismiss="alert">×</a>
        		<?php echo validation_errors();?>
        	</div>
        <?php endif;?>

		<!-- CHECKOUT BLOCK -->
		<div class="checkout_block">
			<ul class="checkout_nav">
				<li class="done_step2">1. Shipping Address</li>
				<li class="done_step">2. Delivery</li>
				<li class="active_step">3. Payment</li>
				<li class="last">4. Confirm Order</li>
			</ul>

			<?php include('order_details.php');?>
			<hr>
			
			<div class="checkout_payment clearfix">
				<div class="payment_method padbot70">
					<h2><?php echo lang('payment_method');?></h2>

					<!-- <ul class="clearfix">
						<li>
							<input id="ridio1" type="radio" name="radio" hidden />
							<label for="ridio1">Visa<br><img src="images/visa.jpg" alt="" /></label>
						</li>
						<li>
							<input id="ridio2" type="radio" name="radio" hidden />
							<label for="ridio2">Master Card<br><img src="images/master_card.jpg" alt="" /></label>
						</li>
						<li>
							<input id="ridio3" type="radio" name="radio" hidden />
							<label for="ridio3">PayPal<br><img src="images/paypal.jpg" alt="" /></label>
						</li>
						<li>
							<input id="ridio4" type="radio" name="radio" hidden />
							<label for="ridio4">Discover<br><img src="images/discover.jpg" alt="" /></label>
						</li>
						<li>
							<input id="ridio5" type="radio" name="radio" hidden />
							<label for="ridio5">Skrill<br><img src="images/skrill.jpg" alt="" /></label>
						</li>
					</ul> -->
					<div class="tabbable tabs-left">
						<ul class="nav nav-tabs">
						<?php
						if(empty($payment_method))
						{
							$selected	= key($payment_methods);
						}
						else
						{
							$selected	= $payment_method['module'];
						}
						foreach($payment_methods as $method=>$info):?>
							<li <?php echo ($selected == $method)?'class="active"':'';?>><a href="#payment-<?php echo $method;?>" data-toggle="tab"><?php echo $info['name'];?></a></li>
						<?php endforeach;?>
						</ul>
						<div class="tab-content">
							<?php foreach ($payment_methods as $method=>$info):?>
								<div id="payment-<?php echo $method;?>" class="tab-pane<?php echo ($selected == $method)?' active':'';?>">
									<?php echo form_open('checkout/step_3', 'id="form-'.$method.'"');?>
										<input type="hidden" name="module" value="<?php echo $method;?>" />
										<?php echo $info['form'];?>
										<input class="btn btn-block btn-large btn-primary" type="submit" value="<?php echo lang('form_continue');?>"/>
									</form>
								</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>

				<!-- <div class="credit_card_number padbot80">
					<p class="checkout_title">Credit Card Number</p>

					<form class="credit_card_number_form clearfix" action="javascript:void(0);" method="get">
						<input type="text" name="card number" value="Number" onFocus="if (this.value == 'Number') this.value = '';" onBlur="if (this.value == '') this.value = 'Number';" />
						<div class="margrightminus20">
							<select class="basic">
								<option value="">MM</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
							</select>
							<select class="basic last">
								<option value="">YYYY</option>
								<option>2010</option>
								<option>2011</option>
								<option>2012</option>
								<option>2013</option>
								<option>2014</option>
							</select>
						</div>
					</form>
				</div> -->
			</div>
		</div><!-- //CHECKOUT BLOCK -->
	</div><!-- //CONTAINER -->
</section><!-- //CHECKOUT PAGE