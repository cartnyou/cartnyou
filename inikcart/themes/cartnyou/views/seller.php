<link rel="stylesheet" href="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.css">
<style type="text/css">
    .review .emojioneemoji{
        max-width: 20px;
    }
</style>

<?php
function val_to_progress_class($val){
    $val = ($val*100)/5;
    $class = "";
    if($val >= 75){
        $class = 'progress-bar-success';
    } else if($val >= 50){
        $class = 'progress-bar-info';
    } else if($val >= 25){
        $class = 'progress-bar-warning';
    } else if($val >= 0){
        $class = 'progress-bar-danger';
    } 
    return $class;
}
?>
<div class="container pdt-129-header">
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('message')):?>
                <div class="alert alert-info  mt-20">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">Ã—</button>
                    <?php echo $this->session->flashdata('message');?>
                </div>
            <?php endif;?>
            
            <?php if ($this->session->flashdata('error')):?>
                <div class="alert alert-danger mt-20">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">Ã—</button>
                    <?php echo $this->session->flashdata('error');?>
                </div>
            <?php endif;?>
            
            <?php if (!empty($error)):?>
                <div class="alert alert-danger mt-20">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">Ã—</button>
                    <?php echo $error;?>
                </div>
            <?php endif;?>
        </div>
    </div>
    
    <div class="row mtb-20 ">

        <div class="col-md-3 col-sm-4 merchantProfile">
            <?php if($seller->logo != ''){ ?>
                <img src="<?php echo base_url('uploads/images/small/'.$seller->logo);?>" alt="">
            <?php } ?>
            <h3 class="mtb-5"><?php echo $seller->store_name;?></h3>
            <h5 class="mtb-5"><?php echo $seller->city;?></h5>

            <?php if($avg_rating->count > 0){ ?>
                <span class="badge rate-badge badge-success"><?php echo round($avg_rating->rating_seller,1); ?> <i class="fa fa-star"></i></span>

                <p class="mtb-5">Rated By (<?php echo $avg_rating->count;?> customer<?php echo ($avg_rating->count > 1)?'s':'';?>)</p>
            <?php } ?>

            <a class="btn btn-red" href="<?php echo site_url('seller/shop/'.$seller->slug);?>">View Storefront</a>
           <!-- <p class="mt-12"><a href="#">About</a></p>-->
            <!-- <p class="mt-12"> <a href="#reviews">Reviews</a></p> -->
        </div>
        <div class="col-md-9 col-sm-8">

            <div class="about">
                <?php if($seller->description != ''){ ?>
                    <h2 class="group-title ">
                        <span>About</span>
                    </h2>
                    <p class="mt-27">
                        <?php echo $seller->description;?>
                    </p>
                <?php } ?>

                <?php if($avg_rating->count > 0){ ?>
                    <div class="rateBox mt-27">
                        <div class="row mtb-5">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="product-star ratebox" data-id="seller_rating" data-rating="<?php echo $split_rating->rating_item_avg;?>"></div>
                                <!-- <?php echo number_format($split_rating->rating_item_avg,1);?>* -->
                            </div>
                            <div class="col-md-3 col-sm-3 hidden-xs">
                                <div class="progress">
                                    <div class="progress-bar <?php echo val_to_progress_class($split_rating->rating_item_avg);?>" role="progressbar" aria-valuenow="<?php echo (($split_rating->rating_item_avg*100)/5);?>"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (($split_rating->rating_item_avg*100)/5);?>%">
                                        <?php echo number_format((($split_rating->rating_item_avg*100)/5),2);?>%
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-6 m-fs-10 pdl-0">
                                <span class="mrl-20"><b>Item as described</b></span>
                            </div>
                        </div>
                        <div class="row mtb-5">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="product-star ratebox" data-id="seller_rating" data-rating="<?php echo $split_rating->rating_communication_avg;?>"></div>
                                <!-- <?php echo number_format($split_rating->rating_communication_avg,1);?>* -->
                            </div>
                           <div class="col-md-3 col-sm-3 hidden-xs">
                                <div class="progress">
                                    <div class="progress-bar <?php echo val_to_progress_class($split_rating->rating_communication_avg);?>" role="progressbar" aria-valuenow="<?php echo (($split_rating->rating_communication_avg*100)/5);?>"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (($split_rating->rating_communication_avg*100)/5);?>%">
                                        <?php echo number_format((($split_rating->rating_communication_avg*100)/5),2);?>%
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-6 m-fs-10 pdl-0">
                                <span class="mrl-20"><b>Communication</b></span>
                            </div>
                        </div>
                        <div class="row mtb-5">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="product-star ratebox" data-id="seller_rating" data-rating="<?php echo $split_rating->rating_time_avg;?>"></div>
                            </div>
                            <div class="col-md-3 col-sm-3 hidden-xs">
                                <div class="progress">
                                    <div class="progress-bar <?php echo val_to_progress_class($split_rating->rating_time_avg);?>" role="progressbar" aria-valuenow="<?php echo (($split_rating->rating_time_avg*100)/5);?>"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (($split_rating->rating_time_avg*100)/5);?>%">
                                        <?php echo number_format((($split_rating->rating_time_avg*100)/5),2);?>%
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-6 m-fs-10 pdl-0">
                                <span class="mrl-20"><b>Shipping time</b></span>
                            </div>
                        </div>
                        <div class="row mtb-5">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="product-star ratebox" data-id="seller_rating" data-rating="<?php echo $split_rating->rating_charges_avg;?>"></div>
                            </div>
                            <div class="col-md-3 col-sm-3 hidden-xs">
                                <div class="progress">
                                    <div class="progress-bar <?php echo val_to_progress_class($split_rating->rating_charges_avg);?>" role="progressbar" aria-valuenow="<?php echo (($split_rating->rating_charges_avg*100)/5);?>"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (($split_rating->rating_charges_avg*100)/5);?>%">
                                        <?php echo number_format((($split_rating->rating_charges_avg*100)/5),2);?>%
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-6 m-fs-10 pdl-0">
                                <span class="mrl-20"><b>Shipping charges</b></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <!--<div class="product-star dinline">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>

                    <i class="fa fa-star-half-o"></i>
                    <div class="progress width-40">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70"
                             aria-valuemin="0" aria-valuemax="100" style="width:70%">
                            70%
                        </div>
                    </div>
                </div>-->

                </p>
            </div>
        </div>

    </div>
</div>

<div class="container">
    <div class="product-tab">
        <ul class="nav-tab">
            <li class="active">
                <a data-toggle="tab" href="#reviews">reviews</a>
            </li>

        </ul>
        <div class="tab-container">
            <div id="reviews" class="tab-panel active">
                <div class="product-comments-block-tab">
                    <?php
                    $review_count = count($reviews);
                    $review_shown_count = 1;
                    if(!empty($reviews)){
                        foreach ($reviews as $review) {
                    ?>
                    <div class="comment row review <?php echo ($review_count > 3 && $review_shown_count > 3) ?'hidden':'';?>">
                        <div class="col-sm-3 author">
                            <div class="grade">

                                <span class="reviewRating">
                                    <span class="badge rate-badge badge-success"><?php echo round($avg_rating->rating_seller,1); ?> <i class="fa fa-star"></i></span>
                                </span>
                                <span><strong><?php echo ucwords($review->firstname.' '.$review->lastname);?></strong></span>
                            </div>
                            <div class="info-author">
                                <em><?php echo nl2br($review->created_at);?></em>
                            </div>
                        </div>
                        <div class="col-sm-9 commnet-dettail">
                            <?php echo $review->review;?>
                        </div>
                    </div>
                    <?php
                            $review_shown_count++;
                        }
                    }
                    ?>
                    
                    <?php
                    if($review_count > 3){
                        echo '<h2 style="margin-bottom:30px;margin-top:15px;"><a href="javascript:;" id="expand-reviews" class="red">View All Reviews >></a></h2>';
                    }
                    ?>
                    <div class="br"></div>
                    <h3 id="write-review">WRITE A REVIEW</h3>

                    <?php if(!$this->Customer_model->is_logged_in(false, false)):?>
                        You need to login to review <?php echo $seller->store_name;?>.<br>
                        <a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-red mt-12">Login Now</a>
                    <?php else: ?>
                        <div class="clearfix">
                            <?php echo form_open('seller/add_review', 'class="form-horizontal"');?>
                                <input type="hidden" name="id" value="<?php echo $seller->id?>" />
                                
                                <textarea id="review-textarea" name="review" rows="4" class="width-100 customTextarea"></textarea>
                                
                                <!--<input id="rating-input-item" name="rating_item" class="hidden form-control">
                                <div class="mb-20-xs"><span class="rsl2">Item as described: </span><span class="ratebox-input mt5" data-id="rating-input-item" data-rating="0"></span></div>

                                <input id="rating-input-communication" name="rating_communication" class="hidden form-control">
                                <div class="mb-20-xs"><span class="rsl2">Communication: </span><span class="ratebox-input mt5" data-id="rating-input-communication" data-rating="0"></span></div>

                                <input id="rating-input-time" name="rating_time" class="hidden form-control">
                                <div class="mb-20-xs"><span class="rsl2">Shipping Time: </span><span class="ratebox-input mt5" data-id="rating-input-time" data-rating="0"></span></div>

                                <input id="rating-input-charges" name="rating_charges" class="hidden form-control">
                                <div class="mb-20-xs"><span class="rsl2">Shipping Charges: </span><span class="ratebox-input mt5" data-id="rating-input-charges" data-rating="0"></span></div>-->
                            <input id="rating-input-item"  type="range" min="0" max="5" value="0" step="0.5" name="rating_item" class="hidden form-control">
                            <div class="mb-20-xs"><span class="rsl2">Item as described: </span><span class="rateit mt5" data-rateit-backingfld="#rating-input-item" data-rating="0"></span></div>

                            <input id="rating-input-communication"  type="range" min="0" max="5" value="0" step="0.5" name="rating_communication" class="hidden form-control">
                            <div class="mb-20-xs"><span class="rsl2">Communication: </span><span class="rateit mt5" data-rateit-backingfld="#rating-input-communication" data-rating="0"></span></div>

                            <input id="rating-input-time"  type="range" min="0" max="5" value="0" step="0.5" name="rating_time" class="hidden form-control">
                            <div class="mb-20-xs"><span class="rsl2">Shipping Time: </span><span class="rateit mt5" data-rateit-backingfld="#rating-input-time" data-rating="0"></span></div>

                            <input id="rating-input-charges"  type="range" min="0" max="5" value="0" step="0.5" name="rating_charges" class="hidden form-control">
                            <div class="mb-20-xs"><span class="rsl2">Shipping Charges: </span><span class="rateit mt5" data-rateit-backingfld="#rating-input-charges" data-rating="0"></span></div>



                                <input type="submit" class="btn btn-red mt-12" value="Submit Review">
                            </form>

                        </div>
                    <?php endif; ?>

                </div>

            </div>



        </div>
    </div>
</div>

<?php echo theme_css('raterater.css', true);?>
<?php echo theme_js('raterater.jquery.js', true);?>
<script type="text/javascript" src="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.1/rateit.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.rateit/1.1.1/jquery.rateit.min.js"></script>
<style>
    .rateit .rateit-selected {
        background-position: left -32px!important;
    }
</style>
<script type="text/javascript">
function ratingChanged(id, rating){
    $('#'+id).val(rating);
}

$(function(){
    $('.ratebox').raterater( { 
        submitFunction: 'rateAlert', 
        allowChange: false,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
        isStatic: true,
    });
    $('.rateit').rateit();
    $('.ratebox-input').raterater( { 
        submitFunction: 'ratingChanged', 
        allowChange: true,
        starWidth: 16,
        spaceWidth: 1,
        numStars: 5,
    });

    $('#expand-reviews').on('click',function(){
        $('.review').removeClass('hidden');
        $('#expand-reviews').addClass('hidden');
    });

    $("#review-textarea").emojioneArea({
        pickerPosition: "bottom",
        tonesStyle: "bullet",
        saveEmojisAs:'image'
    });
});
</script>

<?php
if(isset($_GET['review'])){
    if($_GET['review'] == 1 || $_GET['review'] == 2 || $_GET['review'] == 3 || $_GET['review'] == 4 || $_GET['review'] == 5){
        ?>
        <script type="text/javascript">
            $(function(){
                $reviewRate = "<?php echo $_GET['review'];?>";

                $('html, body').stop().animate({
                    'scrollTop': $('#write-review').offset().top-120
                }, 500);

                $('#rating-input-item').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-item"').attr('data-rating', $reviewRate);

                $('#rating-input-communication').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-communication"').attr('data-rating', $reviewRate);

                $('#rating-input-time').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-time"').attr('data-rating', $reviewRate);

                $('#rating-input-charges').val($reviewRate);
                $('.ratebox-input[data-id="rating-input-charges"').attr('data-rating', $reviewRate);

                $('.ratebox-input').raterater( {
                    submitFunction: 'ratingChanged',
                    allowChange: true,
                    starWidth: 16,
                    spaceWidth: 1,
                    numStars: 5,
                });

            });
        </script>
        <?php
    }
}
?>