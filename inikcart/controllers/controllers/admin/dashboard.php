<?php

class Dashboard extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		if($this->auth->check_access('Orders'))
		{
			redirect($this->config->item('admin_folder').'/orders');
		}
		
		$this->load->model('Order_model');
		$this->load->model('Customer_model');
		$this->load->helper('date');
		
		$this->lang->load('dashboard');
	}
	
	function index()
	{
		//check to see if shipping and payment modules are installed
		$data['payment_module_installed']	= (bool)count($this->Settings_model->get_settings('payment_modules'));
		$data['shipping_module_installed']	= (bool)count($this->Settings_model->get_settings('shipping_modules'));
		
		$data['page_title']	=  lang('dashboard');
		
		// get 5 latest orders
		$data['orders']	= $this->Order_model->get_orders(false, '' , 'DESC', 5);

		// get 5 latest customers
		$data['customers'] = $this->Customer_model->get_customers(5);
				
		
		$this->view($this->config->item('admin_folder').'/dashboard', $data);
	}

	function export_search_queries(){
		$this->load->model('Product_model');
		$data = $this->Product_model->get_searches();
		
		$list = '';
		foreach($data as $row){
			if(is_string($row->term) && is_array(json_decode($row->term, true)) && (json_last_error() == JSON_ERROR_NONE)){
				$a = json_decode($row->term, true);
				if(isset($a['term'])){
					if($a['term'] != ''){
						$list .= $a['term'].",";
						$list .= $row->count."\n";
					}
				}
			} else {
				if($row->term != ''){
					$list .= $row->term.",";
					$list .= $row->count."\n";
				}
			}
		}
		
		$data['list']	= $list;
		
		$this->load->view($this->config->item('admin_folder').'/search_list', $data);
	}

}