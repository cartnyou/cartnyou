<style type="text/css">
	.pagination {
		margin:0px;
	}
</style>

<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10 col-md-offset-4 col-sm-offset-3">
	    <div class="dashboard-stat blue">
	        <div class="visual">
	            <i class="fa fa-briefcase fa-icon-medium"></i>
	        </div>
	        <div class="details">
	            <div class="number"> <i class="fa fa-inr"></i> <?php echo $remaining_amount;?> </div>
	            <div class="desc"> Remaining Amount </div>
	        </div>
	        <a class="more recharge" href="javascript:;"> Recharge Now
	            <i class="m-icon-swapright m-icon-white"></i>
	        </a>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<h4>Transactions</h4>
	</div>
	<div class="col-sm-8">
		<?php echo $this->pagination->create_links();?>
	</div>
</div>

<?php if(count($transactions) >0): ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Type</th>
				<?php if($active_sub_menu == 'transactions' || $active_sub_menu == 'courier_logs'){ ?>
				<th>Order ID</th>
				<th>Weight</th>
				<?php } ?>
				<th>Amount</th>
				<th>Transaction ID</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($transactions as $transaction): ?>
			<tr>
				<td><?php echo $transaction->id;?></td>
				<td><?php echo ($transaction->amount > 0) ? 'Recharge' : 'Deduction' ;?></td>
				<?php if($active_sub_menu == 'transactions' || $active_sub_menu == 'courier_logs'){ ?>
				<td><?php echo ($transaction->amount > 0) ? '-' : $transaction->order_id ;?></td>
				<td><?php echo ($transaction->amount > 0) ? '-' : $transaction->weight ;?></td>
				<?php } ?>
				<td><?php echo $transaction->amount;?></td>
				<td><?php echo $transaction->txn_id;?></td>
				<td><?php echo $transaction->created_at;?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>