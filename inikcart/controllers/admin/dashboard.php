<?php

class Dashboard extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		if($this->auth->check_access('Orders'))
		{
			redirect($this->config->item('admin_folder').'/orders');
		}
		
		$this->load->model('Order_model');
		$this->load->model('Customer_model');
		$this->load->helper('date');
		$this->load->helper('form');
		$this->lang->load('dashboard');
	}
	
	function index()
	{
		//check to see if shipping and payment modules are installed
		$data['payment_module_installed']	= (bool)count($this->Settings_model->get_settings('payment_modules'));
		$data['shipping_module_installed']	= (bool)count($this->Settings_model->get_settings('shipping_modules'));
		
		$data['page_title']	=  lang('dashboard');
		
		// get 5 latest orders
		$data['orders']	= $this->Order_model->get_orders(false, 'ordered_on' , 'DESC', 5);

		// get 5 latest customers
		$data['customers'] = $this->Customer_model->get_customers(5);
		
		// get pending orders count for last 48 hours
		$data['pending_last']=$this->Order_model->get_orders_filter((object)array('search_status'=>'Order Placed','search_last'=>date('Y-m-d',strtotime('-48 hours'))));
		
		// get pending orders count for before 48 hours
		$data['pending_before']=$this->Order_model->get_orders_filter((object)array('search_status'=>'Order Placed','search_before'=>date('Y-m-d',strtotime('-48 hours'))));
		
		// get total sales
		if($this->input->post('submit') == 'filter'){
			$data['sales']=$this->Order_model->get_orders_sales((object)array(
																				'search_status'=>$this->input->post('filter_status'),
																				'date_from'=>$this->input->post('from_date')!=''?date('Y-m-d H:i:s',strtotime($this->input->post('from_date'))):null,
																				'date_to'=>$this->input->post('to_date')!=''?date('Y-m-d H:i:s',strtotime($this->input->post('to_date'))):null
																			));
			$data['filter_status']=$this->input->post('filter_status');
			$data['date_from']=$this->input->post('from_date');
			$data['date_to']=$this->input->post('to_date');
		}else{
			$data['sales']=$this->Order_model->get_orders_sales((object)array('search_status'=>'Order Placed'));
			$data['filter_status']='Order Placed';
			$data['date_from']='';
			$data['date_to']='';
		}
		
		//{"Order Placed":"Order Placed","Pending":"Pending","Processing":"Processing","Shipped":"Shipped","On Hold":"On Hold","Cancelled":"Cancelled","Delivered":"Delivered"}
		
		//donut graph no of orders wise
		$orders=array();$orders_percent=array();$count=0;$totalOrders=0;
		foreach($this->config->item('order_statuses') as $key=>$value){
			$orders[$count]['label']=$value;
			$orders[$count]['value']=$this->Order_model->get_orders_filter((object)array('search_status'=>$value));
			$totalOrders=$totalOrders+$orders[$count]['value'];
			$count++;
		}
		$data['orders_status']=$orders;
		
		//donut graph no of orders percentage wise
		foreach($orders as $oKey=>$oValue){
			$orders_percent[$oKey]['label']=$oValue['label'];
			$orders_percent[$oKey]['value']=($oValue['value']/$totalOrders)*100;
		}
		$data['orders_percent']=$orders_percent;
		
		$this->view($this->config->item('admin_folder').'/dashboard', $data);
	}

	function export_search_queries(){
		$this->load->model('Product_model');
		$data = $this->Product_model->get_searches();
		
		$list = '';
		foreach($data as $row){
			if(is_string($row->term) && is_array(json_decode($row->term, true)) && (json_last_error() == JSON_ERROR_NONE)){
				$a = json_decode($row->term, true);
				if(isset($a['term'])){
					if($a['term'] != ''){
						$list .= $a['term'].",";
						$list .= $row->count."\n";
					}
				}
			} else {
				if($row->term != ''){
					$list .= $row->term.",";
					$list .= $row->count."\n";
				}
			}
		}
		
		$data['list']	= $list;
		
		$this->load->view($this->config->item('admin_folder').'/search_list', $data);
	}

}