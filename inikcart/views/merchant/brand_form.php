<?php echo form_open_multipart($this->config->item('merchant_folder').'/brands/form/'.$id, array('class'=>'form-horizontal form-row-seperated') ); ?>
<div class="row">
	<div class="col-md-12">
	
		<div class="portlet">
			<div class="portlet-title">
	            <div class="caption">
	                <i class="fa fa-shopping-cart"></i><?php echo ($name)?$name:'Add New Brand';?>
	            </div>
	            <div class="actions btn-set">
	                <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> <?php echo lang('save');?></button>
	            </div>
	        </div>

			<div class="form-body">
				<div class="form-group">
	                <label class="col-md-2 control-label">Name:
	                    <span class="required"> * </span>
	                </label>
	                <div class="col-md-10">
	                    <?php
						$data	= array('placeholder'=>'Name', 'name'=>'name', 'value'=>set_value('name', $name), 'class'=>'form-control');
						echo form_input($data);
						?>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label">Description:
	                </label>
	                <div class="col-md-10">
	                    <?php
						$data	= array('name'=>'description', 'class'=>'form-control', 'value'=>set_value('description', $description));
						echo form_textarea($data);
						?>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label">Slug:
	                </label>
	                <div class="col-md-10">
	                    <?php
						$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'form-control');
						echo form_input($data);
						?>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label">SEO Title:
	                </label>
	                <div class="col-md-10">
	                    <?php
						$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'form-control');
						echo form_input($data);
						?>
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-md-2 control-label">Meta Description:
	                </label>
	                <div class="col-md-10">
	                    <?php
						$data	= array('rows'=>3, 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'form-control');
						echo form_textarea($data);
						?>
	                </div>
	            </div>

	            <div class="form-group">
	                <label class="col-md-2 control-label">Image:
	                </label>
	                <div class="col-md-10">
	                    <div class="input-append">
							<?php echo form_upload(array('name'=>'image'));?>
						</div>
						<?php if($id && $image != ''):?>
				
						<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image);?>" alt="current"/><br/><?php echo lang('current_file');?></div>
						
						<?php endif;?>
	                </div>
	            </div>

	            <?php echo form_dropdown('enabled', array('0' => lang('disabled'), '1' => lang('enabled')), set_value('enabled',$enabled), 'class="hidden"'); ?>


	        </div>
	    </div>
	</div>
</div>
</form>

<script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>