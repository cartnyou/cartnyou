<?php

class Shop extends Front_Controller {

	function index()
	{
		$data['gift_cards_enabled'] = $this->gift_cards_enabled;
		$data['homepage']			= true;
		$data['popular_products'] = $this->Product_model->popular_products(10);
		
		$this->view('homepage', $data);
	}

	
}