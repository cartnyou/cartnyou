<?php

$f_id		= array('id'=>'f_id', 'style'=>'display:none;', 'name'=>'id', 'value'=> set_value('id',$id));
$f_address1	= array('id'=>'f_address1', 'class'=>'col-md-12 form-control', 'name'=>'address1', 'value'=>set_value('address1',$address1), 'placeholder'=>'Flat House No., Building, Company, Apartment');
$f_address2	= array('id'=>'f_address2', 'class'=>'col-md-12 form-control mt5', 'name'=>'address2', 'value'=> set_value('address2',$address2), 'placeholder'=>'Area, Colony, Street, Sector, Village');
$f_first	= array('id'=>'f_firstname', 'class'=>'col-md-12 form-control', 'name'=>'firstname', 'value'=> set_value('firstname',$firstname),'placeholder'=>lang('address_firstname'));
$f_last		= array('id'=>'f_lastname', 'class'=>'col-md-12 form-control', 'name'=>'lastname', 'value'=> set_value('lastname',$lastname),'placeholder'=>lang('address_lastname'));
$f_email	= array('id'=>'f_email', 'class'=>'col-md-12 form-control', 'name'=>'email', 'value'=>set_value('email',$email),'placeholder'=>lang('address_email'));
$f_phone	= array('id'=>'f_phone', 'maxlength'=>'10', 'class'=>'col-md-12 form-control', 'name'=>'phone', 'value'=> set_value('phone',$phone),'placeholder'=>lang('address_phone_placeholder'));
$f_city		= array('id'=>'f_city', 'class'=>'col-md-12 form-control', 'name'=>'city', 'value'=>set_value('city',$city),'placeholder'=>lang('address_city'));
$f_zip		= array('id'=>'f_zip', 'maxlength'=>'6', 'class'=>'col-md-12 form-control', 'name'=>'zip', 'value'=> set_value('zip',$zip),'placeholder'=>lang('address_zip_placeholder'));
$f_landmark	= array('id'=>'f_landmark', 'class'=>'col-md-12 form-control', 'name'=>'landmark', 'value'=>set_value('landmark',$landmark), 'placeholder'=>'Landmark: e.g.: Near JLN Stadium');
echo form_input($f_id);

?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Address</h3>
	</div>
	<div class="modal-body">
		<div class="alert alert-danger" style="display: none;" id="form-error">
			<a class="close" data-dismiss="alert">×</a>
		</div>
		<div class="row">
			<div class="col-md-12">
				<label><?php echo lang('address_firstname');?></label>
				<?php echo form_input($f_first);?>
			</div>
			<div class="col-md-6 hidden">
				<label><?php echo lang('address_lastname');?></label>
				<?php echo form_input($f_last);?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label><?php echo lang('address_email');?></label>
				<?php echo form_input($f_email);?>
			</div>
			<div class="col-md-6">
				<label>Mobile Number</label>
				<?php echo form_input($f_phone);?>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12">
				<label><?php echo lang('address_type');?></label>
				<!--<div class="col-md-6">
					<input type="radio" value="Home" > Home ( Delivery between 7 AM to 10 PM )
				</div>
				<div class="col-md-6">
					<input type="radio" value="Work" > Work ( Delivery between 9 AM to 5 PM )
				</div>-->
				<?php echo form_dropdown('address_type', array('Home'=>'Home ( Delivery between 7 AM to 10 PM )','Work'=>'Work ( Delivery between 9 AM to 5 PM )'), set_value('address_type', $address_type), 'id="f_address_type" class="col-md-12 basic form-control nice-select"');?>
			</div>
			
        </div>
		<div class="row">
			<div class="col-md-12">
				<label><?php echo lang('address');?></label>
				<?php
				echo form_input($f_address1);
				echo form_input($f_address2);
				?>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <label>Landmark</label>
                <?php
				echo form_input($f_landmark);
				?>
            </div>
        </div>
		<div class="row hidden">
			<div class="col-md-12">
				<label><?php echo lang('address_country');?></label>
				<?php echo form_dropdown('country_id', $countries_menu, set_value('country_id', $country_id), 'id="f_country_id" class="col-md-12 basic form-control nice-select"');?>
			</div>
		</div>
		<div class="row">
            <div class="col-md-4">
                <label><?php echo lang('address_zip');?></label>
                <?php echo form_input($f_zip);?>
            </div>
			<div class="col-md-4">
				<label><?php echo lang('address_city');?></label>
				<?php echo form_input($f_city);?>
			</div>
            <div class="col-md-4">
                <label><?php echo lang('address_state');?></label>
                <?php echo form_dropdown('zone_id', $zones_menu, set_value('zone_id', $zone_id), 'id="f_zone_id" class="col-md-12 basic form-control nice-select"');?>
            </div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal"><?php echo lang('close');?></a>
		<a href="#" class="btn btn-green" type="button" onclick="save_address(); return false;">Save</a>
	</div>


<script>
$(function(){
	$('#f_country_id').change(function(){
		$.post('<?php echo site_url('locations/get_zone_menu');?>',{id:$('#f_country_id').val()}, function(data) {
		  $('#f_zone_id').html(data);
		});
	});

	$("#f_zip").on('keyup keypress', function() {
	    if($(this).val().length == 6) {
	        $.post('<?php echo site_url('locations/getZip');?>',{zip:$('#f_zip').val()}, function(data) {
	        	data = JSON.parse(data);
	        	if(data.success){
				  $('#f_city').val(data.data.city);
				  $('#f_zone_id').val(data.data.zone_id);
				}
			});
	    }
	});
	
	$("#f_phone").on('keyup keypress', function() {
		$('.error_validate').remove();
		var phone=$(this).val();
		if(Math.floor(phone) == phone && $.isNumeric(phone)){ 
			if(phone.length != 10) {
				$('#f_phone').after('<div class="error_validate" style="color:#FF0000">The length of the mobile no. should be 10</div>');
			}
		}else{
			$('#f_phone').after('<div class="error_validate" style="color:#FF0000">'+(phone.length==0?"This Field is required":"Invalid Number")+'</div>');
		}
	});

});

function save_address()
{
	$.post("<?php echo site_url('secure/address_form');?>/"+$('#f_id').val(), {	company: $('#f_company').val(),
																				firstname: $('#f_firstname').val(),
																				lastname: $('#f_lastname').val(),
																				email: $('#f_email').val(),
																				phone: $('#f_phone').val(),
																				address1: $('#f_address1').val(),
																				address2: $('#f_address2').val(),
																				landmark: $('#f_landmark').val(),
																				city: $('#f_city').val(),
																				country_id: $('#f_country_id').val(),
																				zone_id: $('#f_zone_id').val(),
																				zip: $('#f_zip').val(),
																				address_type: $('#f_address_type').val()
																				},
		function(data){
			if(data == 1)
			{
				//window.location = "<?php echo site_url('secure/my_account');?>";
				location.reload();
			}
			else
			{
				$('#form-error').html(data).show();
			}
		});
}
</script>