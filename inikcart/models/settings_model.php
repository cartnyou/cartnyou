<?php
Class Settings_model extends CI_Model
{
	function __construct()
	{
			parent::__construct();
	}
	
	
	
	function get_settings($code)
	{
		$this->db->where('code', $code);
		$result	= $this->db->get('settings');
		
		$return	= array();
		foreach($result->result() as $results)
		{
			$return[$results->setting_key]	= $results->setting;
		}
		return $return;	
	}

	function get_setting_by_code_key($code, $key)
	{
		$this->db->where('code', $code);
		$this->db->where('setting_key', $key);
		return $this->db->get('settings')->row()->setting;
	}
	
	/*
	settings should be an array
	array('setting_key'=>'setting')
	$code is the item that is calling it
	ex. any shipping settings have the code "shipping"
	*/
	function save_settings($code, $values)
	{
	
		//get the settings first, this way, we can know if we need to update or insert settings
		//we're going to create an array of keys for the requested code
		$settings	= $this->get_settings($code);
	
		
		//loop through the settings and add each one as a new row
		foreach($values as $key=>$value)
		{
			//if the key currently exists, update the setting
			if(array_key_exists($key, $settings))
			{
				$update	= array('setting'=>$value);
				$this->db->where('code', $code);
				$this->db->where('setting_key',$key);
				$this->db->update('settings', $update);
			}
			//if the key does not exist, add it
			else
			{
				$insert	= array('code'=>$code, 'setting_key'=>$key, 'setting'=>$value);
				$this->db->insert('settings', $insert);
			}
			
		}
		
	}
	
	//delete any settings having to do with this particular code
	function delete_settings($code)
	{
		$this->db->where('code', $code);
		$this->db->delete('settings');
	}
	
	//this deletes a specific setting
	function delete_setting($code, $setting_key)
	{
		$this->db->where('code', $code);
		$this->db->where('setting_key', $setting_key);
		$this->db->delete('settings');
	}

	public function get_payment_methods($prepaid=false)
	{
		$payment_methods	= array();

		foreach ($this->Settings_model->get_settings('payment_modules') as $payment_method=>$order)
		{
			$pmtype = $this->get_setting_by_code_key($payment_method, 'type');

			if($prepaid){
				if($pmtype == 0){
					continue;
				}
			}
			
			$this->load->add_package_path(APPPATH.'packages/payment/'.$payment_method.'/');
			$this->load->library($payment_method);

			$payment_form = $this->$payment_method->checkout_form();

			if(!empty($payment_form))
			{
				$payment_methods[$payment_method] = $payment_form;
			}
		}

		if(!empty($payment_methods))
		{
			return $payment_methods;
		}
		else
		{
			return false;
		}
	}
}