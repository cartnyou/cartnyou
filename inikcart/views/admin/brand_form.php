<?php echo form_open_multipart($this->config->item('admin_folder').'/brands/form/'.$id); ?>

<div class="tabbable">

	<ul class="nav nav-tabs">
		<li class="active"><a href="#description_tab" data-toggle="tab">Description</a></li>
		<li><a href="#seo_tab" data-toggle="tab">SEO</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="description_tab">
			
			<fieldset>
				<label for="name">Name</label>
				<?php
				$data	= array('name'=>'name', 'value'=>set_value('name', $name), 'class'=>'span12');
				echo form_input($data);
				?>
				
				<label for="Type">Type</label>
				<?php echo form_dropdown('type', array('Own Brand' => 'Own Brand', 'Branded' => 'Branded','Unbranded'=>'Unbranded'), set_value('type',$type) ); ?>
				
				<label for="description">Description</label>
				<?php
				$data	= array('name'=>'description', 'class'=>'redactor', 'value'=>set_value('description', $description));
				echo form_textarea($data);
				?>

				<label for="enabled"><?php echo lang('enabled');?> </label>
        		<?php echo form_dropdown('enabled', array('1' => lang('enabled'), '0' => lang('disabled')), set_value('enabled',$enabled) ); ?>

        		<label for="image">Image </label>
				<div class="input-append">
					<?php echo form_upload(array('name'=>'image'));?><span class="add-on">Max File Size <?php echo $this->config->item('size_limit')/1024; ?>kb</span>
				</div>
					
				<?php if($id && $image != ''):?>
				
				<div style="text-align:center; padding:5px; border:1px solid #ddd;"><img src="<?php echo base_url('uploads/images/small/'.$image);?>" alt="current"/><br/>Current File</div>
				
				<?php endif;?>


			</fieldset>
		</div>
		
		<div class="tab-pane" id="seo_tab">
			<fieldset>

				<label for="slug">Slug</label>
				<?php
				$data	= array('name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'span12');
				echo form_input($data);
				?>

				<label for="seo_title">SEO Title </label>
				<?php
				$data	= array('name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'span12');
				echo form_input($data);
				?>
				
				<label>Meta</label> 
				<?php
				$data	= array('rows'=>3, 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'span12');
				echo form_textarea($data);
				?>
				<p class="help-block"><?php echo lang('meta_data_description');?></p>
			</fieldset>
		</div>

	</div>

</div>

<div class="form-actions">
	<button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
</div>
</form>

<script type="text/javascript">
$('form').submit(function() {
	$('.btn').attr('disabled', true).addClass('disabled');
});
</script>