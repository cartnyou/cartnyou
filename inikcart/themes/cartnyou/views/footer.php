<footer id="footer">
	<div class="container">
		<!-- introduce-box -->
		<div id="introduce-box" class="row">
			<div class="col-md-3">
				<div id="address-box">
					<!-- <div class="text-center hidden-xs">
                        <a href="#"><img src="<?php echo theme_img('logo.png');?>" alt="" /></a>
                    </div> -->
					<div id="address-list" class="mb-15">
						<div class="tit-name">Address:</div>
						<div class="tit-contain">Chandigarh, INDIA.</div>
						<div class="tit-name">Phone:</div>
						<div class="tit-contain">+91-8283004545</div>
						<div class="tit-name">Email:</div>
						<div class="tit-contain">info@cartnyou.com</div>
					</div>
				</div>
			</div>

            <div class="">
                <?php
                $footer_pages_general = array();
                $footer_pages_support = array();
                $footer_pages_other = array();
                if(isset($this->pages[0])) {
                    foreach ($this->pages[0] as $menu_page) {
                        if($menu_page->type == 'general'){
                            $footer_pages_general[] = array('slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        } else if($menu_page->type == 'support'){
                            $footer_pages_support[] = array('slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        } else if($menu_page->type == 'other'){
                            $footer_pages_other[] = array('slug'=>$menu_page->slug, 'menu_title'=>$menu_page->menu_title, 'new_window'=>$menu_page->new_window);
                        }
                        if(isset($this->pages[$menu_page->id])) {
                            foreach($this->pages[$menu_page->id] as $sub_page){
                                if($menu_page->type == 'general'){
                                    $footer_pages_general[] = array('slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                } else if($menu_page->type == 'support'){
                                    $footer_pages_support[] = array('slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                } else if($menu_page->type == 'other'){
                                    $footer_pages_other[] = array('slug'=>$sub_page->slug, 'menu_title'=>$sub_page->menu_title, 'new_window'=>$sub_page->new_window);
                                }
                            }
                        }
                    }
                }
                ?>
            </div>

			<div class="col-md-6">
				<div class="row">
					<div class="col-sm-4 col-xs-6">
						<div class="introduce-title">Quick Links</div>
						<ul id="introduce-company"  class="introduce-list">
							<?php
							if(!empty($footer_pages_general)){
								foreach ($footer_pages_general as $footer_page) { ?>
									<li><a href="<?php echo site_url($footer_page['slug']);?>" <?php if($footer_page['new_window'] ==1){echo 'target="_blank"';}?> ><?php echo $footer_page['menu_title'];?></a></li>
								<?php
								}
							}
							?>
						</ul>
					</div>
					<div class="col-sm-4 col-xs-6">
						<div class="introduce-title">My Account</div>
						<ul id = "introduce-Account" class="introduce-list">
							<?php if(!$this->Customer_model->is_logged_in(false, false)):?>
								<li><a href="#" data-toggle="modal" data-target="#loginModal">My Profile</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal">My Orders</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal">My Addresses</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal">My Wishlist</a></li>
								<li><a href="<?php echo site_url('track-order');?>">Track Order</a></li>
							<?php else: ?>
								<li><a href="<?php echo site_url('secure/my-account');?>">My Profile</a></li>
								<li><a href="<?php echo site_url('secure/orders');?>">My Orders</a></li>
								<li><a href="<?php echo site_url('secure/addresses');?>">My Addresses</a></li>
								<li><a href="<?php echo site_url('cart/wishlist');?>">My Wishlist</a></li>
								<!--<li><a href="<?php //echo site_url('secure/track_order');?>">Track Order</a></li>-->
							<?php endif; ?>

						</ul>
					</div>
					<div class="col-sm-4 col-xs-12">
						<div class="introduce-title">Support</div>
						<ul id = "introduce-support"  class="introduce-list">
							<?php
							if(!empty($footer_pages_support)){
								foreach ($footer_pages_support as $footer_page) { ?>
									<li><a href="<?php echo site_url($footer_page['slug']);?>" <?php if($footer_page['new_window'] ==1){echo 'target="_blank"';}?> ><?php echo $footer_page['menu_title'];?></a></li>
								<?php
								}
							}
							?>
                            <li><a href="<?php echo site_url('cart/shippingpolicy');?>">Shipping Policy</a></li>
                            <li><a href="<?php echo site_url('cart/refund');?>">Refund Policy</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3 hidden-xs">
				<div id="contact-box">
					<!-- <div class="introduce-title">Newsletter</div>
					<div class="input-group" id="mail-box">
						<input type="text" placeholder="Your Email Address"/>
						<span class="input-group-btn">
                            <button class="btn btn-default" type="button">OK</button>
                          </span>
					</div> -->
					<div class="introduce-title">Let's Socialize</div>
					<div class="social-link">
						<a target="_blank" href="https://www.facebook.com/CartnYou-277729052962135/"><i class="fa fa-facebook"></i></a>
						<!--<a target="_blank" href="http://youtube.com/c/"><i class="fa fa-youtube"></i></a>-->
						<a target="_blank" href="https://www.instagram.com/cartnyou/"><i class="fa fa-instagram-p fa-instagram"></i></a>
						<a target="_blank" href="https://twitter.com/CartnYou/"><i class="fa fa-twitter"></i></a>
						<!--<a target="_blank" href="http://google.com/"><i class="fa fa-google-plus"></i></a>
						<a target="_blank" href="https://chat.whatsapp.com/"><i class="fa whatsapp-btn fa-whatsapp"></i></a>
						<a target="_blank" href="https://www.pinterest.com/"><i class="fa pintrest-btn fa-pinterest"></i></i></a>-->

					</div>
					<div class="mt-20">
						<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=M16dT49vVklRvgehn2u0Q7q6P0Bd6OeTvTcxDlBQzz9QOTQAd2VKJd8VGY83"></script></span>
					</div>
				</div>

			</div>
		</div><!-- /#introduce-box -->

		<!-- #trademark-box -->
       <!-- <div class="bottomBorder visible-xs"></div>-->
		<div id="trademark-box" class="row hidden-xs">
			<div class="col-sm-12">
				<ul id="trademark-list">
					<li id="payment-methods">Accepted Payment Methods and Logistic Partners</li>
					<li>
						<a href="#"><img src="<?php echo theme_img('/secure.jpg');?>"  alt="payment-methods"/></a>
					</li>

				</ul>
			</div>
		</div> <!-- /#trademark-box -->

		<!-- #trademark-text-box -->
		<div id="trademark-text-box" class="row hidden-xs">
			<div class="col-sm-12" id="trademark-search-list-container">
				<ul id="trademark-search-list" class="trademark-list">
					<li class="trademark-text-tit">HOT SEARCHED KEYWORDS:</li>
				</ul>
			</div>

			<?php
			if(!empty($this->featured_categories)){
				for ($fc=0 ; $fc<count($this->featured_categories) ; $fc++) {
					$featured_category = $this->featured_categories[$fc];
					if(count($featured_category['child']) > 0){
			?>
						<div class="col-sm-12">
							<ul class="trademark-list">
								<li class="trademark-text-tit"><?php echo $featured_category['name'];?>:</li>
								<?php
								if(!empty($featured_category['child'])){
									foreach ($featured_category['child'] as $child) { ?>
										<li><a href="<?php echo site_url().$child['slug'];?>" target="_blank"><?php echo $child['name'];?></a></li>
									<?php
									}
								}
								?>
							</ul>
						</div>
			<?php
					}
				}
			}
			?>

		</div><!-- /#trademark-text-box -->
		<div id="footer-menu-box" class="hidden-xs">

			<p class="text-center hidden-xs colorLight2">Copyrights &#169; 2018 CartnYou Ventures Pvt. Ltd. Developed with <i class="fa fa-heart"></i> by <a href="http://inikworld.com">InikWorld Technologies</a></p>
		</div><!-- /#footer-menu-box -->
	</div>
</footer>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            <div class="modal-body">
                <div class="row lg">
                	<div class="col-sm-6 left-block hidden-xs">
                		<div class="left-block-container login-content-login">
	                		<h1>HELLO!</h1>
	                		<h2>WE ARE HAPPY TO SEE YOU AGAIN</h2>
	                		<div class="mt200">
	                			Don't have an account<span class="qs colorWhite">?</span> <a href="javascript:;" class="showSignupForm white"><b><i>Register Now</i></b></a> and enjoy all our benefits!
	                		</div>
                		</div>
                		<div class="left-block-container login-content-register">
	                		<h1>WELCOME!</h1>
	                		<h2>REGISTER AND ENJOY OUR BENEFITS</h2>
	                		<div class="mt270">
	                			Already a member<span class="qs">?</span> <a href="javascript:;" class="showLoginForm white"><b><i>Login Now</i></b></a>
	                		</div>
                		</div>
                	</div>
                    <div class="col-sm-6 pbt15">
                    	<h3 class="mbt20 login-content-login">Login to your Account</h3>
                    	<h3 class="mbt20 login-content-register">Register Here!</h3>

                        <?php echo form_open('secure/login/ajax', array('id'=>'login-form','class'=>'login_form loginForm')); ?>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <input class="hidden" name="remember" value="true" type="checkbox" />

                            <div class="mtb-5 text-right mbt10">
                                <a href="<?php echo site_url('secure/forgot-password'); ?>" class="grey">Forgot Password<span class="qs">?</span></a>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-red" type="submit">LOGIN</button>
                            </div>

                        	<input type="hidden" value="submitted" name="submitted"/>


	                        <div class="form-group text-center visible-xs">
	                            <p>Not registered yet<span class="qs">?</span> <a href="javascript:;" class="showSignupForm colorRed"> Register Here</a></p>
	                        </div>
                        </form>
                        <!--<div class="form-group">
                            <p class="or orCss"><span>OR</span></p>
                        </div>-->

                        <?php
						$company	= array('id'=>'bill_company', 'class'=>'form-control', 'name'=>'company', 'value'=> set_value('company'));
						$first		= array('id'=>'bill_firstname', 'class'=>'form-control', 'name'=>'firstname', 'value'=> set_value('firstname'),'placeholder'=>'Name');
						$last		= array('id'=>'bill_lastname', 'class'=>'form-control', 'name'=>'lastname', 'value'=> set_value('lastname'),'placeholder'=>'Last Name');
						$email		= array('id'=>'bill_email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email'),'placeholder'=>'Email');
						$phone		= array('id'=>'bill_phone', 'class'=>'form-control', 'name'=>'phone', 'value'=> set_value('phone'),'placeholder'=>'Password');
						?>

                       	<?php echo form_open('secure/register/ajax', array('id'=>'register-form','class'=>'login_form signUpForm')); ?>
                            <div class="form-group">
                                <?php echo form_input($first);?>
                            </div>
                            <div class="form-group hidden">
                                <?php echo form_input($last);?>
                            </div>
                            <div class="form-group">
                                <?php echo form_input($email);?>
                            </div>
                            <div class="form-group">
								<input class="form-control last" type="password" autocomplete="off" name="password" placeholder="Password"/>
							</div>
							<!-- <div class="form-group">
								<label>Confirm Password</label>
								<input class="form-control last" type="password" autocomplete="off" name="confirm" placeholder="Confirm Password"/>
							</div> -->
							<input name="email_subscribe" class="hidden" value="1" type="checkbox" id="categorymanufacturer1" <?php echo set_radio('email_subscribe', '1', TRUE); ?> />
							<div class="form-group text-center">
                                <button class="btn btn-red" type="submit">Sign Up</button>
                                <p class="mtb-5 visible-xs">Already have an account<span class="qs">?</span> <a href="javascript:;" class="colorRed showLoginForm">Login Here</a></p>
                            </div>
                        </form>

                        <div class="text-center mbt20 or-div">Or Login With Your Social Profile</div>

                        <div class="mbt20 text-center">
                        	<div class="social-link social-link-round">
								<a href="javascript:;" id="facebook-login-btn" onclick="fbLogin()"><i class="fa fa-facebook"></i></a>
								<a href="javascript:;" id="google-login-btn"><i class="fa fa-google-plus"></i></a>
							</div>
                        </div>

                        <!-- <div class="form-group pdt20p">
                            <a class="loginBtn btn-block loginBtn--facebook mt-20" href="javascript:;" id="facebook-login-btn" onclick="fbLogin()">
                                Facebook
                            </a>
                            <a class="loginBtn btn-block loginBtn--google" href="javascript:;" id="google-login-btn">
                                Google+
                            </a>
                        </div> -->
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="phoneVerificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Update Phone</h4>
            </div>
            <div class="modal-body">
                <div class="row lg">
                    <div class="col-md-12">
                    	<form id="phone-verification-form" type="POST" class="form-horizontal">
                    		<div class="form-group">
                    			<label class="col-md-4 control-label" for="textinput">Mobile Number</label>
                    			<div class="col-md-8">
                    				<input name="phone" id="phone-verification-form-phone" placeholder="An otp will be sent to this number." class="form-control input-md" type="text">
                    			</div>
                    		</div>

                    		<div class="form-group">
                    			<div class="col-md-8 col-md-offset-4">
                    				<button type="submit" class="btn btn-primary">Submit</button>
                    			</div>
                    		</div>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="otpVerificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">OTP Confirmation</h4>
            </div>
            <div class="modal-body">
                <div class="row lg">
                    <div class="col-md-12">
                    	<form id="otp-verification-form" type="POST" class="form-horizontal">
                    		<div class="form-group">
                    			<label class="col-md-4 control-label" for="textinput">OTP</label>
                    			<div class="col-md-8">
                    				<input name="otp" id="otp-verification-form-phone" placeholder="" class="form-control input-md" type="text">
                    				<a href="javascript:;" id="resend-otp-btn">Resend OTP</a> |
                    				<a href="#" data-toggle="modal" data-target="#phoneVerificationModal">Change Phone</a>
                    			</div>
                    		</div>

                    		<div class="form-group">
                    			<div class="col-md-8 col-md-offset-4">
                    				<button type="submit" class="btn btn-primary">Submit</button>
                    			</div>
                    		</div>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->

<?php echo theme_js('bootstrap.min.js', true);?>
<?php echo theme_js('select2.min.js', true);?>
<?php echo theme_js('jquery.bxslider.min.js', true);?>
<?php echo theme_js('owl.carousel.min.js', true);?>
<?php echo theme_js('jquery.countdown.min.js', true);?>
<?php echo theme_js('jquery.elevatezoom.js', true);?>
<script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js"></script>
<?php echo theme_js('jquery.actual.min.js', true);?>
<?php echo theme_js('jquery.lazyload.min.js', true);?>
<?php echo theme_js('theme-script.js', true);?>
<?php echo theme_js('alertify.js', true);?>

<script>
var tempUId = 0;
$(function(){
    $("img.lazy").Lazy();

    $('#clogin').on('click',function(){
        $('#loginModal').modal('show');
        $('#wsnavtoggle').click();
    });






	$('.modal').on('show.bs.modal', function () {
        $('.modal').modal('hide');
    });

    $('#loginModal').on('show.bs.modal', function () {
        $('.showLoginForm').trigger('click');
    });

    $('.showSignupForm').on('click',function(){
    	$('.login-content-login').hide();
    	$('.login-content-register').show();
        $('.loginForm').hide();
    	$('.signUpForm').show();
    });
    $('.showLoginForm').on('click',function(){
    	$('.login-content-register').hide();
    	$('.login-content-login').show();
        $('.loginForm').show();
        $('.signUpForm').hide();
    });

    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $('.toSpecs').on('click',function(){
        $('html, body').animate({
            scrollTop: $("#specs").offset().top-150
        }, 1000);
    });
    $('.getTc').on('click',function(){
        $('.tcContainer').show();
    });
    $('.tcclose').on('click',function(){
        $('.tcContainer').hide();
    });

    $(document).on("submit", '#login-form', function(e){
        e.preventDefault();
        $('.error').remove();
        var ele = $('#login-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
                location.reload();
            } else if(getdata.code == 501){
            	tempUId = getdata.id;
            	$('#phoneVerificationModal').modal('show');
            } else if(getdata.code == 404){
            	alertify.alert('Incorrect Email/Password combination.');
            } else {
                alertify.alert("Oops! Something went wrong.");
            }
        });
    });

    $(document).on("submit", '#register-form', function(e){
        e.preventDefault();
        $('.error').remove();
        var ele = $('#register-form');
        var data = ele.serialize();
        var url = ele.attr('action');
        $.ajax({
            data : data,
            url : url,
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
                ele[0].reset();
                tempUId = getdata.id;
            	$('#phoneVerificationModal').modal('show');
            } else {
                if(getdata.hasOwnProperty('error')) {
                    $.each(getdata.error, function(key, val) {
                       $('#register-form input[name='+key+']').after(val);
                   });
                } else {
                   alertify.alert("Oops! Something went wrong.");
                }
            }

        });
    });

    function get_mini_cart(){
    	$.ajax({
            url : '<?php echo site_url("cart/get_mini_cart");?>',
            method: 'GET'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            $('.items_total_string').html(getdata.items_total_string);
        	$('.cart-items-count').html(getdata.items);
        	$('.cart-items-total').html(getdata.total);
        });
    }
    get_mini_cart();

    $(document).on("click", '.add-to-cart-cart-btn', function(e){
        e.preventDefault();
        var $this = $(this);

        $.ajax({
            data : {
            	id: $this.attr('data-id'),
            	pm_id: $this.attr('data-pm-id'),
            	quantity: 1
            },
            url : '<?php echo site_url("cart/add_to_cart/ajax");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
        	//console.log(getdata);return;
            if (getdata.success) {
            	get_mini_cart();
                //alertify.alert(getdata.data);
                //alertify.success('Product Added in Cart Successfully');

                alertify.success('Product Added in Cart Successfully');
                $('.alertify').find('button').hide();
                $('.alertify').find('.msg').hide();
            } else {
                window.location = getdata.data;
            }

        });
    });

    $(document).on('click','.hide-alertify',function(e){
    	e.preventDefault();
    	$('.alertify').remove();
    });

    $(document).on("click", '.add-to-wishlist', function(e){
        e.preventDefault();
        var $this = $(this);

        $.ajax({
            data : {
            	id: $this.attr('data-id'),
            	pm_id: $this.attr('data-pm-id')
            },
            url : '<?php echo site_url("cart/add_to_wishlist");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.success) {
                alertify.alert(getdata.data);
            } else {
                alertify.alert(getdata.data);
            }

        });
    });

    $(document).on("click", '.remove-from-wishlist', function(e){
        e.preventDefault();
        var $this = $(this);

        $.ajax({
            data : {
            	id: $this.attr('data-id'),
            	pm_id: $this.attr('data-pm-id')
            },
            url : '<?php echo site_url("cart/remove_from_wishlist");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.success) {
                location.reload();
            } else {
                alertify.alert(getdata.data);
            }

        });
    });

    function get_hot_search_keywords(){
    	$.ajax({
            url : '<?php echo site_url("cart/get_hot_search_keywords");?>',
            method: 'GET'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
        	if (getdata.success) {
            	$.each(getdata.data, function(i,row){
            		$('#trademark-search-list').append('<li><a class="text-capitalize" href="<?php echo site_url("cart/search");?>/'+row.code+'/0">'+row.term+'</a></li>');
            	});
                $('#trademark-search-list-container').show();
            } else {
                $('#trademark-search-list-container').hide();
            }
        });
    }
    get_hot_search_keywords();

    $('.dropdown').on('click',function(e){
			if($(window).width() < 768){
				$(this).on('show.bs.dropdown', function () {
					$(this).find('div.dropdown-backdrop').remove();
				});
			}
			// $(this).find('.dropdown-backdrop').css('display','none!important');
		setTimeout($.proxy(function() {
		    if ('ontouchstart' in document.documentElement) {


		        $(this).siblings('.dropdown-backdrop').off().remove();
		    }
		}, this), 0);

	});
	// $('.dropdown-backdrop').on('click',function(e){
	// 	e.stopPropagation();
	// 	e.preventDefault();
	// });

	$('.dropdown-submenu a.drmenu').on('click',function(e){
		$(this).next('ul').toggle();
		e.stopPropagation();
		e.preventDefault();
	});
});
</script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId      : '736223796457078',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });

    /*FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            getFbUserData();
        }
    });*/
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            getFbUserData();
        } else {
        	console.log('User cancelled login or did not fully authorize.');
        }
    }, {scope: 'email'});
}

function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,name,email,link,gender,locale,picture'},
    function (response) {
        document.getElementById('facebook-login-btn').setAttribute("onclick","fbLogout()");
        document.getElementById('facebook-login-btn').innerHTML = 'Logout';
        console.log(response);

        $.ajax({
            data : {
            	provider_id: response.id,
            	name: response.name,
            	lastname: response.last_name,
            	email: response.email,
            	provider: 'Facebook'
            },
            url : '<?php echo site_url("secure/social_login");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
                location.reload();
            } else if(getdata.code == 501){
            	tempUId = getdata.id;
            	$('#phoneVerificationModal').modal('show');
            } else {
                alertify.alert("Oops! Something went wrong.");
            }
        });

    });
}

function fbLogout() {
    FB.logout(function() {
        document.getElementById('facebook-login-btn').setAttribute("onclick","fbLogin()");
        document.getElementById('facebook-login-btn').innerHTML = 'Facebook';
    });
}

$(function(){
	$('#phone-verification-form').on('submit',function(e){
		e.preventDefault();
        $.ajax({
            data : {
            	id: tempUId,
            	phone : $('#phone-verification-form-phone').val()
            },
            url : '<?php echo site_url("secure/update_phone");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
            	$('#otpVerificationModal').modal('show');
            } else if(getdata.code == 501){
            	alertify.alert('The requested phone number is already in use.');
            } else if(getdata.code == 404){
            	alertify.alert(getdata.error);
            } else {
                alertify.alert("Oops! Something went wrong.");
            }
        });
	});

	$('#otp-verification-form').on('submit',function(e){
		e.preventDefault();
        $.ajax({
            data : {
            	id: tempUId,
            	otp : $('#otp-verification-form-phone').val()
            },
            url : '<?php echo site_url("secure/verify_otp");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
            	location.reload();
            } else if(getdata.code == 404){
            	alertify.alert(getdata.error);
            } else {
                alertify.alert("Incorrect OTP.");
            }
        });
	});

	$('#resend-otp-btn').on('click',function(e){
		//$('#phone-verification-form').submit();

		e.preventDefault();
        $.ajax({
            data : {
            	id: tempUId,
            	phone : $('#phone-verification-form-phone').val()
            },
            url : '<?php echo site_url("secure/update_phone");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
            	//$('#otpVerificationModal').modal('show');
            } else if(getdata.code == 501){
            	alertify.alert('The requested phone number is already in use.');
            } else if(getdata.code == 404){
            	alertify.alert(getdata.error);
            } else {
                alertify.alert("Oops! Something went wrong.");
            }
        });
	});

//	if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {
//	   	var styleEl = document.createElement('style'), styleSheet;
//	   	document.head.appendChild(styleEl);
//	   	styleSheet = styleEl.sheet;
//		styleSheet.insertRule(".modal { position:absolute; bottom:auto; }", 0);
//	}

});
</script>

<script src="https://apis.google.com/js/api:client.js"></script>
<script type="text/javascript">
var googleUser = {};
var startApp = function() {
    gapi.load('auth2', function(){
        auth2 = gapi.auth2.init({
            client_id: '975064320986-2i4cm3k35c021k34a6lvoesu4viipvet.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignin(document.getElementById('google-login-btn'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
    function(googleUser) {

    	$.ajax({
            data : {
            	provider_id: googleUser.getBasicProfile().getId(),
            	firstname: googleUser.getBasicProfile().getName(),
            	lastname: googleUser.getBasicProfile().getFamilyName(),
            	email: googleUser.getBasicProfile().getEmail(),
            	provider: 'Google'
            },
            url : '<?php echo site_url("secure/social_login");?>',
            method: 'POST'
        }).done(function(getdata){
        	getdata = JSON.parse(getdata);
            if (getdata.result) {
                location.reload();
            } else if(getdata.code == 501){
            	tempUId = getdata.id;
            	$('#phoneVerificationModal').modal('show');
            } else {
                alertify.alert("Oops! Something went wrong.");
            }
        });
    }, function(error) {
        //alertify.alert(JSON.stringify(error, undefined, 2));
    });
}
startApp();
</script>

<!-- mailchimp code -->
<!-- <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/4cff9685c0f9e58815727e473/ea8247aa9b7ffc8cc73b719d5.js");</script> -->

<!--Start of Zendesk Chat Script-->
<!--<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4FLMWhSWgfU7PKplmLEHoFSN9PaV8n9D";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>-->
<!--End of Zendesk Chat Script-->
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b7178fdafc2c34e96e78614/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script> -->
<!--End of Tawk.to Script-->
<!--<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>-->
<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/serviceWorker.js')
            .then(function (response) {

                // Service worker registration done
                console.log('Registration Successful', response);
            }, function (error) {
                // Service worker registration failed
                console.log('Registration Failed', error);
            });
    }
</script>
</body>
</html>
