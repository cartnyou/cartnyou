<form class="form-horizontal" method="post" action="<?php echo site_url('merchant/support/request_category_save')?>">
    <div class="row">
        <div class="col-md-12">
          <?php if(isset($categories[0])):?>
            <label><strong><?php echo lang('select_a_category');?></strong></label>
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                  <th colspan="2"><?php echo lang('name')?></th>
                </tr>
              </thead>
              <?php
              function list_categories($parent_id, $cats, $sub='', $merchant_categories) {
      
                foreach ($cats[$parent_id] as $cat):

                  if(isset($cats[$cat->id])){
                    $has_child_string = 'readonly="readonly" ';
                  } else {
                    $has_child_string = '';
                  }

                  if(isset($merchant_categories[$cat->id])){
                    if($merchant_categories[$cat->id]->enabled == 1){
                      echo '<tr class="success"><td>'.$sub.$cat->name.'</td><td><input '.$has_child_string.' id="cat-'.$cat->id.'" data-parent="cat-'.$parent_id.'" class="category-checkbox" type="checkbox" name="categories[]" value="'.$cat->id.'" checked="checked" /> Approved</td></tr>';
                    } else {
                      echo '<tr class="warning"><td>'.$sub.$cat->name.'</td><td><input '.$has_child_string.' id="cat-'.$cat->id.'" data-parent="cat-'.$parent_id.'" class="category-checkbox" type="checkbox" name="categories[]" value="'.$cat->id.'" checked="checked" /> Pending</td></tr>';
                    }
                  } else {
                    echo '<tr><td>'.$sub.$cat->name.'</td><td><input '.$has_child_string.' id="cat-'.$cat->id.'" data-parent="cat-'.$parent_id.'" class="category-checkbox" type="checkbox" name="categories[]" value="'.$cat->id.'" /></td></tr>';
                  }
                  
                  if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0)
                  {
                    $sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
                      $sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
                    list_categories($cat->id, $cats, $sub2, $merchant_categories);
                  }
                endforeach;
              }
            
              list_categories(0, $categories, '', $merchant_categories);
              ?>
            </table>
          <?php else:?>
            <div class="alert"><?php echo lang('no_available_categories');?></div>
          <?php endif;?>
        </div>
    </div>
    
    <div class="row">
        <div class="form-group">
          <label for=""></label>
          <div class="col-md-8">
            <button class="btn btn-info" type="submit">Submit</button>
          </div>
        </div>
    </div>
</form>

<script type="text/javascript">
$(function(){
  $('.category-checkbox').on('click',function (event) {
    var ele = $(this);

    if(this.checked) {
      this.checked = true;
      var parent_id = $(this).data('parent').substring(4);
      parent_selector(parent_id, 'check');
    } else {
      this.checked = false;
      var parent_id = $(this).data('parent').substring(4);
      parent_selector(parent_id, 'uncheck');
    }
  });

  function parent_selector(parent_id, mode){
    if(parent_id != 0){
      var ele = $("#cat-"+parent_id);

      if(mode == 'check'){
        ele.prop('checked', true);
      } else {
        var childs = $('[data-parent="cat-'+parent_id+'"]');
        var stcheck = false;
        $.each(childs, function(i,row){
          if($(row).prop('checked') == true){
            stcheck = true;
          }
        });
        
        if(stcheck == false){
          ele.prop('checked', false);
        }  
      }
      
      var parent_id_this = ele.data('parent').substring(4);
      parent_selector(parent_id_this, mode);
    }
  }

});
</script>